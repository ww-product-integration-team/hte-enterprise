ALTER TABLE hte.autoassignmentrules
	MODIFY banner varchar(150) NOT NULL,
    MODIFY region varchar(150) NOT NULL,
    MODIFY store varchar(150) NOT NULL,
    MODIFY dept varchar(150) NOT NULL,
    MODIFY enabled tinyint(1) NOT NULL DEFAULT '1',
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (ipAddressStart, ipAddressEnd, banner, region, store, dept, enabled),
    ADD UNIQUE KEY (ipAddressStart, ipAddressEnd, banner, region, store, dept, enabled);
    
ALTER TABLE hte.upgradedevice
	MODIFY status enum('unassigned','awaiting_upload','awaiting_primary','ready_to_upload','upload_in_progress','upload_completed_no_errors','upload_completed_errors','upload_failed','upload_now','file_uploaded_awaiting_upgrade','ready_to_upgrade','upgrade_in_progress','post_install_process','upgrade_completed_no_errors','upgrade_completed_errors','upgrade_failed','upgrade_now') NOT NULL DEFAULT 'unassigned',
    ADD COLUMN fileSelected varchar(45) DEFAULT NULL AFTER groupId;
    
ALTER TABLE hte.upgradegroup
	ADD COLUMN batchGroup bit(1) NOT NULL AFTER status;