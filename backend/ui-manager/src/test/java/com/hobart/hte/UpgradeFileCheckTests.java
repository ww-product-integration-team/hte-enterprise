package com.hobart.hte;

import com.hobart.hte.repositories.*;
import com.hobart.hte.upgrading.fileChecking.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class UpgradeFileCheckTests {

    @Autowired
    UpgradeDeviceRepository upgradeDeviceRepo;

    @Autowired
    DeviceRepository deviceRepo;

    FileCheckFactory fileCheckFactory;

    private final String ipAddress = "1.1.1.1"; // Bogus Class-A address -- strictly for testing!

    @BeforeEach
    public void setup() {
        fileCheckFactory = new FileCheckFactory();
    }

    @Test
    public void testHtFeatureFileType() {
        String htFile = "hti-linux-feature-hte_enterprise_test_file-4.2.0-07192024.package.tgz";
        UpgradeFileCheckStrategy testStrat = fileCheckFactory.determineFileCheckLogic(
                ipAddress, htFile, false, deviceRepo, upgradeDeviceRepo);

        assertTrue(testStrat instanceof HTFeatureFileCheck,
                "The example file was not determined to be a feature file when it should be one.");

        assertEquals("hte_enterprise_test_file", ((HTFeatureFileCheck)testStrat).getFeatureName(),
                "Example file did not have the expected feature name. Expected 'hte_enterprise_test_file', got: " +
                        ((HTFeatureFileCheck)testStrat).getFeatureName());
    }

    @Test
    public void testHtApplicationUpgradeFileType() {
        String htFile = "hti-linux-application-4.1.890-04272023_081054.package.tgz";
        UpgradeFileCheckStrategy testStrat = fileCheckFactory.determineFileCheckLogic(
                ipAddress, htFile, false, deviceRepo, upgradeDeviceRepo);

        assertTrue(testStrat instanceof HTUpgradeFileCheck,
                "The example app-upgrade file was not determined to be an HT feature file when it should be one.");
    }

    @Test
    public void testHtCoreUpgradeFileType() {
        String htFile = "hti-linux-core-4.2.200-03272024_101601.package.tgz";
        UpgradeFileCheckStrategy testStrat = fileCheckFactory.determineFileCheckLogic(
                ipAddress, htFile, false, deviceRepo, upgradeDeviceRepo);

        assertTrue(testStrat instanceof HTUpgradeFileCheck,
                "The example core-upgrade file was not determined to be an HT upgrade file when it should be one.");
    }

    @Test
    public void testHtMonolithicUpgradeFileType() {
        String htFile = "hti-linux-feature-monolithic_core-4.2.0-05232023_130708.package.tgz";
        UpgradeFileCheckStrategy testStrat = fileCheckFactory.determineFileCheckLogic(
                ipAddress, htFile, false, deviceRepo, upgradeDeviceRepo);

        assertTrue(testStrat instanceof HTUpgradeFileCheck,
                "The example monolithic-upgrade file was not determined to be an HT upgrade file when it should be one.");
    }

    @Test
    public void testGtFeatureFileType() {
        String gtFile = "something-something.deb";

        UpgradeFileCheckStrategy testStrat = fileCheckFactory.determineFileCheckLogic(
                ipAddress, gtFile, false, deviceRepo, upgradeDeviceRepo);

        assertTrue(testStrat instanceof GTUpgradeFileCheck,
                "The example feature file was not determined to be a GT feature file when it should be one.");
    }

    @Test
    public void testGtApplicationUpgradeFileType() {
        String gtFile = "ngs-application_0.0.9-20231201-151336868_rk3568-hobart-x11.tgz";

        UpgradeFileCheckStrategy testStrat = fileCheckFactory.determineFileCheckLogic(
                ipAddress, gtFile, false, deviceRepo, upgradeDeviceRepo);

        assertTrue(testStrat instanceof GTUpgradeFileCheck,
                "The example application file was not determined to be a GT application file when it should be one.");
    }

    @Test
    public void testGtRecoveryUpgradeFileType() {
        String gtFile = "ngs-linux-recovery_0.0.79-20240716-183709872_rk3568-hobart-x11.img.bz2";

        UpgradeFileCheckStrategy testStrat = fileCheckFactory.determineFileCheckLogic(
                ipAddress, gtFile, false, deviceRepo, upgradeDeviceRepo);

        assertTrue(testStrat instanceof GTUpgradeFileCheck,
                "The example recovery file was not determined to be a GT recovery file when it should be one.");
    }
}
