// Commenting this whole file out until these are refactored into test cases with proper assertions.

/*
package com.hobart.hte;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.hobart.hte.repositories.*;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.stats.DeviceCountPerFirmware;
import com.hobart.hte.utils.device.stats.DeviceCountPerModel;
import com.hobart.hte.utils.device.stats.DeviceCountPerOnlineStatus;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.event.DeviceStatusEntry;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HteEnterpriseApplicationTests {
	@Autowired
	DeviceRepository deviceRepo;

	@Autowired
	FileRepository fileRepo;

	@Autowired
	DeviceStatusLogRepo devStatusLog_repo;

	@Autowired
	DeviceStatusLogRepo deviceSyncRepo;

	@Test
	void contextLoads() {
	}

//	@Test	
//	void updateProfileInDevices() {
//		System.out.println("update the profile some devices are assigned to");
//
//		String _new = null;
//		String current = "c98a4699-8efd-abba-affa-4077de1e8977";
//
//		System.out.printf("new: %s,\nold: %s\n", _new, current);
//
//		int updated = deviceRepo.updateScaleDeviceSetProfileIdForProfileId(_new, current);
//		
//		System.out.printf("records updated: %d\n", updated);
//		
//		// list the null values		
//		String profileid = null; 
//		List<ScaleDevice> findByProfileId = deviceRepo.findByProfileId(profileid);
//
//		for (ScaleDevice s : findByProfileId) {
//			System.out.printf("\t%s\n", s.getIpAddress());
//		}
//		
//		System.out.println("--- end --- ");
//	}

	@Test
	void getDeviceswithProfile() {
		System.out.println("retrieve devices assigned to a profile ");

		String profileid = null; // "c98a4699-8efd-abba-affa-4077de1e8977");

		List<ScaleDevice> findByProfileId = deviceRepo.findByProfileId(profileid);

		for (ScaleDevice s : findByProfileId) {
			System.out.printf("\t%s\n", s.getIpAddress());
		}
		System.out.println("--- end --- ");
	}

	@Test
	void getScaleCountByFirmware() {
		List<DeviceCountPerFirmware> totalCount = deviceRepo.countTotalDevicesByFirmwareVersion();

		for (DeviceCountPerFirmware d : totalCount) {
			System.out.printf("firware version: %s, count: %d\n", d.getApplication(), d.getTotal());
		}
	}

	@Test
	void getScaleCountByModel() {
		List<DeviceCountPerModel> totalCount = deviceRepo.countTotalDevicesByModel();

		for (DeviceCountPerModel d : totalCount) {
			System.out.printf("firware version: %s, count: %d\n", d.getScaleModel(), d.getTotal());
		}
	}

	@Test
	void retrieveFiles() {
		String profile_id = "00000000-0000-0000-0000-000000000000";
		List<FileForEvent> allFilesInProfile = fileRepo.findByProfileIdAndEnabled(profile_id, true);

		if (allFilesInProfile != null) {
			System.out.println("retrieved elements: " + allFilesInProfile.size());
			for (FileForEvent f : allFilesInProfile) {
				System.out.println("file: " + f.getFilename());
			}
		} else {
			System.out.println("retrieved null");
		}
	}

	@Test
	void onlineStatus() {
		Timestamp timestamp = new Timestamp(new Date().getTime());
		System.out.println(timestamp);

		Calendar cal = Calendar.getInstance();

		cal.setTimeInMillis(timestamp.getTime());
		cal.add(Calendar.HOUR, -2);
		timestamp = new Timestamp(cal.getTime().getTime());
		System.out.println(timestamp);

		List<DeviceCountPerOnlineStatus> onlineStatus = deviceRepo
				.countTotalDevicesOnlineStatus(Timestamp.from(Instant.now()), timestamp);

		System.out.println("records retrieved: " + onlineStatus.size());
	}

	@Test
	void cleanString() {
		String tmp = "[OFFICE_VIEWER, FBVNC, RDP, WM_CAKE_LABELLIC, PDF_VIEWER, BLUEZ, WALMART, WPASUPPLICANT]";

		String new_s = tmp.replaceAll("(\\[|\\]| )", "");

		System.out.println("clean string: " + new_s);

		tmp = "[WalMart\n" + "WAL-MART STORES INC.\n" + "BENTONVILLE, AR 72716\n" + "Products:\n" + "SCALE_PDF_VIEWER\n"
				+ "SCALE_REMOTE_MONITOR\n" + "SCALE_FPP\n" + "Type: USB_SERIAL_NUMBER\n" + "Allotment: 1\n"
				+ "Key: 00F67C49BCF5A88AB2BE0B4B4A3EF2CA49B1E27E, SPENCER STOKES\n" + "WAL-MART INC.\n"
				+ "805 MOBERLY LANE\n" + "Products:\n" + "SCALE_REMOTE_PRINTING\n" + "Type: USB_SERIAL_NUMBER\n"
				+ "Allotment: 1\n" + "Key: 023FC05B7B392D98DAAEFFA390801342007A95F0]";

		StringBuilder sb = new StringBuilder();
		String[] split = tmp.split("\\n");
		for (String s : split) {
			if (s.startsWith("SCALE_")) {
				sb.append(s.trim()).append(",");
			}
		}

	}

	/*-
	@Test
	void deleteUpgradeFile() {
		String filename = "configuration_data.ht";
	
		long deleteByFilename = fileRepo.deleteByFilename(filename);
	
		System.out.println("records deleted: " + deleteByFilename);
	
	}

	@Test
	void retrieveStatusLog() {
		String device_uuid = "167ea0b5-6b05-11eb-998a-fa163e138193";

		List<DeviceStatusEntry> findTop24ByDeviceUuid = devStatusLog_repo.findTop24ByDeviceUuid(device_uuid);

		for (DeviceStatusEntry entry : findTop24ByDeviceUuid) {
			System.out.println("device entry: " + entry.getTimestamp() + ", " + entry.getDeviceUuid());
		}

	}

	@Test
	void retrieveSyncStatus() {
		String device_uuid = "167ea0b5-6b05-11eb-998a-fa163e138193";

		Timestamp end = new Timestamp(System.currentTimeMillis());
		System.out.println("[end] current time: " + end.toString());

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, -24);
		Timestamp start = new Timestamp(cal.getTimeInMillis());
		System.out.println("[start] 24 hours ago: " + start.toString());

		List<DeviceStatusEntry> listofscales = deviceSyncRepo.findByTimestampBetweenAndDeviceUuid(start, end, device_uuid);

		for (DeviceStatusEntry d : listofscales) {
			System.out.println("status entry: " + d.toString());
		}

	}
}
*/
