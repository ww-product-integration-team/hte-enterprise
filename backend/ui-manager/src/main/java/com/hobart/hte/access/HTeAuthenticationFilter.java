package com.hobart.hte.access;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hobart.hte.helpers.DomainHelper;
import com.hobart.hte.helpers.LicenseHelper;
import com.hobart.hte.helpers.NodeHelper;
import com.hobart.hte.helpers.TokenHelper;
import com.hobart.hte.ui.UIController;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.hobart.hte.access.DomainAuthorizationFilter.USER_ENABLED_SUBSTRING;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class HTeAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private LicenseHelper licenseHelper;
    private DomainHelper domainHelper;
    private UIController uiController;
    public HTeAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        if (licenseHelper == null) { // This class type can't use injection, so do a lazy set
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils
                    .getWebApplicationContext(servletContext);
            assert webApplicationContext != null;
            licenseHelper = webApplicationContext.getBean(LicenseHelper.class);
        }

        if (domainHelper == null) { // This class type can't use injection, so do a lazy set
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils
                    .getWebApplicationContext(servletContext);
            assert webApplicationContext != null;
            domainHelper = webApplicationContext.getBean(DomainHelper.class);
        }

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,
                password);
        return authenticationManager.authenticate(authenticationToken);

    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
            Authentication authResult) throws IOException, ServletException {
        User user = (User) authResult.getPrincipal();
        // TODO Save token string elsewhere
        TokenHelper tokenHelper = new TokenHelper(user, request);
        String access_token = tokenHelper.createAccessToken();
        String refresh_token = tokenHelper.createRefreshToken();

        // User has logged in, check if they're enabled before granting access
        boolean isEnabled = false;
        for (GrantedAuthority auth : user.getAuthorities()) {
            String authority = auth.getAuthority();
            String userEnabled = authority.substring(USER_ENABLED_SUBSTRING.length());
            if (userEnabled.equals("true") || userEnabled.equals("TRUE")) {
                isEnabled = true;
                break;
            }
        }

        // If hobart admin then allow the login
        if (!licenseHelper.validLicenseInstalled() && !domainHelper.isRegisteredHobartAdmin(user.getUsername())) {
            response.setStatus(FORBIDDEN.value());
            response.setContentType(APPLICATION_JSON_VALUE);
            response.setHeader("Access-Control-Allow-Origin", "*");
            OperationStatusModel result = new OperationStatusModel("Login");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("A valid license is not installed");
            new ObjectMapper().writeValue(response.getOutputStream(), result);
            logger.error("A valid license is not installed, prevent login");
            return;
        }

        if (!isEnabled) {
            response.setStatus(FORBIDDEN.value());
            response.setContentType(APPLICATION_JSON_VALUE);
            response.setHeader("Access-Control-Allow-Origin", "*");
            OperationStatusModel result = new OperationStatusModel("Login");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Could not login, user is not enabled");
            new ObjectMapper().writeValue(response.getOutputStream(), result);
            logger.error("User is not enabled, cannot login");
            return;
        }

        logger.info("Login Success!");
        Map<String, String> tokens = new HashMap<>();
        response.setHeader("Access-Control-Allow-Origin", "*");

        tokens.put("access_token", access_token);
        tokens.put("refresh_token", refresh_token);
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(), tokens);

    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) throws IOException, ServletException {

        response.setStatus(FORBIDDEN.value());
        response.setContentType(APPLICATION_JSON_VALUE);
        response.setHeader("Access-Control-Allow-Origin", "*");
        OperationStatusModel result = new OperationStatusModel("Login");
        result.setResult(RequestOperationResult.ERROR.name());
        result.setErrorDescription("Could not login, incorrect username or password");
        new ObjectMapper().writeValue(response.getOutputStream(), result);
        logger.error("Unsuccessful Login attempt");
    }
}
