package com.hobart.hte.upgrading.fileChecking;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.UpgradeDeviceRepository;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import com.hobart.hte.utils.util.HteTools;
import com.hobart.hte.utils.device.ScaleDevice;
import com.jcraft.jsch.Session;

public class HTFeatureFileCheck extends UpgradeFileCheckStrategy {

    DeviceRepository deviceRepo;
    UpgradeDeviceRepository upgradeDeviceRepo;

    private String ipAddress;
    private String fileName;
    private String featureName;
    private final int upgradeTime = 10;

    public HTFeatureFileCheck(
            String ipAddress, String fileName, DeviceRepository deviceRepo, UpgradeDeviceRepository upgradeDeviceRepo) {
        this.ipAddress = ipAddress;
        this.fileName = fileName;
        this.deviceRepo = deviceRepo;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
        this.featureName = fileName.split("-")[3];
    }

    /* This check, to be politic about it, is approximative.

        Most HT feature files leave behind a directory in /usr/local/hobart/features/ containing the extracted tarball,
        which is usually sufficient to substring from -- and we will do just that in this class -- but there are a few
        rare feature files that pull a Mission: Impossible routine and delete that directory after the upgrade.

        Again, said files are rare, and the users of those files will likely never even use this mechanism,
        let alone HTe Enterprise, but it's worth noting for future troubleshooting.
     */
    @Override
    public boolean verifyUpgrade() {

        ScaleDevice device = deviceRepo.findByIpAddress(ipAddress);
        String deviceId = device.getDeviceId();

        boolean upgradeApplied = false;

        Session sshSession = HteTools.getSSHSession(ipAddress);
        if (sshSession != null && sshSession.isConnected()) {
            String command = "cd /usr/local/hobart/features/ && find -maxdepth 1 -name '" + featureName + "' -print";
            String commandOutput = HteTools.executeSSHCommandGetOutput(sshSession, command);
            if (commandOutput.contains(featureName)) {
                upgradeApplied = true;
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_completed_no_errors.name(), "Upgrade applied successfully.", deviceId);
            } else {
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_failed.name(), "Upgrade was not applied successfully.", deviceId);
            }
        } else { // We couldn't connect; can't certify the status of the scale. Warn the user.
            upgradeDeviceRepo.updateStatusByDeviceId(
                    UpgradeDevice.Status.upgrade_failed.name(), "Could not connect to scale after applying upgrade; " +
                            "cannot verify upgrade application. ", deviceId);
        }
        HteTools.disconnectSSH(sshSession);
        return upgradeApplied;
    }

    public void run() {
        verifyUpgrade();
    }

    public int getUpgradeTime() {
        return upgradeTime;
    }

    public String getFeatureName() {
        return featureName;
    }
}
