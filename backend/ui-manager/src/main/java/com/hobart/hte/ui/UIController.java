package com.hobart.hte.ui;

import java.io.*;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.hobart.hte.assetList.FilterAPI;
import com.hobart.hte.assetList.InstantiateNodeStatus;
import com.hobart.hte.assetList.LazyLoadAssetTree;
import com.hobart.hte.assetList.LazyLoadFilterAssetTree;
import com.hobart.hte.config.NodeStatusUpdater;
import com.hobart.hte.dashboard.Dashboard;
import com.hobart.hte.helpers.DomainHelper;
import com.hobart.hte.helpers.LicenseHelper;
import com.hobart.hte.helpers.NodeHelper;
import com.hobart.hte.repositories.*;
import com.hobart.hte.scale.DeleteAllAssetsEndpoint;
import com.hobart.hte.scale.OneOffFeatureUpgradeEndPoint;
import com.hobart.hte.transaction.TransactionData;
import com.hobart.hte.transaction.TransactionDataExcelExport;
import com.hobart.hte.utils.autoAssign.AutoAssign;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.dashboard.DashboardData;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.CombinedScaleDataInt;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.event.DeviceStatusEntry;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.file.HobFiles;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.model.*;
import com.hobart.hte.utils.profile.ChecksumLog;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.repositories.StoreDeptPairsRepository;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.transaction.TransEntry;
import com.hobart.hte.utils.tree.*;
import com.hobart.hte.utils.upgrade.DeptAndBatchIds;
import com.hobart.hte.utils.upgrade.StoreAndBatchIds;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import com.hobart.hte.utils.util.HteTools;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;

import static com.hobart.hte.utils.config.AppConfig.DefaultAdminId;
import static com.hobart.hte.utils.config.AppConfig.DefaultUnassignedId;

@RestController
@RequestMapping("/ui")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class UIController {

	private static final Logger log = LogManager.getLogger(UIController.class);
	@PersistenceContext
	EntityManager entityManager;
	@Autowired
	DeviceRepository device_repo;
	@Autowired
	ConfigAppRepository configapp;
	@Autowired
	BannerRepository banner_repo;
	@Autowired
	RegionRepository region_repo;
	@Autowired
	StoreRepository store_repo;
	@Autowired
	DepartmentRepository dept_repo;
	@Autowired
	ProfileRepository profile_repo;
	@Autowired
	FileRepository file_repo;
	@Autowired
	ScaleSettingsRepository ssRepo;
	@Autowired
	NodeStatusRepository nodeStatusRepo;
	@Autowired
	StoreDeptPairsRepository storeDeptPairsRepository;
	@Autowired
	DomainRepository domain_repo;
	@Autowired
	DomainHelper domainHelper;
	@Autowired
	LicenseHelper licenseHelper;
	@Autowired
	DeviceStatusLogRepo deviceSyncRepo;
	@Autowired
	TransRepository transRepo;
	@Autowired
	UpgradeDeviceRepository upgrade_device_repo;
	@Autowired
	UpgradeGroupRepository upgrade_group_repo;
	@Autowired
	UpgradeBatchRepository upgrade_batch_repo;
	@Autowired
	FilterRepository filterRepo;
	@Autowired
	AutoAssignRepository assign_repo;
	@Autowired
	ChecksumLogRepository checksumLogRepo;
	@Autowired
	NodeStatusUpdater statusUpdater;

	private Iterable<Department> allDepts;

	@Operation(summary = "Updates the roles table", description = "Registers a user in the database")
	@PostMapping(path = "/cleanAssets")
	public ResponseEntity<OperationStatusModel> cleanAssets() {
		log.info("Cleaning Assets....");
		OperationStatusModel result = new OperationStatusModel("CleanAssets");

		//==============================================================================================================
		//ScaleDevice
		//==============================================================================================================
		//Make Sure all Devices have a deptId and storeId
		for(ScaleDevice device : device_repo.findAll()){
			if(device.getDeptId() == null || device.getStoreId() == null){
				log.info("Found a null assigned scale, reassigning:  " + device.getIpAddress());
				device.setDeptId(DefaultUnassignedId);
				device.setStoreId(DefaultUnassignedId);
				device_repo.save(device);
			}

			if(device.getProfileId() == null){
				log.info("Found a null assigned scale (profile), reassigning:  " + device.getIpAddress());
				device.setProfileId(AppConfig.DefaultProfileId);
				device_repo.save(device);
			}

			if(!device.getDeptId().equals(DefaultUnassignedId) && !dept_repo.existsById(device.getDeptId())){
				log.info("Found a hanging scale(dept), reassigning:  " + device.getIpAddress());
				device.setDeptId(DefaultUnassignedId);
				device.setStoreId(DefaultUnassignedId);
				device_repo.save(device);
			}

			if(!device.getStoreId().equals(DefaultUnassignedId) && !store_repo.existsById(device.getStoreId())){
				log.info("Found a hanging scale(store), reassigning:  " + device.getIpAddress());
				device.setDeptId(DefaultUnassignedId);
				device.setStoreId(DefaultUnassignedId);
				device_repo.save(device);
			}
		}

		//==============================================================================================================
		//Store
		//======================================================/========================================================
		for(Store store : store_repo.findAll()) {
			if(store.getEnabled() == null){
				store.setEnabled(true);
			}
			store_repo.save(store);
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		result.setErrorDescription("Assets Cleaned!");
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Parameters({
			@Parameter(name = "ip", description = "IP address of the scale to query, if no Ip provided a list with all scales will be returned. (Optional)", in = ParameterIn.QUERY, example = "10.3.128.65", schema = @Schema(implementation = String.class)) })
	@Operation(summary = "${uicontroller.getScale.Operation.Value}", description = "${uicontroller.getScale.Operation.Notes}")
	@GetMapping(path = "/scale", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getScale(@RequestParam(name = "ip", required = false) String scaleIp) {
		OperationStatusModel result = new OperationStatusModel("GetScale");
		if (scaleIp == null) {
			Long unassignedScales = device_repo.countByStoreIdAndDeptId(DefaultUnassignedId, DefaultUnassignedId);
			Long assignedScales = device_repo.countByStoreIdNotAndDeptIdNot(DefaultUnassignedId, DefaultUnassignedId);
			List<CombinedScaleDataInt> combinedData = device_repo.findCombinedScaleData();

			Map<String, Object> scaleData = new HashMap<>();
			scaleData.put("unassignedScales", unassignedScales);
			scaleData.put("assignedScales", assignedScales);
			scaleData.put("combinedScaleData", combinedData);
			return new ResponseEntity<>(scaleData, HttpStatus.OK);
		}

		try{
			ScaleDevice scale = device_repo.findByIpAddress(scaleIp);
			return new ResponseEntity<>(scale, HttpStatus.OK);
		}
		catch (Exception e){
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Error grabbing scale: " + e.getMessage());
			return new ResponseEntity<>(result, HttpStatus.OK);
		}
	}

	@Operation(summary = "Disables all scale assignment rules", description = "Prevents automatic assignment of scales")
	@PostMapping(path = "/disableAssignmentRules",  produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<OperationStatusModel> disableRules() {
		log.info("Disabling assignment rules");
		OperationStatusModel result = new OperationStatusModel("disableAssignmentRules");

		assign_repo.disableRules();
		result.setResult(RequestOperationResult.SUCCESS.name());
		log.info("Assignment rules disabled!");
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Enables all scale assignment rules", description = "Enables automatic assignment of scales")
	@PostMapping(path = "/enableAssignmentRules", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<OperationStatusModel> enableRules() {
		log.info("Enabling assignment rules");
		OperationStatusModel result = new OperationStatusModel("enableAssignmentRules");

		assign_repo.enableRules();
		result.setResult(RequestOperationResult.SUCCESS.name());
		log.info("Assignment rules enabled!");
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary="", description="")
	@GetMapping(path="/autoAssignRules", produces={MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> getAutoAssignRules() {
		log.info("Begin Get Auto Assign Rules");
		OperationStatusModel result = new OperationStatusModel("Get Auto Assign Rules");

		Iterable<AutoAssign> autoAssignRules = assign_repo.findAll();

		log.info("Finish Get Auto Assign Rules");
		return new ResponseEntity<>(autoAssignRules, HttpStatus.OK);
	}

	@Operation(summary = "Retrieve current auto-assignment rules from the database",
			description = "This one's for you, Jeremy Kromholtz")
	@GetMapping("/downloadAssignmentRules")
	public ResponseEntity<?> downloadAssignmentRules(HttpServletResponse response) {
		// The return on this is a bit unusual, but this was a deliberate design choice
		// The React frontend took some convincing to work with CSV downloads
		// This was the solution that agreed with it

		Iterable<AutoAssign> assignRecords = assign_repo.findAll();
		List<List<String>> assignmentData = new ArrayList();

		for (AutoAssign record : assignRecords) {
			List<String> exportData = new ArrayList();
			exportData.add(record.getIpAddressStart());
			exportData.add(record.getIpAddressEnd());
			exportData.add(record.getBanner());
			exportData.add(record.getRegion());
			exportData.add(record.getStore());
			exportData.add(record.getDept());

			assignmentData.add(exportData);
		}

		return new ResponseEntity<>(assignmentData, HttpStatus.OK);
	}

	@Operation(summary = "Import automatic scale assignment rules from a CSV file",
				description = "Parses and formats data from a CSV file into the assignment logic table")
	@PostMapping(path = "/importCSVToAutoAssigner", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<OperationStatusModel> importCSVToAutoAssigner (@RequestParam("file") MultipartFile file)
		throws IOException {
		OperationStatusModel result = new OperationStatusModel("ImportCSVToAutoAssigner");

		BufferedReader br;
		List<List<String>> assignmentData = new ArrayList<>(); // Used for rebuilding the CSV data from the frontend
		List<AutoAssign> autoAssignRules = new ArrayList<>(); // usd for validating IPs
		try {
			String line;
			InputStream is = file.getInputStream(); // Read bytes from what the frontend sent
			br = new BufferedReader(new InputStreamReader(is));
			String headers = br.readLine(); // Skip the first line (header), otherwise chaos will ensue
			while ((line = br.readLine()) != null) {
				List<String> record = new ArrayList<>(Arrays.asList(line.split(","))); // Split each row into its own array
				assignmentData.add(record);
			}
		} catch (IOException e) {
			result.setErrorDescription("Potential data corruption in CSV file: " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		// We'll first do some data validation before anything in the file hits the database
		int iteration = 2; // Starting at the zeroth index will just confuse customer; the file is meant to have headers
		for(List<String> dataValidationRow : assignmentData) {

			// Checking whether each row has, at minimum, the first six columns filled in
			// The user can optionally specify data in the seventh and eighth columns
			if (dataValidationRow.size() < 6 || dataValidationRow.contains("")) {
				log.debug("Incomplete file detected -- cancelling file upload.");
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Row " + iteration + " appears to be incomplete. Please review file and resubmit.");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}
			//Creating rule to help manage data access - Corey
			AutoAssign newRule = new AutoAssign(dataValidationRow.get(0), dataValidationRow.get(1), dataValidationRow.get(2),dataValidationRow.get(3), dataValidationRow.get(4), dataValidationRow.get(5));
			// Checking for profiles that match the provided department name
			if (!profile_repo.existsByNameIgnoreCase(newRule.getDept())) {
				log.debug("Department name {} at row {} did not match any profile names -- canceling file upload.", newRule.getDept(), iteration);
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Department name " + newRule.getDept() + " at row " + iteration + " did not match any profile names -- canceling file upload.");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			// Checking for valid IPv4 addresses in the first two fields
			final String ipStart = newRule.getIpAddressStart();
			final String ipEnd = newRule.getIpAddressEnd();
			if (!HttpUtils.isValidInet4Address(ipStart) || !HttpUtils.isValidInet4Address(ipEnd)) {
				log.debug("Invalid IPv4 address detected -- cancelling file upload.");
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Invalid IPv4 address detected in row " + iteration +
						". Please review file and resubmit.");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			// Checking for matching subnets in the "IPv4 start" and "IPv4 end" fields
			final String ipStartSubnet = ipStart.substring(0, ipStart.lastIndexOf("."));
			final String ipEndSubnet = ipEnd.substring(0, ipStart.lastIndexOf("."));
			if (!ipStartSubnet.equals(ipEndSubnet)) {
				log.debug("Mismatched subnet addresses detected -- cancelling file upload.");
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Mismatched IPv4 subnets detected in row " + iteration +
						". Please review file and resubmit.");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}
			/*
			// Checking whether the IP addresses already exist in the database
			 */
			final String[] ipStartArr = ipStart.split("\\.");
			final String[] ipEndArr = ipEnd.split("\\.");
			for(AutoAssign rule : autoAssignRules){
				String[] currentIPStart = rule.getIpAddressStart().split("\\.");
				String[] currentIPEnd = rule.getIpAddressEnd().split("\\.");
				// If editing a rule, IPs will be exactly the same and be dealt with later
				if(rule.getIpAddressStart().equals(String.join(".", ipStart)) && rule.getIpAddressEnd().equals(String.join(".", ipEnd))){
					log.debug("error: Duplicated IP addresses not allowed");
					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("error: Duplicated IP addresses not allowed" );
					return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
				}
				if (ipRangeConflicts(currentIPStart, ipStartArr, currentIPEnd, ipEndArr)) {
					// No exceptions for file imports -- every rule will be enabled, so we can't accept duplicates!
					log.debug("error: Overlapping IP addresses not allowed");
					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("error: Overlapping IP addresses not allowed" );
					return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			autoAssignRules.add(newRule);

			iteration++;
		}

		for (int i : new int[]{5, 4, 3, 2}) { // Sort our array by department, store, region, and banner, respectively
			// We'll do this in case the user sends data that isn't already neatly structured... which is likely
			assignmentData.sort(Comparator.comparing(strings -> strings.get(i)));
		}

		// Since we've passed the validation by this point, we'll go ahead and delete the contents of the table--
		// to make way for the new/current assignment rules
		try {
			assign_repo.deleteAll();
		} catch (Exception e) {
			log.error("Exception while removing auto assigning rules");
			result.setErrorDescription("Exception while removing auto assigning rules");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		for (List<String> row : assignmentData) {

			boolean optionalStoreFieldsSpecified = false;

			final String ipStart = row.get(0);
			final String ipEnd = row.get(1);
			final String banner = row.get(2);
			final String region = row.get(3);
			final String store = row.get(4);
			final String dept = row.get(5);
			String storeAddress = "";
			String customerStoreID = "";
			// Determining whether our optional fields are populated
			if (row.size() == 8) {
				if (row.get(6) != null && !row.get(6).equals("")) {
					storeAddress = row.get(6);
				}
				if (row.get(7) != null && !row.get(7).equals("")) {
					customerStoreID = row.get(7);
				}
				optionalStoreFieldsSpecified = true;
			}

			AutoAssign autoAssignment = new AutoAssign();
			autoAssignment.setIpAddressStart(ipStart);
			autoAssignment.setIpAddressEnd(ipEnd);
			autoAssignment.setBanner(banner);
			autoAssignment.setRegion(region);
			autoAssignment.setStore(store);
			autoAssignment.setDept(dept);
			autoAssignment.setEnabled(true);

			try {
				assign_repo.save(autoAssignment);
				log.debug("Added new assignment rule!");
			} catch (Exception ex) {
				log.debug("error while creating/updating assignment rule: {}", ex.getMessage());
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Error creating/updating assignment rule: {} " + ex.getMessage());
				return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			// Now that the assignment rule is saved to the DB, we'll see if the banner/region/store/dept is new
			// If so, we'll add them to their respective table(s)
			String bannerDomain = "", regionDomain = "", storeDomain = "", deptDomain = "";
			// Making distinct ID generators for each asset/object; we're in a for loop, so we can't reuse the same one
			String newBannerID = UUID.randomUUID().toString();
			String newRegionID = UUID.randomUUID().toString();
			String newStoreID = UUID.randomUUID().toString();
			String newDeptID = UUID.randomUUID().toString();

			if (!banner_repo.existsByBannerNameIgnoreCase(banner)) {
				Banner newBanner = new Banner();
				newBanner.setBannerId(newBannerID);
				newBanner.setBannerName(banner);

				bannerDomain = newBanner.getBannerId();
				try {
					domainHelper.saveDomain(newBanner.getBannerId(), EntityType.BANNER, AppConfig.DefaultAdminId);
					banner_repo.save(newBanner);
					result.setResult(RequestOperationResult.SUCCESS.name());
					log.debug("adding new banner: {}", newBanner.getBannerId());
				} catch (Exception ex) {
					log.debug("error while adding banner: {}", ex.getMessage());
					result.setErrorDescription("Error while adding banner: " + ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}
			} else {
				log.debug("Existing banner found for {}", banner);
			}

			if (!region_repo.existsByRegionNameIgnoreCase(region)) {
				Region newRegion = new Region();
				newRegion.setRegionId(newRegionID);
				newRegion.setRegionName(region);

				newRegion.setBannerId(bannerDomain);
				if (newRegion.getBannerId() != null && newRegion.getBannerId() != "") {
					log.debug("Banner ID validated");
				} else {
					newRegion.setBannerId(banner_repo.findBannerDomainFromBannerName(banner));
				}

				log.debug("New region banner ID: " + newRegion.getBannerId());

				regionDomain = newRegion.getRegionId();
				try {
					domainHelper.saveDomain(newRegion.getRegionId(), EntityType.REGION, newRegion.getBannerId());
					region_repo.save(newRegion);
					log.debug("adding new region: {}", newRegion.getRegionId());
				} catch (Exception ex) {
					log.debug("error while creating/updating region: {}", ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("Error creating/updating region: " + ex.getMessage());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}
			} else {
				log.debug("Existing region found for {}", region);
			}
			if (!store_repo.existsByStoreNameIgnoreCase(store)) {
				Store newStore = new Store();
				newStore.setStoreId(newStoreID);
				newStore.setAddress(storeAddress);
				newStore.setCustomerStoreNumber(customerStoreID);
				newStore.setStoreName(store);
				newStore.setRegionId(regionDomain);

				if (newStore.getRegionId() != null && newStore.getRegionId() != "") {
					newStore.setRegionId(regionDomain);
					log.debug("Region ID validated: " + newStore.getRegionId());
				} else {
					newStore.setRegionId(region_repo.findRegionDomainFromRegionName(region));
					log.debug("Region ID was null or empty, created new one: " + newStore.getRegionId());
				}

				storeDomain = newStore.getStoreId();
				try {
					domainHelper.saveDomain(newStore.getStoreId(), EntityType.STORE, newStore.getRegionId());
					if (newStore.getEnabled() == null) {
						newStore.setEnabled(true);
					}
					store_repo.save(newStore);
					log.debug("adding new store: {}", newStore.getStoreId());
				} catch (Exception ex) {
					result.setErrorDescription("Error Saving Store: " + ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}
			} else {
				log.debug("Existing store found for {}", store);
				Store foundStore = store_repo.findByStoreNameIgnoreCase(store);
				storeDomain = foundStore.getStoreId();

				// Handling an edge case where a store was previously instantiated -- either manually or via rules --
				// but was not given an address and ID when it maybe should have received one
				// (e.g., the user only specified the optional store info on one row in the file)
				if (optionalStoreFieldsSpecified &&
						(foundStore.getAddress().equals("") || foundStore.getCustomerStoreNumber().equals("") )) {
					foundStore.setAddress(storeAddress);
					foundStore.setCustomerStoreNumber(customerStoreID);
					store_repo.save(foundStore);
				}
			}
			if (!dept_repo.existsByDeptName1IgnoreCase(dept)) {
				Department newDepartment = new Department();
				newDepartment.setdeptId(newDeptID);
				newDepartment.setRequiresDomainId(AppConfig.DefaultAdminId);
				newDepartment.setDeptName1(dept);

				// See whether a profile name that matches the department name exists in the DB
				// If so, we'll give this incoming department that profile ID
				if (profile_repo.findProfileIDByProfileName(dept) == null) {
					newDepartment.setDefaultProfileId(AppConfig.DefaultProfileId);
					log.debug("Configured dept {} with default profile", newDepartment.getDeptName1());
				} else {
					newDepartment.setDefaultProfileId(profile_repo.findProfileIDByProfileName(dept));
					log.debug("Configured dept {} with profile {}", newDepartment.getDeptName1(), profile_repo.findProfileIDByProfileName(dept));
				}

				deptDomain = newDepartment.getdeptId();
				try {
					domainHelper.saveDomain(newDepartment.getdeptId(), EntityType.DEPARTMENT, newDepartment.getRequiresDomainId());
					dept_repo.save(newDepartment);
					storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, newDepartment.getdeptId()));
					log.debug("adding new department: {}", newDepartment.getdeptId());
				} catch (Exception ex) {
					result.setErrorDescription("Error saving department: " + ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.CONFLICT);
				}
			} else {
				log.debug("Existing department found for {}", dept);
				Department foundDepartment = dept_repo.findByDeptName1IgnoreCase(dept);
				deptDomain = foundDepartment.getdeptId();
				storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, deptDomain));
			}
		}
		try {
			NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
					dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
			nodeHelper.instantiateNodeStatus();
		}catch (Exception e){
			result.setErrorDescription("Could not set node statuses " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Parameters({
			@Parameter(name = "id", description = "The concatenation of IP start, -, and IP End of the rule",
					in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Deletes existing rule from auto assigner",
			description = "Deletes rule from database from formatted ip start and end")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The rule was successfully deleted"),
			@ApiResponse(responseCode = "404", description = "The rule you were trying to reach is not found"),
			@ApiResponse(responseCode = "409", description = "Unable to delete rule from database") })
	@DeleteMapping(path = "/deleteAutoAssignerRule", produces = {MediaType.APPLICATION_JSON_VALUE})
	public  ResponseEntity<OperationStatusModel> deleteAutoAssignerRule (@RequestBody AutoAssign rule){
		log.debug("Attempting deletion of rule");
		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());
		/*
		// id format: ipStart-ipEnd -> e.g. -> 10.3.128.1-10.3.128.166
		String IPStart = id.split("-")[0];
		String IPEnd= id.split("-")[1];
		*/
		String IPStart = rule.getIpAddressStart();
		String IPEnd = rule.getIpAddressEnd();

		if (!assign_repo.existsByIpAddressStart(IPStart) || !assign_repo.existsByIpAddressEnd(IPEnd) ){
			result.setErrorDescription("Rule not found with IPs - Start:" + IPStart + "End:" + IPEnd);
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		try {
			log.debug("deleting Rule with IPs - Start:" + IPStart + " End:" + IPEnd);
			/* Keeping the code below as a memento of how this used to be done but shouldn't be done anymore.
			   Rules with overlapping addresses are okay *so long as* only one of those rules are enabled.
			   Before July 2024, we fervently prevented duplicate/overlapping addresses from being saved.

			   Customer needs have changed, as has this code. Exercise extreme caution with the commented method below;
			   where possible, use the uncommented one right below it.
			*/
			//assign_repo.deleteByIpAddressStartAndIpAddressEnd(IPStart, IPEnd);
			assign_repo.delete(rule);
		} catch (Exception ex) {
			result.setErrorDescription(ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Adds/Edits a new rule to the auto assigner",
			description = "Accepts a new rule created by the user and adds/replaces the rule to the auto assigner repo")
	@PostMapping(path = "/postAutoAssignerRule", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> addEditAutoAssignRule (@RequestParam(required = false) String id, @RequestBody AutoAssign AutoAssignRule) {


		OperationStatusModel result = new OperationStatusModel("postAutoAssignerRule");
		log.debug("Operating on rule from frontend: " + AutoAssignRule);

		final String[] ipStart = AutoAssignRule.getIpAddressStart().split("\\.");
		final String[] ipEnd = AutoAssignRule.getIpAddressEnd().split("\\.");
		final String banner = AutoAssignRule.getBanner();
		final String region = AutoAssignRule.getRegion();
		final String store = AutoAssignRule.getStore();
		final String dept = AutoAssignRule.getDept();
		final Boolean disableFlag = AutoAssignRule.getDisableFlag();
		final Boolean deleteFlag = AutoAssignRule.getDeleteFlag();
		AutoAssign matchingRule = assign_repo.findRuleByAssetData(banner, region, store, dept);

		Iterable<AutoAssign> autoAssignRules = assign_repo.findAll();
		List<AutoAssign> rulesToDelete = new ArrayList<>(); // To handle potential duplicates later on
		// Validate there are no overlapping IPs
		for (AutoAssign rule : autoAssignRules) {
			String[] currentIPStart = rule.getIpAddressStart().split("\\.");
			String[] currentIPEnd = rule.getIpAddressEnd().split("\\.");

				/* Checking for IPv4 address overlap in currently enabled rules.
				   Unlike the assignment CSV import, we can potentially course-correct if we encounter a rule conflict.
				   The logic is straightforward:
				   		- If the user specifies that conflicting rules be disabled, disable them
				   		- If the user specifies conflicting rules be deleted, delete them
				   		- If the user doesn't specify an action -- but they need to -- stop and alert them accordingly
				 */
			if (ipRangeConflicts(currentIPStart, ipStart, currentIPEnd, ipEnd) && rule.getEnabled()) {
				log.debug("Handling conflicting rule: {}", rule);
				if (disableFlag) {
					log.debug("Disabling the following conflicting rule: {}", rule);
					assign_repo.disableRule(rule.getIpAddressStart(), rule.getIpAddressEnd(),
							rule.getBanner(), rule.getRegion(), rule.getStore(), rule.getDept());
				} else if (deleteFlag) {
					rulesToDelete.add(rule); // Delete this later to avert concurrent modification issues
				} else if (matchingRule != null && rule.equals(matchingRule)) {
						/* Last-ditch effort before we give up; if the rule seems to match an existing rule by
							banner, region, store, and department -- which implies an edit of an existing rule --
							then we'll mark the existing rule for deletion and let this new rule take its place
						 */
					log.debug("Edit of existing rule detected: {}", rule);
					rulesToDelete.add(rule);
				} else {
					// No other recourse; we have to tell the user to handle this
					log.debug("error: Overlapping IP addresses not allowed: {} overlaps with {} for rule {} - {}",
							Arrays.toString(ipStart), Arrays.toString(ipEnd),
							rule.getIpAddressStart(), rule.getIpAddressEnd());

					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("This rule conflicts with an enabled rule using range "
							+ rule.getIpAddressStart() + " - " + rule.getIpAddressEnd() + ". " +
							"That rule must be disabled or deleted before this one can be saved.");
					return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} else if (ipRangeConflicts(currentIPStart, ipStart, currentIPEnd, ipEnd) && !rule.getEnabled()) {
				if (matchingRule != null && AutoAssignRule.getEnabled()) {
					// The two checks above are just to be absolutely sure that it's safe to remove this rule
					rulesToDelete.add(matchingRule);
				}
			}
		}

			for (AutoAssign ruleToBeExecuted : rulesToDelete) {
				log.debug("Deleting the following conflicting rule: {}", ruleToBeExecuted);
				assign_repo.delete(ruleToBeExecuted);
			}

			try {
				assign_repo.save(AutoAssignRule);
				log.debug("Added new assignment rule!");
			} catch (Exception ex) {
				log.debug("error while creating/updating assignment rule: {}", ex.getMessage());
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Error creating/updating assignment rule: {} " + ex.getMessage());
				return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			// Now that the assignment rule is saved to the DB, we'll see if the banner/region/store/dept is new
			// If so, we'll add them to their respective table(s)
			String bannerDomain = "", regionDomain = "", storeDomain = "", deptDomain = "";
			// Making distinct ID generators for each asset/object; we're in a for loop, so we can't reuse the same one
			String newBannerID = UUID.randomUUID().toString();
			String newRegionID = UUID.randomUUID().toString();
			String newStoreID = UUID.randomUUID().toString();
			String newDeptID = UUID.randomUUID().toString();

			if (!banner_repo.existsByBannerNameIgnoreCase(banner)) {
				Banner newBanner = new Banner();
				newBanner.setBannerId(newBannerID);
				newBanner.setBannerName(banner);

				bannerDomain = newBanner.getBannerId();
				try {
					domainHelper.saveDomain(newBanner.getBannerId(), EntityType.BANNER, AppConfig.DefaultAdminId);
					banner_repo.save(newBanner);
					result.setResult(RequestOperationResult.SUCCESS.name());
					log.debug("adding new banner: {}", newBanner.getBannerId());
				} catch (Exception ex) {
					log.debug("error while adding banner: {}", ex.getMessage());
					result.setErrorDescription("Error while adding banner: " + ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}
			} else {
				log.debug("Existing banner found for {}", banner);
			}

			if (!region_repo.existsByRegionNameIgnoreCase(region)) {
				Region newRegion = new Region();
				newRegion.setRegionId(newRegionID);
				newRegion.setRegionName(region);

				newRegion.setBannerId(bannerDomain);
				if (newRegion.getBannerId() != null && newRegion.getBannerId() != "") {
					log.debug("Banner ID validated");
				} else {
					newRegion.setBannerId(banner_repo.findBannerDomainFromBannerName(banner));
				}

				log.debug("New region banner ID: " + newRegion.getBannerId());

				regionDomain = newRegion.getRegionId();
				try {
					domainHelper.saveDomain(newRegion.getRegionId(), EntityType.REGION, newRegion.getBannerId());
					region_repo.save(newRegion);
					log.debug("adding new region: {}", newRegion.getRegionId());
				} catch (Exception ex) {
					log.debug("error while creating/updating region: {}", ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("Error creating/updating region: " + ex.getMessage());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}
			} else {
				log.debug("Existing region found for {}", region);
			}
			Store existingStore = store_repo.findByStoreNameIgnoreCase(store);
			if (existingStore == null) {
				Store newStore = new Store();
				newStore.setStoreId(newStoreID);
				newStore.setAddress("");
				newStore.setCustomerStoreNumber("");
				newStore.setStoreName(store);

				newStore.setRegionId(regionDomain);
				if (newStore.getRegionId() != null && newStore.getRegionId() != "") {
					newStore.setRegionId(regionDomain);
					log.debug("Region ID validated: " + newStore.getRegionId());
				} else {
					newStore.setRegionId(region_repo.findRegionDomainFromRegionName(region));
					log.debug("Region ID was null or empty, created new one: " + newStore.getRegionId());
				}

				storeDomain = newStore.getStoreId();
				try {
					domainHelper.saveDomain(newStore.getStoreId(), EntityType.STORE, newStore.getRegionId());
					if (newStore.getEnabled() == null) {
						newStore.setEnabled(true);
					}
					store_repo.save(newStore);
					log.debug("adding new store: {}", newStore.getStoreId());
				} catch (Exception ex) {
					result.setErrorDescription("Error Saving Store: " + ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}
			} else {
				log.debug("Existing store found for {}", store);
				storeDomain = existingStore.getStoreId();
			}
			Department existingDept = dept_repo.findByDeptName1IgnoreCase(dept);
			if (existingDept == null) {
				Department newDepartment = new Department();
				newDepartment.setdeptId(newDeptID);
				newDepartment.setRequiresDomainId(AppConfig.DefaultAdminId);
				newDepartment.setDeptName1(dept);

				// See whether a profile name that matches the department name exists in the DB
				// If so, we'll give this incoming department that profile ID
				if (profile_repo.findProfileIDByProfileName(dept) == null) {
					newDepartment.setDefaultProfileId(AppConfig.DefaultProfileId);
					log.debug("Configured dept {} with default profile", newDepartment.getDeptName1());
				} else {
					newDepartment.setDefaultProfileId(profile_repo.findProfileIDByProfileName(dept));
					log.debug("Configured dept {} with profile {}", newDepartment.getDeptName1(), profile_repo.findProfileIDByProfileName(dept));
				}
				if(!storeDeptPairsRepository.existsByStoreIdAndDeptId(storeDomain, newDepartment.getdeptId())){
					storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, newDepartment.getdeptId()));
				}
				deptDomain = newDepartment.getdeptId();
				try {
					domainHelper.saveDomain(newDepartment.getdeptId(), EntityType.DEPARTMENT, newDepartment.getRequiresDomainId());
					dept_repo.save(newDepartment);
					log.debug("adding new department: {}", newDepartment.getdeptId());
				} catch (Exception ex) {
					result.setErrorDescription("Error saving department: " + ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.CONFLICT);
				}
			} else {
				log.debug("Existing department found for {}", dept);

				if (!storeDeptPairsRepository.existsByStoreIdAndDeptId(storeDomain, existingDept.getdeptId())) {
					storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, existingDept.getdeptId()));
				}
			}
		try {
			NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
					dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
			nodeHelper.instantiateNodeStatus();
		}catch (Exception e){
			result.setErrorDescription("Could not set node statuses " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Import existing setup from a CSV file", description = "Parses and formats data from a CSV file into the database")
	@PostMapping(path = "/importCSVToTree", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<OperationStatusModel> importCSVToTree (@RequestParam("file") MultipartFile file) throws IOException {
		OperationStatusModel result = new OperationStatusModel("ImportCSVToTree");

		BufferedReader br;
		List<List<String>> csvData = new ArrayList<>(); // Used for rebuilding the CSV data from the frontend submission
		try {
			String line;
			InputStream is = file.getInputStream(); // Read bytes from what the frontend sent
			br = new BufferedReader(new InputStreamReader(is));
			br.readLine(); // Skip the first line (header), otherwise chaos will ensue
			while ((line = br.readLine()) != null) {
				List<String> record = new ArrayList<>(Arrays.asList(line.split(","))); // Split each row into its own array
				csvData.add(record);
			}
		} catch (IOException e) {
			log.error("Potential data corruption in CSV file: {}", e.getMessage());
			result.setErrorDescription("Potential data corruption in CSV file: " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		int uploadToTreeValidationIteration = 2; // Starting at the zeroth index will just confuse customers
		for(List<String> array : csvData) { // Validating that the user filled in all the requisite fields
			final String ipAddressFromTreeUpload = array.get(5);
			try {
				if (array.size() < 6 || array.contains("")) { // All six columns need to be filled out for each row
					log.debug("CSV file is missing data. Aborting parsing operation.");
					result.setErrorDescription("CSV file is missing fields. Please validate data and resubmit.");
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}
				if (!HttpUtils.isValidInet4Address(ipAddressFromTreeUpload)) {
					log.debug("CSV file has an invalid IP address. Aborting parsing operation.");
					result.setErrorDescription("Invalid IPv4 address in row " + uploadToTreeValidationIteration +
							". Please validate data and resubmit.");
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}

			} catch (Exception e) {
				log.debug("Error occurred during CSV data validation!");
				result.setErrorDescription("Error occurred during data validation: " +  e.getMessage());
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			uploadToTreeValidationIteration++;
		}

		for (int i : new int[]{3, 2, 1, 0}) { // Sort our array by department, store, region, and banner, respectively
			// We'll do this in case the user sends data that isn't already neatly structured... which is likely
			csvData.sort(Comparator.comparing(strings -> strings.get(i)));
		}
		List<ScaleNode> payload = HteTools.readCSVForAssetList(csvData);
		return importUsingScaleNode(payload, result);
	}
	@Operation(summary = "Import existing setup from HTe Business", description = "Consumes a parsed enterprise.xml file from HTe Business")
	@PostMapping(path = "/importTreeView", consumes = { MediaType.APPLICATION_JSON_VALUE },
			produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> importTree(@RequestBody List<ScaleNode> tree,
															@RequestParam(name = "replaceAssetList", required = false) Boolean deleteTreeStructure) {
		OperationStatusModel result = new OperationStatusModel("ImportTree");
		String bannerDomain = "", regionDomain = "", storeDomain = "", deptDomain = "";

		Iterable<ScaleSettings> scaleSettings = new ArrayList<ScaleSettings>();
		Iterable<ScaleDevice> devices = new ArrayList<ScaleDevice>();
		Iterable<Department> departments = new ArrayList<Department>();
		Iterable<Store> stores = new ArrayList<Store>();
		Iterable<Region> regions = new ArrayList<Region>();
		Iterable<Banner> banners = new ArrayList<Banner>();
		Iterable<StoreDeptPair> storeDeptpairs = new ArrayList<StoreDeptPair>();

		if(deleteTreeStructure){
			Iterable<UpgradeDevice> upgradeDevices = upgrade_device_repo.findAll();
			log.debug("upgradeDevices size:"+ upgradeDevices.spliterator().getExactSizeIfKnown());
			if(upgradeDevices.spliterator().getExactSizeIfKnown() > 0){
				String message = "Upgrade devices still exist!";
				log.debug("error while adding banner: {}", message);
				result.setErrorDescription("Error while deleting Tree structure banner: " +  message);
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}
			//saves current Tree structure in case error is thrown
			try{
				scaleSettings = ssRepo.findAll();
				ssRepo.deleteAll();
				devices = device_repo.findAll();
				device_repo.deleteAll();
				departments =  dept_repo.findAll();
				dept_repo.deleteAll();
				stores =  store_repo.findAll();
				store_repo.deleteAll();
				regions = region_repo.findAll();
				region_repo.deleteAll();
				banners =  banner_repo.findAll();
				banner_repo.deleteAll();
				storeDeptpairs  = storeDeptPairsRepository.findAll();
				storeDeptPairsRepository.deleteAll();
			}
			catch (Exception ex) {
				if(scaleSettings.spliterator().getExactSizeIfKnown() > 0){
					ssRepo.saveAll(scaleSettings);
				}
				if(devices.spliterator().getExactSizeIfKnown() > 0){
					device_repo.saveAll(devices);
				}
				if(departments.spliterator().getExactSizeIfKnown() > 0){
					dept_repo.saveAll(departments);
				}
				if(regions.spliterator().getExactSizeIfKnown() > 0){
					region_repo.saveAll(regions);
				}
				if(banners.spliterator().getExactSizeIfKnown() > 0){
					banner_repo.saveAll(banners);
				}
				if(storeDeptpairs.spliterator().getExactSizeIfKnown() > 0){
					storeDeptPairsRepository.saveAll(storeDeptpairs);
				}
				log.debug("error while adding banner: {}", ex.getMessage());
				result.setErrorDescription("Error while deleting Tree structure banner: " +  ex.getMessage());
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}
		}
		return importUsingScaleNode(tree, result);
	}

	@Operation(summary = "Get all scales for treeview object", description = "Returns a representation of the scales in a nested arrangement to display on a treeview")
	@GetMapping(path = "/scaletreeview", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<ScaleNode> getAllScalesTreeview() {
//		log.info("------------------------------------------------------");
//		log.info("getAllScalesTreeview(): generating tree view structure");

		ArrayList<ScaleNode> root = new ArrayList<>();
		// read departments once
		allDepts = dept_repo.findAll();


		root.addAll(getBanners());
		return root;
	}

	// -----------
	// Banners
	// -----------

	@Operation(summary = "Get one or all banners.", description = "If an Id is provided it will return the data for that corresponding banner, otherwise it will return a list of all the available banners")
	@Parameters({
			@Parameter(name = "id", description = "Identificator of the banner. (Optional)", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/banner", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getBanner(@RequestParam(name = "id", required = false) String id) {
		if (id == null) {
			Iterable<Banner> findAll = banner_repo.findAll();
			return new ResponseEntity<>(findAll, HttpStatus.OK);
		}

		Banner one_banner = banner_repo.findById(id).get();
		return new ResponseEntity<>(one_banner, HttpStatus.OK);
	}

	@Operation(summary = "Create/Update a banner in the database.", description = "Creates/Updates the given banner in the database.")
	@PostMapping(path = "/banner", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> updateBanner(@RequestBody Banner banner, @RequestHeader(value = "testingValidation", required = false) String testingValidation) {

		OperationStatusModel result = new OperationStatusModel("UpdateBanner");

		if (banner.getBannerName() == null || banner_repo.existsByBannerNameIgnoreCase(banner.getBannerName())) {
			result.setErrorDescription("Banner name already exists");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		if (HteTools.isNullorEmpty(banner.getBannerId())) {
			// A new Banner is being created
			String newID = UUID.randomUUID().toString();
			banner.setBannerId(newID);
		} else {
			// User is attempting to edit an existing Banner
			if (!banner_repo.findById(banner.getBannerId()).isPresent() && !HteTools.isTestRequest(testingValidation)) {
				// If the existing banner does not exist
				result.setErrorDescription("Existing banner not found");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
			}
		}

		if (!DomainHelper.isValidUUID(banner.getBannerId()) || DomainHelper.isDefaultUUID(banner.getBannerId())) {
			result.setErrorDescription("The Banner number field must be a valid UUID");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		Banner savedBanner;
		try {
			domainHelper.saveDomain(banner.getBannerId(), EntityType.BANNER, AppConfig.DefaultAdminId);
			savedBanner = banner_repo.save(banner);
			result.setResult(RequestOperationResult.SUCCESS.name());
			log.debug("adding new banner: {}", savedBanner.getBannerId());
		} catch (Exception ex) {
			log.debug("error while adding banner: {}", ex.getMessage());
			result.setErrorDescription("Error while adding banner: " + ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode scaleNode = new ScaleNode(savedBanner);
			nodeHelper.updateNodeStatus(scaleNode);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "id", description = "The Id of the banner", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Deletes the banner.", description = "Deletes the banner in the database based on the given Id .")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The banner was successfully deleted"),
			@ApiResponse(responseCode = "404", description = "The banner you were trying to reach is not found"),
			@ApiResponse(responseCode = "409", description = "Unable to delete from the database") })
	@DeleteMapping(path = "/banner", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> deleteBanner(@RequestParam String id) {

		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

		if (!banner_repo.existsById(id)) {
			result.setErrorDescription("Banner not found with Id: " + id);
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		if (domainHelper.hasChild(id)) {
			result.setErrorDescription("Banner still has child entities pointing it.");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		try {
			log.debug("deleting banner: {}", id);
			banner_repo.deleteById(id);
			domainHelper.safeDelete(id);
		} catch (Exception ex) {
			result.setErrorDescription(ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}
		try {
			nodeStatusRepo.deleteById(id);
		}
		catch (Exception ex) {
			log.info("Could not delete banner node");
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// -----------
	// Regions
	// -----------

	@Operation(summary = "Get one or all regions.", description = "If an Id is provided it will return the data for that corresponding region, otherwise it will return a list of all the available regions for the given banner.")
	@Parameters({
			@Parameter(name = "id", description = "Identificator of the region (Optional).", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
			@Parameter(name = "bannerId", description = "The id of the banner the this region is associated with.", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/region", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getRegion(@RequestParam(name = "id", required = false) String id,
									   @RequestParam(name = "bannerId", required = false) String bannerId) {
		if (id == null) {
			if (bannerId != null) {
				Iterable<Region> findAll = region_repo.findByBannerId(bannerId);
				return new ResponseEntity<>(findAll, HttpStatus.OK);
			} else {
				Iterable<Region> findAll = region_repo.findAll();
				return new ResponseEntity<>(findAll, HttpStatus.OK);
			}
		}

		Region one_region = region_repo.findById(id).get();
		return new ResponseEntity<>(one_region, HttpStatus.OK);
	}

	@Operation(summary = "Create/Update a region in the database.", description = "Creates/Updates the given region in the database.")
	@PostMapping(path = "/region", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> updateRegion(@RequestBody Region region, @RequestHeader(value = "testingValidation", required = false) String testingValidation) {
		OperationStatusModel result = new OperationStatusModel("UpdateRegion");

		if (HteTools.isNullorEmpty(region.getRegionId())) {
			// A new Banner is being created
			String newID = UUID.randomUUID().toString();
			region.setRegionId(newID);
		} else {
			// User is attempting to edit an existing Region
			if (!region_repo.findById(region.getRegionId()).isPresent() && !HteTools.isTestRequest(testingValidation)) {
				// If the existing banner does not exist
				result.setErrorDescription("Existing region not found");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
			}
		}

		// Make sure that region belongs to an existing Banner
		if (!banner_repo.findById(region.getBannerId()).isPresent()) {
			// If the banner does not exist
			result.setErrorDescription("Region's Banner not found");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		// Double check that the region number type is valid
		if (!DomainHelper.isValidUUID(region.getRegionId()) || DomainHelper.isDefaultUUID(region.getRegionId())) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("The Region number field must be a valid UUID");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		Region savedRegion;
		try {
			domainHelper.saveDomain(region.getRegionId(), EntityType.REGION, region.getBannerId());
			savedRegion = region_repo.save(region);

			log.debug("adding new region: {}", savedRegion.getRegionId());
		} catch (Exception ex) {
			log.debug("error while creating/updating region: {}", ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Error creating/updating region: " + ex.getMessage());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode scaleNode = new ScaleNode(savedRegion);
			nodeHelper.updateNodeStatus(scaleNode);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "id", description = "The Id of the region", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Deletes the region.", description = "Deletes the region in the database based on the given Id.")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The region was successfully deleted"),
			@ApiResponse(responseCode = "404", description = "The region you were trying to reach is not found"),
			@ApiResponse(responseCode = "409", description = "Unable to delete from the database") })
	@DeleteMapping(path = "/region", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> deleteRegion(@RequestParam String id) {

		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

		if (!region_repo.existsById(id)) {
			result.setErrorDescription("Region not found with Id: " + id);
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		if (domainHelper.hasChild(id)) {
			result.setErrorDescription("Region still has child entities pointing it.");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		try {
			Region region = region_repo.findById(id).get();
			Banner b = banner_repo.findById(region.getBannerId()).get();
			log.debug("deleting region: {}", id);
			region_repo.deleteById(id);
			domainHelper.safeDelete(id);
			try{
				nodeStatusRepo.deleteById(id);
				NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
						dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
				ScaleNode node = new ScaleNode(b);
				nodeHelper.updateNodeStatus(node);

			}catch (Exception e){
				log.debug("Could not delete region ndoe status");
			}
		} catch (Exception ex) {
			log.debug("error while deleting region: {}", ex.getMessage());
			result.setErrorDescription(ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// -----------
	// Stores
	// -----------

	@Operation(summary = "Get one or all stores.", description = "If an Id is provided it will return the data for that corresponding store, otherwise it will return a list of all the available stores for the given region.")
	@Parameters({
			@Parameter(name = "id", description = "Identificator of the store (Optional).", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
			@Parameter(name = "regionId", description = "The id of the region the given store is associated with. (Optional)", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/store", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getStore(@RequestParam(name = "id", required = false) String id,
									  @RequestParam(name = "regionId", required = false) String regionId) {
		if (id == null) {
			if (regionId != null) {
				Iterable<Store> findAll = store_repo.findByRegionId(regionId);
				return new ResponseEntity<>(findAll, HttpStatus.OK);
			} else {
				Iterable<Store> findAll = store_repo.findAll();
				//Return all stores as a map
				HashMap<String, Store> scaleMap = new HashMap<>();
				for(Store store : findAll){
					if(!DomainHelper.isDefaultUUID(store.getStoreId()))
						scaleMap.put(store.getStoreId(), store);
				}
				return new ResponseEntity<>(scaleMap, HttpStatus.OK);
			}
		}

		Store one_store = store_repo.findById(id).orElse(null);
		return new ResponseEntity<>(one_store, HttpStatus.OK);
	}

	@Operation(summary = "Create/Update a store in the database.", description = "Creates/Updates the given store in the database.")
	@PostMapping(path = "/store", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> updateStore(@RequestBody Store store, @RequestHeader(value = "testingValidation", required = false) String testingValidation) {
		OperationStatusModel result = new OperationStatusModel("UpdateStore");
		Store newStore = store;

		if(newStore.getIpAddress() != null){
			if(!HttpUtils.isValidInet4Address(newStore.getIpAddress())){
				result.setErrorDescription("Not a valid IP Address");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
			}
		}

		if (HteTools.isNullorEmpty(newStore.getStoreId())) {
			// A new Store is being created
			String newID = UUID.randomUUID().toString();
			newStore.setStoreId(newID);

			if (newStore.getStoreName() == null || store_repo.existsByStoreNameIgnoreCase(newStore.getStoreName())){
				result.setErrorDescription("Store name already exists");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
			}

		} else {
			// User is attempting to edit an existing Store
			Store existingStore = store_repo.findById(store.getStoreId()).orElse(null);
			if (existingStore == null && !HteTools.isTestRequest(testingValidation)) {
				// If the existing banner does not exist
				result.setErrorDescription("Existing store not found");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
			}

			if(existingStore != null) {
				//Merge in existing store info for any entries that are null in the current store
				newStore = HteTools.mergeObjects(store, existingStore);

				//Special Case for sendStoreInfo, incoming store takes precendence even if null
				newStore.setSendStoreInfo(store.getSendStoreInfo());
			}


		}

		// Make sure that store belongs to an existing region
		if (!region_repo.findById(newStore.getRegionId()).isPresent()) {
			// If the parent region does not exist
			result.setErrorDescription("Store's Region not found");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		// Double Check that store ID is valid
		if (!DomainHelper.isValidUUID(newStore.getStoreId()) || DomainHelper.isDefaultUUID(newStore.getStoreId())) {
			result.setErrorDescription("Invalid Assigned Store number");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		try {
			domainHelper.saveDomain(newStore.getStoreId(), EntityType.STORE, newStore.getRegionId());
			if(newStore.getEnabled() == null){newStore.setEnabled(true);}
			// Store savedStore =
			store_repo.save(newStore);
		} catch (Exception ex) {
			result.setErrorDescription("Error Saving Store: " + ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode scaleNode = new ScaleNode(newStore);
			nodeHelper.updateNodeStatus(scaleNode);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "id", description = "The Id of the store to delete", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Deletes a store.", description = "Deletes the store specified in the parameter from the database.")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The store was successfully deleted"),
			@ApiResponse(responseCode = "404", description = "The store you were trying to reach is not found"),
			@ApiResponse(responseCode = "409", description = "Unable to delete from the database") })
	@DeleteMapping(path = "/store", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> deleteStore(@RequestParam String id) {

		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

		if (!store_repo.existsById(id)) {
			result.setErrorDescription("Store not found with Id: " + id);
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		if (domainHelper.hasChild(id)) {
			result.setErrorDescription("Store still has child entities pointing it.");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		Store store = store_repo.findById(id).get(); // Cant be null bc we checked above for existence
		try {
			store_repo.deleteById(id);
			domainHelper.safeDelete(id);
		} catch (Exception ex) {
			result.setErrorDescription(ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}
		try {
			nodeStatusRepo.deleteById(id);
			storeDeptPairsRepository.deleteByStoreId(id);
			//update parent node
			Region region = region_repo.findById(store.getRegionId()).orElse(null);
			NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
					dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
			try{
				ScaleNode node = new ScaleNode(region);
				nodeHelper.updateNodeStatus(node);
			}catch (Exception e){
				throw new RuntimeException("Unable to update node status - " + e.getMessage());
			}
		}catch (Exception e){
			log.info("Unable to delete store node status");
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// -----------
	// Department
	// -----------

	@Operation(summary = "Get one or all departments.", description = "If an Id is provided it will return the data for that corresponding department, otherwise it will return a list of all the available departments.")
	@Parameters({
			@Parameter(name = "id", description = "Identificator of the department (Optional).", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/dept", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getDepartment(@RequestParam(name = "id", required = false) String id) {
		if (id == null) {
			Iterable<Department> findAll = dept_repo.findAll();

			HashMap<String, Department> deptMap = new HashMap<>();
			for(Department department : findAll){
				if(!DomainHelper.isDefaultUUID(department.getdeptId()))
					deptMap.put(department.getdeptId(), department);
			}

			return new ResponseEntity<>(deptMap, HttpStatus.OK);
		}

		Department one_dept = dept_repo.findById(id).get();
		return new ResponseEntity<>(one_dept, HttpStatus.OK);
	}

	@Parameters({
			@Parameter(name = "deptId", description = "The ID of the department to be added", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
			@Parameter(name = "storeId", description = "The ID of the department's store", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class))

	})
	@Operation(summary = "Allows the user to assign departments to the tree", description = "Allows the user to assign departments to the tree")
	@PostMapping(path = "/dept/remove", produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> removeDepartment(@RequestParam String deptId, @RequestParam String storeId) {
		OperationStatusModel result = new OperationStatusModel("Remove Department");

		Department existingDept = dept_repo.findById(deptId).orElse(null);
		if (existingDept == null){
			//If the existing department does not exist
			result.setErrorDescription("Existing department not found");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		Store existingStore = store_repo.findById(storeId).orElse(null);
		if (existingStore == null){
			//If the existing department does not exist
			result.setErrorDescription("Existing store not found");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}


		try {
			log.info("Removing scales from: " + existingDept.getDeptName1() + " in Store: " + existingStore.getStoreName());
			List<String> scaleIds= device_repo.findDeviceIdByStoreIdAndDeptId(deptId, storeId);
			try{
				nodeStatusRepo.deleteByDeviceId(scaleIds);
			} catch(Exception e){
				log.info("Unable to delete scale node status - "+ e.getMessage());
			}
			int updated = device_repo.removeScalesOfOneDepartment(DefaultUnassignedId, DefaultUnassignedId, deptId, storeId);
			log.info("records affected: {}", updated);

			storeDeptPairsRepository.deleteByStoreIdAndDeptId(storeId, deptId);
			try{
				//Delete any invisible scales
				device_repo.deleteByHostnameAndDeptIdAndStoreId(ScaleDevice.getInvisibleScaleName(), DefaultUnassignedId, DefaultUnassignedId);
			}catch (Exception e){
				log.debug("Couldn't delete invisible scales:" + e.getMessage());
			}
		} catch (Exception ex) {
			log.debug("error while removing the department: {}", ex.getMessage());
			result.setErrorDescription(ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}
		//update parent node
		log.debug("Updating nod`estatus");
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			nodeStatusRepo.deleteById(storeId+"|"+deptId);
			ScaleNode node = new ScaleNode(existingStore);
			nodeHelper.updateNodeStatus(node);
		} catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "deptId", description = "The ID of the department to be added", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
			@Parameter(name = "storeId", description = "The ID of the department's store", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class))
	})
	@Operation(summary = "Allows the user to assign departments to the tree", description = "Allows the user to assign departments to the tree")
	@PostMapping(path = "/dept/assign", produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> assignDepartment(@RequestParam String deptId, @RequestParam String storeId) {

		OperationStatusModel result = new OperationStatusModel("AssignDepartment");

		Department existingDept = dept_repo.findById(deptId).orElse(null);
		if (existingDept == null){
			//If the existing department does not exist
			result.setErrorDescription("Existing department not found");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		Store existingStore = store_repo.findById(storeId).orElse(null);
		if (existingStore == null){
			//If the existing store does not exist
			result.setErrorDescription("Existing store not found");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		//If both the store and the department exist we need to make sure that this department can be assigned to this store
		//In this case The department being assigned will have the higher permission and the store it's being assigned to needs to move up
		//The domain tree to see if it has access
		//UserID: Departments current assigned domain
		//EntityID: the Store's Domain ID
		if(DefaultUnassignedId.equals(existingDept.getRequiresDomainId())){
			result.setErrorDescription("Department " + existingDept.getDeptName1() + " is unassigned " +
					"please give this department a domain first. ");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
		}

		if(!domainHelper.hasDomainPermission(existingDept.getRequiresDomainId(), storeId)){
			result.setErrorDescription("Department " + existingDept.getDeptName1() + " cannot be assigned to this store, " +
					"if this is a mistake please change the department's required domain");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
		}

		List<ScaleDevice> tmp = device_repo.findByStoreIdAndDeptId(storeId, deptId);
		if(!device_repo.findByStoreIdAndDeptId(storeId, deptId).isEmpty()){
			result.setErrorDescription("Department " + existingDept.getDeptName1() + " already exists in this store.");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
		}

		//When you assign a scale to a new department, it also inherits its new departments
		//If the scale is being unassigned then don't update the profile
		Department newAssignedDepartment = dept_repo.findById(deptId).orElse(null);
		String newProfileId = AppConfig.DefaultProfileId;
		if(newAssignedDepartment != null && newAssignedDepartment.getDefaultProfileId() != null){
			newProfileId = newAssignedDepartment.getDefaultProfileId();
		}
		try {
			storeDeptPairsRepository.save(new StoreDeptPair(storeId,deptId));
		}catch(Exception e){
			log.debug("Error when creating store dept key pair:" + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Error when creating store dept key pair: " + e.getMessage());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		//update node
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode node = new ScaleNode(newAssignedDepartment);
			node.setId(storeId+"|"+newAssignedDepartment.getdeptId());
			nodeHelper.updateNodeStatus(node);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}


		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Operation(summary = "Create/Update a department in the database.", description = "Creates/Updates the given department in the database.")
	@PostMapping(path = "/dept", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> updateDepartment(@RequestBody Department dept, @RequestHeader(value = "testingValidation", required = false) String testingValidation) {

		OperationStatusModel result = new OperationStatusModel("UpdateDepartment");


		if(dept.getDeptName1() == null || dept.getDeptName1().equals("")){
			result.setErrorDescription("Department name cannot be empty");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}

		if (HteTools.isNullorEmpty(dept.getdeptId())) {
			//A new department is being created
			String newID = UUID.randomUUID().toString();
			dept.setdeptId(newID);

			if(dept_repo.existsByDeptName1IgnoreCase(dept.getDeptName1())){
				result.setErrorDescription("Department name already exists");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.CONFLICT);
			}
		}
		else{

			//User is attempting to edit an existing department
			if (!dept_repo.findById(dept.getdeptId()).isPresent() && !HteTools.isTestRequest(testingValidation)){
				//If the existing department does not exist
				result.setErrorDescription("Existing department not found");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
			}
			else{
				Department existingDept = dept_repo.findByDeptName1IgnoreCase(dept.getDeptName1());
				//If it does exist check that there is not a duplicate name with another existing department
				if(existingDept != null && !existingDept.getdeptId().equals(dept.getdeptId())){
					result.setErrorDescription("There is already a department with that name!");
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.CONFLICT);
				}
			}
		}


		//Make sure that new department has a valid RequiredDomainID
		if(dept.getRequiresDomainId() == null || !domain_repo.existsById(dept.getRequiresDomainId())){
			//If it doesn't have a valid domainId assign it to the Unassigned domain
			dept.setRequiresDomainId(DefaultUnassignedId);
		}

		//Make sure that new department has a valid ProfileId
		if(dept.getDefaultProfileId() == null || !profile_repo.existsById(dept.getDefaultProfileId())){
			//If it doesn't have a valid domainId assign it to the Unassigned domain
			dept.setDefaultProfileId(AppConfig.DefaultProfileId);
		}

		//Double check that department has a valid ID

		if (!DomainHelper.isValidUUID(dept.getdeptId()) || DomainHelper.isDefaultUUID(dept.getdeptId())) {
			result.setErrorDescription("Invalid assigned department number");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		Department savedDept;
		try {
			domainHelper.saveDomain(dept.getdeptId(), EntityType.DEPARTMENT, dept.getRequiresDomainId());
			savedDept = dept_repo.save(dept);
		} catch (Exception ex) {
			result.setErrorDescription("Error saving department: " + ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		result.setErrorDescription(savedDept.getdeptId());
		return new ResponseEntity<>(savedDept, HttpStatus.CREATED);
	}

	@Transactional
	@Operation(summary = "Assigns a department profile", description = "Assigns a department profile")
	@PostMapping(path = "/dept/assignProfile", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> assignDeptProfile(@RequestParam String deptId,
																  @RequestParam String profileId,
																  @RequestBody List<String> deptNodePath) {
		OperationStatusModel result = new OperationStatusModel("AssignDepartmentProfile");
		Department existingDept = dept_repo.findById(deptId).orElse(null);
		Profile existingProfile = profile_repo.findById(profileId).orElse(null);
		if (existingDept == null) {
			result.setErrorDescription("Could not find requested department");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		if (existingProfile == null) {
			result.setErrorDescription("Could not find requested department");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		// Department savedDept;
		try {
			/*
			 * Find all scales that are assigned to this department and if they share the
			 * same original profile ID then set their profile ID as well. If their
			 * profileID is different assume scale profile has been manually set and leave
			 * alone
			 */
			List<ScaleDevice> departmentScales = device_repo.findByDeptId(deptId);
			log.info("AssignDeptProfiles : Updating child scales");
			for (ScaleDevice scale : departmentScales) {
				if (scale.getProfileId() == null || scale.getProfileId().equals(existingDept.getDefaultProfileId())) {
					scale.setProfileId(profileId);
					device_repo.save(scale);
				}

				ScaleSettings scaleSettings = ssRepo.findById(scale.getDeviceId()).orElse(null);
				if(scaleSettings != null){
					try{
						scaleSettings.addCurrentFilesItem("newProfile");
						ssRepo.save(scaleSettings);
					}
					catch (Exception e){
						log.error("Could not update scale settings for device : " + scale.getIpAddress());
						log.error("Update error is : " + e.getMessage());
					}
				}

			}
			log.info("AssignDeptProfiles : updating department profile");
			existingDept.setDefaultProfileId(existingProfile.getProfileId());
			dept_repo.save(existingDept);
		} catch (Exception ex) {
			result.setErrorDescription("Error assigning new dept profile: " + ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode node = new ScaleNode(existingDept);
			node.setPath(deptNodePath);
			nodeHelper.updateNodeStatus(node);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "id", description = "The Id of the department to delete", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Deletes the department.", description = "Deletes the department in the database based on the given Id .")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The department was successfully deleted"),
			@ApiResponse(responseCode = "400", description = "The provided description in the parameter is invalid"),
			@ApiResponse(responseCode = "404", description = "The department you were trying to reach is not found"),
			@ApiResponse(responseCode = "409", description = "Unable to delete from the database") })
	@DeleteMapping(path = "/dept", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> deleteDepartment(@RequestParam String id) {

		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

		if (!DomainHelper.isValidUUID(id)) {
			result.setErrorDescription("Department Number is not a valid ID");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		if (!dept_repo.existsById(id)) {
			result.setErrorDescription("Department not found with ID: " + id);
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}
		try {

			int updated = device_repo.removeScalesOfDeletedDepartment(DefaultUnassignedId, DefaultUnassignedId, id);
			log.info("records affected: {}", updated);

			log.debug("deleting related store-department pairs for: {}", id);
			// Do away with vestigial store-dept pairs or else Asset List rendering will be screwed!
			storeDeptPairsRepository.deletePairsByDeptId(id);

			log.debug("deleting Department: {}", id);
			dept_repo.deleteById(id);
			domainHelper.safeDelete(id);
		} catch (Exception ex) {
			log.debug("error while deleting the department: {}", ex.getMessage());
			result.setErrorDescription(ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.CONFLICT);
		}
		// TODO - edit stores node status
		// TODO - get store id to update node
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// -----------
	// Scales
	// -----------

	@Operation(summary = "Create/Update a scale device in the system", description = "Creates the given device in the database, if scale already exists updates the database.")
	@PostMapping(path = "/device", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> updateDevice(@RequestBody ScaleDevice scaleDev, @RequestHeader(value = "testingValidation", required = false) String testingValidation) {
		OperationStatusModel result = new OperationStatusModel("UpdateDevice");

		if (HteTools.isNullorEmpty(scaleDev.getIpAddress())) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("The IP address field cannot be null or empty.");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		if(scaleDev.isInvisibleScale()){
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Cannot make changes to this scale");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		if (scaleDev.getSerialNumber() < 0) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("The Serial number field cannot be zero or negative.");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		if (!(scaleDev.getStoreId().equals(DefaultUnassignedId)
				&& scaleDev.getDeptId().equals(DefaultUnassignedId))) {
			// If the store number and dept number both equal unassigned then you can skip
			// the valid checks
			// Since the user is allowed to assign and unassign scales
			if (!DomainHelper.isValidUUID(scaleDev.getStoreId())
					|| DomainHelper.isDefaultUUID(scaleDev.getStoreId())) {

				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("The Store number field is invalid ");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			if (!DomainHelper.isValidUUID(scaleDev.getDeptId())
					|| DomainHelper.isDefaultUUID(scaleDev.getDeptId())) {

				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("The Department number field is invalid");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}
		}

		String tmp;
		ScaleDevice existingScale = device_repo.findByIpAddress(scaleDev.getIpAddress());
		if(!HteTools.isTestRequest(testingValidation)) {
			if (existingScale == null) {
				// generate a UUID for the new scale
				tmp = UUID.randomUUID().toString();
				log.debug("new UUID assigned for new scale: {}", tmp);
				scaleDev.setDeviceId(tmp);
			} else if (existingScale.getStoreId().equals(DefaultUnassignedId)
					&& existingScale.getDeptId().equals(DefaultUnassignedId)) {
				existingScale.setStoreId(scaleDev.getStoreId());
				existingScale.setDeptId(scaleDev.getDeptId());
				scaleDev = existingScale;
			} else {
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("That device is already assigned to a different store");
				return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
			}
		}

		if(!licenseHelper.canAssignScale(device_repo.findAll())){
			//If the scale limit has been reached, set scale as unassigned
			result.setErrorDescription("You have reached the limit for assigned " +
					"scales for your current license");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
		}

		try {
			log.debug("creating/updating scale device: {}", scaleDev.getIpAddress());
			device_repo.save(scaleDev);
		} catch (Exception ex) {
			log.debug("error when creating/updating scale on db: {}", ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Error creating/updating scale: " + ex.getMessage());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode node = new ScaleNode(scaleDev);
			nodeHelper.updateNodeStatus(node);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "scaleId", description = "The UUID of the desired scale", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
			@Parameter(name = "profileId", description = "The UUID of the desired profile", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Assigns a scale profile", description = "Assigns a scale profile")
	@PostMapping(path = "/device/assignProfile", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> assignScaleProfile(@RequestParam String scaleId,
																   @RequestParam String profileId) {
		OperationStatusModel result = new OperationStatusModel("AssignDeviceProfile");
		ScaleDevice existingScale = device_repo.findById(scaleId).orElse(null);
		Profile existingProfile;
		log.debug("Assigning Profile");
		if (existingScale == null) {
			result.setErrorDescription("Could not find requested scale");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		if(existingScale.isInvisibleScale()){
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Cannot make changes to this scale");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		if(profileId.equals("AUTO")){
			//Frontend would like to assign the scale to whatever it's department is
			Department parentDept = dept_repo.findById(existingScale.getDeptId()).orElse(null);
			if(parentDept == null) {
				result.setErrorDescription("Could not find requested department profile");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}
			existingProfile = profile_repo.findById(parentDept.getDefaultProfileId()).orElse(null);

		} else {
			existingProfile = profile_repo.findById(profileId).orElse(null);
		}

		if (existingProfile == null) {
			result.setErrorDescription("Could not find requested department");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		try {
			ScaleSettings scaleSettings = ssRepo.findById(existingScale.getDeviceId()).orElse(null);
			if(scaleSettings != null){
				try{
					scaleSettings.addCurrentFilesItem("newProfile");
					ssRepo.save(scaleSettings);
				}
				catch (Exception e){
					log.error("AssignScaleProfile, Could not update scale settings for device : " + existingScale.getIpAddress());
					log.error("Update error is : " + e.getMessage());
				}
			}

			existingScale.setProfileId(existingProfile.getProfileId());
			device_repo.save(existingScale);
		} catch (Exception ex) {
			result.setErrorDescription("Error assigning new scale profile: " + ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode scaleNode = new ScaleNode(existingScale);
			nodeHelper.updateNodeStatus(scaleNode);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "upgradeId", description = "The UUID of the desired upgrade device", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
			@Parameter(name = "isPrimary", description = "Boolean description indicating if scale is a primary scale", in = ParameterIn.QUERY, schema = @Schema(implementation = Boolean.class)) })
	@Operation(summary = "Designates a scale as primary", description = "Designates a scale as primary")
	@PostMapping(path = "/device/setPrimary", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> assignPrimaryScale(@RequestParam String upgradeId,
																   @RequestParam Boolean isPrimary) {
		OperationStatusModel result = new OperationStatusModel("AssignPrimaryScale");
		UpgradeDevice upgradeDevice = upgrade_device_repo.findById(upgradeId).get();
		ScaleDevice existingScale = device_repo.findById(upgradeDevice.getDeviceId()).orElse(null);
		if (existingScale == null) {
			result.setErrorDescription("Could not find requested scale");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		if(existingScale.isInvisibleScale()){
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Cannot make changes to this scale");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		try {
			existingScale.setIsPrimaryScale(isPrimary);
			device_repo.save(existingScale);
		} catch (Exception ex) {
			result.setErrorDescription("Error assigning new primary scale: " + ex.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode node = new ScaleNode(existingScale);
			nodeHelper.updateNodeStatus(node);
		}catch (Exception e){
			throw new RuntimeException("Unable to update node status - " + e.getMessage());
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	@Parameters({
			@Parameter(name = "lstScale", description = "String of scale IDs that you wish to reassign", in = ParameterIn.DEFAULT, schema = @Schema(implementation = String.class)),
			@Parameter(name = "deptId", description = "The ID of the scales new department", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
			@Parameter(name = "storeId", description = "The ID of the scales new store", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class))
	})
	@Operation(summary = "Allows the user to assign and unassign scales, to and from their own Domain", description = "Allows the user to assign and unassign scales, to and from their own Domain")
	@PostMapping(path = "/device/assignScale", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> assignScale(@RequestBody List<String> lstScale,
															@RequestParam String deptId, @RequestParam String storeId) {

		OperationStatusModel result = new OperationStatusModel("AssignScaleDomain");
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		// String failedAssignments = "";

		// When you assign a scale to a new department, it also inherits it's new
		// department's
		// If the scale is being unassigned then don't update the profile
		Department newAssignedDepartment = dept_repo.findById(deptId).orElse(null);
		String newProfileId = AppConfig.DefaultProfileId;
		if (newAssignedDepartment != null && newAssignedDepartment.getDefaultProfileId() != null) {
			newProfileId = newAssignedDepartment.getDefaultProfileId();
		}

		for (String scaleId : lstScale) {

			ScaleDevice existingScale = device_repo.findById(scaleId).orElse(null);
			String oldDeptId = existingScale.getDeptId();
			String oldStoreId = existingScale.getStoreId();
			if (existingScale == null) {
				result.setErrorDescription("Could not find requested scale: " + scaleId);
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			//Verify that the scale is either completely unassigned (Both deptId and
			// storeId are default) or
			//completely assigned (Both new store and dept exist), domain filter will
			// decide if they can assign it there
			if ((DefaultUnassignedId.equals(deptId)
					&& DefaultUnassignedId.equals(storeId))
					|| (dept_repo.existsById(deptId) && store_repo.existsById(storeId))) {
				try {
					if(DefaultUnassignedId.equals(deptId)){
						//Scale is being unassigned set profile back to default
						existingScale.setProfileId(AppConfig.DefaultProfileId);
					}
					else{
						//If the scale is being assigned then check that license allows to have that many scales assigned
						if(!licenseHelper.canAssignScale(device_repo.findAll())){
							result.setErrorDescription("You have reached the limit for assigned " +
									"scales for your current license");
							result.setResult(RequestOperationResult.ERROR.name());
							return new ResponseEntity<>(result, HttpStatus.FORBIDDEN);
						}

						//If scale isn't being unassigned then set it's profile to it's new department
						existingScale.setProfileId(newProfileId);
					}

					existingScale.setDeptId(deptId);
					existingScale.setStoreId(storeId);
					device_repo.save(existingScale);
					try{
						// if scale is unassigned update the parent dept node status
						if(DefaultUnassignedId.equals(existingScale.getDeptId())){
							Department d = dept_repo.findById(oldDeptId).orElse(null);
							if(d == null){
								throw new NullPointerException("Unable to find department to update");
							}
							try {
								nodeStatusRepo.deleteById(existingScale.getDeviceId());
							} catch (Exception e){
								log.info("Unable to delete scale node status - "+ e.getMessage());
							}
							ScaleNode deptNode = new ScaleNode(d);
							deptNode.setId(oldStoreId + "|" + oldDeptId);
							nodeHelper.updateNodeStatus(deptNode);
						}else{
							ScaleNode scaleNode = new ScaleNode(existingScale);
							nodeHelper.updateNodeStatus(scaleNode);
						}
					}catch (Exception e){
						throw new IllegalArgumentException("Unable to update scale status - " + e.getMessage());
					}
				} catch (Exception ex) {
					result.setErrorDescription(
							"Error assigning scale: " + existingScale.getHostname() + " - " + ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name());
					return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
				}

			} else {
				result.setErrorDescription("Invalid scale assignment");
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.CREATED);

	}

	/**
	 * { "deviceId": null, "ipAddress": "10.3.128.167", "hostname": null,
	 * "serialNumber": 0, "scaleModel": null, "storeNumber": 0, "deptId": 0,
	 * "countryCode": null, "buildNumber": null, "application": null,
	 * "operatingSystem": null, "systemController": null, "loader": null,
	 * "smBackend": null, "lastReportTimestampUtc": "2021-10-18T14:23:30Z",
	 * "lastTriggerType": "INITIAL", "profileId": null }, { "deviceId": null,
	 * "ipAddress": "10.3.128.164", "hostname": "ht-ss-ws1", "serialNumber":
	 * 555634444, "scaleModel": "HTi-SS", "storeNumber": 0, "deptId": 0,
	 * "countryCode": null, "buildNumber": null, "application": null,
	 * "operatingSystem": null, "systemController": null, "loader": null,
	 * "smBackend": null, "lastReportTimestampUtc": "2021-10-18T14:23:30Z",
	 * "lastTriggerType": "INITIAL", "profileId": null }
	 *
	 * @param lstScale
	 * @return
	 */

	@Operation(summary = "Creat/Update a list of scale devices in the system", description = "Creates the given list of devices in the database, if scales already exists updates the database.")
	@PostMapping(path = "/devices", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> updateDevices(@RequestBody List<ScaleDevice> lstScale, @RequestHeader(value = "testingValidation", required = false) String testingValidation) {

		log.info("adding multiple devices");
		ArrayList<OperationStatusModel> results = new ArrayList<>();
		ArrayList<ScaleDevice> devResults = new ArrayList<>();

		for (ScaleDevice scaleDev : lstScale) {
			log.info("Adding : " + scaleDev.getIpAddress());
			OperationStatusModel result = new OperationStatusModel(RequestOperationType.CREATE.name());
			result.setResult(RequestOperationResult.SUCCESS.name());

			if (HteTools.isNullorEmpty(scaleDev.getIpAddress())) {
				result.setErrorDescription("The IP address field cannot be null or empty.");
				result.setResult(RequestOperationResult.ERROR.name());
			}

			if (scaleDev.getSerialNumber() < 0) {
				result.setErrorDescription("The Serial number field cannot be zero or negative.");
				result.setResult(RequestOperationResult.ERROR.name());
			}

			if (!DomainHelper.isValidUUID(scaleDev.getStoreId())) {
				result.setErrorDescription("The Store number field cannot be zero or negative.");
				result.setResult(RequestOperationResult.ERROR.name());
			}

			if (!DomainHelper.isValidUUID(scaleDev.getStoreId())) {
				result.setErrorDescription("The Department number field cannot be zero or negative.");
				result.setResult(RequestOperationResult.ERROR.name());
			}

			if (result.getResult().equals(RequestOperationResult.ERROR.name())) {
				results.add(result);
				log.info("Scale Dev " + scaleDev.getIpAddress() + " faile " + result.getErrorDescription());
			} else {
				String tmp;
				if (!device_repo.existsByIpAddress(scaleDev.getIpAddress()) && scaleDev.getDeviceId() == null) {
					// generate a UUID for the new scale
					tmp = UUID.randomUUID().toString();
					log.info("new UUID assigned for new scale: {}", tmp);
					scaleDev.setDeviceId(tmp);
				}

				try {
					log.info("creating/updating scale device: {}", scaleDev.getIpAddress());
//					if (scaleDev.getProfileId() == null || scaleDev.getProfileId().isEmpty()) {
//						scaleDev.setProfileId("00000000-0000-0000-0000-000000000000");
//					}
					ScaleDevice newDev = device_repo.save(scaleDev);
					devResults.add(newDev);
					result.setResult(RequestOperationResult.SUCCESS.name() + ", " + scaleDev.getIpAddress());
				} catch (Exception ex) {
					log.info("error when creating/updating scale on db: {}", ex.getMessage());
					result.setErrorDescription(ex.getMessage());
					result.setResult(RequestOperationResult.ERROR.name() + ", " + scaleDev.getIpAddress());
					// return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
				}
				NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
						dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
				try{
					ScaleNode scaleNode = new ScaleNode(scaleDev);
					nodeHelper.updateNodeStatus(scaleNode);
				}catch (Exception e){
					throw new IllegalArgumentException("Unable to update scale status - " + e.getMessage());
				}
				results.add(result);
			}
		}

		return new ResponseEntity<>(devResults, HttpStatus.ACCEPTED);
	}

	@Parameters({
			@Parameter(name = "id", description = "IP address or UUID of the scale to delete", in = ParameterIn.QUERY, example = "10.3.128.65 | 10c2f499-5f95-40a6-b03a-2cd1c68f38d7", schema = @Schema(implementation = String.class)),
			@Parameter(name = "idtype", description = "The type of id to use, it could be a UUID or an IP address", in = ParameterIn.QUERY, example = "IPADDRESS | UUID", schema = @Schema(implementation = IdType.class)) })
	@Operation(summary = "Deletes a scale device in the database.", description = "Deletes the scale device in the database based on the given Id type.")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The scale device was successfully deleted"),
			@ApiResponse(responseCode = "404", description = "The scale you were trying to reach is not found"),
			@ApiResponse(responseCode = "409", description = "Unable to delete from the database") })
	@DeleteMapping(path = "/device", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> deleteDevice(@RequestParam String id, @RequestParam IdType idtype) {

		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		ScaleDevice tmp = null;

		if (idtype.equals(IdType.IPADDRESS)) {
			tmp = device_repo.findByIpAddress(id);
		} else if (idtype.equals(IdType.UUID)) {
			tmp = device_repo.findById(id).get();
		} else {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("The given IdType is incorrect, check parameters");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		HttpStatus status = HttpStatus.OK;
		if (tmp == null) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Record not found");
			status = HttpStatus.NOT_FOUND;
		} else if (upgrade_device_repo.findByDeviceId(tmp.getDeviceId()) != null) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Device is assigned to an upgrade group");
			status = HttpStatus.CONFLICT;
		} else {
			String oldDeptId = tmp.getDeptId();
			String oldStoreId = tmp.getStoreId();
			try {
				log.debug("deleting device: {}", tmp.getDeviceId());
				device_repo.deleteById(tmp.getDeviceId());
				result.setResult(RequestOperationResult.SUCCESS.name());
			} catch (Exception ex) {
				result.setErrorDescription(ex.toString());
				result.setResult(RequestOperationResult.ERROR.name());
				status = HttpStatus.CONFLICT;
			}
			try{
				nodeStatusRepo.deleteById(tmp.getDeviceId());
				Department d = dept_repo.findById(oldDeptId).orElse(null);
				if(d == null){
					throw new NullPointerException("Unable to find department to update");
				}
				ScaleNode deptNode = new ScaleNode(d);
				deptNode.setId(oldStoreId + "|" + oldDeptId);
				nodeHelper.updateNodeStatus(deptNode);
			}catch (Exception e){
				throw new IllegalArgumentException("Unable to update scale status - " + e.getMessage());
			}
		}


		return new ResponseEntity<>(result, status);
	}

	@Parameters({
			@Parameter(name = "id", description = "IP address or UUID of the scale to delete", in = ParameterIn.QUERY, example = "10.3.128.65,10.3.125.95", schema = @Schema(implementation = String.class)),
			@Parameter(name = "idtype", description = "The type of id to use, it could be a UUID or an IP address", in = ParameterIn.QUERY, example = "IPADDRESS | UUID", schema = @Schema(implementation = IdType.class)) })
	@Operation(summary = "Deletes a scale device in the database.", description = "Deletes the scale device in the database based on the given Id type.")
	@DeleteMapping(path = "/devices", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> deleteDevices(@RequestParam List<String> ids, @RequestParam IdType idtype) {

		ArrayList<OperationStatusModel> results = new ArrayList<>();

		for (String id : ids) {
			OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

			ScaleDevice tmp = null;
			if (idtype.equals(IdType.IPADDRESS)) {
				tmp = device_repo.findByIpAddress(id);
			} else if (idtype.equals(IdType.UUID)) {
				tmp = device_repo.findById(id).get();
			} else {
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("The given IdType is incorrect, provide proper parameters");
			}

			if (tmp != null) {
				log.debug("deleting device: {}", tmp.getDeviceId());
				device_repo.deleteById(tmp.getDeviceId());
				NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
						dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
				try{
					nodeStatusRepo.deleteById(tmp.getDeviceId());
					Department dept = dept_repo.findById(tmp.getDeptId()).orElse(null);
					if(dept == null){
						throw new NullPointerException("Unable to update node status - Department not found");
					}
					ScaleNode node = new ScaleNode(dept);
					nodeHelper.updateNodeStatus(node);
				}catch (Exception e){
					throw new RuntimeException("Unable to update node status - " + e.getMessage());
				}
				result.setResult(RequestOperationResult.SUCCESS.name() + ", " + tmp.getDeviceId());
			}
			results.add(result);
		}

		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	@Operation(summary = "Enable/Disable a given store server", description = "Changes the status of the given store to enable or disable, depending on the value provided.")
	@Parameters({
			@Parameter(name = "id", description = "id of the store asset to enable/disable", in = ParameterIn.QUERY, example = "4dc3a601-b41d-4969-80b6-4cec4c524be1", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/store/enable", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> enableStore(@RequestParam(name = "id") String storeId,
															@RequestParam(name = "enable") boolean enable) {
		OperationStatusModel result = new OperationStatusModel("EnableDisableStore");
		Store store = store_repo.findById(storeId).orElse(null);

		if (store == null) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Store " + storeId + "not found");
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		try{
			store.setEnabled(enable);
			store_repo.save(store);
		}
		catch (Exception e){
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Could not Enable/Disable Store!");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Enable/Disable a given scale", description = "Changes the status of the given scale to enable or disable, depending on the value provided.")
	@Parameters({
			@Parameter(name = "ip", description = "IP address of the scale to enable/disable", in = ParameterIn.QUERY, example = "10.3.128.65", schema = @Schema(implementation = String.class)) })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The scale status was successfully changed"),
			@ApiResponse(responseCode = "404", description = "The given scale IP address is not found in the database") })
	@GetMapping(path = "/enablescale", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> enableScale(@RequestParam(name = "ip") String scaleIp,
															@RequestParam(name = "enable") boolean enable) {
		OperationStatusModel result = new OperationStatusModel("EnableDisableScale");
		ScaleDevice scale = device_repo.findByIpAddress(scaleIp);

		if (scale == null) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Scale " + scaleIp + "not found");
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		scale.setEnabled(enable);
		ScaleDevice newScale = device_repo.save(scale);
		NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
		try{
			ScaleNode scaleNode = new ScaleNode(scale);
			nodeHelper.updateNodeStatus(scaleNode);
		}catch (Exception e){
			throw new IllegalArgumentException("Unable to update scale status - " + e.getMessage());
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	// ------------
	// Filters
	// ------------
	@Parameters({})
	@Operation(summary = "Fetch all filters", description = "Call fetchAll() to package and return a hashmap of " +
			"the filter table")
	@GetMapping(path = "/filter", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> filter(){
		FilterAPI filterAPI = new FilterAPI(entityManager, filterRepo, device_repo);
		HashMap<String, FilterAPI.FilterReturn> acc = new HashMap<>();

		try {
			acc = filterAPI.fetchAll();
		} catch(Exception ex){
			OperationStatusModel result = new OperationStatusModel("UpdateFilter");
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Error creating filter: " + ex.getMessage());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(acc, HttpStatus.CREATED);
	}
	@Operation(summary = "Save a filter", description = "Save the List<Filter> newFilter to the database if it has " +
			"corresponding scales.")
	@PostMapping(path = "/filter", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> updateFilter(@RequestBody List<Filter> newFilter) {
		OperationStatusModel result = new OperationStatusModel("SaveFilter");
		FilterAPI filterAPI = new FilterAPI( entityManager,filterRepo, device_repo);
		try {
			result = filterAPI.save(newFilter);
		} catch(Exception ex){
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Error creating filter: " + ex.getMessage());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}
 	@Operation(summary = "Delete a single or all filters", description = "Deletes all filters if the string DELETEALL " +
			"is provided or delete filter parts by filterId")
	@DeleteMapping(path = "/filter", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> deleteFilter(@RequestParam(name = "filterId") String filterId){
		OperationStatusModel result = new OperationStatusModel("DeleteFilter");
		FilterAPI filterAPI = new FilterAPI( entityManager,filterRepo, device_repo);
		log.info("Delete Filter");
		if(filterId.equals("DELETEALL")){
			result = filterAPI.deleteAll();
		}
		else {
			result = filterAPI.deleteByFilterId(filterId);
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		result.setErrorDescription(filterId);
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}
	@Operation(summary="Asset data by lazy load", description="Based on provided node path, loads in the children of that node, or if nothing provided, loads banners")
	@Parameters({
			@Parameter(name="nodePath", description="The IDs of the node and its parents", in=ParameterIn.QUERY, example="['bannerUUID','regionUUID','storeUUID']")
	})
	@GetMapping(path="/fetchFilterPart")
	public ResponseEntity<?> fetchFilterPart(@RequestParam(required=true, name="filterId") String  filterId,
											 @RequestParam(required=false, name="nodePath") List<String> nodePath) {
		LazyLoadFilterAssetTree tree = new LazyLoadFilterAssetTree(nodePath, filterId, domain_repo, banner_repo,
				region_repo, store_repo, dept_repo, device_repo, ssRepo, file_repo, upgrade_device_repo, nodeStatusRepo,
				storeDeptPairsRepository, configapp, entityManager, filterRepo);
		Map<String, ScaleNode> treeNodes;
		try {
				treeNodes = tree.execute();
		} catch (Exception e) {
			log.error("Error retrieving tree data during fetchFilterPart- " + e.getMessage());
			OperationStatusModel result = new OperationStatusModel("FetchLazyTree");
			result.setErrorDescription("Error retrieving tree data - " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(treeNodes, HttpStatus.OK);
	}
	@Operation(summary="Asset data by lazy load", description="Based on provided node path, loads in the children of that node, or if nothing provided, loads banners")
	@Parameters({
			@Parameter(name="nodePath", description="The IDs of the node and its parents", in=ParameterIn.QUERY, example="['bannerUUID','regionUUID','storeUUID']")
	})
	@GetMapping(path="/refreshFilterTree")
	public ResponseEntity<?> refreshFilter(@RequestParam(required=true, name="expandedKeys") List<String> expandedKeys) {
		LazyLoadFilterAssetTree tree = new LazyLoadFilterAssetTree(new ArrayList<>(), "", domain_repo, banner_repo,
				region_repo, store_repo, dept_repo, device_repo, ssRepo, file_repo, upgrade_device_repo, nodeStatusRepo,
				storeDeptPairsRepository, configapp, entityManager, filterRepo);
		Map<String, ScaleNode> treeNodes;
		try {
			treeNodes = tree.refreshFilterTree(expandedKeys);
		} catch (Exception e) {
			log.error("Error retrieving tree data during refreshFilterTree - " + e.getMessage());
			OperationStatusModel result = new OperationStatusModel("FetchLazyTree");
			result.setErrorDescription("Error retrieving tree data - " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(treeNodes, HttpStatus.OK);
	}
	@Operation(summary="Filtered assets", description="Based on the provided filter, compiles the scales that meet it's critiera")
	@GetMapping(path="/filteredAssets")
	public ResponseEntity<?> getFilteredAssets() {
		OperationStatusModel result = new OperationStatusModel("Get Filtered Assets");
		log.info("Get Filtered Assets");

		FilterAPI filtering = new FilterAPI(entityManager, filterRepo, device_repo);
		HashMap<String, List<ScaleDevice>> filteredAssets = new HashMap<>();
		try {
			List<String> filterIds = filterRepo.findDistinctFilterIds();
			filterIds.forEach(filterId -> filteredAssets.put(filterId, filtering.getFilteredScales(filterId)));
		} catch (Exception e) {
			log.error("Error occurred while finding filtered assets: {}", e.getMessage());
			result.setErrorDescription("Error occurred while finding filtered assets: " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(filteredAssets, HttpStatus.OK);
	}
	// ------------
	// Profiles
	// ------------
	@Operation(summary = "Returns the information of the given profile from the database.", description = "Returns all the information related to the the given profile Id.")
	@GetMapping(path = "/profile", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getProfile(@RequestParam(name = "profileId", required = false) String id) {
		if (id == null) {
			Iterable<Profile> findAll = profile_repo.findAll();
			return new ResponseEntity<>(findAll, HttpStatus.OK);
		}

		Profile profile = profile_repo.findById(id).orElse(null);
		return new ResponseEntity<>(profile, HttpStatus.OK);
	}

	@Operation(summary = "Creates/updates a profile in the database.", description = "Creates/updates a profile with the received information in the database. When creating the profile ID will be generated, otherwise it is required to provide it.")
	@PostMapping(path = "/profile", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> updateProfile(@RequestBody Profile profile) {
		OperationStatusModel result = new OperationStatusModel("UpdateProfile");
		String tmp;
		if (profile.getProfileId() == null || !profile_repo.existsByProfileIdIgnoreCase(profile.getProfileId())) {
			// find by profile name
			if (profile_repo.existsByNameIgnoreCase(profile.getName())) {
				// retrieve id from database if found
				Profile tmp_profile = profile_repo.findByNameIgnoreCase(profile.getName());
				profile.setProfileId(tmp_profile.getProfileId());
			} else {
				// generate a UUID for the new scale
				tmp = UUID.randomUUID().toString();
				log.debug("new UUID assigned for new profile: {}", tmp);
				profile.setProfileId(tmp);
			}
		}

		if (profile.getRequiresDomainId() != null) {
			if (!domain_repo.existsById(profile.getRequiresDomainId())) {
				// If profile's requested domain does not exist throw an error
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Could not find requested domain");
				return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
			}
			// Else the DomainID is valid, send it on and save
		} else { // If the profile does not assign a domain set it to unassigned
			profile.setRequiresDomainId(DefaultUnassignedId);
		}

		// refresh the time stamp
		profile.setLastUpdate(Timestamp.from(Instant.now()));

		// Profile save;
		try {
			log.debug("creating/updating scale device: {}", profile.getProfileId());
			// save =
			profile_repo.save(profile);
			domainHelper.saveDomain(profile.getProfileId(), EntityType.PROFILE, profile.getRequiresDomainId());
		} catch (Exception ex) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Error creating/updating a profile: " + ex.getMessage());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		String profileId = profile.getProfileId();
		device_repo.notifyScalesOfUpdate(profileId); // This value will be reset by

		result.setResult(RequestOperationResult.SUCCESS.name());
		result.setErrorDescription(profile.getProfileId());
		return new ResponseEntity<>(result, HttpStatus.CREATED);
	}

	//TODO: Move to HeartbeatController file in the future
	@Parameter(name = "ipAddress", description = "IP address of the reporting scale",
			in = ParameterIn.QUERY, example = "10.3.128.93", schema = @Schema(implementation = String.class))
	@PostMapping(path="/disableProfileUpdateStatus")
	public ResponseEntity<?> disableProfileUpdateStatus(
			@RequestParam(name = "ipAddress", required = false) String ipAddress) {

		OperationStatusModel result = new OperationStatusModel();
		HttpStatus status;

		try {
			device_repo.disableScaleNotificationStatus(ipAddress);
			result.setResult(RequestOperationResult.SUCCESS.name());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (Exception ex) {
			log.debug("Scale {} failed to modify profile-update status", ipAddress);
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("An error occurred while trying to update the scale table.");
			status = HttpStatus.INTERNAL_SERVER_ERROR;

			return new ResponseEntity<>(result, status);
		}

		return new ResponseEntity<>(result, status);
	}

	//TODO: Move to HeartbeatController file in the future
	@Parameter(name = "ipAddress", description = "IP address of the reporting scale",
			in = ParameterIn.QUERY, example = "10.3.128.93", schema = @Schema(implementation = String.class))
	@GetMapping(path="/profileUpdatedStatus")
	public ResponseEntity<?> getProfileUpdateStatus(
			@RequestParam(name = "ipAddress", required = false) String ipAddress) {
		try {
			Boolean wasProfileUpdatedRecently = profile_repo.scaleProfileUpdatedStatus(ipAddress);
			return new ResponseEntity<>(wasProfileUpdatedRecently, HttpStatus.OK);
		} catch (Exception ex) {
			log.debug("An issue occurred while trying to check the profile status of scale {}", ipAddress);
			log.debug(ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Deletes a profile.", description = "Deletes a profile with the given profile Id. This function will delete all files associated to it and set to null all the devices referencing to this profile.")
	@Parameters({
			@Parameter(name = "id", description = "Id of the profile to delete", in = ParameterIn.QUERY, example = "c98a4699-8efd-abba-affa-4077de1e8977", schema = @Schema(implementation = String.class)) })
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "The profile was successfully deleted"),
			@ApiResponse(responseCode = "400", description = "The given parameters are invalid"),
			@ApiResponse(responseCode = "404", description = "The profile you were trying to reach is not found"),
			@ApiResponse(responseCode = "409", description = "Unable to delete from the database"),
			@ApiResponse(responseCode = "500", description = "An internal error occurred when attempting to update the database or local disk") })
	@DeleteMapping(path = "/profile", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> deleteProfile(@RequestParam String id) {

		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());
		log.info("delete profile");

		if (DomainHelper.isDefaultUUID(id)) {
			log.debug("cannot delete default profile");
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("cannot delete default profile");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		HttpStatus status = HttpStatus.OK;

		if (!profile_repo.existsById(id)) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Record not found");
			status = HttpStatus.NOT_FOUND;
		} else {

			// delete all files and DB entries associated to this profileId and the
			// directory in the repository
			try {
				Iterable<FileForEvent> allFiles = file_repo.findAllByProfileId(id);
				for (FileForEvent toDelete : allFiles) {
					File file = Paths.get(configapp.findById(AppConfig.repositoryPath)
									.orElse(new ConfigEntry("", "")).getCoValue(), toDelete.getProfileId(), toDelete.getFilename())
							.toFile();
					if (file.delete()) {
						log.info("file {} has been deleted from disk.", toDelete.getFilename());
					}
					file_repo.deleteById(toDelete.getFileId());
					log.info("file {} has been deleted from DB.", toDelete.getFilename());
				}
				File dir_to_delete = Paths.get(configapp.findById(AppConfig.repositoryPath)
						.orElse(new ConfigEntry("", "")).getCoValue(), id).toFile();

				log.info("deleting directory from disk, status: {}", dir_to_delete.delete());
			} catch (Exception ex) {
				log.info("an error ocurred while deleting a file associated to a profile: {}", ex.toString());
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription(ex.getMessage());
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}

			log.debug("deleting profile: {}", id);
			try {
				// assign scales to a null profile
				int updated = device_repo.updateScaleDeviceSetProfileIdForProfileId(AppConfig.DefaultProfileId, id);
				log.info("records affected: {}", updated);

				// assign depts to the default value
				int deptsUpdated = dept_repo.updateDepartmentSetProfileIdForProfileId(AppConfig.DefaultProfileId, id);
				log.info("dept records affected: {}", deptsUpdated);

				// delete profile and it's associated domain
				profile_repo.deleteById(id);
				domainHelper.safeDelete(id);
				result.setResult(RequestOperationResult.SUCCESS.name());
				status = HttpStatus.OK;
			} catch (Exception ex) {
				log.info("an error ocurred while deleting a profile: {}", ex.toString());
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription(ex.getMessage());
				status = HttpStatus.INTERNAL_SERVER_ERROR;
			}
		}

		return new ResponseEntity<>(result, status);
	}
	@Operation(summary = "Gets all apphook versions from", description = "Retrieves all Apphook versions from the Database with the corresponding DeviceId and IpAddress")
	@Parameters({})
	@GetMapping(path = "/appHook", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getAppHook() {

		List<DeviceIdAndIpAddressAndAppHookVersion> appHook = ssRepo.findDeviceIdAndIpAddressAndAppHookVersionByAppHookVersionIsNotNull();
		HashMap<String, DeviceIdAndIpAddressAndAppHookVersion>  AppHookMap= new HashMap<>();
		for(DeviceIdAndIpAddressAndAppHookVersion scale : appHook){
			AppHookMap.put(scale.getDeviceId(), scale);
		}
		return new ResponseEntity<>(AppHookMap, HttpStatus.OK);
	}
	@Parameters({
			@Parameter(name = "id", description = "IP address or UUID of the scale to delete", in = ParameterIn.QUERY, example = "10.3.128.65", schema = @Schema(implementation = String.class)),
			@Parameter(name = "idtype", description = "The type of id to use, it could be a UUID or an IP address", in = ParameterIn.QUERY, example = "IPADDRESS | UUID", schema = @Schema(implementation = IdType.class)) })
	@Operation(summary = "Retrieves the extra settings of the corresponding device.", description = "Returns the additional scale device settings in the database based on the given Id type.")
	@GetMapping(path = "/deviceSettings", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getDeviceSettings(@RequestParam String id, @RequestParam IdType idtype) {

		OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

		ScaleSettings scaleSettings = null;
		if (idtype.equals(IdType.IPADDRESS)) {
			scaleSettings = ssRepo.findByIpAddress(id);
		} else if (idtype.equals(IdType.UUID)) {
			scaleSettings = ssRepo.findById(id).get();
		} else {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("The given IdType is incorrect, check parameters");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		HttpStatus status = HttpStatus.OK;
		if (scaleSettings == null) {
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Record not found");
			status = HttpStatus.NOT_FOUND;
			return new ResponseEntity<>(result, status);
		}

		log.debug("retrieving data for device: {}", scaleSettings.getDeviceId());
		return new ResponseEntity<>(scaleSettings, status);
	}

	@Parameters({
			@Parameter(name = "id", description = "UUID of the asset you are trying to retrieve", in = ParameterIn.QUERY, example = "4dc3a601-b41d-4969-80b6-4cec4c524be1", schema = @Schema(implementation = String.class)),
			@Parameter(name = "nodeType", description = "the type of node you are trying access", in = ParameterIn.QUERY, example = "DEVICE | STORE | DEPT ", schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Retrieves nodeStatus for a particular asset", description = "")
	@GetMapping(path = "/nodeStatus", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getNodeStatus(@RequestParam String id, @RequestParam String nodeType) {

		OperationStatusModel result = new OperationStatusModel("GetNodeStatus-"+nodeType);
		String awayThreshold = configapp.findById(AppConfig.scaleAwayHours).get().getCoValue();
		String offlineThreshold = configapp.findById(AppConfig.scaleOfflineHours).get().getCoValue();

		NodeStatus status = new NodeStatus();

		switch (nodeType.toLowerCase()){
			case "store":
				Store store = store_repo.findById(id).orElse(null);
				List<ScaleDevice> devices = device_repo.findByStoreId(id);
				if(store == null){
					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("Could not find requested store!");
					return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
				}

				status = new NodeStatus(store, devices);
				break;
			case "device":
			case "scale":
				ScaleDevice device = device_repo.findById(id).orElse(null);
				if(device == null){
					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("Could not find requested device!");
					return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
				}

				status = new NodeStatus(device);
				status.setScaleReportStatus(device, awayThreshold, offlineThreshold);

				ScaleSettings ss = ssRepo.findById(device.getDeviceId()).orElse(null);
				status.setScaleSync(device, ss, retrieveFilesforProfile(device.getProfileId()));
				break;
		}

		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	@Parameters({
			@Parameter(name = "ids", description = "List of UUID of the scale to retrieve data from", in = ParameterIn.DEFAULT, example = "167ea0b5-6b05-11eb-998a-fa163e138193", schema = @Schema(implementation = List.class)),
			@Parameter(name = "range", description = "range of the query options are 'day', 'week' or 'month'", in = ParameterIn.QUERY, example = "day", schema = @Schema(implementation = String.class)),
			@Parameter(name = "type", description = "format of the data you want to output 'raw'(List of all sync entries) and 'graph'(counter like json object) ", in = ParameterIn.QUERY, example = "raw", schema = @Schema(implementation = String.class))})

	@Operation(summary = "Retrieves device sync status.", description = "Returns the additional scale device settings in the database based on the given Id type.")
	@PostMapping(path = "/getSyncStatus", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getSyncStatus(@RequestBody(required = false) List<String> ids, @RequestParam String range, @RequestParam String type) {
		OperationStatusModel result = new OperationStatusModel("getSyncStatus");

		Timestamp end = new Timestamp(System.currentTimeMillis());
		Timestamp start;
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		double del = 0;

		class GraphSync{

			public GraphSync(long start, long end, double del, int numScales, String range) {
				this.start = start;
				this.end = end;
				this.del = (long) del;
				this.range = range;
				this.numScales = numScales;
				numHeartbeats = new ArrayList<>();
				numInSync = new ArrayList<>();
			}

			public long start;
			public long end;
			public long del;
			public int numScales;
			public String uptime;
			public String range;

			public List<Integer> numHeartbeats;
			public List<Integer> numInSync;
		}

		//Create the timestamp range
		switch (range.toLowerCase()){
			case "day":
				cal.add(Calendar.HOUR, -24);
				start = new Timestamp(cal.getTimeInMillis());
				del = 3.6E+6; //One hour in milliseconds
				break;
			case "week":
				cal.add(Calendar.DATE, -7);
				start = new Timestamp(cal.getTimeInMillis());
				del = 2.88E+7; //8 Hour snippets
				break;
			case "month":
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.add(Calendar.DATE, -29);
				start = new Timestamp(cal.getTimeInMillis());
				del = 8.64E+7; //1 Day in millis
				break;
			default:
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Invalid range selection");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		int numScales;
		if(ids == null){
			numScales = device_repo.findByHostnameNot(ScaleDevice.getInvisibleScaleName()).size();
		}
		else{
			numScales = ids.size();
		}

		GraphSync graphSync = new GraphSync(start.getTime(), end.getTime(), del, numScales, range);


		List<DeviceStatusEntry> rawList = new ArrayList<>();

		if(ids == null){ //All scales
			rawList = deviceSyncRepo.findByTimestampBetween(start, end);
		}
		else{ //Append all the scale heartbeat lists
			for(String deviceId : ids){
				rawList.addAll(deviceSyncRepo.findByTimestampBetweenAndDeviceUuid(start, end, deviceId));
			}
		}

		//List where each index indicates an interval dictated by the time range(hour, 8 hours or day) and each interval
		//contains a map which will track whether or not a scale has performed a heartbeat in this interval as well as a
		//boolean to indicate if the scale has been out of sync within this time range
		List<Map<String, Integer>> tracker = Arrays.asList(new HashMap<>(), new HashMap<>(), new HashMap<>(),new HashMap<>(),
				new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),
				new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),
				new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),
				new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),new HashMap<>(),
				new HashMap<>(),new HashMap<>());
		String syncCounterKey = "syncCounter";

		switch (type.toLowerCase()){
			case "raw":
				return new ResponseEntity<>(rawList, HttpStatus.OK);
			case "graph":

				//Generate a list of reports at each level
				for(DeviceStatusEntry status : rawList){
					int index = (int) ((status.getTimestamp().getTime() - start.getTime())/del);
					if(index >= tracker.size() || index < 0){
						log.error("ERROR: index out of bounds generating sync table");
						continue;
					}
					tracker.get(index).putIfAbsent(syncCounterKey, 0);

					Integer existingInSync = tracker.get(index).get(status.getDeviceUuid());
					Integer inSyncCounter = tracker.get(index).get(syncCounterKey);
					//If the existingInSync is true update the entry with a new InSync status from a different time in the interval
					//The purpose of this is to see if there is an out-of-sync for the scale in this interval and
					// if there is persist it
					if(existingInSync == null){

						tracker.get(index).put(syncCounterKey, inSyncCounter + status.isInSyncInt());
						tracker.get(index).put(status.getDeviceUuid(), status.isInSyncInt());
					}
					else if(existingInSync == 1){
						//We are defaulting to "not in sync" if one shows in the given interval
						//So update the counter if one is false by subtracting one from the counter
						if(!status.isInSync()){
							tracker.get(index).put(syncCounterKey, inSyncCounter - 1);
						}
						tracker.get(index).put(status.getDeviceUuid(), status.isInSyncInt());
					}
				}

				//Compress the list of reports into a single counter
				for(Map<String, Integer> item : tracker){
					if(item.size() != 0){ //Remove 1 for the syncCounter in each map
						graphSync.numHeartbeats.add(item.size() - 1);
					}
					else{ //If zero then there is no sync counter so don't subtract
						graphSync.numHeartbeats.add(item.size());
					}

					graphSync.numInSync.add(Optional.ofNullable(item.get(syncCounterKey)).orElse(0));
				}

				OptionalDouble average = graphSync.numHeartbeats
						.stream()
						.mapToDouble(a -> a)
						.average();

				if(graphSync.numScales > 0) {
					graphSync.uptime = String.format("%.2f%%", ((average.isPresent() ? average.getAsDouble() : 0) / graphSync.numScales) * 100);
				}
				else{
					graphSync.uptime = "0%";
				}

				return new ResponseEntity<>(graphSync, HttpStatus.OK);
			default:
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("Invalid format selection");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

		}
	}

	// ---------------
	// Checksumming
	// ---------------
	@Operation(summary="Saves checksum logs", description="Saves to the database logging info why scales are different from profile checksum")
	@ApiResponses({
			@ApiResponse(responseCode="200", description="Logs saved successfully"),
			@ApiResponse(responseCode="400", description="A parameter did not meet requirements")
	})
	@PostMapping(path="/saveChecksumLogs")
	public ResponseEntity<OperationStatusModel> saveChecksumLogs(@RequestBody ChecksumLog checksumLog) {
		log.info("Begin SaveCheckSumLogs");
		OperationStatusModel result = new OperationStatusModel("SaveChecksumLogs");

		if (HteTools.isNullorEmpty(checksumLog.getScaleIp())) {
			log.error("Provided scale IP is null or empty");
			result.setErrorDescription("Provided scale IP is null or empty");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		if (!HttpUtils.isValidInet4Address(checksumLog.getScaleIp())) {
			log.error("Provided scale IP is incorrectly formatted: {}", checksumLog.getScaleIp());
			result.setErrorDescription("Provided scale IP is incorrectly formatted: " + checksumLog.getScaleIp());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		if (HteTools.isNullorEmpty(checksumLog.getDate())) {
			log.error("Provided time is null or empty");
			result.setErrorDescription("Provided time is null or empty");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		LocalDateTime parsed = LocalDateTime.parse(checksumLog.getDate());
		if (!parsed.toString().equals(checksumLog.getDate())) {
			log.error("Provided time is not correct format: {}", checksumLog.getDate());
			result.setErrorDescription("Provided time is not correct format: " + checksumLog.getDate());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		if (HteTools.isNullorEmpty(checksumLog.getLogEntry())) {
			log.error("Provided log entry is null or empty for: {}", checksumLog.getScaleIp());
			result.setErrorDescription("Provided log entry is null or empty for: " + checksumLog.getScaleIp());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		ChecksumLog cLog = new ChecksumLog();
		cLog.setScaleIp(checksumLog.getScaleIp());
		cLog.setDate(parsed.toString());
		cLog.setLogEntry(checksumLog.getLogEntry());
		checksumLogRepo.save(cLog);

		log.info("Finish SaveChecksumLogs");
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary="Get checksum logs", description="Obtains the checksum logs which are saved because of a difference in checksums")
	@ApiResponses({
			@ApiResponse(responseCode="200", description="The logs were obtained")
	})
	@GetMapping(path="/getChecksumLogs")
	public ResponseEntity<?> getChecksumLogs() {
		log.info("Begin GetChecksumLogs");
		OperationStatusModel result = new OperationStatusModel("GetChecksumLogs");

		Iterable<ChecksumLog> logs = checksumLogRepo.findAll();

		log.info("Finish GetChecksumLogs");
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(logs, HttpStatus.OK);
	}

	// ----------------
	// Transactions
	// ----------------

	@Parameters({
			@Parameter(name = "scale_ips", description = "List of scale IP addresses to use to filter the transaction records. If no list is provided all scales will be returned. (Optional)", in = ParameterIn.QUERY, example = "10.3.128.65,10.3.128.128", schema = @Schema(implementation = String.class)),
			@Parameter(name = "plu_list", description = "List of the PLUs to use to filter the transaction records. If no list is provided all items will be returned. (Optional)", in = ParameterIn.QUERY, example = "4011,256,1297,6309", schema = @Schema(implementation = String.class)),
			@Parameter(name = "start", description = "Initial interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)),
			@Parameter(name = "end", description = "Final interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Export Excel report", description = "Generates a report of the transactions reported from scales based on the given parameters.")
	@GetMapping(path = "/excelExport")
	public ResponseEntity<InputStreamResource> exportToExcel(@RequestParam(required = false) List<String> scale_ips,
															 @RequestParam(required = false) List<Integer> plu_list,
															 @RequestParam(required = false) List<String> store_ids,
															 @RequestParam(required = false) List<String> dept_ids,
															 @RequestParam String start,
															 @RequestParam String end, HttpServletResponse response) {
		long timestamp = System.currentTimeMillis();
		log.debug("START OF REQUEST {}", timestamp);

		List<String> adjustedScaleList = getScaleIpList(scale_ips, store_ids, dept_ids);

		log.debug("scale ips: {}", adjustedScaleList.toString());

		log.debug("list of plus: {}", plu_list);
		log.debug("start timestamp: {} ", start);
		log.debug("end timestamp: {}", end);

		Timestamp _start = validateDate(start);
		Timestamp _end = validateDate(end);

		if (start == null || end == null || _end == null || _start == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		List<TransEntry> transactions = null;
		if (adjustedScaleList.isEmpty() && plu_list == null) { //No Scales or PLUs specified
			transactions = (List<TransEntry>) transRepo.findByTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(_start,
					_end);
		} else if (adjustedScaleList.isEmpty() && plu_list != null) { //Only PLUs specified
			transactions = transRepo.findByTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(plu_list, _start,
					_end);
		} else if (plu_list == null) { //Only Scales Specified
			transactions = transRepo.findByScaleIdInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(adjustedScaleList, _start,
					_end);
		} else { //Both PLUs and Scales specified
			transactions = transRepo.findByScaleIdInAndTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(
					adjustedScaleList, plu_list, _start, _end);
		}

		if (transactions == null) {
			transactions = new ArrayList<>();
		}

		TransactionDataExcelExport transactionDataExcel = new TransactionDataExcelExport();
		ByteArrayInputStream in = transactionDataExcel.buildExcelDocument(transactions);

		InputStreamResource file = new InputStreamResource(in);
		String filename = "TransData" + new SimpleDateFormat("-ddMMyy-hhmmss").format(new Date()) + ".xlsx";

		response.addHeader("Content-Length",  String.valueOf(in.available()));

		log.debug("Time to Compile/Export Excel File (ms): {}", System.currentTimeMillis() - timestamp);

		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename +"\"")
				.body(file);
	}

	@Parameters({
			@Parameter(name = "scale_ips", description = "List of scale IP addresses to use to filter the transaction records. If no list is provided all scales will be returned. (Optional)", in = ParameterIn.QUERY, example = "10.3.128.65,10.3.128.128", schema = @Schema(implementation = String.class)),
			@Parameter(name = "plu_list", description = "List of the PLUs to use to filter the transaction records. If no list is provided all items will be returned. (Optional)", in = ParameterIn.QUERY, example = "4011,256,1297,6309", schema = @Schema(implementation = String.class)),
			@Parameter(name = "start", description = "Initial interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)),
			@Parameter(name = "end", description = "Final interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)) })
	@Operation(summary = "Export Excel report", description = "Generates a report of the transactions reported from scales based on the given parameters.")
	@GetMapping(path = "/fetchTransactions")
	public ResponseEntity<TransactionData> fetchTransData(@RequestParam(required = false) List<String> scale_ips,
														  @RequestParam(required = false) List<Integer> plu_list,
														  @RequestParam(required = false) List<String> store_ids,
														  @RequestParam(required = false) List<String> dept_ids,
														  @RequestParam String start,
														  @RequestParam String end, HttpServletResponse response) {
		long timestamp = System.currentTimeMillis();
		List<String> adjustedScaleList = getScaleIpList(scale_ips, store_ids, dept_ids);

		log.debug("scale ips: {}", adjustedScaleList.toString());

		log.debug("list of plus: {}", plu_list);
		log.debug("start timestamp: {} ", start);
		log.debug("end timestamp: {}", end);

		Timestamp _start = validateDate(start);
		Timestamp _end = validateDate(end);

		if (start == null || end == null || _end == null || _start == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		long diffInMillies = Math.abs(_end.getTime() - _start.getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

		List<TransEntry> transactions = null;
		if (adjustedScaleList.isEmpty() && plu_list == null) { //No Scales or PLUs specified
			transactions = (List<TransEntry>) transRepo.findByTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(_start,
					_end);
		} else if (adjustedScaleList.isEmpty() && plu_list != null) { //Only PLUs specified
			transactions = transRepo.findByTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(plu_list, _start,
					_end);
		} else if (plu_list == null) { //Only Scales Specified
			transactions = transRepo.findByScaleIdInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(adjustedScaleList, _start,
					_end);
		} else { //Both PLUs and Scales specified
			transactions = transRepo.findByScaleIdInAndTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(
					adjustedScaleList, plu_list, _start, _end);
		}

		if (transactions == null) {
			transactions = new ArrayList<>();
		}

		return ResponseEntity.ok().body(new TransactionData(transactions, start, end, diff + " day(s)", plu_list,
				adjustedScaleList, dept_ids, store_ids));
	}

	@Operation(summary="Deletes all asset data", description="Deletes banners, regions, stores, departments, devices and their settings, all upgrade data and domains")
	@DeleteMapping(path="/deleteAllAssets")
	public ResponseEntity<OperationStatusModel> deleteAllAssets() {
		DeleteAllAssetsEndpoint endpoint = new DeleteAllAssetsEndpoint(banner_repo, region_repo, store_repo, dept_repo,
				device_repo, ssRepo, upgrade_group_repo, upgrade_device_repo, upgrade_batch_repo, domain_repo, file_repo,
				domainHelper, nodeStatusRepo, storeDeptPairsRepository, filterRepo);

		int numberOfActiveUpgrades = upgrade_device_repo.numberOfActiveUpgrades();
		if (numberOfActiveUpgrades > 0) {
			OperationStatusModel result = new OperationStatusModel("deleteAllAssets");
			result.setErrorDescription("At least one scale is currently undergoing an upgrade.\n" +
					"Please wait for the upgrade to finish or cancel the upgrade, then try again.");
			result.setResult(RequestOperationResult.ERROR.name());

			return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		OperationStatusModel result = endpoint.execute();
		if (result.getResult().equals(RequestOperationResult.ERROR.name())) {
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(result, HttpStatus.NO_CONTENT);
	}

	// ----------------
	// NodeStatus / Lazy Tree APIS
	// ----------------

	@Operation(summary="Asset data by lazy load", description="Based on provided node path, loads in the children of that node, or if nothing provided, loads banners")
	@Parameters({
			@Parameter(name="nodePath", description="The IDs of the node and its parents", in=ParameterIn.QUERY, example="['bannerUUID','regionUUID','storeUUID']")
	})
	@GetMapping(path="/fetchLazyTree")
	public ResponseEntity<?> fetchLazyTree(@RequestParam(required=false, name="nodePath") List<String> nodePath,
										   @RequestParam(required = false, name="expandedKeys") List<String>  expandedKeys) {
		LazyLoadAssetTree tree = new LazyLoadAssetTree(nodePath, domain_repo, banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, upgrade_device_repo, nodeStatusRepo, storeDeptPairsRepository,
				configapp);
		Map<String, ScaleNode> treeNodes;
		try {
			if (expandedKeys == null || expandedKeys.size() == 0) {
				log.info("Executing treeNodes endpoint");
				treeNodes = tree.execute();
			} else {
				treeNodes = tree.refreshTree(expandedKeys);
				log.info("Refreshing tree");
			}
		} catch (Exception e) {
			log.error("Error retrieving tree data during fetchLazyTree - " + e.getMessage());
			OperationStatusModel result = new OperationStatusModel("FetchLazyTree");
			result.setErrorDescription("Error retrieving tree data - " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(treeNodes, HttpStatus.OK);
	}

	@PostMapping(path="/instantiateNodeStatus")
	public ResponseEntity<?> instantiateNodeStatus() {
		log.info("Cleaning and instantiating node statuses");
		InstantiateNodeStatus instantiateNodeStatus = new InstantiateNodeStatus(banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo,  nodeStatusRepo, storeDeptPairsRepository, configapp);
		OperationStatusModel result = new OperationStatusModel();
		try {
			instantiateNodeStatus.execute();
		} catch (Exception e){
			log.error("Error updating node status" + e.getMessage());
			result = new OperationStatusModel("updateNodeStatus" );
			result.setErrorDescription("Error updating node status - "+ e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	// ----------------
	// Dashboard APIs
	// ----------------
	@Operation(summary="Dashboard data by lazy load", description="Fetch dashboard data for provided node id " +
			"(if null grab everything")
			@Parameters({
			@Parameter(name="nodePath", description="The IDs of the node and its parents", in=ParameterIn.QUERY,
					example="['bannerUUID','regionUUID','storeUUID']")
	})
	@GetMapping(path="/dashboard")
	public ResponseEntity<?> dashboard(@RequestParam(required=false, name="nodeId") String nodeId,
										   @RequestParam(required = false, name="nodeType") String  nodeType) {
		Dashboard dashboard = new Dashboard(domain_repo, banner_repo, region_repo, store_repo,
				dept_repo, device_repo, ssRepo, file_repo, upgrade_device_repo, nodeStatusRepo, storeDeptPairsRepository,
				nodeId, nodeType);
		DashboardData dash = new DashboardData();
		try {
			log.debug("Fetching dashboard data");
			dash = dashboard.execute();
			log.debug("Returning dashboard data");
		} catch (Exception e ) {
			OperationStatusModel result = new OperationStatusModel("FetchDashboardData");
			result.setErrorDescription("Error retrieving dashboard data - " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(dash, HttpStatus.OK);
	}
	// ----------------
	// Miscellaneous
	// ----------------

	private Timestamp validateDate(String dateString) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			Date parsedDate = dateFormat.parse(dateString);
			log.debug("parsed date: {}", parsedDate.toString());
			Timestamp timestamp = new Timestamp(parsedDate.getTime());
			log.debug("timestamp: {}", timestamp.toString());
			return timestamp;
		} catch (Exception e) {
			return null;
		}
	}

	@Operation(summary = "Gets the last-update and next-update times for automatic nodeStatus updates.")
	@GetMapping(path = "/getNodeStatusTimers")
	public ResponseEntity<?> getNodeStatusTimers() {

		Map<String, LocalDateTime> timerMap = new HashMap<>();
		timerMap.put("lastUpdate", statusUpdater.getLastUpdate().toLocalDateTime());
		timerMap.put("nextUpdate", statusUpdater.getNextUpdate().toLocalDateTime());

		return new ResponseEntity<>(timerMap, HttpStatus.OK);
	}

	private List<String> getScaleIpList(List<String> scale_ips, List<String> store_ids, List<String> dept_ids){
		List<String> adjustedScaleList = new ArrayList<>();
		if(scale_ips != null && !scale_ips.isEmpty()){
			adjustedScaleList.addAll(scale_ips);
		}

		//Next add scale IPs under any of the entities specified.
		if(store_ids != null && dept_ids != null ){
			adjustedScaleList.addAll(device_repo.findByStoreIdInAndDeptIdInAndHostnameNot(store_ids, dept_ids, ScaleDevice.getInvisibleScaleName())
					.stream().map(ScaleDevice::getIpAddress).collect(Collectors.toList()));
		}
		else if(store_ids != null){
			adjustedScaleList.addAll(device_repo.findByStoreIdInAndHostnameNot(store_ids, ScaleDevice.getInvisibleScaleName())
					.stream().map(ScaleDevice::getIpAddress).collect(Collectors.toList()));
		}
		else if (dept_ids != null){
			adjustedScaleList.addAll(device_repo.findByDeptIdInAndHostnameNot(dept_ids, ScaleDevice.getInvisibleScaleName())
					.stream().map(ScaleDevice::getIpAddress).collect(Collectors.toList()));
		}

		return adjustedScaleList;
	}

	/**
	 *
	 * @return
	 */
	private List<ScaleNode> getBanners() {
		Iterable<Banner> findAll = banner_repo.findAll();
		List<ScaleNode> list = new ArrayList<>();

		for (Banner b : findAll) {
			if (DomainHelper.isValidUUID(b.getBannerId()) && !DomainHelper.isDefaultUUID(b.getBannerId())) {
				//log.debug("banner: {}", b.getBannerId());
				ScaleNode node = new ScaleNode(b);
				node.addNode(getRegions(b.getBannerId()));
				list.add(node);
			}
		}
		Collections.sort(list, (node1, node2) -> node1.getName().compareToIgnoreCase(node2.getName()));
		return list;
	}

	/**
	 *
	 * @param bannerId
	 * @return
	 */
	private NodeContainer getRegions(String bannerId) {
		Iterable<Region> findAll = region_repo.findByBannerId(bannerId);

		// Create an empty container for current region
		// This container will hold a list of nodes of all the stores in the region as
		// well as the status for their region
		NodeContainer regionsNodeContainer = new NodeContainer(new HashMap<>(), new NodeStatus());

		for (Region r : findAll) {
			if (DomainHelper.isValidUUID(r.getRegionId()) && !DomainHelper.isDefaultUUID(r.getRegionId())) {
				//log.debug("    region: {}", r.getRegionNum());
				ScaleNode node = new ScaleNode(r);
				node.setId(bannerId + "|" + node.getId());
				node.addNode(getStores(r.getRegionId()));
				regionsNodeContainer.addNode(node);
			}
		}

		regionsNodeContainer.generateNodeStatus();
		return regionsNodeContainer;
	}

	/**
	 *
	 * @param region
	 * @return
	 */
	private NodeContainer getStores(String region) {
		Iterable<Store> findAll = store_repo.findByRegionId(region);
		List<StoreAndBatchIds> storeIds = device_repo.findStoreIdByBatchId();

		// Create an empty container for current region
		// This container will hold a list of nodes of all the stores in the region as
		// well as the status for their region
		NodeContainer storesNodeContainer = new NodeContainer(new HashMap<>(), new NodeStatus());

		for (Store s : findAll) {
			if (DomainHelper.isValidUUID(s.getStoreId()) && !DomainHelper.isDefaultUUID(s.getStoreId())) {
				//log.debug("        store: {}", s.getStoreNum());
				ScaleNode node = new ScaleNode(s);
				node.setId(String.valueOf(region) + "|" + node.getId());
				node.setBatchIds(storeIds.stream().filter(ids -> ids.getStoreId().equals(s.getStoreId())).map(
						StoreAndBatchIds::getBatchId).collect(Collectors.toList()));

				// node.addNode(getScales(s.getStoreNum()));
				node.addNode(getDepts(s.getStoreId()));
				storesNodeContainer.addNode(node);
			}
		}

		storesNodeContainer.generateNodeStatus();
		return storesNodeContainer;
	}

	public NodeContainer getDepts(String storeNum) {
		// Create an empty container for current store
		// This container will hold a list of nodes of all the departments in the store
		// as well as the nodestatus for their store
		Store store = store_repo.findById(storeNum).orElse(null);
		String awayThreshold = configapp.findById(AppConfig.scaleAwayHours).get().getCoValue();
		String offlineThreshold = configapp.findById(AppConfig.scaleOfflineHours).get().getCoValue();

		NodeStatus storeStatus;
		if(store != null)
			storeStatus = new NodeStatus(store, new ArrayList<>());
		else{
			storeStatus = new NodeStatus();
		}

		List<DeptAndBatchIds> deptIds = device_repo.findDeptIdByBatchId();
		List<UpgradeDevice> upgradeDevices = upgrade_device_repo.findByBatchIdIsNotNull();
		List<String> scaleIds = upgradeDevices.stream().map(UpgradeDevice::getDeviceId).collect(Collectors.toList());

		NodeContainer deptsNodeContainer = new NodeContainer(new HashMap<>(), storeStatus);
		for (Department d : allDepts) {
			if (DomainHelper.isValidUUID(d.getdeptId()) && !DomainHelper.isDefaultUUID(d.getdeptId())) {
				List<ScaleDevice> scales = device_repo.findByStoreIdAndDeptId(storeNum, d.getdeptId());
				if (!scales.isEmpty()) {
					ScaleNode dept = new ScaleNode(d);
					//Create an empty container for current department's children
					//This container will hold the current department's status and it's child nodes
					NodeContainer singleDeptNodeContainer = new NodeContainer(new HashMap<>(), new NodeStatus());
					//log.debug("            dept: {}", d.getDeptName1());
					dept.setId(String.valueOf(storeNum) + "|" + dept.getId());
					dept.setBatchIds(deptIds.stream().filter(ids -> ids.getDeptId().equals(d.getdeptId())).map(
							DeptAndBatchIds::getBatchId).collect(Collectors.toList()));
					for (ScaleDevice s : scales) {
						//log.info("               scale: {}", s.getIpAddress());

						if(!s.isInvisibleScale()){
							ScaleNode newScaleNode = new ScaleNode(s); //Create the Scale
							NodeStatus scaleStatus = new NodeStatus(s);
							scaleStatus.setScaleReportStatus(s, awayThreshold, offlineThreshold);

							ScaleSettings ss = ssRepo.findById(s.getDeviceId()).orElse(null);
							scaleStatus.setScaleSync(s,ss, retrieveFilesforProfile(s.getProfileId()));

							newScaleNode.setNodeStatus(scaleStatus); //Set the Scale Status

							newScaleNode.setBatchIds(upgradeDevices.stream().filter(ids -> ids.getDeviceId().equals(s.getDeviceId())).map(
									UpgradeDevice::getBatchId).collect(Collectors.toList()));

							singleDeptNodeContainer.addNode(newScaleNode); //Add the node to the container
						}

					}
					//Now generate the single deparment's status based off of it's children
					singleDeptNodeContainer.generateNodeStatus();
					//Inject the container into the department node
					dept.addNode(singleDeptNodeContainer);

					//Now add the single department node to the depts container
					deptsNodeContainer.addNode(dept);
				}
			}
		}
		// Once all the departments have been added to the container
		// Generate the status for their parent store
		deptsNodeContainer.generateNodeStatus();
		return deptsNodeContainer;
	}

	private List<HobFiles> retrieveFilesforProfile(String profileId) {
		if(profileId == null){
			return null;
		}

		List<HobFiles> activeFiles = new ArrayList<>();
		List<FileForEvent> allFilesInProfile = file_repo.findByProfileIdAndEnabled(profileId, true);

		Timestamp now = Timestamp.from(Instant.now());

		for (FileForEvent ffe : allFilesInProfile) {
			if ((ffe.getStartDate() == null && ffe.getEndDate() == null)
					|| (ffe.getStartDate().before(now) && ffe.getEndDate() == null)
					|| (ffe.getStartDate().before(now) && ffe.getEndDate().after(now))) {
				activeFiles.add(new HobFiles(ffe));
			}
		}
		return activeFiles;
	}

    // Ahh, much better!
	private ResponseEntity<OperationStatusModel> importUsingScaleNode(List<ScaleNode> payload,
																	  OperationStatusModel model) {

		OperationStatusModel result = model;
		String bannerDomain = "", regionDomain = "", storeDomain = "", deptDomain = "";

		for(ScaleNode element : payload){
			String newID = UUID.randomUUID().toString();
			switch (element.getType()){
				case "banner":
					if (!banner_repo.existsByBannerNameIgnoreCase(element.getName())) {
						Banner newBanner = new Banner();
						newBanner.setBannerId(newID);
						newBanner.setBannerName(element.getName());

						bannerDomain = newBanner.getBannerId();
						try {
							domainHelper.saveDomain(newBanner.getBannerId(), EntityType.BANNER, AppConfig.DefaultAdminId);
							banner_repo.save(newBanner);
							result.setResult(RequestOperationResult.SUCCESS.name());
							log.debug("adding new banner: {}", newBanner.getBannerId());
						} catch (Exception ex) {
							log.debug("error while adding banner: {}", ex.getMessage());
							result.setErrorDescription("Error while adding banner: " + ex.getMessage());
							result.setResult(RequestOperationResult.ERROR.name());
							return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
						}
					}
					else {
						break;
					}

					break;
				//======================================================================================================
				case "region":
					if (!region_repo.existsByRegionNameIgnoreCase(element.getName())) {
						Region newRegion = new Region();
						newRegion.setRegionId(newID);
						newRegion.setBannerId(bannerDomain);
						newRegion.setRegionName(element.getName());

						regionDomain = newRegion.getRegionId();
						try {
							domainHelper.saveDomain(newRegion.getRegionId(), EntityType.REGION, newRegion.getBannerId());
							region_repo.save(newRegion);
							log.debug("adding new region: {}", newRegion.getRegionId());
						} catch (Exception ex) {
							log.debug("error while creating/updating region: {}", ex.getMessage());
							result.setResult(RequestOperationResult.ERROR.name());
							result.setErrorDescription("Error creating/updating region: " + ex.getMessage());
							return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
						}
					}
					else {
						break;
					}

					break;
				//======================================================================================================
				case "store":
					if (!store_repo.existsByStoreNameIgnoreCase(element.getName())) {
						Store newStore = new Store();
						newStore.setStoreId(newID);
						newStore.setRegionId(regionDomain);
						newStore.setAddress("");
						newStore.setCustomerStoreNumber("");
						newStore.setStoreName(element.getName());

						storeDomain = newStore.getStoreId();
						try {
							domainHelper.saveDomain(newStore.getStoreId(), EntityType.STORE, newStore.getRegionId());
							if (newStore.getEnabled() == null) {
								newStore.setEnabled(true);
							}
							store_repo.save(newStore);

							log.debug("adding new store: {}", newStore.getStoreId());
						} catch (Exception ex) {
							result.setErrorDescription("Error Saving Store: " + ex.getMessage());
							result.setResult(RequestOperationResult.ERROR.name());
							return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
						}
					}
					else {
						Store foundStore = store_repo.findByStoreNameIgnoreCase(element.getName());
						storeDomain = foundStore.getStoreId();
						break;
					}
					break;
				//======================================================================================================
				case "dept":
					// We *really* don't want to risk duplicating departments! It'll leave a mess for the customer.
					// We can do away with the previous validations if needed, but this should stay.

					String deptName = element.getName();

					if (!dept_repo.existsByDeptName1IgnoreCase(deptName)) {
						Department newDepartment = new Department();
						newDepartment.setdeptId(newID);
						newDepartment.setRequiresDomainId(DefaultAdminId);
						newDepartment.setDeptName1(element.getName());
						if (profile_repo.existsByNameIgnoreCase(deptName)) {
							String profile = profile_repo.findProfileIDByProfileName(deptName);
							newDepartment.setDefaultProfileId(profile);
						}
						else {
							newDepartment.setDefaultProfileId(AppConfig.DefaultProfileId);
						}

						deptDomain = newDepartment.getdeptId();
						try {
							domainHelper.saveDomain(newDepartment.getdeptId(), EntityType.DEPARTMENT, newDepartment.getRequiresDomainId());
							dept_repo.save(newDepartment);
							storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, deptDomain));
							log.debug("adding new department: {}", newDepartment.getdeptId());
						} catch (Exception ex) {
							result.setErrorDescription("Error saving department: " + ex.getMessage());
							result.setResult(RequestOperationResult.ERROR.name());
							return new ResponseEntity<>(result, HttpStatus.CONFLICT);
						}
					}
					// We don't need to break the method here, since we'll be iterating through departments quite often
					// All we'll do is retrieve the department domain for the sake of any subsequent scale objects
					// Just note the comments above regarding duplication if you need to modify this code
					else {
						Department foundDepartment = dept_repo.findByDeptName1IgnoreCase(element.getName());
						deptDomain = foundDepartment.getdeptId();
						storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, deptDomain));
					}

					break;
				//======================================================================================================
				case "scale":
					if (!device_repo.existsByIpAddress(element.getProperties().getIpAddress())) {
						ScaleDevice device = element.getProperties();
						device.setDeviceId(newID);
						device.setProfileId(AppConfig.DefaultProfileId);
						device.setStoreId(storeDomain);
						device.setDeptId(deptDomain);
						if(device.getIsPrimaryScale() == null){
							device.setIsPrimaryScale(false);
						}
						try {
							log.info("creating/updating scale device: {}", device.getIpAddress());

							device_repo.save(device);
						} catch (Exception ex) {
							log.info("error when creating/updating scale on db: {}", ex.getMessage());
							result.setErrorDescription(ex.getMessage());
							result.setResult(RequestOperationResult.ERROR.name());
							return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
						}
					}

					else {
						break;
					}
					break;
				default:
					break;

			}
		}
		try {
			NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, store_repo,
					dept_repo, device_repo, ssRepo, file_repo, nodeStatusRepo, storeDeptPairsRepository, configapp);
			nodeHelper.instantiateNodeStatus();
		}catch (Exception e){
			result.setErrorDescription("Could not set node statuses " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		result.setResult(RequestOperationResult.SUCCESS.name());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

    private boolean ipRangeConflicts(String[] currentIPStart, String[] ipStart,
										String[] currentIPEnd, String[] ipEnd) {
		boolean isThereOverlap = false;

		if (currentIPStart[0].equals(ipStart[0]) && currentIPEnd[0].equals(ipEnd[0])) {
			if (currentIPStart[1].equals(ipStart[1]) && currentIPEnd[1].equals(ipEnd[1])) {
				if (currentIPStart[2].equals(ipStart[2]) && currentIPEnd[2].equals(ipEnd[2])) {
					if ((Integer.parseInt(currentIPStart[3]) <= Integer.parseInt(ipStart[3])) && (Integer.parseInt(ipStart[3]) <= Integer.parseInt(currentIPEnd[3]))
							|| ((Integer.parseInt(ipStart[3]) <= Integer.parseInt(currentIPStart[3])) && (Integer.parseInt(currentIPStart[3]) <= Integer.parseInt(ipEnd[3])))) {
						isThereOverlap = true;
					}
				}
			}
		}
		return isThereOverlap;
	}


	@Operation(summary = "Push one feature file to a scale",
			description = "Receives a single scale and a feature file to send it, then sends and applies that file")
	@PostMapping(path = "/pushOneOffFeature", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<OperationStatusModel> pushOneOffFeature
			(@RequestBody MultipartFile file,
			 @RequestParam(name = "ipAddress") String ipAddress) throws IOException {

		OneOffFeatureUpgradeEndPoint featureEndpoint = new OneOffFeatureUpgradeEndPoint();
		OperationStatusModel result = featureEndpoint.execute(ipAddress, file);

		if (result.getResult().equals(RequestOperationResult.ERROR.name())) {
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(result, HttpStatus.NO_CONTENT);
	}

	@GetMapping("/test")
	public String testService() {
		log.info("The hte-enterprise.war test endpoint has been invoked!");
		List<CombinedScaleDataInt> unassignedScales = device_repo.findUnassignedCombinedScaleData();
		String unassignedId = DefaultUnassignedId;
		log.info(device_repo.combinedScaleQuery + " WHERE sd.storeId = " + unassignedId + " AND sd.deptId = " + unassignedId);
		log.info(unassignedScales.size());
		return "Greetings from HTe Enterprise!";
	}
}
