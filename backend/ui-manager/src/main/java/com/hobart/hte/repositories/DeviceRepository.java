package com.hobart.hte.repositories;

import java.sql.Timestamp;
import java.util.List;

import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.device.CombinedScaleData;
import com.hobart.hte.utils.device.CombinedScaleDataInt;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.stats.DeviceCountPerFirmware;
import com.hobart.hte.utils.device.stats.DeviceCountPerModel;
import com.hobart.hte.utils.device.stats.DeviceCountPerOnlineStatus;
import com.hobart.hte.utils.upgrade.DeptAndBatchIds;
import com.hobart.hte.utils.upgrade.StoreAndBatchIds;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface DeviceRepository extends CrudRepository<ScaleDevice, String> {

	ScaleDevice findByIpAddress(String ipAddress);

	List<ScaleDevice> findByStoreId(String storeId);
	@Query("SELECT sd.deviceId FROM ScaleDevice AS sd WHERE sd.storeId = :storeId")
	List<String> findDeviceIdByStoreId(String storeId);

	List<ScaleDevice> findByDeptId(String deptId);

	List<ScaleDevice> findByStoreIdAndDeptId(String storeId, String deptId);
	@Query("SELECT sd.deviceId FROM ScaleDevice AS sd WHERE sd.storeId = :storeId AND sd.deptId =:deptId")
	List<String> findDeviceIdByStoreIdAndDeptId(@Param("storeId") String storeId, @Param("deptId") String deptId);
	List<ScaleDevice> findByStoreIdNotAndDeptIdNot(String storeId, String deptId);

	List<ScaleDevice> findByProfileId(String profileId);

	List<ScaleDevice> findByHostnameNot(String hostname);

	List<ScaleDevice> findByStoreIdInAndHostnameNot(List<String> storeId, String ignoreHostName);

	List<ScaleDevice> findByDeptIdInAndHostnameNot(List<String> deptId, String ignoreHostName);

	List<ScaleDevice> findByStoreIdInAndDeptIdInAndHostnameNot(List<String> storeId,List<String> deptId, String ignoreHostName);

	List<ScaleDevice> findByIsPrimaryScale(Boolean isPrimaryScale);

	@Transactional
	void deleteByHostnameAndDeptIdAndStoreId(String hostname, String deptId, String storeId);

	boolean existsByIpAddress(String ipAddress);

	@Query("SELECT sd FROM ScaleDevice AS sd LEFT JOIN UpgradeDevice AS ud ON sd.deviceId = ud.deviceId LEFT JOIN" +
			" UpgradeGroup AS ug ON ud.groupId = ug.groupId WHERE ug.groupId IN (:groupIds)")
	List<ScaleDevice> findStoreIdByGroupId(@Param("groupIds") List<String> groupIds);

	@Query("SELECT new com.hobart.hte.utils.device.stats.DeviceCountPerFirmware(c.application, COUNT(c.application)) "
			+ "FROM ScaleDevice AS c GROUP BY c.application ORDER BY c.application DESC")
	List<DeviceCountPerFirmware> countTotalDevicesByFirmwareVersion();

	@Query("SELECT new com.hobart.hte.utils.device.stats.DeviceCountPerModel(c.scaleModel, COUNT(c.scaleModel)) "
			+ "FROM ScaleDevice AS c GROUP BY c.scaleModel ORDER BY c.scaleModel DESC")
	List<DeviceCountPerModel> countTotalDevicesByModel();

	@Query("SELECT new com.hobart.hte.utils.upgrade.StoreAndBatchIds(sd.storeId, ud.batchId) FROM ScaleDevice " +
			"AS sd LEFT JOIN UpgradeDevice AS ud ON sd.deviceId = ud.deviceId WHERE ud.batchId IS NOT NULL")
	List<StoreAndBatchIds> findStoreIdByBatchId();

	@Query("SELECT new com.hobart.hte.utils.upgrade.DeptAndBatchIds(sd.deptId, ud.batchId) FROM ScaleDevice " +
			"AS sd LEFT JOIN UpgradeDevice AS ud ON sd.deviceId = ud.deviceId WHERE ud.batchId IS NOT NULL")
	List<DeptAndBatchIds> findDeptIdByBatchId();

	@Query("SELECT sd FROM ScaleDevice AS sd LEFT JOIN Store AS s ON s.storeId = sd.storeId LEFT JOIN Region AS r " +
			"ON r.regionId = s.regionId LEFT JOIN Banner AS b ON b.bannerId = r.bannerId WHERE b.bannerId = :bannerId")
	List<ScaleDevice> findByBannerId(@Param("bannerId") String bannerId);

	@Query("SELECT sd.deviceId FROM ScaleDevice AS sd LEFT JOIN Store AS s ON s.storeId = sd.storeId LEFT JOIN Region AS r " +
			"ON r.regionId = s.regionId LEFT JOIN Banner AS b ON b.bannerId = r.bannerId WHERE b.bannerId = :bannerId")
	List<String> findDeviceIdByBannerId(@Param("bannerId") String bannerId);
	@Query("SELECT sd FROM ScaleDevice AS sd LEFT JOIN Store AS s ON s.storeId = sd.storeId LEFT JOIN Region AS r " +
			"ON r.regionId = s.regionId WHERE r.regionId = :regionId")
	List<ScaleDevice> findByRegionId(@Param("regionId") String regionId);
	@Query("SELECT sd.deviceId FROM ScaleDevice AS sd LEFT JOIN Store AS s ON s.storeId = sd.storeId LEFT JOIN Region AS r " +
			"ON r.regionId = s.regionId WHERE r.regionId = :regionId")
	List<String> findDeviceIdByRegionId(@Param("regionId") String regionId);

	@Query("SELECT new com.hobart.hte.utils.device.ScaleDevice(sd.deviceId, sd.ipAddress, sd.hostname, sd.serialNumber, " +
			"sd.scaleModel, sd.storeId, sd.deptId, sd.countryCode, sd.buildNumber, sd.application, sd.operatingSystem, " +
			"sd.systemController, sd.loader, sd.smBackend, sd.lastTriggerType, sd.profileId, sd.isPrimaryScale, sd.enabled, " +
			"sd.pluCount, sd.totalLabelsPrinted, sd.profileRecentlyUpdated, ss.labelStockSize, ss.weigherPrecision)"
			+ " FROM ScaleDevice AS sd LEFT JOIN ScaleSettings AS ss ON sd.deviceId = ss.deviceId")
	List<ScaleDevice> findAllWithExtraData();

	List<ScaleDevice> findByLastReportTimestampUtcAfter(Timestamp timeago);

	List<ScaleDevice> findByLastReportTimestampUtcBefore(Timestamp timeago);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE scaleDevice SET profileRecentlyUpdated = 1 WHERE profileId = :profileId", nativeQuery = true)
	int notifyScalesOfUpdate(@Param("profileId") String profileId);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE scaleDevice SET profileRecentlyUpdated = 0 WHERE ipAddress = :ipAddress", nativeQuery = true)
	int disableScaleNotificationStatus (@Param("ipAddress") String ipAddress);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "update scaleDevice s set s.profileId = :new_profile where s.profileId = :current_profile", nativeQuery = true)
	int updateScaleDeviceSetProfileIdForProfileId(@Param("new_profile") String new_profile,
			@Param("current_profile") String current_profile);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE scaleDevice s SET s.deptId = :new_department, s.storeId = :new_store where s.deptId = :current_department", nativeQuery = true)
	int removeScalesOfDeletedDepartment(@Param("new_department") String new_profile,
											  @Param("new_store") String new_store,
											  @Param("current_department") String current_department);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE scaleDevice s SET s.deptId = :new_department, s.storeId = :new_store where s.deptId = :current_department AND s.storeId = :current_store", nativeQuery = true)
	int removeScalesOfOneDepartment(@Param("new_department") String new_profile,
											  @Param("new_store") String new_store,
											  @Param("current_department") String current_department,
										      @Param("current_store") String current_store);

	@Query("SELECT new com.hobart.hte.utils.device.stats.DeviceCountPerOnlineStatus(COUNT(c.scaleModel)) "
			+ "FROM ScaleDevice AS c WHERE c.lastReportTimestampUtc BETWEEN :t1 AND :t2 GROUP BY c.scaleModel ORDER BY c.scaleModel DESC")
	List<DeviceCountPerOnlineStatus> countTotalDevicesOnlineStatus(@Param("t1") Timestamp t1, @Param("t2") Timestamp t2);

	@Override
	void deleteAll();

	// This allows us to subvert the need for a relationship between the ScaleDevice and ScaleSettings entities,
	// which would break many other implementations as of this writing
	@Query(value=combinedScaleQuery, nativeQuery=true)
	List<CombinedScaleDataInt> findCombinedScaleData();

	@Query(value=combinedScaleQuery + " WHERE sd.storeId != '" + unassignedId + "' AND sd.deptId != '" + unassignedId + "'",
			nativeQuery=true)
	List<CombinedScaleDataInt> findAssignedCombinedScaleData();

	@Query(value=combinedScaleQuery + " WHERE sd.storeId = '" + unassignedId + "' AND sd.deptId = '" + unassignedId + "'",
			nativeQuery=true)
	List<CombinedScaleDataInt> findUnassignedCombinedScaleData();

	@Query(value = combinedScaleQuery + " LEFT JOIN store AS s ON s.storeId = sd.storeId LEFT JOIN region AS r " +
			"ON r.regionId = s.regionId LEFT JOIN banner AS b ON b.bannerId = r.bannerId WHERE b.bannerId = :bannerId"
	, nativeQuery = true)
	List<CombinedScaleDataInt> findCombinedScalesByBannerId(@Param("bannerId") String bannerId);

	@Query(value = combinedScaleQuery + " LEFT JOIN store AS s ON s.storeId = sd.storeId LEFT JOIN region AS r " +
			"ON r.regionId = s.regionId WHERE r.regionId = :regionId", nativeQuery = true)
	List<CombinedScaleDataInt> findCombinedScalesByRegionId(@Param("regionId") String regionId);

	@Query(value = combinedScaleQuery + " WHERE sd.storeId = :storeId", nativeQuery = true)
	List<CombinedScaleDataInt> findCombinedScalesByStoreId(@Param("storeId") String storeId);

	/**
	* returns the number of scales that are assigned
	*/
	Long countByStoreIdNotAndDeptIdNot(String storeId, String deptId);

	/**
	* returns the number of scales that are unassigned
	*/
	Long countByStoreIdAndDeptId(String storeId, String dept);

	/**
	 * returns the total number of scales
	 */
    default long countDeviceId() {
		return this.count();
	}

	// Just so we have to write this behemoth of a query once, and only once.
	String combinedScaleQuery = "SELECT sd.deviceId, sd.ipAddress, sd.hostname, sd.serialNumber, " +
			"sd.scaleModel, sd.storeId, sd.deptId, sd.countryCode, sd.buildNumber, sd.application, sd.operatingSystem, " +
			"sd.systemController, sd.loader, sd.smBackend, sd.lastTriggerType, sd.profileId, sd.isPrimaryScale, sd.enabled, " +
			"sd.pluCount, sd.totalLabelsPrinted, sd.profileRecentlyUpdated, sd.lastReportTimestampUtc, sd.healthInfoLog, " +
			"    (SELECT ss.labelStockSize FROM scaleSettings ss\n" +
			"    WHERE ss.deviceId = sd.deviceId) AS labelStockSize,\n" +
			"    (SELECT ss.weigherPrecision FROM scaleSettings ss\n" +
			"    WHERE ss.deviceId = sd.deviceId) AS weigherPrecision,\n" +
			"    (SELECT ss.appHookVersion FROM scaleSettings ss\n" +
			"    WHERE ss.deviceId = sd.deviceId) AS appHookVersion\n" +
			"    FROM scaleDevice AS sd";

	String unassignedId = AppConfig.DefaultUnassignedId;
}