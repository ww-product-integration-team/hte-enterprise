package com.hobart.hte.upgrading;

import com.hobart.hte.helpers.DomainHelper;
import com.hobart.hte.repositories.*;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.file.FileType;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.upgrade.*;
import com.hobart.hte.utils.util.HteTools;
import com.jcraft.jsch.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Tag(name="upgrade-controller", description="Endpoints for upgrading the firmware of devices")
@RestController
@RequestMapping("/upgrade")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class UpgradeController {

    @Autowired
    UpgradeDeviceRepository upgradeDeviceRepo;

    @Autowired
    UpgradeBatchRepository upgradeBatchRepo;

    @Autowired
    UpgradeGroupRepository upgradeGroupRepo;

    @Autowired
    DeviceRepository deviceRepo;

    @Autowired
    ConfigAppRepository configRepo;

    @Autowired
    FileRepository fileRepo;

    @Autowired
    DomainHelper domainHelper;

    @Autowired
    UpgradeHelper upgradeHelper;

    @Autowired
    AutoAssignRepository rulesRepo;

    private static final Logger log = LogManager.getLogger(UpgradeController.class);

    @Parameters({
            @Parameter(name = "", description = "", in = ParameterIn.QUERY, example = "", schema = @Schema(implementation = String.class))
    })
    @Operation(summary = "Import CSV of group(s) of devices", description = "")
    @PostMapping(path = "/import", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> importGroup(@RequestBody Object file) {
        OperationStatusModel result = new OperationStatusModel("ImportGroup");

        return new ResponseEntity<>(result, HttpStatus.NOT_IMPLEMENTED);
    }

    @Operation(summary = "Saves a file to the server database", description = "Will save a file to the database used for batches")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The file was successfully uploaded to the server"),
            @ApiResponse(responseCode = "400", description = "The provided file is null."),
            @ApiResponse(responseCode = "500", description = "There was an issue with using the file.")
    })
    @PostMapping(path = "/uploadFile")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        log.info("Begin Upgrade File Upload");

        OperationStatusModel result = new OperationStatusModel("UploadFile");
        String repoPath = configRepo.findById(AppConfig.repositoryPath)
                .orElse(new ConfigEntry("", "")).getCoValue();

        // validate file argument
        if (file == null) {
            log.error("Provided file cannot be null");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided file cannot be null");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        // check if directory exists and create it if not
        String filename = file.getOriginalFilename();
        File upgradeDirectory = Paths.get(repoPath, "upgrades").toFile();
        if (!upgradeDirectory.exists() && filename != null && (filename.endsWith(".package.tgz") || upgradeHelper.isFileGTType(filename))) {
            if (!upgradeDirectory.mkdirs()) {
                log.error("Unable to create directory: {}", upgradeDirectory.getAbsolutePath());
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Unable to create directory: " + upgradeDirectory.getAbsolutePath());
                return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            } else {
                log.info("Upgrade directory created ;D");
            }
        }

        try {
            String subDirectory = "upgrades";
            File tempFile = Paths.get(repoPath, subDirectory, filename).toFile();

            try {
                file.transferTo(tempFile);
            } catch (Exception e) {
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("An error occurred when trying to transfer the file: "
                        + upgradeDirectory.getAbsolutePath() + "\nerror: " + e.toString());
                return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            log.info("File uploaded: ", filename);

            // Using MD5 Checksumming
            MessageDigest md5Digest = MessageDigest.getInstance("MD5");
            String checksum = HteTools.getFileChecksum(md5Digest, tempFile);

            FileForEvent saved;
            FileForEvent newFile = new FileForEvent();
            newFile.setChecksum(checksum);
            Optional<FileForEvent> existingFile = fileRepo.findByFilenameAndProfileId(filename, null);
            if (!existingFile.isPresent()) {
                log.info("New file, creating a new UUID");
                newFile.setFileId(UUID.randomUUID().toString());
                newFile.setFileVersion("1.0");
                newFile.setFilename(filename);
                newFile.setSize(tempFile.length());
                newFile.setUploadDate(Timestamp.from(Instant.now()));
                newFile.setEnabled(true);
                if (filename != null) {
                    if (filename.endsWith(".package.tgz")) {
                        newFile.setFileType(FileType.FEATURE);
                    } else if (upgradeHelper.isFileGTType(filename)) {
                        newFile.setFileType(FileType.DEBIAN);
                    }
                }
                saved = fileRepo.save(newFile);
            } else {
                log.info("File already exists in the database, it will be replaced");
                existingFile.get().setFileVersion(new Double(Double.parseDouble(existingFile.get().getFileVersion()) + 1).toString());
                existingFile.get().setUploadDate(Timestamp.from(Instant.now()));
                saved = fileRepo.save(existingFile.get());
            }

            log.info("File '{}' uploaded successfully ;D", tempFile.getAbsolutePath());
            log.info("Finish Upgrade File Upload");
            return new ResponseEntity<>(saved, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("An error occurred while uploading the file: " + e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Makes a CSV of all the groups in the database", description = "")
    @GetMapping(path = "/export", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<InputStreamResource> exportGroup(HttpServletResponse response) {
        log.info("Begin Upgrade Device Excel Export Download");

        Iterable<UpgradeDevice> all = upgradeDeviceRepo.findAll();
        List<UpgradeDevice> upgradeDevices = new ArrayList<>();
        all.forEach(upgradeDevices::add);

        UpgradeDeviceExcelExport export = new UpgradeDeviceExcelExport();
        ByteArrayInputStream in = export.buildExcelDocument(upgradeDevices);
        InputStreamResource file = new InputStreamResource(in);
        String filename = "UpgradeDevices" + new SimpleDateFormat("-ddMMyy-hhmmss").format(new Date()) + ".xlsx";

        response.addHeader("Content-Length", String.valueOf(in.available()));

        log.info("Finish Upgrade Device Excel Export Download");
        return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"")
                .body(file);
    }

    @Operation(summary = "Saves a group with the provided devices", description = "Using the provided devices, saves a group")
    @Parameters({
            @Parameter(name="batchUpgrade", description="Flag for if action is for a batch upgrading group or manual", in=ParameterIn.QUERY,
                    example="false", schema=@Schema(implementation=Boolean.class))
    })
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The group was saved successfully."),
            @ApiResponse(responseCode = "400", description = "The provided data does not meet the criteria for saving a group."),
            @ApiResponse(responseCode = "500", description = "An error occurred while saving to the database.")
    })
    @PostMapping(path = "/saveGroup", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> saveGroup(@RequestParam(name="batchUpgrade", required=false) Boolean batchUpgrade,
                                                          @RequestBody UpgradeGroup group) {
        log.info("Begin Saving a Group");
        OperationStatusModel result = new OperationStatusModel("SaveGroup");

        String mixedScaleTypesReport = "";
        if (batchUpgrade) { // This evaluation is only meaningful if we're doing batch upgrades
            mixedScaleTypesReport = findIfMixingTypes(group.getScales());
        }

        // validation
        log.info("Validating request data");
        if (group.getScales() == null || group.getScales().isEmpty()) {
            log.error("Provided devices is null or empty, please provide devices to group");
            result.setErrorDescription("Provided devices is null or empty, please provide devices to group");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (batchUpgrade && mixedScaleTypesReport.contains("ERROR")) {

            // For clarity, this can be either an IP address or a scale model, depending on the error type
            String problematicScaleData = mixedScaleTypesReport.split(":")[2];

            if (mixedScaleTypesReport.contains("GT IN HT")) {
                log.error("A non-HT scale was found in this upgrade group: {}", problematicScaleData);
                result.setErrorDescription("A non HT-scale (" + problematicScaleData + ") was found in the group. " +
                        "Please remove this scale before attempting to proceed.");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            if (mixedScaleTypesReport.contains("HT IN GT")) {
                log.error("A non-GT scale was found in this upgrade group: {}", problematicScaleData);
                result.setErrorDescription("A non GT-scale (" + problematicScaleData + ") was found in the group. " +
                        "Please remove this scale before attempting to proceed.");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            if (mixedScaleTypesReport.contains("TIMEOUT")) {
                log.error("An unascertainable scale couldn't be reached: {}", problematicScaleData);
                result.setErrorDescription("An attempt to reach an unascertained scale (" + problematicScaleData + ") via SSH has failed. " +
                        "Please verify connectivity to this scale before proceeding.");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            if (mixedScaleTypesReport.contains("UNKNOWN")) {
                log.error("A scale of unknown model was part of the upgrade group {}", problematicScaleData);
                result.setErrorDescription("An unsupported/unknown scale model (" + problematicScaleData + ") is in this group. " +
                        "Please remove this scale or contact Hobart Support.");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
        }

        if (group.getGroupName() == null || group.getGroupName().isEmpty()) {
            log.error("Provided group name is null or empty, please provide a valid name");
            result.setErrorDescription("Provided group name is null or empty, please provide a valid name");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        } else if (group.getGroupName().length() > 255) {
            log.error("Provided name is too long, please shorten to a more concise length");
            result.setErrorDescription("Provided name is too long, please shorten to a more concise length");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        // check if group exists in db
        boolean inDb = false;
        if (DomainHelper.isValidUUID(group.getGroupId()) && upgradeGroupRepo.findById(group.getGroupId()).isPresent()) {
            inDb = true;
        } else {
            group.setGroupId(UUID.randomUUID().toString());
            group.setStatus(UpgradeGroup.Status.DISABLED.name());
        }

        if (inDb) {
            // remove left over scales
            List<UpgradeDevice> newGroupScales = group.getScales();
            List<UpgradeDevice> oldGroupScales = upgradeDeviceRepo.findAllByGroupId(group.getGroupId());
            for (UpgradeDevice oldGroupScale : oldGroupScales) {
                if (newGroupScales.stream().noneMatch(scale -> scale.getDeviceId().equals(oldGroupScale.getDeviceId()))) {
//                if (!newGroupScales.contains(oldGroupScale)) {
                    try {
                        upgradeDeviceRepo.deleteByDeviceId(oldGroupScale.getDeviceId());
                    } catch (Exception e) {
                        log.error("Exception while trying to delete upgrade device: " + oldGroupScale.getDeviceId() + ".");
                        result.setErrorDescription("Exception while trying to delete upgrade device: " + oldGroupScale.getDeviceId() + ".");
                        result.setResult(RequestOperationResult.ERROR.name());
                        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
            }
        }
        // create new upgrade devices if they don't exist
        List<UpgradeDevice> newScales = new ArrayList<>();
        String duplicatedScales = "";
        boolean throwDuplicationError = false;
        for (UpgradeDevice scale : group.getScales()) {
            if (upgradeDeviceRepo.existsByDeviceId(scale.getDeviceId())) {
                if (!inDb) {
                    throwDuplicationError = true;
                    duplicatedScales += (scale.getIpAddress()) + ", ";
                }
            } else {
                scale.setUpgradeId(UUID.randomUUID().toString());
                scale.setGroupId(group.getGroupId());
                scale.setBatchId(null);
                newScales.add(scale);
            }
        }
        if (throwDuplicationError) {
            result.setErrorDescription("Upgrade device(s) already exists for: \n" + duplicatedScales);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        try {
            log.info("Saving group");
            upgradeGroupRepo.save(group);
        } catch (Exception e) {
            log.error("Exception while trying to save group");
            result.setErrorDescription("Exception while trying to save group");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        try {
            log.info("Saving upgrade devices");
            upgradeDeviceRepo.saveAll(newScales);
        } catch (Exception e) {
            log.error("Exception while trying to save upgrade devices. There might be some scales already in a group.");
            if (!inDb) {
                try {
                    upgradeGroupRepo.deleteById(group.getGroupId());
                } catch (Exception ex) {
                    log.error("Exception deleting group to clean up group creation.");
                    result.setErrorDescription("Exception deleting group to clean up group creation.");
                    result.setResult(RequestOperationResult.ERROR.name());
                    return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            result.setErrorDescription("Exception while trying to save upgrade devices. There might be some scales already in a group.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info("Finish Saving Group");
        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private String findIfMixingTypes(List<UpgradeDevice> devices) {
        String firstType = null; // The first scale evaluated from the provided list
        boolean isFirstHTScale = true; // i.e., is the first scale in the group in the HT "family"?
        for (UpgradeDevice device : devices) {

            String nextType;
            if (device.getScaleModel() != null) { // If we're heartbeating, we *should* have this data
                nextType = device.getScaleModel();
            } else { // We have no way to infer the model from the DB, so we'll inquire via SSH
                nextType = HteTools.getIsHTScale(device.getIpAddress());
            }

            if (nextType.equals("TIMEOUT")) { // We queried over SSH and failed. Shucks.
                return "ERROR:TIMEOUT:" + device.getIpAddress();
            }

            String modelPrefix = StringUtils.substring(nextType, 0, 2);

            // If this is true, we got some ghoulish scale that doesn't match any known prefix.
            // This should *never* happen outside a testing environment, but for everyone's sanity, we'll chuck it.
            boolean neitherHTNorGT =
                    !HteTools.validHTPrefixes.contains(modelPrefix) && !HteTools.validGTPrefixes.contains(modelPrefix);
            if (neitherHTNorGT) {
                return "ERROR:UNKNOWN SCALE MODEL:" + device.getScaleModel();
            }

            if (firstType == null) {
                firstType = modelPrefix;

                if (!HteTools.validHTPrefixes.contains(modelPrefix)) {
                    isFirstHTScale = false; // We started with a GT in the group.
                }
                continue;
            }

            // Writing these in English syntax to make the following code a bit easier to follow
            boolean firstIsHTButNextIsGT = isFirstHTScale && !HteTools.validHTPrefixes.contains(modelPrefix);
            boolean firstIsGTButNextIsHT = !isFirstHTScale && HteTools.validHTPrefixes.contains(modelPrefix);

            if (firstIsHTButNextIsGT) {
                return "ERROR:GT IN HT GROUP:" + device.getIpAddress();
            }

            if (firstIsGTButNextIsHT) {
                return "ERROR:HT IN GT GROUP:" + device.getIpAddress();
            }
        }
        return "NO ISSUE";
    }

    @Operation(summary = "Sets the groups as enabled", description = "Sets all the groups as enabled, only if at least one device is primary per store and a batch exists")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The provided groups were enabled."),
            @ApiResponse(responseCode = "400", description = "Stores that contain the scales of a group to be enabled do not have a primary scale."),
            @ApiResponse(responseCode = "404", description = "At least one batch must be created to enable groups.")
    })
    @PostMapping(path = "/enableGroups", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> enableGroups(@RequestBody List<UpgradeGroup> groups) {
        log.info("Begin Enabling Provided Groups");
        OperationStatusModel result = new OperationStatusModel("EnableGroups");

        log.info("Combining group ids into list");
        List<String> groupIds = groups.stream().map(UpgradeGroup::getGroupId).collect(Collectors.toList());

        log.info("Verifying stores have primary scales");
        List<ScaleDevice> storesWithPrimaries = deviceRepo.findByIsPrimaryScale(true);
        Set<ScaleDevice> uniqueStores = new HashSet<>(storesWithPrimaries);
        Set<ScaleDevice> storesToCompare = new HashSet<>(deviceRepo.findStoreIdByGroupId(groupIds));
        List<ScaleDevice> verifiedStores = new ArrayList<>();
        for (ScaleDevice withPrimary : uniqueStores) {
            for (ScaleDevice toCompare : storesToCompare) {
                if (withPrimary.getStoreId().equals(toCompare.getStoreId())) {
                    verifiedStores.add(toCompare);
                }
            }
        }
        Set<ScaleDevice> verifiedSet = new HashSet<>(verifiedStores);
        storesToCompare.removeAll(verifiedSet);
        if (!verifiedSet.isEmpty() && !storesToCompare.isEmpty()) {
            log.error("Some stores do not contain primary scales.");
            result.setErrorDescription("Some stores do not contain primary scales.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        log.info("Checking if any batches exist");
        if (upgradeBatchRepo.count() == 0) {
            log.error("No batches found. At least one batch is required to enable groups");
            result.setErrorDescription("No batches found. At least one batch is required to enable groups");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        log.info("Saving enabled groups");
        for (UpgradeGroup group : groups) {
            group.setStatus(UpgradeGroup.Status.ENABLED.name());
        }
        upgradeGroupRepo.saveAll(groups);
        log.info("Finish Enabling Provided Groups");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Get the groups from the database", description = "All groups in the database will be retrieved and returned.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Groups successfully retrieved.")
    })
    @GetMapping(path = "/getGroups", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getGroups() {
        log.info("Begin Get Groups");

        List<UpgradeGroup> groups = new ArrayList<>();
        upgradeGroupRepo.findAll().forEach(groups::add);
        List<UpgradeDevice> devices = new ArrayList<>();
        upgradeDeviceRepo.findAll().forEach(devices::add);
        log.debug("Devices found:" + devices);
        devices.forEach(device -> {
            UpgradeGroup foundGroup = groups.stream().filter(group -> device.getGroupId().equals(group.getGroupId())).findFirst().get();
            ScaleDevice foundScale = deviceRepo.findById(device.getDeviceId()).get();
            device.assignFields(foundScale);

            if (foundGroup.getScales() == null) {
                foundGroup.setScales(new ArrayList<>());
            }
            if (device.getGroupId().equals(foundGroup.getGroupId())) {
                foundGroup.getScales().add(device);
            }
        });

        HashMap<String, UpgradeGroup> groupMap = new HashMap<>();
        for (UpgradeGroup group : groups) {
            groupMap.put(group.getGroupId(), group);
        }

        log.info("Finish Get Groups");
        return new ResponseEntity<>(groupMap, HttpStatus.OK);
    }

    @Operation(summary = "Deletes the group by provided ID", description = "The group assigned the provided ID will be deleted.")
    @Parameters({
            @Parameter(name = "groupId", description = "The ID of the group to delete", in = ParameterIn.QUERY, example = "0000aaaa-00aa-00aa-00aa-000000aaaaaa", schema = @Schema(implementation = String.class))
    })
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "The group was deleted successfully"),
            @ApiResponse(responseCode = "400", description = "The group ID provided is not useful"),
            @ApiResponse(responseCode = "409", description = "A batch is assigned to at least one upgrade device"),
            @ApiResponse(responseCode = "500", description = "A database error occurred")
    })
    @DeleteMapping(path = "/deleteGroup", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> deleteGroup(@RequestParam(name = "groupId", required = false) String groupId) {
        log.info("Begin Delete Group");
        OperationStatusModel result = new OperationStatusModel("DeleteGroup");

        log.info("Verify provided group ID");
        if (HteTools.isNullorEmpty(groupId)) {
            log.error("Provided group ID does not exist. Please provide a real group ID.");
            result.setErrorDescription("Provided group ID does not exist. Please provide a real group ID.");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        if (!DomainHelper.isValidUUID(groupId)) {
            log.error("Invalid group ID provided. Please provide a valid group ID: {}", groupId);
            result.setErrorDescription("Invalid group ID provided. Please provide a valid group ID: " + groupId);
            result.setResult(RequestOperationResult.ERROR.name());
            result.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        if (!upgradeGroupRepo.findById(groupId).isPresent()) {
            log.error("Database cannot find group with ID: {}", groupId);
            result.setErrorDescription("Database cannot find group with ID: " + groupId);
            result.setResult(RequestOperationResult.ERROR.name());
            result.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        try {
            List<UpgradeDevice> devices = upgradeDeviceRepo.findAllByGroupId(groupId);
            if (devices != null && !devices.isEmpty()) {
                if (devices.stream().anyMatch(device -> !HteTools.isNullorEmpty(device.getBatchId()))) {
                    log.error("Some or all devices in the group are assigned a batch. Please remove this assignment.");
                    result.setErrorDescription("Some or all devices in the group are assigned a batch. Please remove this assignment.");
                    result.setResult(RequestOperationResult.ERROR.name());
                    result.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
                    return new ResponseEntity<>(result, HttpStatus.CONFLICT);
                }
                devices.forEach(device -> upgradeDeviceRepo.deleteById(device.getUpgradeId()));
            }

            upgradeGroupRepo.deleteById(groupId);
        } catch (Exception e) {
            log.error("Error deleting group with ID: {}", groupId);
            result.setErrorDescription("Error deleting group with ID: " + groupId);
            result.setResult(RequestOperationResult.ERROR.name());
            result.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Finish Delete Group");
        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        return new ResponseEntity<>(result, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Get the devices from the database", description = "Retrieve all upgrade devices from the database")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The upgrade devices were retrieved")
    })
    @GetMapping(path = "/getDevices", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getDevices() {
        log.info("Begin Get Devices");

        OffsetDateTime offset = OffsetDateTime.now(ZoneId.systemDefault());
        ZoneOffset zoneOffset = offset.getOffset();
        int offsetValue = zoneOffset.toString().equals("Z") ? 0 : Integer.parseInt(zoneOffset.toString().split(":")[0]);

        Iterable<UpgradeDevice> devices = upgradeDeviceRepo.findAll();
        HashMap<String, UpgradeDevice> deviceMap = new HashMap<>();
        devices.forEach(device -> {
            ScaleDevice foundScale = deviceRepo.findById(device.getDeviceId()).get();
            device.assignFields(foundScale);

            if (device.getLastUpdate() != null) {
                Timestamp newLastUpdate = convertToLocal(offsetValue, device.getLastUpdate());
                device.setLastUpdate(newLastUpdate);
            }
            deviceMap.put(device.getUpgradeId(), device);
        });

        log.info("Finish Get Devices");
        return new ResponseEntity<>(deviceMap, HttpStatus.OK);
    }

    @Operation(summary = "Get info about the selected scales for upgrading", description = "Provides helpful information about each provided upgrade device to see if it is coming along in the upgrade process")
    @Parameters({
            @Parameter(name="devices", description="The IDs of the upgrade devices", in=ParameterIn.QUERY, example="0000aaaa-00aa-00aa-00aa-000000aaaaaa but in a List"),
            @Parameter(name="batchUpgrade", description="Flag to say if request is for batch or async upgrading", in=ParameterIn.QUERY, example="false", schema=@Schema(implementation=Boolean.class))
    })
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The info about the provided scales is returned"),
            @ApiResponse(responseCode = "400", description = "The provided list scale IDs does not exist or is empty")
    })
    @GetMapping(path="/utilitiesInfo", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getInfo(@RequestParam(name="devices", required=false) List<String> devices, @RequestParam(name="batchUpgrade", required=false) Boolean batchUpgrade) {
        OperationStatusModel result = new OperationStatusModel("GetInfo");
        log.info("Start gathering information for the selected scales");

        if (devices == null || devices.isEmpty()) {
            log.error("No scales selected to obtain more information on. Please selected scales.");
            result.setErrorDescription("No scales selected to obtain more information about. Please selected scales.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        List<UpgradeDeviceInfo> infos = new ArrayList<>();

        for (String device : devices) {
            UpgradeDevice dbDevice = upgradeDeviceRepo.findById(device).get();
            if (batchUpgrade) {
                if (!HteTools.isNullorEmpty(dbDevice.getBatchId())) {
                    testConnection(infos, dbDevice, true);
                } else {
                    UpgradeDeviceInfo info = new UpgradeDeviceInfo();
                    info.setDeviceId(dbDevice.getDeviceId());
                    infos.add(info);
                }
            } else {
                if (!HteTools.isNullorEmpty(dbDevice.getFileSelected())) {
                    testConnection(infos, dbDevice, false);
                } else {
                    UpgradeDeviceInfo info = new UpgradeDeviceInfo();
                    info.setDeviceId(dbDevice.getDeviceId());
                    infos.add(info);
                }
            }
        }

        log.info("Finished gathering information for the selected scales");
        return new ResponseEntity<>(infos, HttpStatus.OK);
    }
    private void testConnection(List<UpgradeDeviceInfo> infos, UpgradeDevice dbDevice, Boolean batchUpgrade) {
        UpgradeDeviceInfo info = new UpgradeDeviceInfo();
        info.setUpgradeId(dbDevice.getUpgradeId());
        info.setDeviceId(dbDevice.getDeviceId());

        FileForEvent file;
        if (batchUpgrade) {
            UpgradeBatch assignedBatch = upgradeBatchRepo.findById(dbDevice.getBatchId()).get();
            file = fileRepo.findById(assignedBatch.getFileId()).get();
        } else {
            file = fileRepo.findById(dbDevice.getFileSelected()).get();
        }
        info.setTargetFirmware(upgradeHelper.getFileFirmwareVersion(file.getFilename()));
        ScaleDevice sDevice = deviceRepo.findById(dbDevice.getDeviceId()).get();

        Session session = null;
        ChannelSftp channel = null;
        try {
            JSch jsch = new JSch();
            session = jsch.getSession("root", sDevice.getIpAddress(), 22);
            session.setPassword("ho1bart2");
            session.setConfig("StrictHostKeyChecking", "no");
            session.setTimeout(10000);
            session.connect();

            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            Vector<ChannelSftp.LsEntry> folderContents;
            if (HteTools.getIsHTScale(sDevice.getIpAddress()).equals("HT")) {
                folderContents = channel.ls("/usr/local/hobart/upgrade");
            } else {
                folderContents = channel.ls("/opt/hobart/upgrade");
            }

            info.setCanConnect(true);
            for (ChannelSftp.LsEntry folderItem : folderContents) {
                if (folderItem.getFilename().contains(file.getFilename()) || folderItem.getFilename().equals(file.getFilename())) {
                    info.setHasFile(true);
                    break;
                }
                info.setHasFile(false);
            }
        } catch (JSchException e) {
            log.error("Could not connect to scale {}", dbDevice.getDeviceId());
            info.setCanConnect(false);
        } catch (SftpException e) {
            log.error("Could not execute file list command for scale {}", dbDevice.getDeviceId());
        } finally {
            infos.add(info);
            if (session != null && session.isConnected()) {
                session.disconnect();
            }
            if (channel != null && channel.isConnected()) {
                channel.disconnect();
            }
        }
    }

    @Operation(summary = "Assigns the provided scales to the provided batch", description = "Using the provided batch ID and the provided upgrade devices, assigns the batch to those devices")
    @Parameters({
            @Parameter(name = "batchId", description = "The ID of the batch to assign to the devices", in = ParameterIn.QUERY, example = "0000aaaa-00aa-00aa-00aa-000000aaaaaa", schema = @Schema(implementation = String.class)),
    })
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The upgrade devices were assigned the provided batch"),
            @ApiResponse(responseCode = "400", description = "The provided arguments are not useful or the file associated with the batch cannot be assigned to a provided upgrade device"),
            @ApiResponse(responseCode = "409", description = "The containing group of the upgrade devices needs to be enabled first"),
            @ApiResponse(responseCode = "500", description = "The database had an error assigning the batch to the upgrade devices")
    })
    @PostMapping(path = "/updateBatchAssignment", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> updateBatchAssignment(@RequestParam(name = "batchId", required = false) String batchId, @RequestBody List<UpgradeDevice> devices) {
        log.info("Begin Updating Batch Assignments");
        OperationStatusModel result = new OperationStatusModel("UpdateBatchAssignment");

        log.info("Validating request inputs");
        if (devices.isEmpty()) {
            log.error("Provided devices is null or empty, please provide valid devices");
            result.setErrorDescription("Provided devices is null or empty, please provide valid devices");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        List<String> groupIds = new ArrayList<>();
        devices.forEach(device -> groupIds.add(device.getGroupId()));

        List<String> groupsStatuses = upgradeGroupRepo.findStatusByGroupId(groupIds);
        boolean ifEnabled = groupsStatuses.stream().allMatch(status -> status.equals(UpgradeGroup.Status.ENABLED.toString()));

        if (!ifEnabled) {
            log.error("Provided devices cannot be assigned a batch because the containing groups are not enabled.");
            result.setErrorDescription("Provided devices cannot be assigned a batch because the containing groups are not enabled.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        if (HteTools.isNullorEmpty(batchId)) {
            // update batch assignment
            log.info("Updating batch assignments");
            for (UpgradeDevice device : devices) {
                device.setBatchId(null);
                device.setStatusText(null);
            }
        } else {
            if (!DomainHelper.isValidUUID(batchId)) {
                log.error("Provided batchId is not valid, please provide a valid batchId");
                result.setErrorDescription("Provided batchId is not valid, please provide a valid batchId");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
            List<String> deviceModelTypes = devices.stream().map(device -> HteTools.getIsHTScale(device.getIpAddress())).collect(Collectors.toList());
            FileForEvent file = fileRepo.findById(upgradeBatchRepo.findById(batchId).get().getFileId()).get();
            if (file.getFilename().contains(".package.tgz") && deviceModelTypes.contains("GT")) {
                log.error("Cannot assign batch. Incorrect file type for scale: .package.tgz to GT");
                result.setErrorDescription("Cannot assign batch. Incorrect file type for scale: .package.tgz to GT");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            } else if (upgradeHelper.isFileGTType(file.getFilename()) && deviceModelTypes.contains("HT")) {
                log.error("Cannot assign batch. Incorrect file type for scale: (.deb, .tgz, .img.bz2) to HT");
                result.setErrorDescription("Cannot assign batch. Incorrect file type for scale: (.deb, .tgz, .img.bz2) to HT");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            // TODO: uncomment when more defined upgrade paths are available
            // query to get scales firmware versions
//            log.info("Checking if scales' firmware can support the upgrade file");
//            UpgradeBatch batch = upgradeBatchRepo.findById(batchId).get();
//            FileIdAndName batchFilename = fileRepo.findFilenamesByFileIds(Arrays.asList(batch.getFileId())).get(0);
//            String fileFirmwareVersion = upgradeHelper.getFileFirmwareVersion(batchFilename.getFilename());
//            for (UpgradeDevice device : devices) {
//                if (fileFirmwareVersion == null || !upgradeHelper.areFirmwaresCompatible(fileFirmwareVersion, device.getFirmware())) {
//                    log.error("Device {} is not compatible with batch upgrade file", device.getDeviceId());
//                    result.setErrorDescription("A device is not compatible with the batch upgrade file");
//                    result.setResult(RequestOperationResult.ERROR.name());
//                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
//                }
//            }

            // update batch assignment
            log.info("Updating batch assignment");
            for (UpgradeDevice device : devices) {
                device.setBatchId(batchId);
            }
        }

        try {
            log.info("Saving new batch assignments");
            upgradeDeviceRepo.saveAll(devices);
        } catch (Exception e) {
            log.error("Exception saving all updated batch assignments");
            result.setErrorDescription("Exception saving all updated batch assignments");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Finish Updating Batch Assignments");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary="Set upgrade file for async upgrade devices", description="Assigns a file for upgrading to the provided asynchronous upgrade devices")
    @ApiResponses({
            @ApiResponse(responseCode="200", description="The file id was successfully assigned to the provided devices"),
            @ApiResponse(responseCode="400", description="The provided file id string is null or empty or the list of devices is null or empty"),
            @ApiResponse(responseCode="500", description="The database had an error saving the upgrade devices")
    })
    @PostMapping(path="/manualFileAssign", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> asyncFileAssign(@RequestBody List<UpgradeDevice> devices) {
        OperationStatusModel result = new OperationStatusModel("AsyncFileAssign");
        log.info("Begin Async File Assign");

        AssignAsyncUpgradeFile assignAsync = new AssignAsyncUpgradeFile(devices, result, upgradeDeviceRepo);
        assignAsync.execute();

        if (result.getResult() == null) {
            result.setResult(RequestOperationResult.SUCCESS.name());
            result.setErrorDescription("Successfully assigned the file to the devices");
            log.info("Successfully assigned the file to the devices");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, assignAsync.resultStatus);
        }
    }

    @Operation(summary="Manually upgrade the provided devices", description="Sets the batch upgrade start time to now and begins upgrading the provided devices")
    @Parameters({
            @Parameter(name="upgradeFileDelete", description="Tells the devices whether to delete upgrade files or not", in=ParameterIn.QUERY, example="false", schema=@Schema(implementation=Boolean.class))
    })
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The provided devices are now being upgraded"),
            @ApiResponse(responseCode = "400", description = "The provided list of devices does not exist or is empty"),
            @ApiResponse(responseCode = "500", description = "The database had an error saving the batches with the current time to upgrade")
    })
    @PostMapping(path = "/upgradeDevices", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> upgradeDevices(@RequestParam(name="upgradeFileDelete", required=false) Boolean upgradeFileDelete,
                                                               @RequestBody List<String> upgradeIds) {
        OperationStatusModel result = new OperationStatusModel("UpgradeDevices");

        if (upgradeIds == null || upgradeIds.isEmpty()) {
            log.error("No devices provided to be upgraded. Please provide devices to upgrade.");
            result.setErrorDescription("No devices provided to be upgraded. Please provide devices to upgrade.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        List<UpgradeDevice> devices = new ArrayList<>();
        for (String upgradeId : upgradeIds) {
            UpgradeDevice dbDevice = upgradeDeviceRepo.findById(upgradeId).get();
            ScaleDevice scale = deviceRepo.findById(dbDevice.getDeviceId()).get();
            dbDevice.setIpAddress(scale.getIpAddress());
            devices.add(dbDevice);
        }

        try {
            for (UpgradeDevice device : devices) {
                if (!device.getStatus().equals(UpgradeDevice.Status.file_uploaded_awaiting_upgrade.name())) {
                    log.error("Device {} is not in the correct state to initiate an upgrade.", device.getIpAddress());
                    result.setErrorDescription("Device " + device.getIpAddress() + " is not in the correct state to initiate an upgrade.");
                    result.setResult(RequestOperationResult.ERROR.name());
                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                }

                device.setStatus(UpgradeDevice.Status.upgrade_now.name());
            }
            upgradeDeviceRepo.saveAll(devices);
        } catch (Exception e) {
            log.error("Error saving upgrade devices to upgrade now");
            result.setErrorDescription("Error saving upgrade devices to upgrade now");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Manually upload the batch file to the assigned provided devices", description = "Uploads the assigned batch file of the provided upgrade devices right now")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The file has begun to be uploaded to the provided upgrade devices"),
            @ApiResponse(responseCode = "400", description = "The provided list of upgrade devices does not exist or is empty"),
            @ApiResponse(responseCode = "500", description = "The database had an error setting the upload time to right now")
    })
    @PostMapping(path = "/uploadFilesToScales", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> uploadFilesToScales(@RequestBody List<String> upgradeIds) {
        OperationStatusModel result = new OperationStatusModel("UploadFilesToScales");

        if (upgradeIds == null || upgradeIds.isEmpty()) {
            log.error("No devices provided to have files uploaded to. Please provide devices to upload files to.");
            result.setErrorDescription("No devices provided to have files uploaded to. Please provide devices to upload files to.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        List<UpgradeDevice> devices = new ArrayList<>();
        for (String upgradeId : upgradeIds) {
            UpgradeDevice dbDevice = upgradeDeviceRepo.findById(upgradeId).get();
            ScaleDevice scale = deviceRepo.findById(dbDevice.getDeviceId()).get();
            dbDevice.setIpAddress(scale.getIpAddress());
            devices.add(dbDevice);
        }

        Optional<ConfigEntry> op = configRepo.findById(AppConfig.hybridMultipath);
        boolean hybridMultipath = false;
        if (op.isPresent()) {
            hybridMultipath = new Boolean(op.get().getCoValue());
        }

        try {
            for (UpgradeDevice device : devices) {
                if (hybridMultipath) {
                    if (device.isPrimaryScale()) {
                        if (!device.getStatus().equals(UpgradeDevice.Status.awaiting_upload.name())) {
                            log.error("Device {} is not in the correct state to initiate an upload.", device.getIpAddress());
                            result.setErrorDescription("Device " + device.getIpAddress() + " is not in the correct state to initiate an upload.");
                            result.setResult(RequestOperationResult.ERROR.name());
                            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                        }

                        device.setStatus(UpgradeDevice.Status.upload_now.name());
                    } else {
                        if (!device.getStatus().equals(UpgradeDevice.Status.awaiting_primary.name())) {
                            log.error("Device {} is not in the correct state to initiate an upload.", device.getIpAddress());
                            result.setErrorDescription("Device " + device.getIpAddress() + " is not in the correct state to initiate an upload.");
                            result.setResult(RequestOperationResult.ERROR.name());
                            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                        }

                        device.setStatus(UpgradeDevice.Status.upload_now.name());
                    }
                } else {
                    if (!device.getStatus().equals(UpgradeDevice.Status.awaiting_upload.name())) {
                        log.error("Device {} is not in the correct state to initiate an upload.", device.getIpAddress());
                        result.setErrorDescription("Device " + device.getIpAddress() + " is not in the correct state to initiate an upload.");
                        result.setResult(RequestOperationResult.ERROR.name());
                        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                    }

                    device.setStatus(UpgradeDevice.Status.upload_now.name());
                }
            }
            upgradeDeviceRepo.saveAll(devices);
        } catch (Exception e) {
            log.error("Error saving upgrade devices to upload now");
            result.setErrorDescription("Error saving upgrade devices to upload now");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @Operation(summary = "Not Implemented", description = "Need to implement")
    @DeleteMapping(path = "/deleteFiles", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> deleteFiles(@RequestParam(name = "files") List<Object> files) {
        OperationStatusModel result = new OperationStatusModel("DeleteFiles");

        return new ResponseEntity<>(result, HttpStatus.NOT_IMPLEMENTED);
    }

    @Operation(summary = "Create/Update a batch", description = "Creates a batch if it doesn't exist or updates an existing one")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successfully created or saved the batch"),
            @ApiResponse(responseCode = "400", description = "The ID of the provided batch is not valid"),
            @ApiResponse(responseCode = "500", description = "The database had an error saving the batch")
    })
    @PostMapping(path = "/saveBatch", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> saveBatch(@RequestBody UpgradeBatch batch) {
        log.info("Begin Save Batch");
        OperationStatusModel result = new OperationStatusModel("SaveBatch");
        //Time validation
        OffsetDateTime offset = OffsetDateTime.now(ZoneId.systemDefault());
        ZoneOffset zoneOffset = offset.getOffset();
        int offsetValue = zoneOffset.toString().equals("Z") ? 0 : Integer.parseInt(zoneOffset.toString().split(":")[0]);
        Timestamp uploadStart = new Timestamp(batch.getUploadStart().toInstant().plus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
        batch.setUploadStart(uploadStart);
        Timestamp uploadEnd = new Timestamp(batch.getUploadEnd().toInstant().plus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
        batch.setUploadEnd(uploadEnd);
        Timestamp upgradeStart = new Timestamp(batch.getUpgradeStart().toInstant().plus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
        batch.setUpgradeStart(upgradeStart);
        Timestamp upgradeEnd = new Timestamp(batch.getUpgradeEnd().toInstant().plus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
        batch.setUpgradeEnd(upgradeEnd);

        if (uploadStart.getTime() >= uploadEnd.getTime()) {
            log.info("Provided batch times are invalid!");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided batch times are invalid!");
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }
        if (upgradeStart.getTime() >= upgradeEnd.getTime()) {
            log.info("Provided batch times are invalid!");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided batch times are invalid!");
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }
        if (upgradeStart.getTime() < uploadEnd.getTime()) {
            log.info("Provided batch times are invalid!");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided batch times are invalid!");
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        if (HteTools.isNullorEmpty(batch.getBatchId())) {
            batch.setBatchId(UUID.randomUUID().toString());
        }

        if (!DomainHelper.isValidUUID(batch.getBatchId()) || DomainHelper.isDefaultUUID(batch.getBatchId())) {
            result.setErrorDescription("The batch must have a valid UUID");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            domainHelper.saveDomain(batch.getBatchId(), EntityType.BATCH, AppConfig.DefaultAdminId);
            UpgradeBatch savedBatch = upgradeBatchRepo.save(batch);
            result.setResult(RequestOperationResult.SUCCESS.name());
            log.info("Batch saved: {}", savedBatch.getBatchId());
        } catch (Exception e) {
            log.info("Error saving batch: {}", e.getMessage());
            result.setErrorDescription("Error saving batch: " + e.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Finish Save Batch");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Delete the provided batch", description = "Using the provided batch ID, deletes the batch forever, no take backs")
    @Parameters({
            @Parameter(name = "batchId", description = "The ID of the batch to delete", in = ParameterIn.QUERY, example = "0000aaaa-00aa-00aa-00aa-000000aaaaaa", schema = @Schema(implementation = String.class))
    })
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The batch has been deleted"),
            @ApiResponse(responseCode = "404", description = "The batch could not be found with the provided ID"),
            @ApiResponse(responseCode = "409", description = "The batch is currently assigned to upgrade devices and cannot be deleted"),
            @ApiResponse(responseCode = "500", description = "The database had an error deleting the batch")
    })
    @DeleteMapping(path = "/deleteBatch", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> deleteBatch(@RequestParam(name = "batchId", required = false) String batchId) {
        log.info("Begin Delete Batch");
        OperationStatusModel result = new OperationStatusModel("DeleteBatch");

        // check if batch exists in db
        if (!upgradeBatchRepo.findById(batchId).isPresent()) {
            log.info("Provided batch does not exist in database: {}", batchId);
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided batch does not exist in database: " + batchId);
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
        // check if batch is assigned to devices
        if (upgradeBatchRepo.countAssignmentsByBatchId(batchId) > 0) {
            log.info("Provided batch is assigned to devices. Unassign the batch from the devices");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided batch is assigned to devices. Unassign the batch from the devices");
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        try {
            upgradeBatchRepo.deleteById(batchId);
        } catch (Exception e) {
            log.info("Error deleting batch: {}", batchId);
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error deleting batch: " + batchId);
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Finish Delete Batch");
        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Get all the upgrade batches", description = "Retrieves all the upgrade batches from the database")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "All upgrade batches in the database are retrieved")
    })
    @GetMapping(path = "/getBatches", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getBatches() {
        log.info("Begin Get Batches");

        OffsetDateTime offset = OffsetDateTime.now(ZoneId.systemDefault());
        ZoneOffset zoneOffset = offset.getOffset();
        int offsetValue = zoneOffset.toString().equals("Z") ? 0 : Integer.parseInt(zoneOffset.toString().split(":")[0]);

        log.info("Gathering batches from database");
        Iterable<UpgradeBatch> batchesDb = upgradeBatchRepo.findAll();
        List<UpgradeBatch> batches = new ArrayList<>();
        batchesDb.forEach(batches::add);

        log.info("Finding file names by their IDs and adding them to the correct batches");
        List<String> fileIds = batches.stream().map(UpgradeBatch::getFileId).collect(Collectors.toList());
        List<FileIdAndName> idsToNames = fileRepo.findFilenamesByFileIds(fileIds);
        batches.forEach(batch -> {
            FileIdAndName match = idsToNames.stream().filter(idAndName -> idAndName.getFileId().equals(batch.getFileId())).findFirst().get();
            batch.setFileName(match.getFilename());
            Timestamp uploadStart = new Timestamp(batch.getUploadStart().toInstant().minus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
            batch.setUploadStart(uploadStart);
            Timestamp uploadEnd = new Timestamp(batch.getUploadEnd().toInstant().minus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
            batch.setUploadEnd(uploadEnd);
            Timestamp upgradeStart = new Timestamp(batch.getUpgradeStart().toInstant().minus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
            batch.setUpgradeStart(upgradeStart);
            Timestamp upgradeEnd = new Timestamp(batch.getUpgradeEnd().toInstant().minus(offsetValue, ChronoUnit.HOURS).toEpochMilli());
            batch.setUpgradeEnd(upgradeEnd);
        });

        log.info("Compiling batches for response data");
        HashMap<String, UpgradeBatch> batchMap = new HashMap<>();
        batches.forEach(batch -> batchMap.put(batch.getBatchId(), batch));

        log.info("Finish Get Batches");
        return new ResponseEntity<>(batchMap, HttpStatus.OK);
    }

    @GetMapping(path = "/timeframe", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getTimeframes() {
        log.info("Begin Get Timeframes");
        OperationStatusModel result = new OperationStatusModel("Get Timeframes");

        Timeframe timeframe = new Timeframe();
        try {
            timeframe.setUploadPrimaryStart(getConfigEntry(AppConfig.uploadPrimaryStart, "").getCoValue());
            timeframe.setUploadPrimaryEnd(getConfigEntry(AppConfig.uploadPrimaryEnd, "").getCoValue());
            timeframe.setUploadSecondaryStart(getConfigEntry(AppConfig.uploadSecondaryStart, "").getCoValue());
            timeframe.setUploadSecondaryEnd(getConfigEntry(AppConfig.uploadSecondaryEnd, "").getCoValue());
            timeframe.setUpgradeTimeStart(getConfigEntry(AppConfig.upgradeTimeStart, "").getCoValue());
            timeframe.setUpgradeTimeEnd(getConfigEntry(AppConfig.upgradeTimeEnd, "").getCoValue());
        } catch (Exception e) {
            log.error("Timeframe field does not exist in the database.");
            result.setErrorDescription("Timeframe field does not exist in the database.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Finish Get Timeframes");
        return new ResponseEntity<>(timeframe, HttpStatus.OK);
    }

    @PostMapping(path = "/timeframe", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> setTimeframes(@RequestBody Timeframe timeframe) {
        log.info("Begin Set Timeframes");
        OperationStatusModel result = new OperationStatusModel("Set Timeframes");

        try {
            configRepo.save(new ConfigEntry(AppConfig.uploadPrimaryStart, timeframe.getUploadPrimaryStart()));
            configRepo.save(new ConfigEntry(AppConfig.uploadPrimaryEnd, timeframe.getUploadPrimaryEnd()));
            configRepo.save(new ConfigEntry(AppConfig.uploadSecondaryStart, timeframe.getUploadSecondaryStart()));
            configRepo.save(new ConfigEntry(AppConfig.uploadSecondaryEnd, timeframe.getUploadSecondaryEnd()));
            configRepo.save(new ConfigEntry(AppConfig.upgradeTimeStart, timeframe.getUpgradeTimeStart()));
            configRepo.save(new ConfigEntry(AppConfig.upgradeTimeEnd, timeframe.getUpgradeTimeEnd()));
        } catch (Exception e) {
            log.error("Exception while trying to save timeframes.");
            result.setErrorDescription("Exception while trying to save timeframes.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Finish Set Timeframes");
        return new ResponseEntity<>(result, HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Uploads a file for asynchronous upgrade",
            description = "Prepares a file on scale(s) to be upgraded at a time of the user's choosing")
    @Parameters({
            @Parameter(name="fileName", description="The file name to upload to the scales", in=ParameterIn.QUERY,
                    example="play_shrek_full_volume.package.tgz", schema=@Schema(implementation=String.class))
    })
    @PostMapping(path = "/uploadManualUpgradeFile",
            produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> uploadAsyncUpgradeFile(
            @RequestParam(name = "fileName", required = true) String fileName,
            @RequestBody List<String> ipAddresses) {

        UploadAsyncUpgradeEndpoint endpoint = new UploadAsyncUpgradeEndpoint(deviceRepo, upgradeDeviceRepo);
        Map<String, Boolean> postUploadFileStatuses = endpoint.execute(ipAddresses, fileName);

        boolean atLeastOneScaleFailed = false;
        for (Map.Entry<String, Boolean> data : postUploadFileStatuses.entrySet()) {
            if (data.getValue() == false) { // One of the uploads didn't turn out. Womp womp.
                atLeastOneScaleFailed = true;
                break;
            }
        }

        if (atLeastOneScaleFailed) {
            return new ResponseEntity<>(postUploadFileStatuses, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(postUploadFileStatuses, HttpStatus.OK);
        }
    }

    @Operation(summary = "Applies a file for asynchronous upgrade",
            description = "Applies upgrade file on scale(s); spawns a thread to check on the upgrade later")
    @Parameters({
            @Parameter(name="fileName", description="The file name to upgrade with", in=ParameterIn.QUERY,
                    example="scale_destroyer.package.tgz", schema=@Schema(implementation=String.class)),
            @Parameter(name="upgradeFileDelete", description="Tells devices whether or not to delete upgrade files",
                    in=ParameterIn.QUERY, example="true", schema=@Schema(implementation=Boolean.class))
    })
    @PostMapping(path = "/applyManualUpgradeFile",
            produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> applyAsyncUpgradeFile(
            @RequestParam(name = "fileName", required = true) String fileName,
            @RequestParam(name="upgradeFileDelete", required=false) Boolean upgradeFileDelete,
            @RequestBody List<String> ipAddresses) {

        ApplyAsyncUpgradeEndpoint endpoint = new ApplyAsyncUpgradeEndpoint(deviceRepo, upgradeDeviceRepo);
        Map<String, Boolean> postUpgradeStatuses = endpoint.execute(ipAddresses, fileName, upgradeFileDelete);

        boolean atLeastOneScaleFailed = false;
        for (Map.Entry<String, Boolean> data : postUpgradeStatuses.entrySet()) {
            ScaleDevice scale = deviceRepo.findByIpAddress(data.getKey());
            String deviceID = scale.getDeviceId();
            if (data.getValue() == false) { // This should only occur this early if the file mysteriously disappeared.
                // See ApplyAsyncUpgradeEndpoint comments for more information.
                atLeastOneScaleFailed = true;
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_failed.name(), "Upgrade was not successfully applied.", deviceID);
            } else if (data.getValue() == true) {
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_in_progress.name(), "Upgrade is being applied to the scale.", deviceID);
            }
        }

        if (atLeastOneScaleFailed) {
            return new ResponseEntity<>(postUpgradeStatuses, HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity<>(postUpgradeStatuses, HttpStatus.OK);
        }
    }


    private ConfigEntry getConfigEntry(String key, String value) {
        return configRepo.findById(key).orElse(new ConfigEntry(key, value));
    }

    private Timestamp convertToUTC(int offset, Timestamp localToConvert) {
        if (offset < 0) {
            return new Timestamp(localToConvert.toInstant().plus(offset, ChronoUnit.HOURS).toEpochMilli());
        } else {
            return new Timestamp(localToConvert.toInstant().minus(offset, ChronoUnit.HOURS).toEpochMilli());
        }
    }

    private Timestamp convertToLocal(int offset, Timestamp utcToConvert) {
        if (offset < 0) {
            return new Timestamp(utcToConvert.toInstant().minus(offset, ChronoUnit.HOURS).toEpochMilli());
        } else {
            return new Timestamp(utcToConvert.toInstant().plus(offset, ChronoUnit.HOURS).toEpochMilli());
        }
    }
}

