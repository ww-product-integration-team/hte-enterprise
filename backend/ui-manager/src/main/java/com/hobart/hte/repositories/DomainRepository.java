package com.hobart.hte.repositories;

import com.hobart.hte.utils.access.HTeDomain;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DomainRepository extends CrudRepository<HTeDomain, String> {
    HTeDomain findByDomainId(String id);

    List<HTeDomain> findByParentId(String parentId);

    HTeDomain findByDomainIdAndTypeNot(String id, String type);

    List<HTeDomain> findByParentIdAndTypeNot(String parentId, String type);

    @Query(value="SELECT * FROM hte.hteDomain WHERE type NOT IN (:type)", nativeQuery=true)
    List<HTeDomain> findByTypeNotIn(@Param("type") List<String> type);
}
