package com.hobart.hte.access;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hobart.hte.helpers.DomainHelper;
import com.hobart.hte.helpers.RoleHelper;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


public class DomainAuthorizationFilter extends OncePerRequestFilter {
    public static String TARGET_HEADER = "TargetDomain";
    public static String ACTION_HEADER = "Action";
    public static String TARGET_SUBSTRING = "Target ";
    public static String DOMAIN_TOKEN_SUBSTRING = "DomainId: ";
    public static String ACCESS_LVL_TOKEN_SUBSTRING = "AccessLvl: ";
    public static String USER_ENABLED_SUBSTRING = "Enabled: ";


    private DomainHelper domainHelper;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if(domainHelper==null){ //This class type can't use injection, so do a lazy set
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            assert webApplicationContext != null;
            domainHelper = webApplicationContext.getBean(DomainHelper.class);
        }

        if(request.getServletPath().equals("/access/login") ||
                request.getServletPath().equals("/access/refreshToken") ||
                request.getServletPath().equals("/access/license") ||
                request.getServletPath().equals("/ui/test")){
            filterChain.doFilter(request, response);
        }
        else{
            String targetDomainHeader = request.getHeader(TARGET_HEADER);
            String authorizationHeader = request.getHeader(AUTHORIZATION);
            if(targetDomainHeader != null && targetDomainHeader.startsWith(TARGET_SUBSTRING) &&
                    authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
                try {
                    String targetDomainId = targetDomainHeader.substring(TARGET_SUBSTRING.length());
                    if(!DomainHelper.isValidUUID(targetDomainId)){
                        //If not valid UUID, default to unassigned
                        logger.info("Invalid Target Domain Id: " + targetDomainId + ", setting to default");
                        targetDomainId = AppConfig.DefaultUnassignedId;
                    }

                    String userDomainId = ""; //DomainId saved inside of authorities object

                    //Get Authorizations
                    Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
                    //logger.info(authorities);
                    for(GrantedAuthority auth : authorities){
                        String authority = auth.getAuthority();
                        if(authority.startsWith(DOMAIN_TOKEN_SUBSTRING)){
                            userDomainId = authority.substring(DOMAIN_TOKEN_SUBSTRING.length());
                        }
                        else if(authority.equals(RoleHelper.MANUAL_OVERRIDE)){
                            //==================================================================================
                            //If the user has the MANUAL_OVERRIDE role, then ignore domain permission
                            logger.info("User has Manual Override Permission");
                            filterChain.doFilter(request, response);
                            return;
                        }
                    }

                    //Found user domain ID, now check if they have appropriate domain to edit target
                    if(!domainHelper.hasDomainPermission(userDomainId, targetDomainId)){
                        throw new SecurityException("User does not have appropriate privileges to modify domain");
                    }

                    filterChain.doFilter(request, response);
                }
                catch(Exception e){
                    logger.error("ERROR Authenticating Domain: " + e.getMessage());

                    response.setStatus(FORBIDDEN.value());
                    OperationStatusModel error = new OperationStatusModel(request.getMethod());
                    error.setResult(RequestOperationResult.ERROR.name());
                    error.setErrorDescription("You do not have the required domain to make that change");

                    response.setContentType(APPLICATION_JSON_VALUE);
                    new ObjectMapper().writeValue(response.getOutputStream(), error);
                }
            }
            else{
                SecurityContextHolder.getContext().setAuthentication(null);
                filterChain.doFilter(request, response);
            }
        }
    }
}
