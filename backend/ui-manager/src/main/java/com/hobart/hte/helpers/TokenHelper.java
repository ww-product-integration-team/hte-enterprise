package com.hobart.hte.helpers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.hobart.hte.access.DomainAuthorizationFilter;
import com.hobart.hte.utils.access.HTeUser;
import com.hobart.hte.utils.config.AppConfig;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import com.hobart.hte.utils.customer.Customer;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

public class TokenHelper {
    public static String secret = "SecretHobartStringForHTe";
    Algorithm algorithm = Algorithm.HMAC256(secret);
    private HTeUser hteUser;
    private final HttpServletRequest request;
    private User user;
    private Customer customer;

    public TokenHelper(HTeUser user, HttpServletRequest request) {
        this.hteUser = user;
        this.request = request;
    }

    public TokenHelper(User user, HttpServletRequest request) {
        this.request = request;
        this.user = user;
    }

    public TokenHelper(Customer customer, HttpServletRequest request) {
        this.request = request;
        this.customer = customer;
    }

    public Collection<GrantedAuthority> generateAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        if (hteUser.getDomain() != null && DomainHelper.isValidUUID(hteUser.getDomain().getDomainId())) {
            // If user has a valid DomainId
            authorities.add(new SimpleGrantedAuthority(
                    DomainAuthorizationFilter.DOMAIN_TOKEN_SUBSTRING + hteUser.getDomain().getDomainId()));
        } else {
            // If domain is invalid set their domain to unassigned
            authorities.add(new SimpleGrantedAuthority(
                    DomainAuthorizationFilter.DOMAIN_TOKEN_SUBSTRING + AppConfig.DefaultUnassignedId));
        }

        Integer accessLevel = 0;
        if (hteUser.getAccessLevel() != null) {
            accessLevel = hteUser.getAccessLevel();
        }
        authorities.add(new SimpleGrantedAuthority(
                DomainAuthorizationFilter.ACCESS_LVL_TOKEN_SUBSTRING + accessLevel.toString()));

        Boolean userEnable = false;
        if (hteUser.getEnabled() != null) {
            userEnable = hteUser.getEnabled();
        }
        authorities.add(new SimpleGrantedAuthority(
                DomainAuthorizationFilter.USER_ENABLED_SUBSTRING + userEnable.toString()));

        hteUser.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });
        return authorities;
    }

    public String createAccessToken() {
        Collection<GrantedAuthority> authorities;
        String username;

        if (hteUser != null) {
            authorities = generateAuthorities();
            username = hteUser.getUsername();
        } else {
            authorities = user.getAuthorities();
            username = user.getUsername();
        }
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(new Date(System.currentTimeMillis() + 60 * 60000)) // 60 Minutes
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles",
                        authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(algorithm);

    }

    public String createRefreshToken() {
        Collection<GrantedAuthority> authorities;
        String username;

        if (hteUser != null) {
            authorities = generateAuthorities();
            username = hteUser.getUsername();
        } else {
            authorities = user.getAuthorities();
            username = user.getUsername();
        }
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(new Date(System.currentTimeMillis() + 180 * 60000)) // 3 Hours
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles",
                        authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(algorithm);

    }

    public String createCustomerAccessToken() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(
                "AccessLvl: 4000"));
        authorities.add(new SimpleGrantedAuthority(
                "DomainId: 87588758-8758-8758-8758-875887588758"));
        authorities.add(new SimpleGrantedAuthority(
                "Enabled: true"));

        return JWT.create()
                .withSubject("scott.rocke@itwfeg.com")
                .withExpiresAt(new Date(System.currentTimeMillis() + 60 * 60000)) // 1 Hour
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles",
                        authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(algorithm);
    }

    public String createCustomerRefreshToken() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(
                DomainAuthorizationFilter.DOMAIN_TOKEN_SUBSTRING + "1234"));

        return JWT.create()
                .withSubject(customer.customerUUID)
                .withExpiresAt(new Date(System.currentTimeMillis() + 180 * 60000)) // 3 Hours
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles",
                        authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(algorithm);

    }

}
