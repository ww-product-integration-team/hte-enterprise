package com.hobart.hte.access;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hobart.hte.helpers.RoleHelper;
import com.hobart.hte.utils.access.HTeRole;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import org.springframework.data.util.Pair;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

import static com.hobart.hte.access.DomainAuthorizationFilter.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class RoleAuthorizationFilter extends OncePerRequestFilter {

    private RoleHelper roleHelper;

    private void checkForRequiredRole(Pair<Integer, String> role, FilterChain filterChain, HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        if (role.getSecond() != "API_USER") {
            HTeRole requiredRole = roleHelper.getRoleByName(role.getSecond());
            if (requiredRole == null) {

                logger.error("Required role was null! " + request.getServletPath());
                response.setStatus(FORBIDDEN.value());
                response.setContentType(APPLICATION_JSON_VALUE);

                OperationStatusModel error = new OperationStatusModel(request.getMethod());
                error.setResult(RequestOperationResult.ERROR.name());
                error.setErrorDescription("Null Exception: User does not have permission to perform that action");
                new ObjectMapper().writeValue(response.getOutputStream(), error);
                return;
            }

            int userAccessLevel = 0;

            Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication()
                    .getAuthorities();
            for (GrantedAuthority auth : authorities) {
                String authority = auth.getAuthority();
                if (authority.startsWith(ACCESS_LVL_TOKEN_SUBSTRING)) {
                    userAccessLevel = Integer.parseInt(authority.substring(ACCESS_LVL_TOKEN_SUBSTRING.length()));

                    // ==================================================================================
                    // First check if the user has the appropriate access level for the request,
                    // If so allow access
                    if (userAccessLevel > requiredRole.getId()) {
                        // logger.info("User has required access level: " + userAccessLevel);
                        filterChain.doFilter(request, response);
                        return;
                    }

                } else if (authority.equals(requiredRole.getName())) {
                    // ==================================================================================
                    // If the user has an individual role assigned to them for this action then
                    // validate
                    logger.info("User has required role: " + requiredRole.getName());
                    filterChain.doFilter(request, response);
                    return;
                } else if (authority.equals(RoleHelper.MANUAL_OVERRIDE.getSecond())) {
                    // ==================================================================================
                    // If the user has the MANUAL_OVERRIDE role, validate immediately
                    logger.info("User has Manual Override Permission: " + requiredRole.getName());
                    filterChain.doFilter(request, response);
                    return;
                }
            }

            // No matching role or access level found.
            response.setStatus(FORBIDDEN.value());
            response.setContentType(APPLICATION_JSON_VALUE);

            OperationStatusModel error = new OperationStatusModel(request.getMethod());
            error.setResult(RequestOperationResult.ERROR.name());
            error.setErrorDescription("You do not have permission to perform that action");
            new ObjectMapper().writeValue(response.getOutputStream(), error);
        } else {
            logger.info("User has Manual Override Permission: API");
            filterChain.doFilter(request, response);
        }

    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if (roleHelper == null) { // This class type can't use injection, so do a lazy set
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils
                    .getWebApplicationContext(servletContext);
            assert webApplicationContext != null;
            roleHelper = webApplicationContext.getBean(RoleHelper.class);
        }

        String authorizationHeader = request.getHeader(AUTHORIZATION);
        // String actionHeader = request.getHeader(ACTION_HEADER);

        // Paths that don't require authentication
        if (request.getServletPath().equals("/access/login") ||
                request.getServletPath().equals("/access/refreshToken") ||
                request.getServletPath().equals("/access/license") ||
                request.getServletPath().equals("/ui/disableProfileUpdateStatus") ||
                request.getServletPath().equals("/ui/profileUpdatedStatus") ||
                request.getServletPath().equals("/ui/saveChecksumLogs") ||
                request.getServletPath().equals("/ui/test")) {
            filterChain.doFilter(request, response);
        } else {

            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
                try {
                    String servletPath = request.getServletPath();
                    String requestMethod = request.getMethod();

                    switch (servletPath) {
                        case "/ui/banner":
                        case "/ui/region":
                        case "/ui/store":
                        case "/ui/dept":
                        case "/ui/device":
                        case "/ui/devices":
                        case "/ui/dashboard":
                        case "/ui/scaletreeview":
                        case "/ui/fetchLazyTree":
                        case "/ui/fetchFilterPart":
                        case "/ui/refreshFilterTree":
                        case "/ui/instantiateNodeStatus":
                        case "/ui/deviceSettings":
                        case "/ui/filter":
                        case "/ui/filteredAssets":
                        case "/ui/appHook":
                        case "/ui/scaleDevice":
                        case "/ui/scale":
                        case "/ui/getSyncStatus":
                        case "/stats/devicesByFirmware":
                        case "/stats/devicesByModel":
                        case "/ui/nodeStatus":
                        case "/ui/getNodeStatusTimers":
                            // UI Controller
                            // ==========================================================================================
                            switch (requestMethod) {
                                case "GET":
                                    checkForRequiredRole(RoleHelper.MONITOR, filterChain, request, response);
                                    break;
                                // filterChain.doFilter(request, response);
                                case "POST":
                                case "DELETE":
                                default:
                                    checkForRequiredRole(RoleHelper.FULL_TREEVIEW, filterChain, request, response);
                                    break;
                            }
                            break;
                        // Import Existing Tree View (and some other asset-related endpoints)
                        // ==============================================================================================
//                        case "/ui/disableAssignmentRules":
//                        case "/ui/enableAssignmentRules":
                        case "/ui/importCSVToAutoAssigner":
                        case "/ui/postAutoAssignerRule":
                        case "/ui/deleteAutoAssignerRule":
                        case "/ui/autoAssignRules":
                        case "/ui/importCSVToTree":
                        case "/ui/importTreeView":
                        case "/ui/downloadAssignmentRules":
                        case "/ui/pushOneOffFeature":
                            checkForRequiredRole(RoleHelper.IMPORT_TREE, filterChain, request, response);
                            break;
                        case "/ui/dept/assignProfile":
                            checkForRequiredRole(RoleHelper.ASSIGN_DEPT_PROFILES, filterChain, request, response);
                            break;
                        case "/ui/dept/assign":
                        case "/ui/dept/remove":
                            checkForRequiredRole(RoleHelper.FULL_TREEVIEW, filterChain, request, response);
                            break;

                        // Modify Scale
                        // ==============================================================================================
                        case "/ui/device/assignProfile":
                            checkForRequiredRole(RoleHelper.ASSIGN_SCALE_PROFILES, filterChain, request, response);
                            break;
                        case "/ui/device/assignScale":
                            checkForRequiredRole(RoleHelper.ASSIGN_SCALES, filterChain, request, response);
                            break;
                        case "/ui/device/setPrimary":
                            checkForRequiredRole(RoleHelper.SET_PRIMARY_SCALE, filterChain, request, response);
                            break;
                        case "/ui/enablescale":
                        case "/ui/store/enable":
                            checkForRequiredRole(RoleHelper.ENABLE_DISABLE, filterChain, request, response);
                            break;

                        // Scale Commands
                        // ==============================================================================================
                        case "/scale/executemanualheartbeat":
                            checkForRequiredRole(RoleHelper.HEARTBEAT, filterChain, request, response);
                            break;
                        case "/scale/getstatus":
                            checkForRequiredRole(RoleHelper.PING, filterChain, request, response);
                            break;
                        case "/scale/rebootdevice":
                            checkForRequiredRole(RoleHelper.REBOOT, filterChain, request, response);
                            break;
                        case "/scale/deletedevicedata":
                        case "/ui/deleteAllAssets":
                            checkForRequiredRole(RoleHelper.DELETE_SCALE_DATA, filterChain, request, response);
                            break;

                        //Profiles
                        //==============================================================================================
                        case "/ui/getChecksumLogs":
                        case "/ui/profile":
                            checkForRequiredRole(RoleHelper.MANAGE_PROFILES, filterChain, request, response);
                            break;

                        // File Manager
                        // ==============================================================================================
                        case "/filemgr/delete":
                        case "/filemgr/update":
                        case "/filemgr/upload":
                            checkForRequiredRole(RoleHelper.FILE_MANAGER, filterChain, request, response);
                            break;
                        case "/filemgr/download":
                        case "/filemgr/getFileInfo":
                        case "/filemgr/getImage":
                            checkForRequiredRole(RoleHelper.FILE_VIEWER, filterChain, request, response);
                            break;

                        // Access
                        // ==============================================================================================
                        case "/access/roles":
                        case "/access/initializeRolesAndDomains":
                            filterChain.doFilter(request, response);
                            break;
                        case "/access/domains":
                        case "/access/user":
                        case "/access/assignDomain":
                        case "/access/assignRole":
                        case "/access/license":
                            switch (requestMethod) {
                                case "GET": // Everyone can see the available roles
                                    filterChain.doFilter(request, response);
                                    break;
                                case "POST":
                                case "DELETE":
                                default: // User should never be allowed to modify roles
                                    checkForRequiredRole(RoleHelper.MANAGE_OPERATORS, filterChain, request, response);
                                    break;
                            }
                            break;
                        case "/access/user/enable":
                            checkForRequiredRole(RoleHelper.USER_ENABLEDISABLE, filterChain, request, response);
                            break;
                        case "/access/user/changePassword":
                            String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal()
                                    .toString();
                            String requestedUser = request.getParameter("username");
                            // Users can change their own password but will need the "Manage supervisors"
                            // role to change others
                            if (loggedInUser.equals(requestedUser)) {
                                filterChain.doFilter(request, response);
                                break;
                            } else {
                                checkForRequiredRole(RoleHelper.MANAGE_SUPERVISORS, filterChain, request, response);
                                break;
                            }
                        case "/filemgr/log":
                        case "/filemgr/filenameInProfiles":
                            filterChain.doFilter(request, response);
                            break;

                        // CONFIG
                        // =======================================================================================
                        case "/config/deleteEvent":
                        case "/config/offlineAwayHours":
                        case "/config/misc":
                            checkForRequiredRole(RoleHelper.SYSTEM_MANAGER, filterChain, request, response);
                            break;
                        case "/config/services":
                            filterChain.doFilter(request, response);
                            break;

                        // Socket
                        // ======================================================================================
                        case "/ws-message/info":
                        case "/socket/fleetStatus":
                        case "/socket/fleetStatus/cancel":
                            checkForRequiredRole(RoleHelper.FLEET_STATUS, filterChain, request, response);
                            break;

                        // LOGS
                        // =========================================================================================
                        case "/events/deleteEvents":
                        case "/events/updateEvents":
                        case "/events/getEvents": // TODO: add new role
                            checkForRequiredRole(RoleHelper.MONITOR, filterChain, request, response);
                            break;

                        case "/transservice/uploadTransactions":
                        case "/transservice/excelExport":
                        case "/transservice/fetchTransactions":
                        case "/transservice/getProds":
                        case "/transservice/postProd":
                            checkForRequiredRole(RoleHelper.DOWNLOAD_TRANS, filterChain, request, response);
                            break;

                        // UPGRADES
                        // =====================================================================================
                        case "/upgrade/upgradeDevices":
                        case "/upgrade/uploadFilesToScales":
                        case "/upgrade/timeframe":
                        case "/upgrade/export":
                        case "/upgrade/uploadFile":
                        case "/upgrade/enableGroups":
                        case "/upgrade/saveGroup":
                        case "/upgrade/getGroups":
                        case "/upgrade/getDevices":
                        case "/upgrade/utilitiesInfo":
                        case "/upgrade/updateBatchAssignment":
                        case "/upgrade/deleteFiles":
                        case "/upgrade/saveBatch":
                        case "/upgrade/getBatches":
                        case "/upgrade/deleteBatch":
                        case "/upgrade/deleteGroup":
                        case "/upgrade/manualFileAssign":
                        case "/upgrade/uploadManualUpgradeFile":
                        case "/upgrade/applyManualUpgradeFile":
                            checkForRequiredRole(RoleHelper.UPGRADE_DEVICES, filterChain, request, response);
                            break;

                        // API
                        case "/licensing/list":
                        case "/licensing/download":
                            checkForRequiredRole(RoleHelper.FULL_TREEVIEW, filterChain, request, response);
                            break;

                        // Zone Pricing
                        case "/pricing/getPricingZones":
                        case "/pricing/saveZone":
                        case "/pricing/deleteZone":
                            checkForRequiredRole(RoleHelper.MANAGE_PROFILES, filterChain, request, response);
                            break;

                        default:
                            // Fail the Check
                            checkForRequiredRole(null, filterChain, request, response);
                            break;
                    }

                    // If code reaches this point that means no appropriate access level or role
                    // could be found
                    // throw new IOException("User does not have permission to perform that
                    // action");
                } catch (Exception e) {
                    logger.error("ERROR Authenticating Role Permission: " + e.getMessage());

                    response.setStatus(FORBIDDEN.value());
                    response.setContentType(APPLICATION_JSON_VALUE);

                    OperationStatusModel error = new OperationStatusModel(request.getMethod());
                    error.setResult(RequestOperationResult.ERROR.name());
                    error.setErrorDescription("Error Authenticating Role Permission: " + e.getMessage());
                    new ObjectMapper().writeValue(response.getOutputStream(), error);
                }
            } else {
                SecurityContextHolder.getContext().setAuthentication(null);
                filterChain.doFilter(request, response);
            }
        }
    }
}
