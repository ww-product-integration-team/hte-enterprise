package com.hobart.hte.event;

import com.hobart.hte.repositories.EventLogRepository;
import com.hobart.hte.repositories.EventRulesRepository;
import com.hobart.hte.utils.event.EventLogEntry;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.model.RequestOperationType;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/events")
@CrossOrigin(origins = "*", allowedHeaders = "*") // allows react to reach service correctly
public class EventController {
    private static final Logger log = LogManager.getLogger(EventController.class);

    @Autowired
    EventLogRepository eventLogRepo;
    @Autowired
    EventRulesRepository eventRulesRepo;
    @Autowired
    SimpMessagingTemplate template;

    @Operation(summary = "Retrieve events either by timestamp range or entityId or eventId or any combination of the three", description = "Retrieve events from the database either by timestamp range or entityId or eventId or any combination")
    @Parameters({
            @Parameter(name = "timestampStart", description = "The beginning timestamp to gather events between, inclusively", in = ParameterIn.QUERY, example = "2023-01-01T04:54:33"),
            @Parameter(name = "timestampEnd", description = "The end timestamp to gather events between, inclusively", in = ParameterIn.QUERY, example = "2023-01-10T12:26:01"),
            @Parameter(name = "entityId", description = "The id of the specific entity to grab", in = ParameterIn.QUERY, example = "0000aaaa-00aa-00aa-00aa-000000aaaaaa"),
            @Parameter(name = "eventId", description = "The id of the specific event to grab", in = ParameterIn.QUERY, example = "1001")
    })
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Events retrieved"),
            @ApiResponse(responseCode = "400", description = "Timestamp values not ordered correctly")
    })
    @GetMapping(path = "/getEvents", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getEvents(@RequestParam(name = "timestampStart", required = false) String timestampStart, @RequestParam(name = "timestampEnd", required = false) String timestampEnd,
                                       @RequestParam(name = "entityId", required = false) String entityId, @RequestParam(name = "eventId", required = false) Integer eventId) {
        List<EventLogEntry> entries = new ArrayList<>();

        if (checkTimestamp(timestampStart) && checkTimestamp(timestampEnd) && !makeTimestamp(timestampStart).after(makeTimestamp(timestampEnd))) {
            OperationStatusModel model = new OperationStatusModel("GetEvents");
            model.setResult(RequestOperationResult.ERROR.name());
            model.setErrorDescription("Starting timestamp cannot be after ending timestamp");
            return new ResponseEntity<>(model, HttpStatus.BAD_REQUEST);
        }

        if (checkTimestamp(timestampStart) && checkTimestamp(timestampEnd) && checkEntityId(entityId) && eventId != null) {
            log.debug("Retrieving events in timestamp range {}:{} and for entityId {} and for eventId {}", timestampStart, timestampEnd, entityId, eventId);
            // Convert strings into Timestamps so data can be compared correctly
            entries = eventLogRepo.findByTimestampBetweenAndEntityIdAndEventId(makeTimestamp(timestampStart), makeTimestamp(timestampEnd), entityId, eventId);
        } else if (checkTimestamp(timestampStart) && checkTimestamp(timestampEnd) && checkEntityId(entityId)) {
            log.debug("Retrieving events in timestamp range {}:{} and for entity {}", timestampStart, timestampEnd, entityId);
            entries = eventLogRepo.findByTimestampBetweenAndEntityId(makeTimestamp(timestampStart), makeTimestamp(timestampEnd), entityId);
        } else if (checkTimestamp(timestampStart) && checkTimestamp(timestampEnd) && eventId != null) {
            log.debug("Retrieving events in timestamp range {}:{} and for event {}", timestampStart, timestampEnd, eventId);
            entries = eventLogRepo.findByTimestampBetweenAndEventId(makeTimestamp(timestampStart), makeTimestamp(timestampEnd), eventId);
        } else if (checkTimestamp(timestampStart) && checkTimestamp(timestampEnd)) {
            log.debug("Retrieving events in timestamp range {}:{}", timestampStart, timestampEnd);
            entries = eventLogRepo.findByTimestampBetween(makeTimestamp(timestampStart), makeTimestamp(timestampEnd));
        } else if (checkEntityId(entityId) && eventId != null) {
            log.debug("Retrieving events for entityId {} and eventId {}", entityId, eventId);
            entries = eventLogRepo.findByEntityIdAndEventId(entityId, eventId);
        } else if (checkEntityId(entityId)) {
            log.debug("Retrieving events for entityId {}", entityId);
            entries = eventLogRepo.findByEntityId(entityId);
        } else if (eventId != null) {
            log.debug("Retrieving events for eventId {}", eventId);
            entries = eventLogRepo.findByEventId(eventId);
        } else {
            log.debug("Retrieving all events");
            eventLogRepo.findAll().forEach(entries::add);
        }

        // Sort entries by date descending
        Collections.sort(entries, new Comparator<EventLogEntry>() {
            public int compare(EventLogEntry event1, EventLogEntry event2) {
                return event2.getTimestamp().compareTo(event1.getTimestamp());
            }
        });

        return new ResponseEntity<>(entries, HttpStatus.OK);
    }

    @Operation(summary = "Update events given a specific entityId")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The event was successfully updated"),
            @ApiResponse(responseCode = "400", description = "Fields changed on provided event that should not be changed"),
            @ApiResponse(responseCode = "404", description = "The event to be updated could not be found"),
            @ApiResponse(responseCode = "500", description = "Unable to update the desired event")
    })
    @PostMapping(path = "/updateEvents", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> updateEvents(@RequestBody EventLogEntry toBeUpdated) {
        OperationStatusModel result = new OperationStatusModel("UpdateEventLogEntry");

        if (toBeUpdated.getTimestamp() == null) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided EventLogEntry timestamp cannot be empty/null");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (eventLogRepo.existsById(toBeUpdated.getTimestamp())) {
            EventLogEntry dbEvent = eventLogRepo.findById(toBeUpdated.getTimestamp()).orElse(null);
            if (dbEvent != null) {
                if (!toBeUpdated.canBeUpdated(dbEvent)) {
                    result.setErrorDescription("Event property changed that should not be. Flagged, Opened, and Status String fields can be changed.");
                    result.setResult(RequestOperationResult.ERROR.name());
                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                }
                eventLogRepo.save(toBeUpdated);
            }
        } else {
            result.setErrorDescription("The event to be updated was not found: " + toBeUpdated.getTimestamp().toString());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        // TODO:  change to find unopened per user
        Integer numUnopened = eventLogRepo.countByOpened(false);
        template.convertAndSend("/topic/eventsCount", numUnopened);
        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Delete events with timestamp, entityId or eventId")
    @Parameters({
            @Parameter(name = "timestamp", description = "The timestamp of the event to delete", in = ParameterIn.QUERY, example = "2023-01-01T04:54:33"),
            @Parameter(name = "entityId", description = "The entityId of the events to delete", in = ParameterIn.QUERY, example = "0000aaaa-00aa-00aa-00aa-000000aaaaaa"),
            @Parameter(name = "eventId", description = "The eventId of the events to delete", in = ParameterIn.QUERY, example = "1001")
    })
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "The event(s) were successfully deleted"),
            @ApiResponse(responseCode = "404", description = "The event(s) to be deleted could not be found"),
            @ApiResponse(responseCode = "500", description = "Unable to delete the desired event(s)")
    })
    @DeleteMapping(path = "/deleteEvents", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> deleteEvents(@RequestParam(name = "timestamp", required = false) String timestamp, @RequestParam(name = "entityId", required = false) String entityId,
                                                           @RequestParam(name = "eventId", required = false) Integer eventId) {
        OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

        if (checkTimestamp(timestamp) && !eventLogRepo.existsById(makeTimestamp(timestamp))) {
            result.setErrorDescription("Event not found with Timestamp: " + timestamp);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        } else if (checkEntityId(entityId) && !eventLogRepo.existsByEntityId(entityId)) {
            result.setErrorDescription("Events not found with entityId: " + entityId);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        } else if (eventId != null && !eventLogRepo.existsByEventId(eventId)) {
            result.setErrorDescription("Events not found with eventId: " + eventId);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        } else if (!checkTimestamp(timestamp) && !checkEntityId(entityId) && eventId == null) {
            result.setErrorDescription("No specifiers provided to delete event(s)");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        try {
            if (checkTimestamp(timestamp)) {
                log.debug("Deleting event with Timestamp {}", timestamp);
                eventLogRepo.deleteById(makeTimestamp(timestamp));
            } else if (checkEntityId(entityId)) {
                log.debug("Deleting event with entityId {}", entityId);
                eventLogRepo.deleteByEntityId(entityId);
            } else if (eventId != null) {
                log.debug("Deleting event with eventId {}", eventId);
                eventLogRepo.deleteByEventId(eventId);
            }
        } catch (Exception ex) {
            result.setErrorDescription(ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        //TODO: change to find by user
        Integer numUnopened = eventLogRepo.countByOpened(false);
        template.convertAndSend("/topic/eventsCount", numUnopened);
        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private Timestamp makeTimestamp(String timestamp) {
        LocalDateTime ld = LocalDateTime.parse(timestamp);
        ZonedDateTime zdt = ld.atZone(ZoneId.systemDefault());
        ZonedDateTime utc = zdt.withZoneSameInstant(ZoneId.of("UTC"));
        Timestamp time = new Timestamp(utc.toInstant().toEpochMilli());
        return time;
    }

    private boolean checkTimestamp(String timestamp) {
        return timestamp != null && !timestamp.isEmpty();
    }

    private boolean checkEntityId(String entityId) {
        return entityId != null && !entityId.isEmpty();
    }

    @MessageMapping("/sendMessageEventsCount")
    public void receiveMessage(@Payload Integer eventsCount) {
        // receive message from client
    }

    @SendTo("/topic/eventsCount")
    public Integer broadcastMessage(@Payload Integer eventsCount) {
        return eventsCount;
    }
}
