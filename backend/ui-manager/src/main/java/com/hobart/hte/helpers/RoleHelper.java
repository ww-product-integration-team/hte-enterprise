package com.hobart.hte.helpers;

import com.hobart.hte.repositories.RoleRepository;
import com.hobart.hte.utils.access.HTeRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service
public class RoleHelper {
    private static final Logger log = LogManager.getLogger(DomainHelper.class);

    // 0
    public static Pair<Integer, String> MONITOR = Pair.of(1, "Monitor");
    public static Pair<Integer, String> REBOOT = Pair.of(2, "Reboot");
    public static Pair<Integer, String> HEARTBEAT = Pair.of(3, "ManualHeartbeat");
    public static Pair<Integer, String> PING = Pair.of(4, "Ping");

    // 1000
    public static Pair<Integer, String> ENABLE_DISABLE = Pair.of(1001, "EnableDisable");
    public static Pair<Integer, String> ASSIGN_SCALES = Pair.of(1002, "AssignScales");
    public static Pair<Integer, String> MANAGE_OPERATORS = Pair.of(1003, "ManageOperators");
    public static Pair<Integer, String> ASSIGN_SCALE_PROFILES = Pair.of(1004, "AssignScaleProfiles");
    public static Pair<Integer, String> DELETE_SCALE_DATA = Pair.of(1005, "DeleteScaleData");
    public static Pair<Integer, String> FILE_VIEWER = Pair.of(1006, "FileViewer");
    public static Pair<Integer, String> USER_ENABLEDISABLE = Pair.of(1007, "UserEnableDisable");
    public static Pair<Integer, String> DOWNLOAD_TRANS = Pair.of(1008, "DownloadTransactions");

    // 2000
    public static Pair<Integer, String> MANAGE_PROFILES = Pair.of(2001, "ManageProfiles");
    public static Pair<Integer, String> FILE_MANAGER = Pair.of(2002, "FileManager");
    public static Pair<Integer, String> ASSIGN_DEPT_PROFILES = Pair.of(2003, "AssignDeptProfiles");
    public static Pair<Integer, String> FULL_TREEVIEW = Pair.of(2004, "FullTreeView");
    public static Pair<Integer, String> MANAGE_SUPERVISORS = Pair.of(2005, "ManageSupervisors");
    public static Pair<Integer, String> MANAGE_LICENSES = Pair.of(2006, "ManageLicenses");
    public static Pair<Integer, String> SYSTEM_MANAGER = Pair.of(2007, "SystemManager");
    public static Pair<Integer, String> SET_PRIMARY_SCALE = Pair.of(2008, "SetPrimaryScale");
    public static Pair<Integer, String> UPGRADE_DEVICES = Pair.of(2009, "UpgradeScales");

    // 3000
    public static Pair<Integer, String> IMPORT_TREE = Pair.of(3001, "ImportTree");
    public static Pair<Integer, String> FLEET_STATUS = Pair.of(3002, "FleetStatus");

    // 4000
    public static Pair<Integer, String> MANUAL_OVERRIDE = Pair.of(4001, "ManualOverride");
    public static Pair<Integer, String> OTP_PASS_MANAGER = Pair.of(4002, "OtpPasswordManager");

    public RoleHelper() {
    }

    @Autowired
    RoleRepository role_repo;

    public HTeRole getRoleByName(String name) {
        return role_repo.findByName(name);
    }
}
