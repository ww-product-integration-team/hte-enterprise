package com.hobart.hte.assetList;

import com.hobart.hte.config.NodeStatusUpdater;
import com.hobart.hte.repositories.*;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.utils.access.HTeDomain;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.file.HobFiles;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.model.NodeType;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.repositories.StoreDeptPairsRepository;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.tree.NodeStatus;
import com.hobart.hte.utils.tree.ScaleNode;
import com.hobart.hte.utils.tree.StoreDeptPair;
import com.hobart.hte.utils.upgrade.DeptAndBatchIds;
import com.hobart.hte.utils.upgrade.StoreAndBatchIds;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class LazyLoadAssetTree extends ControllerEndpoint<Map<String, ScaleNode>> {

    private static final Logger log = LogManager.getLogger(LazyLoadAssetTree.class);
    @Autowired
    List<String> nodePath;
    @Autowired
    DomainRepository domainRepo;
    @Autowired
    BannerRepository bannerRepo;
    @Autowired
    RegionRepository regionRepo;
    @Autowired
    StoreRepository storeRepo;
    @Autowired
    DepartmentRepository deptRepo;
    @Autowired
    DeviceRepository deviceRepo;
    @Autowired
    ScaleSettingsRepository ssRepo;
    @Autowired
    FileRepository fileRepo;
    @Autowired
    UpgradeDeviceRepository upgradeDeviceRepo;
    @Autowired
    NodeStatusRepository nodeStatusRepo;
    @Autowired
    StoreDeptPairsRepository storeDeptPairsRepository;
    @Autowired
    ConfigAppRepository configAppRepository;

    public LazyLoadAssetTree(List<String> nodePath, DomainRepository domainRepo, BannerRepository bannerRepo,
                             RegionRepository regionRepo, StoreRepository storeRepo, DepartmentRepository deptRepo,
                             DeviceRepository deviceRepo, ScaleSettingsRepository ssRepo, FileRepository fileRepo,
                             UpgradeDeviceRepository upgradeDeviceRepo, NodeStatusRepository nodeStatusRepo, StoreDeptPairsRepository storeDeptPairsRepository,
                             ConfigAppRepository configAppRepository) {
        this.nodePath = nodePath;
        this.domainRepo = domainRepo;
        this.bannerRepo = bannerRepo;
        this.regionRepo = regionRepo;
        this.storeRepo = storeRepo;
        this.deptRepo = deptRepo;
        this.deviceRepo = deviceRepo;
        this.ssRepo = ssRepo;
        this.fileRepo = fileRepo;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
        this.nodeStatusRepo = nodeStatusRepo;
        this.storeDeptPairsRepository = storeDeptPairsRepository;
        this.configAppRepository = configAppRepository;
    }



    protected ScaleNode findNodeData(HTeDomain domain) {
        ScaleNode node;
        switch (domain.getType()) {
            case BANNER:
                node = new ScaleNode(bannerRepo.findById(domain.getDomainId()).get());
                break;
            case REGION:
                node = new ScaleNode(regionRepo.findById(domain.getDomainId()).get());
                break;
            case STORE:
                node = new ScaleNode(storeRepo.findById(domain.getDomainId()).get());
                break;
            case DEPARTMENT:
                node = new ScaleNode(deptRepo.findById(domain.getDomainId()).get());
                break;
            default:
                node = null;
        }
        return node;
    }

    protected List<HobFiles> getProfileFiles(String profileId) {
        if(profileId == null){
            return null;
        }

        List<HobFiles> activeFiles = new ArrayList<>();
        List<FileForEvent> allFilesInProfile = fileRepo.findByProfileIdAndEnabled(profileId, true);

        Timestamp now = Timestamp.from(Instant.now());

        for (FileForEvent ffe : allFilesInProfile) {
            if ((ffe.getStartDate() == null && ffe.getEndDate() == null)
                    || (ffe.getStartDate().before(now) && ffe.getEndDate() == null)
                    || (ffe.getStartDate().before(now) && ffe.getEndDate().after(now))) {
                activeFiles.add(new HobFiles(ffe));
            }
        }
        return activeFiles;
    }

//    protected void createNodeStatus(ScaleNode node, String nodeId, String parentId) {
//        NodeStatus status = new NodeStatus();
//
//        switch (node.getType()) {
//            case "banner":
//                List<ScaleDevice> bannerDevices = deviceRepo.findByBannerId(nodeId);
//                compileNodeStatusData(bannerDevices, status);
//                break;
//            case "region":
//                List<ScaleDevice> regionDevices = deviceRepo.findByRegionId(nodeId);
//                compileNodeStatusData(regionDevices, status);
//                break;
//            case "store":
//                List<ScaleDevice> storeDevices = deviceRepo.findByStoreId(nodeId);
//                compileNodeStatusData(storeDevices, status);
//                break;
//            case "dept":
//                List<ScaleDevice> deptDevices = deviceRepo.findByStoreIdAndDeptId(parentId, nodeId);
//                compileNodeStatusData(deptDevices, status);
//                break;
//            default:
//                status = null;
//        }
//
//        node.setNodeStatus(status);
//    }
//    //not current actually used... - Corey
//    protected void compileNodeStatusData(List<ScaleDevice> devices, NodeStatus status) {
//        for (ScaleDevice device : devices) {
//            NodeStatus deviceStatus = new NodeStatus(device);
//            ScaleSettings settings = ssRepo.findById(device.getDeviceId()).orElse(null);
//            deviceStatus.setScaleSync(device, settings, getProfileFiles(device.getProfileId()));
//            deviceStatus.setTimeSyncStatus(settings);
//            status.AddChildStatus(deviceStatus);
//        }
//    }
    protected void hasChildren(ScaleNode node){
        switch (node.getType()){
            case "banner":
                try{
                    List<Region> regions = regionRepo.findByBannerId(node.getId());
                    node.setHasChildren(regions.size() > 0);
                }catch (Exception e){
                    node.setHasChildren(false);
                }
                break;
            case "region":
                try{
                    List<Store> stores = storeRepo.findByRegionId(node.getId());
                    node.setHasChildren(stores.size() > 0);
                }catch (Exception e){
                    node.setHasChildren(false);
                }
                break;
            case "store":
                try{
                    List<StoreDeptPair> depts = storeDeptPairsRepository.findByStoreId(node.getId());
                    node.setHasChildren(depts.size() > 0);
                }catch (Exception e){
                    node.setHasChildren(false);
                }
                break;
            case "dept":
                try{
                    String deptId = node.getId();
                    String storeId = "";
                    List<ScaleDevice> scales = new ArrayList<>();
                    if(deptId.contains("|")){
                        storeId = deptId.split("\\|")[0];
                        deptId = deptId.split("\\|")[1];
                        scales = deviceRepo.findByStoreIdAndDeptId(storeId, deptId);
                    } else {
                        throw new IllegalArgumentException("Provided department does not have store info");
                    }
                    node.setHasChildren(scales.size()> 0);
                }catch (Exception e){
                    node.setHasChildren(false);
                }
                break;
            case "default":
                node.setHasChildren(false);
                break;
        }
    }

    protected ScaleNode packageNodeData(String nodeId, List<String> path){

        String awayThreshold = configAppRepository.findById(AppConfig.scaleAwayHours).get().getCoValue();
        String offlineThreshold = configAppRepository.findById(AppConfig.scaleOfflineHours).get().getCoValue();

        // Returns the node with mapped children
        if(nodeId.contains("|")){
            // this is a dept id and must be split
            nodeId = nodeId.split("\\|")[1];
        }
        HTeDomain parentDomain = domainRepo.findByDomainIdAndTypeNot(nodeId, EntityType.PROFILE.toString());
        if(parentDomain == null){
            log.info("Could not find parent domain");
            return null;
        }
        ScaleNode parent = findNodeData(parentDomain);
        if(parent == null){
            log.info("Could not find parent node");
            return null;
        }
        parent.setPath(path);
        List<HTeDomain> childrenDomains = domainRepo.findByParentIdAndTypeNot(nodeId, EntityType.PROFILE.toString());
        Map<String, ScaleNode> childrenMap = new LinkedHashMap<>();
        if (childrenDomains == null || childrenDomains.isEmpty()) {
            // both stores and departments don't have children from domains
            if (parent.getType().equals("store")) {
                // add all departments by default
                // TODO: take into consideration when making structure manually
                List<StoreDeptPair> storeDeptPairs = storeDeptPairsRepository.findByStoreId(parent.getId());
                // TODO: Log the pairs!
                for(StoreDeptPair storeDeptpair : storeDeptPairs){
                    Department dept = deptRepo.findById(storeDeptpair.getDeptId()).orElse(null);
                    if(dept == null){
                        throw new NullPointerException("Department key pair does not exist in DB!");
                    }
                    try {
                        ScaleNode child = new ScaleNode(dept);
                        final String newChildId = parent.getId() + "|" + child.getId();
                        child.setId(newChildId);

                        boolean childNodeExists = nodeStatusRepo.existsById(newChildId);
                        if (!childNodeExists) {

                            boolean currentlyUpdatingNodes = NodeStatusUpdater.getUpdatingState();
                            if (currentlyUpdatingNodes) {
                                throw new NullPointerException("Nodes are being updated; please stand by.");
                            }

                            // If we're this far along, we're probably not updating anymore, so we have a problem.
                            // Let's try to provide immediate mitigation by recreating the MIA node on the fly.
                            try {
                                log.warn("Could not find node for {}. Recreating node.", newChildId);
                                NodeStatus recoveryNode = new NodeStatus();

                                recoveryNode.setNodeType(NodeType.DEPT);
                                recoveryNode.setDeviceId(newChildId); // Store|Department
                                recoveryNode.setParentId(parent.getId()); // Store

                                nodeStatusRepo.save(recoveryNode);

                                childNodeExists = nodeStatusRepo.existsById(newChildId);
                                if (!childNodeExists) { // We still don't have it, somehow. Throwing in the towel.
                                    throw new Exception();
                                }
                            } catch (Exception e) { // Something went wrong -- again. We've done what we could.
                                throw new NullPointerException(
                                        "An unexpected error occurred while loading this store's department node(s).");
                            }
                        }
                        List<String> newPath = new ArrayList<>(parent.getPath());
                        newPath.add(child.getId());
                        hasChildren(child);
                        child.setPath(newPath);
                        List<DeptAndBatchIds> deptAndBatchIds = deviceRepo.findDeptIdByBatchId();
                        child.setBatchIds(deptAndBatchIds.stream().filter(ids -> ids.getDeptId().equals(child.getId())).map(
                                DeptAndBatchIds::getBatchId).collect(Collectors.toList()));
                        child.setNodeStatus(nodeStatusRepo.findById(child.getId()).get());
                        childrenMap.put(child.getId(), child);
                        child.setChildren(new HashMap<>());
                    }
                    catch (Exception e){
                        throw e;
                    }
                }
            } else if (parent.getType().equals("dept")) {
                // using combined ids, find devices for specific department and store
                String deptNodeId = nodeId;
                if(!storeDeptPairsRepository.existsByStoreIdAndDeptId(path.get(path.size()-2), nodeId)){
                    // If key pairing does not exist dept node should not be created
                    return null;
                }
                List<ScaleDevice> devices = deviceRepo.findByStoreIdAndDeptId(path.get(path.size()-2), nodeId);
                for (ScaleDevice device : devices) {
                    ScaleNode child = new ScaleNode(device);
                    List<String> newPath = new ArrayList<>(parent.getPath());
                    newPath.add(device.getDeviceId());
                    child.setPath(newPath);
                    List<UpgradeDevice> upgradeDevices = upgradeDeviceRepo.findByBatchIdIsNotNull();
                    child.setBatchIds(upgradeDevices.stream().filter(ids -> ids.getDeviceId().equals(child.getId())).map(
                            UpgradeDevice::getBatchId).collect(Collectors.toList()));

                    if (!device.isInvisibleScale()) {
                        NodeStatus status = new NodeStatus(device);
                        status.setScaleReportStatus(device, awayThreshold, offlineThreshold);

                        ScaleSettings settings = ssRepo.findById(device.getDeviceId()).orElse(null);
                        status.setScaleSync(device, settings, getProfileFiles(device.getProfileId()));
//                        status.setTimeSyncStatus(settings);
                        child.setNodeStatus(status);
                        child.setChildren(new HashMap<>());
                    }
                    hasChildren(child);
                    childrenMap.put(child.getId(), child);
                }
            }
        } else {
            for (HTeDomain childDomain : childrenDomains) {
                ScaleNode child = findNodeData(childDomain);
                if (child.getType().equals("store")) {
                    List<StoreAndBatchIds> storeAndBatchIds = deviceRepo.findStoreIdByBatchId();
                    child.setBatchIds(storeAndBatchIds.stream().filter(ids -> ids.getStoreId().equals(child.getId())).map(
                            StoreAndBatchIds::getBatchId).collect(Collectors.toList()));
                }
                List<String> newPath = new ArrayList<>(parent.getPath());
                newPath.add(childDomain.getDomainId());
                child.setPath(newPath);
                child.setChildren(new HashMap<>());
                Optional<NodeStatus> childStatus = nodeStatusRepo.findById(child.getId());
                NodeStatus useableValue = new NodeStatus();
                if (childStatus.isPresent()) {
                    useableValue = childStatus.get();
                }
                child.setNodeStatus(useableValue);
                hasChildren(child);
                childrenMap.put(child.getId(), child);
            }
        }

        NodeStatus parentStatus = new NodeStatus();
        Map<String, ScaleNode> sortedMap = childrenMap.entrySet().stream().map(entry -> {
                    parentStatus.AddChildStatus(entry.getValue().getNodeStatus());
                    return entry;
                }).sorted((entry1, entry2) -> entry1.getValue().getName().compareToIgnoreCase(entry2.getValue().getName()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        parent.setChildren(sortedMap);
        parent.setHasChildren(sortedMap.size()>0);
        parent.setNodeStatus(parentStatus);

        return parent;
    }
    public Map<String, ScaleNode> execute() {
        log.info("Begin fetching tree data");
        //temp code to split branch
        Map<String, ScaleNode> treeMap = new LinkedHashMap<>();
        if (nodePath != null && !nodePath.isEmpty()) {
            String nodeId = nodePath.get(nodePath.size()-1);
            ScaleNode node = packageNodeData(nodeId, nodePath);
            treeMap.put(nodeId, node);
        } else { // no parent id provided so banners are requested
            Iterable<Banner> banners = bannerRepo.findAll();
            for (Banner banner : banners) {
                try{
                    ScaleNode node = new ScaleNode(banner);
                    node.setPath(Arrays.asList(banner.getBannerId()));
                    node.setChildren(new HashMap<>());
                    hasChildren(node);
                    node.setBatchIds(new ArrayList<>());
                    node.setNodeStatus(nodeStatusRepo.findById(node.getId()).get());
                    treeMap.put(node.getId(), node);
                } catch(Exception e){
                    log.info("creating banner node - caught exception:"+ e.getMessage());
                    throw e;
                }
            }
        }

        Map<String, ScaleNode> sortedMap = treeMap.entrySet().stream().sorted((entry1, entry2) ->
                        entry1.getValue().getName().compareToIgnoreCase(entry2.getValue().getName()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        log.info("Finish fetching tree data");
        return sortedMap;
    }
    public final Map<String, ScaleNode> refreshTree(List<String> expandedKeys) {
        log.info("Begin refreshing tree data");
        List<String> refreshedNode = new ArrayList<>();
        Map<String, ScaleNode> treeMap = new LinkedHashMap<>();
        // add all banners first then add children
        Iterable<Banner> banners = bannerRepo.findAll();
        for (Banner banner : banners) {
            try {
                ScaleNode node = new ScaleNode(banner);
                node.setPath(Arrays.asList(banner.getBannerId()));
                node.setChildren(new HashMap<>());
                node.setBatchIds(new ArrayList<>());
                hasChildren(node);
                node.setNodeStatus(nodeStatusRepo.findById(node.getId()).get());
                treeMap.put(node.getId(), node);
            } catch (Exception e) {
                log.info("creating banner node - caught exception:" + e.getMessage());
                throw e;
            }
        }
        // TODO itteracte through expanded keys and remove parent nodes because they will be created by their children.
        for(String key : expandedKeys) {
            Map<String, ScaleNode> currentBranch = null;
            String[] path = key.split("~");
            List<String> nodePath = new ArrayList<>();
            for(String nodeId : path) {
                nodePath.add(nodeId);
                if (!refreshedNode.contains(nodeId) ) {
                    try {
                        ScaleNode newNode = packageNodeData(nodeId, nodePath);
                        if(newNode == null){
                            break;
                        }
                        newNode.setBatchIds(new ArrayList<>());
                        if(currentBranch == null){
                            treeMap.put(nodeId, newNode);
                        } else {
                            currentBranch.put(nodeId,newNode);
                        }
                        refreshedNode.add(nodeId);
                    } catch (Exception e) {
                        log.info("creating node - caught exception:" + e.getMessage());
                        throw e;
                    }
                }
                if(currentBranch == null) {
                    currentBranch = treeMap.get(nodeId).getChildren();
                } else{
                    currentBranch = currentBranch.get(nodeId).getChildren();
                }
            }
        }
        Map<String, ScaleNode> sortedMap = treeMap.entrySet().stream().sorted((entry1, entry2) ->
                        entry1.getValue().getName().compareToIgnoreCase(entry2.getValue().getName()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        log.info("Finish fetching tree data");
        return sortedMap;
    }
}
