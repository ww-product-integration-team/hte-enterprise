package com.hobart.hte.diagnostics;

import java.net.MalformedURLException;

import javax.servlet.http.HttpServletRequest;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import java.util.Date;

import com.hobart.hte.utils.access.Diagnostic;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.event.EventConfig;
import com.hobart.hte.utils.event.EventImportance;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.store.Store;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import java.io.PrintWriter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.data.util.Streamable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import com.hobart.hte.repositories.DiagnosticsRepository;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.stream.StreamSupport;
import java.util.stream.Collectors;

@Tag(description = "Endpoints for Diagnostics Functions", name = "diagnostics")
@RestController
@RequestMapping("/diagnostics")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class DiagnosticsController {

	@Autowired
	DiagnosticsRepository diagnostics_repo;

	private static final Logger log = LogManager.getLogger(DiagnosticsController.class);
	private static final String COMPANY_KEY = "HoBaRt241@#@-_@%$#)(^Products";

	@Operation(summary = "Receive HQ's diagnostic check-in", description = "Receives information about a customer's fleet for reporting and analytics")
	@PostMapping(path = "/receive", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> receiveDiagnosics(@RequestBody Diagnostic diagnostic) {
		Diagnostic savedDiagnostic;
		savedDiagnostic = diagnostics_repo.save(diagnostic);
		return new ResponseEntity<>("{'result': 'diagnostics_received'}", HttpStatus.OK);
	}

	@Operation(summary = "List diagnostics", description = "View a list of diagnostics for a customer's fleet")
	@GetMapping(path = "/list", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> listDiagnosics() {
		List<Diagnostic> diagnostics = diagnostics_repo.findAll();
		return new ResponseEntity<>(diagnostics, HttpStatus.OK);
	}

	@Operation(summary = "Generate diagnostics report", description = "Create a report from diagnostics insights to send to HT Support Team")
	@GetMapping(value = "/report", produces = "text/csv")
	public ResponseEntity<?> exportCSV() {
		// replace this with your header (if required)
		String[] csvHeader = {
				"CustomerUUID", "NumScales", "NumScalesOnline", "NumScalesOffline", "AvgLabelsPrinted", "DateCreated"
		};

		List<Diagnostic> diagnostics = diagnostics_repo.findAll();
		List<List<String>> csvBody = new ArrayList<>();

		for (int i = 0; i < diagnostics.size(); i++) {
			csvBody.add(
					Arrays.asList(
							diagnostics.get(i).getCustomerUUID(),
							diagnostics.get(i).getNumScales().toString(),
							diagnostics.get(i).getNumScalesOnline().toString(),
							diagnostics.get(i).getNumScalesOffline().toString(),
							diagnostics.get(i).getAvgLabelsPrinted().toString(),
							diagnostics.get(i).getDateCreated().toString()));
			System.out.println(diagnostics.get(i));
		}

		ByteArrayInputStream byteArrayOutputStream;

		try (
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				// defining the CSV printer
				CSVPrinter csvPrinter = new CSVPrinter(
						new PrintWriter(out),
						// withHeader is optional
						CSVFormat.DEFAULT.withHeader(csvHeader));) {
			// populating the CSV content
			for (List<String> record : csvBody)
				csvPrinter.printRecord(record);

			// writing the underlying stream
			csvPrinter.flush();

			byteArrayOutputStream = new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}

		InputStreamResource fileInputStream = new InputStreamResource(byteArrayOutputStream);

		String csvFileName = "Diagnostics_" + LocalDateTime.now() + ".csv";

		// setting HTTP headers
		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + csvFileName);
		// defining the custom Content-Type
		headers.set(HttpHeaders.CONTENT_TYPE, "text/csv");

		return new ResponseEntity<>(
				fileInputStream,
				headers,
				HttpStatus.OK);
	}

	private boolean isNullorEmpty(String s) {
		return (s == null || s.isEmpty());
	}
}