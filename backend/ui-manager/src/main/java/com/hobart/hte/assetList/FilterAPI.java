package com.hobart.hte.assetList;

import com.hobart.hte.repositories.*;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.utils.tree.Filter;
import com.hobart.hte.utils.tree.ScaleNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.*;

/***
 * Description:
 * Params:
 * Tips:
 */
public class FilterAPI extends ControllerEndpoint<Boolean> {

    private static final Logger log = LogManager.getLogger(FilterAPI.class);

    private final EntityManager entityManager;
    private final FilterRepository filterRepo;

    private final DeviceRepository deviceRepository;

    public class FilterReturn {
        private  String filterId;
        private List<String> header;
        private Long numScales;
        public FilterReturn(String filterId, List<String> header, Long numScales){
            this.filterId = filterId;
            this.header = header;
            this.numScales = numScales;
        }


        public FilterReturn(String filterId){
            Iterable<Filter> filter = filterRepo.findByFilterId(filterId);
            this.header = new ArrayList<>();
            this.filterId = filterId;
            this.numScales = getFilterScaleCount(filter);
            this.header = new ArrayList<>();
            for(Filter filterPart : filter){
                this.header.add(filterPart.getProp());
            }
        }

        public String getFilterId() {
            return filterId;
        }

        public void setFilterId(String filterId) {
            this.filterId = filterId;
        }

        public List<String> getHeader() {
            return header;
        }

        public void setHeader(List<String>header) {
            this.header = header;
        }

        public Long getNumScales() {
            return numScales;
        }

        public void setNumScales(Long numScales) {
            this.numScales = numScales;
        }
    }
    public FilterAPI( EntityManager entityManager, FilterRepository filterRepo, DeviceRepository deviceRepository) {
        this.entityManager = entityManager;
        this.filterRepo = filterRepo;
        this.deviceRepository = deviceRepository;
    }

    /**
     *
     * @param filter parsed filter to iterate over and grab the prop and corresponding values to query against
     * @return returns the configured where query to be part of a SQL query:
     * @exampleQuery select sd from ScaleDevice as sd left join ScaleSettings as ss on sd.deviceId = ss.deviceId where
     * sd.scaleModel in ('HTi-7LS2','HTi-7LS','FS-20V') and ss.appHookVersion in ('3.0.12')
     */
    public String configureWhereQuery(Iterable<Filter> filter) {
        StringBuilder acc = new StringBuilder();
        ScaleDevice scaleDevice = new ScaleDevice();
        ScaleSettings ss = new ScaleSettings();
        Banner b = new Banner();
        Region r = new Region();
        for (Filter filterPart : filter) {

            if (scaleDevice.has(filterPart.getProp())) {
                acc.append("sd.");
                acc.append(filterPart.getProp());
            } else if (ss.has(filterPart.getProp())) {
                acc.append("ss.");
                acc.append(filterPart.getProp());
            } else if (b.has(filterPart.getProp())) {
                acc.append("b.");
                acc.append(filterPart.getProp());
            } else if (r.has(filterPart.getProp())) {
                acc.append("r.");
                acc.append(filterPart.getProp());
            } else {
                throw new NoResultException("Property not found on scaleDevice or scaleSettings:" + filterPart.getProp());
            }
            String[] values = filterPart.getValue().split(",");
            acc.append(" in (");
            for (String val : values) {
                if (val.equalsIgnoreCase("true")) {
                    acc.append("'1'");
                } else if (val.equalsIgnoreCase("false")) {
                    acc.append("'0'");
                } else {
                    acc.append("'" + val + "'" );
                }
                acc.append(",");
            }
            acc.replace(acc.length() - 1, acc.length(), ""); //remove extra "," added by for loop
            acc.append(") and ");
        }
        acc.replace(acc.length() - 4, acc.length(), ""); //remove extra "and'" added by for loop
        return acc.toString();
    }
    /**
     *
     * @param filter parsed filter to iterate over and grab the prop and corresponding values to query against
     * @return the number of scales found based on provided filter
     * @exampleQuery select COUNT(*) from ScaleDevice as sd left join ScaleSettings as ss on sd.deviceId = ss.deviceId where
     * sd.scaleModel in ('HTi-7LS2','HTi-7LS','FS-20V') and ss.appHookVersion in ('3.0.12')
     */
    public long getFilterScaleCount(Iterable<Filter> filter){
        // Customer query must look like =>
        // select COUNT(*) from ScaleDevice as sd left join ScaleSettings as ss on sd.deviceId = ss.deviceId where 'prop' in ('val1','val2',...) AND ...
        StringBuilder customQuery = new StringBuilder();
        customQuery.append("select COUNT(*) from ScaleDevice as sd " +
                "left join ScaleSettings as ss on sd.deviceId = ss.deviceId " +
                "inner join Store as s on sd.storeId = s.storeId " +
                "inner join Region as r on s.regionId = r.regionId " +
                "inner join Banner as b on r.bannerId = b.bannerId " +
                "where ");
        customQuery.append(configureWhereQuery(filter));
        TypedQuery<Long> acc = entityManager.createQuery(customQuery.toString(), Long.class);
        return acc.getSingleResult();
    };
    /**
     *
     * @param filter parsed filter to iterate over and grab the prop and corresponding values to query against
     * @return a list of scales found based on provided filter
     * @exampleQuery select COUNT(*) from ScaleDevice as sd left join ScaleSettings as ss on sd.deviceId = ss.deviceId where
     * sd.scaleModel in ('HTi-7LS2','HTi-7LS','FS-20V') and ss.appHookVersion in ('3.0.12')
     */
    public List<ScaleDevice> getFilteredScales(String filterId){
        List<Filter> filter = filterRepo.findByFilterId(filterId);
        if(filter.isEmpty()){
            throw new NullPointerException("Could not find filter:" + filterId);
        }
        StringBuilder customQuery = new StringBuilder();
        customQuery.append("select sd from ScaleDevice as sd " +
                "left join ScaleSettings as ss on sd.deviceId = ss.deviceId " +
                "inner join Store as s on sd.storeId = s.storeId " +
                "inner join Region as r on s.regionId = r.regionId " +
                "inner join Banner as b on r.bannerId = b.bannerId " +
                "where ");
        customQuery.append(configureWhereQuery(filter));
        TypedQuery<ScaleDevice> acc = entityManager.createQuery(customQuery.toString(), ScaleDevice.class);
        return acc.getResultList();
    };
    /**
     *
     * @param filter parsed filter to iterate over and grab the prop and corresponding values to query against
     * @return a list of scale ids found based on provided filter
     * @exampleQuery select sd.deviceId from ScaleDevice as sd left join ScaleSettings as ss on sd.deviceId = ss.deviceId where
     * sd.scaleModel in ('HTi-7LS2','HTi-7LS','FS-20V') and ss.appHookVersion in ('3.0.12')
     */
    public List<String> getFilteredScaleIds(String filterId){
        List<Filter> filter = filterRepo.findByFilterId(filterId);
        if(filter.isEmpty()){
            throw new NullPointerException("Could not find filter:" + filterId);
        }
        StringBuilder customQuery = new StringBuilder();
        customQuery.append("select sd.deviceId from ScaleDevice as sd " +
                "left join ScaleSettings as ss on sd.deviceId = ss.deviceId " +
                "inner join Store as s on sd.storeId = s.storeId " +
                "inner join Region as r on s.regionId = r.regionId " +
                "inner join Banner as b on r.bannerId = b.bannerId " +
                "where ");
        customQuery.append(configureWhereQuery(filter));
        TypedQuery<String> acc = entityManager.createQuery(customQuery.toString(), String.class);
        return acc.getResultList();
    };

    /**
     *
     * @param filterId = fitlerId of corresponding filter
     * @param node = current child of lazyFilter fetch
     * @return true if a filteredscale is in the nodeScale list else false
     * @comment There may be a faster way to check if node has scales that match the provided filter
     */
    public boolean nodeHasScales(String filterId, ScaleNode node){

        List<String> filteredScalesIds = getFilteredScaleIds(filterId);
        List<String> nodeScaleIds = new ArrayList<>();
        String nodeId = node.getId();
         if(nodeId.contains("~")){
             nodeId = nodeId.split("~")[1];
         }
        switch (node.getType()) {
            case "banner":
                nodeScaleIds = deviceRepository.findDeviceIdByBannerId(nodeId);
                break;
            case "region":
                nodeScaleIds = deviceRepository.findDeviceIdByRegionId(nodeId);
                break;
            case "store":
                nodeScaleIds = deviceRepository.findDeviceIdByStoreId(nodeId);
                break;
            case "dept":
                if(!node.getId().contains("|")){
                    throw new IllegalArgumentException("Dept id is not a valid Id");
                }
                String deptId = nodeId.split("\\|")[1];
                String storeId = nodeId.split("\\|")[0];
                nodeScaleIds = deviceRepository.findDeviceIdByStoreIdAndDeptId(storeId, deptId);
                break;
            default:
                return false;
        }
        if(filteredScalesIds.isEmpty()){
            return false;
        }
        for ( String filteredScaleId : filteredScalesIds){
            if(nodeScaleIds.contains(filteredScaleId)){
                return true;
            }
        }

        return false;
    }



    public OperationStatusModel save(List<Filter> newFilter){
        String newUUID = UUID.randomUUID().toString();
        OperationStatusModel result = new OperationStatusModel("UpdateFilter");
        Long scaleCount = getFilterScaleCount(newFilter);
        if(scaleCount == 0){
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error creating filter: Zero scales found");
            return result;
        }
        for( Filter filterPart : newFilter){
            filterPart.setFilterId(newUUID);
            filterRepo.save(filterPart);
        }
        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Successfully created filter\nFound " + scaleCount.toString() + " Scales.");
        return result;
    }

    public HashMap<String, FilterReturn> fetchAll(){
        HashMap<String, FilterReturn> acc = new HashMap<>();
        Iterable<String> filterIds = filterRepo.findDistinctFilterIds();
        for(String filterId : filterIds){
            acc.put(filterId, new FilterReturn(filterId));
        }
        return acc;
    }

    public OperationStatusModel deleteAll() {
        OperationStatusModel result = new OperationStatusModel("DeleteAllFilter");
        filterRepo.deleteAll();

        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Successfully deleted filters");
        return result;
    }
    public OperationStatusModel deleteByFilterId(String filterId) {
        OperationStatusModel result = new OperationStatusModel("DeleteAllFilter");
        if( filterRepo.existsByFilterId(filterId)) {
            filterRepo.deleteByFilterId(filterId);
        } else {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error deleting filter. Could not find filterId" + filterId);
            return result;
        }
        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Successfully deleted filters");
        return result;
    }
}