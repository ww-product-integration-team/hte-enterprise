package com.hobart.hte.scale;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class HobartScaleCommDriver {
	private final String Header = "RT01\u001fDT1\u001e";
	private final String Footer = "RTFF\u001e";
	private int timeout = 30000; // 30 seconds by default
	private String ip_address;
	private String scale_status;
	private final int SMBACKEND_PORT = 5076;

	private static final Logger log = LogManager.getLogger(HobartScaleCommDriver.class);

	public HobartScaleCommDriver(String scale) {
		ip_address = scale;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 *
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public String readData(InputStream in) throws IOException {
		StringBuilder buf = new StringBuilder();
		boolean isReceiving = true;
		char c;

		while (isReceiving) {
			c = (char) in.read();
			buf.append(c);

			if (buf.length() == Header.length() && !buf.toString().startsWith(Header)) {
				log.error("Not a scale, garbage received: {}", buf.toString());
				throw new IOException("Not a scale, garbage received");
			}

			if (buf.toString().endsWith(Footer)) {
				isReceiving = false;
			}
		}
		return buf.toString();
	}

	/**
	 *
	 * @param data
	 * @param out
	 * @throws IOException
	 * @throws SocketTimeoutException
	 */
	public void sendData(String data, OutputStream out) throws IOException, SocketTimeoutException {
		for (int i = 0; i < data.length(); i++) {
			out.write((int) data.charAt(i));
		}
	}

	public void setIP_address(String ip) {
		ip_address = ip.trim();
	}

	public String getIP_address() {
		return ip_address;
	}

	/**
	 * Sends a message on port 6000 to read all the scale status
	 * 
	 * @return
	 */
	public String getScaleStatus() {
		try {

			scale_status = sendCommand("RT7D\u001f\u001eRT68\u001f\u001eRT65\u001f\u001e");

		} catch (Exception iox) {
			scale_status = "<error> " + iox.toString();
		}
		return scale_status;
	}

	/**
	 * Send a message to the scale to reboot it using port 6000
	 * 
	 * @return
	 */
	public String rebootScale() {
		try {
			scale_status = sendCommand("RT4F\u001e");

		} catch (Exception iox) {
			scale_status = "<error> " + iox.toString();
		}

		return scale_status;
	}

	/**
	 * Sends a message to the scale instructing to execute a MANUAL heartbeat on
	 * port 6000
	 * 
	 * @return
	 */
	public String executeHeartbeat() {
		try {
			scale_status = sendCommand("RTDO\u001fHAHealthCheck|MANUAL\u001e");

		} catch (Exception iox) {
			scale_status = "<error> " + iox.toString();
		}

		return scale_status;
	}

	/**
	 * Sends a message to HT scales instructing to delete All data. The 2A function
	 * code is based on the scalemasterEvol.h file
	 * 
	 * @return the acknowledge message returned from the scale in tagged format
	 */
	public String deleteAllData() {
		try {
			scale_status = sendCommand("RT2A\u001e");
		} catch (Exception iox) {
			scale_status = "<error> " + iox.toString();
		}

		return scale_status;
	}

	/**
	 *
	 * @param cmd
	 * @return
	 * @throws IOException
	 */
	public String sendSMBcommand(String cmd) throws IOException {
		String raw;
		Socket scale;
		byte[] data = new byte[256];

		SocketAddress sockaddr = new InetSocketAddress(ip_address, SMBACKEND_PORT);
		scale = new Socket();
		scale.connect(sockaddr);

		sendData(cmd, scale.getOutputStream());
		scale.getInputStream().read(data);
		scale.close();

		raw = new String(data);
		log.trace("data read: {}", raw);
		return raw;
	}

	public String sendCommand(String command) throws IOException, SocketTimeoutException {
		String rec;
		StringBuilder cmd = new StringBuilder();
		Socket socket;

		cmd.append(Header);
		cmd.append(command);
		cmd.append(Footer);

		socket = new Socket();
		socket.setSoTimeout(timeout);
		log.debug("attempting connection to scale: {} on port 6000, timeout: {} ms", getIP_address(), timeout);
		socket.connect(new InetSocketAddress(getIP_address(), 6000), timeout);

		sendData(cmd.toString(), socket.getOutputStream());
		log.trace("sending command to scale: {}", cmd.toString());

		rec = readData(socket.getInputStream());

		log.trace("ip: {}, response: {}", getIP_address(), rec);

		socket.close();

		return rec;
	}
}
