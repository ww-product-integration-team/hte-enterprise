package com.hobart.hte.repositories;

import com.hobart.hte.utils.profile.ChecksumLog;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChecksumLogRepository extends CrudRepository<ChecksumLog, String> {
}
