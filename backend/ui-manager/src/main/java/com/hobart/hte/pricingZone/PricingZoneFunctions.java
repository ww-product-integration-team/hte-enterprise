package com.hobart.hte.pricingZone;

import com.hobart.hte.repositories.PricingZoneRepository;
import com.hobart.hte.utils.item.PricingZone;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.util.HteTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PricingZoneFunctions {
    private final Logger log = LogManager.getLogger(PricingZoneFunctions.class);

    private final PricingZoneRepository pricingRepo;
    private final OperationStatusModel result;

    public HttpStatus resultStatus;

    public PricingZoneFunctions(PricingZoneRepository pricingRepo, OperationStatusModel result) {
        this.pricingRepo = pricingRepo;
        this.result = result;
    }

    public Map<String, PricingZone> getZones(String zoneId) {
        Map<String, PricingZone> zones = new HashMap<>();
        if (HteTools.isNullorEmpty(zoneId)) {
            Iterable<PricingZone> dbZones = pricingRepo.findAll();
            dbZones.forEach(zone -> zones.put(zone.getId(), zone));
        } else {
            if (pricingRepo.existsById(zoneId)) {
                PricingZone zone = pricingRepo.findById(zoneId).get();
                zones.put(zone.getId(), zone);
            } else {
                log.error("Pricing zone does not exist with id: {}", zoneId);
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Pricing zone does not exist with id: " + zoneId);
                resultStatus = HttpStatus.NOT_FOUND;
                return null;
            }
        }

        return zones;
    }

    public void saveZone(PricingZone zone) {
        if (zone == null) {
            log.error("Provided zone is null");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided zone is null");
            resultStatus = HttpStatus.BAD_REQUEST;
            return;
        }
        if (HteTools.isNullorEmpty(zone.getName())) {
            log.error("Provided zone name is null");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided zone ID or zone name is null");
            resultStatus = HttpStatus.BAD_REQUEST;
            return;
        }
        if (pricingRepo.existsByName(zone.getName())) {
            log.error("Provided zone name is already in use");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided zone name is already in use");
            resultStatus = HttpStatus.BAD_REQUEST;
            return;
        }

        zone.setId(UUID.randomUUID().toString());

        pricingRepo.save(zone);
    }

    public void deleteZone(String zoneId) {
        if (HteTools.isNullorEmpty(zoneId)) {
            log.error("Provided zone ID is null or empty");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided zone ID is null or empty");
            resultStatus = HttpStatus.BAD_REQUEST;
            return;
        }

        if (pricingRepo.existsById(zoneId)) {
            pricingRepo.deleteById(zoneId);
        } else {
            log.error("Provided zone ID does not exist in database");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided zone ID does not exist in database");
            resultStatus = HttpStatus.BAD_REQUEST;
        }
    }
}
