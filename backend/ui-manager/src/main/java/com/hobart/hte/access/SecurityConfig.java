package com.hobart.hte.access;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean(name="customUserDetailsService")
    public AccessController userDetailsService() {
        return new AccessController();
    }

    @Bean
    public BCryptPasswordEncoder htePasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.cors(cors -> cors.configurationSource(corsConfigurationSource()))
                .csrf(AbstractHttpConfigurer::disable).authorizeHttpRequests(authorizationManagerRequestMatcherRegistry ->
                        authorizationManagerRequestMatcherRegistry
                                .antMatchers("/access/token/**").permitAll()
                                .antMatchers("/access/login/**").permitAll()
                                .antMatchers("/access/license/**").permitAll()
                                .antMatchers("/access/refreshToken/**").permitAll()
                                .antMatchers("/licensing/**/**").permitAll()
                                .antMatchers("/ui/saveChecksumLogs").permitAll()
                                .antMatchers("/ws-message/**").permitAll()
                                .antMatchers("/swagger-ui/**").permitAll()
                                .antMatchers("/v3/**").permitAll()
                                .antMatchers("/ui/disabledProfileUpdatedStatus").permitAll()
                                .antMatchers("/ui/profileUpdatedStatus").permitAll()
                                .antMatchers("/ui/test").permitAll()
                                .anyRequest().authenticated()).sessionManagement(httpSecuritySessionManagementConfigurer ->
                        httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS));

        http.apply(new CustomAuthenticationManager());
        http.addFilterBefore(customAuthenticationFilter(),
                UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(new HteAuthorizationFilter(),
                UsernamePasswordAuthenticationFilter.class);
        http.addFilterAfter(new DomainAuthorizationFilter(),
                BasicAuthenticationFilter.class);
        http.addFilterAfter(new RoleAuthorizationFilter(),
                BasicAuthenticationFilter.class);

        return http.build();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH"));
        config.setAllowedOrigins(Arrays.asList("http://localhost:8080", "http://*:8080", "http://localhost:3000", "http://*:3000", "https://localhost:443", "https://*:443"));
        config.setAllowCredentials(true);
        config.setAllowedHeaders(Arrays.asList("*"));
        config.setExposedHeaders(Arrays.asList("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    class CustomAuthenticationManager extends AbstractHttpConfigurer<CustomAuthenticationManager, HttpSecurity> {
        @Autowired
        UserDetailsService service;

        @Override
        public void configure(HttpSecurity http) throws Exception {
            DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
            provider.setPasswordEncoder(htePasswordEncoder());
            provider.setUserDetailsService(service);
            HTeAuthenticationFilter hteAuth = new HTeAuthenticationFilter(new ProviderManager(provider));
            hteAuth.setFilterProcessesUrl("/access/login");
            http.addFilter(hteAuth);
        }
    }
    public HTeAuthenticationFilter customAuthenticationFilter() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(htePasswordEncoder());
        provider.setUserDetailsService(userDetailsService());
        HTeAuthenticationFilter hteAuth = new HTeAuthenticationFilter(new ProviderManager(provider));
        hteAuth.setFilterProcessesUrl("/access/login");
        return hteAuth;
    }
}
