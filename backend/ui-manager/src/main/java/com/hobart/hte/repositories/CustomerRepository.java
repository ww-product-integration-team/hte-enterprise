package com.hobart.hte.repositories;

import com.hobart.hte.utils.customer.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	Customer findByApiKeyAndApiSecret(String apiKey, String apiSecret);
}
