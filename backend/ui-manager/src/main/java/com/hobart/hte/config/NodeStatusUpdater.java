package com.hobart.hte.config;

import com.hobart.hte.helpers.NodeHelper;
import com.hobart.hte.repositories.RepoBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

// Basic singleton class to manage automatic refreshes of the Asset List nodes.
// I didn't want to bother with storing this data in the DB somewhere, so this in-memory object will do.
@Component
public class NodeStatusUpdater implements Runnable {

    @Autowired
    RepoBundle repoBundle;

    private static final Logger log = LogManager.getLogger(NodeStatusUpdater.class);

    private Timestamp lastUpdate; // Don't listen if your IDE says this should be final; it shouldn't
    private Timestamp nextUpdate;

    // TODO: Make these configurable and have them evaluated when startUpdateNodeScheduler() executes
    private final int maxSeconds = 180;  // Maximum seconds allocated to nodeStatus updates per hour
    private final int scalesCompiledPerSecond = 25; // Average number of scales updated by instantiateNodeStatus()...
    private long updateFrequencyInSeconds = 60;

    private static boolean updatingNodes = false;

    private NodeStatusUpdater() {
        long rightNow = System.currentTimeMillis();
        lastUpdate = new Timestamp(rightNow);
        nextUpdate = new Timestamp(rightNow);
    }

    public void startUpdateNodeScheduler() {

        long totalScales = repoBundle.device_repo.countDeviceId();
        if (totalScales == 0) {
            totalScales = 500; // We'll err on the safe side and update sparingly
        }

        // Believe it or not, web developers are capable of basic arithmetic
        updateFrequencyInSeconds = getUpdateFrequencySeconds(totalScales);

        log.info(
                "With current scale data and configurations, nodeStatus will be automatically updated every {} seconds",
                updateFrequencyInSeconds);

        ScheduledExecutorService statusUpdater = Executors.newScheduledThreadPool(1);
        statusUpdater.scheduleWithFixedDelay(
                this, 0, updateFrequencyInSeconds, TimeUnit.SECONDS);
    }

    private long getUpdateFrequencySeconds(long totalScales) {

        long estimatedScaleCalculationTime = totalScales / scalesCompiledPerSecond;
        if (estimatedScaleCalculationTime < 1) {estimatedScaleCalculationTime = 1;}

        long updateFrequencyQuotient = maxSeconds / estimatedScaleCalculationTime ;
        if (updateFrequencyQuotient > 60) {updateFrequencyQuotient = 60;} // One status update once per minute, tops

        return 3600 / updateFrequencyQuotient;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public Timestamp getNextUpdate() {
        return nextUpdate;
    }

    public static boolean getUpdatingState() {
        return updatingNodes;
    }

    public void run() {

        boolean failed = false;

        log.info(
                "Starting automatic nodeStatus update. Previous update time: {}", lastUpdate.toLocalDateTime());

        NodeHelper nodeHelper = new NodeHelper(repoBundle.banner_repo, repoBundle.region_repo, repoBundle.store_repo,
                repoBundle.dept_repo, repoBundle.device_repo, repoBundle.ssRepo, repoBundle.file_repo,
                repoBundle.nodeStatusRepo, repoBundle.storeDeptPairsRepository, repoBundle.configRepo);

        // Go!
        updatingNodes = true;
        try {
            nodeHelper.instantiateNodeStatus();
        } catch (Exception e) {
            log.warn("An error occurred during an attempt to automatically refresh node status...");
            failed = true;
        }

        updatingNodes = false;
        long rightNow = System.currentTimeMillis();

        lastUpdate.setTime(rightNow);
        nextUpdate.setTime(rightNow + (updateFrequencyInSeconds * 1000));

        if (!failed) {
            log.info("nodeStatus updated. Next update is due at {}", nextUpdate.toLocalDateTime());
        }
    }
}
