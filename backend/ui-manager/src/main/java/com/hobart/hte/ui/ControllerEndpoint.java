package com.hobart.hte.ui;

import org.apache.commons.lang3.NotImplementedException;

public class ControllerEndpoint<T> {
    T execute() throws NotImplementedException {
        throw new NotImplementedException("This method, execute, was not overridden");
    };

}
