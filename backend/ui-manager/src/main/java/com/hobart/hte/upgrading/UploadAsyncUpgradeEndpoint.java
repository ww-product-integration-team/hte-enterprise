package com.hobart.hte.upgrading;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.UpgradeDeviceRepository;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.model.OperationStatusModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UploadAsyncUpgradeEndpoint extends ControllerEndpoint<OperationStatusModel> {

    private static final Logger log = LogManager.getLogger(UpgradeController.class);
    private final DeviceRepository deviceRepo;
    private final UpgradeDeviceRepository upgradeDeviceRepo;

    public UploadAsyncUpgradeEndpoint(DeviceRepository deviceRepo, UpgradeDeviceRepository upgradeDeviceRepo) {
        this.deviceRepo = deviceRepo;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
    }

    public Map<String, Boolean> execute(List<String> ipAddresses, String fileName) {

        // Preliminary check to see whether the file is already present. The user may have SCP'd the file over to the
        // scale already -- if so, there's no need to tax the server with a redundant file upload.
        Map<String, Boolean> fileStatusMap = UpgradeHelper.checkFileStatuses(
                ipAddresses, fileName, true, true, this.deviceRepo, this.upgradeDeviceRepo);
        return fileStatusMap;
    }
}
