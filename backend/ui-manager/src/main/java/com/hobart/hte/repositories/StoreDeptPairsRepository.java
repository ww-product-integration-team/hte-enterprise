package com.hobart.hte.repositories;

import com.hobart.hte.utils.tree.StoreDeptPair;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StoreDeptPairsRepository extends CrudRepository<StoreDeptPair, String>{

    List<StoreDeptPair> findByStoreId(String storeId);

    List<StoreDeptPair> findByDeptId(String deptId);

    Boolean existsByStoreIdAndDeptId(String storeId, String deptId);

    @Query(value = "SELECT deptId FROM storeDeptPairs WHERE storeId = :storeId", nativeQuery = true)
    List<String> findDeptIdByStoreId(@Param("storeId") String storeId);

    void deleteByStoreId(String storeId);

    @Modifying
    @Transactional
    @Query("DELETE FROM StoreDeptPair WHERE deptId = :deptId")
    void deletePairsByDeptId(@Param("deptId") String deptId);

    @Transactional
    void deleteByStoreIdAndDeptId(String storeId, String deptId);
}
