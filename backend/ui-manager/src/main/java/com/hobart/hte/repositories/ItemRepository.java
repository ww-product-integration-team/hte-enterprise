package com.hobart.hte.repositories;

import com.hobart.hte.utils.item.HobItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<HobItem, Integer> {
	HobItem findByPrNumAndDeptNum(int prNum, int deptNum);
	HobItem findByPrNum(int prNum);
	Boolean existsByPrID(int prID);
}
