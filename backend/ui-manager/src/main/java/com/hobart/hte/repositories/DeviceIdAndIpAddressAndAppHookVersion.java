package com.hobart.hte.repositories;

public interface DeviceIdAndIpAddressAndAppHookVersion {
    String getDeviceId();
    String getIpAddress();
    String getAppHookVersion();
}
