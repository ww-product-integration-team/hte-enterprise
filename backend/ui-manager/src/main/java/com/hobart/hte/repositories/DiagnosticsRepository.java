package com.hobart.hte.repositories;

import com.hobart.hte.utils.access.Diagnostic;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface DiagnosticsRepository extends CrudRepository<Diagnostic, String> {

	List<Diagnostic> findByCustomerUUID(String customer_uuid);

	@Override
	List<Diagnostic> findAll();
}
