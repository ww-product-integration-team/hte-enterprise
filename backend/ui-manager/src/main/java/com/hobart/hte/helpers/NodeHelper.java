package com.hobart.hte.helpers;

import com.hobart.hte.repositories.*;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.file.HobFiles;
import com.hobart.hte.utils.model.NodeType;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.repositories.StoreDeptPairsRepository;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.tree.NodeStatus;
import com.hobart.hte.utils.tree.ScaleNode;
import com.hobart.hte.utils.tree.StoreDeptPair;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

public class NodeHelper {
    private static final Logger log = LogManager.getLogger(NodeHelper.class);
    private final BannerRepository bannerRepo;
    private final RegionRepository regionRepo;
    private final StoreRepository storeRepo;
    private final DepartmentRepository deptRepo;
    private final DeviceRepository deviceRepo;
    private final ScaleSettingsRepository ssRepo;
    private final FileRepository fileRepo;
    private final NodeStatusRepository nodeStatusRepo;
    private final StoreDeptPairsRepository storeDeptPairsRepository;
    private final ConfigAppRepository configAppRepository;

    public NodeHelper( BannerRepository bannerRepo,
                      RegionRepository regionRepo, StoreRepository storeRepo, DepartmentRepository deptRepo,
                      DeviceRepository deviceRepo, ScaleSettingsRepository ssRepo, FileRepository fileRepo,
                      NodeStatusRepository nodeStatusRepo, StoreDeptPairsRepository storeDeptPairsRepository,
                       ConfigAppRepository configAppRepository) {

        this.bannerRepo = bannerRepo;
        this.regionRepo = regionRepo;
        this.storeRepo = storeRepo;
        this.deptRepo = deptRepo;
        this.deviceRepo = deviceRepo;
        this.ssRepo = ssRepo;
        this.fileRepo = fileRepo;
        this.nodeStatusRepo = nodeStatusRepo;
        this.storeDeptPairsRepository = storeDeptPairsRepository;
        this.configAppRepository = configAppRepository;
    }
    private List<HobFiles> getProfileFiles(String profileId) {
        if(profileId == null){
            return null;
        }

        List<HobFiles> activeFiles = new ArrayList<>();
        List<FileForEvent> allFilesInProfile = fileRepo.findByProfileIdAndEnabled(profileId, true);

        Timestamp now = Timestamp.from(Instant.now());

        for (FileForEvent ffe : allFilesInProfile) {
            if ((ffe.getStartDate() == null && ffe.getEndDate() == null)
                    || (ffe.getStartDate().before(now) && ffe.getEndDate() == null)
                    || (ffe.getStartDate().before(now) && ffe.getEndDate().after(now))) {
                activeFiles.add(new HobFiles(ffe));
            }
        }
        return activeFiles;
    }

    private void createNodeStatus(ScaleNode node, String nodeId, String parentId) {
        NodeStatus status = null;

        switch (node.getType()) {
            case "banner":
                List<ScaleDevice> bannerDevices = deviceRepo.findByBannerId(nodeId);
                status = compileNodeStatusData(bannerDevices);
                status.setDeviceId(nodeId);
                status.setParentId("");
                status.setNodeType(NodeType.BANNER);
                break;
            case "region":
                List<ScaleDevice> regionDevices = deviceRepo.findByRegionId(nodeId);
                status = compileNodeStatusData(regionDevices);
                status.setDeviceId(nodeId);
                status.setParentId(parentId);
                status.setNodeType(NodeType.REGION);
                break;
            case "store":
                //optional constructor can be called
                List<ScaleDevice> storeDevices = deviceRepo.findByStoreId(nodeId);
                status = compileNodeStatusData(storeDevices);
                status.setDeviceId(nodeId);
                status.setParentId(parentId);
                status.setNodeType(NodeType.STORE);
                break;
            case "dept":
                // Node status UUID for dept-> <storeId>|<deptId>
                List<ScaleDevice> deptDevices = deviceRepo.findByStoreIdAndDeptId(parentId, nodeId);
                status = compileNodeStatusData(deptDevices);
                status.setDeviceId(parentId + "|" + nodeId);
                status.setParentId(parentId);
                status.setNodeType(NodeType.DEPT);
                break;
            default:
                status = null;
        }
        node.setNodeStatus(status);
    }
    private NodeStatus compileNodeStatusData(List<ScaleDevice> devices) {
        NodeStatus status = new NodeStatus();
        for (ScaleDevice device : devices) {

            String awayThreshold = configAppRepository.findById(AppConfig.scaleAwayHours).get().getCoValue();
            String offlineThreshold = configAppRepository.findById(AppConfig.scaleOfflineHours).get().getCoValue();

            NodeStatus deviceStatus = new NodeStatus(device);
            deviceStatus.setScaleReportStatus(device, awayThreshold, offlineThreshold);

            ScaleSettings settings = ssRepo.findById(device.getDeviceId()).orElse(null);
            deviceStatus.setScaleSync(device, settings, getProfileFiles(device.getProfileId()));
//            deviceStatus.setTimeSyncStatus(settings);
            status.AddChildStatus(deviceStatus);
        }
        return status;
    }
    public void instantiateNodeStatus(){

        log.info("Creating Nodes Statuses");
        // clean node status repo
        log.info("cleaning table");
        nodeStatusRepo.deleteAll();

        log.info("Creating banner status");
        List<NodeStatus> statuses =  new ArrayList<>();
        List<String> deptIds = new ArrayList<>();
        Iterable<Banner> banners = bannerRepo.findAll();
        for (Banner banner : banners) {
            ScaleNode node = new ScaleNode(banner);
            createNodeStatus(node, banner.getBannerId(), null);
            statuses.add(node.getNodeStatus());
        }

        log.info("Creating region status");
        // create node status for regions
        Iterable<Region> regions = regionRepo.findAll();
        for (Region region : regions) {
            ScaleNode node = new ScaleNode(region);
            createNodeStatus(node, region.getRegionId(), region.getBannerId());
            statuses.add(node.getNodeStatus());
        }

        log.info("Creating store status");
        // create node status for stores
        Iterable<Store> stores = storeRepo.findAll();
        for (Store store : stores) {
            ScaleNode node = new ScaleNode(store);
            createNodeStatus(node, store.getStoreId(), store.getRegionId());
            statuses.add(node.getNodeStatus());
        }

        // This used to happen *before* the scale statuses below, oddly. In case the saveAll has issues with one or
        // more of the scales -- perhaps due to a concurrent heartbeat -- I've moved this up in the sequence.
        log.info("Creating depts status");
        Iterable<StoreDeptPair> storeDeptPairs = storeDeptPairsRepository.findAll();
        for (StoreDeptPair storeDeptPair : storeDeptPairs) {
            try {
                String storeId = storeDeptPair.getStoreId();
                String deptId = storeDeptPair.getDeptId();
                if (deptId.equals(AppConfig.DefaultUnassignedId)) {
                    continue;
                }
                Department dept = deptRepo.findById(deptId).orElse(null);;
                if (dept == null) {
                    continue;
                }
                ScaleNode node = new ScaleNode(dept);
                node.setId(storeId + "|" + deptId);
                createNodeStatus(node, deptId, storeId);
                statuses.add(node.getNodeStatus());
            } catch (Exception e) {
                log.info("Error saving statuses" + e.getMessage());
                throw e;
            }
        }

        // I don't think we need this; feel free to uncomment it if its absence breaks something, though.
        /*
        log.info("Creating scale status");
        // create node status for scales
        Iterable<ScaleDevice> scales = deviceRepo.findAll();
        for (ScaleDevice scale : scales) {
            String deptNodeId = scale.getStoreId() + "|" + scale.getDeptId();
            ScaleNode node = new ScaleNode(scale);
            node.setNodeStatus(new NodeStatus(scale));

            statuses.add(node.getNodeStatus());
            if (!deptIds.contains(deptNodeId)) {
                deptIds.add(deptNodeId);
            }
        }
         */

        log.info("Saving statuses");
        try {
            nodeStatusRepo.saveAll(statuses);
        } catch(Exception e) {
            log.info("Error saving statuses " + e.getMessage());
            throw e;
        }
    }

    public void updateNodeStatus(ScaleNode node) {

        String awayThreshold = configAppRepository.findById(AppConfig.scaleAwayHours).get().getCoValue();
        String offlineThreshold = configAppRepository.findById(AppConfig.scaleOfflineHours).get().getCoValue();

        String nodeId = node.getId();
        NodeStatus status = new NodeStatus();
        ScaleNode parentNode = null;
        switch (node.getType()) {
            case "banner":
                List<ScaleDevice> bannerDevices = deviceRepo.findByBannerId(nodeId);
                status = compileNodeStatusData(bannerDevices);
                status.setDeviceId(nodeId);
                status.setParentId("");
                status.setNodeType(NodeType.BANNER);
                break;
            case "region":
                Region r = regionRepo.findById(nodeId).orElse(null);
                if (r == null) {
                    throw new RuntimeException("Region does not exist!");
                }
                List<ScaleDevice> regionDevices = deviceRepo.findByRegionId(nodeId);
                status = compileNodeStatusData(regionDevices);
                status.setDeviceId(nodeId);
                status.setParentId(r.getBannerId());
                status.setNodeType(NodeType.REGION);
                Banner b = bannerRepo.findById(r.getBannerId()).orElse(null);
                if (b == null) {
                    throw new RuntimeException("Region's banner does not exist!");
                }
                parentNode = new ScaleNode(b);
                break;
            case "store":
                if(nodeId.equals(AppConfig.DefaultUnassignedId)){
                    break;
                }
                Store s = storeRepo.findById(nodeId).orElse(null);
                if (s == null) {
                    throw new RuntimeException("Store does not exist!");
                }
                List<ScaleDevice> storeDevices = deviceRepo.findByStoreId(nodeId);
                status = compileNodeStatusData(storeDevices);
                status.setDeviceId(nodeId);
                status.setParentId(s.getRegionId());
                status.setNodeType(NodeType.STORE);
                Region storeRegion = regionRepo.findById(s.getRegionId()).orElse(null);
                if( storeRegion == null){
                    throw new NullPointerException("Store's region does not exist!");
                }
                parentNode = new ScaleNode(storeRegion);
                break;
            case "dept":
                // Node status UUID for dept-> <storeId>|<deptId>
                if(nodeId.equals(AppConfig.DefaultUnassignedId)){
                    break;
                }
                List<String> pathing = node.getPath();
                if(!nodeId.contains("|") && pathing.stream().noneMatch(path -> path.contains("|"))){
                    throw new IllegalArgumentException("Dept id is not a valid Id");
                }
//                String storeDeptId = pathing.get(3);
                String storeDeptId = nodeId;
                List<ScaleDevice> deptDevices = deviceRepo.findByStoreIdAndDeptId(storeDeptId.split("\\|")[0], storeDeptId.split("\\|")[1]);
                status = compileNodeStatusData(deptDevices);
                status.setDeviceId(storeDeptId);
                status.setParentId(storeDeptId.split("\\|")[0]);
                status.setNodeType(NodeType.DEPT);
                Store deptStore = storeRepo.findById(storeDeptId.split("\\|")[0]).orElse(null);
                if(deptStore == null){
                    throw new NullPointerException("Department's store does not exist!");
                }
                parentNode = new ScaleNode(deptStore);
                break;
            case "scale":
                ScaleDevice scale = deviceRepo.findById(nodeId).orElse(null);
                if (scale == null) {
                    throw new RuntimeException("Scale does not exist!");
                }
                status = new NodeStatus(scale);
                status.setScaleReportStatus(scale, awayThreshold, offlineThreshold)
                ;
                Department scaleDept = deptRepo.findById(scale.getDeptId()).orElse(null);
                if(scaleDept == null){
                    throw new NullPointerException("Scale's department does not exist!");
                }
                parentNode = new ScaleNode(scaleDept);
                parentNode.setId(scale.getStoreId()+"|"+scale.getDeptId());
                break;
            default:
                status = null;
        }
        if(status == null){
            throw new NullPointerException("Node status was not created!");
        }
        node.setNodeStatus(status);
        nodeStatusRepo.save(status);
        if (parentNode != null) {
            updateNodeStatus(parentNode);
        }
    }
}


