package com.hobart.hte.helpers;

import com.hobart.hte.repositories.ConfigAppRepository;
import com.hobart.hte.repositories.LicenseRepository;
import com.hobart.hte.ui.UIController;
import com.hobart.hte.utils.access.HTeDomain;
import com.hobart.hte.utils.access.HTeUser;
import com.hobart.hte.utils.access.License;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.util.HteTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Service
public class LicenseHelper {
    private static final Logger log = LogManager.getLogger(LicenseHelper.class);

    @Autowired
    LicenseRepository license_repo;

    @Autowired
    ConfigAppRepository configRepo;

    public LicenseHelper() {
    }

     public static String LICENSE_PATH = HteTools.isUnix() ?
     "/home/hobart/license" : "C:\\Users\\Public\\Documents\\hte\\license";
    public static final String ENTERPRISE_LICENSE_ID = "enterprise";

    @PostConstruct
    public void init() {
        ConfigEntry licenseEntry = configRepo.findById(AppConfig.licensePath).orElse(null);
        if (licenseEntry != null && licenseEntry.getCoValue() != null) {
            LICENSE_PATH = licenseEntry.getCoValue();
        }
    }

    private static final String COMPANY_KEY = "HoBaRt241@#@-_@%$#)(^Products";

    private static final String individualName = "individual name";
    private static final String companyName = "company name";
    private static final String companyAddress = "company address";
    private static final String productNames = "product names";
    private static final String licenseType = "license type";
    private static final String licenseActivation = "license activation";
    private static final String licenseActivated = "license activated";
    private static final String licenseAllotment = "license allotment";
    private static final String licenseTimeout = "license timeout";
    private static final String licenseKey = "license key";
    private static final String userEmail = "user email";
    private static final String numScales = "num scales";
    private static final String numUsers = "num users";
    private static final String enterpriseKey = "enterprise key";

    private static List<String> HTE_PRODUCTS = Arrays.asList("HTE_ALERTS",
            "HTE_FLASHKEY_DESIGNER",
            "HTE_LABEL_DESIGNER",
            "HTE_LEGACY_SUPPORT",
            "HTE_MEDIA_CONTENT_MANAGER",
            "HTE_REMOTE_MONITOR",
            "HTE_REMOTESCALEACCESS",
            "HTE_SCALEMGMT_LITE",
            "HTE_SCALEMGMT_MULTISTORE",
            "HTE_SCALEMGMT_SINGLESTORE",
            "HTE_SCALEMGMT_ENTERPRISE",
            "HTE_USBKEYGEN");

    private static List<String> SCALE_PRODUCTS = Arrays.asList("SCALE_AD_SERVICE",
            "SCALE_CAT3_AUDITTRAIL",
            "SCALE_CODE_CHECKER", // requires ARK hardware
            "SCALE_DIGIMARC",
            "SCALE_FPP",
            "SCALE_FULLSCREEN_FLASHKEYS",
            "SCALE_HANDSFREEDEVICE", // requires HFPD hardware and feature to be installed
            "SCALE_INTERNATIONAL_ROUNDING",
            "SCALE_MEAL_BUILDER",
            "SCALE_NOW_SERVING",
            "SCALE_OFFICE_VIEWER",
            "SCALE_PDF_VIEWER", // requires feature to be installed
            "SCALE_PICK5",
            "SCALE_PRINTER_EPP",
            "SCALE_REMOTE_PRINTING",
            "SCALE_REMOTE_MONITOR",
            "SCALE_TRANS_GCD",
            "SCALE_WEB_BROWSER",
            "SCALE_ENHANCED_CENTERSCREEN",
            "SCALE_ONLINE_ORDERING");

    private static Map<String, String> PROMPTS;
    static {
        PROMPTS = new HashMap<>();

        PROMPTS.put(individualName, "Responsible Individual Full Name");
        PROMPTS.put(companyName, "Company Name");
        PROMPTS.put(companyAddress, "Company Address");
        PROMPTS.put(productNames, "Product Name(s)");
        PROMPTS.put(licenseType, "License Key Type");
        PROMPTS.put(licenseActivation, "License Activation Date");
        PROMPTS.put(licenseActivated, "License Activated");
        PROMPTS.put(licenseAllotment, "License Allotment");
        PROMPTS.put(licenseTimeout, "License Timeout");
        PROMPTS.put(licenseKey, "License Key");
        PROMPTS.put(userEmail, "Email");
        PROMPTS.put(numScales, "Number of Scales");
        PROMPTS.put(numUsers, "Number of Users");
        PROMPTS.put(enterpriseKey, "Enterprise Key");
    }

    public static Pair<License, Map<String, String>> getLicenseElements(MultipartFile licenseFile)
            throws IOException, ParseException {
        String content = new String(licenseFile.getBytes());

        List<String> items = Arrays.asList(content.split("\\s*\n\\s*"));
        Map<String, String> elements = new HashMap<>();
        for (String item : items) {
            List<String> line = Arrays.asList(item.split("\\s*:\\s*"));
            if (line.size() == 2) {
                String type = line.get(0).trim();
                String value = line.get(1).trim();

                for (Map.Entry<String, String> entry : PROMPTS.entrySet()) {
                    if (type.equals(entry.getValue())) {
                        elements.put(entry.getKey(), value);
                    }
                }

            }
        }
        log.info("License Install:");
        // log.info(elements);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Timestamp activationTimestamp = new Timestamp((dateFormat.parse(elements.get(licenseActivation))).getTime());
        String licenseAndEnterpriseKeys = elements.get(licenseKey) + "|" + elements.get(enterpriseKey);
        String uuid = UUID.randomUUID().toString();

        License newLicense = new License(uuid, ENTERPRISE_LICENSE_ID, licenseFile.getOriginalFilename(),
                elements.get(userEmail), elements.get(individualName), elements.get(companyName),
                elements.get(companyAddress), activationTimestamp, Integer.parseInt(elements.get(licenseTimeout)),
                elements.get(productNames), Integer.parseInt(elements.get(numUsers)),
                Integer.parseInt(elements.get(numScales)), null, null, null, Timestamp.from(Instant.now()),
                null, licenseAndEnterpriseKeys);

        log.info(newLicense.toString());

        return Pair.of(newLicense, elements);
    }

    public static OperationStatusModel validateLicense(Map<String, String> elements, LicenseRepository licenseRepo) {
        OperationStatusModel result = new OperationStatusModel("validateLicense");

        try {
            // General License
            // ==========================================================================================================
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            String digestString = elements.get(individualName) +
                    elements.get(companyName) +
                    elements.get(companyAddress);

            // Get Products
            String[] products = elements.get(productNames).split("\\s*,\\s*");
            for (String product : products) {
                digestString += product;
            }

            digestString += elements.get(licenseType);
            digestString += elements.get(licenseActivation);
            digestString += elements.get(licenseTimeout);
            digestString += "IGNORE"; // MAC Address
            digestString += ""; // Serial Number
            digestString += COMPANY_KEY;

            byte[] messageDigest = md.digest(digestString.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 40) {
                hashtext = "0" + hashtext;
            }

            if (!hashtext.equalsIgnoreCase(elements.get(licenseKey))) {
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("License checksums do not match");
                return result;
            }

            // Enterprise License
            // ==========================================================================================================
            digestString = elements.get(userEmail) +
                    elements.get(numUsers) +
                    elements.get(numScales) +
                    COMPANY_KEY;
            messageDigest = md.digest(digestString.getBytes());
            no = new BigInteger(1, messageDigest);
            String ehashText = no.toString(16);
            while (ehashText.length() < 40) {
                ehashText = "0" + ehashText;
            }

            if (!ehashText.equalsIgnoreCase(elements.get(enterpriseKey))) {
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Enterprise License checksums do not match");
                return result;
            }

            String licenseAndEnterpriseKeys = elements.get(licenseKey) + "|" + elements.get(enterpriseKey);
            Iterable<License> all = licenseRepo.findAll();
            for (License lic : all) {
                if (lic.getLicenseAndEnterpriseKeys().equals(licenseAndEnterpriseKeys)) {
                    result.setResult(RequestOperationResult.ERROR.name());
                    result.setErrorDescription("License has already been uploaded");
                    return result;
                }
            }

            result.setResult(RequestOperationResult.SUCCESS.name());
            return result;
        } catch (Exception e) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error parsing out license elements");
            return result;
        }
    }

    public static HTeUser generateUser(Map<String, String> elements, HTeDomain domain, int accessLevel) {
        HTeUser newUser = new HTeUser();
        newUser.setEnabled(true);
        newUser.setDateCreated((Timestamp.from(Instant.now())));
        newUser.setUsername(elements.get(userEmail));
        newUser.setAccessLevel(accessLevel);
        newUser.setDomain(domain);
        String tmp = UUID.randomUUID().toString();
        newUser.setId(tmp);

        String fullname = elements.get(individualName);
        fullname = fullname.charAt(0) + fullname.substring(1).toLowerCase();
        String[] firstAndLast = fullname.split(" ", 2);
        newUser.setFirstName(firstAndLast[0]);
        if (firstAndLast.length > 1) {
            newUser.setLastName(firstAndLast[1]);
        }

        return newUser;
    }

    private License findLicense() {
        List<License> licenses = license_repo.findActiveLicenses();
        License enterpriseLic = null;
        if (licenses != null && !licenses.isEmpty()) {
            if (licenses.size() > 1) {
                License closest = null;
                Timestamp now = Timestamp.from(Instant.now());
                for (License lic : licenses) {
                    if (closest != null) {
                        Long closestTime = now.getTime() - closest.getActivationDate().getTime();
                        Long newTime = now.getTime() - lic.getActivationDate().getTime();
                        if (closestTime > newTime) {
                            closest = lic;
                        }
                    } else {
                        closest = lic;
                    }
                }
                enterpriseLic = closest;
            } else {
                enterpriseLic = licenses.get(0);
            }
        }

        return enterpriseLic;
    }

    public boolean validLicenseInstalled() {
        License enterpriseLic = findLicense();

        if (enterpriseLic == null) {
            return false;
        }

        try {
            long currentTime = new Date().getTime();

            // Get Activation date as a long
            long expiryTime = enterpriseLic.getActivationDate().getTime();
            // Then add the timeout(days converted to millis)
            expiryTime += (long) (enterpriseLic.getTimeout() * 8.64E+7);

            // License is still valid as long as expiry time is greater than the current
            // time
            return expiryTime > currentTime;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean canAssignScale(Iterable<ScaleDevice> devices) {
        License enterpriseLic = findLicense();
        if (enterpriseLic == null) {
            return false;
        }

        int assignedScaleCount = 0;
        for (ScaleDevice device : devices) {
            // Filter out devices which are assigned and are not invisible scales
            if (!device.getDeptId().equals(AppConfig.DefaultUnassignedId) &&
                    !device.getStoreId().equals(AppConfig.DefaultUnassignedId) &&
                    !device.isInvisibleScale()) {
                assignedScaleCount += 1;
            }
        }

        // Return true if the number of allowed scales is higher than the number
        // actually assigned (Note: Needs to be only > because you are adding one more
        // scale)
        // OR
        // If the number of allowed scales is 0 i.e. no limit on scales
        return enterpriseLic.getNumScales() > assignedScaleCount || enterpriseLic.getNumScales() == 0;
    }

    public boolean canCreateUser(long userCount) {
        License enterpriseLic = findLicense();
        if (enterpriseLic == null) {
            return false;
        }

        // Subtracting 1 for a default hobart user
        return enterpriseLic.getNumUsers() >= userCount - 1 || enterpriseLic.getNumUsers() == 0;
    }
}
