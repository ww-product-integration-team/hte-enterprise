package com.hobart.hte.repositories;

import com.hobart.hte.utils.item.PricingZone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PricingZoneRepository extends CrudRepository<PricingZone, String> {

    boolean existsByName(String name);
}
