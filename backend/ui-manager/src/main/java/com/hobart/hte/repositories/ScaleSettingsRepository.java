package com.hobart.hte.repositories;

import com.hobart.hte.utils.device.ScaleSettings;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface ScaleSettingsRepository extends CrudRepository<ScaleSettings, String> {

	ScaleSettings findByIpAddress(String id);

	@Override
	void deleteAll();

	List<DeviceIdAndIpAddressAndAppHookVersion> findDeviceIdAndIpAddressAndAppHookVersionByAppHookVersionIsNotNull();
}

