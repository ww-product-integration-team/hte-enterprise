package com.hobart.hte.repositories;

import java.util.List;

import com.hobart.hte.utils.store.Store;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface StoreRepository extends CrudRepository<Store, String> {
	List<Store> findByRegionId(@Param("regionId") String regionId);

	Boolean existsByStoreNameIgnoreCase(String name);

	@Override
	void deleteAll();

	Store findByStoreIdAndRegionId(String storeId, String regionId);

//	@Query("SELECT sd FROM ScaleDevice AS sd LEFT JOIN Store AS s ON s.storeId = sd.storeId LEFT JOIN Region AS r " +
//			"ON r.regionId = s.regionId LEFT JOIN Banner AS b ON b.bannerId = r.bannerId WHERE b.bannerId = :bannerId")
	@Query("SELECT s FROM Store AS s LEFT JOIN Region AS r " +
			"ON r.regionId = s.regionId LEFT JOIN Banner AS b ON b.bannerId = r.bannerId WHERE b.bannerId = :bannerId")
	Iterable<Store> findByBannerId(@Param("bannerId") String bannerId);
	// A query that tries to find a store's parent region, assuming one exists
	// This is banking on the user not having identically named stores
	// If two or more stores share the same name, this will return nothing due to the aggregation clause
	@Query(value = "SELECT regionID\n" +
			"FROM store\n" +
			"WHERE storeName = :storeName\n" +
			"GROUP BY regionID\n" +
			"HAVING COUNT(*) = 1\n" +
			"LIMIT 1", nativeQuery = true)
	String findRegionIDFromStoreName(@Param("storeName") String store);

	Store findByStoreNameIgnoreCase(String name);
}
