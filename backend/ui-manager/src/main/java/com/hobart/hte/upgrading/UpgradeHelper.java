package com.hobart.hte.upgrading;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.UpgradeDeviceRepository;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import com.hobart.hte.utils.util.HteTools;
import com.jcraft.jsch.Session;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UpgradeHelper {

    public boolean isFileGTType(String filename) {
        for (String gtType : HteTools.validGTExtensions) {
            if (filename.endsWith(gtType) && !filename.endsWith(".package.tgz")) {
                return true;
            }
        }
        return false;
    }

    /**
     * The key of the map is the end result of the upgrade, the file version. The value of the map is a start and stop range
     * of all the device versions that can be upgraded to the file version
     * @return The versions that are compatible with each other for upgrading
     */
    private Map<String, List<String>> getCompatibleVersions() {
        Map<String, List<String>> compatibleVersions = new HashMap<>();
        compatibleVersions.put("3.2.0", Arrays.asList("3.0.0", "3.1.*"));
        compatibleVersions.put("4.1.100", Arrays.asList("4.*.*", "4.*.*"));
        compatibleVersions.put("4.1.300", Arrays.asList("4.*.*", "4.*.*"));
        compatibleVersions.put("4.2.0", Arrays.asList("4.*.*", "4.*.*"));

        return compatibleVersions;
    }

    private boolean compareVersions(String start, String end) {
        String[] startNums = start.split("\\.");
        String[] endNums = end.split("\\.");
        if (Integer.parseInt(startNums[0]) != Integer.parseInt(endNums[0])) {
            return false;
        }
        if (!startNums[1].equals("*") && !endNums[1].equals("*") &&
                Integer.parseInt(startNums[1]) > Integer.parseInt(endNums[1])) {
            return false;
        }
        if (!startNums[1].equals("*") && !endNums[1].equals("*") &&
                Integer.parseInt(startNums[2]) > Integer.parseInt(endNums[2])) {
            return false;
        }
        return true;
    }

    public boolean areFirmwaresCompatible(String fileFirmware, String deviceFirmware) {
        List<String> versionRange = getCompatibleVersions().get(fileFirmware);
        if (versionRange == null) {
            return false;
        }
        String start = versionRange.get(0);
        String end = versionRange.get(1);
        return compareVersions(start, deviceFirmware) && compareVersions(deviceFirmware, end);
    }

    public String getFileFirmwareVersion(String fileName) {
        Pattern pattern = Pattern.compile("\\d+\\.\\d+\\.\\d+");
        Matcher matcher = pattern.matcher(fileName);
        String fileFirmwareVersion = null;
        if (matcher.matches() || matcher.find()) {
            fileFirmwareVersion = matcher.group();
        }
        return fileFirmwareVersion;
    }

    public static Map<String, Boolean> checkFileStatuses(
            List<String> ipAddresses, String fileName, boolean uploadFileIfNotPresent, boolean firstUpload,
            DeviceRepository deviceRepo, UpgradeDeviceRepository upgradeDeviceRepo) {

        Map<String, Boolean> scaleCheckStatuses = new HashMap<>(); // IPv4 address, HTTP code
        for (String ipAddress : ipAddresses) {
            Session sshSession = HteTools.getSSHSession(ipAddress);
            ScaleDevice deviceToUpdate = deviceRepo.findByIpAddress(ipAddress);

            if (firstUpload) { // Only change status if it's our first go; otherwise, leave the status alone
                upgradeDeviceRepo.updateStatusByDeviceId(UpgradeDevice.Status.upload_in_progress.name(),
                        "Uploading file to scale, please wait...", deviceToUpdate.getDeviceId());
            }

            if (sshSession != null && sshSession.isConnected()) {

                // I know there's already a method to do this in HteTools, but I'd rather use this session to get it
                // After all, we're already in the scale -- why make another SSH connection?
                String scaleOperatingSystemInfo = HteTools.executeSSHCommandGetOutput(
                        sshSession, "cd /etc ; cat os-version");
                boolean isHTScale = scaleOperatingSystemInfo.contains("Timesys");
                String upgradeDir = isHTScale ? "/usr/local/hobart/upgrade/" : "/opt/hobart/upgrade/";

                // Bash command to see if the file is there; returns a boolean-esque value
                String checkFileCommand = "test -f " + upgradeDir + fileName + " && echo '1' || echo '0'";
                String output = HteTools.executeSSHCommandGetOutput(
                        sshSession, checkFileCommand).replace("\n", "");

                // The file is there in name -- but is it the right size? Let's check.
                if (output != null && output.equals("1")) {
                    File localFile = determineLocalFilePath(fileName);
                    boolean correctFileSize = fileIsTheRightSize(upgradeDir, localFile, ipAddress);

                    if (correctFileSize) {
                        scaleCheckStatuses.put(ipAddress, true); // The file really is there! No further action.
                        upgradeDeviceRepo.updateStatusByDeviceId(UpgradeDevice.Status.ready_to_upgrade.name(),
                                "File upload complete; awaiting upgrade command.",
                                deviceToUpdate.getDeviceId());
                    }
                    else {
                        output = "0"; // Aha, the file wasn't *really* there. Time to re-upload it.
                    }
                }
                // No file or the file has issues? Let's (re-)upload it then.
                if (output != null && output.equals("0") && uploadFileIfNotPresent) {
                    boolean fileUploaded = handleFileUpload(upgradeDir, fileName, ipAddress);
                    if (fileUploaded) {
                        scaleCheckStatuses.put(ipAddress, true);
                        upgradeDeviceRepo.updateStatusByDeviceId(UpgradeDevice.Status.ready_to_upgrade.name(),
                                "File upload complete; awaiting upgrade command.",
                                deviceToUpdate.getDeviceId());
                    } else {
                        scaleCheckStatuses.put(ipAddress, false);
                        upgradeDeviceRepo.updateStatusByDeviceId(
                                UpgradeDevice.Status.upload_failed.name(), "File upload was unsuccessful.",
                                deviceToUpdate.getDeviceId());
                    }
                }
            } else { // The connection failed for some reason, so the status is unknown. We'll assume it's not there.
                scaleCheckStatuses.put(ipAddress, false);
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upload_failed.name(),
                        "File upload was unsuccessful; session was likely interrupted.",
                        deviceToUpdate.getDeviceId());
            }
            HteTools.disconnectSSH(sshSession);
        }
        return scaleCheckStatuses;
    }

    private static File determineLocalFilePath(String fileName) {
        if (HteTools.isWindows()) {
            return new File("C:\\Users\\Public\\Documents\\hte\\repository\\upgrades\\" + fileName);
        } else if (HteTools.isUnix()) {
            return new File("/home/hobart/repository/upgrades/" + fileName);
        } else {
            return null;
        }
    }

    private static boolean handleFileUpload(String upgradeDir, String fileName, String ipAddress) {
        File localUpgradeFilePath = determineLocalFilePath(fileName);
        Session sshSession = HteTools.getSSHSession(ipAddress);
        boolean fileUploaded = HteTools.putFileSFTP(sshSession, localUpgradeFilePath, upgradeDir);
        return fileUploaded;
    }

    private static boolean fileIsTheRightSize(String upgradeDir, File file, String ipAddress) {
        long localFileSize = file.length();
        String fileName = file.getName();
        String scaleUpgradeFilePath = upgradeDir + fileName;
        Session sshSession = HteTools.getSSHSession(ipAddress);

        if (sshSession != null && sshSession.isConnected()) {
            String checkSizeCommand = "ls -l " + scaleUpgradeFilePath + " | cut -d ' ' -f 5";
            String sizeOutput = HteTools.executeSSHCommandGetOutput(
                    sshSession, checkSizeCommand).replace("\n", "");

            Long scaleFileSize;
            if (sizeOutput != null && sizeOutput != "" ) {
                scaleFileSize = Long.parseLong(sizeOutput);
            } else {
                return false; // It can't be the right size; it's not even on the scale!
            }

            if (scaleFileSize >= localFileSize) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
