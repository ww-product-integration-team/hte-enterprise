package com.hobart.hte.repositories;

import com.hobart.hte.utils.upgrade.FileIdAndName;
import com.hobart.hte.utils.upgrade.UpgradeBatch;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UpgradeBatchRepository extends CrudRepository<UpgradeBatch, String> {

    Boolean existsByNameIgnoreCase(String name);

    @Query("SELECT COUNT(ud.batchId) FROM UpgradeBatch AS ub LEFT JOIN UpgradeDevice AS ud ON ub.batchId = ud.batchId WHERE ub.batchId = :batchId")
    Integer countAssignmentsByBatchId(@Param("batchId") String batchId);
}
