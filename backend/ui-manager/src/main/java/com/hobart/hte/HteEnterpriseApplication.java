package com.hobart.hte;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HteEnterpriseApplication extends SpringBootServletInitializer {
	private static final Logger log = LogManager.getLogger(HteEnterpriseApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(HteEnterpriseApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) { 
		return application.sources(HteEnterpriseApplication.class);
	}
}
