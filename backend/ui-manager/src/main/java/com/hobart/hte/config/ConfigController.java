package com.hobart.hte.config;

import com.hobart.hte.repositories.ConfigAppRepository;
import com.hobart.hte.repositories.EventRulesRepository;
import com.hobart.hte.repositories.ServiceRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.event.EventConfig;
import com.hobart.hte.utils.event.EventRule;
import com.hobart.hte.utils.heartbeat.DeleteEvent;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.model.RequestOperationType;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.util.HteTools;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.*;

@Tag(name = "config", description = "Endpoints for modifying the runtime behavior of the server.")
@RestController
@RequestMapping("/config")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class ConfigController {
    private static final Logger log = LogManager.getLogger(ConfigController.class);

    @Autowired
    ConfigAppRepository configRepo;

    @Autowired
    EventRulesRepository eventRuleRepo;

    @Autowired
    ServiceRepository serviceRepo;

    private ConfigEntry getConfigEntry(String key, String defaultValue) {
        return configRepo.findById(key).orElse(new ConfigEntry(key, defaultValue));
    }

    private void validateConfigPath(String key, String folderName) {
        ConfigEntry pathEntry = getConfigEntry(key, "asdf");
        if (!HteTools.isValidPath(pathEntry.getCoValue())) {
            if (HteTools.isWindows()) {
                pathEntry.setCoValue("C:\\Users\\Public\\Documents\\hte\\" + folderName);
            } else {
                pathEntry.setCoValue("/home/hobart/" + folderName);
            }
            log.warn("    Setting Default Path for folder: " + key + " to default value:" + pathEntry.getCoValue());

            try {
                configRepo.save(pathEntry);
            } catch (Exception ex) {
                log.error("Error saving path: " + key + "  | " + ex);
            }
        }
    }

    @Operation(summary = "Updates the config table", description = "Creates config entries if they do not exist or their value is invalid")
    @PostMapping(path = "/initializeConfigTable")
    public ResponseEntity<OperationStatusModel> initializeConfigTable() {
        OperationStatusModel result = new OperationStatusModel(RequestOperationType.CREATE.name());
        log.info("Validating Config Table Entries...");

        ConfigEntry defaultPort = getConfigEntry(AppConfig.defaultPort, "8080");
        if (!HteTools.isIntegerAndInRange(defaultPort.getCoValue(), 0, 65535)) {
            // If the default port is not a number between 0->65535
            log.warn("    Setting defaultPort to default value: 8080");
            defaultPort.setCoValue("8080");
        }

        ConfigEntry httpMethod = getConfigEntry(AppConfig.httpMethod, "http");
        if (!httpMethod.getCoValue().equals("http") && !httpMethod.getCoValue().equals("https")) {
            // If the http method is not "http" or "https" set it to http
            log.warn("    Setting httpMethod to default value: http");
            httpMethod.setCoValue("http");
        }

        ConfigEntry heartbeatOperation = getConfigEntry(AppConfig.heartbeatOperation, "HQ");
        if (!heartbeatOperation.getCoValue().equals("HQ")) {
            // Heartbeat Operation can only be HQ if ui-manager is pointing to the same DB
            log.warn("    Setting heartbeatOperation to default value: HQ");
            heartbeatOperation.setCoValue("HQ");
        }

        validateConfigPath(AppConfig.donePath, "hte_done");
        validateConfigPath(AppConfig.importPath, "hte_import");
        validateConfigPath(AppConfig.licensePath, "license");
        validateConfigPath(AppConfig.logRootPath, "log");
        validateConfigPath(AppConfig.repositoryPath, "repository");

        ConfigEntry interval = getConfigEntry(AppConfig.scaleDeleteInterval, "Daily");
        List<String> intervalTypes = Arrays.asList("Monthly", "Weekly", "Daily", "Never");
        if (!intervalTypes.contains(interval.getCoValue())) {
            // Interval can only one of the following : "Monthly", "Weekly", "Daily",
            // "Never"
            log.warn("    Setting scaleDeleteInterval to default value: Daily");
            interval.setCoValue("Daily");
        }

        ConfigEntry time = getConfigEntry(AppConfig.scaleDeleteTime, "3");
        if (!HteTools.isIntegerAndInRange(time.getCoValue(), 0, 23)) {
            // If the default port is not a number between 0->65535
            log.warn("    Setting scaleDeleteTime to default value: 3");
            time.setCoValue("3");
        }

        ConfigEntry day = getConfigEntry(AppConfig.scaleDeleteDay, "1");
        if (!HteTools.isIntegerAndInRange(day.getCoValue(), 1, 7)) {
            // If the default port is not a number between 0->65535
            log.warn("    Setting scaleDeleteDay to default value: 1");
            day.setCoValue("1");
        }

        ConfigEntry items = getConfigEntry(AppConfig.scaleDeleteItems, "Daily");
        DeleteEvent tmpEvent = new DeleteEvent(items, "");
        String prefix = "";
        StringBuilder filteredItems = new StringBuilder();
        for (String item : tmpEvent.getDeleteTypes()) {
            filteredItems.append(prefix);
            filteredItems.append(item);
            prefix = ",";
        }
        items.setCoValue(filteredItems.toString());

        ConfigEntry sendStoreInfo = getConfigEntry(AppConfig.sendStoreInfo, "true");
        if (!sendStoreInfo.getCoValue().equals("true") && !sendStoreInfo.getCoValue().equals("false")) {
            // If the sendStoreInfo is not equal to either true or false then set it to the
            // default : true
            log.warn("    Setting sendStoreInfo to default value: true");
            sendStoreInfo.setCoValue("true");
        }

        ConfigEntry customStoreName = getConfigEntry(AppConfig.customStoreName, null);

        ConfigEntry awayHours = getConfigEntry(AppConfig.scaleAwayHours, "12");
        if (!HteTools.isIntegerAndInRange(awayHours.getCoValue(), 2, 95)) {
            // If the default port is not a number between 0->65535
            log.warn("    Setting scaleAwayHours to default value: 12");
            awayHours.setCoValue("12");
        }

        ConfigEntry offlineHours = getConfigEntry(AppConfig.scaleOfflineHours, "24");
        if (!HteTools.isIntegerAndInRange(offlineHours.getCoValue(), Integer.parseInt(awayHours.getCoValue()) + 1,
                96)) {
            // If the default port is not a number between 0->65535
            log.warn("    Setting scaleOfflineHours to default value: 24");
            offlineHours.setCoValue("24");
        }

        ConfigEntry validateRecoveryFiles = getConfigEntry(AppConfig.validateRecoveryFiles, "false");
        if (!validateRecoveryFiles.getCoValue().equals("false")) {
            log.warn("    Setting validateRecoveryFiles to default value: false");
            validateRecoveryFiles.setCoValue("false");
        }

        ConfigEntry hybridMultipath = getConfigEntry(AppConfig.hybridMultipath, "false");
        if (!hybridMultipath.getCoValue().equals("false")) {
            log.warn("    Setting hybridMultipath to default value: false");
            hybridMultipath.setCoValue("false");
        }

        ConfigEntry zonePricing = getConfigEntry(AppConfig.zonePricing, "false");
        if (!zonePricing.getCoValue().equals("false")) {
            log.warn("   Setting zonePricing to default value: false");
            zonePricing.setCoValue("false");
        }

        try{
            configRepo.save(defaultPort);
            configRepo.save(httpMethod);
            configRepo.save(heartbeatOperation);

            configRepo.save(interval);
            configRepo.save(time);
            configRepo.save(day);
            configRepo.save(items);

            configRepo.save(sendStoreInfo);
            configRepo.save(customStoreName);

            configRepo.save(awayHours);
            configRepo.save(offlineHours);

            configRepo.save(hybridMultipath);
        }
        catch (Exception ex){
            log.error("Error saving validated Config entries: " + ex.getMessage());
            result.setErrorDescription("Error saving validated Config entries: " + ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private static class DeleteConfig {
        String interval;
        Integer time;
        Integer day;
        List<String> deleteTypes;

        public DeleteConfig() {
        }

        public String getInterval() {
            return interval;
        }

        public void setInterval(String interval) {
            this.interval = interval;
        }

        public Integer getTime() {
            return time;
        }

        public void setTime(Integer time) {
            this.time = time;
        }

        public Integer getDay() {
            return day;
        }

        public void setDay(Integer day) {
            this.day = day;
        }

        public List<String> getDeleteTypes() {
            return deleteTypes;
        }

        public void setDeleteTypes(List<String> deleteTypes) {
            this.deleteTypes = deleteTypes;
        }

        public DeleteConfig(ConfigEntry interval, ConfigEntry time, ConfigEntry day, ConfigEntry deleteTypes) {
            Integer dayInt = null;
            Integer timeInt = null;
            try {
                dayInt = Integer.parseInt(day.getCoValue());
                timeInt = Integer.parseInt(time.getCoValue());
            } catch (Exception e) {
                log.error("ERROR - Invalid delete interval or time configuration");

            }
            this.interval = interval.getCoValue();
            this.time = timeInt;
            this.day = dayInt;
            this.deleteTypes = new ArrayList<>();

            if (deleteTypes == null || deleteTypes.getCoValue() == null) {
                log.error("ERROR - no value found for delete types!");
            } else {
                for (String item : deleteTypes.getCoValue().split(",")) {
                    String stripped = item.toLowerCase().trim();
                    switch (stripped) {
                        case "flashkey":
                        case "label":
                        case "plu":
                        case "operator":
                        case "config":
                            this.deleteTypes.add(stripped);
                            break;
                        default:
                            log.error("ERROR - unknown deletion type: {}", stripped);
                    }
                }
            }
        }
    }

    @Operation(summary = "Retrieve the delete event configuration for the the application", description = "The server uses this configuration to determine when to send down' delete/reset events?")
    @GetMapping(path = "/deleteEvent", produces = { MediaType.APPLICATION_JSON_VALUE })
    public DeleteConfig getDeleteConfig() {
        List<ConfigEntry> deleteList = new ArrayList<>();
        ConfigEntry interval = configRepo.findById(AppConfig.scaleDeleteInterval).orElse(new ConfigEntry(null, null));
        ConfigEntry time = configRepo.findById(AppConfig.scaleDeleteTime).orElse(new ConfigEntry(null, null));
        ConfigEntry day = configRepo.findById(AppConfig.scaleDeleteDay).orElse(new ConfigEntry(null, null));
        ConfigEntry items = configRepo.findById(AppConfig.scaleDeleteItems).orElse(new ConfigEntry(null, null));

        return new DeleteConfig(interval, time, day, items);
    }

    @Operation(summary = "Change when the server sends down the delete config", description = "Creates/Updates the given region in the database.")
    @PostMapping(path = "/deleteEvent", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> updateDeleteConfig(@RequestBody DeleteConfig deleteConfig) {
        OperationStatusModel result = new OperationStatusModel("UpdateDeleteConfig");

        ConfigEntry interval, time, day, del;
        try {
            interval = new ConfigEntry(AppConfig.scaleDeleteInterval, deleteConfig.interval);
            time = new ConfigEntry(AppConfig.scaleDeleteTime, String.valueOf(deleteConfig.time));
            day = new ConfigEntry(AppConfig.scaleDeleteDay, String.valueOf(deleteConfig.day));

            if (deleteConfig.day > 7 || deleteConfig.day < 1) {
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Invalid day config, must be value between sunday(1) - saturday(7)");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            if (deleteConfig.time > 23 || deleteConfig.time < 0) {
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Invalid time config, must be value between 0 - 23");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            String concatenatedString = "";
            for (String word : deleteConfig.deleteTypes) {
                concatenatedString += word + ",";
            }
            String deleteTypes;
            if (concatenatedString.length() > 0) {
                deleteTypes = concatenatedString.substring(0, concatenatedString.length() - 1);
            } else {
                deleteTypes = "";
            }
            del = new ConfigEntry(AppConfig.scaleDeleteItems, deleteTypes);
        } catch (Exception e) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error parsing delete config! " + e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            configRepo.save(interval);
            configRepo.save(time);
            configRepo.save(day);
            configRepo.save(del);
        } catch (Exception e) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error saving delete config! " + e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Delete Config successfully updated!");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Get the current configuration for hours used to be considered away and offline", description = "")
    @GetMapping(path = "/offlineAwayHours", produces = { MediaType.APPLICATION_JSON_VALUE })
    public Map<String, Integer> getHoursConfig() {
        ConfigEntry awayHours = configRepo.findById(AppConfig.scaleAwayHours)
                .orElse(new ConfigEntry(AppConfig.scaleAwayHours, "12"));
        ConfigEntry offlineHours = configRepo.findById(AppConfig.scaleOfflineHours)
                .orElse(new ConfigEntry(AppConfig.scaleOfflineHours, "24"));

        Map<String, Integer> hourConfig = new HashMap<>();
        try {
            hourConfig.put(awayHours.getCoName(), Integer.valueOf(awayHours.getCoValue()));
            hourConfig.put(offlineHours.getCoName(), Integer.valueOf(offlineHours.getCoValue()));
        } catch (Exception e) {
            hourConfig.put(awayHours.getCoName(), 12);
            hourConfig.put(offlineHours.getCoName(), 24);
        }

        return hourConfig;
    }

    @Operation(summary = "Change when a scale is considered offline or away", description = "")
    @PostMapping(path = "/offlineAwayHours", produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> updateHoursConfig(@RequestParam Integer awayHours,
            @RequestParam Integer offlineHours) {
        OperationStatusModel result = new OperationStatusModel("UpdateHoursConfig");

        if (awayHours == null || offlineHours == null) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Invalid config, hours must not be null");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (awayHours < 0 || offlineHours < 0 || awayHours > 96 || offlineHours > 96) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Invalid config, hours must be between range 0 and 96");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (awayHours >= offlineHours) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Invalid config, away hours must be lower than offline hours");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            ConfigEntry awayConfig = new ConfigEntry(AppConfig.scaleAwayHours, awayHours.toString());
            ConfigEntry offlineConfig = new ConfigEntry(AppConfig.scaleOfflineHours, offlineHours.toString());

            configRepo.save(awayConfig);
            configRepo.save(offlineConfig);

        } catch (Exception e) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error saving hour config! " + e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Hour Config successfully updated!");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Reset default rules for all HTe Events ", description = "")
    @PostMapping(path = "/resetEventRuleDefaults", produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> resetEventRuleDefaults(@RequestParam Boolean clearExisting,
            @RequestParam Boolean overwrite) {
        OperationStatusModel result = new OperationStatusModel("ResetEventDefaults");

        Field[] tmp = EventConfig.class.getDeclaredFields();

        // Used to clear any custom rules if thats what the user wants
        if (clearExisting) {
            try {
                eventRuleRepo.deleteAll();
            } catch (Exception e) {
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Could not clear current rules!");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
        }

        for (Field f : tmp) {
            try {
                EventRule defaultEvent = (EventRule) f.get(null);
                EventRule existingEvent = eventRuleRepo.findById(defaultEvent.getEventId()).orElse(null);
                if (existingEvent == null || overwrite) {
                    // Write the default event if the event rule doesn't exist or the user has
                    // requested to overwrite the default rules.
                    eventRuleRepo.save(defaultEvent);
                }
            } catch (Exception ex) {
                log.debug("Could not Cast Field : " + f.toString());
            }
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Successfully reset event rule defaults!");
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Retrieve the AppConfig Table from ", description = "")
    @GetMapping(path = "/services", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<Service>> getCurrentServices() {
        List<Service> services = new ArrayList<>();
        try {
            services = serviceRepo.findByDatabaseId(AppConfig.DefaultHqDatabaseId);
        } catch (Exception e) {
            log.error("Error retrieving available services");
            return new ResponseEntity<>(services, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(services, HttpStatus.OK);
    }

    @Operation(summary = "Retrieve the rest of the Config table", description = "Included Tags : "
            + AppConfig.sendStoreInfo)
    @GetMapping(path = "/misc", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<HashMap<String, String>> getConfigurations(@RequestParam List<String> appConfigKeys) {
        OperationStatusModel result = new OperationStatusModel("getConfigurations");

        HashMap<String, String> configs = new HashMap<>();

        for (String configKey : appConfigKeys) {
            switch (configKey) {
                case AppConfig.sendStoreInfo:
                case AppConfig.customStoreName:
                case AppConfig.hybridMultipath:
                case AppConfig.zonePricing:
                    configs.put(configKey, getConfigEntry(configKey, null).getCoValue());
                    break;
                default:
                    configs.put(configKey, null);
            }
        }

        return new ResponseEntity<>(configs, HttpStatus.OK);
    }

    @Operation(summary = "Retrieve the rest of the Config table", description = "Included Tags : "
            + AppConfig.sendStoreInfo)
    @PostMapping(path = "/misc", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> setConfiguration(@RequestBody HashMap<String, String> updatedConfigs) {
        OperationStatusModel result = new OperationStatusModel("setConfiguration");

        StringBuilder keysSet = new StringBuilder();
        String preset = "";
        for (String appConfigKey : updatedConfigs.keySet()) {
            switch (appConfigKey) {
                case AppConfig.sendStoreInfo:
                    if (updatedConfigs.get(appConfigKey).equals("false")
                            || updatedConfigs.get(appConfigKey).equals("true")) {
                        try {
                            configRepo.save(new ConfigEntry(appConfigKey, updatedConfigs.get(appConfigKey)));
                            keysSet.append(preset);
                            keysSet.append(appConfigKey);
                        } catch (Exception ex) {
                            log.error("Error saving AppConfig Entry " + appConfigKey + "  " + ex.getMessage());
                            result.setResult(RequestOperationResult.ERROR.name());
                            result.setErrorDescription(
                                    "Error saving AppConfig Entry " + appConfigKey + "  " + ex.getMessage());
                            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
                        }
                    } else {
                        result.setResult(RequestOperationResult.ERROR.name());
                        result.setErrorDescription(
                                "Invalid Value for: " + appConfigKey + "  must be either 'true' or 'false' ");
                        return new ResponseEntity<>(result, HttpStatus.CONFLICT);
                    }
                    break;
                case AppConfig.customStoreName:
                    try {
                        configRepo.save(new ConfigEntry(appConfigKey, updatedConfigs.get(appConfigKey)));
                        keysSet.append(preset);
                        keysSet.append(appConfigKey);
                    } catch (Exception ex) {
                        log.error("Error saving AppConfig Entry " + appConfigKey + "  " + ex.getMessage());
                        result.setResult(RequestOperationResult.ERROR.name());
                        result.setErrorDescription(
                                "Error saving AppConfig Entry " + appConfigKey + "  " + ex.getMessage());
                        return new ResponseEntity<>(result, HttpStatus.CONFLICT);
                    }
                    break;
                case AppConfig.hybridMultipath:
                case AppConfig.zonePricing:
                    try {
                        configRepo.save(new ConfigEntry(appConfigKey, updatedConfigs.get(appConfigKey)));
                        keysSet.append(preset);
                        keysSet.append(appConfigKey);
                    } catch (Exception ex) {
                        log.error("Error saving AppConfig Entry {} {}", appConfigKey, ex.getMessage());
                        result.setErrorDescription("Error saving AppConfig Entry " + appConfigKey + " " + ex.getMessage());
                        result.setResult(RequestOperationResult.ERROR.name());
                        return new ResponseEntity<>(result, HttpStatus.CONFLICT);
                    }
                    break;
                default:
                    result.setResult(RequestOperationResult.ERROR.name());
                    result.setErrorDescription(
                            "Invalid Key " + appConfigKey + " key is not recognized as valid configuration! ");
                    return new ResponseEntity<>(result, HttpStatus.CONFLICT);
            }
            preset = ",";
        }
        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Config Values successfully set: " + keysSet);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}