package com.hobart.hte.filemgr;

import java.io.*;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.hobart.hte.repositories.*;
import com.hobart.hte.upgrading.UpgradeHelper;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.file.FileType;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.model.RequestOperationType;
import com.hobart.hte.utils.util.HteTools;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.yaml.snakeyaml.reader.ReaderException;

@RestController
@RequestMapping("/filemgr")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class FileController {

	private static final String LOG_PATH = "/var/log/hte";
	private static final Logger log = LogManager.getLogger(FileController.class);

	@Autowired
	FileRepository fileRepo;
	@Autowired
	ProfileRepository profileRepo;
	@Autowired
	DeviceRepository deviceRepo;
	@Autowired
	ScaleSettingsRepository ssRepo;
	@Autowired
	ConfigAppRepository configRepo;
	@Autowired
	UpgradeHelper upgradeHelper;

	private String REPOSITORY_PATH = null;

	@Operation(summary = "Retrieve the information of the files associated to a specific profile Id or all files in the system.", description = "Returns a list of the files asociated with a specific profile Id, if no profile Id is provided, this endpoint will return the list of all files in the system, and their corresponding attributes.")
	@Parameters({
			@Parameter(name = "profileId", description = "The profile Id to use to filter, if no value given it will return the list of all files.", in = ParameterIn.QUERY, example = "becee5be-cd8d-4621-94e1-f49740530a7a", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/getFileInfo", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Iterable<FileForEvent> getFileInfo(@RequestParam(required = false) String profileId) {
		Iterable<FileForEvent> allFiles;
		if (profileId == null) {
			allFiles = fileRepo.findAll();
		} else {
			allFiles = fileRepo.findAllByProfileId(profileId);
		}

		for (FileForEvent file : allFiles) {
			try {
				String content = parseContent(file);
				file.setContent(content);
			} catch (Exception e){
				file.setContent("Error parsing file: " + e.getMessage());
			}
		}

		return allFiles;
	}
	private String parseContent(FileForEvent file) throws IOException {
		FileType type = HteTools.inferFileType(file.getFilename());
		switch (type) {
			case FEATURE:

				return "feature in progress";
			case CONFIG:

				return "config in progress";
			case DEBIAN:

				return "debian in progress";
			case TARBALL:

				return "tarball in progress";
			case INSTALL_IMAGE:

				return ".img.bz2 in progress";
			case LICENSE:
				REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
						.orElse(new ConfigEntry("", "")).getCoValue();
				if(!HteTools.isLicenseFile(file.getFileType())) {
					return "Not able to parse License";
				}

				Path licensePath = Paths.get(REPOSITORY_PATH, file.getProfileId(), file.getFilename());

				if(!Files.isReadable(licensePath)){
					return "License is not readable";
				}
				List<String> buffer = null;
				try {
					buffer = Files.readAllLines(licensePath);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				if(buffer.isEmpty()){
					return "License was empty";
				}
				// TODO add license validation

				String content = String.join("\n", buffer);
				return content;
			case UNKNOWN:
				return "File type " + type.name() + " cannot be parsed for content.";
			default:
				return "File type " + type.name() + " does not have a way to parse content to display.";
		}
	}

	@GetMapping(value = "/getImage")
	@Parameters({
			@Parameter(name = "fileId", description = "The id of the media file you are trying to render")
	})
	public ResponseEntity<?> getImage(@RequestParam String fileId){
		OperationStatusModel result = new OperationStatusModel("Get Image");
		FileForEvent file = fileRepo.findById(fileId).orElse(null);
		if(file == null) {
			result.setErrorDescription("File requested does not exist");
			result.setResult(RequestOperationResult.ERROR.name());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(result);
		}

		REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
				.orElse(new ConfigEntry("", "")).getCoValue();
		if(!HteTools.isImageFile(file.getFileType())) {
			result.setErrorDescription("File requested is not an image");
			result.setResult(RequestOperationResult.ERROR.name());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result);
		}

		Path filePath = Paths.get(REPOSITORY_PATH, file.getProfileId(), file.getFilename());

		try {
			log.info("Reading file: " + filePath.toString());

			byte[] targetArray = Files.readAllBytes(filePath);

			return ResponseEntity.ok().body(targetArray);
		}
		catch (IOException e) {
			result.setErrorDescription("Error reading file from repository: " + e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
		}
	}

	// upload file

	class licFilter implements FilenameFilter {
		@Override

		public boolean accept(final File dir, final String filename) {
			return filename.endsWith(".lic");

		}
	}
	@Operation(summary = "Upload a file in the repository.", description = "Uploads a file to the server and it is associated to the given profile parameter, as a response the file Id is returned. The file will be available for scales to download it.")
	@Parameters({
			@Parameter(name = "profile", description = "The profile Id of the file to upload, this profile must exists in the database.", in = ParameterIn.QUERY, example = "becee5be-cd8d-4621-94e1-f497e0b30a78", schema = @Schema(implementation = String.class)),
			@Parameter(name = "shortDesc", description = "A short description of the file to upload.", in = ParameterIn.QUERY, example = "Label data for deli or default configurations for scales", schema = @Schema(implementation = String.class)),
			@Parameter(name = "fileType", description = "The type of file to upload", in = ParameterIn.QUERY, example = "CONFIG", schema = @Schema(implementation = FileType.class)),
			@Parameter(name = "enabled", description = "If the file is enabled or not, if it is enabled, then the scales will download it and apply it.", in = ParameterIn.QUERY, example = "true", schema = @Schema(implementation = Boolean.class)),
			@Parameter(name = "startDate", description = "The timestamp where the system will start applying this file in the scales. If it doesn't apply, then send null.", schema = @Schema(implementation = String.class)),
			@Parameter(name = "endDate", description = "The timestamp where the system will stop applying this file in the scales. If it doesn't apply, then send null.", schema = @Schema(implementation = String.class)) })
	@PostMapping("/upload")
	public ResponseEntity<?> handleUploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request,
											  @RequestParam(name = "profile") String profile, @RequestParam(name = "shortDesc") String shortDesc,
											  @RequestParam(name = "fileType") FileType fileType, @RequestParam(name = "enabled") boolean enabled,
											  @RequestParam(name = "startDate") String startDate, @RequestParam(name = "endDate") String endDate,
											  @RequestParam(required = false, name = "startRebootWindow") Integer startRebootWindow,
											  @RequestParam(required = false, name = "endRebootWindow") Integer endRebootWindow){
		OperationStatusModel result = new OperationStatusModel("Upload File");
		REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
				.orElse(new ConfigEntry("", "")).getCoValue();

		log.debug("handling file upload...");
		log.debug("save file into this profile: {}", profile);
		String checksum = "N/A";

		// Check if profile exists, otherwise return error
		if (!profileRepo.existsById(profile)) {
			log.debug("profile not found in the system, the file can't be added.");
			result.setErrorDescription("The profile Id given (" + profile
					+ ") is not valid or not found the database, the file can't be uploaded.");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		// validate target path to save file in
		File profileDir = Paths.get(REPOSITORY_PATH, profile).toFile();
		if (!profileDir.exists()) {
			if (!profileDir.mkdirs()) {
				log.error("unable to create directory: {}", profileDir.getAbsolutePath());
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("unable to create directory: " + profileDir.getAbsolutePath());
				return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				log.debug("directory created!");
			}
		}

		String filename = file.getOriginalFilename();
		File upgradeDir = Paths.get(REPOSITORY_PATH, "upgrades").toFile();
		if (!upgradeDir.exists() && filename != null && (filename.endsWith(".package.tgz") || upgradeHelper.isFileGTType(filename))) {
			if (!upgradeDir.mkdirs()) {
				log.error("unable to create directory: {}", upgradeDir.getAbsolutePath());
				result.setErrorDescription("unable to create directory: " + upgradeDir.getAbsolutePath());
				result.setResult(RequestOperationResult.ERROR.name());
				return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				log.debug("directory created!");
			}
		}

		try {
			String subdir = "";
			if(filename != null &&  (filename.endsWith(".package.tgz")  || (filename.endsWith("deb") )) ) {
				subdir = "upgrades";
			} else{
				subdir = profile;
			}
			File ftmp = Paths.get(REPOSITORY_PATH, subdir, filename).toFile();

			// uploading license -> THERE CAN ONLY BE ONE per dir
			if(filename.endsWith(".lic")){
				File fList = Paths.get(REPOSITORY_PATH, subdir).toFile();
				File[] profileList = fList.listFiles(new licFilter());
				if(profileList.length > 0){
					result.setResult(RequestOperationResult.ERROR.name());
					result.setErrorDescription("License Already Exists for this profile. Please delete license before " +
							"uploading a new one");
					return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
			try {
				file.transferTo(ftmp);
			} catch (Exception ex) {
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("An error occurred when trying to transfer the file: "
						+ profileDir.getAbsolutePath() + "\nerror: " + ex.toString());
				return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			log.info("file uploaded: ", filename);

			// Use MD5 algorithm
			// ToDo: do we need to to do this asynchronously?
			MessageDigest md5Digest = MessageDigest.getInstance("MD5");

			// Get the checksum
			long t1 = System.currentTimeMillis();
			checksum = HteTools.getFileChecksum(md5Digest, ftmp);
			long t2 = System.currentTimeMillis();
			log.info("the uploaded file has the following checksum: {}, calculated in {} ms", checksum, (t2 - t1));

			// create/update entry on database
			FileForEvent newFile = new FileForEvent();
			newFile.setChecksum(checksum);
			Optional<FileForEvent> existingFile = fileRepo.findByFilenameAndProfileId(filename, profile);
			if (!existingFile.isPresent()) {
				log.debug("new file, creating a new uuid");
				newFile.setFileId(UUID.randomUUID().toString());
			} else {
				log.debug("file already exists in the db, it will be replaced");
				newFile.setFileId(existingFile.get().getFileId());
			}
			newFile.setFilename(filename);
			newFile.setShortDesc(shortDesc);
			newFile.setFileVersion("1.0");
			newFile.setProfileId(profile);
			newFile.setSize(ftmp.length());
			newFile.setUploadDate(Timestamp.from(Instant.now()));
			newFile.setEnabled(enabled);
			newFile.setStartRebootTime(startRebootWindow);
			newFile.setEndRebootTime(endRebootWindow);

			if (fileType == null) {
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("The file type was not specified, please check the documentation for the valid file types");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

			}

			if(!HteTools.verifyFileExtention(filename, fileType)) {
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("The file extension is not valid for the given filetype. Please check the documentation for the valid extensions");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			newFile.setFileType(fileType);


			// validate dates
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			try {
				Date parse;
				if (startDate != null && !startDate.isEmpty()) {
					parse = dateFormat.parse(startDate);
					newFile.setStartDate(new Timestamp(parse.getTime()));
				}

				if (endDate != null && !endDate.isEmpty()) {
					parse = dateFormat.parse(endDate);
					newFile.setEndDate(new Timestamp(parse.getTime()));
				}
			} catch (Exception ex) {
				log.info("an error occurred while parsing the date fields");
				result.setResult(RequestOperationResult.ERROR.name());
				result.setErrorDescription("An error occurred when trying to parse the date fields, expected pattern: [yyyy-MM-dd'T'HH:mm:ssXXX], "
						+ ex.toString());
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
			}

			FileForEvent saved = fileRepo.save(newFile);

			log.info("file '{}' uploaded successfully!", ftmp.getAbsolutePath()); // profileDir.getAbsolutePath());

			//A Change has been made to a profile, need to update all the scale settings to have "profileChanged" as part of their current files
			//their current files. This indicates that the profile has changed and the scale has not yet performed a heartbeat
			List<ScaleDevice> devices = deviceRepo.findByProfileId(profile);
			for(ScaleDevice device : devices){
				ScaleSettings scaleSettings = ssRepo.findById(device.getDeviceId()).orElse(null);
				if(scaleSettings != null){
					try{
						scaleSettings.addCurrentFilesItem("profileChanged");
						ssRepo.save(scaleSettings);
					}
					catch (Exception e){
						log.error("Could not update scale settings for device : " + device.getIpAddress());
						log.error("Update error is : " + e.getMessage());
					}

				}
			}


			deviceRepo.notifyScalesOfUpdate(profile);
			return ResponseEntity.ok(saved);

		} catch (Exception e) {
			log.error(e.getMessage());
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("an error occurred while uploading the file: " + e.getMessage());
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/filenameInProfiles", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> handleRetrieveFilenameInProfiles(@RequestParam(name = "filename") String filename) {
		// ToDo: search ignoring case
		List<FileForEvent> list = fileRepo.findByFilename(filename);
		log.info("retrieving all entries in the DB for file {}, found {} records", filename, list.size());
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	// remove file
	@Operation(summary = "Delete a file from the repository and database.", description = "Deletes the file identified with the given file Id. The file is deleted from the repository along with the database entry.")
	@Parameters({
			@Parameter(name = "id", description = "The Id of the file to delete, it must exists in the database.", in = ParameterIn.QUERY, example = "becee5be-cd8d-4621-94e1-f49740530a7a", schema = @Schema(implementation = String.class)),
			@Parameter(name = "deleteInAllProfiles", description = "[Optional] If this is a feature file:<br>True to delete all references and the file in the repository,<br>False to delete only the entry in the corresponding profile, but not the file.", in = ParameterIn.QUERY, example = "true", schema = @Schema(implementation = Boolean.class)) })
	@DeleteMapping("/delete")
	public ResponseEntity<?> handleDeleteFile(@RequestParam(name = "id") String id,
			@RequestParam(name = "deleteInAllProfiles", required = false, defaultValue = "false") boolean deleteInAllProfiles) {
		// check if exists in the DB, get the profile and check if it exists in the disk
		if (!fileRepo.existsById(id)) {
			log.info("The file was not found!");
			return ResponseEntity.notFound().build();
		}

		REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
				.orElse(new ConfigEntry("", "")).getCoValue();

		boolean deleteFile = false;
		boolean deleteFileInDB = false;
		String profileId = null;

		FileForEvent toDelete = fileRepo.findById(id).get();
		if(toDelete != null){
			profileId = toDelete.getProfileId();
		}
		String subdir;
		if (toDelete.getFilename().endsWith(".package.tgz") || upgradeHelper.isFileGTType(toDelete.getFilename())) {
			subdir = "upgrades";
			if (deleteInAllProfiles) {
				List<FileForEvent> listToDelete = fileRepo.findByFilename(toDelete.getFilename());
				deleteFile = true;
				for (FileForEvent ffe : listToDelete) {
					fileRepo.deleteById(ffe.getFileId());
					log.info("deleting in db file id: {}", ffe.getFileId());
				}
			} else {
				deleteFileInDB = true;
			}
		} else { // .ht file
			subdir = toDelete.getProfileId();
			deleteFileInDB = true;
			deleteFile = true;
		}

		if (deleteFile) {
			File file = Paths.get(REPOSITORY_PATH, subdir, toDelete.getFilename()).toFile();
			log.info("file to delete: {}", file.toString());
			if (file.delete()) {
				log.info("file {} has been deleted.", toDelete.getFilename());
			}
		}

		if (deleteFileInDB) {
			fileRepo.deleteById(id);
			log.info("file {} has been deleted from DB.", toDelete.getFilename());
		}

		//A Change has been made to a profile, need to update all the scale settings to have "profileChanged" as part of their current files
		//their current files. This indicates that the profile has changed and the scale has not yet performed a heartbeat
		List<ScaleDevice> devices = deviceRepo.findByProfileId(profileId);
		for(ScaleDevice device : devices){
			ScaleSettings scaleSettings = ssRepo.findById(device.getDeviceId()).orElse(null);
			if(scaleSettings != null){
				try{
					scaleSettings.addCurrentFilesItem("profileChanged");
					ssRepo.save(scaleSettings);
				}
				catch (Exception e){
					log.error("Could not update scale settings for device : " + device.getIpAddress());
					log.error("Update error is : " + e.getMessage());
				}

			}
		}

		return ResponseEntity.ok().build();

	}

	// download file

	@Operation(summary = "Returns a file from the repository.", description = "Returns the file identified with the given file Id. This entry point is for a scale to download a .ht file containing configuration information.")
	@Parameters({
			@Parameter(name = "id", description = "The Id of the file to download, it must exists in the database. Otherwise an error will return.", in = ParameterIn.QUERY, example = "dacfe5bf-ce8d-4621-94e1-f49740530873", schema = @Schema(implementation = String.class)) })
	@GetMapping("/download")
	public ResponseEntity<?> handleFileDownload(@RequestParam(name = "id") String id) {

		REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
				.orElse(new ConfigEntry("", "")).getCoValue();

		if (fileRepo.existsById(id)) {
			FileForEvent toDownload = fileRepo.findById(id).get();
			Path path = null;
			if(toDownload.getFilename().endsWith(".package.tgz") || upgradeHelper.isFileGTType(toDownload.getFilename())){
				path = Paths.get(REPOSITORY_PATH, "upgrades", toDownload.getFilename());
			}
			else{
				path = Paths.get(REPOSITORY_PATH, toDownload.getProfileId(), toDownload.getFilename());
			}

			log.info("file requested: {}", path.getFileName().toString());
			if (Files.exists(path)) {
				Resource resource = null;
				try {
					resource = new UrlResource(path.toUri());
				} catch (MalformedURLException e) {
					e.printStackTrace();
					log.error(e.getMessage());
					return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
				}

				log.debug("The file was found, returning!");
				// ToDo: log in DB what file and when it was downloaded and who did it.
				return ResponseEntity.ok()
						// .contentType(MediaType.parseMediaType(contentType))
						.contentType(MediaType.TEXT_PLAIN).header(HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename=\"" + resource.getFilename() + "\"")
						.body(resource);
			}
		}

		log.info("The file was not found!");
		return ResponseEntity.notFound().build();
	}

	// change file attributes
	@Operation(summary = "Update the file record in the database.", description = "Updates the data related to a file in the server. No file upload supported, this endpoint only supports to change the following fields in the database records: "
			+ "<br/><br/>- description<br/>- file category<br/>- enable<br/>- start date<br/>- end date")
	@Parameters({
			@Parameter(name = "newFileData", description = "Object representing the Hobart file", in = ParameterIn.QUERY, schema = @Schema(implementation = FileForEvent.class)) })
	@PostMapping(path = "/update", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> handleUpdatedFile(@RequestBody FileForEvent newFileData) {

		log.debug("handling file update...");
		log.debug("save file into this profile: {}", newFileData.getProfileId());

		Optional<FileForEvent> filefound = fileRepo.findById(newFileData.getFileId());
		FileForEvent fileToSave = null;
		if (filefound.isPresent()) {
			fileToSave = filefound.get();

			fileToSave.setShortDesc(newFileData.getShortDesc());
			fileToSave.setFileType(newFileData.getFileType());
			fileToSave.setEnabled(newFileData.isEnabled());
			fileToSave.setStartDate(newFileData.getStartDate());
			fileToSave.setEndDate(newFileData.getEndDate());
			fileRepo.save(fileToSave);

		} else {
			log.info("the given file was not found in the system, the file entry can't be updated.");
			OperationStatusModel result = new OperationStatusModel(RequestOperationType.CREATE.name());
			result.setErrorDescription("The given file Id (" + newFileData.getFileId()
					+ ") is not valid or not found the database, the file entry can't be updated.");
			result.setResult(RequestOperationResult.ERROR.name());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

		//A Change has been made to a profile, need to update all the scale settings to have "profileChanged" as part of their current files
		//their current files. This indicates that the profile has changed and the scale has not yet performed a heartbeat
		List<ScaleDevice> devices = deviceRepo.findByProfileId(fileToSave.getProfileId());
		for(ScaleDevice device : devices){
			ScaleSettings scaleSettings = ssRepo.findById(device.getDeviceId()).orElse(null);
			if(scaleSettings != null){
				try{
					scaleSettings.addCurrentFilesItem("profileChanged");
					ssRepo.save(scaleSettings);
				}
				catch (Exception e){
					log.error("Could not update scale settings for device : " + device.getIpAddress());
					log.error("Update error is : " + e.getMessage());
				}

			}
		}

		return ResponseEntity.ok().build();
	}

	//Front end log related
	//==================================================================================================================
	static class LogEntry{
		public String dateAndType;
		public String message;
		public String object;

		@Override
		public String toString() {
			return "LogEntry{" +
					"dateAndType='" + dateAndType + '\'' +
					", message='" + message + '\'' +
					", object='" + object + '\'' +
					'}';
		}
	}

	@Operation(summary = "Upload recent logs from the frontend ", description = "Consumes a parsed enterprise.xml file from HTe Business")
	@PostMapping(path = "/log", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<OperationStatusModel> uploadFrontendLogs (@RequestBody List<LogEntry> frontendLogs) {
		OperationStatusModel result = new OperationStatusModel("UploadFrontendLogs");
		String fileName = LOG_PATH + "/hte_frontend";
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName + ".log", true));
			for(LogEntry entry : frontendLogs) {
				writer.append(entry.dateAndType).append(" ").append(entry.message).append(" ");

				if(entry.object != null){
					writer.append(entry.object);
				}
				writer.append("\n");
			}

			writer.close();
		}
		catch (Exception e){
			result.setResult(RequestOperationResult.ERROR.name());
			result.setErrorDescription("Could not write to log file : " + e.getMessage());
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}

//		Enumeration e = Logger.getLogger()

		//Logs have been written, now check the current log file
		File f = new File(fileName + ".log");
		if(f.length() > 2e+7){

			//Delete any existing archived files
			//==========================================================================
			File dir = new File(LOG_PATH);
			// Creates an array in which we will store the names of files and directories
			String[] pathnames;
			// Populates the array with names of files and directories
			pathnames = dir.list();
			// Delete the older archived file
			for (String pathname : pathnames) {
				if(pathname.startsWith("hte_frontend-")){
					log.info("Deleting old frontend log: {}", pathname);
					File myObj = new File(LOG_PATH + "/" + pathname);
					if (myObj.delete()) {
						log.info("Successful Delete: " + myObj.getName());
					} else {
						log.info("Could not delete file " + myObj.getName());
					}
				}
			}

			//Archive the existing log
			//==========================================================================
			// Create an object of the File class
			// Replace the file path with path of the directory
			File file = new File(fileName + ".log");

			// Create an object of the File class
			// Replace the file path with path of the directory
			File rename = new File(fileName + "-" + LocalDate.now() + ".log");

			// store the return value of renameTo() method in
			// flag
			boolean flag = file.renameTo(rename);

			// if renameTo() return true then if block is
			// executed
			if (flag) {log.info("Successfully archived old frontend log");}
			else {log.info("Frontend log archive failed!");}
		}

		result.setResult(RequestOperationResult.SUCCESS.name());
		result.setErrorDescription("Logs successfully uploaded");
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
