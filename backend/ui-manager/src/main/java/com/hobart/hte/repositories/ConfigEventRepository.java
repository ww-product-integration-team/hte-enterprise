package com.hobart.hte.repositories;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.hobart.hte.utils.config.ConfigEvent;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;



public interface ConfigEventRepository extends CrudRepository<ConfigEvent, String> {
	/**
	 * Search for the events for the specific given event type, store and department number
	 * 
	 * @param eventType
	 * @param storeId
	 * @param deptId
	 * @return
	 */
	List<ConfigEvent> findAllByEventTypeAndStoreIdAndDeptId(int eventType, int storeId, int deptId);

	/**
	 * return all events for the given store and department number
	 *
	 * @param storeId
	 * @param deptId
	 * @return
	 */
	List<ConfigEvent> findByStoreIdAndDeptId(int storeId, int deptId);

	/**
	 * return all events for the given department and store numbers, and the interval of time
	 * @param deptId
	 * @param storeId
	 * @param now
	 * @return
	 */
	@Query("select c from com.hobart.hte.utils.config.ConfigEvent c where c.eventType = 2 and c.deptId = ?1 and c.storeId = ?2 and c.startOn < ?3 and c.finishOn > ?3")
	List<ConfigEvent> findByDeptIdAndStoreId(int deptId, int storeId, Date now);
	
	/**
	 * Search events associated to the Device id
	 * 
	 * @param deviceUuid
	 * @return
	 */
	List<ConfigEvent> findByDeviceUuid(String deviceUuid);
	
	/**
	 * Search events by department and based on date/time
	 * @param deptId
	 * @param now
	 * @return
	 */
	@Query("select c from com.hobart.hte.utils.config.ConfigEvent c where c.eventType = 2 and c.deptId = ?1 and c.startOn < ?2 and c.finishOn > ?2")
	List<ConfigEvent> findByDeptId(int deptId, Date now);
	
	/**
	 * Search for events of a specific event type in the given store and department lists
	 * @param eventType the type of event 
	 * @param sn An array containing a list of store numbers
	 * @param dn An array containing a list of department numbers
	 * @return a list of events matching the search criteria 
	 */
	List<ConfigEvent> findByEventTypeAndStoreIdInAndDeptIdIn(int eventType,Collection<Integer> sn, Collection<Integer> dn);
}
