package com.hobart.hte.repositories;

import java.sql.Timestamp;
import java.util.List;

import com.hobart.hte.utils.event.DeviceStatusEntry;
import org.springframework.data.repository.CrudRepository;



public interface DeviceStatusLogRepo extends CrudRepository<DeviceStatusEntry, Timestamp> {
	List<DeviceStatusEntry> findTop24ByDeviceUuid(String deviceUuid);

	// return the last 24 occurrences
	List<DeviceStatusEntry> findTop24ByDeviceUuidOrderByTimestampDesc(String deviceUuid);

	// return the entries between the given time stamps
	//List<DeviceSyncStatus> findByTimestampBetweenAndByDeviceUuid(Timestamp start, Timestamp end, String deviceUuid);
	List<DeviceStatusEntry> findByTimestampBetweenAndDeviceUuid(Timestamp start, Timestamp end, String deviceUuid);

	List<DeviceStatusEntry> findByTimestampBetween(Timestamp start, Timestamp end);

}
