package com.hobart.hte.repositories;

import com.hobart.hte.utils.autoAssign.AutoAssign;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

public interface AutoAssignRepository extends CrudRepository<AutoAssign, String>  {

    Boolean existsByIpAddressStart(String ipStart);
    Boolean existsByIpAddressEnd(String ipEnd);
    @Transactional
    List<AutoAssign> findIpAddressStartAndIpAddressEndByIpAddressStartIsNotNull();
    List<AutoAssign> findAll();
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE autoAssignmentRules SET enabled = 0", nativeQuery = true)
    int disableRules();

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE autoAssignmentRules SET enabled = 1", nativeQuery = true)
    int enableRules();

    @Transactional
    void deleteByIpAddressStartAndIpAddressEnd(String ipStart, String ipEnd);

    @Query("SELECT rules FROM AutoAssign AS rules " +
            "WHERE banner = :banner " +
            "AND region = :region " +
            "AND store = :store " +
            "AND dept = :dept " +
            "AND enabled = 1")
    AutoAssign findRuleByAssetData(@Param("banner") String banner,
                                   @Param("region") String region,
                                   @Param("store") String store,
                                   @Param("dept") String dept);

    @Modifying
    @Transactional
    @Query(value = "UPDATE autoAssignmentRules " +
            "SET enabled = 0 " +
            "WHERE ipAddressStart = :ipAddressStart " +
            "AND ipAddressEnd = :ipAddressEnd " +
            "AND banner = :banner " +
            "AND region = :region " +
            "AND store = :store " +
            "AND dept = :dept", nativeQuery = true)
    int disableRule(@Param("ipAddressStart") String ipAddressStart,
                        @Param("ipAddressEnd") String ipAddressEnd,
                        @Param("banner") String banner,
                        @Param("region") String region,
                        @Param("store") String store,
                        @Param("dept") String dept);
}