package com.hobart.hte.scale;

import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.ui.UIController;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.util.HteTools;
import com.jcraft.jsch.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;


public class OneOffFeatureUpgradeEndPoint extends ControllerEndpoint<OperationStatusModel> {

    private static final Logger log = LogManager.getLogger(UIController.class);

    public OperationStatusModel execute(String ipAddress, MultipartFile file) {

        OperationStatusModel response = new OperationStatusModel();
        response.setOperation("Apply Feature File");

        // See whether the scale is reachable; if not, abort
        Session sshSession = HteTools.getSSHSession(ipAddress);
        if (!sshSession.isConnected()) {
            log.error("{} could not be reached for one-off feature file submission.", ipAddress);
            response.setResult(RequestOperationResult.ERROR.name());
            response.setErrorDescription(ipAddress + " could not be reached.");
            return response;
        }

        boolean isHTScale = HteTools.getIsHTScale(ipAddress).equals("HT");

        String upgradePath = isHTScale ? "/usr/local/hobart/upgrade/" : "/opt/hobart/upgrade/";
        String upgradeFilePath = upgradePath + file.getOriginalFilename();
        String upgradeCommand = isHTScale ?
                "python /usr/local/hobart/python/installUpgrade.py " + upgradeFilePath :
                "python3 /usr/bin/installUpgrade.py " + upgradeFilePath;

        // Move the file to the scale, execute the command to initiate the upgrade, then cut the SSH session loose
        boolean fileMoved = HteTools.putFileSFTP(sshSession, file, upgradePath);
        boolean commandExecuted = HteTools.executeSSHCommand(sshSession, upgradeCommand);
        HteTools.disconnectSSH(sshSession);

        if (fileMoved && commandExecuted) {
            response.setResult(RequestOperationResult.SUCCESS.name());
        } else if (!fileMoved) {
            response.setResult(RequestOperationResult.ERROR.name());
            response.setErrorDescription("An error occurred while trying to push the file.");
        } else if (!commandExecuted) {
            response.setResult(RequestOperationResult.ERROR.name());
            response.setErrorDescription("An error occurred while trying to execute the upgrade command.");
        }

        return response;
    }
}
