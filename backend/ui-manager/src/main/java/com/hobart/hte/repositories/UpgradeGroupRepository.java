package com.hobart.hte.repositories;

import com.hobart.hte.utils.upgrade.UpgradeGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface UpgradeGroupRepository extends CrudRepository<UpgradeGroup, String> {

    @Query("SELECT status FROM UpgradeGroup WHERE groupId IN (:groupIds)")
    List<String> findStatusByGroupId(@Param("groupIds") List<String> groupIds);
}
