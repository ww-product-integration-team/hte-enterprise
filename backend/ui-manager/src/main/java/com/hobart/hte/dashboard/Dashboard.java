package com.hobart.hte.dashboard;


import com.hobart.hte.repositories.*;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.dashboard.DashboardData;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.CombinedScaleDataInt;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.repositories.StoreDeptPairsRepository;
import com.hobart.hte.utils.store.Store;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Dashboard extends ControllerEndpoint<DashboardData> {

    private static final Logger log = LogManager.getLogger(Dashboard.class);
    private final DomainRepository domainRepo;
    private final BannerRepository bannerRepo;
    private final RegionRepository regionRepo;
    private final StoreRepository storeRepo;
    private final DepartmentRepository deptRepo;
    private final DeviceRepository deviceRepo;
    private final ScaleSettingsRepository ssRepo;
    private final FileRepository fileRepo;
    private final UpgradeDeviceRepository upgradeDeviceRepo;
    private final NodeStatusRepository nodeStatusRepo;
    private final StoreDeptPairsRepository storeDeptPairsRepository;
    private final String nodeId;
    private final String nodeType;

    public Dashboard(DomainRepository domainRepo, BannerRepository bannerRepo,
                             RegionRepository regionRepo, StoreRepository storeRepo, DepartmentRepository deptRepo,
                             DeviceRepository deviceRepo, ScaleSettingsRepository ssRepo, FileRepository fileRepo,
                             UpgradeDeviceRepository upgradeDeviceRepo, NodeStatusRepository nodeStatusRepo, StoreDeptPairsRepository storeDeptPairsRepository,
                             String nodeId, String nodeType) {
        this.domainRepo = domainRepo;
        this.bannerRepo = bannerRepo;
        this.regionRepo = regionRepo;
        this.storeRepo = storeRepo;
        this.deptRepo = deptRepo;
        this.deviceRepo = deviceRepo;
        this.ssRepo = ssRepo;
        this.fileRepo = fileRepo;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
        this.nodeStatusRepo = nodeStatusRepo;
        this.storeDeptPairsRepository = storeDeptPairsRepository;

        this.nodeId = nodeId;
        this.nodeType = nodeType;
    }

    private DashboardData fetchMain() {
        DashboardData dash = new DashboardData();

        //return all data
        Iterable<Banner> banners = bannerRepo.findAll();
        dash.addBanners(banners);
        Iterable<Region> regions = regionRepo.findAll();;
        dash.addRegions(regions);
        Iterable<Store> stores = storeRepo.findAll();
        dash.addStores(stores);
        Iterable<Department> depts = deptRepo.findByDeptIdNotNull();
        dash.addDepts(depts);
        Iterable<CombinedScaleDataInt> assignedScales = deviceRepo.findAssignedCombinedScaleData();
        dash.addAssignedScales(assignedScales);
        List<CombinedScaleDataInt> unassignedScales = deviceRepo.findUnassignedCombinedScaleData();
        dash.addUnassignedScales(unassignedScales);

        return dash;
    }

    public DashboardData execute() {
        DashboardData dash = new DashboardData();
        if(nodeId == null || nodeType == null){
            //return all data
            dash = fetchMain();
        } else {
            switch (nodeType){
                case "BANNER":
                    Banner b = bannerRepo.findById(nodeId).orElse(null);
                    if(b == null){
                        throw new NullPointerException("Could not find bannerId:"+nodeId);
                    }
                    dash.addBanners(b);
                    dash.addRegions(regionRepo.findByBannerId(nodeId));
                    dash.addStores(storeRepo.findByBannerId(nodeId));
                    for(Store s : dash.getStores()){
                        dash.addDepts(storeDeptPairsRepository.findDeptIdByStoreId(s.getStoreId()));
                    }
                    dash.addAssignedScales(deviceRepo.findCombinedScalesByBannerId(nodeId));
                    break;
                case "REGION":
                    Region region = regionRepo.findById(nodeId).orElse(null);
                    if(region== null){
                        throw new NullPointerException("Could not find regionId:"+nodeId);
                    }
                    dash.addRegions(region);
                    dash.addStores(storeRepo.findByRegionId(nodeId));
                    for(Store s : dash.getStores()){
                        dash.addDepts(storeDeptPairsRepository.findDeptIdByStoreId(s.getStoreId()));
                    }
                    dash.addAssignedScales(deviceRepo.findCombinedScalesByRegionId(nodeId));
                    break;
                case "STORE":
                    Store s = storeRepo.findById(nodeId).orElse(null);
                    if(s == null){
                        throw new NullPointerException("Could not find storeId:"+nodeId);
                    }
                    dash.addStores(s);
                    dash.addDepts(storeDeptPairsRepository.findDeptIdByStoreId(s.getStoreId()));
                    dash.addAssignedScales(deviceRepo.findCombinedScalesByStoreId(nodeId));
                    break;
            }
            // Fetched tree struct, now need unassigned scales
            Iterable<CombinedScaleDataInt> unassignedScales = deviceRepo.findAssignedCombinedScaleData();
            dash.addUnassignedScales(unassignedScales);
        }
        return dash;
    };
}
