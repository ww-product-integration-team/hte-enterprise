package com.hobart.hte.upgrading.fileChecking;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.UpgradeDeviceRepository;
import org.apache.tomcat.util.http.parser.Upgrade;

public class FileCheckFactory {

    public UpgradeFileCheckStrategy determineFileCheckLogic(
            String ipAddress, String fileName, boolean deleteAfterUpgrade,
            DeviceRepository deviceRepo, UpgradeDeviceRepository upgradeDeviceRepo) {

        boolean isPackageTarballZip = fileName.endsWith(".package.tgz"); // Bread-and-butter HT upgrade format
        boolean isHtAppUpgrade = fileName.startsWith("hti-linux-application");
        boolean isHtCoreUpgrade = fileName.startsWith("hti-linux-core");
        boolean isHtMonolithicUpgrade = fileName.startsWith("hti-linux-feature-monolithic");

        if (isPackageTarballZip) {
            if (isHtAppUpgrade || isHtCoreUpgrade || isHtMonolithicUpgrade) {
                return new HTUpgradeFileCheck(ipAddress, fileName, deleteAfterUpgrade, deviceRepo, upgradeDeviceRepo);
            } else { // We will assume, by deduction, that it's just a feature file otherwise
                return new HTFeatureFileCheck(ipAddress, fileName, deviceRepo, upgradeDeviceRepo);
            }
        } else { // By the same token, we will assume this is otherwise a GT upgrade
            return new GTUpgradeFileCheck(ipAddress, fileName, deleteAfterUpgrade, deviceRepo, upgradeDeviceRepo);
        }
    }
}

/* If you're wondering why only the upgrade classes get the "deleteAfterUpgrade" flag, there's a good reason:
   When the upgrade is finished, the file will likely never be executed again, and it's usually quite hefty --
   your typical upgrade file is close to a gigabyte -- therefore it's a needless waste of storage.

   Feature files (for the HT especially) are more delicate; if you upgrade the scale, part of the reboot logic that's
   *supposed* to happen is that the installers (install.py) in those feature subdirectories are executed again,
   thereby reapplying the feature data. If we clear out the features directory, there's no way to recover them besides
   reapplying the feature file, which can be very exasperating to technicians and system administrators.

   On a design level, I don't include a "delete-all-files" handler for the abstract class; at least one major subclass
   *should not* use it for the reasons above, therefore it seems unwise to force each subclass to implement it.
 */
