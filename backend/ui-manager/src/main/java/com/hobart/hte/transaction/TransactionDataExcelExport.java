package com.hobart.hte.transaction;

import com.hobart.hte.utils.ExcelFunctions;
import com.hobart.hte.utils.transaction.TransEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class TransactionDataExcelExport implements ExcelFunctions<TransEntry> {

    private static final Logger log = LogManager.getLogger(TransactionDataExcelExport.class);

    private static final String[] HEADERS = {"Scale IP", "Scale ID", "Trans Id", "Trans Type Number",
        "Trans Group", "Timestamp", "Void", "APrdt", "Read Clear", "Number Pack", "Second Label",
        "COOL Field", "Total Value", "Operator Total Value", "Department Number", "Product Number",
        "Product Type", "Product Description", "Unit Price", "By Count", "Net Weight", "Label 1", "Label 2"
    };

    public ByteArrayInputStream buildExcelDocument(List<TransEntry> transEntryList){
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            SXSSFWorkbook workbook = beginDocument(100, "Transactions", HEADERS);
            SXSSFSheet sheet = workbook.getSheet("Transactions");

            int rowNum = 1;
            for (TransEntry t : transEntryList) {
                Row row = sheet.createRow(rowNum);

                createCell(row, 0, t.getScaleId());
                createCell(row, 1, t.getTrScID());
                createCell(row, 2, t.getTrID());
                createCell(row, 3, validateInt(t.getTTNum()));
                createCell(row, 4, validateInt(t.getTrGrp()));
                createCell(row, 5, t.getTrDtTm().toString());
                createCell(row, 6, t.getTrVoid());
                createCell(row, 7, t.getTrAPrdt());
                createCell(row, 8, t.getTrRdClr());
                createCell(row, 9, t.getTrNmPck());
                createCell(row, 10, t.isTrSecLb());
                createCell(row, 11, t.isTrCOOLF());
                createCell(row, 12, t.getTrTtVal());
                createCell(row, 13, t.getTrOTtVl());
                createCell(row, 14, t.getDeptNum());
                createCell(row, 15, t.getTrPrNum());
                createCell(row, 16, t.getTrPrTyp());
                createCell(row, 17, t.getTrPrDes());
                createCell(row, 18, t.getTrUnPr());
                createCell(row, 19, t.getTrByCount());
                createCell(row, 20, t.getTrFnlNtWt());
                createCell(row, 21, validateInt(t.getTrLab1()));
                createCell(row, 22, validateInt(t.getTrLab2()));

                rowNum++;
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            log.error("Something went wrong compiling Excel file: " + e.getMessage());
        }

        return null;
    }
}
