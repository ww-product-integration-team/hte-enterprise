package com.hobart.hte.scheduledtasks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

import com.hobart.hte.repositories.ConfigAppRepository;
import com.hobart.hte.repositories.FileRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.util.HteTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hobart.hte.repositories.ProfileRepository;

@Component
public class ImportTask {

	private static final Logger log = LogManager.getLogger(ImportTask.class);

	@Autowired
	FileRepository fileRepo;
	@Autowired
	ProfileRepository profileRepo;
	@Autowired
	ConfigAppRepository configRepo;

	// uuid regex pattern:
	public static final String filename_regex = "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}_[a-zA-Z0-9]+\\.(?:(?:ht)|(?:deb))";
	File import_path = null;
	File done_path = null;
	private String REPOSITORY_PATH = null;

	@Scheduled(cron = "0 */1 * * * *")
	public void importFiles() {
		import_path = getFile(AppConfig.importPath);
		done_path = getFile(AppConfig.donePath);
//		log.info("checking hte import path for files to import...");
		if(import_path == null || done_path == null || import_path.list() == null){
			log.error("Import and/or Done paths are null! Cannot perform import task!");
			return;
		}

		if (import_path.list().length != 0) {
			log.debug("files found: {}", import_path.list().length);
			for (String f : import_path.list()) {
				log.debug("> {}", f);
				f = f.toLowerCase();
				if (f.endsWith(".std")) {
					// ToDo: process STD file and translate to .ht file
					log.debug("starting to translate file to ht");
				} else if (Pattern.matches(filename_regex, f)) {
					log.info("matches pattern");
					processFile(f);
				} else {
					System.out.println("file doesn't match any pattern, discarding file: " + f);
					moveFile(Paths.get(import_path.toString(), f), Paths.get(done_path.toString(), f));
				}
			}
		} else {
			log.debug("no files found in path, waiting for next cycle");
		}
	}

	/**
	 * Move a file locally from one path to another
	 * 
	 * @param source
	 * @param destiny
	 * @return True if moving the file was successful, otherwise False
	 */
	private boolean moveFile(Path source, Path destiny) {
		log.info("moving file [{}] to path [{}]", source.toString(), destiny.toString());
		try {
			Files.move(source, destiny, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException ex) {
			log.error("an error ocurred while attempting to move files: {}", ex.toString());
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param f
	 */
	private void processFile(String f) {
		REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
				.orElse(new ConfigEntry("", "")).getCoValue();

		import_path = getFile(AppConfig.importPath);

		String farr[] = f.split("_");
		String profile = farr[0];
		String final_filename = farr[1];

		File ftmp = Paths.get(import_path.toString(), f).toFile();
		File dest_file = Paths.get(REPOSITORY_PATH, profile, final_filename).toFile();

		log.info("handling file import... {}", ftmp);
		log.info("save file into this profile: {}", profile);
		String checksum = "N/A";

		// Check if profile exists, otherwise abort
		if (!profileRepo.existsById(profile)) {
			log.info("profile not found in the system, the file can't be added.");
			log.info("deleting: {}", ftmp.toString());
			// ToDo: transfer to a dumpster folder
			ftmp.delete();
			return;
		}

		// validate target path to move file in
		File profileDir = Paths.get(REPOSITORY_PATH, profile).toFile();
		if (!profileDir.exists()) {
			if (!profileDir.mkdirs()) {
				log.error("unable to create directory: {}", profileDir.getAbsolutePath());
				ftmp.delete();
				return;
			} else {
				log.info("directory created!");
			}
		}

		// move file to the right place
		boolean bresult = moveFile(ftmp.toPath(), dest_file.toPath());
		log.info("the file was moved: {}", bresult);

		try {
			// Use MD5 algorithm
			MessageDigest md5Digest = MessageDigest.getInstance("MD5");

			// calculate the checksum
			long t1 = System.currentTimeMillis();
			checksum = calculateChecksum(md5Digest, dest_file);
			long t2 = System.currentTimeMillis();
			log.info("the imported file has the following checksum: {}, calculated in {} ms", checksum, (t2 - t1));
		} catch (Exception ex) {
			log.error("an error occurred while calculating the checksum: {}", ex.toString());
		}

		// create/update entry on database
		FileForEvent newFile = new FileForEvent();
		Timestamp _now = Timestamp.from(Instant.now());
		newFile.setChecksum(checksum);
		Optional<FileForEvent> existingFile = fileRepo.findByFilenameAndProfileId(final_filename, profile);
		if (!existingFile.isPresent()) {
			log.info("new file, creating a new uuid");
			newFile.setFileId(UUID.randomUUID().toString());
		} else {
			log.info("file already exists in the db, it will be replaced");
			newFile.setFileId(existingFile.get().getFileId());
		}

		newFile.setFilename(final_filename);
		newFile.setShortDesc("File imported automatically");
		newFile.setFileVersion("1.0");
		newFile.setProfileId(profile);
		newFile.setSize(dest_file.length());
		newFile.setUploadDate(_now);
		newFile.setStartDate(_now);
		newFile.setEnabled(true);
		newFile.setFileType(HteTools.inferFileType(final_filename));

		try {
			// FileForEvent saved =
			fileRepo.save(newFile);
		} catch (Exception ex) {
			log.error(ex.toString());
		}

		log.info("file '{}' imported sucessfully!", ftmp.getAbsolutePath());

	}

	// calculate checksum
	/**
	 * 
	 * @param digest
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private String calculateChecksum(MessageDigest digest, File file) throws IOException {
		// Get file input stream for reading the file content
		FileInputStream fis = new FileInputStream(file);

		// Create byte array to read data in chunks
		byte[] byteArray = new byte[1024];
		int bytesCount = 0;

		// Read file data and update in message digest
		while ((bytesCount = fis.read(byteArray)) != -1) {
			digest.update(byteArray, 0, bytesCount);
		}
		// close the stream; We don't need it now.
		fis.close();

		// Get the hash's bytes
		byte[] bytes = digest.digest();

		// This bytes[] has bytes in decimal format;
		// Convert it to hexadecimal format
		StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }

		// return complete hash
		return sb.toString();
	}

	private File getFile(String configKey){
		ConfigEntry pathEntry = configRepo.findById(configKey).orElse(null);
		if(pathEntry == null || pathEntry.getCoValue().isEmpty() || pathEntry.getCoValue() == null){
			log.error("Could not find path entry for key: " + configKey);
			return null;
		}
		return new File(pathEntry.getCoValue());
	}
}
