package com.hobart.hte.repositories;

import com.hobart.hte.utils.access.HTeRole;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<HTeRole, Long>{

    HTeRole findById(Integer id);
    HTeRole findByName(String name);

}
