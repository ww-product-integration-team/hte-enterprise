package com.hobart.hte.upgrading.fileChecking;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.UpgradeDeviceRepository;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import com.hobart.hte.utils.util.HteTools;
import com.jcraft.jsch.Session;

public class GTUpgradeFileCheck extends UpgradeFileCheckStrategy {

    DeviceRepository deviceRepo;
    UpgradeDeviceRepository upgradeDeviceRepo;

    private String ipAddress;
    private String fileName;
    private boolean deleteFiles;
    private final int upgradeTime = 20;

    public GTUpgradeFileCheck(
        String ipAddress, String fileName, boolean deleteFiles,
        DeviceRepository deviceRepo, UpgradeDeviceRepository upgradeDeviceRepo) {
            this.ipAddress = ipAddress;
            this.fileName = fileName;
            this.deviceRepo = deviceRepo;
            this.deleteFiles = deleteFiles;
            this.upgradeDeviceRepo = upgradeDeviceRepo;
        }

    @Override
    public boolean verifyUpgrade() {
        ScaleDevice device = deviceRepo.findByIpAddress(ipAddress);
        String deviceId = device.getDeviceId();

        boolean upgradeApplied = false;
        // For GTs, we'll first figure out the file type before we get a substring on the version:
        String upgradeType = fileName.startsWith("ngs-linux-recovery") ?
                fileName.split("-")[2] : fileName.split("-")[1]; // 'ngs-linux-recovery' or 'gt-application'
        // *Now* we get the version...
        String versionNumber = upgradeType.split("_")[1];

        Session sshSession = HteTools.getSSHSession(ipAddress);
        if (sshSession != null && sshSession.isConnected()) {
            String command = "sed '7!d' /etc/hobart/version.ini | cut -d '.' -f 4-6";
            String commandOutput = HteTools.executeSSHCommandGetOutput(sshSession, command);

            if (deleteFiles) {
                HteTools.executeSSHCommand(sshSession, "cd /opt/hobart/upgrade ; rm *");
            }

            if (commandOutput.contains(versionNumber)) {
                upgradeApplied = true;
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_completed_no_errors.name(),
                        "Upgrade applied successfully.", deviceId);
            } else {
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_failed.name(),
                        "Upgrade was not applied successfully.", deviceId);
            }
        } else { // We couldn't connect; can't certify the status of the scale. Warn the user.
            upgradeDeviceRepo.updateStatusByDeviceId(
                    UpgradeDevice.Status.upgrade_failed.name(),
                    "Could not connect to scale after applying upgrade; cannot verify upgrade application.", deviceId);
        }
        HteTools.disconnectSSH(sshSession);
        return upgradeApplied;
    }

    public void run() {
        verifyUpgrade();
    }

    public int getUpgradeTime() {
        return upgradeTime;
    }
}
