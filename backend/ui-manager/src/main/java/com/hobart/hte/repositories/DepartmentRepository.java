package com.hobart.hte.repositories;

import com.hobart.hte.utils.dept.Department;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface DepartmentRepository extends CrudRepository<Department, String> {
    Boolean existsByDeptName1IgnoreCase(String name);

    Department findByDeptName1IgnoreCase(String name);
    Iterable<Department> findByDeptIdNotNull();
    @Override
    void deleteAll();

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update depts d set d.defaultProfileId = :new_profile where d.defaultProfileId = :current_profile", nativeQuery = true)
    int updateDepartmentSetProfileIdForProfileId(@Param("new_profile") String new_profile,
                                                  @Param("current_profile") String current_profile);
}
