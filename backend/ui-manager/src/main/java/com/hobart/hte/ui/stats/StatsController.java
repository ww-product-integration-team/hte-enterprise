package com.hobart.hte.ui.stats;

import java.util.List;

import com.hobart.hte.utils.device.stats.DeviceCountPerFirmware;
import com.hobart.hte.utils.device.stats.DeviceCountPerModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.ui.UIController;

@Tag(name = "statistics", description = "Endpoints for retrieving statistics from the scales.")
@RestController
@RequestMapping("/stats")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class StatsController {
	private static final Logger log = LogManager.getLogger(UIController.class);

	@Autowired
	DeviceRepository device_repo;

	@Operation(summary = "Get a list of the number of devices by firmware version", description = "Retrieves a list of the number devices in the system by firmware version.")
	@GetMapping(path = "/devicesByFirmware", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getDevicesByFirmware() {
		log.info("retrieving list of devices by firmware");
		List<DeviceCountPerFirmware> totalCount = device_repo.countTotalDevicesByFirmwareVersion();
		return new ResponseEntity<>(totalCount, HttpStatus.OK);
	}

	@Operation(summary = "Get a list of the number of devices by model", description = "Retrieves a list of the number of devices in the system by scale model.")
	@GetMapping(path = "/devicesByModel", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getDevicesByModel() {
		log.info("retrieving list of devices by scale model");
		List<DeviceCountPerModel> totalCount = device_repo.countTotalDevicesByModel();
		return new ResponseEntity<>(totalCount, HttpStatus.OK);
	}
}
