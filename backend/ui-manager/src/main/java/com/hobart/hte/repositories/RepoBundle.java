package com.hobart.hte.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// Singleton object with a shed-load of repository objects.
// If you're working with a class outside UIController that needs asset-related repositories, you can use this as
// a broker for most of the repositories you would ever need.
@Component
public class RepoBundle {

    @Autowired
    public BannerRepository banner_repo;
    @Autowired
    public RegionRepository region_repo;
    @Autowired
    public StoreRepository store_repo;
    @Autowired
    public DepartmentRepository dept_repo;
    @Autowired
    public DeviceRepository device_repo;
    @Autowired
    public ScaleSettingsRepository ssRepo;

    @Autowired
    public ConfigAppRepository configRepo;
    @Autowired
    public FileRepository file_repo;
    @Autowired
    public NodeStatusRepository nodeStatusRepo;
    @Autowired
    public StoreDeptPairsRepository storeDeptPairsRepository;

/*
    public static RepoBundle fetchRepoBundle() {
        if (repoBundle == null) {
            repoBundle = new RepoBundle();
        }
        return repoBundle;
    }

 */
}
