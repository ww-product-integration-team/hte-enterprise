package com.hobart.hte.upgrading.fileChecking;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.UpgradeDeviceRepository;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import com.hobart.hte.utils.util.HteTools;
import com.jcraft.jsch.Session;
import com.hobart.hte.utils.device.ScaleDevice;


public class HTUpgradeFileCheck extends UpgradeFileCheckStrategy {

    DeviceRepository deviceRepo;
    UpgradeDeviceRepository upgradeDeviceRepo;

    private String ipAddress;
    private String fileName;
    private boolean deleteFiles;
    private final int upgradeTime = 30;

    public HTUpgradeFileCheck(
            String ipAddress, String fileName, boolean deleteFiles,
            DeviceRepository deviceRepo, UpgradeDeviceRepository upgradeDeviceRepo) {
        this.ipAddress = ipAddress;
        this.fileName = fileName;
        this.deleteFiles = deleteFiles;
        this.deviceRepo = deviceRepo;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
    }

    @Override
    public boolean verifyUpgrade() {

        ScaleDevice device = deviceRepo.findByIpAddress(ipAddress);
        String deviceId = device.getDeviceId();

        boolean upgradeApplied = false;
        String regex = "[-\\s]";
        // Monolithic files are a bit more verbose, like me
        String applicationVersion = fileName.startsWith("hti-linux-feature-monolithic") ?
                                    fileName.split(regex)[4] : fileName.split(regex)[3];

        Session sshSession = HteTools.getSSHSession(ipAddress);
        if (sshSession != null && sshSession.isConnected()) {
            String command = "head -1 /SOFTWARE_VERSION_INFO";
            String commandOutput = HteTools.executeSSHCommandGetOutput(sshSession, command);

            if (deleteFiles) {
                HteTools.executeSSHCommand(sshSession,"cd /usr/local/hobart/upgrade ; rm *");
            }

            if (commandOutput.contains(applicationVersion)) {
                upgradeApplied = true;
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_completed_no_errors.name(),
                        "Upgrade applied successfully.", deviceId);
            } else {
                upgradeDeviceRepo.updateStatusByDeviceId(
                        UpgradeDevice.Status.upgrade_failed.name(),
                        "Upgrade was not applied successfully.", deviceId);
            }
        } else { // We couldn't connect; can't certify the status of the scale. Warn the user.
            upgradeDeviceRepo.updateStatusByDeviceId(
                    UpgradeDevice.Status.upgrade_failed.name(),
                    "Could not connect to scale after applying upgrade; cannot verify upgrade application.", deviceId);
        }
        HteTools.disconnectSSH(sshSession);
        return upgradeApplied;
    }

    public void run() {
        verifyUpgrade();
    }

    public int getUpgradeTime() {
        return upgradeTime;
    }
}
