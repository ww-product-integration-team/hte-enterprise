package com.hobart.hte.config;

import com.hobart.hte.access.AccessController;
import com.hobart.hte.repositories.ConfigAppRepository;
import com.hobart.hte.ui.UIController;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.tree.NodeStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartupBean {

    public static String projectVersion = "3.1.0";
    public static String buildDate = "10/14/2024";
    private static final Logger log = LogManager.getLogger(StartupBean.class);

    @Autowired
    @Qualifier("customUserDetailsService")
    AccessController accessController;

    @Autowired
    UIController uiController;

    @Autowired
    AboutService aboutService;

    @Autowired
    ConfigController configController;

    @Autowired
    SwaggerConfiguration swaggerConfiguration;

    @Autowired
    ConfigAppRepository configRepo;

    @Autowired
    NodeStatusUpdater statusUpdater;

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {

        // Setup the log location
        ConfigEntry logPath = configRepo.findById(AppConfig.logRootPath).orElse(null);
        if (logPath != null && logPath.getCoValue() != null) {
            System.setProperty("APP_LOG_ROOT", logPath.getCoValue());
            ((org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false)).reconfigure();
        }

        accessController.initializeRoles();
        uiController.cleanAssets();

        aboutService.getServiceInfo(true);

        configController.resetEventRuleDefaults(false, false);
        configController.initializeConfigTable();

        log.info("  _    _ _______     ______       _                       _          ");
        log.info(" | |  | |__   __|   |  ____|     | |                     (_)         ");
        log.info(" | |__| |  | | ___  | |__   _ __ | |_ ___ _ __ _ __  _ __ _ ___  ___ ");
        log.info(" |  __  |  | |/ _ \\ |  __| | '_ \\| __/ _ \\ '__| '_ \\| '__| / __|/ _ \\");
        log.info(" | |  | |  | |  __/ | |____| | | | ||  __/ |  | |_) | |  | \\__ \\  __/");
        log.info(" |_|  |_|  |_|\\___| |______|_| |_|\\__\\___|_|  | .__/|_|  |_|___/\\___|");
        log.info(" ===========================================  | |  ==================");
        log.info(" ITW FEG Hobart 2024(c)                       |_|           (v" + projectVersion + ")");
        log.info("[Build Date: " + buildDate + "]");

        statusUpdater.startUpdateNodeScheduler();
    }
}
