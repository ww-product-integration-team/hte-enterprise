package com.hobart.hte.access;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hobart.hte.helpers.DomainHelper;
import com.hobart.hte.helpers.LicenseHelper;
import com.hobart.hte.helpers.RoleHelper;
import com.hobart.hte.helpers.TokenHelper;
import com.hobart.hte.repositories.*;
import com.hobart.hte.utils.access.HTeDomain;
import com.hobart.hte.utils.access.HTeRole;
import com.hobart.hte.utils.access.HTeUser;
import com.hobart.hte.utils.access.License;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.model.*;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.utils.store.Store;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.hobart.hte.utils.customer.Customer;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "access", description = "Endpoints for logging in and verifying access of the user")
@RestController("customUserDetailsService")
@RequestMapping("/access")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class AccessController implements UserDetailsService {
    private static final Logger log = LogManager.getLogger(AccessController.class);
    private static final String COMPANY_KEY = "HoBaRt241@#@-_@%$#)(^Products";

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    UserRepository user_repo;
    @Autowired
    RoleRepository role_repo;
    @Autowired
    DomainRepository domain_repo;
    @Autowired
    @Lazy
    PasswordEncoder passwordEncoder;

    @Autowired
    LicenseRepository license_repo;

    @Autowired
    LicenseHelper licenseHelper;

    // For Generating Domain Table
    @Autowired
    BannerRepository banner_repo;
    @Autowired
    RegionRepository region_repo;
    @Autowired
    StoreRepository store_repo;
    @Autowired
    DepartmentRepository dept_repo;
    @Autowired
    ProfileRepository profile_repo;

    @Autowired
    CustomerRepository customer_repo;

    private final DomainHelper domainHelper = new DomainHelper();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        OperationStatusModel result = new OperationStatusModel("RequestLogin");
        HTeUser user = user_repo.findByUsername(username);
        if (user == null) {
            log.error("User not found: " + username);
            result.setErrorDescription("The Name fields cannot be null or empty.");
            result.setResult(RequestOperationResult.ERROR.name());
            throw new UsernameNotFoundException("User not found in the database");
        } else {
            log.info("User found in database: " + username + " and domainId: " + user.getDomain().getDomainId());

        }
        TokenHelper tokenHelper = new TokenHelper(user, null);

        Collection<GrantedAuthority> authorities = tokenHelper.generateAuthorities();

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                authorities);
    }

    @Operation(summary = "Generate an access token for a user", description = "Generate an access token for a user based on API Key and Secret")
    @Parameters({
            @Parameter(name = "apiKey", description = "API Key", in = ParameterIn.QUERY, example = "00000000-0000-0000"),
            @Parameter(name = "apiSecret", description = "API Secret", in = ParameterIn.QUERY, example = "00000000-0000-0000")
    })
    @PostMapping(path = "/token")
    public ResponseEntity<?> generateToken(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String apiKey = request.getParameter("apiKey");
        String apiSecret = request.getParameter("apiSecret");
        Customer customer = customer_repo.findByApiKeyAndApiSecret(apiKey, apiSecret);
        if (customer == null) {
            throw new RuntimeException("Can't find requested customer");
        }
        TokenHelper tokenHelper = new TokenHelper(customer, request);
        String access_token = tokenHelper.createCustomerAccessToken();

        Map<String, String> tokens = new HashMap<>();

        tokens.put("access_token", access_token);
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(), tokens);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @Operation(summary = "Refresh the access token of the user", description = "Refresh the access token of the user")
    @GetMapping("/refreshToken")
    public ResponseEntity<?> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            try {
                String refreshToken = authorizationHeader.substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256(TokenHelper.secret);
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refreshToken);
                String username = decodedJWT.getSubject();
                HTeUser user = user_repo.findByUsername(username);

                if (user == null) {
                    throw new RuntimeException("Can't find requested user");
                }

                TokenHelper tokenHelper = new TokenHelper(user, request);
                String access_token = tokenHelper.createAccessToken();
                String refresh_token = tokenHelper.createRefreshToken();

                Map<String, String> tokens = new HashMap<>();

                tokens.put("access_token", access_token);
                tokens.put("refresh_token", refresh_token);
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (Exception e) {
                log.error("ERROR Authenticating : " + e.getMessage());

                response.setStatus(FORBIDDEN.value());
                response.setContentType(APPLICATION_JSON_VALUE);

                OperationStatusModel error = new OperationStatusModel(request.getMethod());
                error.setResult(RequestOperationResult.ERROR.name());
                error.setErrorDescription("Refresh token is not valid");
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Refresh Token is missing");
        }

        return new ResponseEntity<>("OK", HttpStatus.OK);

    }

    @Operation(summary = "Add a user to the database", description = "Registers a user in the database")
    @PostMapping(path = "/user", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> saveUser(@RequestBody HTeUser hteUser) {
        log.info("Attempting to register a new user");

        log.info(hteUser.toString());

        OperationStatusModel result = new OperationStatusModel("CreateNewUser");
        // Check for valid request
        if (isNullorEmpty(hteUser.getFirstName()) ||
                isNullorEmpty(hteUser.getLastName())) {
            result.setErrorDescription("The name fields cannot be null or empty.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (isNullorEmpty(hteUser.getUsername())) {
            result.setErrorDescription("The username field cannot be null or empty.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (hteUser.getId() != null) {
            // ==========================================================================================================
            // Updating an Existing User
            HTeUser existingUser = user_repo.findById(hteUser.getId());
            if (existingUser == null || existingUser.isAdmin()) {
                result.setErrorDescription("Existing user cannot be found");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
            // Set First and Last name
            existingUser.setFirstName(hteUser.getFirstName());
            existingUser.setLastName(hteUser.getLastName());

            // Set Phone number
            existingUser.setPhoneNumber(hteUser.getPhoneNumber());

            // TODO Set new username??

            // TODO separate into new endpoint?
            if (hteUser.getPassword() != null) {
                existingUser.setPassword(passwordEncoder.encode(hteUser.getPassword()));
            }

            // Set Enable/Disable
            if (hteUser.getEnabled() != null) {
                existingUser.setEnabled(hteUser.getEnabled());
            }

            // Set New Access Level
            if (hteUser.getAccessLevel() != null) {
                existingUser.setAccessLevel(hteUser.getAccessLevel());
            }

            // Set New Domain
            if (domain_repo.existsById(hteUser.getDomain().getDomainId())) {
                existingUser.setDomain(hteUser.getDomain());
            } else {
                result.setErrorDescription("Invalid new Domain ID");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            // Save the updated User
            HTeUser savedUser;
            try {
                savedUser = user_repo.save(existingUser);
            } catch (Exception ex) {
                result.setErrorDescription("Error Updating User Info: " + ex.getMessage());
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            result.setResult(RequestOperationResult.SUCCESS.name());
            log.info(String.format("Updating User Info : %s", hteUser.getUsername()));
            return new ResponseEntity<>(result, HttpStatus.OK);
        }

        // ==========================================================================================================
        // If the ID is null then Create a new user
        //
        if (!licenseHelper.canCreateUser(user_repo.count())) {
            result.setErrorDescription("User limit has been reached for the current license");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, FORBIDDEN);
        }

        if (isNullorEmpty(hteUser.getPassword())) {
            result.setErrorDescription("The password field cannot be null or empty.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (user_repo.existsByUsername(hteUser.getUsername())) {
            result.setErrorDescription("That email address is already registered in the system.");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        // Set New Domain
        if (hteUser.getDomain() == null || !domain_repo.existsById(hteUser.getDomain().getDomainId())) {
            result.setErrorDescription("Invalid new Domain ID");
            log.info(hteUser.getDomain().toString());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (hteUser.getEnabled() == null) {
            hteUser.setEnabled(true);
        }

        // Set The created on TimeStamp
        hteUser.setDateCreated(Timestamp.from(Instant.now()));

        // Generate a new UUID to represent the user
        String tmp = UUID.randomUUID().toString();
        hteUser.setId(tmp);
        log.info("New UUID assigned for new user: {}", tmp);
        hteUser.setPassword(passwordEncoder.encode(hteUser.getPassword()));

        HTeUser savedUser;
        try {
            savedUser = user_repo.save(hteUser);
        } catch (Exception ex) {
            result.setErrorDescription("Error Saving New User: " + ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        log.info(String.format("Registering User : %s", hteUser.getUsername()));
        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @Operation(summary = "Change a password of a user", description = "Deactivates a user")
    @PostMapping(path = "/user/changePassword", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> changePassword(@RequestParam String username,
            @RequestParam String newPassword) {
        OperationStatusModel result = new OperationStatusModel("ChangeUserPassword");

        HTeUser existingUser = user_repo.findByUsername(username);
        if (existingUser == null) {
            result.setErrorDescription("Invalid username");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (newPassword == null || newPassword.length() < 4) {
            result.setErrorDescription("Invalid new password");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            existingUser.setPassword(passwordEncoder.encode(newPassword));
            user_repo.save(existingUser);
        } catch (Exception ex) {
            result.setErrorDescription("Could not update password: " + ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("User password successfully reset!");
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @Operation(summary = "Enable/Disable A User", description = "Deactivates a user")
    @PostMapping(path = "/user/enable", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> enableUser(@RequestParam String userId, @RequestParam Boolean enabled) {
        log.info("Attempting to enable/disable user : " + userId);

        OperationStatusModel result = new OperationStatusModel("EnableDisableUser");

        HTeUser existingUser = user_repo.findById(userId);
        if (existingUser == null) {
            result.setErrorDescription("Invalid User ID");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (enabled == null) {
            result.setErrorDescription("Invalid Enable Status");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        existingUser.setEnabled(enabled);

        // Save the updated User
        HTeUser savedUser;
        try {
            savedUser = user_repo.save(existingUser);
        } catch (Exception ex) {
            result.setErrorDescription("Error Updating User Info: " + ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        log.info(String.format("Updating User Info (Enabled/Disabled) : %s", existingUser.getUsername()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Transactional
    @Parameters({
            @Parameter(name = "id", description = "The Id or username of the user", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)),
            @Parameter(name = "idtype", description = "The type of id to use, it could be a UUID or an Username", in = ParameterIn.QUERY, example = "USERNAME | UUID", schema = @Schema(implementation = String.class)) })
    @Operation(summary = "Removes a registered user from the database", description = "Removes a registered user from the database")
    @DeleteMapping(path = "/user", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> deleteUser(@RequestParam(name = "id", required = false) String id,
            @RequestParam(name = "idtype", required = false) IdType idType) {
        log.info("Attempting to delete user: " + id + "...");

        OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());
        // If the user ID is not provided
        if (id == null) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Requested ID cannot be null");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        HTeUser user;
        switch (idType) {
            case UUID:
                user = user_repo.findById(id);
                break;
            case USERNAME:
                user = user_repo.findByUsername(id);
                break;
            default:
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("The given IdType is incorrect, check parameters");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (user == null) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Requested user cannot be found");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            // Remove all the User's assigned roles and then save
            user.setRoles(new ArrayList<>());
            user_repo.save(user);

            // Now delete the user
            entityManager.remove(user);

        } catch (Exception ex) {
            log.error("Error Deleting User " + user.getUsername() + ": " + ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error Deleting User " + user.getUsername() + ": " + ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        log.info("Successfully deleted user");
        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Updates the roles table", description = "Registers a user in the database")
    @PostMapping(path = "/initializeRolesAndDomains")
    public ResponseEntity<OperationStatusModel> initializeRoles() {

        OperationStatusModel result = new OperationStatusModel(RequestOperationType.CREATE.name());

        try {
            log.info("Generating Default Entities");
            try {

                Profile defaultProfile = new Profile();
                defaultProfile.setProfileId(AppConfig.DefaultProfileId);
                defaultProfile.setDescription("Initial Profile assigned to new scales");
                defaultProfile.setName("Default Profile");
                defaultProfile.setRequiresDomainId(AppConfig.DefaultAdminId);
                defaultProfile.setLastUpdate(new Timestamp(System.currentTimeMillis()));
                profile_repo.save(defaultProfile);

            } catch (Exception ex) {

                log.error("Couldn't generate defaults" + ex);
            }

            log.info("Generating Domain Table");
            // ==================================================================
            // Domains
            Iterable<HTeUser> allUsers = user_repo.findAll();
            Map<String, String> userMap = new HashMap<>();

            log.info("Resetting User Domains... ");
            for (HTeUser user : allUsers) {
                if (user.getDomain() != null) {
                    userMap.put(user.getId(), user.getDomain().getDomainId());
                } else {
                    userMap.put(user.getId(), AppConfig.DefaultUnassignedId);
                }
                try {
                    user.setDomain(null);
                    user_repo.save(user);
                } catch (Exception err) {
                    log.error("Error resetting domain Ids : " + err.getMessage());
                }
            }

            try {
                domain_repo.deleteAll();
            } catch (Exception ex) {
                log.error("Could not clear domain table" + ex);
            }
            log.info("Creating Default Domains");
            HTeDomain adminDomain = domain_repo.save(new HTeDomain(AppConfig.DefaultAdminId, EntityType.ENTITY, null));
            domain_repo.save(new HTeDomain(AppConfig.DefaultUnassignedId, EntityType.ENTITY, AppConfig.DefaultAdminId));
            domain_repo.save(new HTeDomain(AppConfig.DefaultBannerId, EntityType.BANNER, AppConfig.DefaultAdminId));

            try {
                log.info("Creating Admin User...");
                HTeUser admin = user_repo.findById(AppConfig.HtAdminId);
                if (admin == null) {
                    admin = new HTeUser();
                    admin.setId(AppConfig.HtAdminId);
                    admin.setDateCreated(Timestamp.from(Instant.now()));
                }
                admin.setFirstName("Hobart");
                admin.setLastName("Support");
                admin.setPhoneNumber("+1-937-332-2789");
                admin.setUsername("ht.support@itwfeg.com");
                admin.setAccessLevel(5000);
                admin.setEnabled(true);
                admin.setPassword(passwordEncoder.encode("admin3230"));
                admin.setDomain(adminDomain);

                user_repo.save(admin);

            } catch (Exception ex) {
                log.error("Could not save admin user!");
            }

            // Banners
            log.info("Creating Banner Domains");
            domain_repo.save(new HTeDomain(AppConfig.DefaultBannerId, EntityType.ENTITY, AppConfig.DefaultAdminId));
            Iterable<Banner> banners = banner_repo.findAll();
            for (Banner b : banners) {
                domain_repo.save(new HTeDomain(b.getBannerId(), EntityType.BANNER, AppConfig.DefaultAdminId));
            }

            // Regions
            log.info("Creating Region Domains");
            Iterable<Region> regions = region_repo.findAll();
            for (Region r : regions) {
                if (r.getBannerId() != null && domain_repo.findByDomainId(r.getBannerId()) != null) {
                    domain_repo.save(new HTeDomain(r.getRegionId(), EntityType.REGION, r.getBannerId()));
                } else {
                    r.setBannerId(AppConfig.DefaultUnassignedId);
                    region_repo.save(r);
                    domain_repo.save(new HTeDomain(r.getRegionId(), EntityType.REGION, AppConfig.DefaultUnassignedId));
                }
            }

            // Stores
            log.info("Creating Store Domains");
            Iterable<Store> stores = store_repo.findAll();
            for (Store s : stores) {
                if (s.getRegionId() != null && domain_repo.findByDomainId(s.getRegionId()) != null) {
                    domain_repo.save(new HTeDomain(s.getStoreId(), EntityType.STORE, s.getRegionId()));
                } else {
                    s.setRegionId(AppConfig.DefaultUnassignedId);
                    store_repo.save(s);
                    domain_repo.save(new HTeDomain(s.getStoreId(), EntityType.STORE, AppConfig.DefaultUnassignedId));
                }
            }

            // Departments
            log.info("Creating Department Domains");
            Iterable<Department> depts = dept_repo.findAll();
            for (Department d : depts) {
                if (d.getRequiresDomainId() != null && domain_repo.findByDomainId(d.getRequiresDomainId()) != null) {
                    domain_repo.save(new HTeDomain(d.getdeptId(), EntityType.DEPARTMENT, d.getRequiresDomainId()));
                } else {
                    d.setRequiresDomainId(AppConfig.DefaultUnassignedId);
                    dept_repo.save(d);
                    domain_repo
                            .save(new HTeDomain(d.getdeptId(), EntityType.DEPARTMENT, AppConfig.DefaultUnassignedId));
                }
            }

            log.info("Creating Profile Domains");
            Iterable<Profile> profiles = profile_repo.findAll();
            for (Profile p : profiles) {
                if (p.getRequiresDomainId() != null && domain_repo.findByDomainId(p.getRequiresDomainId()) != null) {
                    domain_repo.save(new HTeDomain(p.getProfileId(), EntityType.PROFILE, p.getRequiresDomainId()));
                } else {
                    p.setRequiresDomainId(AppConfig.DefaultUnassignedId);
                    profile_repo.save(p);
                    domain_repo
                            .save(new HTeDomain(p.getProfileId(), EntityType.PROFILE, AppConfig.DefaultUnassignedId));
                }
            }

            log.info("Setting User Domains... ");
            HTeDomain unassignedDomain = domain_repo.findById(AppConfig.DefaultUnassignedId).orElse(null);
            if (unassignedDomain == null) {
                log.error("FATAL ERROR : cannot find default unassigned domain!");
                throw new EntityNotFoundException("Cannot find default unassigned domain");
            }
            for (HTeUser user : allUsers) {
                HTeDomain userDomain = domain_repo.findById(userMap.get(user.getId())).orElse(null);
                if (userDomain == null) {
                    log.error("Can't find user's domain user: " + user.getUsername() + " domain: "
                            + userMap.get(user.getId()));
                    user.setDomain(unassignedDomain);
                } else {
                    user.setDomain(userDomain);
                }

                try {
                    user_repo.save(user);
                } catch (Exception err) {
                    log.error("Error setting user domain Ids : " + err.getMessage());
                }
            }

            log.info("Initializing Roles...");
            // ==================================================================
            // Operator Role Permissions - Access Level 1000
            role_repo.save(new HTeRole(RoleHelper.MONITOR,
                    "Gives the user the ability to view the current status of the scales in the fleet"));
            role_repo.save(new HTeRole(RoleHelper.REBOOT, "Allows the user to send the reboot command to a scale"));
            role_repo.save(new HTeRole(RoleHelper.HEARTBEAT,
                    "Allows the user to send a manual heartbeat command to the scale"));
            role_repo.save(
                    new HTeRole(RoleHelper.PING, "Allows the user to perform status checks on individual scales"));
            // Supervisor Role Permissions - Access Level 2000
            role_repo.save(new HTeRole(RoleHelper.ENABLE_DISABLE,
                    "Allows the user to toggle the scales enable function, when a scale is disabled no updates will occur"));
            role_repo.save(new HTeRole(RoleHelper.ASSIGN_SCALES,
                    "Allows the user to assign and reassign scales to different departments"));
            role_repo.save(new HTeRole(RoleHelper.MANAGE_OPERATORS, "Permissions to add and remove new HTe operators"));
            role_repo.save(new HTeRole(RoleHelper.ASSIGN_SCALE_PROFILES,
                    "Allows the user to assign a manual profile to a scale"));
            role_repo.save(new HTeRole(RoleHelper.DELETE_SCALE_DATA,
                    "User can send a \"Delete All Data\" command to the scale"));
            role_repo.save(new HTeRole(RoleHelper.FILE_VIEWER,
                    "User can view and download available files hosted on the server"));
            role_repo.save(new HTeRole(RoleHelper.USER_ENABLEDISABLE,
                    "User can deactivate/activate other users in their domain, preventing them from using the site"));
            role_repo.save(
                    new HTeRole(RoleHelper.DOWNLOAD_TRANS, "Ability to download transactions from a store server"));

            // IT Role Permissions - Access Level 3000
            role_repo.save(new HTeRole(RoleHelper.MANAGE_PROFILES,
                    "Adds ability to add, remove or modify department profiles"));
            role_repo.save(new HTeRole(RoleHelper.ASSIGN_DEPT_PROFILES,
                    "Allows the user to assign existing profiles to departments"));
            role_repo.save(
                    new HTeRole(RoleHelper.FULL_TREEVIEW, "Gives the user full access to modify the asset tree view"));
            role_repo.save(
                    new HTeRole(RoleHelper.MANAGE_SUPERVISORS, "Permissions to add and remove new HTe supervisors"));
            role_repo.save(new HTeRole(RoleHelper.MANAGE_LICENSES, "Import/Delete new and existing licenses for HTe"));
            role_repo.save(new HTeRole(RoleHelper.SYSTEM_MANAGER, "Permissions to upgrade frontend/backend systems"));
            role_repo.save(new HTeRole(RoleHelper.FILE_MANAGER, "User can modify and upload files to the server"));
            role_repo.save(
                    new HTeRole(RoleHelper.SET_PRIMARY_SCALE, "User can indicate a primary scale inside a department"));
            role_repo.save(
                    new HTeRole(RoleHelper.UPGRADE_DEVICES, "User can upgrade firmware and send files to devices"));

            // Admin Role Permissions - Access Level 4000
            role_repo.save(new HTeRole(RoleHelper.IMPORT_TREE,
                    "User can import existing scale data from their HTe Business Application"));
            role_repo.save(new HTeRole(RoleHelper.FLEET_STATUS,
                    "User can get and export the status of all scales in a fleet"));

            // Hobart Role Permissions - Access Level 5000
            role_repo.save(new HTeRole(RoleHelper.MANUAL_OVERRIDE, "General Override Permission"));
            role_repo
                    .save(new HTeRole(RoleHelper.OTP_PASS_MANAGER, "Manage configuration of One time password access"));

        } catch (Exception ex) {
            result.setErrorDescription(ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }
        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @Parameters({
            @Parameter(name = "id", description = "UserID or Username of the user to retrieve", in = ParameterIn.QUERY, example = "john.doe@mail.com", schema = @Schema(implementation = String.class)),
            @Parameter(name = "idtype", description = "The type of id to use, it could be a UUID or an username", in = ParameterIn.QUERY, example = "USERNAME | UUID", schema = @Schema(implementation = IdType.class)) })
    @Operation(summary = "Grabs one or all users", description = "Grabs a user or a list of user")
    @GetMapping(path = "/user", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getUser(@RequestParam(name = "id", required = false) String id,
            @RequestParam(name = "idtype", required = false) IdType idType) {
        OperationStatusModel result = new OperationStatusModel(RequestOperationType.REQUEST.name());
        // If the user ID is not provided
        if (id == null) {
            Iterable<HTeUser> allUsers = user_repo.findByIdNot(AppConfig.HtAdminId);
            return new ResponseEntity<>(allUsers, HttpStatus.OK);
        }
        HTeUser user;
        switch (idType) {
            case UUID:
                user = user_repo.findById(id);
                break;
            case USERNAME:
                user = user_repo.findByUsername(id);
                break;
            default:
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("The given IdType is incorrect, check parameters");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (user == null) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("The requested user cannot be found");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Operation(summary = "Grabs one or all roles", description = "Grabs a role or a list of roles")
    @GetMapping(path = "/roles", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getRoles(@RequestParam(name = "id", required = false) Integer roleId) {
        OperationStatusModel result = new OperationStatusModel(RequestOperationType.REQUEST.name());
        if (roleId == null) {
            Iterable<HTeRole> allRoles = role_repo.findAll();

            HashMap<String, HTeRole> roleMap = new HashMap<>();
            for (HTeRole role : allRoles) {
                roleMap.put(role.getName(), role);
            }

            return new ResponseEntity<>(roleMap, HttpStatus.OK);
        }

        HTeRole role = role_repo.findById(roleId);
        if (role == null) {
            result.setErrorDescription("Role " + roleId + " not found");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(role, HttpStatus.OK);

    }

    @Operation(summary = "Assign a users roles", description = "Overwrites a user's roles with the provided parameter")
    @PostMapping(path = "/assignRole", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> addRoleToUser(@RequestParam(name = "userId") String userId,
            @RequestParam(name = "roleId") List<Integer> roles) {
        OperationStatusModel result = new OperationStatusModel("AddRoleToUser");

        HTeUser user = user_repo.findById(userId);
        if (user == null) {
            result.setErrorDescription("User not found with ID: " + userId);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
        // Clear existing roles before adding new ones
        user.setRoles(new ArrayList<>());
        for (Integer roleId : roles) {

            HTeRole role = role_repo.findById(roleId);
            if (role == null) {
                result.setErrorDescription("Role not found with ID: " + roleId);
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
            }

            // Only add if the new role is not a duplicate
            user.addRole(role);
        }

        try {
            user_repo.save(user);
        } catch (Exception ex) {
            result.setErrorDescription(ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Removes a role assignment from a user", description = "Removes a role assignment from a user")
    @DeleteMapping(path = "/assignRole", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> removeRoleFromUser(@RequestParam(name = "userId") String userId,
            @RequestParam(name = "roleId") Integer roleId) {

        OperationStatusModel result = new OperationStatusModel(RequestOperationType.DELETE.name());

        HTeUser user = user_repo.findById(userId);
        if (user == null) {
            result.setErrorDescription("User not found with ID: " + userId);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        HTeRole role = role_repo.findById(roleId);
        if (role == null) {
            result.setErrorDescription("Role not found with ID: " + roleId);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        try {
            boolean success = user.removeRole(role);
            if (!success) {
                result.setErrorDescription("User does not have selected role");
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
            }
            user_repo.save(user);
        } catch (Exception ex) {
            result.setErrorDescription(ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Grabs one or all of the domains", description = "Grabs one or all of the domains")
    @GetMapping(path = "/domains", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getDomains(@RequestParam(name = "domainId", required = false) String domainId) {
        OperationStatusModel result = new OperationStatusModel(RequestOperationType.REQUEST.name());
        if (domainId == null) {
            Iterable<HTeDomain> allDomains = domain_repo.findAll();

            HashMap<String, HTeDomain> domainMap = new HashMap<>();
            for (HTeDomain domain : allDomains) {
                domainMap.put(domain.getDomainId(), domain);
            }
            return new ResponseEntity<>(domainMap, HttpStatus.OK);
        }

        HTeDomain domain = domain_repo.findByDomainId(domainId);
        if (domain == null) {
            result.setErrorDescription("Domain " + domainId + " not found");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(domain, HttpStatus.OK);

    }

    @Operation(summary = "Assign a domain to a user", description = "Sets the users domain for assets they are allowed to alter")
    @PostMapping(path = "/assignDomain", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> assignDomain(@RequestParam(name = "userId") String userId,
            @RequestParam(name = "domainId") String domainId) {
        OperationStatusModel result = new OperationStatusModel("AssignUserDomain");

        HTeUser user = user_repo.findById(userId);
        if (user == null) {
            result.setErrorDescription("User not found with ID: " + userId);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        HTeDomain domain = domain_repo.findByDomainId(domainId);
        if (domain == null) {
            result.setErrorDescription("Domain not found with ID: " + domainId);
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        try {
            user.setDomain(domain);
            user_repo.save(user);
        } catch (Exception ex) {
            result.setErrorDescription(ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Upload a license in the repository.", description = "Uploads a license to the server, checks the license and if valid will send temporary access keys to allow the user to change the password")
    @Parameters({
            @Parameter(name="createUser", description="Optional field to say if the license should create a user", in=ParameterIn.QUERY, example="false", schema=@Schema(implementation=Boolean.class))
    })
    @PostMapping("/license")
    public ResponseEntity<?> handleUploadLicense(@RequestParam("file") MultipartFile file, @RequestParam(value="createUser", required=false) Boolean createUser,
                                                 HttpServletRequest request) {
        OperationStatusModel result = new OperationStatusModel("installLicense");

        // Parse the license file
        // ==============================================================================================================
        Pair<License, Map<String, String>> licenseMapPair;
        try {
            licenseMapPair = LicenseHelper.getLicenseElements(file);
        } catch (Exception e) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error parsing out license elements");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        // Validate the license file
        // ==============================================================================================================
        try {

            OperationStatusModel validLicenseResult = LicenseHelper.validateLicense(licenseMapPair.getSecond(), license_repo);

            // If the license is not valid then return an error
            if (validLicenseResult.getResult().equals(RequestOperationResult.ERROR.name())) {
                return new ResponseEntity<>(validLicenseResult, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.error("Install license Something went wrong " + e.getMessage());
            result.setErrorDescription("Error validating license: " + e.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        // Save the license file
        // ==============================================================================================================

        // Create the License directory if it doesn't exist
        File licenseDir = Paths.get(LicenseHelper.LICENSE_PATH).toFile();
        if (!licenseDir.exists()) {
            if (!licenseDir.mkdirs()) {
                log.error("Unable to create license directory: {} ", licenseDir.getAbsolutePath());
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Unable to create license directory: " + licenseDir.getAbsolutePath());
                return new ResponseEntity<>(result,
                        HttpStatus.INTERNAL_SERVER_ERROR);
            } else {
                log.debug("license directory created!");
            }
        }

        // Save the file under the license
        String filename = file.getOriginalFilename();
        File ftmp = Paths.get(LicenseHelper.LICENSE_PATH, filename).toFile();
        try {
            file.transferTo(ftmp);
        } catch (Exception ex) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("An error occurred when trying to transfer the file: "
                    + ftmp.getAbsolutePath() + "\nerror: " + ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info("license uploaded: " + filename);

        // Save the license object to the table
        // ==============================================================================================================
        try {
            license_repo.save(licenseMapPair.getFirst());
        } catch (Exception ex) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("An error occurred when trying to create a new license entry: "
                    + ftmp.getAbsolutePath() + "\nerror: " + ex.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (createUser == null || createUser) {
            // Create a new user or update existing, by resetting password
            // ==============================================================================================================
            HTeUser user = user_repo.findByUsername(licenseMapPair.getSecond().get("user email"));
            String newPass = "Hobart!$" + generatePasscode();
            log.info("New password is: " + newPass);
            if (user == null) { // New User
                user = LicenseHelper.generateUser(licenseMapPair.getSecond(),
                        domain_repo.findByDomainId(AppConfig.DefaultAdminId), 4000);
            }

            // Set or reset the password of the new license holder
            user.setPassword(passwordEncoder.encode(newPass));

            try {
                user_repo.save(user);
            } catch (Exception ex) {
                result.setErrorDescription(
                        "License successfully installed, error updating license holder user: " + ex.getMessage());
                result.setResult(RequestOperationResult.ERROR.name());
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }

            result.setErrorDescription("License successfully installed, your new login is:\nUsername: " + user.getUsername()
                    + "\nPassword: " + newPass);
        } else {
            result.setErrorDescription("License successfully installed");
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Get a license", description = "Get license details")
    @Parameters({
            @Parameter(name = "id", description = "Identificator of the license. (Optional)", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
    @GetMapping(path = "/license", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getLicense(@RequestParam(name = "id", required = false) String id) {
        if (id == null) {
            Iterable<License> findAll = license_repo.findAll();
            return new ResponseEntity<>(findAll, HttpStatus.OK);
        }

        if (license_repo.findById(id).isPresent()) {
            return new ResponseEntity<>(license_repo.findById(id), HttpStatus.OK);
        }
        OperationStatusModel result = new OperationStatusModel("Get License");
        result.setResult(RequestOperationResult.ERROR.name());
        result.setErrorDescription("License id: [" + id + "] not found!");
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

    }

    private int generatePasscode() {
        int min = 100000;
        int max = 999999;
        return (int) ((Math.random() * (max - min)) + min);
    }

    private boolean isNullorEmpty(String s) {
        return (s == null || s.isEmpty());
    }
}