package com.hobart.hte.pricingZone;

import com.hobart.hte.repositories.ItemRepository;
import com.hobart.hte.repositories.PricingZoneRepository;
import com.hobart.hte.utils.item.PricingZone;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/pricing")
@CrossOrigin(origins="*", allowedHeaders="*") // added to let react reach the service correctly
public class PricingZoneController {
    private static final Logger log = LogManager.getLogger(PricingZoneController.class);

    @Autowired
    PricingZoneRepository pricingRepo;
    @Autowired
    ItemRepository itemRepo;

    @Operation(summary="Gets all or one pricing zone(s)", description="Retrieves all or one pricing zone(s) from the database")
    @Parameters({
            @Parameter(name="zoneId", description="The id of the zone to get", in= ParameterIn.QUERY, example="", schema=@Schema(implementation=String.class))
    })
    @GetMapping(path="/getPricingZones", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getPricingZones(@RequestParam(name="zoneId", required=false) String zoneId) {
        OperationStatusModel result = new OperationStatusModel("Get Pricing Zones");
        log.info("Begin fetching pricing zones");

        PricingZoneFunctions funcs = new PricingZoneFunctions(pricingRepo, result);
        Map<String, PricingZone> zones = funcs.getZones(zoneId);

        if (result.getResult() == null) {
            result.setResult(RequestOperationResult.SUCCESS.name());
            result.setErrorDescription("Successfully fetched pricing zones");
            log.info("Finish fetching pricing zones");
            return new ResponseEntity<>(zones, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, funcs.resultStatus);
        }
    }

    @Operation(summary="Saves a pricing zone", description="Given a pricing zone, saves it to the database and updates any existing ones")
    @PostMapping(path="/saveZone", consumes={MediaType.APPLICATION_JSON_VALUE}, produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> saveZone(@RequestBody PricingZone zone) {
        OperationStatusModel result = new OperationStatusModel("Save Pricing Zone");
        log.info("Begin saving zone");

        PricingZoneFunctions funcs = new PricingZoneFunctions(pricingRepo, result);
        funcs.saveZone(zone);

        if (result.getResult() == null) {
            result.setResult(RequestOperationResult.SUCCESS.name());
            result.setErrorDescription("Successfully saved zone to database");
            log.info("Finish saving zone");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, funcs.resultStatus);
        }
    }

    @Operation(summary="Deletes a pricing zone", description="Given a pricing zone id, deletes the matching zone from the database")
    @Parameters({
            @Parameter(name="zoneId", description="The id of the zone to get", in= ParameterIn.QUERY, example="", schema=@Schema(implementation=String.class))
    })
    @DeleteMapping(path="/deleteZone", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OperationStatusModel> deleteZone(@RequestParam(name="zoneId", required=false) String zoneId) {
        OperationStatusModel result = new OperationStatusModel("Delete Pricing Zone");
        log.info("Begin deleting zone");

        PricingZoneFunctions funcs = new PricingZoneFunctions(pricingRepo, result);
        funcs.deleteZone(zoneId);

        if (result.getResult() == null) {
            result.setResult(RequestOperationResult.SUCCESS.name());
            result.setErrorDescription("Successfully deleted zone from database");
            log.info("Finish deleting zone");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, funcs.resultStatus);
        }
    }

}
