package com.hobart.hte.scale;

import com.hobart.hte.helpers.DomainHelper;
import com.hobart.hte.repositories.*;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.utils.access.HTeDomain;
import com.hobart.hte.utils.dashboard.DashboardData;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import java.util.Arrays;

import com.hobart.hte.repositories.StoreDeptPairsRepository;
import com.hobart.hte.utils.tree.ScaleNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public class DeleteAllAssetsEndpoint extends ControllerEndpoint<OperationStatusModel> {

    private static final Logger log = LogManager.getLogger(DeleteAllAssetsEndpoint.class);

    private final BannerRepository bannerRepo;
    private final RegionRepository regionRepo;
    private final StoreRepository storeRepo;
    private final DepartmentRepository deptRepo;
    private final DeviceRepository deviceRepo;
    private final ScaleSettingsRepository ssRepo;
    private final UpgradeGroupRepository upgradeGroupRepo;
    private final UpgradeDeviceRepository upgradeDeviceRepo;
    private final UpgradeBatchRepository upgradeBatchRepo;
    private final FileRepository fileRepo;
    private final DomainRepository domainRepo;
    private final DomainHelper domainHelper;
    private final NodeStatusRepository nodeStatusRepository;
    private final StoreDeptPairsRepository storeDeptPairsRepository;
    private final FilterRepository filterRepository;

    public DeleteAllAssetsEndpoint(BannerRepository bannerRepo, RegionRepository regionRepo,
                                   StoreRepository storeRepo, DepartmentRepository deptRepo,
                                   DeviceRepository deviceRepo, ScaleSettingsRepository ssRepo,
                                   UpgradeGroupRepository upgradeGroupRepo, UpgradeDeviceRepository upgradeDeviceRepo,
                                   UpgradeBatchRepository upgradeBatchRepo, DomainRepository domainRepo,
                                   FileRepository fileRepo, DomainHelper domainHelper, NodeStatusRepository nodeStatusRepository,
                                   StoreDeptPairsRepository storeDeptPairsRepository, FilterRepository filterRepository) {
        this.bannerRepo = bannerRepo;
        this.regionRepo = regionRepo;
        this.storeRepo = storeRepo;
        this.deptRepo = deptRepo;
        this.deviceRepo = deviceRepo;
        this.ssRepo = ssRepo;
        this.upgradeGroupRepo = upgradeGroupRepo;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
        this.upgradeBatchRepo = upgradeBatchRepo;
        this.fileRepo = fileRepo;
        this.domainRepo = domainRepo;
        this.domainHelper = domainHelper;
        this.nodeStatusRepository = nodeStatusRepository;
        this.storeDeptPairsRepository = storeDeptPairsRepository;
        this.filterRepository = filterRepository;
    }

    public OperationStatusModel execute() {
        log.info("Begin deleting all asset data");
        OperationStatusModel result = new OperationStatusModel("DeleteAllAssets");

        try {
            upgradeDeviceRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting upgrade device data");
            result.setErrorDescription("Error deleting upgrade device data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            upgradeGroupRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting upgrade group data");
            result.setErrorDescription("Error deleting upgrade group data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            upgradeBatchRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting upgrade batch data");
            result.setErrorDescription("Error deleting upgrade batch data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            fileRepo.deleteByProfileId(null);
        } catch (Exception e) {
            log.error("Error deleting upgrade file data");
            result.setErrorDescription("Error deleting upgrade file data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            ssRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting scale settings data");
            result.setErrorDescription("Error deleting scale settings data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            deviceRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting scale device data");
            result.setErrorDescription("Error deleting scale device data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            deptRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting department data");
            result.setErrorDescription("Error deleting department data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            storeRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting store data");
            result.setErrorDescription("Error deleting store data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            regionRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting region data");
            result.setErrorDescription("Error deleting region data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            bannerRepo.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting banner data");
            result.setErrorDescription("Error deleting banner data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            List<HTeDomain> domains = domainRepo.findByTypeNotIn(Arrays.asList("ENTITY", "PROFILE"));
            for (HTeDomain domain : domains) {
                domainHelper.safeDelete(domain.getDomainId());
            }
        } catch (Exception e) {
            log.error("Error deleting domain data");
            result.setErrorDescription("Error deleting domain data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            nodeStatusRepository.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting nodestatus data");
            result.setErrorDescription("Error deleting nodestatus  data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            storeDeptPairsRepository.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting store dept key pairs data");
            result.setErrorDescription("Error deleting store dept key pairs data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }
        try {
            filterRepository.deleteAll();
        } catch (Exception e) {
            log.error("Error deleting store dept key pairs data");
            result.setErrorDescription("Error deleting store dept key pairs data");
            result.setResult(RequestOperationResult.ERROR.name());
            return result;
        }

        log.info("Successfully deleted all asset data");
        result.setErrorDescription("Successfully deleted all asset data");
        result.setResult(RequestOperationResult.SUCCESS.name());
        return result;
    }
}
