package com.hobart.hte.licensing;

import javax.servlet.http.HttpServletRequest;

import java.io.*;
import java.util.List;
import java.util.UUID;

import com.hobart.hte.repositories.ConfigAppRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.util.HteTools;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;

import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.http.HttpStatus;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import com.hobart.hte.utils.access.License;
import com.hobart.hte.repositories.LicenseRepository;

import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@SecurityRequirement(name="Authorization")

@Tag(name="licensing", description="Endpoints for Licensing Functions")
@RestController
@RequestMapping("/licensing")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class LicensingController {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    LicenseRepository license_repo;

    @Autowired
    ConfigAppRepository configRepo;

    private static final Logger log = LogManager.getLogger(LicensingController.class);
    private static final String COMPANY_KEY = "HoBaRt241@#@-_@%$#)(^Products";

    @Operation(summary="Validate a license", description="Daily license check-in and validation with HQ from a customer system")
    @GetMapping(path="/check", produces={ MediaType.APPLICATION_JSON_VALUE })

    public ResponseEntity<?> checkLicense(@RequestParam(name="licenseId", required = false) String licenseId,
            HttpServletRequest request) {
        License existingLicense = license_repo.findByLicenseId(licenseId);
        if (existingLicense == null) {
            // If the existing license does not exist
            OperationStatusModel result = new OperationStatusModel("Update License");
            result.setErrorDescription("Existing license not found");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>("{'status': 'found license', 'licenseId': " + licenseId + "}",
                HttpStatus.OK);
    }

    @Transactional
    @Operation(summary="Delete a license", description="Mark a license as deleted")
    @PostMapping(path="/delete", produces={ MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> deleteLicense(@RequestParam(name="id", required = false) String licenseId) {
        License existingLicense = license_repo.findByLicenseId(licenseId);
        entityManager.remove(existingLicense);
        return new ResponseEntity<>("{'result': 'license_deleted', 'license_uuid': '" + licenseId + "'}",
                HttpStatus.OK);
    }

    @Operation(summary="Create a license", description="Create new licenses")
    @PostMapping(path="/create", produces={ MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> createLicense(@RequestBody License license) {
        String newID = UUID.randomUUID().toString();
        license.setLicenseId(newID);
        License savedLicense;
        savedLicense = license_repo.save(license);
        return new ResponseEntity<>("{'result': 'license_created', 'license_id': '" + newID + "'}", HttpStatus.OK);
    }

    @Operation(summary="List licenses", description="View a list of licenses by owner email")
    @GetMapping(path="/list", produces={ MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> listLicences(@RequestParam(name="email") String email) {
        List<License> licenses = license_repo.findByOwnerEmail(email);
        return new ResponseEntity<>(licenses, HttpStatus.OK);
    }

    @Operation(summary="Request a license", description="Receive License requests from an HQ server. If a license exists, it is returned from the cloud service.")
    @GetMapping(path="/get", produces={ MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> requestLicense(@RequestParam(name="id", required = false) String licenseId,
            HttpServletRequest request) {
        License existingLicense = license_repo.findByLicenseId(licenseId);
        return new ResponseEntity<>(existingLicense, HttpStatus.OK);
    }

    @Operation(summary="Update a license", description="Make changes to existing licenses")
    @PostMapping(path="/update", produces={ MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> updateLicense(@RequestBody License license) {
        OperationStatusModel result = new OperationStatusModel("Update License");
        // look for existing license
        License existingLicense = license_repo.findByLicenseId(license.licenseId);
        if (existingLicense == null) {
            // If the existing license does not exist
            result.setErrorDescription("Existing license not found");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

        license.setLicenseId(license.licenseId);
        License savedLicense;
        savedLicense = license_repo.save(license);
        return new ResponseEntity<>("{'result': 'license_updated', 'licenseId': " + license.licenseId + "}",
                HttpStatus.OK);
    }

    @Operation(summary="Download a license", description="Downloads the license specified")
    @Parameters({
            @Parameter(name="licenseFilename", description="The name of the file to download", in= ParameterIn.QUERY, example="super_cool_file.txt", schema=@Schema(implementation=String.class))
    })
    @GetMapping(path="/download")
    public ResponseEntity<?> downloadLicenseFile(@RequestParam(name="licenseFilename", required=false) String licenseFilename) {
        log.info("Begin Download Cloud License File");
        OperationStatusModel result = new OperationStatusModel("Download Cloud License File");

        if (HteTools.isNullorEmpty(licenseFilename)) {
            log.error("No license file name provided");
            result.setErrorDescription("No license file name provided");
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        String licensePath = configRepo.findById(AppConfig.licensePath)
                .orElse(new ConfigEntry("", "")).getCoValue();
        InputStreamResource resource;
        try {
            resource = new InputStreamResource(new FileInputStream(licensePath + "/" + licenseFilename));
        } catch (FileNotFoundException e) {
            log.error("An error occurred while reading the file: {}", e.getMessage());
            result.setErrorDescription("An error occurred while reading the file: " + e.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        log.info("Finish Download Cloud License file");
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);
    }
}