package com.hobart.hte.utils;

import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.util.List;

public interface ExcelFunctions<T> {

    default void autoSizeColumns(SXSSFSheet sheet, Integer numCols) {
        sheet.trackAllColumnsForAutoSizing();
        for (int i = 0; i <= numCols; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    default void createCell(Row row, Integer colCount, Object value, CellStyle style) {
        Cell cell = row.createCell(colCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    default void createCell(Row row, int columnCount, Object value) {
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
    }

     default SXSSFWorkbook beginDocument(Integer size, String sheetName, String[] headers) throws Exception {
        SXSSFWorkbook workbook = new SXSSFWorkbook(size);
        SXSSFSheet sheet = workbook.createSheet(sheetName);
        SXSSFRow header = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);

        for (int i = 0; i < headers.length; i++) {
            createCell(header, i, headers[i], style);
        }
        autoSizeColumns(sheet, headers.length-1);

        return workbook;
    }

    public ByteArrayInputStream buildExcelDocument(List<T> data);

    default int validateInt(Integer value) {
        return (value == null) ? 0 : value;
    }
}
