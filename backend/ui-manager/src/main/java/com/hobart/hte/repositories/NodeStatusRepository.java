package com.hobart.hte.repositories;

import com.hobart.hte.utils.access.HTeUser;
import com.hobart.hte.utils.tree.NodeStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface NodeStatusRepository extends CrudRepository<NodeStatus, String>{
    List<NodeStatus> findByParentId(String parentId);
    @Transactional
    @Query("DELETE FROM NodeStatus WHERE deviceId IN (:id)")
    void deleteByDeviceId(@Param("id") List<String> id);
}
