package com.hobart.hte.upgrading.fileChecking;

public abstract class UpgradeFileCheckStrategy implements Runnable {
    abstract public boolean verifyUpgrade();
    abstract public void run();
    abstract public int getUpgradeTime();
}
