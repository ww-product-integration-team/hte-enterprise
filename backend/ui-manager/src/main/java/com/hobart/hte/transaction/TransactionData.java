package com.hobart.hte.transaction;

import com.hobart.hte.utils.transaction.TransEntry;

import java.util.List;

public class TransactionData {
    public TransactionData(List<TransEntry> list, String start, String end, String span,
                           List<Integer> plus, List<String> scales, List<String> depts, List<String> stores) {
        this.list = list;
        this.start = start;
        this.end = end;
        this.span = span;
        this.scales = scales;
        this.plus = plus;
        this.numberOfTransactions = list.size();
        this.depts = depts;
        this.stores = stores;
        for(TransEntry entry : list){
            this.totalPrice += entry.getTrTtVal();
            this.totalWeight += entry.getTrFnlNtWt();
        }
    }

    List<TransEntry> list;
    long numberOfTransactions;
    long totalPrice;
    long totalWeight;
    String start;
    String end;
    String span;
    List<Integer> plus;
    List<String> scales;
    List<String> depts;
    List<String> stores;

    public List<TransEntry> getList() {
        return list;
    }

    public void setList(List<TransEntry> list) {
        this.list = list;
    }

    public long getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(long numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public long getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(long totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getSpan() {
        return span;
    }

    public void setSpan(String span) {
        this.span = span;
    }

    public List<Integer> getPlus() {
        return plus;
    }

    public void setPlus(List<Integer> plus) {
        this.plus = plus;
    }

    public List<String> getScales() {
        return scales;
    }

    public void setScales(List<String> scales) {
        this.scales = scales;
    }

    public List<String> getDepts() {
        return depts;
    }

    public void setDepts(List<String> depts) {
        this.depts = depts;
    }

    public List<String> getStores() {
        return stores;
    }

    public void setStores(List<String> stores) {
        this.stores = stores;
    }
}
