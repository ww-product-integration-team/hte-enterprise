package com.hobart.hte.assetList;

import com.hobart.hte.repositories.*;
import com.hobart.hte.utils.access.HTeDomain;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.tree.NodeStatus;
import com.hobart.hte.utils.tree.ScaleNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

public final class LazyLoadFilterAssetTree extends LazyLoadAssetTree {
    private static final Logger log = LogManager.getLogger(LazyLoadFilterAssetTree.class);

    private final FilterAPI filterAPI;
    private final String filterId;

    private final FilterRepository filterRepo;
    public LazyLoadFilterAssetTree(List<String> nodePath, String filterId, DomainRepository domainRepo,
                                   BannerRepository bannerRepo, RegionRepository regionRepo, StoreRepository storeRepo,
                                   DepartmentRepository deptRepo, DeviceRepository deviceRepo, ScaleSettingsRepository ssRepo,
                                   FileRepository fileRepo, UpgradeDeviceRepository upgradeDeviceRepo,
                                    NodeStatusRepository nodeStatusRepo, StoreDeptPairsRepository storeDeptPairsRepository,
                                   ConfigAppRepository configAppRepository, EntityManager entityManager, FilterRepository filterRepo ) {

        super(nodePath, domainRepo, bannerRepo, regionRepo, storeRepo, deptRepo, deviceRepo, ssRepo, fileRepo,
                upgradeDeviceRepo, nodeStatusRepo, storeDeptPairsRepository, configAppRepository);
        this.filterId = filterId;
        this.filterAPI = new FilterAPI(entityManager, filterRepo, deviceRepo);
        this.filterRepo = filterRepo;
    }


    protected Map<String, ScaleNode> getChildren(ScaleNode parent, String filterId){

        String awayThreshold = configAppRepository.findById(AppConfig.scaleAwayHours).get().getCoValue();
        String offlineThreshold = configAppRepository.findById(AppConfig.scaleOfflineHours).get().getCoValue();

        HashMap<String, ScaleNode> children = new HashMap<>();
        parent.updatePathWithFilter(filterId);
        switch (parent.getType()){
            case "banner":
                List<Region> regions = regionRepo.findByBannerId(parent.getId());
                for(Region region : regions) {
                    ScaleNode node = new ScaleNode(region);
                    if(filterAPI.nodeHasScales(filterId, node)) {
                        // fetch nodestatus w/ original nodeId
                        node.setNodeStatus(nodeStatusRepo.findById(node.getId()).get());
                        // set node id to be filter node
                        node.setId(filterId + "~" + node.getId());
                        // set Node path w/ new filter node id
                        List<String> newPath = new ArrayList<>(parent.getPath());
                        newPath.add(node.getId());
                        node.setPath(newPath);
                        node.setHasChildren(true); //proven by if Statement
                        //set filter node id
                        node.setFilterId(filterId);

                        children.put(node.getId(), node);
                    } else {
                        continue;
                    }
                }
                break;
            case "region":
                List<Store> stores = storeRepo.findByRegionId(parent.getId());
                for(Store store : stores) {
                    ScaleNode node = new ScaleNode(store);
                    if(filterAPI.nodeHasScales(filterId, node)) {
                        // fetch nodestatus w/ original nodeId
                        node.setNodeStatus(nodeStatusRepo.findById(node.getId()).get());
                        // set node id to be filter node
                        node.setId(filterId + "~" + node.getId());
                        // set Node path w/ new filter node id
                        List<String> newPath = new ArrayList<>(parent.getPath());
                        newPath.add(node.getId());
                        node.setPath(newPath);
                        node.setHasChildren(true); //proven by if Statement
                        //set filter node id
                        node.setFilterId(filterId);

                        children.put(node.getId(), node);
                    } else {
                        continue;
                    }
                }
                break;
            case "store":
                Iterable<String> deptIds = storeDeptPairsRepository.findDeptIdByStoreId(parent.getId());
                for(String deptId : deptIds){
                    Department dept = deptRepo.findById(deptId).orElse(null);
                    if(dept == null){
                        throw new NullPointerException("Department could not be found");
                    }
                    ScaleNode node = new ScaleNode(dept);
                    // dept node id = deptid + |  + store id
                    node.setId(parent.getId() + "|" + node.getId());
                    if(filterAPI.nodeHasScales(filterId, node)) {
                        // fetch nodestatus w/ original nodeId
                        node.setNodeStatus(nodeStatusRepo.findById(node.getId()).get());
                        // set node id to be filter node
                        node.setId(filterId + "~" + node.getId());
                        // set Node path w/ new filter node id
                        List<String> newPath = new ArrayList<>(parent.getPath());
                        newPath.add(node.getId());
                        node.setPath(newPath);
                        node.setHasChildren(true); //proven by if Statement
                        //set filter node id
                        node.setFilterId(filterId);

                        children.put(node.getId(), node);
                    } else {
                        continue;
                    }
                }
                break;
            case "dept":

                if(!parent.getId().contains("|")){
                    throw new RuntimeException("Dept node id is missing store id!");
                }
                String storeId = parent.getId().split("\\|")[0];
                String deptId = parent.getId().split("\\|")[1];
                Iterable<ScaleDevice> scales = deviceRepo.findByStoreIdAndDeptId(storeId, deptId);
                List<ScaleDevice> filteredScales = filterAPI.getFilteredScales(filterId);
                for(ScaleDevice scale : scales){
                    for(ScaleDevice filteredScale : filteredScales) {
                        if(filteredScale.equals(scale)) {
                            ScaleNode node = new ScaleNode(scale);
                            NodeStatus status = new NodeStatus(scale);
                            status.setScaleReportStatus(scale, awayThreshold, offlineThreshold);

                            ScaleSettings settings = ssRepo.findById(scale.getDeviceId()).orElse(null);
                            status.setScaleSync(scale, settings, getProfileFiles(scale.getProfileId()));
//                            status.setTimeSyncStatus(settings);
                            node.setNodeStatus(status);
                            //set node id to new filter node
                            node.setId(filterId + "~" + node.getId());
                            // set node path with new filter id
                            List<String> newPath = new ArrayList<>(parent.getPath());
                            newPath.add(node.getId());
                            node.setPath(newPath);
                            node.setFilterId(filterId);

                            children.put(node.getId(), node); // does not set batchIds and path

                        }
                    }
                }
                break;
            case "default":
                break;
        }
        return children;
    }
    /**
     *
     * @param nodeId = node id of parent node
     * @return the parent with the children of the node appended
     */
    protected ScaleNode packageNodeData(String nodeId,List<String> path, String filterId){
        // Returns the node with mapped children
        String tempNodeId = nodeId;
        if(tempNodeId.contains("|")){
            // this is a dept id and must be split to find node data
            tempNodeId = tempNodeId.split("\\|")[1];
        }
        HTeDomain parentDomain = domainRepo.findByDomainId(tempNodeId);
        if(parentDomain == null){
            log.info("Could not find parent domain");
            return null;
        }
        ScaleNode parent = findNodeData(parentDomain);
        if(parent == null){
            log.info("Could not find parent node");
            return null;
        }
        parent.setPath(path);
        parent.setId(nodeId); // set node id to original node id, for depts
        parent.updatePathWithFilter(filterId); //set filter path so children have correct path
        Map<String, ScaleNode> childrenMap = getChildren(parent, filterId);
        parent.setChildren(childrenMap);
        hasChildren(parent);
        parent.setFilterId(filterId);
        try {
            parent.setNodeStatus(nodeStatusRepo.findById(parent.getId()).get());
        } catch (Exception e){
            throw new NullPointerException("Could not find node status for parent node");
        }
        parent.setId(filterId + "~" + parent.getId());

        return parent;
    }
    public Map<String, ScaleNode> execute() {
        log.info("Begin fetching tree data");
        //temp code to split branch
        Map<String, ScaleNode> treeMap = new LinkedHashMap<>();
        if (nodePath != null && !nodePath.isEmpty()) {
            String nodeId = nodePath.get(nodePath.size()-1);
            ScaleNode node = packageNodeData(nodeId, nodePath, filterId);
            treeMap.put(node.getId(), node);
        } else { // no parent id provided so banners are requested
            Iterable<Banner> banners = bannerRepo.findAll();
            for (Banner banner : banners) {
                try{
                    ScaleNode node = new ScaleNode(banner);
                    if(filterAPI.nodeHasScales(filterId, node)) {
                        node.setChildren(new HashMap<>());
                        hasChildren(node);
                        node.setBatchIds(new ArrayList<>());
                        node.setNodeStatus(nodeStatusRepo.findById(node.getId()).get());
                        node.setFilterId(filterId);
                        node.setId(filterId + "~" + node.getId());
                        node.setPath(Arrays.asList(node.getId()));
                        treeMap.put(node.getId(), node);
                    }
                } catch(Exception e){
                    log.info("creating banner node - caught exception:"+ e.getMessage());
                    throw e;
                }
            }
        }

        Map<String, ScaleNode> sortedMap = treeMap.entrySet().stream().sorted((entry1, entry2) ->
                        entry1.getValue().getName().compareToIgnoreCase(entry2.getValue().getName()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        log.info("Finish fetching tree data");
        return sortedMap;
    }

    public final Map<String, ScaleNode> refreshFilterTree(List<String> expandedKeys) {
        log.info("Begin refreshing tree data");
        List<String> refreshedNode = new ArrayList<>();
        Map<String, ScaleNode> treeMap = new LinkedHashMap<>();
        // Find filters :
        List<String> filterIds = filterRepo.findDistinctFilterIds();

        for (String currfilterId : filterIds) {
            Iterable<Banner> banners = bannerRepo.findAll();

            for (Banner banner : banners) {
                try {
                    ScaleNode node = new ScaleNode(banner);
                    if (filterAPI.nodeHasScales(currfilterId, node)) {
                        node.setChildren(new HashMap<>());
                        hasChildren(node);
                        node.setBatchIds(new ArrayList<>());
                        node.setNodeStatus(nodeStatusRepo.findById(node.getId()).get());
                        node.setFilterId(currfilterId);
                        node.setId(currfilterId + "~" + node.getId());
                        node.setPath(Arrays.asList(node.getId()));
                        treeMap.put(node.getId(), node);
                    }
                } catch (Exception e) {
                    log.info("creating banner node - caught exception:" + e.getMessage());
                    throw e;
                }
            }
            // TODO itteracte through expanded keys and remove parent nodes because they will be created by their children.
            for (String key : expandedKeys) {
                Map<String, ScaleNode> currentBranch = null;
                String[] path = key.split("~");
                List<String> nodePath = new ArrayList<>();
                for (String nodeId : path) {
                    String filterNodeId = currfilterId + "~" + nodeId;
                    nodePath.add(filterNodeId);
                    if (!refreshedNode.contains(filterNodeId)) {
                        try {
                            ScaleNode newNode = packageNodeData(nodeId, nodePath, currfilterId);
                            if (!filterAPI.nodeHasScales(currfilterId, newNode)) {
                                break;
                            }
                            if (newNode == null) {
                                break;
                            }
                            newNode.setBatchIds(new ArrayList<>());
                            if (currentBranch == null) {
                                treeMap.put(newNode.getId(), newNode);
                            } else {
                                currentBranch.put(newNode.getId(), newNode);
                            }
                            refreshedNode.add(newNode.getId());
                        } catch (Exception e) {
                            log.info("creating node - caught exception:" + e.getMessage());
                            throw e;
                        }
                    }
                    if (currentBranch == null) {
                        currentBranch = treeMap.get(filterNodeId).getChildren();
                    } else {
                        currentBranch = currentBranch.get(filterNodeId).getChildren();
                    }
                }
            }
        }
        Map<String, ScaleNode> sortedMap = treeMap.entrySet().stream().sorted((entry1, entry2) ->
                        entry1.getValue().getName().compareToIgnoreCase(entry2.getValue().getName()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
                log.info("Finish fetching tree data");
        return sortedMap;
    }
}
