package com.hobart.hte.scale;

import com.hobart.hte.filemgr.FileController;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.model.ScaleStatusModel;
import com.hobart.hte.utils.socket.AsyncState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;

public class ScaleStatusDTO {
    private static final Logger log = LogManager.getLogger(ScaleStatusDTO.class);


    private String deviceId;
    private String ipAddress;
    private AsyncState asyncState;
    private int asyncProgress;

    //Scale Model
    private Boolean pingSuccess = false;

    private ScaleStatusModel statusModel;

    //ScaleDevice
    private Timestamp lastReportTimestamp;
    private String apphookVersion;

    private String message;


    public ScaleStatusDTO() {
    }

    public ScaleStatusDTO(AsyncState asyncState, Float asyncProgress) {
        this.asyncState = asyncState;
        this.asyncProgress = (int) (asyncProgress * 100);
    }

    public void importScaleStatus(ScaleStatusModel model) {
        if(model == null){
            log.error("ScaleStatusModel is null for IP: " + this.ipAddress);
            return;
        }
        this.pingSuccess = true;
        this.statusModel = model;
    }

    public void importScaleDevice(ScaleDevice device) {
        if(device == null){
            log.error("Device is null for IP: " + this.ipAddress);
            return;
        }
        this.lastReportTimestamp = device.getLastReportTimestampUtc();
        this.deviceId = device.getDeviceId();
    }

    public void importScaleSettings(ScaleSettings settings) {
        if(settings == null){
            log.error("ScaleSettings is null for IP: " + this.ipAddress);
            return;
        }

        this.apphookVersion = settings.getAppHookVersion();
    }

    public void setProgress(AsyncState asyncState, Float asyncProgress) {
        this.asyncState = asyncState;
        this.asyncProgress = (int) (asyncProgress * 100);
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public ScaleStatusModel getStatusModel() {
        return statusModel;
    }

    public void setStatusModel(ScaleStatusModel scaleStatusModel) {
        this.statusModel = scaleStatusModel;
    }

    public AsyncState getAsyncState() {
        return asyncState;
    }

    public void setAsyncState(AsyncState asyncState) {
        this.asyncState = asyncState;
    }

    public int getAsyncProgress() {
        return asyncProgress;
    }

    public void setAsyncProgress(int asyncProgress) {
        this.asyncProgress = asyncProgress;
    }

    public Boolean getPingSuccess() {
        return pingSuccess;
    }

    public void setPingSuccess(Boolean pingSuccess) {
        this.pingSuccess = pingSuccess;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Timestamp getLastReportTimestamp() {
        return lastReportTimestamp;
    }

    public void setLastReportTimestamp(Timestamp lastReportTimestamp) {
        this.lastReportTimestamp = lastReportTimestamp;
    }

    public String getApphookVersion() {
        return apphookVersion;
    }

    public void setApphookVersion(String apphookVersion) {
        this.apphookVersion = apphookVersion;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
