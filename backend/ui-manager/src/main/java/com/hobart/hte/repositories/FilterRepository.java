package com.hobart.hte.repositories;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.tree.Filter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


import javax.transaction.Transactional;
import java.util.List;

public interface FilterRepository extends CrudRepository<Filter, String> {

    List<Filter> findByFilterId(String id);

    @Transactional
    void deleteByFilterId(String filterId);
    boolean existsByFilterId(String filterId);
    @Query("SELECT distinct filterId FROM Filter")
    List<String> findDistinctFilterIds();

}