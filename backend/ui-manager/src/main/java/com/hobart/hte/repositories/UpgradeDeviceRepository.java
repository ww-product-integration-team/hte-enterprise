package com.hobart.hte.repositories;

import com.hobart.hte.utils.upgrade.UpgradeDevice;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UpgradeDeviceRepository extends CrudRepository<UpgradeDevice, String> {

    List<UpgradeDevice> findByBatchIdIsNotNull();

    UpgradeDevice findByDeviceId(String deviceId);

    List<UpgradeDevice> findAllByGroupId(String groupId);

    boolean existsByDeviceId(String deviceId);

    @Query(value = "SELECT count(deviceId)\n" +
            "FROM upgradeDevice\n" +
            "WHERE status IN \n" +
            "(\n" +
            "'awaiting_upload',\n" +
            "'awaiting_primary', \n" +
            "'upload_in_progress', \n" +
            "'upload_now', \n" +
            "'file_uploaded_awaiting_upgrade', \n" +
            "'upgrade_in_progress', \n" +
            "'upgrade_now',\n" +
            "'post_install_process'\n" +
            ");", nativeQuery = true)
    int numberOfActiveUpgrades();

    @Transactional
    void deleteByDeviceId(String deviceId);

    @Transactional
    void deleteByGroupId(String groupId);

    @Modifying
    @Transactional
    @Query(value="UPDATE upgradeDevice " +
            "SET status = :status, statusText = :message " +
            "WHERE deviceId = :deviceId", nativeQuery = true)
    int updateStatusByDeviceId(
            @Param("status") String status, @Param("message") String message, @Param("deviceId") String deviceId);
}
