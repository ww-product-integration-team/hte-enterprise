package com.hobart.hte.transaction;

import com.ctc.wstx.shaded.msv_core.grammar.NotNameClass;
import com.hobart.hte.helpers.DomainHelper;
import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.ItemRepository;
import com.hobart.hte.repositories.PricingZoneRepository;
import com.hobart.hte.repositories.TransRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.item.HobItem;
import com.hobart.hte.utils.item.PricingZone;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.transaction.TransEntry;
import com.hobart.hte.utils.upgrade.UpgradeBatch;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import com.hobart.hte.utils.upgrade.UpgradeGroup;
import com.hobart.hte.utils.util.HteTools;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/transservice")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class TransactionController {

    private static final Logger log = LogManager.getLogger(TransactionController.class);

    @Autowired
    TransRepository transRepo;
    @Autowired
    DeviceRepository deviceRepo;
    @Autowired
    ItemRepository itemRepo;
    @Autowired
    PricingZoneRepository zoneRepo;

    @PostMapping(path = "/uploadTransactions", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> fetchItemTransactions(@RequestBody ArrayList<TransEntry> transl,
                                                   HttpServletRequest request) {

        log.info("receiving transaction data...");
        log.info("client connecting is: {}", request.getRemoteAddr());

        try {
            transRepo.saveAll(transl);
            log.info("{} records saved successfully", transl.size());
        } catch (Exception ex) {
            log.info("error while saving transactions: {}", ex.toString());
            OperationStatusModel op = new OperationStatusModel("saving transactions", ex.toString());
            return new ResponseEntity<>(op, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @SuppressWarnings("unused")
    @Parameters({
            @Parameter(name = "scale_ips", description = "List of scale IP addresses to use to filter the transaction records. If no list is provided all scales will be returned. (Optional)", in = ParameterIn.QUERY, example = "10.3.128.65,10.3.128.128", schema = @Schema(implementation = String.class)),
            @Parameter(name = "plu_list", description = "List of the PLUs to use to filter the transaction records. If no list is provided all items will be returned. (Optional)", in = ParameterIn.QUERY, example = "4011,256,1297,6309", schema = @Schema(implementation = String.class)),
            @Parameter(name = "start", description = "Initial interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)),
            @Parameter(name = "end", description = "Final interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)) })
    @Operation(summary = "Export Excel report", description = "Generates a report of the transactions reported from scales based on the given parameters.")
    @GetMapping(path = "/excelExport")
    public ResponseEntity<InputStreamResource> exportToExcel(@RequestParam(required = false) List<String> scale_ips,
                                                             @RequestParam(required = false) List<Integer> plu_list,
                                                             @RequestParam(required = false) List<String> store_ids,
                                                             @RequestParam(required = false) List<String> dept_ids,
                                                             @RequestParam String start,
                                                             @RequestParam String end, HttpServletResponse response) {
        long timestamp = System.currentTimeMillis();
        log.debug("START OF REQUEST {}", timestamp);

        List<String> adjustedScaleList = getScaleIpList(scale_ips, store_ids, dept_ids);

        log.debug("scale ips: {}", adjustedScaleList.toString());

        log.debug("list of plus: {}", plu_list);
        log.debug("start timestamp: {} ", start);
        log.debug("end timestamp: {}", end);

        Timestamp _start = validateDate(start);
        Timestamp _end = validateDate(end);

        if (start == null || end == null || _end == null || _start == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<TransEntry> transactions = null;
        if (adjustedScaleList.isEmpty() && plu_list == null) { //No Scales or PLUs specified
            transactions = (List<TransEntry>) transRepo.findByTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(_start,
                    _end);
        } else if (adjustedScaleList.isEmpty() && plu_list != null) { //Only PLUs specified
            transactions = transRepo.findByTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(plu_list, _start,
                    _end);
        } else if (plu_list == null) { //Only Scales Specified
            transactions = transRepo.findByScaleIdInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(adjustedScaleList, _start,
                    _end);
        } else { //Both PLUs and Scales specified
            transactions = transRepo.findByScaleIdInAndTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(
                    adjustedScaleList, plu_list, _start, _end);
        }

        if (transactions == null) {
            transactions = new ArrayList<>();
        }

        TransactionDataExcelExport transactionDataExcel = new TransactionDataExcelExport();
        ByteArrayInputStream in = transactionDataExcel.buildExcelDocument(transactions);

        InputStreamResource file = new InputStreamResource(in);
        String filename = "TransData" + new SimpleDateFormat("-ddMMyy-hhmmss").format(new Date()) + ".xlsx";

        response.addHeader("Content-Length",  String.valueOf(in.available()));

        log.debug("Time to Compile/Export Excel File (ms): {}", System.currentTimeMillis() - timestamp);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename +"\"")
                .body(file);
    }

    @Parameters({
            @Parameter(name = "scale_ips", description = "List of scale IP addresses to use to filter the transaction records. If no list is provided all scales will be returned. (Optional)", in = ParameterIn.QUERY, example = "10.3.128.65,10.3.128.128", schema = @Schema(implementation = String.class)),
            @Parameter(name = "plu_list", description = "List of the PLUs to use to filter the transaction records. If no list is provided all items will be returned. (Optional)", in = ParameterIn.QUERY, example = "4011,256,1297,6309", schema = @Schema(implementation = String.class)),
            @Parameter(name = "start", description = "Initial interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)),
            @Parameter(name = "end", description = "Final interval date to filter transactions. Must match this format: MM-dd-yyyy", in = ParameterIn.QUERY, example = "08-01-2022", schema = @Schema(implementation = String.class)) })
    @Operation(summary = "Export Excel report", description = "Generates a report of the transactions reported from scales based on the given parameters.")
    @GetMapping(path = "/fetchTransactions", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<TransactionData> fetchTransData(@RequestParam(required = false) List<String> scale_ips,
                                                          @RequestParam(required = false) List<Integer> plu_list,
                                                          @RequestParam(required = false) List<String> store_ids,
                                                          @RequestParam(required = false) List<String> dept_ids,
                                                          @RequestParam String start,
                                                          @RequestParam String end, HttpServletResponse response) {
        long timestamp = System.currentTimeMillis();
        List<String> adjustedScaleList = getScaleIpList(scale_ips, store_ids, dept_ids);

        log.debug("scale ips: {}", adjustedScaleList.toString());

        log.debug("list of plus: {}", plu_list);
        log.debug("start timestamp: {} ", start);
        log.debug("end timestamp: {}", end);

        Timestamp _start = validateDate(start);
        Timestamp _end = validateDate(end);

        if (start == null || end == null || _end == null || _start == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        long diffInMillies = Math.abs(_end.getTime() - _start.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

        List<TransEntry> transactions = null;
        if (adjustedScaleList.isEmpty() && plu_list == null) { //No Scales or PLUs specified
            transactions = (List<TransEntry>) transRepo.findByTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(_start,
                    _end);
        } else if (adjustedScaleList.isEmpty() && plu_list != null) { //Only PLUs specified
            transactions = transRepo.findByTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(plu_list, _start,
                    _end);
        } else if (plu_list == null) { //Only Scales Specified
            transactions = transRepo.findByScaleIdInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(adjustedScaleList, _start,
                    _end);
        } else { //Both PLUs and Scales specified
            transactions = transRepo.findByScaleIdInAndTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(
                    adjustedScaleList, plu_list, _start, _end);
        }

        if (transactions == null) {
            transactions = new ArrayList<>();
        }

        return ResponseEntity.ok().body(new TransactionData(transactions, start, end, diff + " day(s)", plu_list,
                adjustedScaleList, dept_ids, store_ids));
    }

    private Timestamp validateDate(String dateString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
            Date parsedDate = dateFormat.parse(dateString);
            log.debug("parsed date: {}", parsedDate.toString());
            Timestamp timestamp = new Timestamp(parsedDate.getTime());
            log.debug("timestamp: {}", timestamp.toString());
            return timestamp;
        } catch (Exception e) {
            return null;
        }
    }

    private List<String> getScaleIpList(List<String> scale_ips, List<String> store_ids, List<String> dept_ids){
        List<String> adjustedScaleList = new ArrayList<>();
        if(scale_ips != null && !scale_ips.isEmpty()){
            adjustedScaleList.addAll(scale_ips);
        }

        //Next add scale IPs under any of the entities specified.
        if(store_ids != null && dept_ids != null ){
            adjustedScaleList.addAll(deviceRepo.findByStoreIdInAndDeptIdInAndHostnameNot(store_ids, dept_ids, ScaleDevice.getInvisibleScaleName())
                    .stream().map(ScaleDevice::getIpAddress).collect(Collectors.toList()));
        }
        else if(store_ids != null){
            adjustedScaleList.addAll(deviceRepo.findByStoreIdInAndHostnameNot(store_ids, ScaleDevice.getInvisibleScaleName())
                    .stream().map(ScaleDevice::getIpAddress).collect(Collectors.toList()));
        }
        else if (dept_ids != null){
            adjustedScaleList.addAll(deviceRepo.findByDeptIdInAndHostnameNot(dept_ids, ScaleDevice.getInvisibleScaleName())
                    .stream().map(ScaleDevice::getIpAddress).collect(Collectors.toList()));
        }

        return adjustedScaleList;
    }

    private static class TransactionData {
        public TransactionData(List<TransEntry> list, String start, String end, String span,
                               List<Integer> plus, List<String> scales, List<String> depts, List<String> stores) {
            this.list = list;
            this.start = start;
            this.end = end;
            this.span = span;
            this.scales = scales;
            this.plus = plus;
            this.numberOfTransactions = list.size();
            this.depts = depts;
            this.stores = stores;
            for(TransEntry entry : list){
                this.totalPrice += entry.getTrTtVal() == null ? 0 : entry.getTrTtVal();
                this.totalWeight += entry.getTrFnlNtWt() == null ? 0 : entry.getTrFnlNtWt();
                this.totalWeight += entry.getTrFxWt() == null ? 0 : entry.getTrFxWt();
            }
        }

        List<TransEntry> list;
        long numberOfTransactions;
        long totalPrice;
        long totalWeight;
        String start;
        String end;
        String span;
        List<Integer> plus;
        List<String> scales;
        List<String> depts;
        List<String> stores;

        public List<TransEntry> getList() {
            return list;
        }

        public void setList(List<TransEntry> list) {
            this.list = list;
        }

        public long getNumberOfTransactions() {
            return numberOfTransactions;
        }

        public void setNumberOfTransactions(long numberOfTransactions) {
            this.numberOfTransactions = numberOfTransactions;
        }

        public long getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(long totalPrice) {
            this.totalPrice = totalPrice;
        }

        public long getTotalWeight() {
            return totalWeight;
        }

        public void setTotalWeight(long totalWeight) {
            this.totalWeight = totalWeight;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public String getSpan() {
            return span;
        }

        public void setSpan(String span) {
            this.span = span;
        }

        public List<Integer> getPlus() {
            return plus;
        }

        public void setPlus(List<Integer> plus) {
            this.plus = plus;
        }

        public List<String> getScales() {
            return scales;
        }

        public void setScales(List<String> scales) {
            this.scales = scales;
        }

        public List<String> getDepts() {
            return depts;
        }

        public void setDepts(List<String> depts) {
            this.depts = depts;
        }

        public List<String> getStores() {
            return stores;
        }

        public void setStores(List<String> stores) {
            this.stores = stores;
        }
    }
    @Operation(summary="Get product table from the database", description="Retrieve product table from the database")
    @ApiResponses({
            @ApiResponse(responseCode="200", description="The product table was retrieved")
    })
    @GetMapping(path="/getProds", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getProds(){
        log.info("Getting Products");
        HashMap<String, HobItem> prodsMap = new HashMap<>();
        Iterable<HobItem> prods = itemRepo.findAll();
        Iterable<PricingZone> zones = zoneRepo.findAll();
        List<HobItem> newProds = new ArrayList<>();
        for(HobItem prod : prods){
            zones.forEach(zone -> {
                if (zone.getId().equals(prod.getZoneId())) {
                    prod.setZoneName(zone.getName());
                }
            });
            newProds.add(prod);
        }
        return new ResponseEntity<>(newProds, HttpStatus.OK);
    }
    @Operation(summary="Update a product", description="Updates an existing Product")
    @ApiResponses({
            @ApiResponse(responseCode="200", description="Successfully saved the Product"),
            @ApiResponse(responseCode="400", description="The ID of the provided Product is not valid"),
            @ApiResponse(responseCode="500", description="The database had an error saving the Product")
    })
    @PostMapping(path="/postProd", consumes={MediaType.APPLICATION_JSON_VALUE}, produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> postProd(@RequestBody HobItem prod) {
        log.info("Begin Post Prod");
        OperationStatusModel result = new OperationStatusModel("PostProd");
        log.info(prod);
        if(!itemRepo.existsByPrID(prod.getPrID())){
            log.info("Provided product ID does not exist!");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Provided product ID does not exist!");
            return new ResponseEntity<>(result, HttpStatus.CONFLICT);
        }
        try {
            itemRepo.save(prod);
        } catch (Exception e){
            log.info("Data base cannot save product");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Data base cannot save product");
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info("Data base cannot save product");
        result.setResult(RequestOperationResult.ERROR.name());
        result.setErrorDescription("Data base cannot save product");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);


//        return new ResponseEntity<>(result, HttpStatus.OK);

    }
}
