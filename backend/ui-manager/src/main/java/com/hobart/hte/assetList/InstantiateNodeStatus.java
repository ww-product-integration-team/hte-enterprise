package com.hobart.hte.assetList;

import com.hobart.hte.helpers.NodeHelper;
import com.hobart.hte.repositories.*;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.repositories.StoreDeptPairsRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InstantiateNodeStatus extends ControllerEndpoint<OperationStatusModel> {

    private static final Logger log = LogManager.getLogger(InstantiateNodeStatus.class);
    private final BannerRepository bannerRepo;
    private final RegionRepository regionRepo;
    private final StoreRepository storeRepo;
    private final DepartmentRepository deptRepo;
    private final DeviceRepository deviceRepo;
    private final ScaleSettingsRepository ssRepo;
    private final FileRepository fileRepo;
    private final NodeStatusRepository nodeStatusRepo;
    private final StoreDeptPairsRepository storeDeptPairsRepository;
    private final ConfigAppRepository configAppRepository;

    public InstantiateNodeStatus(BannerRepository bannerRepo,
                             RegionRepository regionRepo, StoreRepository storeRepo, DepartmentRepository deptRepo,
                             DeviceRepository deviceRepo, ScaleSettingsRepository ssRepo, FileRepository fileRepo,
                            NodeStatusRepository nodeStatusRepo, StoreDeptPairsRepository storeDeptPairsRepository,
                                 ConfigAppRepository configAppRepository) {

        this.bannerRepo = bannerRepo;
        this.regionRepo = regionRepo;
        this.storeRepo = storeRepo;
        this.deptRepo = deptRepo;
        this.deviceRepo = deviceRepo;
        this.ssRepo = ssRepo;
        this.fileRepo  = fileRepo;
        this.nodeStatusRepo = nodeStatusRepo;
        this.storeDeptPairsRepository = storeDeptPairsRepository;
        this.configAppRepository = configAppRepository;
    }
    public OperationStatusModel execute(){

        NodeHelper nodeHelper = new NodeHelper(bannerRepo, regionRepo, storeRepo,
                deptRepo, deviceRepo, ssRepo, fileRepo, nodeStatusRepo, storeDeptPairsRepository, configAppRepository);
        nodeHelper.instantiateNodeStatus();
        OperationStatusModel result = new OperationStatusModel("Updating Node Status");

        return result;
    }
}
