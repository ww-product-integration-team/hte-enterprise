package com.hobart.hte.upgrading;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.utils.ExcelFunctions;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class UpgradeDeviceExcelExport implements ExcelFunctions<UpgradeDevice> {

    private static final Logger log = LogManager.getLogger(UpgradeDeviceExcelExport.class);

    private static final String[] HEADERS = {"Upgrade ID", "Device IP", "Batch ID", "Application Version",
            "Enabled/Disabled", "Assigned Store", "Assigned Department", "Last Report"
    };

    @Autowired
    DeviceRepository deviceRepo;

    public ByteArrayInputStream buildExcelDocument(List<UpgradeDevice> upgradeDevices) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            SXSSFWorkbook workbook = beginDocument(5000, "Upgrade Devices", HEADERS);
            SXSSFSheet sheet = workbook.getSheet("Upgrade Devices");

            int rowNum = 1;
            for (UpgradeDevice device : upgradeDevices) {
                Row row = sheet.createRow(rowNum);
                ScaleDevice scale = deviceRepo.findById(device.getDeviceId()).get();

                createCell(row, 0, device.getUpgradeId());
                createCell(row, 1, scale.getIpAddress());
                createCell(row, 2, device.getBatchId());
                createCell(row, 3, device.getApplication());
                createCell(row, 4, scale.isEnabled());
                createCell(row, 5, scale.getStoreId());
                createCell(row, 6, scale.getDeptId());
//                createCell(row, 5, device.getBootloader());
//                createCell(row, 6, device.getStatus());
                createCell(row, 7, device.getLastUpdate());

                rowNum++;
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (Exception e) {
            log.error("Something went wrong compiling Excel file: " + e.getMessage());
        }

        return null;
    }
}
