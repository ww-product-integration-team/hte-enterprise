package com.hobart.hte.scale;

import com.hobart.hte.filemgr.FileController;
import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.ScaleSettingsRepository;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.model.ScaleStatusModel;
import com.hobart.hte.utils.socket.AsyncState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/socket")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class SocketController {

    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    DeviceRepository device_repo;

    @Autowired
    ScaleSettingsRepository settings_repo;

    private static final Logger log = LogManager.getLogger(FileController.class);

    private AsyncState asyncState = AsyncState.IDLE;
    private HashMap<String, CompletableFuture<?>> futures = new HashMap<>();

    private Float asyncProgress = 0f;

    private void updateAsyncProgress(Integer totalRecords) {
        /*
            Compares the total number of records with the length of the list of futures
            which is the number of records that still need to be processed.
        */
        asyncProgress = (float) (totalRecords - futures.size()) / totalRecords;
    }

    @PostMapping("/fleetStatus/cancel")
    public ResponseEntity<?> cancelFleetStatus() {
        OperationStatusModel status = new OperationStatusModel("Cancel Fleet Status Request");
        if(asyncState == AsyncState.CANCELLED || asyncState == AsyncState.IDLE){
            log.info("Async task is not currently running, returning");
            status.setErrorDescription("Async task is not currently running, returning");
            status.setResult(RequestOperationResult.ERROR.name());

            return new ResponseEntity<>(status, HttpStatus.CONFLICT);
        }

        for (CompletableFuture<?> future : futures.values()) {
            log.info("Cancelling future: {}", future);
            future.cancel(true);
        }
        futures.clear();

        asyncState = AsyncState.CANCELLED;

        ScaleStatusDTO cancelMsg = new ScaleStatusDTO(asyncState, 1f);
        template.convertAndSend("/topic/fleetStatus", cancelMsg);

        status.setResult(RequestOperationResult.SUCCESS.name());
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @PostMapping("/fleetStatus")
    public ResponseEntity<?> sendFleetStatus(@RequestBody List<String> scaleIpList) {
        OperationStatusModel status = new OperationStatusModel("Get Fleet Stats");

        if(asyncState == AsyncState.RUNNING){
            log.info("Async task is already running, returning");
            status.setErrorDescription("The Name fields cannot be null or empty.");
            status.setResult(RequestOperationResult.ERROR.name());

            return new ResponseEntity<>(status, HttpStatus.CONFLICT);
        }
        asyncState = AsyncState.RUNNING;
        asyncProgress = 0f;
        ScaleStatusDTO startMessage = new ScaleStatusDTO(asyncState, asyncProgress);
        template.convertAndSend("/topic/fleetStatus", startMessage);


        for (String scaleIp : scaleIpList) {
            log.info("Starting Future: {}", scaleIp);
            futures.put(scaleIp, CompletableFuture.runAsync(() -> {

                ScaleStatusDTO scaleStatusDTO = new ScaleStatusDTO();
                scaleStatusDTO.setIpAddress(scaleIp);
                scaleStatusDTO.importScaleDevice(device_repo.findByIpAddress(scaleIp));
                scaleStatusDTO.importScaleSettings(settings_repo.findByIpAddress(scaleIp));

                if(asyncState == AsyncState.CANCELLED){
                    log.info("Async task was cancelled, returning");
                    scaleStatusDTO.setProgress(asyncState, asyncProgress);
                    scaleStatusDTO.setMessage("Status request was cancelled");
                    template.convertAndSend("/topic/fleetStatus", scaleStatusDTO);
                    return;
                }
                HobartScaleCommDriver scale = new HobartScaleCommDriver(scaleIp);
                scale.setTimeout(5000);
                log.info("      Checking scale status: {}", scaleIp);
                try {
                    String scaleStatus = scale.getScaleStatus();
                    if (scaleStatus.startsWith("<error>")) {
                        scaleStatusDTO.setMessage(scaleStatus);
                        scaleStatusDTO.setProgress(asyncState, asyncProgress);
                        log.error("     An error occurred while attempting to connect to scale: " + scaleIp + ", " + scaleStatus);
                        updateAsyncProgress(scaleIpList.size());
                        template.convertAndSend("/topic/fleetStatus", scaleStatusDTO);
                        futures.remove(scaleIp);
                    } else {
                        log.info("      Scale Status successfully received!");
                        scaleStatusDTO.importScaleStatus(new ScaleStatusModel(scaleIp, scaleStatus));
                        scaleStatusDTO.setProgress(asyncState, asyncProgress);
                        updateAsyncProgress(scaleIpList.size());
                        template.convertAndSend("/topic/fleetStatus", scaleStatusDTO);
                        futures.remove(scaleIp);
                    }
                } catch (Exception e) {
                    log.error("     Error getting scale status: {}", e.getMessage());;
                    scaleStatusDTO.setMessage(e.getMessage());
                    scaleStatusDTO.setProgress(asyncState, asyncProgress);
                    updateAsyncProgress(scaleIpList.size());
                    template.convertAndSend("/topic/fleetStatus", scaleStatusDTO);
                    futures.remove(scaleIp);
                }
                log.info(futures.size() + " futures remaining");
            }));
        }

        futures.put("requestStatus", CompletableFuture.runAsync(() -> {
            try {
                while(futures.size() > 1){
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            asyncState = AsyncState.IDLE;
            futures.remove("requestStatus");
            log.info("All futures complete");

            ScaleStatusDTO finishedStatusDTO = new ScaleStatusDTO();
            finishedStatusDTO.setProgress(asyncState, 1f);
            template.convertAndSend("/topic/fleetStatus", finishedStatusDTO);
        }));

        status.setResult(RequestOperationResult.SUCCESS.name());
        status.setErrorDescription("Request completed successfully");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @MessageMapping("/sendMessage")
    public void receiveMessage(@Payload ScaleStatusDTO scaleStatusDTO) {
        // receive message from client
    }

    @SendTo("/topic/fleetStatus")
    public ScaleStatusModel broadcastMessage(@Payload ScaleStatusModel scaleStatusModel) {
        return scaleStatusModel;
    }

}
