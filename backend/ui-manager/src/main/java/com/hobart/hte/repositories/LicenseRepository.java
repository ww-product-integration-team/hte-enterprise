package com.hobart.hte.repositories;

import com.hobart.hte.utils.access.License;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LicenseRepository extends CrudRepository<License, String> {

	License findByLicenseId(String licenseUUID);

	@Query(value = "SELECT * FROM hteLicense WHERE addDate(activationDate, timeout) > NOW()", nativeQuery = true)
	List<License> findActiveLicenses();

	List<License> findByOwnerEmail(@Param(value="ownerEmail") String ownerEmail);
}
