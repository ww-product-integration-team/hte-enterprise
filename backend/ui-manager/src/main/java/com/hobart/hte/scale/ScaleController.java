package com.hobart.hte.scale;

import com.hobart.hte.utils.event.DeviceStatusEntry;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.ScaleStatusModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hobart.hte.repositories.DeviceStatusLogRepo;

import java.util.List;

@RestController
@RequestMapping("/scale")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class ScaleController {
	private static final Logger log = LogManager.getLogger(ScaleController.class);

	@Autowired
	DeviceStatusLogRepo devStatusLog_repo;

	@Operation(summary = "Retrieve the status on a scale", description = "Retrieve the status of the scale IP Address using port 6000. The default timeout is 30 seconds.")
	@Parameters({
			@Parameter(name = "ip", description = "IP address or hostname of the scale to retrieve the status information.", in = ParameterIn.QUERY, example = "10.3.128.65", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/getstatus", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> getScaleStatus(@RequestParam(name = "ip") String scaleIp) {
		log.debug("Checking status of scale: {}", scaleIp);
		HobartScaleCommDriver scale = new HobartScaleCommDriver(scaleIp);

		String tmp = scale.getScaleStatus();
		log.debug("message received from {}: {}", scaleIp, tmp);

		if (tmp.startsWith("<error>")) {
			OperationStatusModel result = new OperationStatusModel("ScaleStatusCheck");
			result.setResult("Error");
			result.setErrorDescription(
					"An error occurred while attempting to connect to scale: " + scaleIp + ", " + tmp);
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		} else {
			ScaleStatusModel scaleStatusModel = new ScaleStatusModel(scaleIp, tmp);
			log.debug("scale status: {}", scaleStatusModel.toString());
			return new ResponseEntity<>(scaleStatusModel, HttpStatus.OK);
		}
	}

	@Operation(summary = "Send message to have the scale execute a manual heartbeat", description = "Sends a message to the given scale IP Address to execute a manual heartbeat using port 6000. The default timeout is 30 seconds.")
	@Parameters({
			@Parameter(name = "ip", description = "IP address or hostname of the scale to command to execute a heartbeat.", in = ParameterIn.QUERY, example = "10.3.128.65", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/executemanualheartbeat", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> executeManualHeartbeat(@RequestParam(name = "ip") String scaleIp) {
		log.debug("Send message to scale to execute a heartbeat: {}", scaleIp);
		HobartScaleCommDriver scale = new HobartScaleCommDriver(scaleIp);

		String tmp = scale.executeHeartbeat();
		log.debug(tmp);

		OperationStatusModel result = new OperationStatusModel("ExecuteManualHeartbeat");
		if (tmp.startsWith("<error>")) {
			result.setResult("Error");
			result.setErrorDescription(
					"An error occurred while attempting to connect to scale: " + scaleIp + ", " + tmp);
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		} else {
			result.setResult("Connection established with the scale successfully, message delivered.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Send message to scale to reboot", description = "Sends a message to the given scale IP Address to execute reboot using port 6000. The default timeout is 30 seconds.")
	@Parameters({
			@Parameter(name = "ip", description = "IP address or hostname of the scale to send the command to reboot.",
					in = ParameterIn.QUERY, example = "10.3.128.65", schema = @Schema(implementation = String.class)) })

	@GetMapping(path = "/rebootdevice", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> rebootDevice(@RequestParam(name = "ip") String scaleIp) {
		log.debug("Sending message to reboot scale: {}", scaleIp);
		HobartScaleCommDriver scale = new HobartScaleCommDriver(scaleIp);

		String tmp = scale.rebootScale();
		log.debug("response received from scale: {}", tmp);

		OperationStatusModel result = new OperationStatusModel("RebootDevice");
		if (tmp.startsWith("<error>")) {
			result.setResult("Error");
			result.setErrorDescription(
					"An error occurred while attempting to connect to scale: " + scaleIp + ", " + tmp);
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		} else {
			result.setResult(
					"Connection established with the scale successfully, message delivered. The scale will reboot now.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Sends a message to scale to delete all data", description = "Sends a message to the given scale IP Address to delete all customer data using port 6000. The default timeout is 30 seconds.")
	@Parameters({
			@Parameter(name = "ip", description = "IP address or hostname of the scale to send the command to delete all.", in = ParameterIn.QUERY, example = "10.3.128.59", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/deletedevicedata", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> deleteAllDataInScale(@RequestParam(name = "ip") String scaleIp) {
		log.debug("Sending message to erase all scale data: {}", scaleIp);
		HobartScaleCommDriver scale = new HobartScaleCommDriver(scaleIp);

		String tmp = scale.deleteAllData();
		log.debug(tmp);

		OperationStatusModel result = new OperationStatusModel("EraseAllDeviceData");
		if (tmp.startsWith("<error>")) {
			result.setResult("Error");
			result.setErrorDescription(
					"An error occurred while attempting to connect to scale: " + scaleIp + ", " + tmp);
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		} else {
			result.setResult(
					"Connection established with the scale successfully, message delivered. All data will be erased.");
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Retrieves the last 24 status entries of the device", description = "Each entry means the scale executed a heartbeat successfully thus it was online, also indicates whether the scale was in sync or not.")
	@Parameters({
			@Parameter(name = "uuid", description = "uuid of the device .", in = ParameterIn.QUERY, example = "167ea0b5-6b05-11eb-998a-fa163e138193", schema = @Schema(implementation = String.class)) })
	@GetMapping(path = "/deviceStatusLog", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> retrieveDeviceStatusLog(@RequestParam(name = "uuid") String uuid) {
		try {
			List<DeviceStatusEntry> findTop24ByDeviceUuid = devStatusLog_repo.findTop24ByDeviceUuid(uuid);

			return new ResponseEntity<>(findTop24ByDeviceUuid, HttpStatus.OK);
		} catch (Exception ex) {
			OperationStatusModel result = new OperationStatusModel("retrieveDeviceStatusLog");
			result.setResult("Error");
			result.setErrorDescription("An error occurred while attempting to retrieve log for device: " + uuid);
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}