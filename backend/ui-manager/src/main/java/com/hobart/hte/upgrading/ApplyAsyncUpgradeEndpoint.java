package com.hobart.hte.upgrading;

import com.hobart.hte.repositories.DeviceRepository;
import com.hobart.hte.repositories.UpgradeDeviceRepository;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.upgrading.fileChecking.FileCheckFactory;
import com.hobart.hte.upgrading.fileChecking.UpgradeFileCheckStrategy;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.util.HteTools;
import com.jcraft.jsch.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ApplyAsyncUpgradeEndpoint extends ControllerEndpoint<OperationStatusModel> {

    private static final Logger log = LogManager.getLogger(UpgradeController.class);
    private final DeviceRepository deviceRepo;
    private final UpgradeDeviceRepository upgradeDeviceRepo;

    public ApplyAsyncUpgradeEndpoint(DeviceRepository deviceRepo, UpgradeDeviceRepository upgradeDeviceRepo) {
        this.deviceRepo = deviceRepo;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
    }

    public Map<String, Boolean> execute(List<String> ipAddresses, String fileName, boolean deleteFilesAfterUpgrade) {

        OperationStatusModel response = new OperationStatusModel();
        response.setOperation("Apply Async Upgrade Files");

        /* Preliminary check to see whether the file is already present.

            You may have noticed that UploadAsyncUpgradeEndpoint does this too; this isn't blind code re-use.
            Under normal circumstances the file will always persist in the upgrade directory, but it won't
            survive an SD card replacement, which isn't all too unlikely in the field.

            Therefore, *just in case*, we will re-query the scales to see if the file is still there.
            If it's gone, we'll try to push the file again to lessen the headache for the user.
         */
        Map<String, Boolean> fileStatusMap = UpgradeHelper.checkFileStatuses(ipAddresses, fileName,
                true, false, this.deviceRepo, this.upgradeDeviceRepo);
        for (Map.Entry<String, Boolean> data : fileStatusMap.entrySet()) {
            String ipAddress = data.getKey();
            Boolean fileWasUploaded = data.getValue();

            if (fileWasUploaded) {
                Session sshSession = HteTools.getSSHSession(ipAddress);
                if (sshSession != null && sshSession.isConnected()) {

                    // I know there's already a method to do this in HteTools, but I'd rather use this session to get it
                    // After all, we're already in the scale -- why make another SSH connection?
                    // And yes, I know the method says "executeSSHCommand", but it uses the sshInstance session.
                    String scaleOperatingSystemInfo = HteTools.executeSSHCommandGetOutput(
                            sshSession,"cd /etc ; cat os-version");
                    boolean isHTScale = scaleOperatingSystemInfo.contains("Timesys");

                    String destinationUpgradeFilePath = isHTScale ?
                            "/usr/local/hobart/upgrade/" + fileName : "/opt/hobart/upgrade/" + fileName;
                    String upgradeCommand = isHTScale ?
                            "python /usr/local/hobart/python/installUpgrade.py " + destinationUpgradeFilePath :
                            "python3 /usr/bin/installUpgrade.py " + destinationUpgradeFilePath;

                    boolean commandExecuted = HteTools.executeSSHCommand(sshSession, upgradeCommand);
                    if (!commandExecuted) { // This should be very rare, but it's worth capturing
                        data.setValue(false);
                        continue;
                    }
                    FileCheckFactory fileChecker = new FileCheckFactory();
                    UpgradeFileCheckStrategy fileCheckLogic =
                            fileChecker.determineFileCheckLogic(
                                    ipAddress, fileName, deleteFilesAfterUpgrade, deviceRepo, upgradeDeviceRepo);

                    ScheduledExecutorService upgradeCheckInThread = Executors.newSingleThreadScheduledExecutor();
                    upgradeCheckInThread.schedule(fileCheckLogic, fileCheckLogic.getUpgradeTime(), TimeUnit.MINUTES);
                }
                HteTools.disconnectSSH(sshSession);
            }
        }
        return fileStatusMap;
    }
}
