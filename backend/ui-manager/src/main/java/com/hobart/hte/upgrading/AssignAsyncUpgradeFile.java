package com.hobart.hte.upgrading;

import com.hobart.hte.repositories.UpgradeDeviceRepository;
import com.hobart.hte.ui.ControllerEndpoint;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.upgrade.UpgradeDevice;
import com.hobart.hte.utils.util.HteTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.util.List;

public class AssignAsyncUpgradeFile extends ControllerEndpoint<OperationStatusModel> {

    private static final Logger log = LogManager.getLogger(AssignAsyncUpgradeFile.class);
    private final List<UpgradeDevice> devices;
    private final OperationStatusModel result;
    private final UpgradeDeviceRepository upgradeDeviceRepo;

    public HttpStatus resultStatus;

    public AssignAsyncUpgradeFile(List<UpgradeDevice> devices, OperationStatusModel result,
                                  UpgradeDeviceRepository upgradeDeviceRepo) {
        this.devices = devices;
        this.result = result;
        this.upgradeDeviceRepo = upgradeDeviceRepo;
    }

    public void execute() {
        if (devices == null || devices.isEmpty()) {
            log.error("The provided list of devices is null or empty");
            result.setErrorDescription("The provided list of devices is null or empty");
            result.setResult(RequestOperationResult.ERROR.name());
            resultStatus = HttpStatus.BAD_REQUEST;
            return;
        }

        try {
            upgradeDeviceRepo.saveAll(devices);
        } catch (Exception ex) {
            log.error("An error occurred while saving upgrade devices with a new assigned async upgrade file");
            result.setErrorDescription("An error occurred while saving upgrade devices with a new assigned async upgrade file");
            result.setResult(RequestOperationResult.ERROR.name());
            resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
