-- MySQL dump 10.13  Distrib 8.0.36, for Linux (x86_64)
--
-- Host: localhost    Database: hte
-- ------------------------------------------------------
-- Server version	8.0.36-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Prods`
--

DROP TABLE IF EXISTS `Prods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Prods` (
  `prID` int NOT NULL,
  `CaNum` int DEFAULT NULL,
  `DLXNum` int DEFAULT NULL,
  `LCNum` int DEFAULT NULL,
  `LLID` int DEFAULT NULL,
  `LPNum` int DEFAULT NULL,
  `LXNum` int DEFAULT NULL,
  `LbNum1` int DEFAULT NULL,
  `LbNum2` int DEFAULT NULL,
  `LbNum3` int DEFAULT NULL,
  `MaNum` int DEFAULT NULL,
  `NFNum` int DEFAULT NULL,
  `NtNum` int DEFAULT NULL,
  `PrBarPref` int DEFAULT NULL,
  `PrBarProN` int DEFAULT NULL,
  `PrBarVenN` int DEFAULT NULL,
  `PrBnsPts` int DEFAULT NULL,
  `PrByCnt` int DEFAULT NULL,
  `PrCOLReq` bit(1) DEFAULT NULL,
  `PrCOLTN` varchar(255) DEFAULT NULL,
  `PrCdNm1` varchar(255) DEFAULT NULL,
  `PrCdNm2` varchar(255) DEFAULT NULL,
  `PrCutTestMeatSpecSheetGroupNum` int DEFAULT NULL,
  `PrDesc` varchar(255) DEFAULT NULL,
  `PrDscMe` int DEFAULT NULL,
  `PrDscPr1` int DEFAULT NULL,
  `PrDscPr2` int DEFAULT NULL,
  `PrDscPr3` int DEFAULT NULL,
  `PrEANCd` varchar(255) DEFAULT NULL,
  `PrExcPr` int DEFAULT NULL,
  `PrGrNm1` varchar(255) DEFAULT NULL,
  `PrGrNm2` varchar(255) DEFAULT NULL,
  `PrGrNm3` varchar(255) DEFAULT NULL,
  `PrGrNm4` varchar(255) DEFAULT NULL,
  `PrImage` varchar(255) DEFAULT NULL,
  `PrInMode` int DEFAULT NULL,
  `PrIsFrcBC` bit(1) DEFAULT NULL,
  `PrIsFrcPr` bit(1) DEFAULT NULL,
  `PrIsFrcTr` bit(1) DEFAULT NULL,
  `PrIsPrnPL` bit(1) DEFAULT NULL,
  `PrIsPrnPO` bit(1) DEFAULT NULL,
  `PrIsPrnSL` bit(1) DEFAULT NULL,
  `PrIsUsSL` bit(1) DEFAULT NULL,
  `PrLblPlc1` int DEFAULT NULL,
  `PrLblPlc2` int DEFAULT NULL,
  `PrLblPlc3` int DEFAULT NULL,
  `PrLblRot1` int DEFAULT NULL,
  `PrLblRot2` int DEFAULT NULL,
  `PrLblRot3` int DEFAULT NULL,
  `PrManualLabelTypeNum1` int DEFAULT NULL,
  `PrManualLabelTypeNum2` int DEFAULT NULL,
  `PrManualLabelTypeNum3` int DEFAULT NULL,
  `PrMealNumSidesIncluded` int DEFAULT NULL,
  `PrMealSideItemsAreFree` bit(1) DEFAULT NULL,
  `PrMealSideItemsGroupNum` int DEFAULT NULL,
  `PrName1` varchar(255) DEFAULT NULL,
  `PrName2` varchar(255) DEFAULT NULL,
  `PrNetWt` int DEFAULT NULL,
  `PrOnlineOrderingLabelTypeNum1` int DEFAULT NULL,
  `PrOnlineOrderingLabelTypeNum2` int DEFAULT NULL,
  `PrOnlineOrderingLabelTypeNum3` int DEFAULT NULL,
  `PrPick5LabelTypeNum1` int DEFAULT NULL,
  `PrPick5LabelTypeNum2` int DEFAULT NULL,
  `PrPick5LabelTypeNum3` int DEFAULT NULL,
  `PrPrMod` int DEFAULT NULL,
  `PrPrPlT` int DEFAULT NULL,
  `PrPrepackLabelTypeNum1` int DEFAULT NULL,
  `PrPrepackLabelTypeNum2` int DEFAULT NULL,
  `PrPrepackLabelTypeNum3` int DEFAULT NULL,
  `PrPrice` int DEFAULT NULL,
  `PrProdEntryLabelTypeNum1` int DEFAULT NULL,
  `PrProdEntryLabelTypeNum2` int DEFAULT NULL,
  `PrProdEntryLabelTypeNum3` int DEFAULT NULL,
  `PrProdLfD` int DEFAULT NULL,
  `PrProdLfH` int DEFAULT NULL,
  `PrProdLfM` int DEFAULT NULL,
  `PrPrpTare` int DEFAULT NULL,
  `PrPrtns` int DEFAULT NULL,
  `PrRawBarC` varchar(255) DEFAULT NULL,
  `PrSelfServLabelTypeNum1` int DEFAULT NULL,
  `PrSelfServLabelTypeNum2` int DEFAULT NULL,
  `PrSelfServLabelTypeNum3` int DEFAULT NULL,
  `PrShlfLfD` int DEFAULT NULL,
  `PrShlfLfH` int DEFAULT NULL,
  `PrShlfLfM` int DEFAULT NULL,
  `PrSpcSD` datetime(6) DEFAULT NULL,
  `PrTare` int DEFAULT NULL,
  `PrTareB` int DEFAULT NULL,
  `PrTotalPriceMax` int DEFAULT NULL,
  `PrTotalPriceMin` int DEFAULT NULL,
  `PrType` int DEFAULT NULL,
  `PrVCOLTxt` bit(1) DEFAULT NULL,
  `PrWeightMax` int DEFAULT NULL,
  `PrWeightMin` int DEFAULT NULL,
  `ProdTxt5Num` int DEFAULT NULL,
  `ProdTxt6Num` int DEFAULT NULL,
  `ProdTxt7Num` int DEFAULT NULL,
  `ProdTxt8Num` int DEFAULT NULL,
  `ProdTxt9Num` int DEFAULT NULL,
  `SMNum` int DEFAULT NULL,
  `SPNum` int DEFAULT NULL,
  `TxNum` int DEFAULT NULL,
  `ClNum` int DEFAULT NULL,
  `DeptNum` int DEFAULT NULL,
  `PrNum` int DEFAULT NULL,
  PRIMARY KEY (`prID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Prods`
--

LOCK TABLES `Prods` WRITE;
/*!40000 ALTER TABLE `Prods` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductTexts`
--

DROP TABLE IF EXISTS `ProductTexts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ProductTexts` (
  `deptNum` int NOT NULL,
  `prodTextNum` int NOT NULL,
  `prodTextType` int NOT NULL,
  `ProdTextDesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deptNum`,`prodTextNum`,`prodTextType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductTexts`
--

LOCK TABLES `ProductTexts` WRITE;
/*!40000 ALTER TABLE `ProductTexts` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProductTexts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Trans`
--

DROP TABLE IF EXISTS `Trans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Trans` (
  `TransID` int NOT NULL AUTO_INCREMENT,
  `TTNum` int DEFAULT NULL,
  `TrAPrdt` int DEFAULT NULL,
  `TrBarNum` varchar(255) DEFAULT NULL,
  `TrBarPref` int DEFAULT NULL,
  `TrCOOLF` bit(1) DEFAULT NULL,
  `TrCOTrTxt` varchar(255) DEFAULT NULL,
  `TrCOTxt` varchar(255) DEFAULT NULL,
  `TrClNum` int DEFAULT NULL,
  `TrExPr` int DEFAULT NULL,
  `TrFnlPr` int DEFAULT NULL,
  `TrFnlTare` int DEFAULT NULL,
  `TrGrp` int DEFAULT NULL,
  `TrID` int NOT NULL,
  `TrModeChange` int DEFAULT NULL,
  `TrNmPck` int DEFAULT NULL,
  `TrOBarDig` int DEFAULT NULL,
  `TrOByCount` int DEFAULT NULL,
  `TrOFnPr` int DEFAULT NULL,
  `TrOFxWt` int DEFAULT NULL,
  `TrOPrLfD` int DEFAULT NULL,
  `TrOPrLfH` int DEFAULT NULL,
  `TrOPrLfM` int DEFAULT NULL,
  `TrOSfLfD` int DEFAULT NULL,
  `TrOSfLfH` int DEFAULT NULL,
  `TrOSfLfM` int DEFAULT NULL,
  `TrOTare` int DEFAULT NULL,
  `TrOUnPr` int DEFAULT NULL,
  `TrOpChg` int DEFAULT NULL,
  `TrOpName` varchar(255) DEFAULT NULL,
  `TrPrLfD` int DEFAULT NULL,
  `TrPrLfH` int DEFAULT NULL,
  `TrPrLfM` int DEFAULT NULL,
  `TrPrMod` int DEFAULT NULL,
  `TrPrPlT` int DEFAULT NULL,
  `TrRdClr` int DEFAULT NULL,
  `TrScID` int NOT NULL,
  `TrSecLb` bit(1) DEFAULT NULL,
  `TrSfLfD` int DEFAULT NULL,
  `TrSfLfH` int DEFAULT NULL,
  `TrSfLfM` int DEFAULT NULL,
  `TrSweetheartWeight` int DEFAULT NULL,
  `TrUTNum` int DEFAULT NULL,
  `TrUTPercentOfPrimalCost` int DEFAULT NULL,
  `TrUTPercentOfPrimalWeight` int DEFAULT NULL,
  `TrUnPr` int DEFAULT NULL,
  `TrVoid` int DEFAULT NULL,
  `deptNum` int DEFAULT NULL,
  `scaleId` varchar(255) DEFAULT NULL,
  `trByCount` int DEFAULT NULL,
  `trDtTm` datetime(6) DEFAULT NULL,
  `trFnlNtWt` int DEFAULT NULL,
  `trFxWt` int DEFAULT NULL,
  `trGrNm1` varchar(255) DEFAULT NULL,
  `trGrNm2` varchar(255) DEFAULT NULL,
  `trGrNm3` varchar(255) DEFAULT NULL,
  `trGrNm4` varchar(255) DEFAULT NULL,
  `trLab1` int DEFAULT NULL,
  `trLab2` int DEFAULT NULL,
  `trLab3` int DEFAULT NULL,
  `trLab4` int DEFAULT NULL,
  `trOGrNm1` varchar(255) DEFAULT NULL,
  `trOGrNm2` varchar(255) DEFAULT NULL,
  `trOGrNm3` varchar(255) DEFAULT NULL,
  `trOGrNm4` varchar(255) DEFAULT NULL,
  `trOLab1` int DEFAULT NULL,
  `trOLab2` int DEFAULT NULL,
  `trOLab3` int DEFAULT NULL,
  `trOLab4` int DEFAULT NULL,
  `trOTtVl` int DEFAULT NULL,
  `trOpNum` varchar(255) DEFAULT NULL,
  `trOpRFID` varchar(255) DEFAULT NULL,
  `trPrDes` varchar(255) DEFAULT NULL,
  `trPrNum` int DEFAULT NULL,
  `trPrTare` int DEFAULT NULL,
  `trPrTyp` int DEFAULT NULL,
  `trTare` int DEFAULT NULL,
  `trTtVal` int DEFAULT NULL,
  PRIMARY KEY (`TransID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Trans`
--

LOCK TABLES `Trans` WRITE;
/*!40000 ALTER TABLE `Trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `Trans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appConfigs`
--

DROP TABLE IF EXISTS `appConfigs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appConfigs` (
  `CoName` varchar(255) NOT NULL,
  `CoValue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CoName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appConfigs`
--

LOCK TABLES `appConfigs` WRITE;
/*!40000 ALTER TABLE `appConfigs` DISABLE KEYS */;
INSERT INTO `appConfigs` VALUES ('customStoreName',NULL),('databaseId','12341234-1234-1234-1234-123412341234'),('defaultPort','8080'),('donePath','/home/hobart/hte_done'),('fallbackIp','10.3.128.150'),('heartbeatOperation','HQ'),('httpMethod','http'),('httpRequestCheckScaleService','http://localhost:8080/scale-service/heartbeat/test'),('hybridMultipath','false'),('importPath','/home/hobart/hte_import'),('licensePath','/home/hobart/license'),('logRootPath','/home/hobart/log'),('repositoryPath','/home/hobart/repository'),('scaleAwayHours','12'),('scaleDeleteDay','1'),('scaleDeleteInterval','Daily'),('scaleDeleteItems',''),('scaleDeleteTime','3'),('scaleOfflineHours','24'),('sendStoreInfo','true');
/*!40000 ALTER TABLE `appConfigs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autoAssignmentRules`
--

DROP TABLE IF EXISTS `autoAssignmentRules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autoAssignmentRules` (
  `ipAddressStart` varchar(15) NOT NULL,
  `ipAddressEnd` varchar(15) NOT NULL,
  `banner` varchar(150) DEFAULT NULL,
  `region` varchar(150) DEFAULT NULL,
  `store` varchar(150) DEFAULT NULL,
  `dept` varchar(150) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ipAddressStart`,`ipAddressEnd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autoAssignmentRules`
--

LOCK TABLES `autoAssignmentRules` WRITE;
/*!40000 ALTER TABLE `autoAssignmentRules` DISABLE KEYS */;
INSERT INTO `autoAssignmentRules` VALUES ('10.3.128.1','10.3.128.255','hb root','hb region','hb store','test',1);
/*!40000 ALTER TABLE `autoAssignmentRules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banner` (
  `bannerId` varchar(36) NOT NULL,
  `bannerName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bannerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES ('a996e6f5-9353-4745-b016-c7d275b50f23','root'),('da3d435f-d99b-4a74-a5e4-22a9236a04fa','hb root');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checksumLogs`
--

DROP TABLE IF EXISTS `checksumLogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checksumLogs` (
  `date` datetime(6) NOT NULL,
  `scaleIp` varchar(15) NOT NULL,
  `logEntry` varchar(500) NOT NULL,
  PRIMARY KEY (`date`,`scaleIp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checksumLogs`
--

LOCK TABLES `checksumLogs` WRITE;
/*!40000 ALTER TABLE `checksumLogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `checksumLogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configEvent`
--

DROP TABLE IF EXISTS `configEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `configEvent` (
  `idfileEvent` varchar(255) NOT NULL,
  `creationTimestamp` datetime(6) DEFAULT NULL,
  `deptId` varchar(255) DEFAULT NULL,
  `deviceUuid` varchar(255) DEFAULT NULL,
  `eventType` int NOT NULL,
  `finishOn` datetime(6) DEFAULT NULL,
  `shortDesc` varchar(255) DEFAULT NULL,
  `startOn` datetime(6) DEFAULT NULL,
  `storeId` varchar(255) DEFAULT NULL,
  `uuidFile` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idfileEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configEvent`
--

LOCK TABLES `configEvent` WRITE;
/*!40000 ALTER TABLE `configEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `configEvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `depts`
--

DROP TABLE IF EXISTS `depts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `depts` (
  `deptId` varchar(36) NOT NULL,
  `defaultProfileId` varchar(255) DEFAULT NULL,
  `deptName1` varchar(255) DEFAULT NULL,
  `deptName2` varchar(255) DEFAULT NULL,
  `requiresDomainId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`deptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `depts`
--

LOCK TABLES `depts` WRITE;
/*!40000 ALTER TABLE `depts` DISABLE KEYS */;
INSERT INTO `depts` VALUES ('89767c2d-da28-4a9d-9245-a812f6da8391',NULL,'test',NULL,'87588758-8758-8758-8758-875887588758'),('b89aaa06-f050-4591-a6d9-1383e591ac04','00000000-0000-0000-0000-000000000000','Dept1',NULL,'11111111-1111-1111-1111-111111111111');
/*!40000 ALTER TABLE `depts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deviceStatusLog`
--

DROP TABLE IF EXISTS `deviceStatusLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `deviceStatusLog` (
  `timestamp` timestamp(3) NOT NULL,
  `deleteEvent` bit(1) NOT NULL,
  `deviceUuid` varchar(255) DEFAULT NULL,
  `event1` bit(1) NOT NULL,
  `event2` bit(1) NOT NULL,
  `event3` bit(1) NOT NULL,
  `event4` bit(1) NOT NULL,
  `inSync` bit(1) NOT NULL,
  `logStatus` int DEFAULT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deviceStatusLog`
--

LOCK TABLES `deviceStatusLog` WRITE;
/*!40000 ALTER TABLE `deviceStatusLog` DISABLE KEYS */;
INSERT INTO `deviceStatusLog` VALUES ('2024-03-11 23:53:36.141',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 00:06:10.939',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 00:23:49.584',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 00:27:11.315',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 00:38:40.609',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 01:43:49.431',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 01:53:50.470',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 02:00:08.767',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 03:00:08.742',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 04:00:08.759',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 05:00:09.679',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 05:19:17.229',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 06:00:09.935',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 07:00:09.969',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 07:19:35.046',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 08:00:09.390',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 09:00:11.049',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 10:00:10.405',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 19:43:50.049',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-12 19:47:05.914',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 19:53:04.003',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 19:54:40.804',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 19:59:05.029',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:00:59.602',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:02:29.834',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:05:30.254',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:13:38.750',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:22:29.361',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:23:08.802',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:24:59.472',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:26:12.085',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:27:43.159',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:28:07.366',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:30:18.118',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:30:44.710',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:32:09.978',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:32:36.682',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:34:22.554',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 20:34:48.579',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 21:06:16.271',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 21:27:47.187',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 22:06:17.057',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-12 23:06:19.154',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 00:06:24.261',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 00:47:01.086',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 00:58:56.256',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 01:00:11.051',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 01:06:17.495',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 02:00:11.095',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 02:06:23.158',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 03:00:12.077',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 03:06:26.347',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 04:00:11.365',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 04:06:26.173',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 05:00:10.888',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 05:06:26.459',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 06:00:12.682',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 06:06:25.285',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 07:00:11.743',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 07:06:19.401',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 08:00:11.572',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 08:06:26.510',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 09:00:11.818',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 09:06:33.986',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 10:00:11.590',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 10:06:29.772',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 11:00:12.972',_binary '','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 11:06:33.059',_binary '','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 12:00:14.150',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 12:06:30.000',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 13:00:14.148',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 13:06:22.678',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 14:00:14.257',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 14:06:29.894',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 15:00:12.332',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 15:06:36.771',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 16:00:13.970',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 16:06:35.913',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 17:00:16.071',_binary '\0','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-13 17:06:24.902',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 19:19:32.915',_binary '\0','',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 19:20:46.213',_binary '\0','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 19:21:31.065',_binary '\0','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 19:38:13.460',_binary '\0','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 19:38:55.197',_binary '\0','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 19:42:47.657',_binary '\0','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 20:00:16.670',_binary '\0','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 20:02:52.748',_binary '\0','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-13 20:06:34.181',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 01:23:23.429',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 02:06:36.487',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 03:06:36.876',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 04:06:29.614',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 05:06:37.709',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 06:06:42.438',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 07:06:40.400',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 08:06:33.341',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 09:06:41.371',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 10:06:40.817',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 11:06:48.507',_binary '','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 12:06:41.253',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 13:06:46.684',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 14:06:40.136',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 15:06:45.895',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 16:06:41.524',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 17:06:37.594',_binary '\0','dd5747c3-53e2-4fe2-b715-cac4c146dff8',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 17:22:18.205',_binary '\0','',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-14 17:26:51.508',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 18:00:16.289',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 18:02:52.859',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-14 18:42:53.572',_binary '\0','',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-14 18:49:05.182',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 18:51:14.772',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 18:53:39.289',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 19:00:16.527',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 19:01:11.073',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 19:12:50.896',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:01:11.037',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:05:12.319',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:05:59.840',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:06:35.679',_binary '\0','',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:11:09.923',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:11:26.975',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:13:54.709',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:14:11.359',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:15:44.806',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:23:20.170',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:31:49.075',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:35:10.215',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:39:45.596',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:42:26.057',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:46:48.245',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:46:59.833',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:47:35.847',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:48:01.518',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:53:42.462',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 20:58:28.943',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 21:01:10.664',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 21:34:26.243',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 21:34:59.718',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:04:21.414',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:05:40.888',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:06:25.896',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:08:56.048',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:25:59.364',_binary '\0','',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-14 22:39:58.103',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:40:16.783',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:46:39.815',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:47:03.886',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:47:45.311',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:49:04.499',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 22:51:14.490',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:00:19.774',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:01:12.741',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:04:58.972',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:09:44.362',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:15:51.635',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:16:18.507',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:20:26.567',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:28:20.468',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:30:17.424',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:45:34.450',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:46:22.092',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:46:58.756',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:52:45.659',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:53:34.683',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:53:56.325',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:54:23.399',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:54:43.432',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-14 23:57:20.020',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:02:17.920',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:02:34.174',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:27:50.849',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:28:18.657',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:33:58.273',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:46:51.739',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:47:40.753',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:48:03.650',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:49:25.981',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 00:50:11.287',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:02:20.147',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:18:32.388',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:18:57.650',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:23:06.232',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:30:59.793',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:31:43.088',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:35:45.648',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:51:02.048',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 01:52:11.123',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 02:02:23.909',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 02:02:39.182',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 03:02:25.105',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 03:02:39.463',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 04:02:24.357',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 04:02:38.996',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 05:02:24.208',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 05:02:38.835',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 06:02:24.361',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 06:02:37.774',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 07:02:26.086',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 07:02:38.339',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 08:02:25.412',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 08:02:38.589',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 09:02:25.189',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 09:02:38.430',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 10:02:24.890',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 10:02:38.896',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 11:02:25.231',_binary '','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 11:02:39.653',_binary '','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 12:02:25.687',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 12:02:40.971',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 13:02:25.087',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 13:02:40.884',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 14:02:24.998',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 14:02:39.343',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 15:02:25.470',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 15:02:40.246',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 16:02:25.419',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 16:02:40.216',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:02:24.815',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:02:41.043',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:14:47.504',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:15:05.186',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:17:13.757',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:17:43.344',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:19:38.339',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:21:56.760',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:24:23.738',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:24:41.019',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:27:48.138',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:53:47.218',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:54:07.429',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:54:19.728',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 17:58:41.010',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:02:18.653',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:02:40.190',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:11:14.136',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:11:27.439',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:15:47.321',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:20:50.364',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:22:19.519',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:25:24.377',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:27:26.722',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:30:19.233',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:30:34.907',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:30:41.850',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:30:48.680',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:38:47.410',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:39:17.915',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:43:22.840',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:47:33.015',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:49:54.747',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:52:07.325',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 18:53:13.258',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:02:16.957',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:02:45.153',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:13:45.878',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:14:03.245',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:15:37.835',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:16:10.177',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:16:33.299',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:18:37.562',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 19:29:36.883',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 20:02:17.491',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 20:02:46.823',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 21:02:17.529',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 21:02:47.210',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 22:02:18.027',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 22:02:46.820',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 22:49:10.759',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 23:02:18.661',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-15 23:02:48.051',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 00:02:18.701',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 00:02:49.493',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 01:02:19.020',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 01:02:49.246',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 02:02:19.956',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 02:02:49.777',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 03:02:20.488',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 03:02:48.914',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 04:02:19.269',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 04:02:48.822',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 05:02:19.754',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 05:02:47.587',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 06:02:20.524',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 06:02:48.954',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 07:02:21.490',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 07:02:49.785',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 08:02:44.845',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 08:02:47.196',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 09:00:36.218',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 09:02:20.409',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 09:02:50.463',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 09:50:45.459',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 10:02:21.923',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 10:02:49.623',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 11:02:21.663',_binary '','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 11:02:50.610',_binary '','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 12:02:22.047',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 12:02:51.138',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 12:21:14.142',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 13:02:22.203',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 13:02:49.261',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 14:02:22.656',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 14:02:50.703',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 15:02:22.422',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 15:02:51.317',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 16:02:23.251',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 16:02:52.632',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 17:02:23.523',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 17:02:53.307',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 18:02:24.344',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 18:02:55.122',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 19:02:23.348',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 19:02:55.047',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 20:02:24.920',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 20:02:55.499',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 21:02:30.927',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 21:03:01.089',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 22:02:27.738',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 22:03:00.208',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 23:02:23.290',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-16 23:02:58.888',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 00:02:27.427',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 00:03:01.449',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 01:02:53.677',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 01:39:07.121',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 02:02:59.686',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 03:03:01.817',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 04:03:03.479',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 05:02:59.492',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 06:03:02.136',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 07:02:59.060',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 08:03:04.219',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 09:03:01.506',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 10:03:09.686',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 11:03:04.581',_binary '','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 12:03:04.568',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 13:03:03.234',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 14:03:04.378',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 15:03:03.271',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 16:03:07.655',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 17:03:06.279',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 18:03:16.462',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 19:03:14.234',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 20:03:08.997',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 21:03:03.864',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 22:03:06.327',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 22:48:57.672',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-17 23:03:03.403',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 00:03:03.894',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 01:03:16.885',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 02:03:04.020',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 03:03:05.637',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 04:03:05.848',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 05:03:09.238',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 06:03:12.953',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 07:03:07.596',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 08:03:15.447',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 09:03:06.461',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 10:03:11.079',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 11:03:20.796',_binary '','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 12:03:16.907',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 13:03:15.342',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 14:03:07.230',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 15:03:08.841',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 16:03:16.430',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-18 16:13:06.052',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 00:08:59.874',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 00:11:14.992',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 00:11:26.472',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 00:15:57.362',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 00:43:26.560',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 01:02:11.896',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 01:02:42.550',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 01:03:28.786',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.006',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.416',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.598',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.707',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.749',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.786',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.862',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.879',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:38.895',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.006',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.042',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.063',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.159',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.162',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.222',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.243',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.247',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.300',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.356',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.376',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.421',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.430',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.434',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.489',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:39.493',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:03:41.288',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:10:52.498',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:13:01.043',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:38:47.271',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 17:43:29.068',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 18:02:17.365',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 18:02:44.586',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 18:50:54.794',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 19:02:17.725',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 19:02:47.005',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 20:02:16.923',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 20:02:46.730',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 20:51:11.544',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 21:02:17.712',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 21:02:47.963',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 22:02:17.422',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 22:02:46.260',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 23:02:17.236',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 23:02:46.866',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-19 23:56:41.414',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 00:02:17.750',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 00:02:47.150',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 01:02:18.507',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 01:02:47.170',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 02:02:19.473',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 02:02:47.672',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 02:32:11.475',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 03:02:19.970',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 03:02:46.959',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 04:02:19.290',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 04:02:49.178',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 05:02:20.337',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 05:02:48.544',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 06:02:21.119',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 06:02:47.215',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 07:02:20.466',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 07:02:49.211',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 08:02:20.148',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 08:02:52.298',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 09:02:20.933',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 09:02:49.966',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 10:02:20.016',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 10:02:51.524',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 11:02:22.477',_binary '','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 11:02:53.429',_binary '','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 12:02:21.035',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 12:02:55.205',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 13:02:22.664',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 13:02:54.317',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 14:02:23.459',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 14:02:56.748',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 15:02:22.099',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 15:02:55.381',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 16:02:21.377',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 16:02:57.774',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 17:02:52.175',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 18:02:22.832',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 18:02:58.215',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 19:02:22.357',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 19:02:57.385',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 20:02:26.726',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 20:03:00.582',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 21:01:19.977',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 21:02:25.010',_binary '\0','ef3a105c-168a-442c-81a7-b16d8c043f10',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-20 21:02:59.003',_binary '\0','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-26 00:31:50.329',_binary '\0','',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-26 00:32:51.606',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-26 00:45:15.733',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-26 01:01:52.289',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-27 17:07:56.159',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-27 17:08:46.664',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-27 17:09:43.024',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-27 17:10:17.185',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-27 17:12:32.093',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-27 17:13:05.657',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-27 17:17:29.207',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-27 18:01:53.694',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-27 19:01:52.720',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-27 20:01:54.261',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-27 21:01:54.370',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-27 22:01:55.412',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-27 23:41:28.880',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '\0',0),('2024-03-28 00:01:54.860',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 00:05:42.045',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 01:01:55.294',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 02:01:55.087',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 03:01:56.953',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 04:01:57.134',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 05:01:55.268',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 06:01:55.180',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 07:01:55.916',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 08:01:57.361',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 08:02:13.834',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 08:02:19.393',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 09:01:56.545',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 10:01:57.412',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 11:01:57.557',_binary '','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 12:01:57.894',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 13:01:59.240',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 14:02:02.569',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 15:01:57.925',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 16:02:00.188',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 17:02:07.896',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 18:02:08.481',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 19:02:09.405',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 20:02:01.564',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 21:02:06.741',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 22:02:04.167',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-28 23:02:09.624',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-29 00:00:53.628',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-29 00:01:44.813',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-29 00:01:57.689',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0),('2024-03-29 00:02:07.997',_binary '\0','78985d25-29e3-4334-be82-ceda7d324baf',_binary '\0',_binary '\0',_binary '\0',_binary '\0',_binary '',0);
/*!40000 ALTER TABLE `deviceStatusLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventLog`
--

DROP TABLE IF EXISTS `eventLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eventLog` (
  `timestamp` timestamp(3) NOT NULL,
  `entityId` varchar(255) DEFAULT NULL,
  `entityType` int DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `eventId` int DEFAULT NULL,
  `flagged` bit(1) DEFAULT NULL,
  `message` text,
  `muted` bit(1) DEFAULT NULL,
  `occurrences` varchar(255) DEFAULT NULL,
  `opened` bit(1) DEFAULT NULL,
  `statusStr` text,
  `type` int DEFAULT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventLog`
--

LOCK TABLES `eventLog` WRITE;
/*!40000 ALTER TABLE `eventLog` DISABLE KEYS */;
INSERT INTO `eventLog` VALUES ('2024-02-19 22:14:26.493','1272c313-7e34-4ab8-9b77-3868756c512a',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, using device\'s existing ID.',_binary '\0','2024-02-28 09:17:07.344,2024-02-27 17:08:37.566,2024-02-27 16:55:23.182,2024-02-27 16:16:24.709,2024-02-27 15:16:27.539,2024-02-27 14:16:22.554,2024-02-27 13:16:23.928,2024-02-27 13:04:06.556,2024-02-27 11:22:23.328,2024-02-27 11:16:47.304',_binary '\0',NULL,0),('2024-03-02 03:00:15.466','dd5747c3-53e2-4fe2-b715-cac4c146dff8',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, generated a unique id for device.',_binary '\0','2024-03-13 18:06:35.222,2024-03-13 17:23:17.653,2024-03-12 13:27:46.587,2024-03-12 12:34:48.119,2024-03-12 12:34:21.508,2024-03-12 12:32:36.127,2024-03-12 12:32:09.162,2024-03-12 12:30:44.175,2024-03-12 12:30:17.699,2024-03-12 12:28:06.816',_binary '\0',NULL,0),('2024-03-05 03:24:53.173','dd5747c3-53e2-4fe2-b715-cac4c146df18',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, using device\'s existing ID.',_binary '\0','2024-03-06 16:24:08.514,2024-03-06 11:17:25.801,2024-03-06 09:23:13.731,2024-03-05 15:38:10.956',_binary '\0',NULL,0),('2024-03-11 22:22:41.967','dd5747c3-53e2-4fe2-b715-cac4c146dff8',0,'Scale File Download',1000,_binary '\0','Scale: 10.3.128.229downloading file: gold-apple.png, profileId: 31aa7ce3-9c0a-425e-b013-3a871897179e',_binary '\0','2024-03-13 18:06:55.243,2024-03-13 18:06:50.846,2024-03-12 17:06:37.382,2024-03-12 17:06:32.269,2024-03-12 17:06:26.833,2024-03-11 15:06:15.825,2024-03-11 14:34:33.394,2024-03-11 14:34:30.858,2024-03-11 14:31:04.402,2024-03-11 14:31:02.588',_binary '\0',NULL,0),('2024-03-12 00:23:49.492','fb6d0d48-9be0-4d31-a36d-e6ef0bb2e506',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, using device\'s existing ID.',_binary '\0','2024-03-12 16:47:00.919,',_binary '\0',NULL,0),('2024-03-13 02:00:17.776','10.3.128.210',0,'Scale Download Error: No IP Address',1001,_binary '\0','Download request from: 10.3.128.210was rejected because no record could be found with given ip address',_binary '\0','2024-03-13 09:00:26.022,2024-03-13 08:00:24.412,2024-03-13 07:00:21.051,2024-03-13 06:00:22.333,2024-03-13 05:00:22.211,2024-03-13 04:00:23.758,2024-03-13 03:00:23.304,2024-03-13 02:00:20.705,2024-03-13 01:00:20.504,2024-03-13 00:00:18.979',_binary '\0',NULL,2),('2024-03-13 17:57:43.614','cdada6c2-3f3b-4cfa-92a7-a803c8ba60a3',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, generated a unique id for device.',_binary '\0','',_binary '\0',NULL,0),('2024-03-14 17:22:16.867','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, generated a unique id for device.',_binary '\0','2024-03-19 09:10:52.157,2024-03-19 09:03:36.257,2024-03-19 09:03:35.358,2024-03-19 09:03:35.505,2024-03-19 09:03:35.324,2024-03-18 16:08:57.573,2024-03-15 17:02:48.395,2024-03-15 14:49:09.873,2024-03-15 09:24:40.706,2024-03-14 11:00:16.284',_binary '\0',NULL,0),('2024-03-14 17:22:22.223','4cb1800a-cceb-4929-861d-cf42a5dcd7d7',0,'Scale File Download',1000,_binary '\0','Scale: 10.3.128.239downloading file: MicrosoftTeams-image (2).png, profileId: aabd38e6-775f-4765-ae9f-db7b1e1dd1c8',_binary '\0','2024-03-18 16:09:05.914,2024-03-18 16:09:04.276,2024-03-14 10:03:00.483,2024-03-14 10:02:58.526,2024-03-14 10:02:56.515,2024-03-14 10:00:21.484,2024-03-14 10:00:20.063,2024-03-14 09:22:28.213,2024-03-14 09:22:26.211',_binary '\0',NULL,0),('2024-03-14 18:42:53.042','ef3a105c-168a-442c-81a7-b16d8c043f10',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, generated a unique id for device.',_binary '\0','2024-03-19 09:03:35.977,2024-03-19 09:03:35.965,2024-03-19 09:03:35.63,2024-03-19 09:03:35.146,2024-03-19 09:03:34.824,2024-03-18 18:02:10.622,2024-03-15 17:02:17.535,2024-03-15 15:02:18.177,2024-03-15 09:24:23.499',_binary '\0',NULL,0),('2024-03-14 18:42:57.546','ef3a105c-168a-442c-81a7-b16d8c043f10',0,'Scale File Download',1000,_binary '\0','Scale: 10.3.128.237downloading file: MicrosoftTeams-image (2).png, profileId: aabd38e6-775f-4765-ae9f-db7b1e1dd1c8',_binary '\0','2024-03-14 14:26:07.616,2024-03-14 14:26:05.608,2024-03-14 14:26:03.957,2024-03-14 10:43:01.235,2024-03-14 10:42:59.245',_binary '\0',NULL,0),('2024-03-26 00:31:49.295','78985d25-29e3-4334-be82-ceda7d324baf',0,'New Device Reported',1100,_binary '\0','A new device reported for the first time, generated a unique id for device.',_binary '\0','2024-03-28 15:02:09.355,2024-03-28 13:02:06.063,2024-03-28 10:02:07.363,2024-03-27 16:05:41.47,2024-03-27 16:01:54.211,2024-03-27 15:41:28.148,2024-03-27 15:40:10.245,2024-03-27 09:08:45.807,2024-03-27 09:07:54.64',_binary '\0',NULL,0),('2024-03-26 00:31:57.957','78985d25-29e3-4334-be82-ceda7d324baf',0,'Scale File Download',1000,_binary '\0','Scale: 10.3.128.233downloading file: MicrosoftTeams-image (2).png, profileId: aabd38e6-775f-4765-ae9f-db7b1e1dd1c8',_binary '\0','2024-03-27 15:41:40.408,2024-03-27 15:41:38.392,2024-03-27 15:41:35.43,2024-03-27 09:17:33.858,2024-03-27 09:13:19.809,2024-03-27 09:13:17.867,2024-03-27 09:13:15.75,2024-03-27 09:12:36.266,2024-03-27 09:09:55.282,2024-03-27 09:08:09.64',_binary '\0',NULL,0);
/*!40000 ALTER TABLE `eventLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventRules`
--

DROP TABLE IF EXISTS `eventRules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eventRules` (
  `eventId` varchar(255) NOT NULL,
  `customRule` int DEFAULT NULL,
  `shouldEmail` bit(1) DEFAULT NULL,
  `shouldLog` bit(1) DEFAULT NULL,
  `shouldMute` bit(1) DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventRules`
--

LOCK TABLES `eventRules` WRITE;
/*!40000 ALTER TABLE `eventRules` DISABLE KEYS */;
INSERT INTO `eventRules` VALUES ('1000',NULL,_binary '\0',_binary '',_binary '\0'),('1001',NULL,_binary '\0',_binary '',_binary '\0'),('1002',NULL,_binary '\0',_binary '',_binary '\0'),('1003',NULL,_binary '\0',_binary '',_binary '\0'),('1004',NULL,_binary '\0',_binary '',_binary '\0'),('1005',NULL,_binary '\0',_binary '',_binary ''),('1100',NULL,_binary '\0',_binary '',_binary '\0'),('1101',NULL,_binary '\0',_binary '\0',_binary '\0'),('1201',NULL,_binary '\0',_binary '\0',_binary '\0'),('1202',NULL,_binary '\0',_binary '',_binary '\0'),('1203',NULL,_binary '\0',_binary '',_binary '\0'),('1204',NULL,_binary '\0',_binary '',_binary '\0');
/*!40000 ALTER TABLE `eventRules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters`
--

DROP TABLE IF EXISTS `filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filters` (
  `id` int NOT NULL AUTO_INCREMENT,
  `filterId` varchar(45) NOT NULL,
  `prop` varchar(45) DEFAULT NULL,
  `value` varchar(15000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters`
--

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fumLog`
--

DROP TABLE IF EXISTS `fumLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fumLog` (
  `entryId` int NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upgradeId` varchar(45) NOT NULL,
  `eventDesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fumLog`
--

LOCK TABLES `fumLog` WRITE;
/*!40000 ALTER TABLE `fumLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `fumLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hobfile`
--

DROP TABLE IF EXISTS `hobfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hobfile` (
  `fileId` varchar(255) NOT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `endDate` datetime(6) DEFAULT NULL,
  `fileType` int DEFAULT NULL,
  `fileVersion` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `profileId` varchar(255) DEFAULT NULL,
  `shortDesc` varchar(255) DEFAULT NULL,
  `size` bigint NOT NULL,
  `startDate` datetime(6) DEFAULT NULL,
  `uploadDate` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`fileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hobfile`
--

LOCK TABLES `hobfile` WRITE;
/*!40000 ALTER TABLE `hobfile` DISABLE KEYS */;
INSERT INTO `hobfile` VALUES ('089851c5-0245-4092-a77b-1b5bead2eadb','43c99622300bff5b9841be09f287fd8c',_binary '',NULL,4,'1.0','MicrosoftTeams-image (2).png','aabd38e6-775f-4765-ae9f-db7b1e1dd1c8','fake file',221622,'2024-03-12 21:01:14.000000','2024-03-12 21:01:29.722000'),('5aacc65e-72f5-4c5b-adfe-6186b8d5d55d','f5db72d8c2e41e5d361d0b66d9d2b8eb',_binary '\0',NULL,3,'1.0','all_ht.lic','aabd38e6-775f-4765-ae9f-db7b1e1dd1c8','NOT This is the all license test',524,'2024-03-26 19:13:55.000000','2024-03-26 19:14:04.462000'),('7db0b79b-bab1-424c-a9f0-93e399057b99','71f7454569e00391695bb9bf38f3e4f7',_binary '',NULL,4,'1.0','gold-apple.png','aabd38e6-775f-4765-ae9f-db7b1e1dd1c8','This is the appHook',173538,'2024-03-12 20:59:54.000000','2024-03-12 21:00:05.932000'),('f1d29666-31ab-4fa6-a0d8-43f526aa51ab','82997e38064e5225b0a6527424a076fb',_binary '',NULL,1,'1.0','costco_bakery_keys_patch (1).ht','aabd38e6-775f-4765-ae9f-db7b1e1dd1c8','test ing overnight ehartbeating',3295349,'2024-03-12 20:59:36.000000','2024-03-12 20:59:47.217000');
/*!40000 ALTER TABLE `hobfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteDomain`
--

DROP TABLE IF EXISTS `hteDomain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteDomain` (
  `domainId` varchar(36) NOT NULL,
  `parentId` varchar(36) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`domainId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteDomain`
--

LOCK TABLES `hteDomain` WRITE;
/*!40000 ALTER TABLE `hteDomain` DISABLE KEYS */;
INSERT INTO `hteDomain` VALUES ('00000000-0000-0000-0000-000000000000','87588758-8758-8758-8758-875887588758','PROFILE'),('0066cf95-cefa-4e4e-9406-f29898004348','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('01518ee8-67d7-4a78-beeb-13b5bec4b385','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('02104942-7b76-43cc-9585-cb6d5f253a93','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('033f08fa-e8ba-4f9d-8433-70336f6ce139','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('03631d3a-fc94-4cb2-b8ec-48abed89b038','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('03910685-a1ea-4106-a09c-a68f738e9c0b','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('0517ff9f-4ef8-4afb-a7fe-c5d44248eef7','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('066759af-4b82-478c-a3e8-0dfed75f4cf0','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('06bb574d-844a-432f-8e04-4abb47432575','a996e6f5-9353-4745-b016-c7d275b50f23','REGION'),('088cd28c-6121-435c-9dba-9ef0fd59a7da','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('09369c96-4be8-41e2-9767-c0eaf729c981','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('0a247e82-7021-4277-897e-fa1d76c56cf8','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('0a68b59d-5865-48d6-adec-f0ce7f645342','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('0acad801-c8bd-4853-9d34-43ea2927d9ce','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('0af39fef-0ad3-4ea6-9237-5252caebd436','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('0c4a66f0-9c96-4ed5-9f18-02dec3d3f11e','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('0ccbda88-445f-4fbd-be22-ec8d3e9bed87','da3d435f-d99b-4a74-a5e4-22a9236a04fa','REGION'),('0cf4da6e-e38a-4345-b8af-b37356386e49','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('0d4548a6-7fd2-41a7-8c09-2b9e0552a742','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('0de3ea62-f308-4f09-98b6-777302076e63','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('0e8d6802-4bbe-476d-a99c-7ae10d20d770','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('0f87cbd7-3c10-46df-9cef-a22a99417b0d','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('0fcc2ebe-42aa-4a26-9065-2e288488618e','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('10c037b4-f33b-4af4-847a-40120d5dd092','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('11111111-1111-1111-1111-111111111111','87588758-8758-8758-8758-875887588758','ENTITY'),('118d3e8a-33e7-4cf2-84d7-b6429566faf7','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('135aebdb-6bdb-4b76-b9f4-dc9766908849','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('145b4f54-191a-4bd0-8fc7-4a65032cb56a','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('14a9a80c-e95e-4617-bc8f-2da9d8a03342','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('16a3d277-bc66-43bb-9a81-89433f1c9921','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('1800c797-82f4-42bd-8898-0f9028757272','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('1856457e-e85e-4ce3-8971-bca52d0a69d3','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('18ef9093-8679-484a-8e2a-747368522e5a','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('1908bd27-cf0d-4d16-8cbf-a7e63765191d','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('1b8dfd92-1222-4597-be88-a3ba8d03d6f2','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('1badaf27-5abb-4f22-a793-1388ec9b18ec','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('1c6e08f4-2998-400c-bb2e-f929ca39cabb','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('1f2ba97e-40cc-4e3e-b30f-40852efe4f72','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('1fd93fbc-cd1b-41d0-b590-5fa38ab756ed','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('21f39af0-90da-4e80-8e76-e6424ef37ddf','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('220bc5e5-e609-465d-95e4-be8d4515fa76','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('22144cba-2485-49d7-87be-dae45513267d','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('2489283c-a6df-4100-98b9-4dea8b0281ea','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('24cad8d5-94d2-4757-89b3-b5fb2e1bd2ed','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('256f2f25-ee8c-4fd5-91f2-f1cc313a7290','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('260c5450-3503-4c55-b726-84ace14c1c31','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('262f2f8d-b797-4410-be0f-91571742e2dc','87588758-8758-8758-8758-875887588758','BANNER'),('2639b53b-8d26-4c70-878a-11b9f2944e2f','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('26edd180-7ad4-4a6d-89ee-11da16678751','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('2a874971-a071-4ef6-a7dc-2ddfcb2059eb','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('2aa2890c-0aaf-41ba-a627-1e5aeed4f972','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('2aa48a9c-de96-4153-88e8-4ffc7b3fcb54','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('2b863121-88ae-4b1a-a895-80fe4af2e1ea','a996e6f5-9353-4745-b016-c7d275b50f23','REGION'),('2c39b9ec-6c90-4a98-8bb8-c22685b2983f','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('2c3cef67-b938-4ad6-8f8c-f5439c785c6f','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('2d19e8f2-bf26-4e2d-82ae-539df0de0dea','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('2dc50588-0b0c-47ea-8507-e52a46012b4a','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('2dc60671-9924-4863-b4cb-936f9ee54547','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('2df455fe-6a5f-4a5b-a29b-022311abb0c3','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('2faa95d9-e21e-4467-b778-27e4a931a8ed','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('300be4af-76b0-49f5-bb45-4e5e0f4c04fc','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('30382656-dae1-4375-ae6a-69adf51d8520','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('30d0ebcd-83ff-48c4-a4fc-6cf439e15bfa','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('312d41a6-aa80-4adb-918b-456eb0e91a96','0ccbda88-445f-4fbd-be22-ec8d3e9bed87','STORE'),('323193ad-2e84-4565-b892-24c95b32fe48','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('3252c39d-8fe6-4a79-8c07-0593bdccca25','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('344152a6-f21a-43de-8453-eb273630c56c','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('3acbaed5-db6b-4f3a-8862-7420675d690b','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('3be77d1b-aa9f-4497-86c1-996a9fb5d50c','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('3c19d135-c096-41cc-868b-6cd0ee4c01f9','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('3dbe6abc-e7a0-426a-9362-30ef7a1eb40a','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('3e67ffb2-1d89-4b1a-bd99-f955f18726ae','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('3ec735cf-abf0-4276-928f-62a412e10f43','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('404bb666-cbfb-4cce-87b0-1971f94bd478','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('405ff8e6-1dcf-4095-9bf0-16c9319923e7','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('41590d9b-ef62-43e9-886c-345be701546f','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('421401e8-e5d3-4cca-9ffa-7c5601f68412','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('42840932-932d-4913-926c-baff58c14f4a','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('42dcaa0a-9b1b-4f25-b9ed-903410cdddd7','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('44ab4514-f77c-4361-abaa-2bfd5e0e724e','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('455b5b8e-2e76-44e5-9040-8d457887fd6b','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('4574f1c5-12ec-4a26-923a-41b32ee26725','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('45e8997b-d67d-42ae-9a35-af64f125b9ae','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('45fad425-9ac7-405e-b2ec-dda703240b83','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('49376b40-538f-44df-be9b-38ce09d946ec','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('4a385c9d-9c06-4a66-8292-ebc1d892fcfa','87588758-8758-8758-8758-875887588758','PROFILE'),('4a9e608a-c02a-467c-83d8-593ad05751b3','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('4b1d83f6-113b-4f53-af5d-f8dcb6df26ff','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('4b1e7415-8e7b-492d-8d46-904e16e1eb2a','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('4c5e48d8-c450-4953-b378-42c750415de1','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22','2b863121-88ae-4b1a-a895-80fe4af2e1ea','STORE'),('5061f7c0-2aa6-486c-a35d-6054f2e04adb','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('5221531e-b4ae-4a50-9895-b4bf494b3ffd','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('53837114-89f0-4759-931b-f25d7dc3f166','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('538db841-7dad-48d4-b7d9-9eafee16b3fa','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('53dee7a6-4f2d-4a2c-a704-aa2c124633b0','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('5617da10-3d61-4740-bf0e-91d9497d74cd','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('561c36bb-153d-4953-a2e5-96cafa15b77a','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('565e2606-660b-4f43-9035-567492ae6f2e','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('58074566-84ce-420a-b6f6-c3e696b62e91','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('5affcb77-7519-40c7-bc3a-aaa34cd09242','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('5c0539c8-ded2-4a7a-99a1-92a185e4acdf','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('5d6dae85-a1ba-4c9f-9ad3-3bba4a9e96ae','87588758-8758-8758-8758-875887588758','DEPARTMENT'),('5d902826-879d-4d1a-b5e7-5788e9169bed','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('5e999b98-b133-4ada-82ac-76c420ca164b','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('5fa97d05-721c-485b-80f5-eab8689ad55e','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('605983a9-6942-434d-baa4-d74c3c2d5898','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('610d6543-01ef-4845-b47c-25539011fa6d','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('648b186c-6229-402c-9f4d-d96c901aa4c2','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('659823c6-8fe9-4b7c-b073-0dafd472190e','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('65e31f82-451b-4c2f-bddf-9a7d2ddce3ea','262f2f8d-b797-4410-be0f-91571742e2dc','REGION'),('675ba123-00c3-40c2-88e7-ca3f99ef32e7','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('676e51f7-0eaa-4e20-8e73-27c554e39181','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('69ca2ee8-78b9-4997-b581-ac4add5c8a20','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('69ead9d3-9f3c-4791-8cb8-b1be3b6bd979','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('6b6a8758-f4b7-4dd6-9151-381268ed39e3','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('6c0743f7-d8a1-4204-9b77-eafabbe0e550','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('6c8ffc53-faa1-493f-88eb-bd3fef0f8a19','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('6e38e7bd-9695-4d26-8e67-d6459fa79977','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('6eba8953-f242-4897-8582-05eba0dea089','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('6f422497-b931-440c-ad2c-1d5080950ef4','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('6fa11467-0c8f-46fc-a8a5-666b35d02add','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('7187d10d-1b0e-43af-b774-937095420f1f','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('72fe2f9e-ec9b-4214-869b-2ced3faa2983','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('73bd7634-408e-4776-8431-0b69cfc6c87f','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('745f1fc6-98b8-44ca-8940-fc59c207538b','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('74d495a9-ece5-4ba6-8385-1072acecac22','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('75697543-2772-44e2-af73-dd1afd22b9fd','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('761c7626-fb24-4fe9-b0a4-156eea4262ca','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('7730928a-2d79-4a10-93c8-e1d03c50a8a1','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('78931a58-8666-49d5-b2b1-d83d40c1eb1f','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('7af96a15-4436-4f4d-bb47-0599f0633a7b','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('7c61ac5f-0e4c-4d97-8648-fa4bd0208013','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('7e049fca-e975-4f20-af39-e240c826145b','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('7f24018c-f6a4-42ec-adc6-0cd586cc0467','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('818b4e9d-08aa-4471-bea4-65ef025614a0','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('820c11fd-0772-4f44-b752-360eb49e724c','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('82264e49-3402-4916-95bc-4271c0511b4f','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('833fcfc1-a5f4-4688-8627-52da03e2159f','65e31f82-451b-4c2f-bddf-9a7d2ddce3ea','STORE'),('84c94473-7e86-48c8-b973-3e2eea2e0765','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('87588758-8758-8758-8758-875887588758',NULL,'ENTITY'),('875c11ed-6a4b-41ad-91e7-40015c2aff72','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('89767c2d-da28-4a9d-9245-a812f6da8391','87588758-8758-8758-8758-875887588758','DEPARTMENT'),('8a00f69c-54cd-48e6-9ce5-0898010e6b9a','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('8c29e872-883d-4ebd-9115-067845e2eaa0','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('8cf36c5e-aa7a-4d2d-b613-06574129d64f','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('911d3eff-150f-4414-849c-b6417ca129cf','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('9133d667-4e74-4f37-a04c-39ce7a2af60a','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('92c8f7f9-f808-4dba-bb2f-3fc4cb81740a','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('93a7833b-dfe0-4e84-a544-3a72dd530e94','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('95668df0-4d9b-4545-beb7-179f6daeb5ad','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('95b379b2-6ddd-4e4f-80b6-e1c32503ea2c','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('989bc0ed-5b4e-4783-bee1-ce149afce73b','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('99abd7a8-2f92-4b82-9dd8-0657f90f4023','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('9a047d2f-8289-447e-8b1f-783ac1c2ec36','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('9ade4f27-4a01-416d-85b1-67b1a5ac67eb','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('9b6e8ba1-b89b-4dc4-862e-5e4ca83d8aa0','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('9d1d68b1-1841-490c-ba32-e727dcc9762b','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('9e031529-ca8d-47cf-8cfd-7474f2079043','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('9f2efc44-a009-4fb2-be6d-d3c443f1b458','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('9fb408c4-fd08-4fc5-9c04-3ba615e2c08e','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('a34fa902-9a70-459c-8ec0-281fc9f46091','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('a407ea25-e317-40ae-b24f-af6e635d3959','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('a54b8229-7969-49fb-88c9-9ec286718264','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('a571bfbb-7c96-4d9e-a1e7-88bf36924041','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('a5c054e6-5cb9-4693-8750-1d2ce86b8527','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('a62f9ab4-ecf0-4ae2-9b45-07225474b0b2','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('a6993a79-34f3-42a6-a1e4-8a11bb04e589','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('a6a3ba9f-c3aa-43cb-b2a4-81552ce19dff','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('a70ae067-3992-48d6-83dd-9a77e674c93d','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('a7298327-2623-462c-b09b-fe69df16e574','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('a7f6456e-18e6-4799-9da8-07a024923859','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('a8884bf7-c6a5-4a72-a2d2-100ff1dd95e8','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('a88e382c-cd37-43a1-b2fd-48dcfb412520','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('a8cabd26-0e65-48f3-9803-4d706a495c68','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('a996e6f5-9353-4745-b016-c7d275b50f23','87588758-8758-8758-8758-875887588758','BANNER'),('aa43b588-806f-488a-b9ce-872c4ea6725e','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa','87588758-8758-8758-8758-875887588758','ENTITY'),('aaaf807c-aa37-4c1d-93ee-ef95aa5ca0e9','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('aabd38e6-775f-4765-ae9f-db7b1e1dd1c8','87588758-8758-8758-8758-875887588758','PROFILE'),('ab9e14b9-1088-4f36-a8f6-d0720b9dba2e','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('abf78968-a56f-44f5-9de8-37f67072f3b7','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('b040c025-51bf-4ccb-9dff-90fafe4829c6','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('b131c9d4-cabb-43be-8dcf-2dedf8f83d50','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('b2032171-9dda-410c-8ef8-8f3e876171c4','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('b4c361e7-6ea9-417d-9e52-53a518e3c353','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('b551e746-91bc-4433-9b03-7c4fa49e7dcf','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('b552d98b-a8d3-4256-82a4-fb264909a486','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('b567e10f-3960-444f-a36a-14af4d2f9ae6','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('b6b827fc-2359-4eb6-a72f-5dd61a11f7a3','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('b76b9234-e0b0-478a-9899-c8b1f488832c','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('b7f6cd72-88bb-448c-9905-4af34a841d54','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('b8509c71-f935-4c35-bb11-87c40f8751ab','c1eea3a3-112c-4d1d-9680-919aee2e2faa','STORE'),('b87909de-48a0-49fd-8ad2-a0b80af922fd','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('b89aaa06-f050-4591-a6d9-1383e591ac04','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('bb6e2a62-0d58-4fa7-98c2-b855b30259cf','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('bf4b3190-caff-49cd-bf79-9bfd4f249bbd','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('c0d3cc9a-badc-4b1f-b0c6-86cd905987cd','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('c1264abc-0b98-405f-84de-d1267ef70e8a','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('c14d1cf9-21e8-45e8-bf3a-ebd63361583d','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('c1eea3a3-112c-4d1d-9680-919aee2e2faa','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('c357a768-335c-4fd1-98a5-bcf82e8d38e3','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('c4720b2a-78b1-4e87-9cc1-2734e0be388b','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('c4ef436a-2916-4a06-8b41-8f6777a327cb','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('c52b7b35-7785-4138-8f05-d6688522278d','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('c7bace49-b16b-41ef-93b1-727b16cace89','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('ce2aff93-6395-4c28-b318-e17385405394','7c61ac5f-0e4c-4d97-8648-fa4bd0208013','STORE'),('ce70617d-34da-4f40-9392-1e36849546b7','11111111-1111-1111-1111-111111111111','DEPARTMENT'),('cfe397d5-5295-49c7-b6b1-33289d78f613','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('d083bce2-46f9-4410-8307-1918941fb6a0','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('d30fe10a-4a16-4185-8d07-c859fb428f62','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('d5dc5903-fbf6-40e6-bcb2-616163fa762a','fb093136-1ebe-4e4d-8623-22560e12263d','STORE'),('da3d435f-d99b-4a74-a5e4-22a9236a04fa','87588758-8758-8758-8758-875887588758','BANNER'),('db1e1e35-0380-4098-abe8-f42a31a9b1b1','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('db34fb00-aba9-47bc-a3ac-228e439cf6f0','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('dbba91c2-37df-44d7-84a6-ee9b8cf8464a','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('ddfe3b86-9ac5-4e65-b538-d2406a2b5680','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('de0723ef-eaf9-4613-9070-56b386885257','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('df43346b-46f0-42ee-b788-d065050f1d93','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('e22bdf87-9331-4062-b2e8-71ad913be57f','3043cd78-c0b4-46c3-9b7f-e9ad5b1ab4a0','STORE'),('e4282660-2144-4ab3-8289-3c1e1ec6f81a','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('e49809cd-8b81-4403-a719-8992aacd67ce','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('e5bfd2ab-146f-4d7f-ad2c-25e39afd91c4','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('e7825da6-b666-4c6e-90e2-df6b08443462','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('e919522f-9bae-44b3-b305-92effbeefbd5','87588758-8758-8758-8758-875887588758','BANNER'),('ec2ce0ff-bd74-4f48-baa2-bc137b458993','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('ee2a7b84-c616-4fef-8e0d-af14dfe42d9e','a88e382c-cd37-43a1-b2fd-48dcfb412520','STORE'),('eebc4774-4bff-403c-9400-13cad4458f4a','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('f03c743d-50a6-4af8-9ab3-daf4e3ebcfc3','3e67ffb2-1d89-4b1a-bd99-f955f18726ae','STORE'),('f26a47e6-a9e6-4d8b-ab72-8d0c73d71e5a','2a874971-a071-4ef6-a7dc-2ddfcb2059eb','STORE'),('f2c04f2d-3aed-4a74-a76d-761b7a4b96e2','99abd7a8-2f92-4b82-9dd8-0657f90f4023','STORE'),('f6ef25b3-3b5d-4a43-bff9-f6c84735c59a','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('f72f64fb-a3d8-4203-bc88-df9481a14ca2','194503c8-1e2a-40fa-8cff-9ba27ee2e8e3','STORE'),('fb093136-1ebe-4e4d-8623-22560e12263d','e919522f-9bae-44b3-b305-92effbeefbd5','REGION'),('fc5033d3-4fb9-4f84-8a33-9ce6ab544899','06bb574d-844a-432f-8e04-4abb47432575','STORE'),('fed83ecc-6c7f-4e96-ac32-12d0745b46a8','5c0539c8-ded2-4a7a-99a1-92a185e4acdf','STORE'),('ffc0c84e-0331-456c-aebc-e2497b21b896','11111111-1111-1111-1111-111111111111','DEPARTMENT');
/*!40000 ALTER TABLE `hteDomain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteLicense`
--

DROP TABLE IF EXISTS `hteLicense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteLicense` (
  `licenseId` varchar(36) NOT NULL,
  `license` varchar(255) NOT NULL,
  `activationDate` datetime(6) DEFAULT NULL,
  `companyAddress` varchar(255) DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `fullName` varchar(255) DEFAULT NULL,
  `numScales` int DEFAULT NULL,
  `numUsers` int DEFAULT NULL,
  `ownerEmail` varchar(255) DEFAULT NULL,
  `productNames` text,
  `timeout` int DEFAULT NULL,
  `licenseAndEnterpriseKeys` varchar(81) DEFAULT NULL,
  PRIMARY KEY (`licenseId`),
  UNIQUE KEY `licenseAndEnterpriseKeys` (`licenseAndEnterpriseKeys`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteLicense`
--

LOCK TABLES `hteLicense` WRITE;
/*!40000 ALTER TABLE `hteLicense` DISABLE KEYS */;
INSERT INTO `hteLicense` VALUES ('0f5d35a3-cea8-41bc-b5ae-ad7ee6891896','enterprise','2026-07-31 04:00:00.000000','401 W MARKET ST. TROY, OH, 45373','HOBART','corey_sloan_enterprise_admin.lic','COREY SLOAN',9999,2,'corey.sloan@itwfeg.com','HTE_SCALEMGMT_ENTERPRISE',0,'35E23A719997EEF8ABFEDFF3E0F24A4C18DBF8A6|E009C9D5BF5C24C4F741E005277971B4BA41AA2C');
/*!40000 ALTER TABLE `hteLicense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteRole`
--

DROP TABLE IF EXISTS `hteRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteRole` (
  `id` int NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteRole`
--

LOCK TABLES `hteRole` WRITE;
/*!40000 ALTER TABLE `hteRole` DISABLE KEYS */;
INSERT INTO `hteRole` VALUES (1,'Gives the user the ability to view the current status of the scales in the fleet','Monitor'),(2,'Allows the user to send the reboot command to a scale','Reboot'),(3,'Allows the user to send a manual heartbeat command to the scale','ManualHeartbeat'),(4,'Allows the user to perform status checks on individual scales','Ping'),(1001,'Allows the user to toggle the scales enable function, when a scale is disabled no updates will occur','EnableDisable'),(1002,'Allows the user to assign and reassign scales to different departments','AssignScales'),(1003,'Permissions to add and remove new HTe operators','ManageOperators'),(1004,'Allows the user to assign a manual profile to a scale','AssignScaleProfiles'),(1005,'User can send a \"Delete All Data\" command to the scale','DeleteScaleData'),(1006,'User can view and download available files hosted on the server','FileViewer'),(1007,'User can deactivate/activate other users in their domain, preventing them from using the site','UserEnableDisable'),(1008,'Ability to download transactions from a store server','DownloadTransactions'),(2001,'Adds ability to add, remove or modify department profiles','ManageProfiles'),(2002,'User can modify and upload files to the server','FileManager'),(2003,'Allows the user to assign existing profiles to departments','AssignDeptProfiles'),(2004,'Gives the user full access to modify the asset tree view','FullTreeView'),(2005,'Permissions to add and remove new HTe supervisors','ManageSupervisors'),(2006,'Import/Delete new and existing licenses for HTe','ManageLicenses'),(2007,'Permissions to upgrade frontend/backend systems','SystemManager'),(2008,'User can indicate a primary scale inside a department','SetPrimaryScale'),(2009,'User can upgrade firmware and send files to devices','UpgradeScales'),(3001,'User can import existing scale data from their HTe Business Application','ImportTree'),(3002,'User can get and export the status of all scales in a fleet','FleetStatus'),(4001,'General Override Permission','ManualOverride'),(4002,'Manage configuration of One time password access','OtpPasswordManager');
/*!40000 ALTER TABLE `hteRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteUser`
--

DROP TABLE IF EXISTS `hteUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteUser` (
  `id` varchar(255) NOT NULL,
  `accessLevel` int DEFAULT NULL,
  `dateCreated` datetime(6) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `domainId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKealsnp9exj8qvqecke0og2wwi` (`domainId`),
  CONSTRAINT `FKealsnp9exj8qvqecke0og2wwi` FOREIGN KEY (`domainId`) REFERENCES `hteDomain` (`domainId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteUser`
--

LOCK TABLES `hteUser` WRITE;
/*!40000 ALTER TABLE `hteUser` DISABLE KEYS */;
INSERT INTO `hteUser` VALUES ('32303230-3230-3230-3230-323032303230',5000,'2023-02-09 21:46:57.628000',_binary '','Hobart','Support','$2a$10$5Wog3FtcatPvb4ogRrJPpOfzL2rnE1BkiJZOAxuIxCl9qzNu/aqqq','+1-937-332-2789','ht.support@itwfeg.com','87588758-8758-8758-8758-875887588758'),('4aacb4d4-0e46-45fe-bcf2-b18f033d4e2c',4000,'2024-02-20 16:17:23.065000',_binary '','Corey','sloan','$2a$10$Xtz9phuA1.YOizqNx6fMTeHJbBRSmrclbwLEKgqqlJAVBXlIlLJHu',NULL,'corey.sloan@itwfeg.com','87588758-8758-8758-8758-875887588758');
/*!40000 ALTER TABLE `hteUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteUser_hteRole`
--

DROP TABLE IF EXISTS `hteUser_hteRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteUser_hteRole` (
  `hteUser_id` varchar(255) NOT NULL,
  `roles_id` int NOT NULL,
  KEY `FKgni346f3ytb0qlede5kwkce6t` (`roles_id`),
  KEY `FKno3hu3qoge34wsngb21mc7i9d` (`hteUser_id`),
  CONSTRAINT `FKgni346f3ytb0qlede5kwkce6t` FOREIGN KEY (`roles_id`) REFERENCES `hteRole` (`id`),
  CONSTRAINT `FKno3hu3qoge34wsngb21mc7i9d` FOREIGN KEY (`hteUser_id`) REFERENCES `hteUser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteUser_hteRole`
--

LOCK TABLES `hteUser_hteRole` WRITE;
/*!40000 ALTER TABLE `hteUser_hteRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `hteUser_hteRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lookupEvent`
--

DROP TABLE IF EXISTS `lookupEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lookupEvent` (
  `eventId` int NOT NULL AUTO_INCREMENT,
  `plu` int NOT NULL,
  `scaleIP` varchar(255) DEFAULT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lookupEvent`
--

LOCK TABLES `lookupEvent` WRITE;
/*!40000 ALTER TABLE `lookupEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `lookupEvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nodeStatus`
--

DROP TABLE IF EXISTS `nodeStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nodeStatus` (
  `deviceId` varchar(255) NOT NULL,
  `status` enum('UNKNOWN','ONLINE','WARNING','ERROR','DISABLED') DEFAULT 'UNKNOWN',
  `sync` enum('NA','NOT_SYNCED','PENDING','SYNCED','UNKNOWN') DEFAULT 'NA',
  `syncMessage` varchar(45) DEFAULT NULL,
  `numOnline` int DEFAULT '0',
  `numError` int DEFAULT '0',
  `numWarning` int DEFAULT '0',
  `numDisabled` int DEFAULT '0',
  `numUnknown` int DEFAULT '0',
  `parentId` varchar(255) DEFAULT NULL,
  `nodeType` enum('BANNER','REGION','STORE','DEPT','SCALE') DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nodeStatus`
--

LOCK TABLES `nodeStatus` WRITE;
/*!40000 ALTER TABLE `nodeStatus` DISABLE KEYS */;
INSERT INTO `nodeStatus` VALUES ('06bb574d-844a-432f-8e04-4abb47432575','UNKNOWN','NA','',0,9,0,0,0,'a996e6f5-9353-4745-b016-c7d275b50f23','REGION',''),('0ccbda88-445f-4fbd-be22-ec8d3e9bed87','UNKNOWN','NA','',1,0,0,0,0,'da3d435f-d99b-4a74-a5e4-22a9236a04fa','REGION',''),('12846544-7eed-4164-b983-934a4575ddd9','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('183e8f4d-f81b-4b4a-ae9b-6507d971dd7b','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('1ec50917-5234-4483-bc2f-43ac23fe2a95','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('2b863121-88ae-4b1a-a895-80fe4af2e1ea','UNKNOWN','NA','',0,9,0,0,0,'a996e6f5-9353-4745-b016-c7d275b50f23','REGION',''),('312d41a6-aa80-4adb-918b-456eb0e91a96','UNKNOWN','NA','',1,0,0,0,0,'0ccbda88-445f-4fbd-be22-ec8d3e9bed87','STORE',''),('312d41a6-aa80-4adb-918b-456eb0e91a96|89767c2d-da28-4a9d-9245-a812f6da8391','UNKNOWN','NA','',1,0,0,0,0,'312d41a6-aa80-4adb-918b-456eb0e91a96','DEPT',''),('4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22','UNKNOWN','NA','',0,9,0,0,0,'2b863121-88ae-4b1a-a895-80fe4af2e1ea','STORE',''),('4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','UNKNOWN','NA','',0,9,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22','DEPT',''),('5d3f8938-02d5-497b-9a93-b97e677eead2','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('61f69f6b-a6a8-4efa-8c0b-5f5635897a27','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('63a42e61-fec7-42cb-b0ab-1ea9f71f3471','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('66fb7000-e439-4fd2-8d3a-c9de29d16227','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('7788a9aa-b331-4647-b9f0-227244d3cb47','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('78985d25-29e3-4334-be82-ceda7d324baf','ONLINE','NA','',1,0,0,0,0,'312d41a6-aa80-4adb-918b-456eb0e91a96|89767c2d-da28-4a9d-9245-a812f6da8391','SCALE','Scale is online and reporting'),('8a847636-d41c-4b68-b3d5-6385ef56761e','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('9643af2c-a5e2-484d-a11a-9da88f2f269e','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('9849fc6d-f955-4bed-86d5-5cc8642eab35','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('9a83d97a-0715-4e5a-aaee-b7054cb55be1','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('a996e6f5-9353-4745-b016-c7d275b50f23','UNKNOWN','NA','',0,18,0,0,0,'','BANNER',''),('b4113968-57d0-4e4e-8331-62dee94e0147','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('bba086e3-d184-49ce-b028-eeb89cbbee19','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('bce93db5-af08-44d2-a61a-6b01fe731dce','ERROR','NA','',0,1,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('da3d435f-d99b-4a74-a5e4-22a9236a04fa','UNKNOWN','NA','',1,0,0,0,0,'','BANNER',''),('e34ded25-3b58-49e0-bb63-e53af8b67fb2','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('e5451170-e484-4bfb-92ac-3985d3bd2681','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('ea685d9a-1027-4f27-a2cd-3266b47428bc','ERROR','NA','',0,1,0,0,0,'4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22|b89aaa06-f050-4591-a6d9-1383e591ac04','SCALE','Could not find any report records for scale, please make sure that scale is online and connected to server.'),('fc5033d3-4fb9-4f84-8a33-9ce6ab544899','UNKNOWN','NA','',0,9,0,0,0,'06bb574d-844a-432f-8e04-4abb47432575','STORE',''),('fc5033d3-4fb9-4f84-8a33-9ce6ab544899|b89aaa06-f050-4591-a6d9-1383e591ac04','UNKNOWN','NA','',0,9,0,0,0,'fc5033d3-4fb9-4f84-8a33-9ce6ab544899','DEPT','');
/*!40000 ALTER TABLE `nodeStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile` (
  `profileId` varchar(36) NOT NULL,
  `deleteSyncConfig` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `lastUpdate` datetime(6) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `requiresDomainId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`profileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES ('00000000-0000-0000-0000-000000000000',NULL,'Initial Profile assigned to new scales','2024-03-28 17:04:04.153000','Default Profile','87588758-8758-8758-8758-875887588758'),('4a385c9d-9c06-4a66-8292-ebc1d892fcfa',NULL,'This is a base profile','2024-03-25 16:49:30.752000','control','87588758-8758-8758-8758-875887588758'),('aabd38e6-775f-4765-ae9f-db7b1e1dd1c8',NULL,'this is used to test scale heartbeating overnight','2024-03-25 00:17:53.951000','test','87588758-8758-8758-8758-875887588758');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `region` (
  `regionId` varchar(36) NOT NULL,
  `bannerId` varchar(36) DEFAULT NULL,
  `regionName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`regionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES ('06bb574d-844a-432f-8e04-4abb47432575','a996e6f5-9353-4745-b016-c7d275b50f23','1.'),('0ccbda88-445f-4fbd-be22-ec8d3e9bed87','da3d435f-d99b-4a74-a5e4-22a9236a04fa','hb region'),('2b863121-88ae-4b1a-a895-80fe4af2e1ea','a996e6f5-9353-4745-b016-c7d275b50f23','2.');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scaleDevice`
--

DROP TABLE IF EXISTS `scaleDevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scaleDevice` (
  `deviceId` varchar(255) NOT NULL,
  `application` varchar(255) DEFAULT NULL,
  `buildNumber` varchar(255) DEFAULT NULL,
  `countryCode` varchar(255) DEFAULT NULL,
  `deptId` varchar(255) DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `isPrimaryScale` bit(1) NOT NULL,
  `lastReportTimestampUtc` datetime(6) DEFAULT NULL,
  `lastTriggerType` varchar(255) DEFAULT NULL,
  `loader` varchar(255) DEFAULT NULL,
  `operatingSystem` varchar(255) DEFAULT NULL,
  `pluCount` int DEFAULT NULL,
  `profileId` varchar(255) DEFAULT NULL,
  `scaleModel` varchar(255) DEFAULT NULL,
  `serialNumber` bigint NOT NULL,
  `smBackend` varchar(255) DEFAULT NULL,
  `storeId` varchar(255) DEFAULT NULL,
  `systemController` varchar(255) DEFAULT NULL,
  `totalLabelsPrinted` int DEFAULT NULL,
  `priority` int unsigned DEFAULT NULL,
  `healthInfoLog` varchar(12500) DEFAULT NULL,
  `profileRecentlyUpdated` bit(1) DEFAULT NULL,
  `hasChildren` bit(1) DEFAULT NULL,
  PRIMARY KEY (`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scaleDevice`
--

LOCK TABLES `scaleDevice` WRITE;
/*!40000 ALTER TABLE `scaleDevice` DISABLE KEYS */;
INSERT INTO `scaleDevice` VALUES ('12846544-7eed-4164-b983-934a4575ddd9','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1735f5','1.1.2.6',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('183e8f4d-f81b-4b4a-ae9b-6507d971dd7b','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1739d4','2.1.2.1',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('1ec50917-5234-4483-bc2f-43ac23fe2a95','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1737ed','1.1.2.4',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('5d3f8938-02d5-497b-9a93-b97e677eead2','2.6.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1735e5','1.1.2.3',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.1.6','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('61f69f6b-a6a8-4efa-8c0b-5f5635897a27','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1737ed','2.1.2.4',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('63a42e61-fec7-42cb-b0ab-1ea9f71f3471','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b17384d','2.1.2.5',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('66fb7000-e439-4fd2-8d3a-c9de29d16227','4.0.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00609d202276','1.1.2.9',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7ELH4',0,'3.3.4','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('7788a9aa-b331-4647-b9f0-227244d3cb47','3.3.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b17394e','1.1.2.7',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'3.2.37','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('78985d25-29e3-4334-be82-ceda7d324baf','4.2.0','hte-4.2.0-05232023_130708','','89767c2d-da28-4a9d-9245-a812f6da8391',_binary '','PIT-lab-hti5','10.3.128.233',_binary '','2024-03-28 20:02:07.934000','MANUAL','3.7.12','4.9.88-1.0.0-hobart-4.0.0-gc946a96',0,'aabd38e6-775f-4765-ae9f-db7b1e1dd1c8','HTi-7LS',451111794,'3.1.17','312d41a6-aa80-4adb-918b-456eb0e91a96','2.0',0,NULL,'Wed Mar 27 15:41:27 2024  Processed downloaded file: /usr/local/hobart/pending/costco_bakery_keys_patch (1).ht\nThu Mar 28 15:52:14 2024  Scale Application PANIC shutdown\nThu Mar 28 15:53:30 2024  Database Integrity Check - OK\nThu Mar 28 15:53:47 2024  Scale Application started\nThu Mar 28 15:53:51 2024  AttachDevice:  Hobart USB APC Printer Attached. node = 45DC9567\nThu Mar 28 15:53:52 2024  Scale information:\nBuild Number: hte-4.2.0-05232023_130708\nApplication Version: 4.2.0.0\nSerial Number: 451111794\nThu Mar 28 15:57:14 2024  Scale Application PANIC shutdown\nThu Mar 28 15:58:31 2024  Database Integrity Check - OK\nThu Mar 28 15:58:49 2024  Scale Application started\nThu Mar 28 15:58:53 2024  AttachDevice:  Hobart USB APC Printer Attached. node = 45DC9567\nThu Mar 28 15:58:54 2024  Scale information:\nBuild Number: hte-4.2.0-05232023_130708\nApplication Version: 4.2.0.0\nSerial Number: 451111794\n',NULL,NULL),('8a847636-d41c-4b68-b3d5-6385ef56761e','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1735d6','2.1.2.8',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('9643af2c-a5e2-484d-a11a-9da88f2f269e','2.6.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1735e5','2.1.2.3',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.1.6','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('9849fc6d-f955-4bed-86d5-5cc8642eab35','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1735d6','1.1.2.8',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('9a83d97a-0715-4e5a-aaee-b7054cb55be1','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b17384d','1.1.2.5',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('b4113968-57d0-4e4e-8331-62dee94e0147','3.3.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b17394e','2.1.2.7',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'3.2.37','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('bba086e3-d184-49ce-b028-eeb89cbbee19','4.0.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00609d202276','1.1.2.2',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7ELH4',0,'3.3.4','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('bce93db5-af08-44d2-a61a-6b01fe731dce','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1739d4','1.1.2.1',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','fc5033d3-4fb9-4f84-8a33-9ce6ab544899',NULL,NULL,NULL,NULL,NULL,NULL),('e34ded25-3b58-49e0-bb63-e53af8b67fb2','2.3.3',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00026b1735f5','2.1.2.6',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7LS2',0,'2.0.22','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('e5451170-e484-4bfb-92ac-3985d3bd2681','4.0.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00609d202276','2.1.2.9',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7ELH4',0,'3.3.4','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL),('ea685d9a-1027-4f27-a2cd-3266b47428bc','4.0.0',NULL,NULL,'b89aaa06-f050-4591-a6d9-1383e591ac04',_binary '','ht00609d202276','2.1.2.2',_binary '\0',NULL,NULL,'',NULL,NULL,'00000000-0000-0000-0000-000000000000','HTi-7ELH4',0,'3.3.4','4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `scaleDevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scaleSettings`
--

DROP TABLE IF EXISTS `scaleSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scaleSettings` (
  `deviceId` varchar(255) NOT NULL,
  `appHookVersion` varchar(255) DEFAULT NULL,
  `currentFiles` text,
  `features` varchar(255) DEFAULT NULL,
  `hasCustomerDisplay` bit(1) NOT NULL,
  `hasLoadCell` bit(1) NOT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `labelStockSize` varchar(255) DEFAULT NULL,
  `lastCommunicationTimestampUtc` datetime(6) DEFAULT NULL,
  `lastFetchPluOffsetTimestampUtc` datetime(6) DEFAULT NULL,
  `lastRebootTimestampUtc` datetime(6) DEFAULT NULL,
  `licenses` varchar(255) DEFAULT NULL,
  `scaleItemEntryMode` varchar(255) DEFAULT NULL,
  `scaleMode` varchar(255) DEFAULT NULL,
  `storeGraphicName` varchar(255) DEFAULT NULL,
  `storeInformation` varchar(255) DEFAULT NULL,
  `timeKeeper` varchar(255) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `weigherCalibratedTimestampUtc` datetime(6) DEFAULT NULL,
  `weigherConfiguredTimestampUtc` datetime(6) DEFAULT NULL,
  `weigherPrecision` varchar(255) DEFAULT NULL,
  `macAddress` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scaleSettings`
--

LOCK TABLES `scaleSettings` WRITE;
/*!40000 ALTER TABLE `scaleSettings` DISABLE KEYS */;
INSERT INTO `scaleSettings` VALUES ('78985d25-29e3-4334-be82-ceda7d324baf','3.0.12','MicrosoftTeams-image (2).png=43c99622300bff5b9841be09f287fd8c,gold-apple.png=71f7454569e00391695bb9bf38f3e4f7,costco_bakery_keys_patch (1).ht=82997e38064e5225b0a6527424a076fb','hte_apphook_v2',_binary '\0',_binary '','10.3.128.233','Continuous','2024-03-27 13:15:53.000000',NULL,'2024-03-28 19:57:56.000000','SCALE_REMOTE_MONITOR,SCALE_NOW_SERVING,','NORMAL','MANUAL','Main Mkt.png',NULL,'MANUAL','Eastern Standard Time',NULL,NULL,'12 x 0.005 lb','00:02:6B:12:E9:B3|60:02:B4:DD:A1:AD');
/*!40000 ALTER TABLE `scaleSettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `id` varchar(255) NOT NULL,
  `buildDate` varchar(255) DEFAULT NULL,
  `databaseId` varchar(255) DEFAULT NULL,
  `lastRestart` datetime(6) DEFAULT NULL,
  `operationMode` varchar(255) DEFAULT NULL,
  `serviceName` varchar(255) DEFAULT NULL,
  `serviceType` int DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES ('28a14b23-c48a-4ba5-94f3-35a9905da95a','03/11/2024','12341234-1234-1234-1234-123412341234','2024-03-27 19:58:52.410000','HQ','Heartbeat Manager',0,'http://10.3.128.204:8081/','1.0.5'),('f10ab2c6-8312-41cd-b736-7b1d874bed3c','03/11/2024','12341234-1234-1234-1234-123412341234','2024-03-28 17:05:12.948000','N/A','HTe Enterprise Manager',4,'http://10.3.128.204:8083/','2.0.5');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store` (
  `storeId` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `customerStoreNumber` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `lastCommunicationTimestampUtc` datetime(6) DEFAULT NULL,
  `lastFetchPluOffsetTimestampUtc` datetime(6) DEFAULT NULL,
  `lastItemChangeReceivedTimestampUtc` datetime(6) DEFAULT NULL,
  `lastRebootTimestampUtc` datetime(6) DEFAULT NULL,
  `regionId` varchar(255) DEFAULT NULL,
  `sendStoreInfo` varchar(255) DEFAULT NULL,
  `storeName` varchar(255) DEFAULT NULL,
  `triggerType` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` VALUES ('312d41a6-aa80-4adb-918b-456eb0e91a96','','',_binary '',NULL,NULL,NULL,NULL,NULL,NULL,'0ccbda88-445f-4fbd-be22-ec8d3e9bed87',NULL,'hb store',NULL,NULL),('4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22','','',_binary '',NULL,NULL,NULL,NULL,NULL,NULL,'2b863121-88ae-4b1a-a895-80fe4af2e1ea',NULL,'2.1',NULL,NULL),('fc5033d3-4fb9-4f84-8a33-9ce6ab544899','','',_binary '',NULL,NULL,NULL,NULL,NULL,NULL,'06bb574d-844a-432f-8e04-4abb47432575',NULL,'1.1',NULL,NULL);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storeDeptPairs`
--

DROP TABLE IF EXISTS `storeDeptPairs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storeDeptPairs` (
  `storeId` varchar(255) NOT NULL,
  `deptId` varchar(255) NOT NULL,
  PRIMARY KEY (`storeId`,`deptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storeDeptPairs`
--

LOCK TABLES `storeDeptPairs` WRITE;
/*!40000 ALTER TABLE `storeDeptPairs` DISABLE KEYS */;
INSERT INTO `storeDeptPairs` VALUES ('312d41a6-aa80-4adb-918b-456eb0e91a96','89767c2d-da28-4a9d-9245-a812f6da8391'),('4c8d4c5d-4150-4b89-ab20-ed5cbd37fb22','b89aaa06-f050-4591-a6d9-1383e591ac04'),('fc5033d3-4fb9-4f84-8a33-9ce6ab544899','b89aaa06-f050-4591-a6d9-1383e591ac04');
/*!40000 ALTER TABLE `storeDeptPairs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storeStatusLog`
--

DROP TABLE IF EXISTS `storeStatusLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storeStatusLog` (
  `timestamp` timestamp(3) NOT NULL,
  `deleteEvent` bit(1) NOT NULL,
  `event1` bit(1) NOT NULL,
  `event2` bit(1) NOT NULL,
  `event3` bit(1) NOT NULL,
  `inSync` bit(1) NOT NULL,
  `storeUuid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storeStatusLog`
--

LOCK TABLES `storeStatusLog` WRITE;
/*!40000 ALTER TABLE `storeStatusLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `storeStatusLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_service`
--

DROP TABLE IF EXISTS `store_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_service` (
  `store_storeId` varchar(255) NOT NULL,
  `services_id` varchar(255) NOT NULL,
  UNIQUE KEY `UK_mby0tx1yc2p6aw0lyo3k5hr6a` (`services_id`),
  KEY `FKbeafncw3vwjrwsb34ra2qiorf` (`store_storeId`),
  CONSTRAINT `FKbeafncw3vwjrwsb34ra2qiorf` FOREIGN KEY (`store_storeId`) REFERENCES `store` (`storeId`),
  CONSTRAINT `FKnba6oakbvj7c7f5cqj07j13fb` FOREIGN KEY (`services_id`) REFERENCES `service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_service`
--

LOCK TABLES `store_service` WRITE;
/*!40000 ALTER TABLE `store_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgradeBatch`
--

DROP TABLE IF EXISTS `upgradeBatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `upgradeBatch` (
  `batchId` varchar(255) NOT NULL,
  `fileId` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `uploadStart` datetime(6) NOT NULL,
  `uploadEnd` datetime(6) NOT NULL,
  `upgradeStart` datetime(6) NOT NULL,
  `upgradeEnd` datetime(6) NOT NULL,
  `name` varchar(255) NOT NULL,
  `afterUpgradeCheckIn` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`batchId`),
  UNIQUE KEY `batchId_UNIQUE` (`batchId`),
  KEY `fileId_idx` (`fileId`),
  CONSTRAINT `fileId` FOREIGN KEY (`fileId`) REFERENCES `hobfile` (`fileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgradeBatch`
--

LOCK TABLES `upgradeBatch` WRITE;
/*!40000 ALTER TABLE `upgradeBatch` DISABLE KEYS */;
/*!40000 ALTER TABLE `upgradeBatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgradeDevice`
--

DROP TABLE IF EXISTS `upgradeDevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `upgradeDevice` (
  `upgradeId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `batchId` varchar(255) DEFAULT NULL,
  `lastUpdate` datetime(6) DEFAULT NULL,
  `scaleModel` varchar(255) DEFAULT NULL,
  `application` varchar(30) DEFAULT NULL,
  `bootloader` varchar(45) DEFAULT NULL,
  `fpc` varchar(20) DEFAULT NULL,
  `status` enum('unassigned','awaiting_upload','awaiting_primary','upload_in_progress','upload_completed_no_errors','upload_completed_errors','upload_failed','upload_now','file_uploaded_awaiting_upgrade','upgrade_in_progress','post_install_process','upgrade_completed_no_errors','upgrade_completed_errors','upgrade_failed','upgrade_now') NOT NULL DEFAULT 'unassigned',
  `statusText` varchar(255) DEFAULT NULL,
  `nextCheck` datetime DEFAULT NULL,
  `groupId` varchar(50) NOT NULL,
  PRIMARY KEY (`upgradeId`),
  UNIQUE KEY `upgradeId_UNIQUE` (`upgradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgradeDevice`
--

LOCK TABLES `upgradeDevice` WRITE;
/*!40000 ALTER TABLE `upgradeDevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `upgradeDevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgradeGroup`
--

DROP TABLE IF EXISTS `upgradeGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `upgradeGroup` (
  `groupId` varchar(255) NOT NULL,
  `groupName` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` enum('ENABLED','DISABLED','COMPLETED') NOT NULL DEFAULT 'DISABLED',
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupId_UNIQUE` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgradeGroup`
--

LOCK TABLES `upgradeGroup` WRITE;
/*!40000 ALTER TABLE `upgradeGroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `upgradeGroup` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-28 16:39:16
