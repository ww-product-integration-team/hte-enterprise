-- MySQL dump 10.13  Distrib 8.0.39, for Linux (x86_64)
--
-- Host: localhost    Database: hte
-- ------------------------------------------------------
-- Server version	8.0.39-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appConfigs`
--

DROP TABLE IF EXISTS `appConfigs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appConfigs` (
  `CoName` varchar(255) NOT NULL,
  `CoValue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CoName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autoAssignmentRules`
--

DROP TABLE IF EXISTS `autoAssignmentRules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `autoAssignmentRules` (
  `ipAddressStart` varchar(15) NOT NULL,
  `ipAddressEnd` varchar(15) NOT NULL,
  `banner` varchar(150) NOT NULL,
  `region` varchar(150) NOT NULL,
  `store` varchar(150) NOT NULL,
  `dept` varchar(150) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ipAddressStart`,`ipAddressEnd`,`banner`,`region`,`store`,`dept`,`enabled`),
  UNIQUE KEY `auto_assign_rules` (`ipAddressStart`,`ipAddressEnd`,`banner`,`region`,`store`,`dept`,`enabled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banner` (
  `bannerId` varchar(36) NOT NULL,
  `bannerName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bannerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checksumLogs`
--

DROP TABLE IF EXISTS `checksumLogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checksumLogs` (
  `date` datetime(6) NOT NULL,
  `scaleIp` varchar(15) NOT NULL,
  `logEntry` varchar(500) NOT NULL,
  PRIMARY KEY (`date`,`scaleIp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configEvent`
--

DROP TABLE IF EXISTS `configEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `configEvent` (
  `idfileEvent` varchar(255) NOT NULL,
  `creationTimestamp` datetime(6) DEFAULT NULL,
  `deptId` varchar(255) DEFAULT NULL,
  `deviceUuid` varchar(255) DEFAULT NULL,
  `eventType` int NOT NULL,
  `finishOn` datetime(6) DEFAULT NULL,
  `shortDesc` varchar(255) DEFAULT NULL,
  `startOn` datetime(6) DEFAULT NULL,
  `storeId` varchar(255) DEFAULT NULL,
  `uuidFile` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idfileEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `depts`
--

DROP TABLE IF EXISTS `depts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `depts` (
  `deptId` varchar(36) NOT NULL,
  `defaultProfileId` varchar(255) DEFAULT NULL,
  `deptName1` varchar(255) DEFAULT NULL,
  `deptName2` varchar(255) DEFAULT NULL,
  `requiresDomainId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`deptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deviceStatusLog`
--

DROP TABLE IF EXISTS `deviceStatusLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `deviceStatusLog` (
  `timestamp` timestamp(3) NOT NULL,
  `deleteEvent` bit(1) NOT NULL,
  `deviceUuid` varchar(255) DEFAULT NULL,
  `event1` bit(1) NOT NULL,
  `event2` bit(1) NOT NULL,
  `event3` bit(1) NOT NULL,
  `event4` bit(1) NOT NULL,
  `inSync` bit(1) NOT NULL,
  `logStatus` int DEFAULT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eventLog`
--

DROP TABLE IF EXISTS `eventLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eventLog` (
  `timestamp` timestamp(3) NOT NULL,
  `entityId` varchar(255) DEFAULT NULL,
  `entityType` int DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `eventId` int DEFAULT NULL,
  `flagged` bit(1) DEFAULT NULL,
  `message` text,
  `muted` bit(1) DEFAULT NULL,
  `occurrences` varchar(255) DEFAULT NULL,
  `opened` bit(1) DEFAULT NULL,
  `statusStr` text,
  `type` int DEFAULT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eventRules`
--

DROP TABLE IF EXISTS `eventRules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eventRules` (
  `eventId` varchar(255) NOT NULL,
  `customRule` int DEFAULT NULL,
  `shouldEmail` bit(1) DEFAULT NULL,
  `shouldLog` bit(1) DEFAULT NULL,
  `shouldMute` bit(1) DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventRules`
--

LOCK TABLES `eventRules` WRITE;
/*!40000 ALTER TABLE `eventRules` DISABLE KEYS */;
INSERT INTO `eventRules` VALUES ('1',NULL,_binary '',_binary '',_binary '\0'),('1000',NULL,_binary '\0',_binary '',_binary '\0'),('1001',NULL,_binary '\0',_binary '',_binary '\0'),('1002',NULL,_binary '\0',_binary '',_binary '\0'),('1003',NULL,_binary '\0',_binary '',_binary '\0'),('1004',NULL,_binary '\0',_binary '',_binary '\0'),('1005',NULL,_binary '\0',_binary '',_binary ''),('1100',NULL,_binary '\0',_binary '',_binary '\0'),('1101',NULL,_binary '\0',_binary '\0',_binary '\0'),('1201',NULL,_binary '\0',_binary '\0',_binary '\0'),('1202',NULL,_binary '\0',_binary '',_binary '\0'),('1203',NULL,_binary '\0',_binary '',_binary '\0'),('1204',NULL,_binary '\0',_binary '',_binary '\0');
/*!40000 ALTER TABLE `eventRules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filters`
--

DROP TABLE IF EXISTS `filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filters` (
  `id` int NOT NULL AUTO_INCREMENT,
  `filterId` varchar(45) NOT NULL,
  `prop` varchar(45) DEFAULT NULL,
  `value` varchar(15000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fumLog`
--

DROP TABLE IF EXISTS `fumLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fumLog` (
  `entryId` int NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upgradeId` varchar(45) NOT NULL,
  `eventDesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hobfile`
--

DROP TABLE IF EXISTS `hobfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hobfile` (
  `fileId` varchar(255) NOT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `endDate` datetime(6) DEFAULT NULL,
  `fileType` int DEFAULT NULL,
  `fileVersion` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `profileId` varchar(255) DEFAULT NULL,
  `shortDesc` varchar(255) DEFAULT NULL,
  `size` bigint NOT NULL,
  `startDate` datetime(6) DEFAULT NULL,
  `uploadDate` datetime(6) DEFAULT NULL,
  `startRebootTime` int DEFAULT NULL,
  `endRebootTime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hteDomain`
--

DROP TABLE IF EXISTS `hteDomain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteDomain` (
  `domainId` varchar(36) NOT NULL,
  `parentId` varchar(36) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`domainId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteDomain`
--

LOCK TABLES `hteDomain` WRITE;
/*!40000 ALTER TABLE `hteDomain` DISABLE KEYS */;
INSERT INTO `hteDomain` VALUES ('00000000-0000-0000-0000-000000000000','87588758-8758-8758-8758-875887588758','PROFILE'),('11111111-1111-1111-1111-111111111111','87588758-8758-8758-8758-875887588758','ENTITY'),('87588758-8758-8758-8758-875887588758',NULL,'ENTITY'),('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa','87588758-8758-8758-8758-875887588758','ENTITY');
/*!40000 ALTER TABLE `hteDomain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteLicense`
--

DROP TABLE IF EXISTS `hteLicense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteLicense` (
  `licenseId` varchar(36) NOT NULL,
  `license` varchar(255) NOT NULL,
  `activationDate` datetime(6) DEFAULT NULL,
  `companyAddress` varchar(255) DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `fullName` varchar(255) DEFAULT NULL,
  `numScales` int DEFAULT NULL,
  `numUsers` int DEFAULT NULL,
  `ownerEmail` varchar(255) DEFAULT NULL,
  `productNames` text,
  `timeout` int DEFAULT NULL,
  `currentLicenseHash` varchar(255) DEFAULT NULL,
  `refreshedAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `statusMessage` varchar(255) DEFAULT NULL,
  `licenseAndEnterpriseKeys` varchar(81) DEFAULT NULL,
  PRIMARY KEY (`licenseId`),
  UNIQUE KEY `licenseAndEnterpriseKeys` (`licenseAndEnterpriseKeys`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hteRole`
--

DROP TABLE IF EXISTS `hteRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteRole` (
  `id` int NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteRole`
--

LOCK TABLES `hteRole` WRITE;
/*!40000 ALTER TABLE `hteRole` DISABLE KEYS */;
INSERT INTO `hteRole` VALUES (1,'Gives the user the ability to view the current status of the scales in the fleet','Monitor'),(2,'Allows the user to send the reboot command to a scale','Reboot'),(3,'Allows the user to send a manual heartbeat command to the scale','ManualHeartbeat'),(4,'Allows the user to perform status checks on individual scales','Ping'),(1001,'Allows the user to toggle the scales enable function, when a scale is disabled no updates will occur','EnableDisable'),(1002,'Allows the user to assign and reassign scales to different departments','AssignScales'),(1003,'Permissions to add and remove new HTe operators','ManageOperators'),(1004,'Allows the user to assign a manual profile to a scale','AssignScaleProfiles'),(1005,'User can send a \"Delete All Data\" command to the scale','DeleteScaleData'),(1006,'User can view and download available files hosted on the server','FileViewer'),(1007,'User can deactivate/activate other users in their domain, preventing them from using the site','UserEnableDisable'),(1008,'Ability to download transactions from a store server','DownloadTransactions'),(2001,'Adds ability to add, remove or modify department profiles','ManageProfiles'),(2002,'User can modify and upload files to the server','FileManager'),(2003,'Allows the user to assign existing profiles to departments','AssignDeptProfiles'),(2004,'Gives the user full access to modify the asset tree view','FullTreeView'),(2005,'Permissions to add and remove new HTe supervisors','ManageSupervisors'),(2006,'Import/Delete new and existing licenses for HTe','ManageLicenses'),(2007,'Permissions to upgrade frontend/backend systems','SystemManager'),(2008,'User can indicate a primary scale inside a department','SetPrimaryScale'),(2009,'User can upgrade firmware and send files to devices','UpgradeScales'),(3001,'User can import existing scale data from their HTe Business Application','ImportTree'),(3002,'User can get and export the status of all scales in a fleet','FleetStatus'),(4001,'General Override Permission','ManualOverride'),(4002,'Manage configuration of One time password access','OtpPasswordManager');
/*!40000 ALTER TABLE `hteRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteUser`
--

DROP TABLE IF EXISTS `hteUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteUser` (
  `id` varchar(255) NOT NULL,
  `accessLevel` int DEFAULT NULL,
  `dateCreated` datetime(6) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `domainId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKealsnp9exj8qvqecke0og2wwi` (`domainId`),
  CONSTRAINT `FKealsnp9exj8qvqecke0og2wwi` FOREIGN KEY (`domainId`) REFERENCES `hteDomain` (`domainId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hteUser`
--

LOCK TABLES `hteUser` WRITE;
/*!40000 ALTER TABLE `hteUser` DISABLE KEYS */;
INSERT INTO `hteUser` VALUES ('32303230-3230-3230-3230-323032303230',5000,'2024-06-21 19:23:13.772000',_binary '','Hobart','Support','$2a$10$UCzOVtt5vXGGBn/HQVXC1OyJy.dYO.lDSTwZ./jRC0Hf1JTCXBFvu','+1-937-332-2789','ht.support@itwfeg.com','87588758-8758-8758-8758-875887588758');
/*!40000 ALTER TABLE `hteUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hteUser_hteRole`
--

DROP TABLE IF EXISTS `hteUser_hteRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hteUser_hteRole` (
  `hteUser_id` varchar(255) NOT NULL,
  `roles_id` int NOT NULL,
  KEY `FKgni346f3ytb0qlede5kwkce6t` (`roles_id`),
  KEY `FKno3hu3qoge34wsngb21mc7i9d` (`hteUser_id`),
  CONSTRAINT `FKgni346f3ytb0qlede5kwkce6t` FOREIGN KEY (`roles_id`) REFERENCES `hteRole` (`id`),
  CONSTRAINT `FKno3hu3qoge34wsngb21mc7i9d` FOREIGN KEY (`hteUser_id`) REFERENCES `hteUser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lookupEvent`
--

DROP TABLE IF EXISTS `lookupEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lookupEvent` (
  `eventId` int NOT NULL AUTO_INCREMENT,
  `plu` int NOT NULL,
  `scaleIP` varchar(255) DEFAULT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nodeStatus`
--

DROP TABLE IF EXISTS `nodeStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nodeStatus` (
  `deviceId` varchar(255) NOT NULL,
  `status` enum('UNKNOWN','ONLINE','WARNING','ERROR','DISABLED') DEFAULT 'UNKNOWN',
  `sync` enum('NA','NOT_SYNCED','PENDING','SYNCED','UNKNOWN') DEFAULT 'NA',
  `syncMessage` varchar(45) DEFAULT NULL,
  `numOnline` int DEFAULT '0',
  `numError` int DEFAULT '0',
  `numWarning` int DEFAULT '0',
  `numDisabled` int DEFAULT '0',
  `numUnknown` int DEFAULT '0',
  `parentId` varchar(255) DEFAULT NULL,
  `nodeType` enum('BANNER','REGION','STORE','DEPT','SCALE') DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pricingZone`
--

DROP TABLE IF EXISTS `pricingZone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pricingZone` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prodZonePricing`
--

DROP TABLE IF EXISTS `prodZonePricing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prodZonePricing` (
  `pluId` int NOT NULL,
  `zoneId` varchar(36) NOT NULL,
  `price` int unsigned NOT NULL,
  `discountPrice` int unsigned NOT NULL,
  PRIMARY KEY (`pluId`,`zoneId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prods`
--

DROP TABLE IF EXISTS `prods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prods` (
  `prID` int NOT NULL AUTO_INCREMENT,
  `CaNum` int DEFAULT NULL,
  `DLXNum` int DEFAULT NULL,
  `LCNum` int DEFAULT NULL,
  `LLID` int DEFAULT NULL,
  `LPNum` int DEFAULT NULL,
  `LXNum` int DEFAULT NULL,
  `LbNum1` int DEFAULT NULL,
  `LbNum2` int DEFAULT NULL,
  `LbNum3` int DEFAULT NULL,
  `MaNum` int DEFAULT NULL,
  `NFNum` int DEFAULT NULL,
  `NtNum` int DEFAULT NULL,
  `PrBarPref` int DEFAULT NULL,
  `PrBarProN` int DEFAULT NULL,
  `PrBarVenN` int DEFAULT NULL,
  `PrBnsPts` int DEFAULT NULL,
  `PrByCnt` int DEFAULT NULL,
  `PrCOLReq` bit(1) DEFAULT NULL,
  `PrCOLTN` varchar(255) DEFAULT NULL,
  `PrCdNm1` varchar(255) DEFAULT NULL,
  `PrCdNm2` varchar(255) DEFAULT NULL,
  `PrCutTestMeatSpecSheetGroupNum` int DEFAULT NULL,
  `PrDesc` varchar(255) DEFAULT NULL,
  `PrDscMe` int DEFAULT NULL,
  `PrDscPr1` int DEFAULT NULL,
  `PrDscPr2` int DEFAULT NULL,
  `PrDscPr3` int DEFAULT NULL,
  `PrEANCd` varchar(255) DEFAULT NULL,
  `PrExcPr` int DEFAULT NULL,
  `PrGrNm1` varchar(255) DEFAULT NULL,
  `PrGrNm2` varchar(255) DEFAULT NULL,
  `PrGrNm3` varchar(255) DEFAULT NULL,
  `PrGrNm4` varchar(255) DEFAULT NULL,
  `PrImage` varchar(255) DEFAULT NULL,
  `PrInMode` int DEFAULT NULL,
  `PrIsFrcBC` bit(1) DEFAULT NULL,
  `PrIsFrcPr` bit(1) DEFAULT NULL,
  `PrIsFrcTr` bit(1) DEFAULT NULL,
  `PrIsPrnPL` bit(1) DEFAULT NULL,
  `PrIsPrnPO` bit(1) DEFAULT NULL,
  `PrIsPrnSL` bit(1) DEFAULT NULL,
  `PrIsUsSL` bit(1) DEFAULT NULL,
  `PrLblPlc1` int DEFAULT NULL,
  `PrLblPlc2` int DEFAULT NULL,
  `PrLblPlc3` int DEFAULT NULL,
  `PrLblRot1` int DEFAULT NULL,
  `PrLblRot2` int DEFAULT NULL,
  `PrLblRot3` int DEFAULT NULL,
  `PrManualLabelTypeNum1` int DEFAULT NULL,
  `PrManualLabelTypeNum2` int DEFAULT NULL,
  `PrManualLabelTypeNum3` int DEFAULT NULL,
  `PrMealNumSidesIncluded` int DEFAULT NULL,
  `PrMealSideItemsAreFree` bit(1) DEFAULT NULL,
  `PrMealSideItemsGroupNum` int DEFAULT NULL,
  `PrName1` varchar(255) DEFAULT NULL,
  `PrName2` varchar(255) DEFAULT NULL,
  `PrNetWt` int DEFAULT NULL,
  `PrOnlineOrderingLabelTypeNum1` int DEFAULT NULL,
  `PrOnlineOrderingLabelTypeNum2` int DEFAULT NULL,
  `PrOnlineOrderingLabelTypeNum3` int DEFAULT NULL,
  `PrPick5LabelTypeNum1` int DEFAULT NULL,
  `PrPick5LabelTypeNum2` int DEFAULT NULL,
  `PrPick5LabelTypeNum3` int DEFAULT NULL,
  `PrPrMod` int DEFAULT NULL,
  `PrPrPlT` int DEFAULT NULL,
  `PrPrepackLabelTypeNum1` int DEFAULT NULL,
  `PrPrepackLabelTypeNum2` int DEFAULT NULL,
  `PrPrepackLabelTypeNum3` int DEFAULT NULL,
  `PrPrice` int DEFAULT NULL,
  `PrProdEntryLabelTypeNum1` int DEFAULT NULL,
  `PrProdEntryLabelTypeNum2` int DEFAULT NULL,
  `PrProdEntryLabelTypeNum3` int DEFAULT NULL,
  `PrProdLfD` int DEFAULT NULL,
  `PrProdLfH` int DEFAULT NULL,
  `PrProdLfM` int DEFAULT NULL,
  `PrPrpTare` int DEFAULT NULL,
  `PrPrtns` int DEFAULT NULL,
  `PrRawBarC` varchar(255) DEFAULT NULL,
  `PrSelfServLabelTypeNum1` int DEFAULT NULL,
  `PrSelfServLabelTypeNum2` int DEFAULT NULL,
  `PrSelfServLabelTypeNum3` int DEFAULT NULL,
  `PrShlfLfD` int DEFAULT NULL,
  `PrShlfLfH` int DEFAULT NULL,
  `PrShlfLfM` int DEFAULT NULL,
  `PrSpcSD` datetime(6) DEFAULT NULL,
  `PrTare` int DEFAULT NULL,
  `PrTareB` int DEFAULT NULL,
  `PrTotalPriceMax` int DEFAULT NULL,
  `PrTotalPriceMin` int DEFAULT NULL,
  `PrType` int DEFAULT NULL,
  `PrVCOLTxt` bit(1) DEFAULT NULL,
  `PrWeightMax` int DEFAULT NULL,
  `PrWeightMin` int DEFAULT NULL,
  `ProdTxt5Num` int DEFAULT NULL,
  `ProdTxt6Num` int DEFAULT NULL,
  `ProdTxt7Num` int DEFAULT NULL,
  `ProdTxt8Num` int DEFAULT NULL,
  `ProdTxt9Num` int DEFAULT NULL,
  `SMNum` int DEFAULT NULL,
  `SPNum` int DEFAULT NULL,
  `TxNum` int DEFAULT NULL,
  `ClNum` int DEFAULT NULL,
  `DeptNum` int DEFAULT NULL,
  `PrNum` int DEFAULT NULL,
  `zoneId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`prID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productTexts`
--

DROP TABLE IF EXISTS `productTexts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productTexts` (
  `deptNum` int NOT NULL,
  `prodTextNum` int NOT NULL,
  `prodTextType` int NOT NULL,
  `ProdTextDesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deptNum`,`prodTextNum`,`prodTextType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile` (
  `profileId` varchar(36) NOT NULL,
  `deleteSyncConfig` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `lastUpdate` datetime(6) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `requiresDomainId` varchar(255) DEFAULT NULL,
  `zoneId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`profileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES ('00000000-0000-0000-0000-000000000000',NULL,'Initial Profile assigned to new scales','2024-10-14 20:33:55.900000','Default Profile','87588758-8758-8758-8758-875887588758',NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `region` (
  `regionId` varchar(36) NOT NULL,
  `bannerId` varchar(36) DEFAULT NULL,
  `regionName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`regionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scaleDevice`
--

DROP TABLE IF EXISTS `scaleDevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scaleDevice` (
  `deviceId` varchar(255) NOT NULL,
  `application` varchar(255) DEFAULT NULL,
  `buildNumber` varchar(255) DEFAULT NULL,
  `countryCode` varchar(255) DEFAULT NULL,
  `deptId` varchar(255) DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `isPrimaryScale` bit(1) NOT NULL,
  `lastReportTimestampUtc` datetime(6) DEFAULT NULL,
  `lastTriggerType` varchar(255) DEFAULT NULL,
  `loader` varchar(255) DEFAULT NULL,
  `operatingSystem` varchar(255) DEFAULT NULL,
  `pluCount` int DEFAULT NULL,
  `profileId` varchar(255) DEFAULT NULL,
  `scaleModel` varchar(255) DEFAULT NULL,
  `serialNumber` bigint NOT NULL,
  `smBackend` varchar(255) DEFAULT NULL,
  `storeId` varchar(255) DEFAULT NULL,
  `systemController` varchar(255) DEFAULT NULL,
  `totalLabelsPrinted` int DEFAULT NULL,
  `priority` int unsigned DEFAULT NULL,
  `healthInfoLog` varchar(12500) DEFAULT NULL,
  `profileRecentlyUpdated` bit(1) DEFAULT NULL,
  `hasChildren` bit(1) DEFAULT NULL,
  PRIMARY KEY (`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scaleSettings`
--

DROP TABLE IF EXISTS `scaleSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scaleSettings` (
  `deviceId` varchar(255) NOT NULL,
  `appHookVersion` varchar(255) DEFAULT NULL,
  `currentFiles` text,
  `features` varchar(255) DEFAULT NULL,
  `hasCustomerDisplay` bit(1) NOT NULL,
  `hasLoadCell` bit(1) NOT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `labelStockSize` varchar(255) DEFAULT NULL,
  `lastCommunicationTimestampUtc` datetime(6) DEFAULT NULL,
  `lastFetchPluOffsetTimestampUtc` datetime(6) DEFAULT NULL,
  `lastRebootTimestampUtc` datetime(6) DEFAULT NULL,
  `licenses` varchar(1000) DEFAULT NULL,
  `scaleItemEntryMode` varchar(255) DEFAULT NULL,
  `scaleMode` varchar(255) DEFAULT NULL,
  `storeGraphicName` varchar(255) DEFAULT NULL,
  `storeInformation` varchar(255) DEFAULT NULL,
  `timeKeeper` varchar(255) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `weigherCalibratedTimestampUtc` datetime(6) DEFAULT NULL,
  `weigherConfiguredTimestampUtc` datetime(6) DEFAULT NULL,
  `weigherPrecision` varchar(255) DEFAULT NULL,
  `macAddress` varchar(255) DEFAULT NULL,
  `frequency` varchar(255) DEFAULT NULL,
  `dayOfWeek` varchar(15) DEFAULT NULL,
  `timeOfDay` varchar(255) DEFAULT NULL,
  `antiMeridian` varchar(255) DEFAULT NULL,
  `timestamp` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`deviceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `id` varchar(255) NOT NULL,
  `buildDate` varchar(255) DEFAULT NULL,
  `databaseId` varchar(255) DEFAULT NULL,
  `lastRestart` datetime(6) DEFAULT NULL,
  `operationMode` varchar(255) DEFAULT NULL,
  `serviceName` varchar(255) DEFAULT NULL,
  `serviceType` int DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store` (
  `storeId` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `customerStoreNumber` varchar(255) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  `lastCommunicationTimestampUtc` datetime(6) DEFAULT NULL,
  `lastFetchPluOffsetTimestampUtc` datetime(6) DEFAULT NULL,
  `lastItemChangeReceivedTimestampUtc` datetime(6) DEFAULT NULL,
  `lastRebootTimestampUtc` datetime(6) DEFAULT NULL,
  `regionId` varchar(255) DEFAULT NULL,
  `sendStoreInfo` varchar(255) DEFAULT NULL,
  `storeName` varchar(255) DEFAULT NULL,
  `triggerType` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `zoneId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`storeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storeDeptPairs`
--

DROP TABLE IF EXISTS `storeDeptPairs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storeDeptPairs` (
  `storeId` varchar(255) NOT NULL,
  `deptId` varchar(255) NOT NULL,
  PRIMARY KEY (`storeId`,`deptId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storeStatusLog`
--

DROP TABLE IF EXISTS `storeStatusLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storeStatusLog` (
  `timestamp` timestamp(3) NOT NULL,
  `deleteEvent` bit(1) NOT NULL,
  `event1` bit(1) NOT NULL,
  `event2` bit(1) NOT NULL,
  `event3` bit(1) NOT NULL,
  `inSync` bit(1) NOT NULL,
  `storeUuid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_service`
--

DROP TABLE IF EXISTS `store_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_service` (
  `store_storeId` varchar(255) NOT NULL,
  `services_id` varchar(255) NOT NULL,
  UNIQUE KEY `UK_mby0tx1yc2p6aw0lyo3k5hr6a` (`services_id`),
  KEY `FKbeafncw3vwjrwsb34ra2qiorf` (`store_storeId`),
  CONSTRAINT `FKbeafncw3vwjrwsb34ra2qiorf` FOREIGN KEY (`store_storeId`) REFERENCES `store` (`storeId`),
  CONSTRAINT `FKnba6oakbvj7c7f5cqj07j13fb` FOREIGN KEY (`services_id`) REFERENCES `service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trans`
--

DROP TABLE IF EXISTS `trans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trans` (
  `TransID` int NOT NULL AUTO_INCREMENT,
  `TTNum` int DEFAULT NULL,
  `TrAPrdt` int DEFAULT NULL,
  `TrBarNum` varchar(255) DEFAULT NULL,
  `TrBarPref` int DEFAULT NULL,
  `TrCOOLF` bit(1) DEFAULT NULL,
  `TrCOTrTxt` varchar(255) DEFAULT NULL,
  `TrCOTxt` varchar(255) DEFAULT NULL,
  `TrClNum` int DEFAULT NULL,
  `TrExPr` int DEFAULT NULL,
  `TrFnlPr` int DEFAULT NULL,
  `TrFnlTare` int DEFAULT NULL,
  `TrGrp` int DEFAULT NULL,
  `TrID` int NOT NULL,
  `TrModeChange` int DEFAULT NULL,
  `TrNmPck` int DEFAULT NULL,
  `TrOBarDig` int DEFAULT NULL,
  `TrOByCount` int DEFAULT NULL,
  `TrOFnPr` int DEFAULT NULL,
  `TrOFxWt` int DEFAULT NULL,
  `TrOPrLfD` int DEFAULT NULL,
  `TrOPrLfH` int DEFAULT NULL,
  `TrOPrLfM` int DEFAULT NULL,
  `TrOSfLfD` int DEFAULT NULL,
  `TrOSfLfH` int DEFAULT NULL,
  `TrOSfLfM` int DEFAULT NULL,
  `TrOTare` int DEFAULT NULL,
  `TrOUnPr` int DEFAULT NULL,
  `TrOpChg` int DEFAULT NULL,
  `TrOpName` varchar(255) DEFAULT NULL,
  `TrPrLfD` int DEFAULT NULL,
  `TrPrLfH` int DEFAULT NULL,
  `TrPrLfM` int DEFAULT NULL,
  `TrPrMod` int DEFAULT NULL,
  `TrPrPlT` int DEFAULT NULL,
  `TrRdClr` int DEFAULT NULL,
  `TrScID` int NOT NULL,
  `TrSecLb` bit(1) DEFAULT NULL,
  `TrSfLfD` int DEFAULT NULL,
  `TrSfLfH` int DEFAULT NULL,
  `TrSfLfM` int DEFAULT NULL,
  `TrSweetheartWeight` int DEFAULT NULL,
  `TrUTNum` int DEFAULT NULL,
  `TrUTPercentOfPrimalCost` int DEFAULT NULL,
  `TrUTPercentOfPrimalWeight` int DEFAULT NULL,
  `TrUnPr` int DEFAULT NULL,
  `TrVoid` int DEFAULT NULL,
  `deptNum` int DEFAULT NULL,
  `scaleId` varchar(255) DEFAULT NULL,
  `trByCount` int DEFAULT NULL,
  `trDtTm` datetime(6) DEFAULT NULL,
  `trFnlNtWt` int DEFAULT NULL,
  `trFxWt` int DEFAULT NULL,
  `trGrNm1` varchar(255) DEFAULT NULL,
  `trGrNm2` varchar(255) DEFAULT NULL,
  `trGrNm3` varchar(255) DEFAULT NULL,
  `trGrNm4` varchar(255) DEFAULT NULL,
  `trLab1` int DEFAULT NULL,
  `trLab2` int DEFAULT NULL,
  `trLab3` int DEFAULT NULL,
  `trLab4` int DEFAULT NULL,
  `trOGrNm1` varchar(255) DEFAULT NULL,
  `trOGrNm2` varchar(255) DEFAULT NULL,
  `trOGrNm3` varchar(255) DEFAULT NULL,
  `trOGrNm4` varchar(255) DEFAULT NULL,
  `trOLab1` int DEFAULT NULL,
  `trOLab2` int DEFAULT NULL,
  `trOLab3` int DEFAULT NULL,
  `trOLab4` int DEFAULT NULL,
  `trOTtVl` int DEFAULT NULL,
  `trOpNum` varchar(255) DEFAULT NULL,
  `trOpRFID` varchar(255) DEFAULT NULL,
  `trPrDes` varchar(255) DEFAULT NULL,
  `trPrNum` int DEFAULT NULL,
  `trPrTare` int DEFAULT NULL,
  `trPrTyp` int DEFAULT NULL,
  `trTare` int DEFAULT NULL,
  `trTtVal` int DEFAULT NULL,
  PRIMARY KEY (`TransID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `upgradeBatch`
--

DROP TABLE IF EXISTS `upgradeBatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `upgradeBatch` (
  `batchId` varchar(255) NOT NULL,
  `fileId` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `uploadStart` datetime(6) NOT NULL,
  `uploadEnd` datetime(6) NOT NULL,
  `upgradeStart` datetime(6) NOT NULL,
  `upgradeEnd` datetime(6) NOT NULL,
  `name` varchar(255) NOT NULL,
  `afterUpgradeCheckIn` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`batchId`),
  UNIQUE KEY `batchId_UNIQUE` (`batchId`),
  KEY `fileId_idx` (`fileId`),
  CONSTRAINT `fileId` FOREIGN KEY (`fileId`) REFERENCES `hobfile` (`fileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `upgradeDevice`
--

DROP TABLE IF EXISTS `upgradeDevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `upgradeDevice` (
  `upgradeId` varchar(255) NOT NULL,
  `deviceId` varchar(255) NOT NULL,
  `batchId` varchar(255) DEFAULT NULL,
  `lastUpdate` datetime(6) DEFAULT NULL,
  `scaleModel` varchar(255) DEFAULT NULL,
  `application` varchar(30) DEFAULT NULL,
  `bootloader` varchar(45) DEFAULT NULL,
  `fpc` varchar(20) DEFAULT NULL,
  `status` enum('unassigned','awaiting_upload','awaiting_primary','ready_to_upload','upload_in_progress','upload_completed_no_errors','upload_completed_errors','upload_failed','upload_now','file_uploaded_awaiting_upgrade','ready_to_upgrade','upgrade_in_progress','post_install_process','upgrade_completed_no_errors','upgrade_completed_errors','upgrade_failed','upgrade_now') NOT NULL DEFAULT 'unassigned',
  `statusText` varchar(255) DEFAULT NULL,
  `nextCheck` datetime DEFAULT NULL,
  `groupId` varchar(50) NOT NULL,
  `fileSelected` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`upgradeId`),
  UNIQUE KEY `upgradeId_UNIQUE` (`upgradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `upgradeGroup`
--

DROP TABLE IF EXISTS `upgradeGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `upgradeGroup` (
  `groupId` varchar(255) NOT NULL,
  `groupName` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` enum('ENABLED','DISABLED','COMPLETED') NOT NULL DEFAULT 'DISABLED',
  `batchGroup` bit(1) NOT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupId_UNIQUE` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-10-14 16:57:18
