package com.hobart.hte.storeserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@EntityScan(basePackages = {"com.hobart.hte.utils"})
public class HteStoreServerApplication extends SpringBootServletInitializer {
	private static final Logger log = LogManager.getLogger(HteStoreServerApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(HteStoreServerApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(HteStoreServerApplication.class);
	}
}
