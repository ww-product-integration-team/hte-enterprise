package com.hobart.hte.storeserver.repositories;

import com.hobart.hte.utils.event.DeviceStatusEntry;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;


public interface DeviceStatusLogRepo extends CrudRepository<DeviceStatusEntry, Timestamp> {

}
