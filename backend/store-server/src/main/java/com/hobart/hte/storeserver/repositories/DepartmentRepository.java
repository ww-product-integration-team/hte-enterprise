package com.hobart.hte.storeserver.repositories;


import com.hobart.hte.utils.dept.Department;
import org.springframework.data.repository.CrudRepository;

public interface DepartmentRepository extends CrudRepository<Department, String> {

    Boolean existsByDeptName1IgnoreCase(String name);
}
