package com.hobart.hte.storeserver.controller;

import com.hobart.hte.storeserver.repositories.ConfigAppRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartupBean {

    public static String projectVersion = "1.0.2";
    public static String buildDate = "01/22/2024";

    private static final Logger log = LogManager.getLogger(StartupBean.class);

    @Autowired
    AboutService aboutService;

    @Autowired
    ConfigAppRepository configRepo;

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {

        //Setup the log location
        ConfigEntry logPath = configRepo.findById(AppConfig.logRootPath).orElse(null);
        if(logPath != null && logPath.getCoValue() != null){
            System.setProperty("APP_LOG_ROOT", logPath.getCoValue());
            ((org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false)).reconfigure();
        }

        aboutService.getServiceInfo(true);

        log.info("   _____ _                   __  __                                   ");
        log.info("  / ____| |                 |  \\/  |                                  ");
        log.info(" | (___ | |_ ___  _ __ ___  | \\  / | __ _ _ __   __ _  __ _  ___ _ __ ");
        log.info("  \\___ \\| __/ _ \\| '__/ _ \\ | |\\/| |/ _` | '_ \\ / _` |/ _` |/ _ \\ '__|");
        log.info("  ____) | || (_) | | |  __/ | |  | | (_| | | | | (_| | (_| |  __/ |   ");
        log.info(" |_____/ \\__\\___/|_|  \\___| |_|  |_|\\__,_|_| |_|\\__,_|\\__, |\\___|_|   ");
        log.info(" ====================================================  __/ | =============");
        log.info(" ITW FEG Hobart 2023(c)                               |___/   (v"+projectVersion +")");
        log.info("[Build Date: " + buildDate + "]");
    }
}