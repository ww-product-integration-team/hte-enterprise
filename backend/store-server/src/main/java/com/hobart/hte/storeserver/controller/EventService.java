package com.hobart.hte.storeserver.controller;


import com.hobart.hte.storeserver.repositories.*;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.config.ConfigSyncEvent;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.dept.DepartmentEvent;
import com.hobart.hte.utils.device.DeviceEvent;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.profile.ProfileEvent;
import com.hobart.hte.utils.profile.ProfileResponse;
import com.hobart.hte.utils.service.ServiceEvent;
import com.hobart.hte.utils.service.ServiceType;
import com.hobart.hte.utils.store.StoreEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class EventService {
	private static final Logger log = LogManager.getLogger(EventService.class);

	@Autowired
	DeviceRepository devRepo;

	@Autowired
	DepartmentRepository deptRepo;

	@Autowired
	ProfileRepository profRepo;

	@Autowired
	FileRepository fileRepo;

	@Autowired
	ConfigAppRepository configRepo;

	@Autowired
	ServiceRepository serviceRepo;

	public ArrayList<StoreEvent> getEvents(String storeId) {

		ArrayList<StoreEvent> toReturn = new ArrayList<StoreEvent>();

		log.info("Processing devices for the store: {}", storeId);
		List<ScaleDevice> listOfScales = devRepo.findByStoreId(storeId);

		List<String> depts = new ArrayList<String>();
		List<String> prof = new ArrayList<String>();
		List<ScaleDevice> devices = new ArrayList<>();

		for (ScaleDevice s : listOfScales) {
			if (!depts.contains(s.getDeptId())) {
				depts.add(s.getDeptId());
			}

			if (!prof.contains(s.getProfileId())) {
				prof.add(s.getProfileId());
			}

			//Don't send the invisible scale down to the store
			if(!s.isInvisibleScale()){
				devices.add(s);
			}
		}

		DeviceEvent deviceEvent = new DeviceEvent();
		deviceEvent.setScales(devices);
		toReturn.add(deviceEvent);

		log.info("Processing departments for the store: {}", storeId);
		if (!depts.isEmpty()) {
			Iterable<Department> findAllById = deptRepo.findAllById(depts);
			List<Department> result = new ArrayList<Department>();
			for (Department d : findAllById) {
				result.add(d);
			}
			DepartmentEvent dptEvent = new DepartmentEvent();
			dptEvent.setDepartments(result);
			toReturn.add(dptEvent);
		}

		log.info("Processing profiles for the store: {}", storeId);
		if (!prof.isEmpty()) {
			Iterable<Profile> findAllById = profRepo.findAllById(prof);
			List<ProfileResponse> result = new ArrayList<ProfileResponse>();
			for (Profile p : findAllById) {
				result.add(new ProfileResponse(p, validateListOfFiles(p.getProfileId()),
						getServerUrl()));
			}
			ProfileEvent prfEvent = new ProfileEvent();

			prfEvent.setProfiles(result);
			toReturn.add(prfEvent);
		}

		toReturn.add(getSyncConfigs());
		toReturn.add(getLocalServices());

		return toReturn;
	}

	private ConfigSyncEvent getSyncConfigs(){
		List<ConfigEntry> configsToSync = new ArrayList<>();
		ConfigSyncEvent configSyncEvent = new ConfigSyncEvent();

		configRepo.findById(AppConfig.scaleDeleteDay).ifPresent(configsToSync::add);
		configRepo.findById(AppConfig.scaleDeleteInterval).ifPresent(configsToSync::add);
		configRepo.findById(AppConfig.scaleDeleteItems).ifPresent(configsToSync::add);
		configRepo.findById(AppConfig.scaleDeleteTime).ifPresent(configsToSync::add);
		configRepo.findById(AppConfig.scaleAwayHours).ifPresent(configsToSync::add);
		configRepo.findById(AppConfig.scaleOfflineHours).ifPresent(configsToSync::add);
		configsToSync.add(new ConfigEntry(AppConfig.heartbeatOperation, "STORE"));

		configSyncEvent.setConfigs(configsToSync);
		return configSyncEvent;
	}

	private ServiceEvent getLocalServices(){
		ServiceEvent serviceEvent = new ServiceEvent();

		ConfigEntry databaseId = configRepo.findById(AppConfig.databaseId).orElse(null);
		if(databaseId == null || databaseId.getCoValue().isEmpty() || databaseId.getCoValue() == null){
			log.error("No Valid Database ID found!");
			return null;
		}


		List<com.hobart.hte.utils.service.Service> localServices = serviceRepo.findByDatabaseId(databaseId.getCoValue());
		serviceEvent.setServices(localServices);

		return serviceEvent;
	}

	public HashMap<String, FileForEvent> getRawFileList(String storeId){
		List<ScaleDevice> listOfScales = devRepo.findByStoreId(storeId);
		List<String> prof = new ArrayList<String>();
		HashMap<String, FileForEvent> fileMap = new HashMap<>();

		for (ScaleDevice s : listOfScales) {
			if (!prof.contains(s.getProfileId())) {
				prof.add(s.getProfileId());
			}
		}

		log.info("Processing profiles for the store: {}", storeId);
		if (!prof.isEmpty()) {
			Iterable<Profile> findAllById = profRepo.findAllById(prof);
			List<FileForEvent> result = new ArrayList<>();
			for (Profile p : findAllById) {
				List<FileForEvent> profileFiles = validateListOfFiles(p.getProfileId());
				if(profileFiles != null){
					for(FileForEvent file : profileFiles){
						fileMap.put(file.getFileId(), file);
					}
				}


			}
		}

		return fileMap;
	}

	private List<FileForEvent> validateListOfFiles(String profileId) {
		log.debug("getting files for profile id: {}", profileId);
		List<FileForEvent> allFilesInProfile = fileRepo.findByProfileIdAndEnabled(profileId, true);
		List<FileForEvent> allFiles = fileRepo.findByProfileId(profileId);
		List<FileForEvent> newList = new ArrayList<>();

		if (allFilesInProfile.isEmpty()) {
			return null;
		}

		Timestamp now = Timestamp.from(Instant.now());

		for (FileForEvent ffe : allFilesInProfile) {
			if ((ffe.getStartDate() == null && ffe.getEndDate() == null)
					|| (ffe.getStartDate().before(now) && ffe.getEndDate() == null)
					|| (ffe.getStartDate().before(now) && ffe.getEndDate().after(now))) {
				// log.info("file is available to use, file= " + ffe.getFilename());
				newList.add(ffe);
			}
		}

		return newList;
	}

	/**
	 *
	 * @return
	 */
	public String getServerUrl() {
		final String fileMgrEndpoint = "filemgr/download";

		ConfigEntry databaseId = configRepo.findById(AppConfig.databaseId).orElse(null);
		if(databaseId == null || databaseId.getCoValue().isEmpty() || databaseId.getCoValue() == null){
			log.error("No Valid Database ID found!");
			return "Unknown";
		}

		List<com.hobart.hte.utils.service.Service> service = serviceRepo.findByDatabaseIdAndServiceType(databaseId.getCoValue(), ServiceType.HEARTBEAT);
		if(service.size() == 0) {
			log.error("No Heartbeat Service Found!");
			return "Unknown";
		}

		return service.get(0).getUrl() + fileMgrEndpoint;
	}
}
