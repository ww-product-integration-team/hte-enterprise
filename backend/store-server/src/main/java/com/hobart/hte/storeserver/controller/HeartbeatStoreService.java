package com.hobart.hte.storeserver.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hobart.hte.storeserver.repositories.*;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.event.DeviceStatusEntry;
import com.hobart.hte.utils.event.EventConfig;
import com.hobart.hte.utils.event.LogEvent;
import com.hobart.hte.utils.event.StoreStatusEntry;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.heartbeat.StoreHeartbeatRequest;
import com.hobart.hte.utils.heartbeat.StoreHeartbeatResponse;
import com.hobart.hte.utils.model.*;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.store.StoreEvent;
import org.apache.juli.logging.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/heartbeat")
public class HeartbeatStoreService {

	private static final Logger log = LogManager.getLogger(HeartbeatStoreService.class);

	@Autowired
	StoreRepo storeRepo;

	@Autowired
	EventService eventService;

	@Autowired
	DeviceStatusLogRepo deviceLogRepo;

	@Autowired
	StoreStatusLogRepository storeLogRepo;

	@Autowired
	DeviceRepository deviceRepo;

	@Autowired
	LogService logService;

	@Autowired
	ServiceRepository serviceRepo;

	@PostMapping(path = "/", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> receiveStoreHeartbeat(@RequestBody StoreHeartbeatRequest shbr) {

		/*-
		 * 1. find store in database
		 * 2. If the store is NOT found return error message
		 * 3. If the store is found,
		 * 4. Update recent store info from store server
		 * 5. retrieve list of departments
		 * 6. Retrieve list of profiles associated
		 * 7. Return list to store client
		 * 8. if the store sent log data, update the db
		 */

		log.info("--==***==--");
		log.info("store reporting: {}", shbr.toString());
//		log.info("store IP: {}", shbr.getIpAddress());
//		log.info("trigger type: {}", shbr.getTriggerType());

		StoreHeartbeatResponse storeHeartbeatResponse = new StoreHeartbeatResponse(shbr);
		Store store;
		boolean storeNotFound = true;

		if (shbr.getStoreId() != null && !shbr.getStoreId().isEmpty()) {
			store = storeRepo.findById(shbr.getStoreId()).orElse(null);
			// find the store in the store table
			if (store != null) {
				storeNotFound = false;
				log.info("store found!");
			}
		} else {
			// find the store by IP address
			log.info(">> >> >> searching by IP address");
			store = storeRepo.findByIpAddress(shbr.getIpAddress());
			if (store != null) {
				storeNotFound = false;
				storeHeartbeatResponse.setStoreId(store.getStoreId());
				log.info("retrieved store using IP address: {}", store.toString());
			} else {
				if (shbr.getHostname() != null && !shbr.getHostname().isEmpty()) {
					store = storeRepo.findByHostname(shbr.getHostname());
					if (store != null) {
						storeNotFound = false;
						storeHeartbeatResponse.setStoreId(store.getStoreId());
						storeHeartbeatResponse.setIpAddress(store.getIpAddress());
						log.info("retrieved store using hostname: {}", store.toString());
					}
				}
			}
		}

		if (storeNotFound) {
			log.info("Store not found {}.", shbr.getIpAddress());
			// nothing to do besides reporting error to the client
			OperationStatusModel result = new OperationStatusModel("Find store",
					"Store not found, store's IP has not been registered with HQ");

			logService.logEvent(EventConfig.StoreNotRegistered, shbr.getIpAddress(), EntityType.STORE,
					"Client with ip " + shbr.getIpAddress() + " requested a store heartbeat, but ip has not been registered!");

			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}

		if(shbr.getStoreId() == null){
			logService.logEvent(EventConfig.NewStoreReported, store.getStoreId(),  EntityType.STORE,
					"Store server " + store.getStoreName() + " completed it's first heartbeat!");
		}
		else{
			logService.logEvent(EventConfig.StoreHeartbeat, store.getStoreId(),  EntityType.STORE,
					"Store completed a heartbeat");
		}


		//Save services
		try{
			serviceRepo.saveAll(shbr.getServices());
		}
		catch (Exception ex){
			log.error("Error saving store services! " + ex.getMessage());
		}

		store.updateStatus(shbr);

		storeRepo.save(store); // protect of any exception
		log.info("store record updated");

		storeHeartbeatResponse.setStoreInfo(store);
		storeHeartbeatResponse.setEventLogs(eventService.getEvents(store.getStoreId()));
		HashMap<String, FileForEvent> HQFiles = eventService.getRawFileList(store.getStoreId());
		List<FileForEvent> storeFiles = shbr.getFiles();
		boolean inSync = true;
		if(storeFiles != null){
			//Iterate through all of the store files
			for(FileForEvent storeFile : storeFiles){
				FileForEvent HqFile = HQFiles.get(storeFile.getFileId());
				//If headquarters doesn't have the file or the checksums are different then the store is out of sync
				//and we don't need to check the rest of the files
				if(HqFile == null || !HqFile.getChecksum().equals(storeFile.getChecksum())){
					inSync = false;
					break;
				}
				else{
					//If there is a match then remove the file from the map
					HQFiles.remove(storeFile.getFileId());
				}
			}
			//If the store is in sync there should not be any files left in the map i.e. matched up all files
			if(!HQFiles.isEmpty()){
				inSync = false;
			}
		}

		//Save the log
		try{
			storeLogRepo.save(new StoreStatusEntry(shbr.getStoreId(), inSync, shbr.isDeleteEvent()));
		}
		catch(Exception e){
			log.error("Error saving store log: " + e.getMessage());
		}

		//Process any events from request
		for(StoreEvent event : shbr.getEventLogs()){
			if (event instanceof LogEvent){
				LogEvent logRsp = processStoreLogs((LogEvent) event);
				storeHeartbeatResponse.addEvent(logRsp);
			}
			else{
				log.error("WARNING - unhandled event type : " + event.toString());
			}
		}

		//Process any recent heartbeats from request
		if(shbr.getLastHeartbeat() != null){
			for(ScaleDevice device : shbr.getLastHeartbeat()){
				ScaleDevice currentDevice = deviceRepo.findById(device.getDeviceId()).orElse(null);
				//If device doesn't exist in the db or store has a more recent version of the device
				if(currentDevice == null || currentDevice.getLastReportTimestampUtc() == null ||
						currentDevice.getLastReportTimestampUtc().compareTo(device.getLastReportTimestampUtc()) < 0){

					/*
					If the scale that that is reporting to the store server does not match the current device's store then
					we shouldn't overwrite store and dept data just latest info about scale
					scale may be assigned elsewhere and is just reporting to the wrong store. The store will not have a record of it and will incorrectly
					place it as unassigned.
					 */
					if(device.getStoreId().equals(AppConfig.DefaultUnassignedId) && currentDevice != null){
						log.info("WARNING: Found mismatched store scale, scale is reporting to a store server to which it does not belong");
						logService.logEvent(EventConfig.WrongStoreAssignment, device.getDeviceId(),  EntityType.SCALE,
								"Found mismatched store scale, scale is reporting to store server " + store.getStoreName() + " to which it does not belong");
						currentDevice.setHostname(device.getHostname());
						currentDevice.setApplication(device.getApplication());
						currentDevice.setBuildNumber(device.getBuildNumber());
						currentDevice.setLoader(device.getLoader());
						currentDevice.setOperatingSystem(device.getOperatingSystem());
						currentDevice.setSystemController(device.getSystemController());
						currentDevice.setSmBackend(device.getSmBackend());
						currentDevice.setLastTriggerType(device.getLastTriggerType());
						currentDevice.setLastReportTimestampUtc(device.getLastReportTimestampUtc());
						currentDevice.setPluCount(device.getPluCount());
						currentDevice.setTotalLabelsPrinted(device.getTotalLabelsPrinted());
						try{
							deviceRepo.save(currentDevice);
						}
						catch (Exception e){
							log.error("Could not save updated device " + currentDevice.getIpAddress() + " : " + e.getMessage());
						}

						continue;
					}

					try{
						deviceRepo.save(device);
					}
					catch (Exception e){
						log.error("Could not save device " + device.getIpAddress() + " : " + e.getMessage());
					}

					log.info("Updating device from last hb : " + device.getHostname());
				}
			}
		}


		log.info("--==***==--");
		return new ResponseEntity<>(storeHeartbeatResponse, HttpStatus.OK);
	}


	private LogEvent processStoreLogs(LogEvent storeLogEvent){
		List<DeviceStatusEntry> storeLogs = storeLogEvent.getLogs();
		List<DeviceStatusEntry> syncedLogs = new ArrayList<>();
		LogEvent ret = new LogEvent();
		for(DeviceStatusEntry logEntry : storeLogs){
			try {
				logEntry.setLogStatus(SyncStatus.SYNCED);
				deviceLogRepo.save(logEntry);
				syncedLogs.add(logEntry);
			}
			catch (Exception e){
				log.error("ERROR - Could not save store log:  " + e.getMessage());
			}

		}

		ret.setLogs(syncedLogs);
		return ret;
	}
}
