package com.hobart.hte.storeserver.repositories;

import com.hobart.hte.utils.event.StoreStatusEntry;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;

public interface StoreStatusLogRepository extends CrudRepository<StoreStatusEntry, Timestamp> {

}