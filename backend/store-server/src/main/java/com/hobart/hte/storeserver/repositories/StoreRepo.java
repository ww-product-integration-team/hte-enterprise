package com.hobart.hte.storeserver.repositories;


import com.hobart.hte.utils.store.Store;
import org.springframework.data.repository.CrudRepository;

public interface StoreRepo extends CrudRepository<Store, String> {
	 Store findByIpAddress(String ipAddress);
	 Store findByHostname(String hostname);
}
