'''
Created on Mar 16, 2016
@author: lopezaz
'''

#from sftpProtocol import SFTPClient, SCPError
import paramiko
import traceback
import os

upgrade_directory = '~upgrade_directory~'

#def displayFiles():
#    # files = os.listdir('/')
#    files = os.listdir(upgrade_directory)
#    print(files)
#    for f in files:
#        if f.startswith('hti-'):
#            # print(str(f) + ',' + str(os.stat('/'+f).st_size))
#            print(str(f) + ',' + str(os.stat(upgrade_directory+'/'+f).st_size))
#
#def download():
#    
#    files = ['hti-linux-feature-rdp-2.7.6-07212017_150805.package.tgz']
#    
#    hostname = "10.3.176.62"
#    password = "admin8758"
#    username = "hobart"
#    
#    try:
#        sftp = SFTPClient(username=username, password=password, host=hostname)
#        log = open('sftp.log','w')
#        print("getting files...")
#        for f in files:
#            try:
#                results = sftp.downloadFile('/'+f, '/'+f)
#
#                log.write(f)
#                log.write(',')
#                log.write(results.split('|')[0])
#                log.write('\n')
#            except Exception as ex:
#                errlog = open("sftp_error.txt","w")
#                errlog.write('*** Caught exception: %s: %s' % (ex.__class__, ex))
#        print("download finished!")
#        displayFiles()
#        log.close()
#    except Exception as e:
#        print('*** Caught exception: %s: %s' % (e.__class__, e))
#        errlog = open("sftp_error.txt","w")
#        errlog.write(e)
#        errlog.write(traceback.print_exc())

def upload():
    file = '~filename~'
    hostnames = ["~host~"]
    password = "~password~"
    username = "~username~"

    try:
        log = open('soft.log', 'w')
        for h in hostnames:
            print("connecting to secondary scale: " + h)
            #sftp = SFTPClient(username=username, password=password, host=h)
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=h, username=username, password=password)
            print("sending files...")
            #results = sftp.uploadFile(upgrade_directory+'/'+file, upgrade_directory+'/'+file)
            sftp = client.open_sftp()
            results = sftp.put(upgrade_directory+'/'+file, upgrade_directory+'/'+file)
            local_file_size = os.stat(upgrade_directory+'/'+file)

            log.write(file + ',')
            #log.write(results.split('|')[0])
            log.write('local size: ' + str(local_file_size.st_size) + ',')
            log.write('remote size: ' + str(results.st_size))
            log.write('\n')

            #print(sftp.listFiles(upgrade_directory))
            print(os.listdir(upgrade_directory))
        print("upload finished!")
        log.close()
    except Exception as e:
        print("*** Caught exception: %s: %s" % (e.__class__, e))
        errlog = open("sftp_error.txt","w")
        errlog.write("*** Caught exception: %s: %s" % (e.__class__, e))
        errlog.write(e)
        errlog.write(traceback.print_exc())
        errlog.close()

if __name__ == '__main__':
    # download()
    upload()
