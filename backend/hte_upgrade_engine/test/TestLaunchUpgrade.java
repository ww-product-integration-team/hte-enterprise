
import com.hobart.hte.filemgr.QueryManager;
import static com.hobart.hte.filemgr.UpgradeManager.log;
import com.hobart.hte.filemgr.UpgradeScaleThread;
import com.hobart.hte.filemgr.util.MyLogFormatter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 *
 * @author lopezaz
 */
public class TestLaunchUpgrade {

    private QueryManager queryManager;
    private static final Logger logger = Logger.getLogger("testlog");
    private static FileHandler fh;

    public TestLaunchUpgrade() {
        try {
            fh = new FileHandler("testLog.txt", true);
            logger.addHandler(fh);
            fh.setFormatter(new MyLogFormatter());
        } catch (SecurityException | IOException e) {
            System.err.println("Unable to start log: " + e.getMessage());
        }

        Properties properties = new Properties();
        try (InputStream input = new FileInputStream("config.ini")) {
            properties.load(input);
        } catch (IOException e) {
            log(e.getMessage(), e);
            return;
        }

        queryManager = new QueryManager(logger,
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password"),
                Integer.parseInt(properties.getProperty("dbConnectRetries")));

    }

    /**
     *
     */
    private void test() {
        logger.info("--== Launch upgrade test ==--");

        String host = "";

        try {
            logger.info("querying the DB for upgrades to process...");
            ResultSet resultSet = queryManager.getScalesToUpgrade();
            int numResults = 0;

            int maxRetries = queryManager.getConfigurationInt("maxRetries", 2);
            int waitMinutes = queryManager.getConfigurationInt("waitMins", 35);

            while (resultSet.next()) {
                numResults++;

                String upgradeId = resultSet.getString("upgradeId");
                String fileName = resultSet.getString("fileName");
                String checksum = resultSet.getString("checksum");
                host = resultSet.getString("ipAddress");
                LocalDateTime end = resultSet.getTimestamp("upgradeEnd").toLocalDateTime();
                //String batchId = resultSet.getString("batchId");

                logger.info(String.format("[upgrade event] upgradeId: %s\nfile name: %s\nchecksum: %s\nhostname: %s\n",
                        upgradeId, fileName, checksum, host));

                // launching one at a time for testing purposes only 
                UpgradeScaleThread ust = new UpgradeScaleThread(host, fileName, end,
                        upgradeId, logger, queryManager);

                ust.setMaxRetry(maxRetries);
                ust.setWaitMinutes(waitMinutes);

                ust.run();
            }

            logger.info("number of scales returned: " + numResults);

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.severe(String.format("An error occurred while processing upgrade request for: %s ", host));
        }

        logger.info("*** Launch upgrade test end ***");
    }

    public static void main(String args[]) {
        new TestLaunchUpgrade().test();
    }

}
