
import com.hobart.hte.filemgr.util.ScaleCommDriver;
import java.util.logging.Logger;

/**
 *
 * @author lopezaz
 */
public class TestGetVersions {
    
    private Logger logger = Logger.getLogger("test.txt");

    public void test() {
        ScaleCommDriver scale = new ScaleCommDriver(logger);

        String[] versions = scale.retrieveVersions("10.3.128.133");

        for (String t : versions)
            System.out.println(t);

    }

    public static void main(String args[]) {
        new TestGetVersions().test();
    }
}
