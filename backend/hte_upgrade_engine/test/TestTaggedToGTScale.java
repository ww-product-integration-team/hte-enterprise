
import com.hobart.hte.filemgr.tasks.BackupRestoreDelete;
import com.hobart.hte.filemgr.util.ScaleCommDriver;
import java.io.IOException;
import java.util.logging.Logger;

public class TestTaggedToGTScale {
    
    private static final Logger logger = Logger.getLogger("testlog");
    
    public void test() {
//        ScaleCommDriver driver = new ScaleCommDriver();
//        int pluCount = driver.getPLUCount("10.3.128.141", logger);
//        System.out.println("pluCount: " + pluCount);
//        long space = driver.getAvailableSpace("10.3.128.141");
//        System.out.println("available space: " + space);
        BackupRestoreDelete brd = new BackupRestoreDelete();
        boolean backedup = false;
        try {
            backedup = brd.backupScale("10.3.128.141");
        } catch (InterruptedException | IOException ex) {
            System.err.println("whoops a thing went wrong");
            ex.printStackTrace();
        }
        System.out.println("scale backup created: " + backedup);
    }
    
    public static void main(String[] args) {
        new TestTaggedToGTScale().test();
    }
}