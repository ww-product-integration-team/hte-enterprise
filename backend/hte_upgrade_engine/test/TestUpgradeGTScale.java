
import com.hobart.hte.filemgr.ssh.SSHTools;
import com.hobart.hte.filemgr.util.MyLogFormatter;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 *
 * @author patrick.hughes
 */
public class TestUpgradeGTScale {
    
    private static final Logger logger = Logger.getLogger("testlog");
    private static FileHandler fh;
    
    public TestUpgradeGTScale() {
        
        try {
            fh = new FileHandler("C:\\Users\\patrick.hughes\\Documents\\testLog.txt", true);
            logger.addHandler(fh);
            fh.setFormatter(new MyLogFormatter());
        } catch (SecurityException | IOException e) {
            System.err.println("Unable to start log: " + e.getMessage());
        }
    }
    
    private void test() {
        logger.info("Begin testing upgrading gt scale");
        
        String host = "10.3.128.133";
        SSHTools ssh = new SSHTools();
        ssh.setLogger(logger);
        
        logger.info(String.format("[%s] executing upgrade for gt scale...", host));
        String linuxCommand = String.format("python3 /usr/bin/installUpgrade.py "
                + "/opt/hobart/upgrade/gt-application_0.0.45-20240415-214913696_rk3568-hobart-x11.tgz");
        logger.info(String.format("[%s] Executing upgrade, command: %s", host, linuxCommand));
        String stmp = ssh.executeSSHCommandVerbose(host, linuxCommand);
        logger.info(String.format("[%s] message from scale: %s", host, stmp));
        
        logger.info("Finish testing upgrading to gt scale");
    }
    
    public static void main(String args[]) {
        new TestUpgradeGTScale().test();
    }
}
