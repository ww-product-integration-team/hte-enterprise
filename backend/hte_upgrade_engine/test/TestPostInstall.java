
import com.hobart.hte.filemgr.QueryManager;
import static com.hobart.hte.filemgr.UpgradeManager.log;
import com.hobart.hte.filemgr.tasks.PostInstallThread;
import com.hobart.hte.filemgr.util.MyLogFormatter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 *
 * @author lopezaz
 */
public class TestPostInstall {

    private QueryManager queryManager;
    private static final Logger logger = Logger.getLogger("testlog");
    private static FileHandler fh;

    public TestPostInstall() {
        try {
            fh = new FileHandler("testLog.txt", true);
            logger.addHandler(fh);
            fh.setFormatter(new MyLogFormatter());
        } catch (SecurityException | IOException e) {
            System.err.println("Unable to start log: " + e.getMessage());
        }

        Properties properties = new Properties();
        try (InputStream input = new FileInputStream("config.ini")) {
            properties.load(input);
        } catch (IOException e) {
            log(e.getMessage(), e);
            return;
        }

        queryManager = new QueryManager(logger,
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password"),
                Integer.parseInt(properties.getProperty("dbConnectRetries")));

    }

    private String parseVersion(String filename) {
        if (filename == null || filename.isEmpty()) {
            return "N/A";
        }
        String[] split = filename.split("-");

        if (split.length > 4) {
            //String stmp = split[3]; 
            return split[3];
        }
        return "N/A";
    }

    private void test() {
        logger.info("--== Post install test ==--");

        String host="something to avoid an error";
        String upgradeId;

        //logger.info("updating next check in 2 minutes");
        //queryManager.updateScaleNextCheck(host, upgradeId, 2);
        try {
            ResultSet resultSet = queryManager.getScalesPostInstall();
            int numResults = 0;

            while (resultSet.next()) {
                numResults++;
                upgradeId = resultSet.getString("upgradeId");
                host = resultSet.getString("ipAddress");
                String newVersion = parseVersion(resultSet.getString("filename"));
                logger.info(String.format("host: %s, new version: %s", host, newVersion));

                PostInstallThread pit = new PostInstallThread(host, newVersion,
                        upgradeId, logger, queryManager, 1);

                pit.runTask();
            }

            logger.info("records processed: " + numResults);

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.severe(String.format("An error occurred while processing post upgrade request for: %s ", host));
            //logger.severe(ex.toString());
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
        }
        logger.info("*** Post install test done ***");
    }

    public static void main(String args[]) {
        new TestPostInstall().test();
    }
}
