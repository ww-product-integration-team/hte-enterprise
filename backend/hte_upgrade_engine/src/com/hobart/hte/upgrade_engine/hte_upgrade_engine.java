package com.hobart.hte.upgrade_engine;

import com.hobart.hte.filemgr.UpgradeManager;

/**
 *
 * @author lopezaz
 */
public class hte_upgrade_engine {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new UpgradeManager().startUpgradeServices();
    }
    
}
