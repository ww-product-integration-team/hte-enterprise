package com.hobart.hte.filemgr;

import com.hobart.hte.filemgr.ssh.SSHTools;
import com.hobart.hte.filemgr.util.ScaleCommDriver;
import com.hobart.hte.filemgr.util.TaskStatus;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.JSch;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class UploadFileThread implements Runnable {

    private final String host;
    private final String storeId;
    private final String groupId;
    private final String localPath;
    private final String localChecksum;
    private final String localFilePath;
    private final String remoteFilePath;
    private final String fileName;
    private ChannelSftp channelSftp = null;
    private final LocalDateTime endTime;
    private boolean connectionInteruption = false;
    private static final int PORT = 22;
    private static final String USERNAME = "root";
    private static final String PASSWORD = "ho1bart2"; // "\u0068\u006F\u0031\u0062\u0061\u0072\u0074\u0032"
    public Session session = null;
    private final String upgradeId;
    //private UpgradeManager upgradeManager;
    private final Logger _log;
    private final QueryManager _qm;
    private int maxRetry = 2;
    private final String scaleModelType;

    /**
     *
     * @param localChecksum
     * @param host
     * @param storeId
     * @param groupId
     * @param fileName
     * @param endTime
     * @param localPath
     * @param upgradeId
     * @param log
     * @param qm
     */
    public UploadFileThread(String localChecksum, String host, String storeId, String groupId, 
            String fileName, LocalDateTime endTime, String localPath, String upgradeId, Logger log, QueryManager qm) {
        this.localChecksum = localChecksum;
        this.upgradeId = upgradeId;
        this.localPath = localPath;
        this.host = host;
        this.storeId = storeId;
        this.groupId = groupId;
        this.scaleModelType = UpgradeManager.determineScaleModelType(host);
        if (this.scaleModelType.equals("GT")) {
            this.remoteFilePath = "/opt/hobart/upgrade/" + fileName;
        } else {
            this.remoteFilePath = "/usr/local/hobart/upgrade/" + fileName;
        }
        this.localFilePath = Paths.get(localPath, fileName).toString();
        this.fileName = fileName;
        this.endTime = endTime;
        _log = log;
        this._qm = qm;
    }

    /**
     * execute the upload of the given file to the scale and update the status
     * of the upload on HTe database
     */
    @Override
    public void run() {
        int retryCount = 0;
        String msg;
        _qm.updateStatus(upgradeId, host,
                TaskStatus.upload_in_progress.toString(), "uploading, wait...");

        // read and update DB with current firmware versions in the scale
        ScaleCommDriver scale = new ScaleCommDriver(_log);
        String[] versions = scale.retrieveVersions(host);
        _qm.updateScaleVersions(upgradeId, versions);
        
        _log.info(String.format("Remote file path: %s", remoteFilePath));

        while ( //LocalDateTime.now().isBefore(endTime) && 
                retryCount <= maxRetry) {
            try {
                session = establishSession();
                if (scaleModelType.equals("GT")) {
                    uploadFileGT();
                } else {
                    uploadFileHT();
                }
                //retryCount = 0;
                connectionInteruption = false;
                break; // Exit the loop if download is successful
            } catch (JSchException | SftpException e) {
                retryCount++;
                _log.severe(e.getMessage());
                _log.info(String.format("[%s] Connection with remote device is lost. Establishing new connection with the host", host));

                try {
                    Thread.sleep(5000); // Delay for 5 seconds before the next retry
                } catch (InterruptedException ex) {
                    _log.severe(ex.getMessage());
                }
            } catch (FileNotFoundException e) {
                _log.severe(String.format("\nFile not found: %s", remoteFilePath));
                break;
            } catch (IOException e) {
                _log.severe(String.format("\nIO error occurred: %s", e.getMessage()));
                break;
            } finally {
                if (session != null && session.isConnected()) {
                    session.disconnect(); // Disconnect the session if it was established
                }
            }
        }

        if (retryCount > maxRetry) {
            msg = String.format("Unable to form proper connection with host: %s. Please attempt again at a different time.", host);
            _log.warning(msg);
            _qm.updateStatus(upgradeId, host, TaskStatus.upload_failed.name(), msg);
        }
        
        _log.info(String.format("[%s] Exiting upload thread", host));
    }

    /**
     *
     * @param session
     * @param localChecksum
     * @param localFilePath
     * @param remoteFilePath
     * @throws JSchException
     * @throws SftpException
     * @throws IOException
     */
    private void uploadFileHT() throws JSchException, SftpException, FileNotFoundException, IOException {
        _log.info("uploading file to HT scale");
        
        String statusMsg;
        String localPythonPath = "script/hobartUploaderCustom.py";
        String remotePythonPath = "/usr/local/hobart/python/hobartUploaderCustom.py";
        long localPythonSize = 0;
        long remotePythonSize = 0;
        try {
            channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();
            
            if (storeId != null) {
                try {
                    ResultSet secondaries = _qm.getScalesToUploadSecondaries(storeId, groupId);
                    List<String> hosts = new ArrayList<>();
                    
                    while (secondaries.next()) {
                        String scaleIp = secondaries.getString("ipAddress");
                        hosts.add("\"" + scaleIp + "\"");
                    }
                    String[] localFileParts = localFilePath.split("/");
                    _log.info(Arrays.toString(localFileParts));
                    customizePython(localFileParts[localFileParts.length-1], hosts);
                    
                    File pythonFile = new File(localPythonPath);
                    _log.info(String.format("python file: %s", localPythonPath));
                    if (pythonFile.exists()) {
                        _log.info("python uploader file exists");
                    }
                    
                    localPythonSize = pythonFile.length();
                    try {
                        SftpATTRS attrsPython = channelSftp.stat(remotePythonPath);
                        remotePythonSize = attrsPython.getSize();
                    } catch (SftpException ex) {
                        
                    }
                    double pythonPerc = (double) (remotePythonSize * 100 / localPythonSize);
                    UpgradeManager.addHost(host, pythonPerc);
                } catch (SQLException e) {
                    System.out.println("Error:");
                    _log.severe(e.toString());
                }
            }

            File local_file = new File(localFilePath);
            _log.info(String.format("local file: %s", localFilePath));
            if (local_file.exists()) {
                _log.info("local file exists");
            }
            long localFileSize = new File(localFilePath).length();
            long remoteFileSize = 0; // Initialize remoteFileSize

            try {
                SftpATTRS attrs = channelSftp.stat(remoteFilePath);
                remoteFileSize = attrs.getSize();
            } catch (SftpException e) {
                _log.info("File does not exist on remote path");
            }
            double percentage = (double) (remoteFileSize * 100) / localFileSize;

            UpgradeManager.addHost(host, percentage);

            String remoteChecksum;
            CustomProgressMonitor progressMonitor;
            CustomProgressMonitor pythonProgress;
            if (remoteFileSize < localFileSize && localFileSize > 0) {
                //progressMonitor = new CustomProgressMonitor(localFileSize, host, endTime, session);
                progressMonitor = new CustomProgressMonitor(localFileSize, host);
                // Upload the file to the remote device
                _log.info(String.format("[%s] Starting file upload...", host));
                try {
                    channelSftp.put(localFilePath, remoteFilePath, progressMonitor, ChannelSftp.RESUME);
                } catch (SftpException ex) {
                    _log.severe(String.format("[%s] File upload failed, exiting upload thread", host));
                    throw ex;
                }
                _log.info(String.format("[%s] File upload ended.", host));

            } else if (localFileSize == 0) {
                statusMsg = String.format("File: %s does not exist", fileName);
                _log.info(statusMsg);
                _qm.updateStatus(upgradeId, host, TaskStatus.upload_failed.toString(), statusMsg);
            } else {
                _log.info(String.format("%s Already has file: %s. Calculating checksum...", host, fileName));
            }
            if (storeId != null) {
                pythonProgress = new CustomProgressMonitor(localPythonSize, host);
                if (remotePythonSize < localPythonSize && localPythonSize > 0) {
                    _log.info(String.format("local python: [%s] remote python: [%s]", localPythonPath, remotePythonPath));
                    channelSftp.put(localPythonPath, remotePythonPath, pythonProgress, ChannelSftp.RESUME);
                } else if (localPythonSize == 0) {
                    _log.info("File: hobartUploaderCustom.py does not exist locally");
                } else {
                    _log.info("Custom python file already exists on primary scale, deleting...");
                    channelSftp.rm(remotePythonPath);
                    _log.info("Uploading updated python file to primary scale");
                    channelSftp.put(localPythonPath, remotePythonPath, pythonProgress, ChannelSftp.RESUME);
                }
            }

            if (localFileSize != 0) {
                // Should we report this here?
                //_qm.setCompleteStatus(upgradeId, host); //update db
                // Calculate the checksum of the remote file
                remoteChecksum = calculateRemoteChecksum(session, remoteFilePath);
                // Compare the checksums
                if (localChecksum != null && localChecksum.equals(remoteChecksum)) {
                    statusMsg = String.format("File uploaded successfully to %s. Checksums match.", host);
                    _log.info(statusMsg);
                    _qm.updateStatus(upgradeId, host,
                            TaskStatus.file_uploaded_awaiting_upgrade.name(), statusMsg);
                    UpgradeManager.removeHost(host);
                } else if (localChecksum == null | remoteChecksum == null) {
                    _log.info("Calculating checksums...");
                    // is this needed? 
                } else {
                    statusMsg = String.format("[%s] File upload completed. Checksums do not match.", host);
                    _log.info(statusMsg);
                    _qm.updateStatus(upgradeId, host, TaskStatus.upload_completed_errors.toString(), statusMsg);
                    // Delete the remote file
                    //channelSftp.rm(remoteFilePath);
                    // Recursively restart the upload process
                    //uploadFile(session, localChecksum, localFilePath, remoteFilePath);
                    throw new SftpException(0, statusMsg);
                }
            }
            executeSecondaries(remotePythonPath);
//            ExecutorService executor = Executors.newSingleThreadExecutor();
//            executor.submit(() -> {
//                Session newSession = null;
//                ChannelExec ssh = null;
//                try {
//                    _log.info("Begin new thread to upload to ht secondaries");
//                    ResultSet secondaries = _qm.getScalesToUploadSecondaries(storeId, groupId);
//                    while (secondaries.next()) {
//                        String upgradeId = secondaries.getString("upgradeId");
//                        String scaleIp = secondaries.getString("ipAddress");
//                        
//                        _qm.updateStatus(upgradeId, scaleIp, TaskStatus.upload_in_progress.toString(), 
//                                String.format("Receiving upgrade file from primary: %s", host));
//                    }
//                    
//                    _log.info("creating new session");
//                    newSession = establishSession();
//                    _log.info("opening executable channel");
//                    ssh = (ChannelExec) newSession.openChannel("exec");
//                    _log.info("channel opened, setting command to execute");
//                    ssh.setCommand("python " + remotePythonPath);
//                    _log.info("command set, connecting to destination");
//                    ssh.setInputStream(null);
//                    ssh.setErrStream(System.err);
//                    
//                    ByteArrayOutputStream output = new ByteArrayOutputStream();
//                    ByteArrayOutputStream error = new ByteArrayOutputStream();
//                    
//                    InputStream in = ssh.getInputStream();
//                    InputStream err = ssh.getErrStream();
//                    
//                    ssh.connect();
//                    
//                    _log.info("connection established and command should be ran, reading logs");
//                    byte[] tmp = new byte[1024];
//                    while (true) {
//                        _log.info("inside true loop");
//                        while (in.available() > 0) {
//                            _log.info("output reported from python");
//                            int i = in.read(tmp, 0, 1024);
//                            if (i < 0) {
//                                break;
//                            }
//                            output.write(tmp, 0, i);
//                        }
//                        while (err.available() > 0) {
//                            _log.info("error reported from python");
//                            int i = err.read(tmp, 0, 1024);
//                            if (i < 0) {
//                                break;
//                            }
//                            error.write(tmp, 0, i);
//                        }
//                        if (ssh.isClosed()) {
//                            _log.info("channel closed");
//                            if ((in.available() > 0) || (err.available() > 0)) {
//                                continue;
//                            }
//                            _log.info(String.format("exit-status: %s", ssh.getExitStatus()));
//                            break;
//                        }
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException ee) {
//                            _log.warning(ee.toString());// ee.getStackTrace().toString());
//                        }
//                    }
//                    _log.info(String.format("[%s] Normal output: %s", host, output.toString()));
//                    _log.severe(String.format("[%s] Error output: %s", host, error.toString()));
//                    
//                    if (output.toString().contains("exception") || error.toString().contains("exception")) {
//                        throw new JSchException(String.format("An exception occurred while attempting to run the python file:\n%s\n%s", 
//                                output.toString(), error.toString()));
//                    }
//                    
//                    secondaries = _qm.getScalesToUploadSecondaries(storeId, groupId);
//                    while (secondaries.next()) {
//                        String upgradeId = secondaries.getString("upgradeId");
//                        String scaleIp = secondaries.getString("ipAddress");
//                        
//                        _qm.updateStatus(upgradeId, scaleIp, TaskStatus.file_uploaded_awaiting_upgrade.toString(), 
//                                String.format("Upgrade file uploaded from primary: %s", host));
//                    }
//                    
//                    _log.info("Finish new thread to upload to secondaries");
//                } catch (SQLException ex) {
//                    _log.severe("Error: SQLException from reading data from database");
//                } catch (JSchException | IOException ex) {
//                    _log.severe(String.format("Error: SftpException from executing python file to send upgrade file to secondaries.\n%s", 
//                            ex.getMessage()));
//                } finally {
//                    if (newSession != null && newSession.isConnected()) {
//                        newSession.disconnect();
//                    }
//                    if (ssh != null && ssh.isConnected()) {
//                        ssh.disconnect();
//                    }
//                }
//            });
        } finally {
            if (channelSftp != null && channelSftp.isConnected()) {
                channelSftp.disconnect(); // Disconnect the SFTP channel if it was established
            }
        }
    }
    
    private void uploadFileGT() throws JSchException, SftpException, FileNotFoundException, IOException {
        _log.info("uploading file to GT scale");
        
        String statusMsg;
        String localPythonPath = "script/hobartUploaderCustom.py";
        String remotePythonPath = "/opt/hobart/upgrade/hobartUploaderCustom.py";
        long localPythonSize = 0;
        long remotePythonSize = 0;
        
        try {
            channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();
            
            SSHTools ssh = new SSHTools();
            ssh.setLogger(_log);
            ssh.deleteOldFiles(host, remoteFilePath.substring(0, remoteFilePath.lastIndexOf("/")+1),
                    new ArrayList<>(), scaleModelType);
            
            if (storeId != null) {
                try {
                    ResultSet secondaries = _qm.getScalesToUploadSecondaries(storeId, groupId);
                    List<String> hosts = new ArrayList<>();
                    
                    while (secondaries.next()) {
                        String scaleIp = secondaries.getString("ipAddress");
                        hosts.add("\"" + scaleIp + "\"");
                    }
                    String[] localFileParts = localFilePath.split("/");
                    _log.info(Arrays.toString(localFileParts));
                    customizePython(localFileParts[localFileParts.length-1], hosts);
                    
                    File pythonFile = new File(localPythonPath);
                    _log.info(String.format("python file: %s", localPythonPath));
                    if (pythonFile.exists()) {
                        _log.info("python uploader file exists");
                    }
                    
                    localPythonSize = pythonFile.length();
                    try {
                        SftpATTRS attrsPython = channelSftp.stat(remotePythonPath);
                        remotePythonSize = attrsPython.getSize();
                    } catch (SftpException ex) {
                        _log.info(String.format("[%s] python utility file does not exist", host));
                    }
                    double pythonPerc = (double) (remotePythonSize * 100 / localPythonSize);
                    UpgradeManager.addHost(host, pythonPerc);
                } catch (SQLException e) {
                    System.out.println("Error:");
                    _log.severe(e.toString());
                }
            }
            
            File localFile = new File(localFilePath);
            _log.info(String.format("local file: %s", localFilePath));
            if (localFile.exists()) {
                _log.info("local file exists");
            }
            long localFileSize = new File(localFilePath).length();
            long remoteFileSize = 0;
            
            try {
                SftpATTRS attrs = channelSftp.stat(remoteFilePath);
                remoteFileSize = attrs.getSize();
            } catch (SftpException e) {
                _log.info("File does not exist on remote path");
            }
            double percentage = (double) (remoteFileSize * 100) / localFileSize;
            
            UpgradeManager.addHost(host, percentage);
            
            String remoteChecksum;
            CustomProgressMonitor progressMonitor;
            CustomProgressMonitor pythonProgress;
            if (remoteFileSize < localFileSize && localFileSize > 0) {
                progressMonitor = new CustomProgressMonitor(localFileSize, host);
                _log.info(String.format("[%s] Starting file upload...", host));
                try {
                    channelSftp.put(localFilePath, remoteFilePath, progressMonitor, ChannelSftp.RESUME);
                } catch (SftpException ex) {
                    _log.severe(String.format("[%s] File upload failed, exiting upload thread", host));
                    throw ex;
                }
                _log.info(String.format("[%s] File upload ended", host));
            } else if (localFileSize == 0) {
                statusMsg = String.format("File: %s does not exist", fileName);
                _log.info(statusMsg);
                _qm.updateStatus(upgradeId, host, TaskStatus.upload_failed.toString(), statusMsg);
            } else {
                _log.info(String.format("%s Already has file: %s. Calculating checksum...", host, fileName));
            }
            if (storeId != null) {
                pythonProgress = new CustomProgressMonitor(localPythonSize, host);
                if (remotePythonSize < localPythonSize && localPythonSize > 0) {
                    _log.info(String.format("local python: [%s] remote python: [%s]", localPythonPath, remotePythonPath));
                    channelSftp.put(localPythonPath, remotePythonPath, pythonProgress, ChannelSftp.RESUME);
                } else if (localPythonSize == 0) {
                    _log.info("File: hobartUploaderCustom.py does not exist locally");
                } else {
                    _log.info("Custom python file already exists on primary scale, deleting...");
                    channelSftp.rm(remotePythonPath);
                    _log.info("Uploading updated python file to primary scale");
                    channelSftp.put(localPythonPath, remotePythonPath, pythonProgress, ChannelSftp.RESUME);
                }
            }
            
            if (localFileSize != 0) {
                remoteChecksum = calculateRemoteChecksum(session, remoteFilePath);
                if (localChecksum != null && localChecksum.equals(remoteChecksum)) {
                    statusMsg = String.format("File uploaded successfully to %s. Checksums match.", host);
                    _log.info(statusMsg);
                    _qm.updateStatus(upgradeId, host, TaskStatus.file_uploaded_awaiting_upgrade.name(), statusMsg);
                    UpgradeManager.removeHost(host);
                } else if (localChecksum == null | remoteChecksum == null) {
                    _log.info("Calculating checksums...");
                    // is this needed?
                } else {
                    statusMsg = String.format("[%s] File upload completed. Checksums do not match.", host);
                    _log.info(statusMsg);
                    _qm.updateStatus(upgradeId, host, TaskStatus.upload_completed_errors.name(), statusMsg);
                    throw new SftpException(0, statusMsg);
                }
            }
            
            executeSecondaries(remotePythonPath);
        } finally {
            if (channelSftp != null && channelSftp.isConnected()) {
                channelSftp.disconnect();
            }
        }
    }
    
    private void executeSecondaries(String remotePythonPath) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            Session newSession = null;
            ChannelExec exec = null;
            List<String> secondariesStrings = new ArrayList<>();
                _log.info(String.format("Begin new thread to upload to %s secondaries", scaleModelType));
            try {
                _log.info("Reading secondaries information from database");
                ResultSet secondaries = _qm.getScalesToUploadSecondaries(storeId, groupId);
                while (secondaries.next()) {
                    String upgradeId = secondaries.getString("upgradeId");
                    secondariesStrings.add(upgradeId);
                    String scaleIp = secondaries.getString("ipAddress");
                    secondariesStrings.add(scaleIp);

                    _qm.updateStatus(upgradeId, scaleIp, TaskStatus.upload_in_progress.toString(),
                            String.format("Receiving upgrade file from primary %s", host));
                }
            } catch (SQLException ex) {
                _log.severe("Error: SQLException from reading data from database");
            }
            
            SSHTools tools = new SSHTools();
            tools.setLogger(_log);
            for (int i = 0; i < secondariesStrings.size()/2; i++) {
                tools.deleteOldFiles(secondariesStrings.get(i+1), remoteFilePath.substring(0, remoteFilePath.lastIndexOf("/")+1),
                        new ArrayList<>(), scaleModelType);
                i++;
            }
            
            try {

                _log.info("creating new session");
                newSession = establishSession();
                _log.info("opening executable channel");
                exec = (ChannelExec) newSession.openChannel("exec");
                _log.info("channel opened, setting command to execute");
                if (scaleModelType.equals("GT")) {
                    exec.setCommand("python3 " + remotePythonPath);
                } else {
                    exec.setCommand("python " + remotePythonPath);
                }
                _log.info("command set, connecting to destination");
                exec.setInputStream(null);
                exec.setErrStream(System.err);

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                ByteArrayOutputStream error = new ByteArrayOutputStream();

                InputStream in = exec.getInputStream();
                InputStream err = exec.getErrStream();

                exec.connect();

                _log.info("connection established and command should be ran, reading logs");
                byte[] tmp = new byte[1024];
                while (true) {
                    _log.info("inside true loop");
                    while (in.available() > 0) {
                        _log.info("output reported from python");
                        int i = in.read(tmp, 0, 1024);
                        if (i < 0) {
                            break;
                        }
                        output.write(tmp, 0, i);
                    }
                    while (err.available() > 0) {
                        _log.info("error reported from python");
                        int i = err.read(tmp, 0, 1024);
                        if (i < 0) {
                            break;
                        }
                        error.write(tmp, 0, i);
                    }
                    if (exec.isClosed()) {
                        _log.info("channel closed");
                        if ((in.available() > 0) || (err.available() > 0)) {
                            continue;
                        }
                        _log.info(String.format("exit-status: %s", exec.getExitStatus()));
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ee) {
                        _log.warning(ee.toString());// ee.getStackTrace().toString());
                    }
                }

                _log.info(String.format("[%s] Normal output: %s", host, output.toString()));
                _log.severe(String.format("[%s] Error output: %s", host, error.toString()));

                if (output.toString().contains("exception") || error.toString().contains("exception")) {
                    throw new JSchException(String.format("An exception occurred while attempting to run the python file:\n%s\n%s", 
                            output.toString(), error.toString()));
                }

//                secondaries = _qm.getScalesToUploadSecondaries(storeId, groupId);
//                while (secondaries.next()) {
//                    String upgradeId = secondaries.getString("upgradeId");
//                    String scaleIp = secondaries.getString("ipAddress");
//
//                    _qm.updateStatus(upgradeId, scaleIp, TaskStatus.file_uploaded_awaiting_upgrade.toString(), 
//                            String.format("Upgrade file uploaded from primary: %s", host));
//                }
                for (int i = 0; i < secondariesStrings.size()/2; i++) {
                    _qm.updateStatus(secondariesStrings.get(i), secondariesStrings.get(i+1), TaskStatus.file_uploaded_awaiting_upgrade.toString(), 
                            String.format("Upgrade file uploaded from primary: %s", host));
                }

                _log.info("Finish new thread to upload to secondaries");
            } catch (JSchException | IOException ex) {
                _log.severe(String.format("Error: SftpException from executing python file to send upgrade file to secondaries.\n%s",
                        ex.getMessage()));
                for (int i = 0; i < secondariesStrings.size()/2; i++) {
                    _qm.updateStatus(secondariesStrings.get(i), secondariesStrings.get(i+1), TaskStatus.upload_failed.name(), "Exception occurred while uploading or running remote python script");
                    i++;
                }
            } finally {
                if (newSession != null && newSession.isConnected()) {
                    newSession.disconnect();
                }
                if (exec != null && exec.isConnected()) {
                    exec.disconnect();
                }
            }
        });
    }
    
    /**
     * 
     * @param filePath
     * @param hosts 
     */
    private void customizePython(String filePath, List<String> hosts) {
        _log.info("customizing python file...");
        Path toRead = Paths.get("script/hobartUploader.py");
        Path toWritePath = Paths.get("script/hobartUploaderCustom.py");
        try {
            if (!toWritePath.toFile().exists()) {
                toWritePath.toFile().delete();
                toWritePath = Files.createFile(toWritePath);
            }
        } catch (IOException ex) {
            _log.severe(String.format("Exception while creating destination python file.\n%s", ex.getMessage()));
        }
        
        // read contents of base file for a fresh slate to customize
        _log.info("reading file contents of template file...");
        StringBuilder contents = new StringBuilder();
        try (BufferedReader buff = new BufferedReader(new FileReader(toRead.toFile()))) {
            String line;
            while ((line = buff.readLine()) != null) {
                contents.append(line);
                contents.append("\n");
            }
        } catch (IOException ex) {
            _log.severe(String.format("Exception while reading python file.\n%s", ex.getMessage()));
        }
        
        // customize the placeholders with the file to upload and the hosts to send the file to
        _log.info("adding custom values...");
        int upgradeDirectoryStart = contents.indexOf("~upgrade_directory~");
        if (this.scaleModelType.equals("GT")) {
            contents.replace(upgradeDirectoryStart, upgradeDirectoryStart+19, "/opt/hobart/upgrade");
        } else {
            contents.replace(upgradeDirectoryStart, upgradeDirectoryStart+19, "/usr/local/hobart/upgrade");
        }
        int fileNameStart = contents.indexOf("~filename~");
        contents.replace(fileNameStart, fileNameStart+10, filePath);
        int hostsStart = contents.indexOf("[\"~host~\"]");
        contents.replace(hostsStart, hostsStart+10, hosts.toString());
        int usernameStart = contents.indexOf("~username~");
        contents.replace(usernameStart, usernameStart+10, USERNAME);
        int passwordStart = contents.indexOf("~password~");
        contents.replace(passwordStart, passwordStart+10, PASSWORD);
        
        // write new contents to customized file to be sent
        _log.info("writing data to destination file...");
        try (BufferedWriter buff = new BufferedWriter(new FileWriter(toWritePath.toFile().getPath()))) {
            buff.write(contents.toString());
        } catch (IOException ex) {
            _log.severe(String.format("Exception while writing customization to python file.\n%s", ex.getMessage()));
        }
    }

    /**
     *
     * @return @throws JSchException
     */
    private Session establishSession() throws JSchException {
        JSch jsch = new JSch();
        Session _session = jsch.getSession(USERNAME, host, PORT);
        _session.setPassword(PASSWORD);
        _session.setConfig("StrictHostKeyChecking", "no");
        _session.setTimeout(5000);
        _session.connect();
        return _session;
    }

    /**
     *
     * @param session
     * @param remoteFilePath
     * @return
     */
    private String calculateRemoteChecksum(Session session, String remoteFilePath) {
        String checksum = "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ChannelExec channel = (ChannelExec) session.openChannel("exec");
            String command = "md5sum " + remoteFilePath;
            channel.setCommand(command);
            channel.setInputStream(null);
            channel.setOutputStream(baos);
            channel.setErrStream(System.err);

            ByteArrayOutputStream outputBuffer = new ByteArrayOutputStream();
            ByteArrayOutputStream errorBuffer = new ByteArrayOutputStream();

            InputStream in = channel.getInputStream();
            InputStream err = channel.getErrStream();

            channel.connect();

            byte[] tmp = new byte[1024];

            // Wait for the command to complete
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) {
                        break;
                    }
                    outputBuffer.write(tmp, 0, i);
                }
                while (err.available() > 0) {
                    int i = err.read(tmp, 0, 1024);
                    if (i < 0) {
                        break;
                    }
                    errorBuffer.write(tmp, 0, i);
                }
                if (channel.isClosed()) {
                    if ((in.available() > 0) || (err.available() > 0)) {
                        continue;
                    }
                    _log.info("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ee) {
                    _log.warning(ee.toString());// ee.getStackTrace().toString());
                }
            }
            checksum = outputBuffer.toString().split(" ")[0];

            channel.disconnect();
        } catch (Exception e) {
            _log.warning(e.getStackTrace().toString());
        }
        return checksum;
    }

    public void setMaxRetry(int maxRetry) {
        this.maxRetry = maxRetry;
    }
}
