package com.hobart.hte.filemgr.util;

/**
 *
 * @author lopezaz
 */
public enum TaskStatus {
    unassigned,
    awaiting_upload,
    awaiting_primary,
    upload_in_progress,
    upload_completed_no_errors,
    upload_completed_errors,
    upload_failed,
    file_uploaded_awaiting_upgrade,
    upgrade_in_progress,
    post_install_process,
    upgrade_completed_no_errors,
    upgrade_completed_errors,
    upgrade_failed
}
