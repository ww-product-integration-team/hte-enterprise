package com.hobart.hte.filemgr.util;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 * @author lopezaz
 */
public class MyLogFormatter extends Formatter {

    //private static final String PATTERN = "[MM-dd-yyyy HH:mm:ss]";
    private final SimpleDateFormat sdf = new SimpleDateFormat("[MM-dd-yyyy HH:mm:ss]");

    @Override
    public String format(final LogRecord record) {
        return String.format(
                "%1$s %2$-7s %3$s\r\n",
                sdf.format(new Date(record.getMillis())),
                record.getLevel().getName(), formatMessage(record));
    }
}
