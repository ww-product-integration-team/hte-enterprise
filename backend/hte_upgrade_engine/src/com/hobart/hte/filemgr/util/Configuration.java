package com.hobart.hte.filemgr.util;

import java.util.Properties;

/**
 *
 * @author lopezaz
 */
public class Configuration {

    private static Configuration config = null;
    private Properties defaultProps;

    public Configuration() {
        defaultProps = new java.util.Properties();
        try {
            java.io.FileInputStream in = new java.io.FileInputStream("config.ini");
            defaultProps.load(in);
            in.close();
        } catch (java.io.IOException io) {

        }
    }

    public static Configuration getInstance() {
        if (config == null) {
            config = new Configuration();
        }
        return config;
    }

    public static Configuration reloadInstance() {
        config = new Configuration();
        return config;
    }

    /**
     *
     * @param key
     * @param def
     * @return
     */
    public String getStringProperty(String key, String def) {
        return defaultProps.getProperty(key, def);
    }

    /**
     *
     * @param key
     * @param def
     * @return
     */
    public int getIntProperty(String key, int def) {
        try {
            return Integer.parseInt(defaultProps.getProperty(key, String.valueOf(def)));
        } catch (Exception ex) {
            return def;
        }
    }

    /*
    
    */
    public String getRepositoryDirectory() {
        String _default;
        String os = System.getProperty("os.name");
        if (os.contains("Windows")) {
            _default = "c:\\hte\\repository\\upgrades\\";
        } else if (os.contains("Linux")) {
            _default = "/home/hobart/repository/upgrades/";
        } else { 
            _default = "/home/hobart/repository/upgrades/";
        }

        return defaultProps.getProperty("repository_directory", _default);
    }
}
