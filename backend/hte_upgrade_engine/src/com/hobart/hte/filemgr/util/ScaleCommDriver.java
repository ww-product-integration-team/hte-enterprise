/*
 * CommDriver.java
 *
 * Created on 24 de marzo de 2007, 10:41 PM
 *
 */
package com.hobart.hte.filemgr.util;

import com.hobart.hte.filemgr.ssh.SSHTools;
import java.io.*;
import java.net.*;
import java.util.logging.Logger;

/**
 *
 * @author Angel
 */
public class ScaleCommDriver {

    // By default port number is always 6000
    private String filePath;
    private final String Header = "RT01" + US + "DT1" + RS;
    private final String HeaderErr = "RTER" + US + "EC03" + US + "ERBA" + RS;
    private final String Footer = "RTFF" + RS;
    private int timeout = 30 * 1000; //30 secs by default
    private int port;
    private String host;
    private String error;
    private Logger logger;

    public final int BUFF_SIZE = 2048;
    public static final char US = 31, RS = 30, ETX = 3, ETB = 23, GS = 29;
    private final int SMBACKEND_PORT = 5076;

    /**
     * @param log
     * Creates a new instance of CommDriver
     */
    public ScaleCommDriver(Logger log) {
        port = 6000;
        logger = log;
    }

//    public void setPort (int port) { Port=port; }
//    public void setIP_address (String ip) { ip_address = ip; }
    public void setTimeOut(int to) {
        timeout = to;
    }

    /**
     * Get Scale configuration thru port 6000
     *
     * @return Configuration string in Hobart Tagged format, otherwise null.
     */
    public String getConfiguration() {
        String cfg_res;
        try {
            cfg_res = sendCommand("RT7D" + US + RS);
            setCommError(null); // communication succesful
            return cfg_res;
        } catch (Exception e) {
            setCommError(e.getMessage());
            return null;
        }
    }

    /**
     *
     * @param command
     * @return
     * @throws IOException
     * @throws SocketTimeoutException
     */
    public String sendCommand(String command) throws IOException, SocketTimeoutException {
        String rec;
        StringBuilder cmd = new StringBuilder();
        Socket socket;

        if (!command.contains("RTB9")) {
            cmd.append(Header);
        }
        cmd.append(command);
        if (!command.contains("RTB9")) {
            cmd.append(Footer);
        }
        socket = new Socket(getIP_address(), getPort());
        socket.setSoTimeout(timeout);
        sendData(cmd.toString(), socket.getOutputStream());
//        System.out.println("[" + getIP_address() + "] sendCommand: " + cmd.toString());
        rec = readData(socket.getInputStream());
//        System.out.println("sendCommand: " + rec);
        socket.close();
//        System.out.println("connection closed.");

        return rec;
    }

    /**
     *
     * @param data
     * @param out
     * @throws IOException
     * @throws SocketTimeoutException
     */
    public void sendData(String data, OutputStream out) throws IOException, SocketTimeoutException {
        for (int i = 0; i < data.length(); i++) {
            out.write((int) data.charAt(i));
        }
    }

    /**
     *
     * @param in
     * @return
     * @throws IOException
     * @throws SocketTimeoutException
     */
    public String readData(InputStream in) throws IOException, SocketTimeoutException {
        StringBuilder buf = new StringBuilder();
        boolean isReceiving = true;
        char c;

        while (isReceiving) {
            c = (char) in.read();
            buf.append(c);
            if (c == '\n') {
                buf.append(GS);
            }

            if (buf.toString().endsWith(Footer)) {
                isReceiving = false;
            }
        }
//        System.out.println("\nreadData(): "+buf.toString());
        return buf.toString();
    }

    /**
     *
     * @param ip address of the scale to get version from
     * @return if successful it returns firmware version of the scale with IP
     * address and comm error null, if can't connect then null and comm error
     * text contains the reason.
     * @throws IOException
     */
    public String getFirmwareVersion(String ip) {
        String cfg_res;
        setIPAddress(ip);

        try {
            cfg_res = sendCommand("RT7D" + US + RS);
            setCommError(null); // communication successful

            StringBuilder st = new StringBuilder();

            String tmp_str = getValue(cfg_res, "\u001fVE");
            if (tmp_str == null) {
                tmp_str = "N/A";
            }
            st.append(tmp_str);

            tmp_str = getValue(cfg_res, "\u001fRe");
            if (tmp_str == null) {
                tmp_str = "N/A";
            }
            st.append(".").append(tmp_str);

            return st.toString();
        } catch (Exception e) {
            setCommError(e.getMessage());
        }
        return null;
    }

    /**
     * Retrieves firmware version via port 22, it considers if there is a
     * revision version and concatenates it at the end
     *
     * @param ip IP address of the scale to connect to
     * @return Firmware version of the scale, including the software revision
     * (if available)
     */
    public String getFirmwareVersionSsh(String ip) {
        SSHTools _ssh = new SSHTools();

        String frw = _ssh.executeSSHCommandVerbose(ip,
                "cat /etc/hobart/version.ini | grep component.2.ver | cut -d \"=\" -f2 | "
                        + "cut -c 4- && if [ -f /etc/hobart/software_revision.ini ]; then cat /etc/hobart/software_revision.ini; fi");

        if (frw.startsWith("<<")) {
            return "N/A";
        }

        frw = frw.replaceAll("\r", "").replaceAll("\n", "");
        return frw;
    }

    /**
     * Read scale statistics and return number of plus in scale
     *
     * @param ip IP address of the scale
     * @return Number of PLUs in scale
     */
    public int getPLUCount(String ip, Logger log) {
        String cfg_res;
        setIPAddress(ip);

        try {
            cfg_res = sendCommand("RT68" + US + RS);
            setCommError(null); // communication successful

            String plucount = getValue(cfg_res, "#p");
            if (plucount == null) {
                plucount = "-1";
            }
            return Integer.parseInt(plucount);

        } catch (Exception e) {
            setCommError(e.getMessage());
            log.severe(String.format("[%s] Exception sending command for plu count: %s", ip, e.getMessage()));
        }
        return -1;
    }

    /**
     * Send command to create file /usr/local/hobart/cat3WeigherConfig.ht and at
     * the same time read scale configuration.
     *
     * @param ip
     * @return
     */
    public String getWeigherConfig(String ip) {
        String cfg_res;
        setIPAddress(ip);

        try {
//            cfg_res = sendCommand("RT5W" + US + RS);
            cfg_res = sendCommand("RTDO\u001fTNreadCat3WeigherConfig\u001eRT7D\u001e");
            System.out.println("cfg=" + cfg_res);
            setCommError(null); // communication successful

            String data = getValue(cfg_res, "Wp");

            data += "|" + getValue(cfg_res, "mT");
            if (data == null) {
                data = "N/A";
            }
            return data;

        } catch (Exception e) {
            setCommError(e.getMessage());
        }
        return null;
    }

    /**
     * Retrieve available space in scale in bytes
     *
     * @param ip
     * @return available space in micro SD card in kilobytes, -1 if can't
     * establish communication to scale
     */
    public long getAvailableSpace(String ip) {
        String cfg_res;
        setIPAddress(ip);

        try {
            cfg_res = sendCommand("RT68" + US + RS);
            setCommError(null); // communication successful

            String available = getValue(cfg_res, "fm");
            if (available == null) {
                available = "-1";
            }
            return Long.parseLong(available);

        } catch (Exception e) {
            setCommError(e.getMessage());
        }
        return -1;
    }

    /**
     * Retrieve the most relevant versions from scale using port 5076
     * (SMBackend)
     *
     * @param ip The IP address of the selected scale
     * @return An String array with the following fields: Model name, release Id
     * (aka firmware version), fpc version and bootloader version.
     */
    public String[] retrieveVersions(String ip) {
        String[] fields = new String[4];
        String raw;
        Socket scale;
        byte[] data = new byte[256];

        try {
            scale = new Socket(ip, SMBACKEND_PORT);
            scale.setSoTimeout(timeout);

            sendData("retrieveStatistics|model_name,application_version,bootloader_version,fpc_version||1\n", scale.getOutputStream());

            scale.getInputStream().read(data);

            raw = new String(data).split("\\|")[1];
            System.out.println("data read: " + raw);
            // parse: {"bootloader_version": "3.3.0", "application_version": {"release_id": "3.0.0", "creation_time": "10:55:29", "creation_date": "06/15/2017"}, "model_name": "HTi-7LS", "fpc_version": "2.6"}

            fields[0] = getJSonValue(raw, "model_name");
            fields[1] = getJSonValue(raw, "release_id").trim();
            fields[3] = getJSonValue(raw, "fpc_version");
            fields[2] = getJSonValue(raw, "bootloader_version");

            System.out.println("model name: " + fields[0]);
            System.out.println("release_id: " + fields[1]);
            System.out.println("bootloader_version: " + fields[2]);
            System.out.println("fpc_version: " + fields[3]);

            scale.close();
        } catch (Exception e) {
//            e.printStackTrace();
            // note: probably scale is too old it doesn't understand the command try to get firmware version using port 6000
            raw = getFirmwareVersionSsh(ip);
            if (raw != null) {
                fields[0] = "N/A";
                fields[1] = raw;
                fields[2] = "N/A";
                fields[3] = "N/A";
            } else {
                return null;
            }
        }

        return fields;
    }

    /**
     *
     * @param data
     * @param tag
     * @return
     */
    public String getJSonValue(String data, String tag) {
        String value;
        int ini = data.indexOf(tag);
        if (ini == -1) {
            return null;
        }
        int end = data.indexOf(",", ini);
        if (end == -1) {
            end = data.indexOf("}", ini);
        }
        value = data.substring(ini + tag.length(), end).replaceAll("\"|:", "").trim();

        return value;
    }

    /**
     * On a tagged format record it retrieves the value of the given tag
     *
     * @param sb String from where to search the given tag
     * @param tag tag ID
     * @return value corresponding to the tag.
     */
    public String getValue(String sb, String tag) {
        int i = sb.indexOf(tag);
        if (i == -1) {
            return null;
        }
        String value = sb.substring(i + tag.length());
        try {
            value = value.substring(0, value.indexOf(US));
        } catch (Exception e) {
            value = value.substring(0, value.indexOf(ETB));
        }
        return value;
    }

    /**
     *
     * @param i
     */
    public void setPort(int i) {
        port = i;
    }

    public int getPort() {
        return port;
    }

    public void setIPAddress(String ip) {
        host = ip;
    }

    public String getIP_address() {
        return host;
    }

    private void setCommError(String err) {
        error = err;
    }

    public String getCommError() {
        return error;
    }

}
