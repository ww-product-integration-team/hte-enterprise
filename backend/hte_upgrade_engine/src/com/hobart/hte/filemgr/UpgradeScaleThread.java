package com.hobart.hte.filemgr;

import com.hobart.hte.filemgr.ssh.SSHTools;
import com.hobart.hte.filemgr.tasks.BackupRestoreDelete;
import com.hobart.hte.filemgr.tasks.CheckFilesInScale;
import com.hobart.hte.filemgr.util.MyLogFormatter;
import com.hobart.hte.filemgr.util.ScaleCommDriver;
import com.hobart.hte.filemgr.util.TaskStatus;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * Definition of a thread that prepares the scale for launching the upgrade file
 * assigned
 *
 * @author lopezaz
 */
public class UpgradeScaleThread implements Runnable {

    private int _maxRetries;
    private final String host;
    private final String upgradeId;
    private final Logger _log, scaleLog;
    private String statusError;
    private TaskStatus upgradeResult;
    private final String upgrade_file;
    private final QueryManager qm;
    private int waitMinutes;
    private final SSHTools _ssh;
    private final ArrayList<String> do_not_delete;
    private boolean validateRecoveryFiles = false;
    private String localUpgradeRepo;
    private String scaleModelType;

    /**
     *
     * @param host
     * @param fileName
     * @param end
     * @param upgradeId
     * @param logger
     * @param queryManager
     */
    public UpgradeScaleThread(String host, String fileName, LocalDateTime end,
            String upgradeId, Logger logger, QueryManager queryManager) {
        this.host = host;
        //this.fileName = fileName;
        upgrade_file = fileName;
        this.upgradeId = upgradeId;
        _log = logger;
        this.qm = queryManager;
        _ssh = new SSHTools();
        _ssh.setLogger(_log);
        do_not_delete = new ArrayList();
        do_not_delete.add(fileName);
        _log.info("--= constructor ==-");
        scaleLog = Logger.getLogger("upgradeLog_" + host);
        this.scaleModelType = UpgradeManager.determineScaleModelType(host);

        try {
            FileHandler fh = new FileHandler("upgradeLog_" + host + ".txt", true);
            scaleLog.addHandler(fh);
            fh.setFormatter(new MyLogFormatter());
        } catch (SecurityException | IOException e) {
            System.err.println("Unable to start log: " + e.getMessage());
        }
    }

    /*
        This thread performs the following tasks: 
        1. verify current firmware version 
        2. validate upgrade files are present in the scale 
        3. backup scale 
        4. launch monolithic or core 
        5. Change scale status to 'upgrade_in_progress' 
        6. re-queue scale if couldn't connect after max retries
     */
    @Override
    public void run() {

        scaleLog.info("--- starting ---");
        try {
            // change scale status so no other process is started in the scale
            qm.updateStatus(upgradeId, host,
                    TaskStatus.upgrade_in_progress.name(), "starting upgrade...");
            prepareScaleAndLaunchUpgrade();
        } catch (Exception e) {
            e.printStackTrace();
            _log.severe(e.toString());
        }
        scaleLog.info("--- launching upgrade ended ---");
    }

    /**
     *
     * @return
     */
    private String getFirmwareVersion() {
        if (this.upgrade_file != null) {
            return upgrade_file.split("-")[3];
        }
        return "N/A";
    }

    public void setMaxRetry(int maxRetries) {
        _maxRetries = maxRetries;
    }

    /**
     *
     * @return
     */
    private boolean backupScale() {
        BackupRestoreDelete database = new BackupRestoreDelete();

        _log.info(String.format("[%s] Backing up scale...", host));

        try {
            int tries = 0;
            while (!database.backupScale(host) && tries < _maxRetries) {
                Thread.sleep(5000);
                tries++;
            }
            if (tries < _maxRetries) {
                _log.info(String.format("[%s] Backup done", host));
            } else {
                _log.info(String.format("An unexpected error occurred when backing up scale %s, backup task could not be uploaded", host));
                //log(host.getHost(), "An unexpected error occurred when backing up scale " + host + ", backup task could not be uploaded");
                return false;
            }
        } catch (IOException | InterruptedException ex) {
            _log.info(String.format("An unexpected error occurred when backing up scale %s: %s", host, ex.toString()));
            return false;
        }

        // confirm and record backup file is present
        String linuxCommand = "ls -l ";
        if (scaleModelType.equals("GT")) {
            linuxCommand += "/opt/hobart";
        } else {
            linuxCommand += "/usr/local/hobart"; // + Configuration.getInstance().getBackupDirectory(); // /usr/local/hobart
        }
        String tmp_res = _ssh.executeSSHCommandVerbose(host, linuxCommand);
        _log.info(String.format("[%s] hobart directory before upgrade:\n%s", host, tmp_res));
        return true;
    }

    /**
     *
     */
    private void validateFreeSpace() {
        ScaleCommDriver q = new ScaleCommDriver(_log);
        long scaleAvailableSpace = q.getAvailableSpace(host);
        //File uf = Paths.get(qm.getConfiguration("repositoryPath", "/home/hobart/repository"), upgrade_file).toFile();
        long file_size = getLocalUpgradeFilePath().length() / 1024;

        if (scaleAvailableSpace == -1) {
            _log.info(String.format("[%s] Unable to connect to scale and retrieve available space in scale using port 6000", host));
            // ToDo: decide what to do if available space can't be retrieved
        }

        _log.info(String.format("[%s] available space in scale: %,d(KB), upgrade file size: %,d(KB)", host, scaleAvailableSpace, file_size));

        if ((file_size * 2.1) < scaleAvailableSpace) {
            _log.info(String.format("[%s] There is enough space in scale partition to install upgrade!", host));
        } else {
            _log.info(String.format("[%s] Not enough space in scale partition to install upgrade!", host));
        }

        String tmp;
        if (scaleModelType.equals("GT")) {
            tmp = _ssh.deleteOldFiles(host, "/opt/hobart/upgrade/", do_not_delete, scaleModelType);
        } else {
            tmp = _ssh.deleteOldFiles(host, "/usr/local/hobart/upgrade/", do_not_delete, scaleModelType);
        }

        tmp += _ssh.deleteOldFiles(host, "/", do_not_delete, scaleModelType);
        if (tmp.contains("<<Error>>")) {
            _log.info(String.format("[%s] %s", host, tmp));
        }

        _log.info(String.format("[%s] available space in scale after cleanup: %,d(KB)", host, q.getAvailableSpace(host)));

    }

    public void setWaitMinutes(int waitMin) {
        waitMinutes = waitMin;
    }

    public void setLocalUpgradeRepository(String repo) {
        localUpgradeRepo = repo;
    }

    /**
     *
     * @return
     */
    private File getLocalUpgradeFilePath() {
        //File uf = Paths.get(qm.getConfiguration("repositoryPath", "/home/hobart/repository"), upgrade_file).toFile();
        //return uf;
        //System.out.println("> > > > > > " + localUpgradeRepo);
        return Paths.get(localUpgradeRepo, upgrade_file).toFile();
    }

    /**
     * Core functionality to prepare scale and launch the installation
     */
    private void prepareScaleAndLaunchUpgrade() {
        String linuxCommand, stmp;
        _log.info(String.format("[%s] === Starting to prepare the scale to launch upgrade. ===\n", host));

        _log.info(String.format("[%s] Setting upload path and processing if files are in scale", host));
        scaleLog.info(String.format("[%s] Verifying if upgrade files are loaded in the scale", host));
        CheckFilesInScale cfis = new CheckFilesInScale(getLocalUpgradeFilePath());
        int tmp;
        if (scaleModelType.equals("GT")) {
            cfis.setUploadPath("/opt/hobart/upgrade/");
            tmp = cfis.processOneScale(host, "gt-*");
        } else {
            cfis.setUploadPath("/usr/local/hobart/upgrade/");
            tmp = cfis.processOneScale(host, "hti-*");
        }
        _log.info(String.format("[%s] Processing complete, evaluating results...", host));
        
        switch (tmp) {
            case CheckFilesInScale.NO:
                // something went wrong and files are not in the scale
                statusError = "Upgrade file not found in the scale";
                break;
            case CheckFilesInScale.OFFLINE:
                statusError = "Scale is offline, can't continue with process!";
        }

        if (tmp != CheckFilesInScale.YES) {
            _log.info(String.format("[%s] There was an issue with processing the scale for upgrading, see scale log file", host));
            scaleLog.info(cfis.getErrorMessage());

            scaleLog.info(String.format("[%s] %s", host, statusError));

            //result = Result.UPGRADE_ABORTED;
            upgradeResult = TaskStatus.upgrade_failed;
            qm.updateStatus(upgradeId, host, upgradeResult.toString(), statusError);
            // ToDo: should we requeue the scale with a future time to attempt? 
            return;
        }
        
        if (!upgrade_file.contains("hti-linux-feature") && !UpgradeManager.isGTFileType(upgrade_file)) {
            _log.info(String.format("[%s] upgrade file does not use normal naming conventions, locating recovery partition...", host));
            // find out what is the path to the recovery partition in the scale 
            linuxCommand = "lsblk | grep p2";
            String partition = _ssh.executeSSHCommandVerbose(host, linuxCommand);
            scaleLog.info(String.format("[%s] cmd: %s\n%s", host, linuxCommand, partition));
            String partName = "/media";
            int index = partition.indexOf(partName);
            if (index > -1) {
                partition = partition.substring(index).trim();
            } else {
                _log.severe(String.format("[%s] Could not find %s partition. Cannot continue upgrade!", host, partName));
                scaleLog.severe(String.format("[%s] Recovery partition could not be found. Cannot continue upgrade!", host));
                statusError = String.format("Could not find \"%s\" recovery partition. Cannot continue.", partName);
                upgradeResult = TaskStatus.upgrade_failed;
                qm.updateStatus(upgradeId, host, upgradeResult.toString(), statusError);
                return;
            }

            // read list of files from recovery file
            if (validateRecoveryFiles) {
                _log.info(String.format("[%s] Partition found, validating recovery files...", host));
                ArrayList<String> recoveryFilesToCheck = new ArrayList<>();
                try {
                    try ( // ToDo: define how to load these files and make this validation configurable
                            BufferedReader br = new BufferedReader(new FileReader("recoveryFiles.ini"))) {
                        while ((stmp = br.readLine()) != null) {
                            if (!stmp.startsWith("#")) {
                                recoveryFilesToCheck.add(partition + stmp.trim());
                            }
                        }
                    }
                } catch (IOException io) {
                    scaleLog.info(String.format("[%s] %s", host, "Couldn't find recoveryFiles.ini file, setting up default files"));
                    recoveryFilesToCheck.add("/media/mmcblk0p1/boot/uImage");
                    recoveryFilesToCheck.add("/media/mmcblk0p1/etc/init.d/recovery.sh");
                    recoveryFilesToCheck.add("/media/mmcblk0p1/etc/init.d/rc.local");
                }

                scaleLog.info(String.format("[%s] %s", host, "Verifing if recovery files are in scale..."));
                if (cfis.processOneScale(host, recoveryFilesToCheck) != CheckFilesInScale.YES) {
                    statusError = "Important Kernel/Recovery files missing! Can't continue.";
                    scaleLog.info(String.format("[%s] %s", host, statusError));
                    scaleLog.info(String.format("[%s] %s", host, cfis.getErrorMessage()));
                    upgradeResult = TaskStatus.upgrade_failed;
                    qm.updateStatus(upgradeId, host, upgradeResult.toString(), statusError);
                    return;
                }
            }

            // Note: no reboot considered in this case
            _log.info(String.format("[%s] Grabbing scale firmware...", host));
            ScaleCommDriver sc = new ScaleCommDriver(_log);
            stmp = sc.getFirmwareVersion(host);
            if (stmp != null) {
                _log.info(String.format("[%s] file firmware version is %s and scale firmware version is %s", host, getFirmwareVersion(), stmp));
                scaleLog.info(String.format("[%s] file_firmware_version = %s, scale firmware version = %s", host, getFirmwareVersion(), stmp));
            } else {
                _log.info(String.format("[%s] Could not connect to scale to read firmware version", host));
                upgradeResult = TaskStatus.upgrade_failed;
                statusError = "Can't connect to scale to read firmware version";
                scaleLog.info(String.format("[%s] %s", host, statusError));
                scaleLog.info(String.format("[%s] %s", host, sc.getCommError()));
                qm.updateStatus(upgradeId, host, upgradeResult.toString(), statusError);
                return;
            }

            // remove any feature file to avoid any problem when rebooting
            _log.info(String.format("[%s] Deleting old upgrade files...", host));
            linuxCommand = "rm -r /usr/local/hobart/features/*.package.tgz && ls -l /usr/local/hobart/features/";
            scaleLog.info(String.format("[%s] %s", host, "command to delete install files in feature dir: " + linuxCommand));
            scaleLog.info(String.format("[%s] %s", host, "deleting old feature files in scale"));
            stmp = _ssh.executeSSHCommandVerbose(host, linuxCommand);
            scaleLog.info(String.format("[%s] %s", host, stmp));

            // delete temp files from previous upload attempts, if there's any still there
            _log.info(String.format("[%s] Removing partial files in scale...", host));
            linuxCommand = "rm /*.filepart /usr/local/hobart/upgrade/*.filepart";
            scaleLog.info(String.format("[%s] %s", host, "deleting partial upgrade files in scale"));
            scaleLog.info(String.format("[%s] %s", host, "command to delete temp files: " + linuxCommand));
            stmp = _ssh.executeSSHCommandVerbose(host, linuxCommand);
            scaleLog.info(String.format("[%s] %s", host, stmp));

            _log.info(String.format("[%s] Validating free space in scale...", host));
            validateFreeSpace();

            // read and store current firmware version and plu count
            int plu_count = sc.getPLUCount(host, _log);
            scaleLog.info(String.format("[%s] plu count in scale: %d", host, plu_count));

            // ToDo: validate if it fails to move forward or not
            _log.info(String.format("[%s] Backing up scale...", host));
            backupScale();

            // execute "sync" command before launching monolithic upgrade
            _log.info(String.format("[%s] Syncing to scale", host));
            linuxCommand = "sync";
            stmp = _ssh.executeSSHCommandVerbose(host, linuxCommand);
            scaleLog.info(String.format("[%s] sync message from scale: %s", host, stmp));

            // run ifconfig or ip addr and log it out
            _log.info(String.format("[%s] Finding networking information of the scale...", host));
            String cmdReport = "[%s] ifconfig reply: %s";
            linuxCommand = "ifconfig";
            stmp = _ssh.executeSSHCommandVerbose(host, linuxCommand);
            scaleLog.info(String.format(cmdReport, host, stmp));

        } else {
            scaleLog.info("Skipping core validations since we are installing a feature file only.");
        }

        stmp = "";
        if (upgrade_file != null && !upgrade_file.isEmpty()) {
            // execute upgrade - ToDo: make upgrade path configurable
            if (upgrade_file.contains(".package.tgz")) {
                _log.info(String.format("[%s] executing upgrade for ht scale...", host));
                // update time in DB when the system should go back and check for the scale
                qm.updateScaleNextCheck(host, upgradeId, waitMinutes);
                linuxCommand = String.format("cd /usr/local/hobart/upgrade ; python /usr/local/hobart/python/installUpgrade.py /usr/local/hobart/upgrade/%s",
                    upgrade_file);
                scaleLog.info(String.format("[%s] Executing upgrade, command: %s", host, linuxCommand));
                // execute installation but don't wait for scale answer? 
                stmp = _ssh.executeSSHCommandVerbose(host, linuxCommand);
                _log.info(String.format("[%s] upgrade in progress, now waiting %s minutes", host, waitMinutes));
                scaleLog.info(String.format("[%s] message from scale: %s", host, stmp));
            }
            else if (UpgradeManager.isGTFileType(upgrade_file)) {
                _log.info(String.format("[%s] executing upgrade for gt scale...", host));
                // update time in DB when the system should go back and check for the scale
                qm.updateScaleNextCheck(host, upgradeId, waitMinutes+3);
                linuxCommand = String.format("python3 /usr/bin/installUpgrade.py /opt/hobart/upgrade/%s", upgrade_file);
                scaleLog.info(String.format("[%s] Executing upgrade, command: %s", host, linuxCommand));
                stmp = _ssh.executeSSHCommandVerbose(host, linuxCommand);
                _log.info(String.format("[%s] upgrade in progress, now waiting %s minutes", host, waitMinutes+3));
                scaleLog.info(String.format("[%s] message from scale: %s", host, stmp));
            }
        }

        statusError = "";
        if (stmp.startsWith("<<error>>")) {
            _log.severe(String.format("[%s] error reported while upgrading scale", host));
            upgradeResult = TaskStatus.upgrade_failed;
            statusError = "an error ocurred while attempting to launch upgrade";
        } else {
            upgradeResult = TaskStatus.upgrade_in_progress;
            statusError = String.format("[%s] executing scale upgrade, waiting %s minutes...",
                    //new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime()),
                    host, waitMinutes);
        }
        scaleLog.info(String.format("[%s] %s", host, statusError));
        _log.info(String.format("[%s] %s", host, statusError));
        qm.updateStatus(upgradeId, host, upgradeResult.toString(), statusError);
    }

    /**
     *
     * @param validateRecoveryFiles
     */
    public void setValidateRecoveryFiles(boolean validateRecoveryFiles) {
        this.validateRecoveryFiles = validateRecoveryFiles;
    }
}
