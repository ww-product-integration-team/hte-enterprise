package com.hobart.hte.filemgr;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 *
 * @author lopezaz
 */
public class QueryManager {

    private Connection dbConnect;
    private String dbUrl;
    private String dbUsername;
    private String dbPassword;
    private int dbConnectRetries;

    private final String getNewUploadsMultipath
            = "SELECT"
            + "    ud.upgradeId, ud.groupId, uf.fileName, uf.checksum, d.ipAddress, d.priority, d.storeId, b.uploadStart, b.uploadEnd "
            + "FROM upgradeBatch b "
            + "JOIN hobfile uf ON uf.fileId = b.fileId "
            + "JOIN upgradeDevice ud ON ud.batchId = b.batchId "
            + "JOIN scaleDevice d ON ud.deviceId = d.deviceId "
            + "WHERE"
            + "	   ((ud.status = 'awaiting_upload' AND NOW() BETWEEN b.uploadStart AND b.uploadEnd)"
            + "    OR (ud.status = 'upload_now'))"
            + "    AND d.isPrimaryScale = 1 AND uf.enabled = 1 "
            + "ORDER BY d.priority ASC";
    private String getNewUploadsFindSecondaries
            = "SELECT s.deviceId, s.ipAddress, u.status, u.upgradeId "
            + "FROM scaleDevice s "
            + "JOIN upgradeDevice u ON s.deviceId = u.deviceId "
            + "WHERE s.isPrimaryScale = 0 and s.storeId = '~storeId~' and u.groupId = '~groupId~'";
    
    private final String getNewUploadsAllPrimaries
            = "SELECT"
            + "    ud.upgradeId, ud.groupId, uf.fileName, uf.checksum, d.ipAddress, d.priority, b.uploadStart, b.uploadEnd "
            + "FROM upgradeBatch b "
            + "JOIN hobfile uf ON uf.fileId = b.fileId "
            + "JOIN upgradeDevice ud ON ud.batchId = b.batchId "
            + "JOIN scaleDevice d ON ud.deviceId = d.deviceId "
            + "WHERE"
            + "	   ((ud.status = 'awaiting_upload' AND NOW() BETWEEN b.uploadStart AND b.uploadEnd)"
            + "    OR (ud.status = 'upload_now'))"
            + "    AND uf.enabled = 1 "
            + "ORDER BY d.priority ASC";

    private final String setStatus = "UPDATE upgradeDevice SET lastUpdate = NOW(), status = ?, statusText = ? WHERE upgradeId = ?;";
    private final String setScaleNextCheckTimestamp = "UPDATE upgradeDevice SET nextCheck = DATE_ADD(NOW(),INTERVAL ? MINUTE)"
            + " where upgradeId = ?";
    private final String getScaleListPostInstall
            = "SELECT ud.upgradeId, ud.nextCheck, d.ipAddress, uf.filename "
            + "FROM upgradeDevice ud "
            + "JOIN upgradeBatch b ON ud.batchId = b.batchId "
            + "JOIN scaleDevice d ON ud.deviceId = d.deviceId "
            + "JOIN hobfile uf ON uf.fileId = b.fileId "
            + "WHERE ud.nextCheck <= NOW() AND ud.status = 'upgrade_in_progress'";

    private final Logger logger;
    private final String updateVersions = "UPDATE upgradeDevice SET lastUpdate = NOW(), "
            + "scaleModel =?, application = ?, bootloader = ?, fpc = ? WHERE (upgradeId = ?);";

    /**
     *
     * @param l
     * @param dbUrl
     * @param dbUsername
     * @param dbPassword
     * @param dbConnectRetries
     */
    public QueryManager(Logger l, String dbUrl, String dbUsername, String dbPassword, int dbConnectRetries) {
        this.dbUrl = dbUrl;
        this.dbUsername = dbUsername;
        this.dbPassword = dbPassword;
        this.dbConnectRetries = dbConnectRetries;
        logger = l;
    }

    /**
     * Connects to HTe Enterprise database, it uses the connection settings
     * store in the config.ini file located in the same directory as this
     * executable
     *
     * @return TRUE if the connection to the DB is successful, otherwise FALSE
     */
    public boolean connect() {

        boolean isConnected = false;
        int connectAttempts = 0;
        while (connectAttempts < dbConnectRetries && !isConnected) {
            try {
                dbConnect = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
                isConnected = true;
            } catch (SQLException e) {
                //e.printStackTrace();
                logger.severe(e.toString());
                if (connectAttempts == 0) {
                    logger.warning("Unable to create initial connection to database. Please confirm you are connected to the network.");
                }
                System.out.printf("Failed attempt number: %s%n", connectAttempts);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    if (connectAttempts == 0) {
                        logger.severe("Unable to sleep.");
                    }
                }
            }
            connectAttempts++;
        }
        return (!isConnected && connectAttempts == dbConnectRetries);
    }

    /**
     *
     * @return
     */
    public ResultSet getScalesToUpload() {
        
        String getUploadConfig
                = "SELECT CoValue FROM appConfigs WHERE CoName = 'hybridMultipath'";
        ResultSet multipath = genericQuery(getUploadConfig);
        
        //logger.info("querying the DB for uploads to process...");
        boolean uploadConfig = false;
        try {
            multipath.next();
            uploadConfig = multipath.getBoolean(1);
        } catch (SQLException e) {
            logger.severe("Error trying to read value of config parameter: hybridMultipath. Defaulting to all scales are primaries");
        }
        if (uploadConfig) {
            return genericQuery(getNewUploadsMultipath);
        } else {
            return genericQuery(getNewUploadsAllPrimaries);
        }
    }
    /**
     * 
     * @param storeId
     * @param groupId
     * @return 
     */
    public ResultSet getScalesToUploadSecondaries(String storeId, String groupId) {
        getNewUploadsFindSecondaries = getNewUploadsFindSecondaries.replace("~storeId~", storeId);
        getNewUploadsFindSecondaries = getNewUploadsFindSecondaries.replace("~groupId~", groupId);
        return genericQuery(getNewUploadsFindSecondaries);
    }

    /**
     * Retrieve from the database those scales that have been submitted to
     * launch an upgrade
     *
     * @return
     */
    public ResultSet getScalesToUpgrade() {

        String getNewUpgrades
                = "SELECT"
                + "    ud.upgradeId, uf.fileName, uf.checksum, d.ipAddress, d.scaleModel, d.priority, b.upgradeStart, b.upgradeEnd, b.afterUpgradeCheckIn "
                + "FROM upgradeBatch b "
                + "JOIN hobfile uf ON uf.fileId = b.fileId "
                + "JOIN upgradeDevice ud ON ud.batchId = b.batchId "
                + "JOIN scaleDevice d ON ud.deviceId = d.deviceId "
                + "WHERE "
                + "     ((ud.status = 'file_uploaded_awaiting_upgrade' AND NOW() BETWEEN b.upgradeStart AND b.upgradeEnd) "
                + "     OR ud.status = 'upgrade_now') AND uf.enabled = 1 "
                + "ORDER BY d.priority ASC";

        return genericQuery(getNewUpgrades);

    }

    /**
     * Retrieve a list of scales that should be validated if all the upgrade
     * process was successful or not
     *
     * @return
     */
    public ResultSet getScalesPostInstall() {
        if (!connectionReady()) {
            connect();
        }

        try {
            PreparedStatement collectUpgrades = dbConnect.prepareStatement(getScaleListPostInstall);

            logger.info("querying the DB for scales ready to process after install...");
            ResultSet resultSet = collectUpgrades.executeQuery();

            return resultSet;
        } catch (SQLException ex) {
            logger.severe(ex.toString());
            return null;
        }
    }

    /**
     *
     * @param host
     * @param id
     * @param mins
     * @return
     */
    public int updateScaleNextCheck(String host, String id, int mins) {
        if (!connectionReady()) {
            connect();
        }

        int updated;

        try {
            PreparedStatement statusChangeStatement = dbConnect.prepareStatement(setScaleNextCheckTimestamp);
            statusChangeStatement.setInt(1, mins);
            statusChangeStatement.setString(2, id);

            int rowsAffected = statusChangeStatement.executeUpdate();
            logger.info(String.format("Rows affected: %d when updating status for scale: %s", rowsAffected, host));

            return rowsAffected;
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.severe(ex.toString());
            updated = -1;
        }

        return updated;
    }

    public int updateScaleVersions(String id, String[] vers) {
        if (!connectionReady()) {
            connect();
        }

        try {
            PreparedStatement statusChangeStatement = dbConnect.prepareStatement(updateVersions);
            statusChangeStatement.setString(1, vers[0]);
            statusChangeStatement.setString(2, vers[1]);
            statusChangeStatement.setString(3, vers[2]);
            statusChangeStatement.setString(4, vers[3]);
            statusChangeStatement.setString(5, id);

            int rowsAffected = statusChangeStatement.executeUpdate();
            logger.info(String.format("Rows affected: %d when updating scale versions: %s", rowsAffected, id));

            return rowsAffected;
        } catch (SQLException ex) {
            ex.printStackTrace();
            logger.severe(ex.toString());
            return -1;
        }
    }

    /**
     *
     * @param query
     * @return
     */
    private ResultSet genericQuery(String query) {
        if (!connectionReady()) {
            connect();
        }

        try {
            PreparedStatement collectUpgrades = dbConnect.prepareStatement(query);

            logger.finest(String.format("querying the DB: %s", query));
            ResultSet resultSet = collectUpgrades.executeQuery();

            return resultSet;
        } catch (SQLException ex) {
            logger.severe(ex.toString());
            return null;
        }
    }

    /**
     *
     * @param id Device ID assigned to this upgrade event
     * @param host Scale IP address of the scale
     * @param status Status code based on the Enum
     * @param statusText Description of the event or a reason why it failed
     * @return
     */
    public int updateStatus(String id, String host, String status, String statusText) {
        if (!connectionReady()) {
            connect();
        }

        try {
            PreparedStatement statusChangeStatement = dbConnect.prepareStatement(setStatus);
            statusChangeStatement.setString(1, status);
            statusChangeStatement.setString(2, statusText);
            statusChangeStatement.setString(3, id);

            int rowsAffected = statusChangeStatement.executeUpdate();
//            if (rowsAffected == 1) {
//                logger.info(String.format("Thread failed for host %s", host));
//            } else {
//                logger.warning(String.format("Status change failed. %s Rows were Affected!", rowsAffected));
//            }
            return rowsAffected;
        } catch (SQLException ex) {
            logger.severe(String.format("Error while updating status in DB: %s", ex.toString()));
        }
        return -1;
    }

    /**
     *
     * @param cfg
     * @param _default
     * @return
     */
    public String getConfiguration(String cfg, String _default) {
        if (!connectionReady()) {
            connect();
        }

        try {
            String res;
            Statement st = dbConnect.createStatement();

            if (st.execute("SELECT CoValue FROM appConfigs WHERE CoName = \"" + cfg + "\"")) {
                ResultSet qryResult = st.getResultSet();
                if (qryResult.next()) {
                    res = qryResult.getString(1);
                    return res;
                }
            }
            return _default;
        } catch (SQLException ex) {
            logger.severe(ex.toString());
            return _default;
        }
    }

    /**
     *
     * @param cfg
     * @param _default
     * @return
     */
    public int getConfigurationInt(String cfg, int _default) {
        String stmp = getConfiguration(cfg, String.valueOf(_default));

        try {
            return Integer.parseInt(stmp);
        } catch (NumberFormatException e) {
            return _default;
        }
    }

    /**
     *
     * @param cfg
     * @param _default
     * @return
     */
    public boolean getConfigurationBool(String cfg, boolean _default) {
        String stmp = getConfiguration(cfg, String.valueOf(_default));
        try {
            return Boolean.parseBoolean(stmp);
        } catch (NumberFormatException e) {
            return _default;
        }
    }

    /**
     * Validate if the connection is ready to be used
     *
     * @return TRUE if ready, FALSE otherwise
     */
    private boolean connectionReady() {
        try {
            return (dbConnect != null && !dbConnect.isClosed());
        } catch (SQLException s) {
            return false;
        }
    }
}
