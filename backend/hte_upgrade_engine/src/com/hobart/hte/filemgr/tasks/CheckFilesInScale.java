package com.hobart.hte.filemgr.tasks;

import com.hobart.hte.filemgr.ssh.SSHTools;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author lopezaz
 */
public class CheckFilesInScale extends SSHTools {

    private File[] local_files;
    private String h;
    public static final int OFFLINE = -1;
    public static final int NO = 0;
    public static final int YES = 1;
    private String error_message;
    private String uploadPath;

    /**
     * 
     * @param l_f Path to local files 
     */
    public CheckFilesInScale(File[] l_f) {
        local_files = l_f;
    }

    /**
     * 
     * @param singleFile Path to local file
     */
    public CheckFilesInScale(File singleFile) {
        local_files = new File[]{singleFile};
    }

    /**
     * decides whether the scale has all the Upgrade files or not. It means only
     * .tgz files, not .ht files
     *
     * @param tmp list of files read from scale
     * @return True if all local files exist in the scale with the exact same
     * size
     */
    private boolean filesInScale(String remote_files) {
        if (remote_files.isEmpty()) {
//            SSHActions.log("[" + h + "] All files missing in scale");
            error_message = "[" + h + "] All files missing in scale";
            return false;
        }

        boolean exists = true;

        for (File local_file : local_files) {
            String f = local_file.getName();
            if (!f.endsWith(".ht")) {
                long f_size = local_file.length();

                boolean tmp_fn = remote_files.contains(f);

                if (!tmp_fn) {
                    error_message = "[" + h + "] File missing: " + f;
//                    SSHActions.log(error_message);
                } else {
                    if (!remote_files.contains(String.valueOf(f_size))) {
                        error_message = "[" + h + "] Incorrect file size: " + f;
//                        SSHActions.log(error_message);
                        tmp_fn = false;
                    }
                }
                exists &= tmp_fn;
            }
        }
        return exists;
    }

    /**
     *
     * @param host IP address of the scale to test
     * @param delimiter used to find the file for use in upgrading
     * @return OFFLINE - if can't connect to get a list of files, YES - Files
     * are in host and match name and size, NO - Files may be there but no match
     * in size.
     */
    public int processOneScale(String host, String delimiter) {
        int result;
        String s_result;
        h = host;
        s_result = executeSSHCommand(host, "ls -l " + uploadPath + delimiter);

        if (!s_result.startsWith("<<error>>")) {
            if (filesInScale(s_result)) {
                result = YES;
            } else {
                result = NO;
            }
        } else {
            result = OFFLINE;
        }

        return result;
    }

    /**
     * This function validates if the given list of files exists in the scale,
     * on the first file not found in the scale a NO will be returned.
     *
     * @param host IP address of the scale to test
     * @param recoveryFilesToCheck list of files to verify existence in the
     * scale.
     * @return YES if All files in the list exist in the scale, otherwise NO is
     * returned on the first missing file.
     */
    public int processOneScale(String host, ArrayList<String> recoveryFilesToCheck) {
        String s_result;
        h = host;
        for (String sysfile : recoveryFilesToCheck) {
            s_result = executeSSHCommand(host, "test -f " + sysfile + " ; echo $?");
            System.out.println("result: " + s_result.trim());

            if (s_result.startsWith("1")) {
                error_message = "[" + host + "] " + sysfile + " file not found in scale.";
//                SSHActions.log(error_message);

                // System.out.printf("[%s] %s file not found in scale.\n", host, sysfile);
                return NO;
            } else {
                error_message = "[" + host + "] " + sysfile + " file found in scale.";
//                SSHActions.log(error_message);
                // System.out.printf("[%s] %s file found in scale.\n", host, sysfile);
            }
        }
        return YES;
    }

    public String getErrorMessage() {
        return error_message;
    }

    /**
     * Set the value of uploadPath, must include the last path separator
     * character '/'
     *
     * @param uploadPath new value of uploadPath
     */
    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

}
