package com.hobart.hte.filemgr.tasks;

import com.hobart.hte.filemgr.QueryManager;
import com.hobart.hte.filemgr.UpgradeManager;
import com.hobart.hte.filemgr.ssh.SSHTools;
import com.hobart.hte.filemgr.util.ScaleCommDriver;
import com.hobart.hte.filemgr.util.TaskStatus;
import java.util.logging.Logger;

/**
 *
 * @author lopezaz
 */
public class PostInstallThread implements Runnable {

    private final String host;
    private String upgradeId;
    private Logger mylog;
    private QueryManager _qm;
    private int maxRetry;
    private String newVersion;
    private String scaleModelType;

    public PostInstallThread(String host, String newVersion,
            String upgradeId, Logger log, QueryManager qm, int mr) {
        this.host = host;
        this.upgradeId = upgradeId;
        this.maxRetry = mr;
        this.mylog = log;
        this._qm = qm;
        this.newVersion = newVersion;
        this.scaleModelType = UpgradeManager.determineScaleModelType(host);
    }

    /**
     *
     */
    @Override
    public void run() {
        runTask();
    }

    /*
     * 1. connect to scale 
       2. retrieve firmware version and plu count 
       3. update state in DB 
       4. restore DB
     */
    public void runTask() {
        ScaleCommDriver scale = new ScaleCommDriver(mylog);
        BackupRestoreDelete brd = new BackupRestoreDelete();
        String[] versions;

        mylog.info("PostInstallTask.runTask()");
        mylog.info(String.format("[%s] postinstall process", host));
        // is done restoring scripted backup?
        if (scaleModelType.equals("GT")) {
            mylog.info("GT scale database restore handled by installUpgrade.py");
            
            int pluCount = scale.getPLUCount(host, mylog);
            if (pluCount > -1) {
                versions = scale.retrieveVersions(host);
                String currentVersion = versions[1];
                _qm.updateScaleVersions(upgradeId, versions);
                
                if (newVersion.equals(currentVersion)) {
                    log("New version mathes with upgrade file applied: " + newVersion);
                    _qm.updateStatus(upgradeId, host, TaskStatus.upgrade_completed_no_errors.name(), "new version matches");
                } else {
                    log("The current scale firmware version (" + currentVersion + ") does not match with upgrade file applied: " + newVersion);
                    _qm.updateStatus(upgradeId, host, TaskStatus.upgrade_completed_errors.name(), "versions do not match");
                }
            } else {
                log("Unable to retrieve PLU count from scale");
                _qm.updateStatus(upgradeId, host, TaskStatus.upgrade_completed_errors.toString(), "could not connect to scale");
            }
            // include plu count check, version check
        } else if (isDBRestoreDone(host)) {
            int pluCount = scale.getPLUCount(host, mylog);

            String currentVersion;
            mylog.info(String.format("[%s] PLU count retrieved: %d", host, pluCount));

            if (pluCount > -1) {
                // currentVersion = scale.getFirmwareVersion(host);
                versions = scale.retrieveVersions(host);
                currentVersion = versions[1];
                _qm.updateScaleVersions(upgradeId, versions);

                log("Restore database...");
                if (brd.restoreDB(host)) {
                    log("Database restored succesfully");
                } else {
                    log("An unexpected problem to restore database occurred: " + brd.errorMessage);
                }

                if (newVersion.equals(currentVersion)) {
                    log("New version matches with upgrade file applied: " + newVersion);
                    // ToDo: need to decide if upgrade files are deleted or not
                    _qm.updateStatus(upgradeId, host, TaskStatus.upgrade_completed_no_errors.toString(), "new version matches");
                } else {
                    log("The current scale firmware version (" + currentVersion + ") does not match with upgrade file applied: " + newVersion);
                    _qm.updateStatus(upgradeId, host, TaskStatus.upgrade_completed_errors.toString(), "versions do not match");
                }
            } else {
                log("Unable to retrieve PLU count from scale");
                _qm.updateStatus(upgradeId, host, TaskStatus.upgrade_completed_errors.toString(), "could not connect to scale");
            }
        } else {
            log("The scale is not ready yet, waiting 5 more minutes");
            // ToDo: wait 5 mins and retry, how to keep retries?
            _qm.updateScaleNextCheck(host, upgradeId, 5);
        }
        mylog.exiting("PostInstallTask", "runTask");
    }

    /**
     * Check if the original backup created by the upgrade script is restored or
     * still working on it, if file still being processed wait 5 minutes.
     *
     * @param host
     * @return True if the scale is ready to continue with the process, False if
     * the scale is offline or not done yet.
     */
    public boolean isDBRestoreDone(String host) {
        int retries = 0;
        String linuxCommand = "ls -l ";
        boolean dbInProcess;
        boolean backupInEtc;
        SSHTools _ssh = new SSHTools();
        _ssh.setLogger(mylog);

        // check SOFTWARE_VERSION_INFO for version and then talk to chobart_d
        linuxCommand += "/usr/local/hobart/download/ /usr/local/hobart/pending/";
        //myframe.updateStatusField(host, "Verifying scale status after core upgrade...");
        log("Verifying scale status after upgrade...");
        log("command: " + linuxCommand);
        String res1 = _ssh.executeSSHCommandVerbose(host, linuxCommand);
        log("response1 = [" + res1 + "], retry = " + retries);

        linuxCommand = "ls -l  /etc/hobart/verIndData";
        log("command: " + linuxCommand);
        String res2 = _ssh.executeSSHCommandVerbose(host, linuxCommand);
        log("response2 = [" + res2 + "]");

        if (res1.contains("<<error>>") || res2.contains("<<error>>")) {
            // scale offline
            log("Scale is offline");
            return false;
        }

        dbInProcess = res1.contains("verIndData");
        if (dbInProcess) {
            log("Scale online, waiting scale to be ready...");
        }

        backupInEtc = res2.contains("No such file or directory") || res2.isEmpty();
        if (backupInEtc) {
            log("/etc/hobart/verIndData file not found");
        }

        return !dbInProcess && backupInEtc;
    }

    /**
     *
     * @param m
     */
    private void log(String m) {
        if (mylog == null) {
            return;
        }
        mylog.info(String.format("[%s] %s", host, m));
    }
}
