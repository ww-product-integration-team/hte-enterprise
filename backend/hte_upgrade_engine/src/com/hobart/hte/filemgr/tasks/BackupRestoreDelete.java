package com.hobart.hte.filemgr.tasks;

import com.hobart.hte.filemgr.UpgradeManager;
import com.hobart.hte.filemgr.ssh.SSHTools;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.CountDownLatch;

/**
 *
 * @author William Zhang
 */
public class BackupRestoreDelete extends SSHTools implements Runnable {

    private final CountDownLatch startSignal;
    private final CountDownLatch doneSignal;
    public String linuxCommand;
    public String errorMessage;
    private final String Footer = "RTFF\u001e";
    private final String backupDir = "/etc/hobart/"; //Configuration.getInstance().getBackupDirectory();
    private String currentHost;
    private String result;

    public BackupRestoreDelete() {
        startSignal = null;
        doneSignal = null;
    }

    public BackupRestoreDelete(CountDownLatch startSignal, CountDownLatch doneSignal) {
        this.startSignal = startSignal;
        this.doneSignal = doneSignal;
    }

    /**
     *
     * @param data
     * @param out
     * @throws IOException
     * @throws SocketTimeoutException
     */
    public void sendData(String data, OutputStream out) throws IOException, SocketTimeoutException {
        for (int i = 0; i < data.length(); i++) {
            out.write((int) data.charAt(i));
        }
    }

    /**
     *
     * @param in
     * @return
     * @throws IOException
     * @throws SocketTimeoutException
     */
    public String readData(InputStream in) throws IOException, SocketTimeoutException {
        StringBuilder buf = new StringBuilder();
        boolean isReceiving = true;
        char c;

        while (isReceiving) {
            c = (char) in.read();
            buf.append(c);
            if (c == '\n') {
                buf.append("\u001d");
            }

            if (buf.toString().endsWith(Footer)) {
                isReceiving = false;
            }
        }
//        System.out.println("\nreadData(): "+buf.toString());
        return buf.toString();
    }

    /**
     * Sends order to create backup file in port 6000 and moves backup file it
     * to /usr/local/hobart/verIndData directory, backs up store graphic too.
     *
     * @param host IP address of the scale
     * @throws java.io.IOException
     * @updates errorMessage
     * @return Boolean whether the file was successfully uploaded
     * @throws InterruptedException
     */
    public boolean backupScale(String host) throws InterruptedException, IOException {
        Socket socket = new Socket();
        boolean res = true;

        //timeout waiting from message from chobart_d
        //after 20 minutes
//        int timeout = 20 * 60 * 1000;
        socket.connect(new InetSocketAddress(host, 6000), 30 * 1000);

        String cmd = "RTDO\u001fTNBackupEntireDB\u001e";
        sendData(cmd, socket.getOutputStream());

        Thread.sleep(10000);
        linuxCommand = "ls -l /etc/hobart/verIndData";
        String firstResult = executeSSHCommand(host, linuxCommand);
        Thread.sleep(30000);
        String secondResult = executeSSHCommand(host, linuxCommand);
        //Until the results of ls -l match with a delay, continue looping
        while (!firstResult.equals(secondResult)) {
            firstResult = executeSSHCommand(host, linuxCommand);
            Thread.sleep(30000);    // wait 30 seconds
            secondResult = executeSSHCommand(host, linuxCommand);
        }

        //When the loop breaks, the backup database has been successfully built
        //Moves the backup file to /usr/local/hobart/verIndData
        linuxCommand = "mv /etc/hobart/verIndData " + backupDir + "/verIndData";
        String tmp = executeSSHCommand(host, linuxCommand);
        System.out.println("backupHT(): " + tmp);
        res &= !tmp.equals("<<error>>");

        // backup store logo
        String scaleModelType = UpgradeManager.determineScaleModelType(host);
        if (scaleModelType.equals("GT")) {
            linuxCommand = "cp /opt/hobart/layouts/storelogo.png " + backupDir + "/storelogo.png";
        } else {
            linuxCommand = "cp /usr/local/hobart/layouts/storelogo.png " + backupDir + "/storelogo.png";
        }
        tmp = executeSSHCommand(host, linuxCommand);
        System.out.println("backupHT(): " + tmp);
        res &= !tmp.equals("<<error>>");
        socket.close();

        return res;
    }

    /**
     * Restores the backup file /usr/local/hobart/verIndData to
     * /etc/hobart/verIndData. Restores store graphic too.
     *
     * @param host IP address of the scale
     * @updates errorMessage if file could not be successfully moved
     * @return boolean whether the file was successfully moved
     */
    public boolean restoreDB(String host) {
        linuxCommand = "cd " + backupDir + " && ls";
        String directoryFileNames = executeSSHCommand(host, linuxCommand);
        //If the file is in the directory, move the file and return true
        if (directoryFileNames.contains("verIndData")) {
            String scaleModelType = UpgradeManager.determineScaleModelType(host);
            if (scaleModelType.equals("GT")) {
                linuxCommand = "mv " + backupDir + "/verIndData /opt/hobart/download/verIndData.ht";
                executeSSHCommand(host, linuxCommand);
                
                linuxCommand = "mv " + backupDir + "/storelogo.png /opt/hobart/layouts/storelogo.png";
            } else {
                linuxCommand = "mv " + backupDir + "/verIndData /usr/local/hobart/download/verIndData.ht";
                executeSSHCommand(host, linuxCommand);

                // restore store logo
                linuxCommand = "mv " + backupDir + "/storelogo.png /usr/local/hobart/layouts/storelogo.png";
                executeSSHCommand(host, linuxCommand);
            }

            return true;
        } //If the file is not in the directory, update errorMessage and return false
        else {
            errorMessage = "Error: Backup file cannot be found.";
            return false;
        }
    }

    /**
     * Removes the backup file /usr/local/hobart/verIndData Updates
     *
     * @param host IP address of the scale
     * @updates errorMessage
     * @return boolean whether the file was successfully deleted
     */
    public boolean deleteBackup(String host) {
        linuxCommand = "cd " + backupDir + " && ls";
        String directoryFileNames = executeSSHCommand(host, linuxCommand);
        //If the file is in the directory, delete the file and return true
        if (directoryFileNames.contains("verIndData")) {
            linuxCommand = "rm " + backupDir + "/verIndData";
            executeSSHCommand(host, linuxCommand);
            return true;
        } //If the file is not in the directory, update errorMessage and return false
        else {
            errorMessage = "Error: Backup file does not exist.";
            return false;
        }
    }

    /**
     *
     * @param ch
     */
    public void setCurrentHost(String ch) {
        currentHost = ch;
    }

    /**
     *
     * @return
     */
    public String getHost() {
        return currentHost;
    }

    /**
     *
     * @return
     */
    public String getResult() {
        return result;
    }

    /**
     *
     */
    @Override
    public void run() {
        System.out.println("backupHT(): starting = " + currentHost);
        try {
            if (backupScale(currentHost)) {
                result = "Backup successful";
            } else {
                result = "Backup with errors";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = ex.getMessage();
        }
        System.out.println("backupHT(): finishing = " + currentHost);
        doneSignal.countDown();
    }
}
