package com.hobart.hte.filemgr;

import com.jcraft.jsch.SftpProgressMonitor;
import java.util.LinkedHashMap;

public class CustomProgressMonitor implements SftpProgressMonitor {
    private LinkedHashMap<String, Double> progressMap = UpgradeManager.getHostHashMap();
    private long fileSize;
    private long bytesTransferred;
    private final String host;
    private static final long MEGABYTE = 1024L * 1024L;
    private double currentProgress;

    /* USE COMMENTED CODE IF YOU WOULD LIKE TO STOP FILE TRANSFERS AS SOON AS THE UPLOAD WINDOW ENDS */
//    private LocalDateTime endTime;
//    private Session session;

//    public CustomProgressMonitor(long fileSize, String host, LocalDateTime endTime, Session session ) {
//        this.fileSize = fileSize;
//        this.host = host;
//        this.endTime = endTime;
//        this.session = session;
//        progressMap.put(host, 0.0); // Initialize the progress for the host
//    }

    public CustomProgressMonitor(long fileSize, String host ) {
        this.fileSize = fileSize;
        this.host = host;
        progressMap.put(host, 0.0); // Initialize the progress for the host
    }

    public CustomProgressMonitor (long fileSize, String host, boolean fileIsUploaded){
        this.fileSize = fileSize;
        this.host = host;
        if (fileIsUploaded){
            progressMap.put(host, 100.00);
        }else{
            progressMap.put(host, 0.0);
        }
    }

    @Override
    public void init(int op, String src, String dest, long max) {
        fileSize = max;
    }

    @Override
    public synchronized boolean count(long count) {
//        if (LocalDateTime.now().isAfter(endTime)){
//            session.disconnect();
//            return false;
//        }

        progressMap = UpgradeManager.getHostHashMap();

        bytesTransferred += count;
        double percentage = (double) (bytesTransferred * 100) / fileSize;
        // Update the progress for the host
        progressMap.put(host, percentage);


        if (percentage > currentProgress && percentage > 0) {
            currentProgress = percentage;
            // Clear the console
            System.out.flush();
            // Print the progress for all hosts on the same line
            StringBuilder progressBuilder = new StringBuilder();

            progressMap.forEach((key, value) -> {
                if (value == 100.00) {
                    progressBuilder.append(String.format("[%s] Progress: Complete, calculating remote checksum...     ", key));
                } else {
                    progressBuilder.append(String.format("[%s] Progress: %.2f%%     ", key, value));
                }
            });

        System.out.print("\r" + progressBuilder);

        }
        return true;
    }

    @Override
    public void end() {
    }

}