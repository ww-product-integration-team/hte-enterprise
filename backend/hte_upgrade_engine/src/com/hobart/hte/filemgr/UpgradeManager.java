package com.hobart.hte.filemgr;

import com.hobart.hte.filemgr.ssh.SSHTools;
import com.hobart.hte.filemgr.tasks.PostInstallThread;
import com.hobart.hte.filemgr.util.MyLogFormatter;
import com.hobart.hte.filemgr.util.TaskStatus;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.HashMap;
import java.time.LocalDateTime;
import java.util.concurrent.ScheduledExecutorService;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.RejectedExecutionException;

import com.mysql.cj.exceptions.CJCommunicationsException;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpgradeManager {

    private static int maxThreads;
    private static final Logger logger = Logger.getLogger("uploadLog");
    private static final Logger upgradeLogger = Logger.getLogger("upgradeLog");
    private static FileHandler fh;
    private static ThreadPoolExecutor executor;
    private static ScheduledFuture scheduleRemoteUpdateAtFixedRate;
    private static final LinkedHashMap<String, Double> hostHashMap = new LinkedHashMap<>();
    private static String sessUsername;
    private static String sessPassword;
    private static String sessHost;
    private static final Path configPath = Paths.get("dbconfig.ini");
    private static final long reconnectInterval = 5000; // 5 seconds
    private QueryManager queryManager;
    private int maxRetries;
    private String repoDir;
    
    public static final List<String> GLOBAL_SCALE_FILE_TYPES = Arrays.asList(".deb", ".tgz", ".img.bz2");

    /**
     *
     */
    public UpgradeManager() {
        // create main upload logger
        try {
            fh = new FileHandler("uploadLog.txt", true);
            logger.addHandler(fh);
            fh.setFormatter(new MyLogFormatter());
        } catch (SecurityException | IOException e) {
            System.err.println("Unable to start log: " + e.getMessage());
        }

        // create main upgrade logger
        try {
            fh = new FileHandler("upgradeLog.txt", true);
            upgradeLogger.addHandler(fh);
            fh.setFormatter(new MyLogFormatter());
        } catch (SecurityException | IOException e) {
            System.err.println("Unable to start log: " + e.getMessage());
        }

        Properties properties = new Properties();
        try (InputStream input = new FileInputStream(configPath.toString())) {
            properties.load(input);
        } catch (IOException e) {
            log(e.getMessage(), e);
            return;
        }

        queryManager = new QueryManager(logger,
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password"),
                Integer.parseInt(properties.getProperty("dbConnectRetries")));

        maxThreads = queryManager.getConfigurationInt("maxActiveThreads", 10);
        maxRetries = queryManager.getConfigurationInt("maxRetries", 2);
        repoDir = queryManager.getConfiguration("repositoryPath", "C:\\Users\\Public\\Documents\\hte\\repository");
        //repoDir = "E:\\hte\\repository";
        if (!repoDir.contains("upgrades")) {
            repoDir = Paths.get(repoDir, "upgrades").toString();
        }        
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxThreads);
    }

    /**
     *
     */
    public void startUpgradeServices() {
        log("\n\n==========================================  Starting Upgrade Service  ==========================================");

        HashMap<String, Runnable> threads = new HashMap<>();

        log("local repository: " + repoDir);

        // create task to run every minute looking for upload requests
        ScheduledExecutorService remoteUploadRequestTask = createUploadTask();

        // --------------------------
        ScheduledExecutorService remoteUgradeRequestTask = createUpgradeTask();
        
// create post install task to run every minute 
        ScheduledExecutorService postInstallTask = createPostinstallTask();
        // is this block necessary? 
        boolean errorLogged = true;
        while (true) {
        }

    }

    public void log(String msg) {
        logger.info(msg);
    }

    public void log(String msg, boolean err) {
        if (err) {
            logger.warning(msg);
        } else {
            logger.info(msg);
        }
    }

    public static void log(String message, Exception ex) {
        logger.log(Level.SEVERE, message, ex);
    }

    public static void log(Exception ex) {
        logger.log(Level.SEVERE, ex.getMessage(), ex);
    }

    public static void addHost(String host, Double startingValue) {
        hostHashMap.put(host, startingValue);
    }

    public static LinkedHashMap<String, Double> getHostHashMap() {
        return hostHashMap;
    }

    public static void removeHost(String host) {
        hostHashMap.remove(host);
    }

    private ScheduledExecutorService createUploadTask() {
        ScheduledExecutorService _tmp = Executors.newScheduledThreadPool(1);
        scheduleRemoteUpdateAtFixedRate = _tmp.scheduleAtFixedRate(() -> {
            String host;
            try {
                logger.info("[upload] querying the DB for uploads to process...");
                ResultSet resultSet = queryManager.getScalesToUpload();

                int numResults = 0;
                int numErrResults = 0;

                while (resultSet.next()) {
                    numResults++;
                    String upgradeId = resultSet.getString("upgradeId");
                    String fileName = resultSet.getString("fileName");
                    String checksum = resultSet.getString("checksum");
                    host = resultSet.getString("ipAddress");
                    String groupId = resultSet.getString("groupId");
                    String storeId = null;
                    try {
                        resultSet.findColumn("storeId");
                        storeId = resultSet.getString("storeId");
                    } catch (SQLException e) {
                        logger.info(String.format("StoreId not found in result set of scale query. All scales will be treated as primaries."));
                    }
                    
                    LocalDateTime end = resultSet.getTimestamp("uploadEnd").toLocalDateTime();
                    //String batchId = resultSet.getString("batchId");

                    logger.info(String.format("[upload event] upgradeId: %s\nfile name: %s\nchecksum: %s\nhostname: %s\nstoreId: %s",
                            upgradeId, fileName, checksum, host, storeId));

                    Runnable worker = new UploadFileThread(checksum, host,
                            storeId, groupId, fileName, end, repoDir,
                            upgradeId, logger, queryManager);
                    ((UploadFileThread) worker).setMaxRetry(maxRetries);

                    //threads.put(host, worker);
                    try {
                        // add task to thread pool 
                        executor.execute(worker);
                    } catch (RejectedExecutionException e) {
                        log(e.getMessage(), true);
                        numErrResults++;
                    }

                    // change status to upgrade_inprogress
//                    int rowsAffected = queryManager.updateStatus(upgradeId, host, "upload_in_progress", "");  //.setQueuedStatus(upgradeId);
//                    if (rowsAffected == 1) {
//                        log(String.format("Thread queued for host %s", host));
//                    } else {
//                        log(String.format("Status change failed. %s Rows were Affected!", rowsAffected), true);
//                    }
                }
                logger.info(String.format("[upload] number of requests received: %d\n", numResults));
                if (numResults >= 1) {
                    log(String.format("%s Upgrades collected and added to queue, %s Errors were produced", numResults, numErrResults));
                }
            } catch (CJCommunicationsException ee) {
                System.out.println("Error:");
                logger.severe(ee.toString());
            } catch (CommunicationsException | SQLNonTransientConnectionException ex) {
                log(ex.toString());
            } catch (Exception e) {
                e.printStackTrace();
                log(e.toString());
                System.exit(-1);
            }
        },
                60 - LocalDateTime.now().getSecond(),
                TimeUnit.MINUTES.toSeconds(1), // execute every minute
                TimeUnit.SECONDS);

        return _tmp;
    }

    /**
     * Create process to launch scale upgrades. Multi-threaded process to
     * prepare scales to execute the upgrade. It updates the status on the
     * database table
     *
     * @return
     */
    private ScheduledExecutorService createUpgradeTask() {

        ScheduledExecutorService _tmp = Executors.newScheduledThreadPool(1);
        scheduleRemoteUpdateAtFixedRate = _tmp.scheduleAtFixedRate(() -> {
            upgradeLogger.info("=== upgrade scale process ===");
            String host = "";

            try {
                upgradeLogger.info("[upgrade] querying the DB for upgrades to process...");
                ResultSet resultSet = queryManager.getScalesToUpgrade();
                int numResults = 0;
                int numErrResults = 0;

                while (resultSet.next()) {
                    numResults++;

                    String upgradeId = resultSet.getString("upgradeId");
                    String fileName = resultSet.getString("fileName");
                    String checksum = resultSet.getString("checksum");
                    host = resultSet.getString("ipAddress");
                    LocalDateTime end = resultSet.getTimestamp("upgradeEnd").toLocalDateTime();

                    upgradeLogger.info(String.format("[upgrade event] upgradeId: %s\nfile name: %s\nchecksum: %s\nhostname: %s",
                            upgradeId, fileName, checksum, host));

                    Runnable worker = new UpgradeScaleThread(host, fileName, end,
                            upgradeId, upgradeLogger, queryManager);
                    ((UpgradeScaleThread) worker).setMaxRetry(maxRetries);
                    
// should this be handled per batch instead of a global config? 
                    int waitMinutes = resultSet.getInt("afterUpgradeCheckIn");
                    ((UpgradeScaleThread) worker).setWaitMinutes(waitMinutes);
                    ((UpgradeScaleThread) worker).setLocalUpgradeRepository(repoDir);
                    // ToDo: add this flag ... 
                    //((UpgradeScaleThread) worker).setValidateRecovery_files(true);

                    //threads.put(host, worker);
                    try {
                        // add task to thread pool 
                        executor.execute(worker);
                    } catch (RejectedExecutionException e) {
                        log(e.getMessage(), true);
                        numErrResults++;
                    }

                    // change status to upgrade_inprogress
//                    int rowsAffected = queryManager.setQueuedStatus(upgradeId);
//                    if (rowsAffected == 1) {
//                        log(String.format("Thread queued for host %s", host));
//                    } else {
//                        log(String.format("Status change failed. %s Rows were Affected!", rowsAffected), true);
//                    }
                }

                upgradeLogger.info(String.format("[upgrade] number of requests received: %d ", numResults));
                if (numResults >= 1) {
                    log(String.format("%s Upgrades collected and added to queue, %s Errors were produced", numResults, numErrResults));
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                upgradeLogger.severe(String.format("An error occurred while processing upgrade request for: %s ", host));
            }

        },
                60 - LocalDateTime.now().getSecond(),
                TimeUnit.MINUTES.toSeconds(1), // execute every minute
                TimeUnit.SECONDS);

        return _tmp;
    }

    /**
     *
     * @return
     */
    private ScheduledExecutorService createPostinstallTask() {
        ScheduledExecutorService _tmp = Executors.newScheduledThreadPool(1);
        scheduleRemoteUpdateAtFixedRate = _tmp.scheduleAtFixedRate(() -> {
            upgradeLogger.info("=== Post install upgrade process ===");
            String host = "";

            try {
                logger.info("[post install] querying the DB for post upgrade scales to process...");
                ResultSet resultSet = queryManager.getScalesPostInstall();
                int numResults = 0;
                int numErrResults = 0;

                while (resultSet.next()) {
                    numResults++;
                    String upgradeId = resultSet.getString("upgradeId");
                    Timestamp timestamp = resultSet.getTimestamp("nextCheck");
                    host = resultSet.getString("ipAddress");
                    String newVersion = parseVersion(resultSet.getString("filename"));

                    upgradeLogger.info(String.format("[post install event] upgradeId: %s\nnext check: %s\nIP address: %s",
                            upgradeId, timestamp.toString(), host));

                    // change status on scale 
                    queryManager.updateStatus(upgradeId, host,
                            TaskStatus.post_install_process.name(), "post install process...");
                    // process scales in post install 
                    Runnable worker = new PostInstallThread(host, newVersion, upgradeId, 
                            upgradeLogger, queryManager, maxRetries);
                    try {
                        executor.execute(worker);
                    } catch (RejectedExecutionException ex) {
                        log(ex.getMessage(), true);
                        numErrResults++;
                    }
                    // if the scale is not ready, update next check field to comeback in 5 minutes
                }

                logger.info(String.format("[post install] number of requests received: %d ", numResults));
                if (numResults >= 1) {
                    log(String.format("%s Upgrades collected and added to queue, %s Errors were produced", numResults, numErrResults));
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                upgradeLogger.severe(String.format("An error occurred while processing post upgrade request for: %s ", host));
            }
        },
                60 - LocalDateTime.now().getSecond(),
                TimeUnit.MINUTES.toSeconds(1), // execute every minute
                TimeUnit.SECONDS);

        return _tmp;
    }

    /**
     * Extract firmware version from the upgrade file
     *
     * @param filename Filename of the feature file that contains the firmware
     * version
     * @return The version number, if it can't be parsed it returns "N/A"
     */
    private String parseVersion(String filename) {
        if (filename == null || filename.isEmpty()) {
            return "N/A";
        }
        
        Pattern pattern = Pattern.compile("\\d+\\.\\d+\\.\\d+");
        Matcher matcher = pattern.matcher(filename);
        if (matcher.matches() || matcher.find()) {
            return matcher.group();
        }
        
        return "N/A";
    }
    
    public static String determineScaleModelType(String host) {
        SSHTools tools = new SSHTools();
        tools.setLogger(logger);
        String scaleModelType;
        logger.info(String.format("[%s] determining scale model type", host));
        String results = tools.executeSSHCommandVerbose(host, "cd /etc ; cat os-version");
        if (results.contains("Timesys")) {
            scaleModelType = "HT";
        } else {
            scaleModelType = "GT";
        }
        logger.info(String.format("[%s] scale model type: %s", host, scaleModelType));
        return scaleModelType;
    }
    
    public static boolean isGTFileType(String filename) {
        return GLOBAL_SCALE_FILE_TYPES.stream().map(type -> filename.endsWith(type)).anyMatch(res -> res == true);
    }
}
