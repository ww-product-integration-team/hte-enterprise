package com.hobart.hte.filemgr.ssh;

import com.hobart.hte.filemgr.UpgradeManager;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Logger;

/**
 *
 * @author lopezaz
 */
public class SSHTools {

    private final String SCALE_USER = "root";
    private final String SCALE_PASS = "ho1bart2";
    private final int MAX_RETRIES = 5;
    protected int exit_status;
    private int timeout;
    private Logger _log;
    private String remoteUploadPath;

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setLogger(Logger l) {
        _log = l;
    }

    /**
     *
     * @param rup
     */
    public void setRemoteUploadPath(String rup) {
        this.remoteUploadPath = rup;
    }

    /**
     *
     * @param host
     * @return
     */
    protected Session openSSHConnection(String host) {
        Session my_session;
        try {
            JSch jsch = new JSch();
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");

            my_session = jsch.getSession(SCALE_USER, host, 22);
            my_session.setPassword(SCALE_PASS);
            my_session.setConfig(config);
            my_session.setTimeout(timeout * 1000);
            my_session.connect();

            return my_session;
        } catch (Exception ex) {
            //ex.printStackTrace();
            _log.info("[" + host + "] error = " + ex.toString());
            return null;
        }
    }

    /**
     * Executes the given cmd command in the current host and waits for the
     * response
     *
     * @param host
     * @param cmd linux command to execute remotely
     * @return if any output stream from the host
     */
    protected String executeSSHCommand(String host, String cmd) {
        return executeSSHCommand(host, cmd, true);
    }

    /**
     * Executes the given cmd command in the current host and doesn't wait for
     * the response
     *
     * @param host
     * @param cmd linux command to execute remotely
     */
    public String executeSSHCommandRemotely(String host, String cmd) {
        return executeSSHCommand(host, cmd, false);
    }

    /**
     * Executes the given cmd command in the current host and wait for the reply
     *
     * @param host
     * @param cmd linux command to execute remotely
     */
    public String executeSSHCommandVerbose(String host, String cmd) {
        return executeSSHCommand(host, cmd, true);
    }

    /**
     *
     * @param host
     * @param dir The remote directory to connect
     * @return
     */
    protected ChannelSftp getSFTPConnection(String host, String dir) {
        ChannelSftp channelSftp;
        Channel channel;
        Session my_session;

        try {
            JSch jsch = new JSch();
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");

            my_session = jsch.getSession(SCALE_USER, host, 22);
            my_session.setPassword(SCALE_PASS);
            my_session.setConfig(config);
            my_session.setTimeout(timeout * 1000);
            _log.info("Connecting to: " + host);
            my_session.connect();

            _log.info("opening sftp channel...");
            channel = my_session.openChannel("sftp");
            channel.connect(timeout * 1000);
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(dir);

            _log.info("Connected to: " + host);
            return channelSftp;

        } catch (JSchException | SftpException ex) {
//            ex.printStackTrace();
            _log.info("An error occurred while connecting: " + ex.toString());
            return null;
        }
    }

    /**
     * Uploads an array of files to the indicated host using SFTP protocol in
     * the designated directory
     *
     * @param host
     * @param files
     * @param SFTP_WORKING_DIR
     * @return True if all successful, otherwise False
     */
    protected boolean uploadFiles(String host, File files[], String SFTP_WORKING_DIR) {

        boolean result = true;
        for (File fname : files) {
            _log.info("Processing file: " + fname);
            result &= uploadOneFile(host, fname, SFTP_WORKING_DIR);
        }

        return result;
    }

    /**
     *
     * @param host IP address or hostname of the scale to connect to
     * @param fname
     * @param SFTP_WORKING_DIR directory in the remote scale where the file will
     * be uploaded
     * @return True if successful, otherwise False
     */
    protected boolean uploadOneFile(String host, File fname, String SFTP_WORKING_DIR) {

        ChannelSftp channelSftp = getSFTPConnection(host, SFTP_WORKING_DIR);
        int upload_mode = ChannelSftp.OVERWRITE;

        if (channelSftp == null) {
            log(host, "Unnable to establish connection to scale");
            return false;
        }

        int retries = 0;

        do {
            try {
//                long currentFileSize = fname.length();
                _log.info("executing put command");
                channelSftp.put(new FileInputStream(fname), fname.getName(), upload_mode);
                break;    // file was transfered correctly 

            } catch (Exception ex) {
//                ex.printStackTrace();
                log(host, "error: " + ex.toString());
                log(host, "waiting 5 seconds before retrying: " + (retries + 1) + "/5");
//                wait(5);

                if (channelSftp == null || !channelSftp.isConnected()) {
                    // reconnect
                    channelSftp = getSFTPConnection(host, SFTP_WORKING_DIR);

                }
            }

        } while (retries++ < MAX_RETRIES);

        if (retries == MAX_RETRIES) {
            _log.info("After 5 retries the program couldn't reconnect to scale.\nAborting upload, try later.");
            return false;
        }

        try {
            channelSftp.disconnect();
            channelSftp.getSession().disconnect();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * Find file with the largest size in order to monitor upload progress
     *
     * @param files
     * @return File object with the largest size of all of the list
     */
    protected File getLargestFile(File[] files) {
        File bigf = null;
        for (File f : files) {
            if (bigf != null && bigf.length() < f.length()) {
                bigf = f;
            }
        }
        return bigf;
    }

    /**
     * Executes the given cmd command in the current host
     *
     * @param host
     * @param cmd linux command to execute remotely
     * @param observe True the code will wait for an output in the console,
     * False won't wait for response
     * @return if any output stream from the host
     */
    protected String executeSSHCommand(String host, String cmd, boolean observe) {
        Session my_session;

        if ((my_session = openSSHConnection(host)) == null) {
            log(host, "Unnable to connect to host");
            return "<<error>> Unnable to connect to host";
        }

        ChannelExec channel = null;
        String toReturn = null;
        try {
            channel = (ChannelExec) my_session.openChannel("exec");
            channel.setCommand(cmd);
            channel.setInputStream(null);
            channel.setErrStream(System.err);

            InputStream in = channel.getInputStream();
            InputStream err = channel.getErrStream();
            channel.connect();
            int buffersize = 1024;
            byte[] tmp = new byte[buffersize];

            StringBuilder sb = new StringBuilder();
            //If observe parameter, wait for output, if false, skip.
            if (observe) {
                while (true) {
                    while (in.available() > 0) {
                        int i = in.read(tmp, 0, buffersize);
                        if (i < 0) {
                            System.out.print(sb.toString());
                            break;
                        }
                        sb.append(new String(tmp, 0, i));
                    }
                    while (err.available() > 0) {
                        log(host, "error reported while executing command: " + cmd);
                        int i = err.read(tmp, 0, buffersize);
                        if (i < 0) {
                            System.out.println(sb.toString());
                            break;
                        }
                        sb.append(new String(tmp, 0, i));
                    }
                    if (channel.isClosed() || !channel.isConnected()) {
                        if (in.available() > 0) {
                            continue;
                        }
                        exit_status = channel.getExitStatus();
//                        System.out.println("executeSSHCommand(): exit-status= " + exit_status);
//                        System.out.println("data received: " + sb.toString());
//                        System.out.println(host);
                        log(host, "executeSSHCommand(): exit-status= " + exit_status);
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ee) {
                    }
                }
            } else {
                sb.append("No observations");
            }

            channel.disconnect();
            toReturn = sb.toString();
        } catch (JSchException | IOException ex) {
//            ex.printStackTrace();
            log(host, "An error occurred: " + ex.toString());
            toReturn = "<<error>>";
        } finally {
            log(host, "finally block, closing channel and session");
            if (channel != null && channel.isConnected()) {
                channel.disconnect();
            }
            my_session.disconnect();
            log(host, "session and channel closed");
        }
        
        return toReturn;
    }

    /**
     *
     * @param host
     * @param cmd
     * @return
     */
    public String executeSSHCommand(String host, ArrayList<String> cmd) {

        StringBuilder result = new StringBuilder();
        String output;
        Session my_session = openSSHConnection(host);
        if (my_session == null) {
            return "Scale Off line,";
        }

        for (String c : cmd) {
            try {
                System.out.println("executeSSHCommand(): executing = " + c);
                Channel channel = my_session.openChannel("exec");
                ((ChannelExec) channel).setCommand(c);
                channel.setInputStream(null);
                ((ChannelExec) channel).setErrStream(System.err);

                InputStream in = channel.getInputStream();
                channel.connect();
                int buffersize = 1024;
                byte[] tmp = new byte[buffersize];

                while (true) {
                    while (in.available() > 0) {
                        int i = in.read(tmp, 0, buffersize);
                        if (i < 0) {
                            System.out.print(result.toString());
                            break;
                        }
                        result.append(new String(tmp, 0, i));
                    }
                    if (channel.isClosed() || !channel.isConnected()) {
                        if (in.available() > 0) {
                            continue;
                        }
                        exit_status = channel.getExitStatus();
                        System.out.println("executeSSHCommand(): exit-status = " + exit_status);
                        //_log.info("[" + host + "] executeSSHCommand(): exit-status= " + exit_status);
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ee) {
                    }
                }
                result.append(System.getProperty("line.separator"));
                //output = result.toString();

            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex.getMessage());
            }
        }
//        System.out.printf(">> (%s): %s\n", host, result.toString());

        return result.toString();
    }

    /**
     *
     * @param host
     * @param remoteDir
     * @return
     */
    public ArrayList listRemoteDirectory(String host, String remoteDir) {

        ChannelSftp sftp = getSFTPConnection(host, remoteDir);
        ArrayList<String> _files;

        try {
            Vector<ChannelSftp.LsEntry> files = sftp.ls(remoteDir);

            HashMap hm = new HashMap();
            _files = new ArrayList();
            for (ChannelSftp.LsEntry f : files) {
                _files.add(f.getFilename());
            }

        } catch (SftpException ex) {
            return null;
        }
        return _files;
    }

    /**
     * delete old upgrade files but don't delete current upgrade files
     *
     * @param host IP address of the scale to work with
     * @param remotePath path in the scale to scan for files to delete
     * @param do_not_delete list of files to exclude from the list of files to
     * @param scaleModelType HT or GT
     * delete
     * @return
     */
    public String deleteOldFiles(String host, String remotePath, ArrayList<String> do_not_delete, String scaleModelType) {
        ArrayList<String> remoteFiles = listRemoteDirectory(host, remotePath);

        if (remoteFiles == null) {
            _log.info("[" + host + "] could not connect to scale to check files in scale");
            return "<<Error>> could not connect to scale to check files in scale";
        }
        _log.info("[" + host + "] BEFORE directory listing: " + remoteFiles.toString());

        if (remoteUploadPath == null) {
            remoteUploadPath = remotePath;
        }

        ArrayList<String> cmds = new ArrayList();

        for (String tmp : remoteFiles) {
//            System.out.println("deleteOldFiles: " + tmp);
            if (scaleModelType.equals("HT")) {
                if (tmp.startsWith("hti-") && do_not_delete.indexOf(tmp) == -1) {

                    if (tmp.contains(".package.tgz")) {
                        System.out.println(">>> delete FILE from the scale: " + tmp);
                        cmds.add("rm " + remoteUploadPath + tmp);
                    } else {
                        System.out.println(">>> delete DIRECTORY from the scale: " + tmp);
                        cmds.add("rm -r " + remotePath + tmp.replaceAll(".package.tgz", "") + "/");
                    }
                }
            } else {
                if (tmp.startsWith("gt-") && do_not_delete.indexOf(tmp) == -1) {
                    if (UpgradeManager.isGTFileType(tmp)) {
                        System.out.println(">>> delete FILE from the scale: " + tmp);
                        cmds.add("rm " + remoteUploadPath + tmp);
                    } else {
                        System.out.println(">>> delete DIRECTORY from the scale: " + tmp);
                        String replaced = tmp.replaceAll(".deb", "").replaceAll(".tgz", "").replaceAll(".img.bz2", "");
                        cmds.add("rm -r " + remotePath + replaced + "/");
                    }
                }
            }
            if (tmp.endsWith(".py")) {
                _log.info(String.format(">>> delete FILE from the scale: %s", tmp));
                cmds.add("rm " + remoteUploadPath + tmp);
            }
            
            System.out.printf(">>> commands to delete: \n%s\n", cmds.toString());
            _log.info(String.format(">>> commands to delete: %s", cmds.toString()));
        }

        String fileNaming;
        if (scaleModelType.equals("HT")) {
            fileNaming = "(hti-*.package.tgz)";
        } else {
            fileNaming = "(gt-*(.deb|.tgz|.img.bz2))";
        }
        if (cmds.isEmpty()) {
            return String.format("Number of files in remote path: %d, but no upgrade files to delete %s.", remoteFiles.size(), fileNaming);
        }
        String output = executeSSHCommand(host, cmds);

        System.out.printf(">>> commands output(%s): \n%s\n", host, output);

        remoteFiles = listRemoteDirectory(host, remotePath);
        _log.info("[" + host + "] AFTER directory listing: " + remoteFiles.toString());

        return output;
    }

    /**
     *
     * @param secs seconds to wait
     */
    public void wait(int secs) {
        try {
            Thread.sleep(secs * 1000);
        } catch (InterruptedException ex) {
        }
    }

    private void log(String h, String m) {
        if (_log == null) {
            return;
        }
        _log.info(String.format("[%s] %s", h, m));
    }
}
