package com.hobart.hte.utils.profile;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hobart.hte.utils.file.FileForEvent;
import io.swagger.v3.oas.annotations.media.Schema;

import java.sql.Timestamp;
import java.util.List;

@Schema(description = "This class represents a profile with its basic information. A profile associates a group of configuration files for Hobart scales.")
public class ProfileResponse {
    private String profileId;
    private String name;
    private String description;
    private Timestamp lastUpdate;
    private String requiresDomainId;
    private String downloadUrl;
    private String zoneId;
    private List<FileForEvent> listOfFiles;

    public ProfileResponse(){
        super();
    }

    public ProfileResponse(Profile p, List<FileForEvent> list, String downloadUrl) {
        super();
        this.profileId = p.getProfileId();
        this.name = p.getName();
        this.description = p.getDescription();
        this.lastUpdate = p.getLastUpdate();
        this.requiresDomainId = p.getRequiresDomainId();
        this.zoneId = p.getZoneId();
        this.downloadUrl = downloadUrl;
        this.listOfFiles = list;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getRequiresDomainId() {
        return requiresDomainId;
    }

    public void setRequiresDomainId(String requiresDomainId) {
        this.requiresDomainId = requiresDomainId;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public List<FileForEvent> getListOfFiles() {
        return listOfFiles;
    }

    public void setListOfFiles(List<FileForEvent> listOfFiles) {
        this.listOfFiles = listOfFiles;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public Profile toProfile(){
        Profile profile = new Profile();
        profile.setProfileId(profileId);
        profile.setName(name);
        profile.setDescription(description);
        profile.setLastUpdate(lastUpdate);
        profile.setRequiresDomainId(requiresDomainId);
        profile.setZoneId(zoneId);
        return profile;
    }
}

