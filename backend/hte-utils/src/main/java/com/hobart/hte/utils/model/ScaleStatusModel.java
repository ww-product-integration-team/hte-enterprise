package com.hobart.hte.utils.model;

public class ScaleStatusModel {
	private String scaleIP;
	private String firmwareVersion;
	private String scaleType;
	private String storeId;
	private int pluCount;
	private long freeMemory;
	private long totalMemory;

	public ScaleStatusModel() {

	}

	public ScaleStatusModel(String ip, String taggedRecord) {
		this.scaleIP = ip;
		String tmp = getValue(taggedRecord, "VE");
		tmp += "." + getValue(taggedRecord, "Re");
		this.firmwareVersion = tmp;
		this.freeMemory = Long.parseLong(getValue(taggedRecord, "fm"));
		//The 'me' tag is actually the used memory so add it to the free memory to get the total
		this.totalMemory = Long.parseLong(getValue(taggedRecord, "me")) + this.freeMemory;
		this.pluCount = Integer.parseInt(getValue(taggedRecord, "#p"));
		this.scaleType = getValue(taggedRecord, "Ms");
		this.storeId = getValue(taggedRecord, "si");
	}

	public String getScaleIP() {
		return scaleIP;
	}

	public void setScaleIP(String scaleIP) {
		this.scaleIP = scaleIP;
	}

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	public String getScaleType() {
		return scaleType;
	}

	public void setScaleType(String scaleType) {
		this.scaleType = scaleType;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public int getPluCount() {
		return pluCount;
	}

	public void setPluCount(int pluCount) {
		this.pluCount = pluCount;
	}

	public long getFreeMemory() {
		return freeMemory;
	}

	public void setFreeMemory(long freeMemory) {
		this.freeMemory = freeMemory;
	}

	public long getTotalMemory() {
		return totalMemory;
	}

	public void setTotalMemory(long totalMemory) {
		this.totalMemory = totalMemory;
	}

	/**
	 *
	 * @param sb
	 * @param tag
	 * @return
	 */
	public String getValue(String sb, String tag) {
		int i = sb.indexOf("\u001f" + tag);
		if (i == -1) {
			return null;
		}
		String value = sb.substring(i + tag.length() + 1);
		try {
			value = value.substring(0, value.indexOf("\u001f"));
		} catch (Exception e) {
			value = value.substring(0, value.indexOf("\u001e"));
		}
		return value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ScaleStatusModel [scaleIP=");
		builder.append(scaleIP);
		builder.append(", firmwareVersion=");
		builder.append(firmwareVersion);
		builder.append(", scaleType=");
		builder.append(scaleType);
		builder.append(", storeId=");
		builder.append(storeId);
		builder.append(", pluCount=");
		builder.append(pluCount);
		builder.append(", freeMemory=");
		builder.append(freeMemory);
		builder.append(", totalMemory=");
		builder.append(totalMemory);
		builder.append("]");
		return builder.toString();
	}
	
	
}
