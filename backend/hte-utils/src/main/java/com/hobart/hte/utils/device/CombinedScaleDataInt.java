package com.hobart.hte.utils.device;

import java.sql.Timestamp;

public interface CombinedScaleDataInt {
     String getDeviceId();
     String getIpAddress();
     String getHostname();
     Long getSerialNumber();
     String getScaleModel();
     String getStoreId();
     String getDeptId();
     String getCountryCode();
     String getBuildNumber();
     String getApplication();
     String getOperatingSystem();
     String getSystemController();
     String getLoader();
     String getSmBackend();
     Timestamp getLastReportTimestampUtc();
     String getLastTriggerType();
     String getProfileId();
     Boolean getIsPrimaryScale();
     Boolean getEnabled();
     Integer getPluCount();
     Integer getTotalLabelsPrinted();
     String getUpgradeId();
     String getWeigherPrecision();
     String getLabelStockSize();
     String getAppHookVersion();
     Boolean getProfileRecentlyUpdated();
     String getHealthInfoLog();
}
