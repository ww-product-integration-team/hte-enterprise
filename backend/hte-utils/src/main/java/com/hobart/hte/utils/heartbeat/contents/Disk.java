package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Disk {
	private String device;
	private String displayName;
	private String fileSystemType;
	private long size;
	private long used;
	private long free;

	@JsonProperty(value = "device")
	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	@JsonProperty(value = "displayName")
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@JsonProperty(value = "fileSystemType")
	public String getFileSystemType() {
		return fileSystemType;
	}

	public void setFileSystemType(String fileSystemType) {
		this.fileSystemType = fileSystemType;
	}

	@JsonProperty(value = "size")
	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	@JsonProperty(value = "used")
	public long getUsed() {
		return used;
	}

	public void setUsed(long used) {
		this.used = used;
	}

	@JsonProperty(value = "free")
	public long getFree() {
		return free;
	}

	public void setFree(long free) {
		this.free = free;
	}

	@Override
	public String toString() {
		return "Disk [device=" + device + ", displayName=" + displayName + ", fileSystemType=" + fileSystemType
				+ ", size=" + size + ", used=" + used + ", free=" + free + "]";
	}
}
