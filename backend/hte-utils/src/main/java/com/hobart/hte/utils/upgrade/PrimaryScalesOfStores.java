package com.hobart.hte.utils.upgrade;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.Entity;
import javax.persistence.Table;

//@Entity
//@Immutable
//@Table(name="`primaryScalesOfStores`")
//@Subselect("SELECT storeId, isPrimaryScale as primary_scales FROM ScaleDevice WHERE isPrimaryScale = 1 GROUP BY storeId")
public class PrimaryScalesOfStores {

    private String storeId;

    private String address;

    private String groupId;

//    private Long numScalesOfStore;

//    private Boolean hasPrimaries;

    public PrimaryScalesOfStores(String storeId, String address, String groupId) {
        this.storeId = storeId;
        this.address = address;
        this.groupId = groupId;
//        this.numScalesOfStore = numScalesOfStore;
//        this.hasPrimaries = hasPrimaries;
    }
    public PrimaryScalesOfStores() {}

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

//    public Long getNumScalesOfStore() {
//        return numScalesOfStore;
//    }
//
//    public void setNumScalesOfStore(Long numScalesOfStore) {
//        this.numScalesOfStore = numScalesOfStore;
//    }

//    public Boolean getHasPrimaries() {
//        return hasPrimaries;
//    }
//
//    public void setHasPrimaries(Boolean hasPrimaries) {
//        this.hasPrimaries = hasPrimaries;
//    }
}
