package com.hobart.hte.utils.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "trans")
public class TransEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int TransID;
	@Schema(description = "IP address of the scale that reported this transaction", name = "scaleId", type = "String", example = "10.3.128.54")
	private String scaleId;
	private int TrScID;
	private int TrID;
	private Integer TTNum;
	private Integer TrGrp;
	@Schema(description = "The timesamp of the transaction using this pattern: (yyyy-MM-dd HH:mm:ss)", name = "trDtTm", type = "Timestamp", example = "2022-09-23 14:27:10")
	private Timestamp trDtTm;
	private Integer TrVoid;
	private Integer TrAPrdt;
	private Integer TrRdClr;
	private Integer TrNmPck;
	private Boolean TrSecLb;
	private Boolean TrCOOLF;
	@Schema(description = "Transaction total value, no decimal point included", name = "trTtVal", type = "Integer", example = "2876")
	private Integer trTtVal;
	@Schema(description = "Transaction OLD total value, no decimal point included", name = "trOTtVl", type = "Integer", example = "3570")
	private Integer trOTtVl;
	@Schema(description = "The department number the item belongs to", name = "deptNum", type = "Integer", example = "0")
	private Integer deptNum;
	@Schema(description = "The product number of the current item in the transaction", name = "trPrNum", type = "Integer", example = "4011")
	private Integer trPrNum;
	@Schema(description = "The class number the item belongs to", name = "trClNum", type = "Integer", example = "999999")
	private Integer TrClNum;
	@Schema(description = "The product type of the item {1=Random Weight, 2=Fixed Weight, 3=By Count, 4=Fluid Ounces}", name = "trPrTyp", type = "Integer", example = "1")
	private Integer trPrTyp;
	private Integer TrPrPlT;
	@Schema(description = "Product description text", name = "trPrDes", type = "String", example = "Bananas")
	private String trPrDes;
	private Integer TrPrMod;
	private Integer TrBarPref;
	private String TrBarNum;
	private Integer TrUnPr;
	@Schema(description = "Exception price of the transaction, no decimal point included", name = "trExPr", type = "Integer")
	private Integer TrExPr;
	private Integer TrFnlPr;
	@Schema(description = "Tare value of the transaction, no decimal point included", name = "trTare", type = "Integer")
	private Integer trTare;
	@Schema(description = "Proportional tare value of the transaction, no decimal point included", name = "trPrTare", type = "Integer")
	private Integer trPrTare;
	private Integer TrFnlTare;
	@Schema(description = "Transaction by count", name = "trByCount", type = "Integer")
	private Integer trByCount;
	@Schema(description = "Transaction final net weight, no decimal point included", name = "trFnlNtWt", type = "Integer")
	private Integer trFnlNtWt;
	@Schema(description = "Fixed weight amount of the transaction (lb = in ounces, kg = in grams)", name = "trFxWt", type = "Integer", example = "18")
	private Integer trFxWt;
	private Integer TrSfLfD;
	private Integer TrSfLfH;
	private Integer TrSfLfM;
	private Integer TrPrLfD;
	private Integer TrPrLfH;
	private Integer TrPrLfM;
	@Schema(description = "Product label type 1", name = "trLab1", type = "Integer", example = "106")
	private Integer trLab1;
	@Schema(description = "Product label type 2", name = "trLab2", type = "Integer", example = "206")
	private Integer trLab2;
	@Schema(description = "Product label type 3", name = "trLab3", type = "Integer", example = "0")
	private Integer trLab3;
	@Schema(description = "Product label type 4", name = "trLab4", type = "Integer", example = "0")
	private Integer trLab4;
	@Schema(description = "Operator number", name = "trOpNum", type = "Integer", example = "8758")
	private String trOpNum;
	@Schema(description = "RFID of the operator using hands free ID", name = "trOpRFID", type = "String")
	private String trOpRFID;
	@Schema(description = "Name of the operator associated to the Operator ID or RFID", name = "trOpName", type = "String")
	private String TrOpName;
	@Schema(description = "Operator change type", name = "trOpChg", type = "Integer")
	private Integer TrOpChg;
	private Integer TrOBarDig;
	private Integer TrOUnPr;
	private Integer TrOFnPr;
	private Integer TrOTare;
	private Integer TrOByCount;
	private Integer TrOFxWt;
	private Integer TrOSfLfD;
	private Integer TrOSfLfH;
	private Integer TrOSfLfM;
	private Integer TrOPrLfD;
	private Integer TrOPrLfH;
	private Integer TrOPrLfM;
	@Schema(description = "Old product label type 1", name = "trOLab1", type = "Integer", example = "506")
	private Integer trOLab1;
	@Schema(description = "Old product label type 2", name = "trOLab2", type = "Integer", example = "0")
	private Integer trOLab2;
	@Schema(description = "Old product label type 3", name = "trOLab3", type = "Integer", example = "0")
	private Integer trOLab3;
	@Schema(description = "Old product label type 4", name = "trOLab4", type = "Integer", example = "0")
	private Integer trOLab4;
	@Schema(description = "Product graphic name 1", name = "trGrNm1", type = "String", example = "grilled.png")
	private String trGrNm1;
	@Schema(description = "Product graphic name 2", name = "trGrNm2", type = "String", example = "high_in_fat.png")
	private String trGrNm2;
	@Schema(description = "Product graphic name 3", name = "trGrNm3", type = "String", example = "bioeng.png")
	private String trGrNm3;
	@Schema(description = "Product graphic name 4", name = "trGrNm4", type = "String", example = "134.png")
	private String trGrNm4;
	private String TrCOTxt;
	private String TrCOTrTxt;
	@Schema(description = "Old product graphic name 1", name = "trOGrNm1", type = "String", example = "grilled.png")
	private String trOGrNm1;
	@Schema(description = "Old product graphic name 2", name = "trOGrNm2", type = "String", example = "bioeng.png")
	private String trOGrNm2;
	@Schema(description = "Old product graphic name 3", name = "trOGrNm3", type = "String", example = "75.png")
	private String trOGrNm3;
	@Schema(description = "Old product graphic name 4", name = "trOGrNm4", type = "String", example = "")
	private String trOGrNm4;
	private Integer TrUTNum;
	private Integer TrUTPercentOfPrimalWeight;
	private Integer TrUTPercentOfPrimalCost;
	private Integer TrModeChange;
	private Integer TrSweetheartWeight;

	public Integer getTransID() {
		return TransID;
	}

	public void setTransID(Integer transID) {
		TransID = transID;
	}

	public String getScaleId() {
		return scaleId;
	}

	public void setScaleId(String scaleId) {
		this.scaleId = scaleId;
	}

	public Integer getTrScID() {
		return TrScID;
	}

	public void setTrScID(Integer trScID) {
		TrScID = trScID;
	}

	public Integer getTrID() {
		return TrID;
	}

	public void setTrID(Integer trID) {
		TrID = trID;
	}

	public Integer getTTNum() {
		return TTNum;
	}

	public void setTTNum(Integer tTNum) {
		TTNum = tTNum;
	}

	public Integer getTrGrp() {
		return TrGrp;
	}

	public void setTrGrp(Integer trGrp) {
		TrGrp = trGrp;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	public Timestamp getTrDtTm() {
		return this.trDtTm;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	public void setTrDtTm(Timestamp trDtTm) {
		this.trDtTm = trDtTm;
	}

	public Integer getTrVoid() {
		return TrVoid;
	}

	public void setTrVoid(Integer trVoid) {
		TrVoid = trVoid;
	}

	public Integer getTrAPrdt() {
		return TrAPrdt;
	}

	public void setTrAPrdt(Integer trAPrdt) {
		TrAPrdt = trAPrdt;
	}

	public Integer getTrRdClr() {
		return TrRdClr;
	}

	public void setTrRdClr(Integer trRdClr) {
		TrRdClr = trRdClr;
	}

	public Integer getTrNmPck() {
		return TrNmPck;
	}

	public void setTrNmPck(Integer trNmPck) {
		TrNmPck = trNmPck;
	}

	public Boolean isTrSecLb() {
		return TrSecLb;
	}

	public void setTrSecLb(Boolean trSecLb) {
		TrSecLb = trSecLb;
	}
	
	public void setTrSecLb(String trSecLb) {
		TrSecLb = !trSecLb.equals("0");
	}

	public Boolean isTrCOOLF() {
		return TrCOOLF;
	}

	public void setTrCOOLF(Boolean trCOOLF) {
		TrCOOLF = trCOOLF;
	}
	
	public void setTrCOOLF(String trCOOLF) {
		TrCOOLF = !trCOOLF.equals(trCOOLF);
	}

	public Integer getTrTtVal() {
		return trTtVal;
	}

	public void setTrTtVal(Integer trTtVal) {
		this.trTtVal = trTtVal;
	}

	public Integer getTrOTtVl() {
		return trOTtVl;
	}

	public void setTrOTtVl(Integer trOTtVl) {
		this.trOTtVl = trOTtVl;
	}

	public Integer getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(Integer deptNum) {
		this.deptNum = deptNum;
	}

	public Integer getTrPrNum() {
		return trPrNum;
	}

	public void setTrPrNum(Integer trPrNum) {
		this.trPrNum = trPrNum;
	}

	public Integer getTrClNum() {
		return TrClNum;
	}

	public void setTrClNum(Integer trClNum) {
		TrClNum = trClNum;
	}

	public Integer getTrPrTyp() {
		return trPrTyp;
	}

	public void setTrPrTyp(Integer trPrTyp) {
		this.trPrTyp = trPrTyp;
	}

	public Integer getTrPrPlT() {
		return TrPrPlT;
	}

	public void setTrPrPlT(Integer trPrPlT) {
		TrPrPlT = trPrPlT;
	}

	public String getTrPrDes() {
		return trPrDes;
	}

	public void setTrPrDes(String trPrDes) {
		this.trPrDes = trPrDes;
	}

	public Integer getTrPrMod() {
		return TrPrMod;
	}

	public void setTrPrMod(Integer trPrMod) {
		TrPrMod = trPrMod;
	}

	public Integer getTrBarPref() {
		return TrBarPref;
	}

	public void setTrBarPref(Integer trBarPref) {
		TrBarPref = trBarPref;
	}

	public String getTrBarNum() {
		return TrBarNum;
	}

	public void setTrBarNum(String trBarNum) {
		TrBarNum = trBarNum;
	}

	public Integer getTrUnPr() {
		return TrUnPr;
	}

	public void setTrUnPr(Integer trUnPr) {
		TrUnPr = trUnPr;
	}

	public Integer getTrExPr() {
		return TrExPr;
	}

	public void setTrExPr(Integer trExPr) {
		TrExPr = trExPr;
	}

	public Integer getTrFnlPr() {
		return TrFnlPr;
	}

	public void setTrFnlPr(Integer trFnlPr) {
		TrFnlPr = trFnlPr;
	}

	public Integer getTrTare() {
		return this.trTare;
	}

	public void setTrTare(Integer trTare) {
		this.trTare = trTare;
	}

	public Integer getTrPrTare() {
		return this.trPrTare;
	}

	public void setTrPrTare(Integer trPrTare) {
		this.trPrTare = trPrTare;
	}

	public Integer getTrFnlTare() {
		return TrFnlTare;
	}

	public void setTrFnlTare(Integer trFnlTare) {
		TrFnlTare = trFnlTare;
	}

	public Integer getTrByCount() {
		return this.trByCount;
	}

	public void setTrByCount(Integer trByCount) {
		this.trByCount = trByCount;
	}

	public Integer getTrFnlNtWt() {
		return this.trFnlNtWt;
	}

	public void setTrFnlNtWt(Integer trFnlNtWt) {
		this.trFnlNtWt = trFnlNtWt;
	}

	public Integer getTrFxWt() {
		return trFxWt;
	}

	public void setTrFxWt(Integer trFxWt) {
		this.trFxWt = trFxWt;
	}

	public Integer getTrSfLfD() {
		return TrSfLfD;
	}

	public void setTrSfLfD(Integer trSfLfD) {
		TrSfLfD = trSfLfD;
	}

	public Integer getTrSfLfH() {
		return TrSfLfH;
	}

	public void setTrSfLfH(Integer trSfLfH) {
		TrSfLfH = trSfLfH;
	}

	public Integer getTrSfLfM() {
		return TrSfLfM;
	}

	public void setTrSfLfM(Integer trSfLfM) {
		TrSfLfM = trSfLfM;
	}

	public Integer getTrPrLfD() {
		return TrPrLfD;
	}

	public void setTrPrLfD(Integer trPrLfD) {
		TrPrLfD = trPrLfD;
	}

	public Integer getTrPrLfH() {
		return TrPrLfH;
	}

	public void setTrPrLfH(Integer trPrLfH) {
		TrPrLfH = trPrLfH;
	}

	public Integer getTrPrLfM() {
		return TrPrLfM;
	}

	public void setTrPrLfM(Integer trPrLfM) {
		TrPrLfM = trPrLfM;
	}

	public Integer getTrLab1() {
		return trLab1;
	}

	public void setTrLab1(Integer trLab1) {
		this.trLab1 = trLab1;
	}

	public Integer getTrLab2() {
		return trLab2;
	}

	public void setTrLab2(Integer trLab2) {
		this.trLab2 = trLab2;
	}

	public Integer getTrLab3() {
		return trLab3;
	}

	public void setTrLab3(Integer trLab3) {
		this.trLab3 = trLab3;
	}

	public Integer getTrLab4() {
		return trLab4;
	}

	public void setTrLab4(Integer trLab4) {
		this.trLab4 = trLab4;
	}

	public String getTrOpNum() {
		return trOpNum;
	}

	public void setTrOpNum(String trOpNum) {
		this.trOpNum = trOpNum;
	}

	public String getTrOpRFID() {
		return trOpRFID;
	}

	public void setTrOpRFID(String trOpRFID) {
		this.trOpRFID = trOpRFID;
	}

	public String getTrOpName() {
		return TrOpName;
	}

	public void setTrOpName(String trOpName) {
		TrOpName = trOpName;
	}

	public Integer getTrOpChg() {
		return TrOpChg;
	}

	public void setTrOpChg(Integer trOpChg) {
		TrOpChg = trOpChg;
	}

	public Integer getTrOBarDig() {
		return TrOBarDig;
	}

	public void setTrOBarDig(Integer trOBarDig) {
		TrOBarDig = trOBarDig;
	}

	public Integer getTrOUnPr() {
		return TrOUnPr;
	}

	public void setTrOUnPr(Integer trOUnPr) {
		TrOUnPr = trOUnPr;
	}

	public Integer getTrOFnPr() {
		return TrOFnPr;
	}

	public void setTrOFnPr(Integer trOFnPr) {
		TrOFnPr = trOFnPr;
	}

	public Integer getTrOTare() {
		return TrOTare;
	}

	public void setTrOTare(Integer trOTare) {
		TrOTare = trOTare;
	}

	public Integer getTrOByCount() {
		return TrOByCount;
	}

	public void setTrOByCount(Integer trOByCount) {
		TrOByCount = trOByCount;
	}

	public Integer getTrOFxWt() {
		return TrOFxWt;
	}

	public void setTrOFxWt(Integer trOFxWt) {
		TrOFxWt = trOFxWt;
	}

	public Integer getTrOSfLfD() {
		return TrOSfLfD;
	}

	public void setTrOSfLfD(Integer trOSfLfD) {
		TrOSfLfD = trOSfLfD;
	}

	public Integer getTrOSfLfH() {
		return TrOSfLfH;
	}

	public void setTrOSfLfH(Integer trOSfLfH) {
		TrOSfLfH = trOSfLfH;
	}

	public Integer getTrOSfLfM() {
		return TrOSfLfM;
	}

	public void setTrOSfLfM(Integer trOSfLfM) {
		TrOSfLfM = trOSfLfM;
	}

	public Integer getTrOPrLfD() {
		return TrOPrLfD;
	}

	public void setTrOPrLfD(Integer trOPrLfD) {
		TrOPrLfD = trOPrLfD;
	}

	public Integer getTrOPrLfH() {
		return TrOPrLfH;
	}

	public void setTrOPrLfH(Integer trOPrLfH) {
		TrOPrLfH = trOPrLfH;
	}

	public Integer getTrOPrLfM() {
		return TrOPrLfM;
	}

	public void setTrOPrLfM(Integer trOPrLfM) {
		TrOPrLfM = trOPrLfM;
	}

	public Integer getTrOLab1() {
		return trOLab1;
	}

	public void setTrOLab1(Integer trOLab1) {
		this.trOLab1 = trOLab1;
	}

	public Integer getTrOLab2() {
		return trOLab2;
	}

	public void setTrOLab2(Integer trOLab2) {
		this.trOLab2 = trOLab2;
	}

	public Integer getTrOLab3() {
		return trOLab3;
	}

	public void setTrOLab3(Integer trOLab3) {
		this.trOLab3 = trOLab3;
	}

	public Integer getTrOLab4() {
		return trOLab4;
	}

	public void setTrOLab4(Integer trOLab4) {
		this.trOLab4 = trOLab4;
	}

	public String getTrGrNm1() {
		return trGrNm1;
	}

	public void setTrGrNm1(String trGrNm1) {
		this.trGrNm1 = trGrNm1;
	}

	public String getTrGrNm2() {
		return trGrNm2;
	}

	public void setTrGrNm2(String trGrNm2) {
		this.trGrNm2 = trGrNm2;
	}

	public String getTrGrNm3() {
		return trGrNm3;
	}

	public void setTrGrNm3(String trGrNm3) {
		this.trGrNm3 = trGrNm3;
	}

	public String getTrGrNm4() {
		return trGrNm4;
	}

	public void setTrGrNm4(String trGrNm4) {
		this.trGrNm4 = trGrNm4;
	}

	public String getTrCOTxt() {
		return TrCOTxt;
	}

	public void setTrCOTxt(String trCOTxt) {
		TrCOTxt = trCOTxt;
	}

	public String getTrCOTrTxt() {
		return TrCOTrTxt;
	}

	public void setTrCOTrTxt(String trCOTrTxt) {
		TrCOTrTxt = trCOTrTxt;
	}

	public String getTrOGrNm1() {
		return trOGrNm1;
	}

	public void setTrOGrNm1(String trOGrNm1) {
		this.trOGrNm1 = trOGrNm1;
	}

	public String getTrOGrNm2() {
		return trOGrNm2;
	}

	public void setTrOGrNm2(String trOGrNm2) {
		this.trOGrNm2 = trOGrNm2;
	}

	public String getTrOGrNm3() {
		return trOGrNm3;
	}

	public void setTrOGrNm3(String trOGrNm3) {
		this.trOGrNm3 = trOGrNm3;
	}

	public String getTrOGrNm4() {
		return trOGrNm4;
	}

	public void setTrOGrNm4(String trOGrNm4) {
		this.trOGrNm4 = trOGrNm4;
	}

	public Integer getTrUTNum() {
		return TrUTNum;
	}

	public void setTrUTNum(Integer trUTNum) {
		TrUTNum = trUTNum;
	}

	public Integer getTrUTPercentOfPrimalWeight() {
		return TrUTPercentOfPrimalWeight;
	}

	public void setTrUTPercentOfPrimalWeight(Integer trUTPercentOfPrimalWeight) {
		TrUTPercentOfPrimalWeight = trUTPercentOfPrimalWeight;
	}

	public Integer getTrUTPercentOfPrimalCost() {
		return TrUTPercentOfPrimalCost;
	}

	public void setTrUTPercentOfPrimalCost(Integer trUTPercentOfPrimalCost) {
		TrUTPercentOfPrimalCost = trUTPercentOfPrimalCost;
	}

	public Integer getTrModeChange() {
		return TrModeChange;
	}

	public void setTrModeChange(Integer trModeChange) {
		TrModeChange = trModeChange;
	}

	public Integer getTrSweetheartWeight() {
		return TrSweetheartWeight;
	}

	public void setTrSweetheartWeight(Integer trSweetheartWeight) {
		TrSweetheartWeight = trSweetheartWeight;
	}

	public void setTransID(int transID) {
		TransID = transID;
	}

	public void setTrScID(int trScID) {
		TrScID = trScID;
	}

	public void setTrID(int trID) {
		TrID = trID;
	}
}
