package com.hobart.hte.utils.device.stats;

public class DeviceCountPerFirmware {
	private String application;
	private Long total;

	public DeviceCountPerFirmware(String application, Long total) {
		super();
		this.application = application;
		this.total = total;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}
}
