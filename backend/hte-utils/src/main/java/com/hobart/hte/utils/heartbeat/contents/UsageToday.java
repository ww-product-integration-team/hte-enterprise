package com.hobart.hte.utils.heartbeat.contents;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsageToday {
	private int cakePrintsCount;
	private List<PluPrintsCount> pluPrintsCount;

	@JsonProperty(value = "cakePrintsCount")
	public int getCakePrintsCount() {
		return cakePrintsCount;
	}

	public void setCakePrintsCount(int cakePrintsCount) {
		this.cakePrintsCount = cakePrintsCount;
	}

	@JsonProperty(value = "pluPrintsCount")
	public List<PluPrintsCount> getPluPrintsCount() {
		return pluPrintsCount;
	}

	public void setPluPrintsCount(List<PluPrintsCount> pluPrints) {
		this.pluPrintsCount = pluPrints;
	}

	static class PluPrintsCount {
		private int pluNbr;
		private int manualPrintsCount;
		private int remotePrintsCount;

		@JsonProperty(value = "pluNbr")
		public int getPluNbr() {
			return pluNbr;
		}

		public void setPluNbr(int pluNbr) {
			this.pluNbr = pluNbr;
		}

		@JsonProperty(value = "manualPrintsCount")
		public int getManualPrintsCount() {
			return manualPrintsCount;
		}

		public void setManualPrintsCount(int manualPrints) {
			this.manualPrintsCount = manualPrints;
		}

		@JsonProperty(value = "remotePrintsCount")
		public int getRemotePrintsCount() {
			return remotePrintsCount;
		}

		public void setRemotePrintsCount(int remotePrints) {
			this.remotePrintsCount = remotePrints;
		}

		@Override
		public String toString() {
			return "PluPrints [pluNbr=" + pluNbr + ", manualPrints=" + manualPrintsCount + ", remotePrints=" + remotePrintsCount
					+ "]";
		}
	}

	@Override
	public String toString() {
		return "UsageToday [cakePrints=" + cakePrintsCount + ", pluPrintsCount=" + pluPrintsCount + "]";
	}
}
