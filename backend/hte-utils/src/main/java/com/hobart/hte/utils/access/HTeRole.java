package com.hobart.hte.utils.access;

import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.data.util.Pair;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Schema(description = "This class represents a role and it's permissions")
@Entity
@Table(name = "hteRole")
public class HTeRole {

    public HTeRole(Pair<Integer, String> roleConfig, String description) {
        super();
        this.id = roleConfig.getFirst();
        this.name = roleConfig.getSecond();
        this.description = description;
    }

    public HTeRole(Integer i, String name, String description) {
        super();
        this.id = i;
        this.name = name;
        this.description = description;
    }

    public HTeRole() {

    }

    @Id
    private Integer id;
    private String name;
    private String description;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

}
