package com.hobart.hte.utils.heartbeat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.store.StoreEvent;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class StoreHeartbeatRequest {
    private String storeId;
    private String ipAddress;
    private String url;
    private List<Service> services;
    private String hostname;
    private String triggerType;
    private Timestamp timestampUTC;
    private HealthCheck HealthCheckObject;
    private boolean deleteEvent;
    private ArrayList<StoreEvent> eventLogs = new ArrayList<>();
    private ArrayList<ScaleDevice> lastHeartbeat = new ArrayList<>();
    private ArrayList<FileForEvent> files = new ArrayList<>();

    // Getter Methods

    public String getStoreId() {
        return storeId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getHostname() {
        return hostname;
    }

    public String getTriggerType() {
        return triggerType;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getTimestampUTC() {
        return timestampUTC;
    }

    public HealthCheck getHealthCheck() {
        return HealthCheckObject;
    }

    // Setter Methods

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setTriggerType(String triggerType) {
        this.triggerType = triggerType;
    }

    public void setTimestampUTC(Timestamp timestampUTC) {
        this.timestampUTC = timestampUTC;
    }

    public HealthCheck getHealthCheckObject() {return HealthCheckObject;}

    public void setHealthCheckObject(HealthCheck healthCheckObject) {HealthCheckObject = healthCheckObject;}

    public boolean isDeleteEvent() {
        return deleteEvent;
    }

    public void setDeleteEvent(boolean deleteEvent) {
        this.deleteEvent = deleteEvent;
    }

    public ArrayList<StoreEvent> getEventLogs() {
        return eventLogs;
    }

    public void setEventLogs(ArrayList<StoreEvent> eventLogs) {
        this.eventLogs = eventLogs;
    }

    public ArrayList<ScaleDevice> getLastHeartbeat() {return lastHeartbeat;}

    public void setLastHeartbeat(ArrayList<ScaleDevice> lastHeartbeat) {this.lastHeartbeat = lastHeartbeat;}

    public ArrayList<FileForEvent> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<FileForEvent> files) {
        this.files = files;
    }
}
