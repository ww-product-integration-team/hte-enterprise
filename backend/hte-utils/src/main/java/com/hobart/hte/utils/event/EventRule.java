package com.hobart.hte.utils.event;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "eventRules")
public class EventRule {
    @Id
    @Schema(description = "The id of the event, or the id of the entity which has a special case")
    String eventId;
    @Schema(description = "For easy queries, indicates the custom rule that you are overriding")
    Integer customRule;
    @Schema(description = "Should log this type of notification, if this is not set then now event will be recorded, " +
                                "if this is not true then no notification will be sent out")
    Boolean shouldLog;
    @Schema(description = "Muted events will be hidden from the user on the frontend unless specially requested")
    Boolean shouldMute;
    @Schema(description = "Should the server send this event out in an email.")
    Boolean shouldEmail;

    public EventRule() {
    }

    /**
     * Constructor for custom rules
     *===============================================================
     * @param eventId - The entity UUID that the custom rule is for
     * @param customRule - The rule that this custom specification is overriding
     * @param shouldLog - Should the server log this event or do nothing
     * @param shouldMute - Should this event show up on the frontend
     * @param shouldEmail - Should the server send out an email when this event occurs
     */
    public EventRule(String eventId, Integer customRule, Boolean shouldLog, Boolean shouldMute, Boolean shouldEmail) {
        this.eventId = eventId;
        this.customRule = customRule;
        this.shouldLog = shouldLog;
        this.shouldMute = shouldMute;
        this.shouldEmail = shouldEmail;
    }

    /**
     * Constructor for default rules
     *===============================================================
     * @param eventId - The id for the default id
     * @param shouldLog - Should the server log this event or do nothing
     * @param shouldMute - Should this event show up on the frontend
     * @param shouldEmail - Should the server send out an email when this event occurs
     */
    public EventRule(Integer eventId, Boolean shouldLog, Boolean shouldMute, Boolean shouldEmail) {
        this.eventId = eventId.toString();
        this.customRule = null;
        this.shouldLog = shouldLog;
        this.shouldMute = shouldMute;
        this.shouldEmail = shouldEmail;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Integer getCustomRule() {
        return customRule;
    }

    public void setCustomRule(Integer customRule) {
        this.customRule = customRule;
    }

    public Boolean getShouldLog() {
        return shouldLog;
    }

    public void setShouldLog(Boolean shouldLog) {
        this.shouldLog = shouldLog;
    }

    public Boolean getShouldMute() {
        return shouldMute;
    }

    public void setShouldMute(Boolean shouldMute) {
        this.shouldMute = shouldMute;
    }

    public Boolean getShouldEmail() {
        return shouldEmail;
    }

    public void setShouldEmail(Boolean shouldEmail) {
        this.shouldEmail = shouldEmail;
    }
}
