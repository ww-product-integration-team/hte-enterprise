package com.hobart.hte.utils.service;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "service")
public class Service {

    @Id
    private String id;
    private String databaseId;
    private ServiceType serviceType;
    private String serviceName;
    private String operationMode;
    private Timestamp lastRestart;
    private String url;
    private String version;
    private String buildDate;

    public Service() {
    }

    public Service(String databaseId, ServiceType serviceType,
                   String operationMode, String url,
                   String version, String buildDate){
        this.id = UUID.randomUUID().toString();
        this.databaseId = databaseId;
        this.serviceType = serviceType;
        this.serviceName = serviceType.getName();
        this.operationMode = operationMode;
        this.url = url;
        this.version = version;
        this.buildDate = buildDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getOperationMode() {
        return operationMode;
    }

    public void setOperationMode(String operationMode) {
        this.operationMode = operationMode;
    }

    public Timestamp getLastRestart() {
        return lastRestart;
    }

    public void setLastRestart(Timestamp lastRestart) {
        this.lastRestart = lastRestart;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }
}
