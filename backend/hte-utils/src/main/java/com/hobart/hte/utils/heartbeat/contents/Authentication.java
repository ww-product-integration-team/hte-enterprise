package com.hobart.hte.utils.heartbeat.contents;

public class Authentication {
	private int DefaultKey;
	private String Key1;
	private String Key2;
	private String Key3;

	public int getDefaultKey() {
		return DefaultKey;
	}

	public void setDefaultKey(int defaultKey) {
		DefaultKey = defaultKey;
	}

	public String getKey1() {
		return Key1;
	}

	public void setKey1(String key1) {
		Key1 = key1;
	}

	public String getKey2() {
		return Key2;
	}

	public void setKey2(String key2) {
		Key2 = key2;
	}

	public String getKey3() {
		return Key3;
	}

	public void setKey3(String key3) {
		Key3 = key3;
	}

	public String getKey4() {
		return Key4;
	}

	public void setKey4(String key4) {
		Key4 = key4;
	}

	private String Key4;
}
