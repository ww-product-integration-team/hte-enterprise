package com.hobart.hte.utils.heartbeat;

import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.heartbeat.contents.DeleteTypes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DeleteEvent extends ScaleEvent{
    private static final Logger log = LogManager.getLogger(DeleteEvent.class);
    /**
     * List of data types to clear on the scale
     *  Available Types: flashkey, label, plu, operator, config
     */
    private List<String> deleteTypes;

    public DeleteEvent(ConfigEntry deleteEntry, String callbackUrl){
        super("DELETE", UUID.randomUUID().toString(), callbackUrl);
        deleteTypes = new ArrayList<>();

        if(deleteEntry == null || deleteEntry.getCoValue() == null){
            log.error("ERROR - no value found for delete types!");
            return;
        }

        for(String item: deleteEntry.getCoValue().split(",")){
            String stripped = item.toLowerCase().trim();
            switch (stripped){
                case "flashkey":
                case "label":
                case "plu":
                case "operator":
                case "config":
                    deleteTypes.add(stripped);
                    break;
                default:
                    log.error("ERROR - unknown deletion type: {}", stripped);
            }
        }

    }

    /**
     * @param deleteTypes
     * @param callbackUrl
     */
    public DeleteEvent(List<String> deleteTypes,  String callbackUrl) {
        super("DELETE", UUID.randomUUID().toString(), callbackUrl);
        this.deleteTypes = deleteTypes;
    }

    public List<String> getDeleteTypes() {
        return deleteTypes;
    }

    public void setDeleteTypes(List<String> deleteTypes) {
        this.deleteTypes = deleteTypes;
    }

    public void addType(DeleteTypes deleteType) {
        if (this.deleteTypes != null) {
            this.deleteTypes.add(deleteType.name());
        }
    }

}
