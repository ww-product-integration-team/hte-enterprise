package com.hobart.hte.utils.config;

public class AppConfig {

    //Path Configurations
    //=====================================================================
    public static final String repositoryPath = "repositoryPath";
    public static final String licensePath = "licensePath";
    public static final String importPath = "importPath";
    public static final String donePath = "donePath";
    public static final String logRootPath = "logRootPath";
    //Location of the trusted certificates for Java (Used by Store Client when using HTTPS)
    public static final String caCertsPath = "caCertsPath";
    public static final String caCertsPassword = "caCertsPassword";

    //Configs to control how often the server sends down a delete (Full resync) command
    //=====================================================================
    public static final String scaleDeleteInterval = "scaleDeleteInterval"; //Never, Daily, Weekly, Monthly (Default Weekly)
    public static final String scaleDeleteDay = "scaleDeleteDay"; //Number related to day of week or month
    public static final String scaleDeleteTime = "scaleDeleteTime"; //Number related to time of day to send command (1-24) Default 1 i.e. 1 am
    public static final String scaleDeleteItems = "scaleDeleteItems"; //Comma separated string, PLUs, flashkeys etc.

    //Heartbeat Manager Specific
    //=====================================================================
    public static final String heartbeatOperation = "heartbeatOperation"; //Values: HQ, STORE

    //Hours since last heartbeat before scale is considered offline or away
    //Online will default to anything below away
    //=====================================================================
    public static final String scaleOfflineHours = "scaleOfflineHours";
    public static final String scaleAwayHours = "scaleAwayHours";

    //Store Specific
    //=====================================================================
    public static final String storeHbInterval = "storeHbInterval";
    public static final String sendStoreInfo = "sendStoreInfo";
    public static final String customStoreName = "customStoreName";

    //Domain IDs
    //=====================================================================
    public static final String HtAdminId =              "32303230-3230-3230-3230-323032303230";
    public final static String DefaultHqDatabaseId =    "12341234-1234-1234-1234-123412341234";
    public final static String DefaultAdminId =         "87588758-8758-8758-8758-875887588758";
    public final static String DefaultUnassignedId =    "11111111-1111-1111-1111-111111111111";
    public final static String DefaultProfileId =       "00000000-0000-0000-0000-000000000000";
    public final static String DefaultBannerId =        "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa";
    public final static String DefaultAllAccessDomain = "22222222-2222-2222-2222-222222222222";

    //Upload/Upgrade Timeframes
    //===================================================================
    public static final String uploadPrimaryStart = "uploadPrimaryStart";
    public static final String uploadPrimaryEnd = "uploadPrimaryEnd";
    public static final String uploadSecondaryStart = "uploadSecondaryStart";
    public static final String uploadSecondaryEnd = "uploadSecondaryEnd";
    public static final String upgradeTimeStart = "upgradeTimeStart";
    public static final String upgradeTimeEnd = "upgradeTimeEnd";

    //Misc Configurations
    //====================================================================
    public static final String httpMethod = "httpMethod";
    public static final String defaultPort = "defaultPort";
    public static final String databaseId = "databaseId";
    public static final String fallbackIp = "fallbackIp";
    public static final String validateRecoveryFiles = "validateRecoveryFiles";
    public static final String hybridMultipath = "hybridMultipath";
    public static final String zonePricing = "zonePricing";
    public static final String httpRequestCheckScaleService = "httpRequestCheckScaleService";
}
