package com.hobart.hte.utils.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;

public class OperationStatusModel {
	private String operation;
	private String result;
	private String errorDescription;
	private Timestamp timestamp;

	public OperationStatusModel(){

	}

	public OperationStatusModel(String operation, String result, String errorDescription, Timestamp timestamp) {
		this.operation = operation;
		this.result = result;
		this.errorDescription = errorDescription;
		this.timestamp = timestamp;
	}

	public OperationStatusModel(String operation, String result) {
		super();
		this.operation = operation;
		this.result = result;
		this.timestamp = new Timestamp(System.currentTimeMillis());
	}

	public OperationStatusModel(String operation) {
		super();
		this.operation = operation;
		this.timestamp = new Timestamp(System.currentTimeMillis());
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "OperationStatusModel:\n" +
				"    operation= " + operation + '\n' +
				"    result= " + result + '\n' +
				"    errorDescription= " + errorDescription + '\n' +
				"    timestamp= " + timestamp + '\n';
	}
}
