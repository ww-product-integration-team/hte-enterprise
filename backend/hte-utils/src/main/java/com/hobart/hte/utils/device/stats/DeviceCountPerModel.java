package com.hobart.hte.utils.device.stats;

public class DeviceCountPerModel {
	private String scaleModel;
	private Long total;

	public DeviceCountPerModel(String scaleModel, Long total) {
		super();
		this.scaleModel = scaleModel;
		this.total = total;
	}

	public String getScaleModel() {
		return scaleModel;
	}

	public void setScaleModel(String scaleModel) {
		this.scaleModel = scaleModel;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}
}
