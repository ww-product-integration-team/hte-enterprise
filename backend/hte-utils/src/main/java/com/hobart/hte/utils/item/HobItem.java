package com.hobart.hte.utils.item;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "prods")
public class HobItem {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer prID;
	@Column(name ="DeptNum")
	private Integer deptNum;
	@Column(name = "ClNum")
	private Integer clNum;
	private Integer TxNum;
	private Integer ProdTxt5Num;
	private Integer ProdTxt6Num;
	private Integer ProdTxt7Num;
	private Integer ProdTxt8Num;
	private Integer ProdTxt9Num;
	private Integer SMNum;
	private Integer NFNum;
	private Integer NtNum;
	private Integer LbNum1;
	private Integer LbNum2;
	private Integer LbNum3;
	private Integer CaNum;
	private Integer SPNum;
	private Integer LCNum;
	private Integer LPNum;
	private Integer LXNum;
	private Integer DLXNum;
	private Integer LLID;
	private Integer MaNum;
	private Integer PrPrice;
	private Integer PrPrMod;
	private Boolean PrIsFrcPr;
	private Integer PrExcPr;
	private Integer PrDscMe;
	private Integer PrDscPr1;
	private Integer PrDscPr2;
	private Integer PrDscPr3;
	private Integer PrTare;
	private Integer PrTareB;
	private Boolean PrIsFrcTr;
	private Integer PrPrpTare;
	private Integer PrNetWt;
	private Integer PrByCnt;
	private Boolean PrIsFrcBC;
	private Integer PrShlfLfD;
	private Integer PrShlfLfH;
	private Integer PrProdLfD;
	private Integer PrProdLfH;
	private Boolean PrIsPrnSL;
	private Boolean PrIsPrnPL;
	private Boolean PrIsPrnPO;
	private Integer PrBnsPts;
	private Integer PrPrtns;
	private Boolean PrIsUsSL;
	private Integer PrInMode;
	private Integer PrLblRot1;
	private Integer PrLblPlc1;
	private Integer PrLblRot2;
	private Integer PrLblPlc2;
	private Integer PrLblRot3;
	private Integer PrLblPlc3;
	private String PrName1;
	private String PrName2;
	private Boolean PrVCOLTxt;
	private Boolean PrCOLReq;
	private Integer PrPrPlT;
	@Column(name = "PrNum")
	private Integer prNum;
	private Integer PrType;
	private Integer PrBarPref;
	private Integer PrBarProN;
	private Integer PrBarVenN;
	private String PrEANCd;
	private String PrRawBarC;
	private String PrCdNm1;
	private String PrCdNm2;
	private Timestamp PrSpcSD;
	private String PrDesc;
	private String PrGrNm1;
	private String PrGrNm2;
	private String PrGrNm3;
	private String PrGrNm4;
	private String PrImage;
	private String PrCOLTN;
	private Integer PrMealSideItemsGroupNum;
	private Boolean PrMealSideItemsAreFree;
	private Integer PrMealNumSidesIncluded;
	private Integer PrCutTestMeatSpecSheetGroupNum;
	private Integer PrWeightMin;
	private Integer PrWeightMax;
	private Integer PrTotalPriceMin;
	private Integer PrTotalPriceMax;
	private Integer PrManualLabelTypeNum1;
	private Integer PrManualLabelTypeNum2;
	private Integer PrManualLabelTypeNum3;
	private Integer PrPrepackLabelTypeNum1;
	private Integer PrPrepackLabelTypeNum2;
	private Integer PrPrepackLabelTypeNum3;
	private Integer PrProdEntryLabelTypeNum1;
	private Integer PrProdEntryLabelTypeNum2;
	private Integer PrProdEntryLabelTypeNum3;
	private Integer PrSelfServLabelTypeNum1;
	private Integer PrSelfServLabelTypeNum2;
	private Integer PrSelfServLabelTypeNum3;
	private Integer PrPick5LabelTypeNum1;
	private Integer PrPick5LabelTypeNum2;
	private Integer PrPick5LabelTypeNum3;
	private Integer PrOnlineOrderingLabelTypeNum1;
	private Integer PrOnlineOrderingLabelTypeNum2;
	private Integer PrOnlineOrderingLabelTypeNum3;
	private Integer PrShlfLfM;
	private Integer PrProdLfM;
	private String zoneId;
	@Transient
	private String zoneName;

	public Integer getPrID() {
		return prID;
	}

	public void setPrID(Integer prID) {
		this.prID = prID;
	}

	public Integer getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(Integer deptNum) {
		this.deptNum = deptNum;
	}

	public Integer getClNum() {
		return clNum;
	}

	public void setClNum(Integer clNum) {
		this.clNum = clNum;
	}

	public Integer getTxNum() {
		return TxNum;
	}

	public void setTxNum(Integer txNum) {
		TxNum = txNum;
	}

	public Integer getProdTxt5Num() {
		return ProdTxt5Num;
	}

	public void setProdTxt5Num(Integer prodTxt5Num) {
		ProdTxt5Num = prodTxt5Num;
	}

	public Integer getProdTxt6Num() {
		return ProdTxt6Num;
	}

	public void setProdTxt6Num(Integer prodTxt6Num) {
		ProdTxt6Num = prodTxt6Num;
	}

	public Integer getProdTxt7Num() {
		return ProdTxt7Num;
	}

	public void setProdTxt7Num(Integer prodTxt7Num) {
		ProdTxt7Num = prodTxt7Num;
	}

	public Integer getProdTxt8Num() {
		return ProdTxt8Num;
	}

	public void setProdTxt8Num(Integer prodTxt8Num) {
		ProdTxt8Num = prodTxt8Num;
	}

	public Integer getProdTxt9Num() {
		return ProdTxt9Num;
	}

	public void setProdTxt9Num(Integer prodTxt9Num) {
		ProdTxt9Num = prodTxt9Num;
	}

	public Integer getSMNum() {
		return SMNum;
	}

	public void setSMNum(Integer sMNum) {
		SMNum = sMNum;
	}

	public Integer getNFNum() {
		return NFNum;
	}

	public void setNFNum(Integer nFNum) {
		NFNum = nFNum;
	}

	public Integer getNtNum() {
		return NtNum;
	}

	public void setNtNum(Integer ntNum) {
		NtNum = ntNum;
	}

	public Integer getLbNum1() {
		return LbNum1;
	}

	public void setLbNum1(Integer lbNum1) {
		LbNum1 = lbNum1;
	}

	public Integer getLbNum2() {
		return LbNum2;
	}

	public void setLbNum2(Integer lbNum2) {
		LbNum2 = lbNum2;
	}

	public Integer getLbNum3() {
		return LbNum3;
	}

	public void setLbNum3(Integer lbNum3) {
		LbNum3 = lbNum3;
	}

	public Integer getCaNum() {
		return CaNum;
	}

	public void setCaNum(Integer caNum) {
		CaNum = caNum;
	}

	public Integer getSPNum() {
		return SPNum;
	}

	public void setSPNum(Integer sPNum) {
		SPNum = sPNum;
	}

	public Integer getLCNum() {
		return LCNum;
	}

	public void setLCNum(Integer lCNum) {
		LCNum = lCNum;
	}

	public Integer getLPNum() {
		return LPNum;
	}

	public void setLPNum(Integer lPNum) {
		LPNum = lPNum;
	}

	public Integer getLXNum() {
		return LXNum;
	}

	public void setLXNum(Integer lXNum) {
		LXNum = lXNum;
	}

	public Integer getDLXNum() {
		return DLXNum;
	}

	public void setDLXNum(Integer dLXNum) {
		DLXNum = dLXNum;
	}

	public Integer getLLID() {
		return LLID;
	}

	public void setLLID(Integer lLID) {
		LLID = lLID;
	}

	public Integer getMaNum() {
		return MaNum;
	}

	public void setMaNum(Integer maNum) {
		MaNum = maNum;
	}

	public Integer getPrPrice() {
		return PrPrice;
	}

	public void setPrPrice(Integer prPrice) {
		PrPrice = prPrice;
	}

	public Integer getPrPrMod() {
		return PrPrMod;
	}

	public void setPrPrMod(Integer prPrMod) {
		PrPrMod = prPrMod;
	}

	public Boolean isPrIsFrcPr() {
		return PrIsFrcPr;
	}

	public void setPrIsFrcPr(Boolean prIsFrcPr) {
		PrIsFrcPr = prIsFrcPr;
	}

	public Integer getPrExcPr() {
		return PrExcPr;
	}

	public void setPrExcPr(Integer prExcPr) {
		PrExcPr = prExcPr;
	}

	public Integer getPrDscMe() {
		return PrDscMe;
	}

	public void setPrDscMe(Integer prDscMe) {
		PrDscMe = prDscMe;
	}

	public Integer getPrDscPr1() {
		return PrDscPr1;
	}

	public void setPrDscPr1(Integer prDscPr1) {
		PrDscPr1 = prDscPr1;
	}

	public Integer getPrDscPr2() {
		return PrDscPr2;
	}

	public void setPrDscPr2(Integer prDscPr2) {
		PrDscPr2 = prDscPr2;
	}

	public Integer getPrDscPr3() {
		return PrDscPr3;
	}

	public void setPrDscPr3(Integer prDscPr3) {
		PrDscPr3 = prDscPr3;
	}

	public Integer getPrTare() {
		return PrTare;
	}

	public void setPrTare(Integer prTare) {
		PrTare = prTare;
	}

	public Integer getPrTareB() {
		return PrTareB;
	}

	public void setPrTareB(Integer prTareB) {
		PrTareB = prTareB;
	}

	public Boolean isPrIsFrcTr() {
		return PrIsFrcTr;
	}

	public void setPrIsFrcTr(Boolean prIsFrcTr) {
		PrIsFrcTr = prIsFrcTr;
	}

	public Integer getPrPrpTare() {
		return PrPrpTare;
	}

	public void setPrPrpTare(Integer prPrpTare) {
		PrPrpTare = prPrpTare;
	}

	public Integer getPrNetWt() {
		return PrNetWt;
	}

	public void setPrNetWt(Integer prNetWt) {
		PrNetWt = prNetWt;
	}

	public Integer getPrByCnt() {
		return PrByCnt;
	}

	public void setPrByCnt(Integer prByCnt) {
		PrByCnt = prByCnt;
	}

	public Boolean isPrIsFrcBC() {
		return PrIsFrcBC;
	}

	public void setPrIsFrcBC(Boolean prIsFrcBC) {
		PrIsFrcBC = prIsFrcBC;
	}

	public Integer getPrShlfLfD() {
		return PrShlfLfD;
	}

	public void setPrShlfLfD(Integer prShlfLfD) {
		PrShlfLfD = prShlfLfD;
	}

	public Integer getPrShlfLfH() {
		return PrShlfLfH;
	}

	public void setPrShlfLfH(Integer prShlfLfH) {
		PrShlfLfH = prShlfLfH;
	}

	public Integer getPrProdLfD() {
		return PrProdLfD;
	}

	public void setPrProdLfD(Integer prProdLfD) {
		PrProdLfD = prProdLfD;
	}

	public Integer getPrProdLfH() {
		return PrProdLfH;
	}

	public void setPrProdLfH(Integer prProdLfH) {
		PrProdLfH = prProdLfH;
	}

	public Boolean isPrIsPrnSL() {
		return PrIsPrnSL;
	}

	public void setPrIsPrnSL(Boolean prIsPrnSL) {
		PrIsPrnSL = prIsPrnSL;
	}

	public Boolean isPrIsPrnPL() {
		return PrIsPrnPL;
	}

	public void setPrIsPrnPL(Boolean prIsPrnPL) {
		PrIsPrnPL = prIsPrnPL;
	}

	public Boolean isPrIsPrnPO() {
		return PrIsPrnPO;
	}

	public void setPrIsPrnPO(Boolean prIsPrnPO) {
		PrIsPrnPO = prIsPrnPO;
	}

	public Integer getPrBnsPts() {
		return PrBnsPts;
	}

	public void setPrBnsPts(Integer prBnsPts) {
		PrBnsPts = prBnsPts;
	}

	public Integer getPrPrtns() {
		return PrPrtns;
	}

	public void setPrPrtns(Integer prPrtns) {
		PrPrtns = prPrtns;
	}

	public Boolean isPrIsUsSL() {
		return PrIsUsSL;
	}

	public void setPrIsUsSL(Boolean prIsUsSL) {
		PrIsUsSL = prIsUsSL;
	}

	public Integer getPrInMode() {
		return PrInMode;
	}

	public void setPrInMode(Integer prInMode) {
		PrInMode = prInMode;
	}

	public Integer getPrLblRot1() {
		return PrLblRot1;
	}

	public void setPrLblRot1(Integer prLblRot1) {
		PrLblRot1 = prLblRot1;
	}

	public Integer getPrLblPlc1() {
		return PrLblPlc1;
	}

	public void setPrLblPlc1(Integer prLblPlc1) {
		PrLblPlc1 = prLblPlc1;
	}

	public Integer getPrLblRot2() {
		return PrLblRot2;
	}

	public void setPrLblRot2(Integer prLblRot2) {
		PrLblRot2 = prLblRot2;
	}

	public Integer getPrLblPlc2() {
		return PrLblPlc2;
	}

	public void setPrLblPlc2(Integer prLblPlc2) {
		PrLblPlc2 = prLblPlc2;
	}

	public Integer getPrLblRot3() {
		return PrLblRot3;
	}

	public void setPrLblRot3(Integer prLblRot3) {
		PrLblRot3 = prLblRot3;
	}

	public Integer getPrLblPlc3() {
		return PrLblPlc3;
	}

	public void setPrLblPlc3(Integer prLblPlc3) {
		PrLblPlc3 = prLblPlc3;
	}

	public String getPrName1() {
		return PrName1;
	}

	public void setPrName1(String prName1) {
		PrName1 = prName1;
	}

	public String getPrName2() {
		return PrName2;
	}

	public void setPrName2(String prName2) {
		PrName2 = prName2;
	}

	public Boolean isPrVCOLTxt() {
		return PrVCOLTxt;
	}

	public void setPrVCOLTxt(Boolean prVCOLTxt) {
		PrVCOLTxt = prVCOLTxt;
	}

	public Boolean isPrCOLReq() {
		return PrCOLReq;
	}

	public void setPrCOLReq(Boolean prCOLReq) {
		PrCOLReq = prCOLReq;
	}

	public Integer getPrPrPlT() {
		return PrPrPlT;
	}

	public void setPrPrPlT(Integer prPrPlT) {
		PrPrPlT = prPrPlT;
	}

	public Integer getPrNum() {
		return prNum;
	}

	public void setPrNum(Integer prNum) {
		this.prNum = prNum;
	}

	public Integer getPrType() {
		return PrType;
	}

	public void setPrType(Integer prType) {
		PrType = prType;
	}

	public Integer getPrBarPref() {
		return PrBarPref;
	}

	public void setPrBarPref(Integer prBarPref) {
		PrBarPref = prBarPref;
	}

	public Integer getPrBarProN() {
		return PrBarProN;
	}

	public void setPrBarProN(Integer prBarProN) {
		PrBarProN = prBarProN;
	}

	public Integer getPrBarVenN() {
		return PrBarVenN;
	}

	public void setPrBarVenN(Integer prBarVenN) {
		PrBarVenN = prBarVenN;
	}

	public String getPrEANCd() {
		return PrEANCd;
	}

	public void setPrEANCd(String prEANCd) {
		PrEANCd = prEANCd;
	}

	public String getPrRawBarC() {
		return PrRawBarC;
	}

	public void setPrRawBarC(String prRawBarC) {
		PrRawBarC = prRawBarC;
	}

	public String getPrCdNm1() {
		return PrCdNm1;
	}

	public void setPrCdNm1(String prCdNm1) {
		PrCdNm1 = prCdNm1;
	}

	public String getPrCdNm2() {
		return PrCdNm2;
	}

	public void setPrCdNm2(String prCdNm2) {
		PrCdNm2 = prCdNm2;
	}

	public Timestamp getPrSpcSD() {
		return PrSpcSD;
	}

	public void setPrSpcSD(Timestamp prSpcSD) {
		PrSpcSD = prSpcSD;
	}

	public String getPrDesc() {
		return PrDesc;
	}

	public void setPrDesc(String prDesc) {
		PrDesc = prDesc;
	}

	public String getPrGrNm1() {
		return PrGrNm1;
	}

	public void setPrGrNm1(String prGrNm1) {
		PrGrNm1 = prGrNm1;
	}

	public String getPrGrNm2() {
		return PrGrNm2;
	}

	public void setPrGrNm2(String prGrNm2) {
		PrGrNm2 = prGrNm2;
	}

	public String getPrGrNm3() {
		return PrGrNm3;
	}

	public void setPrGrNm3(String prGrNm3) {
		PrGrNm3 = prGrNm3;
	}

	public String getPrGrNm4() {
		return PrGrNm4;
	}

	public void setPrGrNm4(String prGrNm4) {
		PrGrNm4 = prGrNm4;
	}

	public String getPrImage() {
		return PrImage;
	}

	public void setPrImage(String prImage) {
		PrImage = prImage;
	}

	public String getPrCOLTN() {
		return PrCOLTN;
	}

	public void setPrCOLTN(String prCOLTN) {
		PrCOLTN = prCOLTN;
	}

	public Integer getPrMealSideItemsGroupNum() {
		return PrMealSideItemsGroupNum;
	}

	public void setPrMealSideItemsGroupNum(Integer prMealSideItemsGroupNum) {
		PrMealSideItemsGroupNum = prMealSideItemsGroupNum;
	}


	public Boolean isPrMealSideItemsAreFree() {
		return PrMealSideItemsAreFree;
	}

	public void setPrMealSideItemsAreFree(Boolean prMealSideItemsAreFree) {
		PrMealSideItemsAreFree = prMealSideItemsAreFree;
	}

	public Integer getPrMealNumSidesIncluded() {
		return PrMealNumSidesIncluded;
	}

	public void setPrMealNumSidesIncluded(Integer prMealNumSidesIncluded) {
		PrMealNumSidesIncluded = prMealNumSidesIncluded;
	}

	public Integer getPrCutTestMeatSpecSheetGroupNum() {
		return PrCutTestMeatSpecSheetGroupNum;
	}

	public void setPrCutTestMeatSpecSheetGroupNum(Integer prCutTestMeatSpecSheetGroupNum) {
		PrCutTestMeatSpecSheetGroupNum = prCutTestMeatSpecSheetGroupNum;
	}

	public Integer getPrWeightMin() {
		return PrWeightMin;
	}

	public void setPrWeightMin(Integer prWeightMin) {
		PrWeightMin = prWeightMin;
	}

	public Integer getPrWeightMax() {
		return PrWeightMax;
	}

	public void setPrWeightMax(Integer prWeightMax) {
		PrWeightMax = prWeightMax;
	}

	public Integer getPrTotalPriceMin() {
		return PrTotalPriceMin;
	}

	public void setPrTotalPriceMin(Integer prTotalPriceMin) {
		PrTotalPriceMin = prTotalPriceMin;
	}

	public Integer getPrTotalPriceMax() {
		return PrTotalPriceMax;
	}

	public void setPrTotalPriceMax(Integer prTotalPriceMax) {
		PrTotalPriceMax = prTotalPriceMax;
	}

	public Integer getPrManualLabelTypeNum1() {
		return PrManualLabelTypeNum1;
	}

	public void setPrManualLabelTypeNum1(Integer prManualLabelTypeNum1) {
		PrManualLabelTypeNum1 = prManualLabelTypeNum1;
	}

	public Integer getPrManualLabelTypeNum2() {
		return PrManualLabelTypeNum2;
	}

	public void setPrManualLabelTypeNum2(Integer prManualLabelTypeNum2) {
		PrManualLabelTypeNum2 = prManualLabelTypeNum2;
	}

	public Integer getPrManualLabelTypeNum3() {
		return PrManualLabelTypeNum3;
	}

	public void setPrManualLabelTypeNum3(Integer prManualLabelTypeNum3) {
		PrManualLabelTypeNum3 = prManualLabelTypeNum3;
	}

	public Integer getPrPrepackLabelTypeNum1() {
		return PrPrepackLabelTypeNum1;
	}

	public void setPrPrepackLabelTypeNum1(Integer prPrepackLabelTypeNum1) {
		PrPrepackLabelTypeNum1 = prPrepackLabelTypeNum1;
	}

	public Integer getPrPrepackLabelTypeNum2() {
		return PrPrepackLabelTypeNum2;
	}

	public void setPrPrepackLabelTypeNum2(Integer prPrepackLabelTypeNum2) {
		PrPrepackLabelTypeNum2 = prPrepackLabelTypeNum2;
	}

	public Integer getPrPrepackLabelTypeNum3() {
		return PrPrepackLabelTypeNum3;
	}

	public void setPrPrepackLabelTypeNum3(Integer prPrepackLabelTypeNum3) {
		PrPrepackLabelTypeNum3 = prPrepackLabelTypeNum3;
	}

	public Integer getPrProdEntryLabelTypeNum1() {
		return PrProdEntryLabelTypeNum1;
	}

	public void setPrProdEntryLabelTypeNum1(Integer prProdEntryLabelTypeNum1) {
		PrProdEntryLabelTypeNum1 = prProdEntryLabelTypeNum1;
	}

	public Integer getPrProdEntryLabelTypeNum2() {
		return PrProdEntryLabelTypeNum2;
	}

	public void setPrProdEntryLabelTypeNum2(Integer prProdEntryLabelTypeNum2) {
		PrProdEntryLabelTypeNum2 = prProdEntryLabelTypeNum2;
	}

	public Integer getPrProdEntryLabelTypeNum3() {
		return PrProdEntryLabelTypeNum3;
	}

	public void setPrProdEntryLabelTypeNum3(Integer prProdEntryLabelTypeNum3) {
		PrProdEntryLabelTypeNum3 = prProdEntryLabelTypeNum3;
	}

	public Integer getPrSelfServLabelTypeNum1() {
		return PrSelfServLabelTypeNum1;
	}

	public void setPrSelfServLabelTypeNum1(Integer prSelfServLabelTypeNum1) {
		PrSelfServLabelTypeNum1 = prSelfServLabelTypeNum1;
	}

	public Integer getPrSelfServLabelTypeNum2() {
		return PrSelfServLabelTypeNum2;
	}

	public void setPrSelfServLabelTypeNum2(Integer prSelfServLabelTypeNum2) {
		PrSelfServLabelTypeNum2 = prSelfServLabelTypeNum2;
	}

	public Integer getPrSelfServLabelTypeNum3() {
		return PrSelfServLabelTypeNum3;
	}

	public void setPrSelfServLabelTypeNum3(Integer prSelfServLabelTypeNum3) {
		PrSelfServLabelTypeNum3 = prSelfServLabelTypeNum3;
	}

	public Integer getPrPick5LabelTypeNum1() {
		return PrPick5LabelTypeNum1;
	}

	public void setPrPick5LabelTypeNum1(Integer prPick5LabelTypeNum1) {
		PrPick5LabelTypeNum1 = prPick5LabelTypeNum1;
	}

	public Integer getPrPick5LabelTypeNum2() {
		return PrPick5LabelTypeNum2;
	}

	public void setPrPick5LabelTypeNum2(Integer prPick5LabelTypeNum2) {
		PrPick5LabelTypeNum2 = prPick5LabelTypeNum2;
	}

	public Integer getPrPick5LabelTypeNum3() {
		return PrPick5LabelTypeNum3;
	}

	public void setPrPick5LabelTypeNum3(Integer prPick5LabelTypeNum3) {
		PrPick5LabelTypeNum3 = prPick5LabelTypeNum3;
	}

	public Integer getPrOnlineOrderingLabelTypeNum1() {
		return PrOnlineOrderingLabelTypeNum1;
	}

	public void setPrOnlineOrderingLabelTypeNum1(Integer prOnlineOrderingLabelTypeNum1) {
		PrOnlineOrderingLabelTypeNum1 = prOnlineOrderingLabelTypeNum1;
	}

	public Integer getPrOnlineOrderingLabelTypeNum2() {
		return PrOnlineOrderingLabelTypeNum2;
	}

	public void setPrOnlineOrderingLabelTypeNum2(Integer prOnlineOrderingLabelTypeNum2) {
		PrOnlineOrderingLabelTypeNum2 = prOnlineOrderingLabelTypeNum2;
	}

	public Integer getPrOnlineOrderingLabelTypeNum3() {
		return PrOnlineOrderingLabelTypeNum3;
	}

	public void setPrOnlineOrderingLabelTypeNum3(Integer prOnlineOrderingLabelTypeNum3) {
		PrOnlineOrderingLabelTypeNum3 = prOnlineOrderingLabelTypeNum3;
	}

	public Integer getPrShlfLfM() {
		return PrShlfLfM;
	}

	public void setPrShlfLfM(Integer prShlfLfM) {
		PrShlfLfM = prShlfLfM;
	}

	public Integer getPrProdLfM() {
		return PrProdLfM;
	}

	public void setPrProdLfM(Integer prProdLfM) {
		PrProdLfM = prProdLfM;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HobItem [prID=");
		builder.append(prID);
		builder.append(", deptNum=");
		builder.append(deptNum);
		builder.append(", clNum=");
		builder.append(clNum);
		builder.append(", TxNum=");
		builder.append(TxNum);
		builder.append(", ProdTxt5Num=");
		builder.append(ProdTxt5Num);
		builder.append(", ProdTxt6Num=");
		builder.append(ProdTxt6Num);
		builder.append(", ProdTxt7Num=");
		builder.append(ProdTxt7Num);
		builder.append(", ProdTxt8Num=");
		builder.append(ProdTxt8Num);
		builder.append(", ProdTxt9Num=");
		builder.append(ProdTxt9Num);
		builder.append(", SMNum=");
		builder.append(SMNum);
		builder.append(", NFNum=");
		builder.append(NFNum);
		builder.append(", NtNum=");
		builder.append(NtNum);
		builder.append(", LbNum1=");
		builder.append(LbNum1);
		builder.append(", LbNum2=");
		builder.append(LbNum2);
		builder.append(", LbNum3=");
		builder.append(LbNum3);
		builder.append(", CaNum=");
		builder.append(CaNum);
		builder.append(", SPNum=");
		builder.append(SPNum);
		builder.append(", LCNum=");
		builder.append(LCNum);
		builder.append(", LPNum=");
		builder.append(LPNum);
		builder.append(", LXNum=");
		builder.append(LXNum);
		builder.append(", DLXNum=");
		builder.append(DLXNum);
		builder.append(", LLID=");
		builder.append(LLID);
		builder.append(", MaNum=");
		builder.append(MaNum);
		builder.append(", PrPrice=");
		builder.append(PrPrice);
		builder.append(", PrPrMod=");
		builder.append(PrPrMod);
		builder.append(", PrIsFrcPr=");
		builder.append(PrIsFrcPr);
		builder.append(", PrExcPr=");
		builder.append(PrExcPr);
		builder.append(", PrDscMe=");
		builder.append(PrDscMe);
		builder.append(", PrDscPr1=");
		builder.append(PrDscPr1);
		builder.append(", PrDscPr2=");
		builder.append(PrDscPr2);
		builder.append(", PrDscPr3=");
		builder.append(PrDscPr3);
		builder.append(", PrTare=");
		builder.append(PrTare);
		builder.append(", PrTareB=");
		builder.append(PrTareB);
		builder.append(", PrIsFrcTr=");
		builder.append(PrIsFrcTr);
		builder.append(", PrPrpTare=");
		builder.append(PrPrpTare);
		builder.append(", PrNetWt=");
		builder.append(PrNetWt);
		builder.append(", PrByCnt=");
		builder.append(PrByCnt);
		builder.append(", PrIsFrcBC=");
		builder.append(PrIsFrcBC);
		builder.append(", PrShlfLfD=");
		builder.append(PrShlfLfD);
		builder.append(", PrShlfLfH=");
		builder.append(PrShlfLfH);
		builder.append(", PrProdLfD=");
		builder.append(PrProdLfD);
		builder.append(", PrProdLfH=");
		builder.append(PrProdLfH);
		builder.append(", PrIsPrnSL=");
		builder.append(PrIsPrnSL);
		builder.append(", PrIsPrnPL=");
		builder.append(PrIsPrnPL);
		builder.append(", PrIsPrnPO=");
		builder.append(PrIsPrnPO);
		builder.append(", PrBnsPts=");
		builder.append(PrBnsPts);
		builder.append(", PrPrtns=");
		builder.append(PrPrtns);
		builder.append(", PrIsUsSL=");
		builder.append(PrIsUsSL);
		builder.append(", PrInMode=");
		builder.append(PrInMode);
		builder.append(", PrLblRot1=");
		builder.append(PrLblRot1);
		builder.append(", PrLblPlc1=");
		builder.append(PrLblPlc1);
		builder.append(", PrLblRot2=");
		builder.append(PrLblRot2);
		builder.append(", PrLblPlc2=");
		builder.append(PrLblPlc2);
		builder.append(", PrLblRot3=");
		builder.append(PrLblRot3);
		builder.append(", PrLblPlc3=");
		builder.append(PrLblPlc3);
		builder.append(", PrName1=");
		builder.append(PrName1);
		builder.append(", PrName2=");
		builder.append(PrName2);
		builder.append(", PrVCOLTxt=");
		builder.append(PrVCOLTxt);
		builder.append(", PrCOLReq=");
		builder.append(PrCOLReq);
		builder.append(", PrPrPlT=");
		builder.append(PrPrPlT);
		builder.append(", prNum=");
		builder.append(prNum);
		builder.append(", PrType=");
		builder.append(PrType);
		builder.append(", PrBarPref=");
		builder.append(PrBarPref);
		builder.append(", PrBarProN=");
		builder.append(PrBarProN);
		builder.append(", PrBarVenN=");
		builder.append(PrBarVenN);
		builder.append(", PrEANCd=");
		builder.append(PrEANCd);
		builder.append(", PrRawBarC=");
		builder.append(PrRawBarC);
		builder.append(", PrCdNm1=");
		builder.append(PrCdNm1);
		builder.append(", PrCdNm2=");
		builder.append(PrCdNm2);
		builder.append(", PrSpcSD=");
		builder.append(PrSpcSD);
		builder.append(", PrDesc=");
		builder.append(PrDesc);
		builder.append(", PrGrNm1=");
		builder.append(PrGrNm1);
		builder.append(", PrGrNm2=");
		builder.append(PrGrNm2);
		builder.append(", PrGrNm3=");
		builder.append(PrGrNm3);
		builder.append(", PrGrNm4=");
		builder.append(PrGrNm4);
		builder.append(", PrImage=");
		builder.append(PrImage);
		builder.append(", PrCOLTN=");
		builder.append(PrCOLTN);
		builder.append(", PrMealSideItemsGroupNum=");
		builder.append(PrMealSideItemsGroupNum);
		builder.append(", PrMealSideItemsAreFree=");
		builder.append(PrMealSideItemsAreFree);
		builder.append(", PrMealNumSidesIncluded=");
		builder.append(PrMealNumSidesIncluded);
		builder.append(", PrCutTestMeatSpecSheetGroupNum=");
		builder.append(PrCutTestMeatSpecSheetGroupNum);
		builder.append(", PrWeightMin=");
		builder.append(PrWeightMin);
		builder.append(", PrWeightMax=");
		builder.append(PrWeightMax);
		builder.append(", PrTotalPriceMin=");
		builder.append(PrTotalPriceMin);
		builder.append(", PrTotalPriceMax=");
		builder.append(PrTotalPriceMax);
		builder.append(", PrManualLabelTypeNum1=");
		builder.append(PrManualLabelTypeNum1);
		builder.append(", PrManualLabelTypeNum2=");
		builder.append(PrManualLabelTypeNum2);
		builder.append(", PrManualLabelTypeNum3=");
		builder.append(PrManualLabelTypeNum3);
		builder.append(", PrPrepackLabelTypeNum1=");
		builder.append(PrPrepackLabelTypeNum1);
		builder.append(", PrPrepackLabelTypeNum2=");
		builder.append(PrPrepackLabelTypeNum2);
		builder.append(", PrPrepackLabelTypeNum3=");
		builder.append(PrPrepackLabelTypeNum3);
		builder.append(", PrProdEntryLabelTypeNum1=");
		builder.append(PrProdEntryLabelTypeNum1);
		builder.append(", PrProdEntryLabelTypeNum2=");
		builder.append(PrProdEntryLabelTypeNum2);
		builder.append(", PrProdEntryLabelTypeNum3=");
		builder.append(PrProdEntryLabelTypeNum3);
		builder.append(", PrSelfServLabelTypeNum1=");
		builder.append(PrSelfServLabelTypeNum1);
		builder.append(", PrSelfServLabelTypeNum2=");
		builder.append(PrSelfServLabelTypeNum2);
		builder.append(", PrSelfServLabelTypeNum3=");
		builder.append(PrSelfServLabelTypeNum3);
		builder.append(", PrPick5LabelTypeNum1=");
		builder.append(PrPick5LabelTypeNum1);
		builder.append(", PrPick5LabelTypeNum2=");
		builder.append(PrPick5LabelTypeNum2);
		builder.append(", PrPick5LabelTypeNum3=");
		builder.append(PrPick5LabelTypeNum3);
		builder.append(", PrOnlineOrderingLabelTypeNum1=");
		builder.append(PrOnlineOrderingLabelTypeNum1);
		builder.append(", PrOnlineOrderingLabelTypeNum2=");
		builder.append(PrOnlineOrderingLabelTypeNum2);
		builder.append(", PrOnlineOrderingLabelTypeNum3=");
		builder.append(PrOnlineOrderingLabelTypeNum3);
		builder.append(", PrShlfLfM=");
		builder.append(PrShlfLfM);
		builder.append(", PrProdLfM=");
		builder.append(PrProdLfM);
		builder.append(", zoneId=");
		builder.append(zoneId);
		builder.append(", zoneName=");
		builder.append(zoneName);
		builder.append("]");
		return builder.toString();
	}
}
