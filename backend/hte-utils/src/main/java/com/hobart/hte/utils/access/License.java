package com.hobart.hte.utils.access;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "hteLicense")
public class License {

    public License() {
    }

    public License(String licenseId, String license, String fileName, String ownerEmail, String fullName, String companyName,
            String companyAddress, Timestamp activationDate, Integer timeout, String productNames, Integer numUsers,
            Integer numScales, String currentLicenseHash, Timestamp refreshedAt, Timestamp updatedAt, Timestamp createdAt,
            String statusMessage, String licenseAndEnterpriseKeys) {
        this.licenseId = licenseId;
        this.license = license;
        this.fileName = fileName;
        this.ownerEmail = ownerEmail;
        this.fullName = fullName;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.activationDate = activationDate;
        this.timeout = timeout;
        this.productNames = productNames;
        this.numUsers = numUsers;
        this.numScales = numScales;
        this.currentLicenseHash = currentLicenseHash;
        this.refreshedAt = refreshedAt;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
        this.statusMessage = statusMessage;
        this.licenseAndEnterpriseKeys = licenseAndEnterpriseKeys;
    }

    @Id
    @Column(columnDefinition = "CHAR(36)")
    public String licenseId;
    @Schema(description = "The type of license installed")
    private String license;
    private String fileName;
    @Schema(description = "email of who the license belongs to")
    private String ownerEmail;
    private String fullName;
    private String companyName;
    private String companyAddress;
    @Schema(description = "When the license was activated")
    private Timestamp activationDate;
    @Schema(description = "How many days the license is valid for")
    private Integer timeout;
    @Schema(description = "Products included in this license")
    @Column(columnDefinition = "TEXT")
    private String productNames;
    @Schema(description = "Number of users permitted to be registered on the site")
    private Integer numUsers;
    @Schema(description = "Number of scales permitted to be registered to the backend")
    private Integer numScales;
    @Schema(description="")
    private String currentLicenseHash;
    @Schema(description="")
    private Timestamp refreshedAt;
    @Schema(description="")
    private Timestamp updatedAt;
    @Schema(description="")
    private Timestamp createdAt;
    @Schema(description="")
    private String statusMessage;
    @Schema(description="The generated keys of the license for unique comparisons")
    private String licenseAndEnterpriseKeys;

    public String getLicenseId() {return this.licenseId;}
    public void setLicenseId(String licenseUUID) {
        this.licenseId = licenseUUID;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public Timestamp getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Timestamp activationDate) {
        this.activationDate = activationDate;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getProductNames() {
        return productNames;
    }

    public void setProductNames(String productNames) {
        this.productNames = productNames;
    }

    public Integer getNumUsers() {
        return numUsers;
    }

    public void setNumUsers(Integer numUsers) {
        this.numUsers = numUsers;
    }

    public Integer getNumScales() {
        return numScales;
    }

    public void setNumScales(Integer numScales) {
        this.numScales = numScales;
    }

    public String getCurrentLicenseHash() {
        return currentLicenseHash;
    }

    public void setCurrentLicenseHash(String currentLicenseHash) {
        this.currentLicenseHash = currentLicenseHash;
    }

    public Timestamp getRefreshedAt() {
        return refreshedAt;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    public void setRefreshedAt(Timestamp refreshedAt) {
        this.refreshedAt = refreshedAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getLicenseAndEnterpriseKeys() {
        return licenseAndEnterpriseKeys;
    }

    public void setLicenseAndEnterpriseKeys(String licenseAndEnterpriseKeys) {
        this.licenseAndEnterpriseKeys = licenseAndEnterpriseKeys;
    }

    @Override
    public String toString() {
        return "License{" +
                "licenseId='" + licenseId + '\'' +
                ", license='" + license + '\'' +
                ", fileName='" + fileName + '\'' +
                ", ownerEmail='" + ownerEmail + '\'' +
                ", fullName='" + fullName + '\'' +
                ", companyName='" + companyName + '\'' +
                ", companyAddress='" + companyAddress + '\'' +
                ", activationDate=" + activationDate +
                ", timeout=" + timeout +
                ", productNames='" + productNames + '\'' +
                ", numUsers=" + numUsers +
                ", numScales=" + numScales +
                ", currentLicenseHash='" + currentLicenseHash + "\'" +
                ", refreshedAt='" + refreshedAt + "\'" +
                ", updatedAt='" + updatedAt + "\'" +
                ", createdAt='" + createdAt + "\'" +
                ", statusMessage='" + statusMessage + "\'" +
                ", licenseAndEnterpriseKeys='" + licenseAndEnterpriseKeys + '\'' +
                '}';
    }
}
