package com.hobart.hte.utils.upgrade;

public class StoreAndBatchIds {

    private String storeId;

    private String batchId;

    public StoreAndBatchIds(String storeId, String batchId) {
        this.storeId = storeId;
        this.batchId = batchId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
}
