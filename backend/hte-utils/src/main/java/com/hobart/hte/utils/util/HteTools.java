package com.hobart.hte.utils.util;

import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.file.FileType;
import com.hobart.hte.utils.item.HobItem;
import com.hobart.hte.utils.tree.ScaleNode;
import com.jcraft.jsch.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class HteTools {

    private static Session scaleSession;
    private static String currentSessionIPAddress;

    public static String[] validConfigExtensions = {".ht", ".label_tagged", ".hlx", ".bin"};
    public static String[] validFeatureExtensions = {".package.tgz"};
    public static String[] validImageExtensions = {".png", ".jpg", ".gif", ".bmp"};
    public static String[] validGraphicsExtensions = {".gif"};
    public static String[] validVideoExtensions = {".mp4", ".avi"};
    public static String[] validAudioExtensions = {".aac", ".m4a"};
    public static String[] validPlaylistExtensions = {".txt"};
    public static String[] validLayoutExtensions = {".png", ".xml"};
    public static String[] validFontExtensions = {".ttf"};
    public static String[] validDocumentExtensions = {".pdf", ".doc", ".docx", ".xls", ".xlsx"};
    public static String[] validLicenseExtensions = {".lic"};
    public static String[] validGTExtensions = {".deb", ".tgz", ".img.bz2"};
    public static List<String> validHTPrefixes = Arrays.asList("HT", "FS", "EP", "NG");
    public static List<String> validGTPrefixes = Arrays.asList("GT", "DT");

    public static <T> T mergeObjects(T first, T second) {
        Class<?> clas = first.getClass();
        Field[] fields = clas.getDeclaredFields();
        Object result = null;
        try {
            result = clas.getDeclaredConstructor().newInstance();
            for (Field field : fields) {
                field.setAccessible(true);
                Object value1 = field.get(first);
                Object value2 = field.get(second);
                Object value = (value1 != null) ? value1 : value2;
                field.set(result, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) result;
    }

    public static boolean isNullorEmpty(String s) {
        return (s == null || s.isEmpty());
    }

    public static boolean isIntegerAndInRange(String strNum, int min, int max) {
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
            if (min > d || max < d) {
                return false;
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isTestRequest(String testValidationStr) {
        if (testValidationStr == null) {
            return false;
        }
        return testValidationStr.equals("crGFr5hGi58KEixP9BbB38BHb57aruAoj8GjwDCmYEkctLbhm");
    }

    public static boolean isValidPath(String path) {
        try {
            Path pathObj = Paths.get(path);
            return Files.exists(pathObj);
        } catch (InvalidPathException | NullPointerException ex) {
            return false;
        }
    }

    private final static String OS = System.getProperty("os.name").toLowerCase();

    public static boolean isWindows() {
        return OS.contains("win");
    }

    public static boolean isMac() {
        return OS.contains("mac");
    }

    public static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
    }

    public static boolean isSolaris() {
        return OS.contains("sunos");
    }

    public static String getServerOS() {
        if (isWindows()) {
            return "win";
        } else if (isMac()) {
            return "osx";
        } else if (isUnix()) {
            return "uni";
        } else if (isSolaris()) {
            return "sol";
        } else {
            return "err";
        }
    }

    public static Session getMainScaleSession() {
        return scaleSession;
    }

    // Starts an SSH session using the instance Session object within this class.
    // Call the disconnectSSH() method to close the instance when you are finished.
    public static Session getSSHSession(String ipAddress) {
        if (scaleSession != null && scaleSession.isConnected() && currentSessionIPAddress.equals(ipAddress)) {
            return scaleSession;
        } else if (scaleSession != null && scaleSession.isConnected() && !currentSessionIPAddress.equals(ipAddress)) {
            // The main session is tied up; create an offshoot session for whatever task needs to be done
            return createScaleSSHSession(ipAddress, false);
        } else {
            // The main session does not seem to be in use or connected, so use it
            scaleSession = createScaleSSHSession(ipAddress, true);
            return scaleSession;
        }
    }

    // Uses this class' Session instance -- if it is connected -- to push a command to the scale.
    // Doesn't bother capturing the result; see executeSSHCommandGetOutput to that end.
    public static boolean executeSSHCommand(Session sshSession, String command) {
        if (sshSession != null && sshSession.isConnected()) {
            try {
                Channel channel = sshSession.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);
                channel.setInputStream(null);

                channel.connect();
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    // Uses this class' Session instance -- if it is connected -- to push a command to the scale.
    // Returns the command output, unlike executeSSHCommand.
    public static String executeSSHCommandGetOutput(Session sshSession, String command) {
        String commandOutput = "";
        if (sshSession != null && sshSession.isConnected()) {
            try {
                Channel channel = sshSession.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                channel.setOutputStream(stream);
                InputStream input = channel.getInputStream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                channel.setInputStream(null);

                channel.connect();

                byte[] read = new byte[1024];
                while (true) {
                    while (input.available() > 0) {
                        int bytes = input.read(read, 0, 1024);
                        if (bytes < 0) {
                            break;
                        }
                        output.write(read, 0, bytes);
                    }
                    if (channel.isClosed()) {
                        if (input.available() > 0) {
                            continue;
                        }
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // do nothing
                    }
                }

                commandOutput = output.toString();
            } catch (Exception e) {
                return commandOutput;
            }
        }
        return commandOutput;
    }

    // Uses this class' Session instance -- if it is connected -- to push a file received from an endpoint to a scale.
    // See this method's overloaded counterpart if you would like to push a file from the local file system.
    public static boolean putFileSFTP(Session sshSession, MultipartFile file, String destinationAbsolutePath) {
        if (sshSession != null && sshSession.isConnected()) {

            File newFile = new File(System.getProperty("java.io.tmpdir") + "/" + file.getOriginalFilename());
            try {
                file.transferTo(newFile);
            } catch (Exception e) {
                return false;
            }

            try {
                Channel channel = sshSession.openChannel("sftp");
                channel.connect();

                ChannelSftp channelSFTP = (ChannelSftp) channel;
                channelSFTP.cd(destinationAbsolutePath);
                channelSFTP.put(newFile.getAbsolutePath(), newFile.getName());
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    // Uses this class' Session instance -- if it is connected -- to push a local file to the scale.
    // Assumes a larger file, such as an upgrade, so it will also monitor the progress of the upload.
    public static boolean putFileSFTP(Session sshSession, File file, String destinationAbsolutePath) {
        if (sshSession != null && sshSession.isConnected() && file != null) {
            try {
                Channel channel = sshSession.openChannel("sftp");
                channel.connect();

                ChannelSftp channelSFTP = (ChannelSftp) channel;
                channelSFTP.cd(destinationAbsolutePath);
                channelSFTP.put(file.getAbsolutePath(), file.getName());

            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    // Disconnects the instance Session object within this class.
    public static void disconnectSSH(Session sshSession) {
        if (sshSession != null && sshSession.isConnected()) {
            sshSession.disconnect();
        }
    }

    // This code works and it has a few usages, so I'm not going to touch this -- yet.
    // For posterity, consider using the more extensible executeSSHCommandGetOutput method.
    public static String getIsHTScale(String ipAddress) {
        JSch jsch = new JSch();
        Session session = null;
        ChannelExec exec = null;
        String result;
        try {
            session = jsch.getSession("root", ipAddress, 22);
            session.setPassword("ho1bart2");
            session.setConfig("StrictHostKeyChecking", "no");
            session.setTimeout(15000);
            session.connect();

            exec = (ChannelExec) session.openChannel("exec");
            exec.setCommand("cd /etc;cat os-version");
            exec.setInputStream(null);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            exec.setOutputStream(stream);
            InputStream input = exec.getInputStream();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            exec.connect();

            byte[] read = new byte[1024];
            while (true) {
                while (input.available() > 0) {
                    int bytes = input.read(read, 0, 1024);
                    if (bytes < 0) {
                        break;
                    }
                    output.write(read, 0, bytes);
                }
                if (exec.isClosed()) {
                    if (input.available() > 0) {
                        continue;
                    }
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // do nothing
                }
            }

            result = output.toString().contains("Timesys") ? "HT" : "GT";
        } catch (JSchException | IOException e) {
            e.printStackTrace();
            result = "TIMEOUT";
        } finally {
            if (exec != null && exec.isConnected()) {
                exec.disconnect();
            }
            if (session != null && session.isConnected()) {
                session.disconnect();
            }
        }

        return result;
    }

    public static Boolean isImageFile(FileType fileType) {
        return fileType == FileType.IMAGE_CATEGORY ||
                fileType == FileType.IMAGE_PRODUCT ||
                fileType == FileType.IMAGE_PLANOGRAM ||
                fileType == FileType.IMAGE_SELFSERVICE ||
                fileType == FileType.IMAGE_SOFTKEY ||
                fileType == FileType.IMAGE_SPECIAL ||
                fileType == FileType.IMAGE_LAYOUT ||
                fileType == FileType.IMAGE_NOW_SERVING ||
                fileType == FileType.IMAGE_ONLINE_ORDERING;
    }

    public static Boolean isLicenseFile(FileType fileType) {
        return fileType == FileType.LICENSE;
    }

    public static boolean verifyFileExtention(String fileName, FileType fileType) {
        if (fileName == null || fileType == null) {
            return false;
        }

        List<Integer> indices = new ArrayList<>();
        int index = fileName.indexOf(".");
        while (index != -1) {
            indices.add(index);
            index = fileName.indexOf(".", index + 1);
        }
        String fileExtension = null;
        if (indices.size() > 1) {
            fileExtension = fileName.substring(indices.get(indices.size() - 2));
        } else {
            fileExtension = fileName.substring(indices.get(indices.size() - 1));
        }

        switch (fileType) {
            case CONFIG:
                for (String ext : validConfigExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case FEATURE:
                for (String ext : validFeatureExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case IMAGE_CATEGORY:
            case IMAGE_PRODUCT:
            case IMAGE_PLANOGRAM:
            case IMAGE_SELFSERVICE:
            case IMAGE_SOFTKEY:
            case IMAGE_LAYOUT:
            case IMAGE_NOW_SERVING:
            case IMAGE_ONLINE_ORDERING:
                for (String ext : validImageExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case IMAGE_SPECIAL:
                for (String ext : validGraphicsExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case VIDEO:
                for (String ext : validVideoExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case AUDIO:
                for (String ext : validAudioExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case PLAYLIST:
                for (String ext : validPlaylistExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case LAYOUT:
                for (String ext : validLayoutExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case FONT:
                for (String ext : validFontExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case DOCUMENT:
                for (String ext : validDocumentExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case LICENSE:
                for (String ext : validLicenseExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case DEBIAN:
            case TARBALL:
            case INSTALL_IMAGE:
                for (String ext : validGTExtensions) {
                    if (fileExtension.equals(ext)) {
                        return true;
                    }
                }
                break;
            case UNKNOWN:
                return true;

        }

        return false;
    }

    //Takes a file name and attempts to infer the file type based on the file extension
    public static FileType inferFileType(String filename) {
        if (filename == null) {
            return FileType.UNKNOWN;
        }
        String fileExtension = filename.substring(filename.indexOf("."));
        for (String ext : validConfigExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.CONFIG;
            }
        }
        for (String ext : validFeatureExtensions) {
            // This evaluation can be flaky for feature file extensions so I've given it an out with the .contains()
            if (fileExtension.equals(ext) || filename.contains(".package.tgz")) {
                return FileType.FEATURE;
            }
        }
        for (String ext : validImageExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.IMAGE_CATEGORY;
            }
        }
        for (String ext : validVideoExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.VIDEO;
            }
        }
        for (String ext : validAudioExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.AUDIO;
            }
        }
        for (String ext : validPlaylistExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.PLAYLIST;
            }
        }
        for (String ext : validLayoutExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.LAYOUT;
            }
        }
        for (String ext : validFontExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.FONT;
            }
        }
        for (String ext : validDocumentExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.DOCUMENT;
            }
        }
        for (String ext : validLicenseExtensions) {
            if (fileExtension.equals(ext)) {
                return FileType.LICENSE;
            }
        }
        if (fileExtension.equals(".deb")) {
            return FileType.DEBIAN;
        }
        if (fileExtension.equals(".tgz")) {
            return FileType.TARBALL;
        }
        if (fileExtension.equals(".img.bz2")) {
            return FileType.INSTALL_IMAGE;
        }

        return FileType.UNKNOWN;
    }

    public static String getFileChecksum(MessageDigest digest, File file) throws IOException {
        // Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        // Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        // Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }
        // close the stream; We don't need it now.
        fis.close();

        // Get the hash's bytes
        byte[] bytes = digest.digest();

        // This bytes[] has bytes in decimal format;
        // Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }

        // return complete hash
        return sb.toString();
    }

    private static final Integer BANNER = 0;
    private static final Integer REGION = 1;
    private static final Integer STORE = 2;
    private static final Integer DEPT = 3;
    private static final Integer SCALE_HOSTNAME = 4;
    private static final Integer SCALE_IP = 5;


    // Takes a specifically formatted CSV file and returns an array of JSON elements
    // This is intended as a one-off bulk import job
    public static List<ScaleNode> readCSVForAssetList(List<List<String>> csvData) {

        List<ScaleNode> payload = new ArrayList<>(); // Payload to give to the endpoint
        List<String> basicCache = new ArrayList<>(5); // Basic memory cache for the algorithm later on

        for (List<String> i : csvData) {

            ScaleNode bannerNode = new ScaleNode(null, i.get(BANNER), null, "banner");
            ScaleNode regionNode = new ScaleNode(null, i.get(REGION), null, "region");
            ScaleNode storeNode = new ScaleNode(null, i.get(STORE), null, "store");
            ScaleNode deptNode = new ScaleNode(null, i.get(DEPT), null, "dept");
            ScaleNode scaleNode = new ScaleNode(null, i.get(SCALE_HOSTNAME), null, "scale");
            ScaleDevice props = new ScaleDevice();
            props.setIpAddress(i.get(SCALE_IP));
            props.setHostname(i.get(SCALE_HOSTNAME));
            props.setEnabled(true); // use Active column to determine true/false
            props.setIsPrimaryScale(false);
            scaleNode.setProperties(props);

            // todo: Make these algorithms a bit less ugly, if possible -- this works but could be more succinct
            if (payload.isEmpty()) {
                System.out.println("Payload empty; loading cache");
                for (int x : new int[]{BANNER, REGION, STORE, DEPT, SCALE_IP}) {
                    basicCache.add(String.valueOf(i.get(x))); // Fill up the cache
                }
                payload.add(bannerNode);
                payload.add(regionNode);
                payload.add(storeNode);
                payload.add(deptNode);
                payload.add(scaleNode);
            }

            if (!basicCache.get(BANNER).equals(i.get(BANNER))) {
                basicCache.clear();

                payload.add(bannerNode);
                basicCache.add((String) i.get(BANNER));
                payload.add(regionNode);
                basicCache.add((String) i.get(REGION));
                payload.add(storeNode);
                basicCache.add((String) i.get(STORE));
                payload.add(deptNode);
                basicCache.add((String) i.get(DEPT));
                payload.add(scaleNode);
                basicCache.add((String) i.get(SCALE_IP));
            }

            if (!basicCache.get(REGION).equals(i.get(REGION))) {
//                    System.out.println("Region mismatch");
                for (int x : new int[]{4, 3, 2, 1}) {
                    basicCache.remove(x); // Empty the cache
                }
                payload.add(regionNode);
                basicCache.add((String) i.get(REGION));
                payload.add(storeNode);
                basicCache.add((String) i.get(STORE));
                payload.add(deptNode);
                basicCache.add((String) i.get(DEPT));
                payload.add(scaleNode);
                basicCache.add((String) i.get(SCALE_IP));
            }

            if (!basicCache.get(STORE).equals(i.get(STORE))) {
//                    System.out.println("Store mismatch");
                for (int x : new int[]{4, 3, 2}) {
                    basicCache.remove(x);
                }
                payload.add(storeNode);
                basicCache.add((String) i.get(STORE));
                payload.add(deptNode);
                basicCache.add((String) i.get(DEPT));
                payload.add(scaleNode);
                basicCache.add((String) i.get(SCALE_IP));
            }

            if (!basicCache.get(DEPT).equals(i.get(DEPT))) {
//                    System.out.println("Department mismatch");
                for (int x : new int[]{4, 3}) {
                    basicCache.remove(x);
                }
                payload.add(deptNode);
                basicCache.add((String) i.get(DEPT));
                payload.add(scaleNode);
                basicCache.add((String) i.get(SCALE_IP));
            }

            if (!basicCache.get(4).equals(i.get(SCALE_IP))) {
//                    System.out.println("Scale mismatch");
                basicCache.remove(4);
                payload.add(scaleNode);
                basicCache.add((String) i.get(SCALE_IP));
            }

            System.out.println(basicCache);
        }
        return payload;
    }

    /**
     * Takes a list of objects to parse and convert into tagged format
     * Currently only handles PLUs
     * @param info the list of objects to parse and translate into tagged format
     * @return
     */
    public static <T extends Object> String createHTFile(List<T> info, String fileNameOverride) throws IOException {

        StringBuilder contents = new StringBuilder();
        final char unitSeparator = 31;
        final char recordSeparator = 30;

        for (Object data : info) {
            if (data instanceof HobItem) {
                contents.append("RT89" + unitSeparator);
                HobItem plu = (HobItem) data;
                contents.append(appendTags("d#", plu.getDeptNum())).append(unitSeparator);
                contents.append(appendTags("p#", plu.getPrNum())).append(unitSeparator);
                contents.append(appendTags("PT", plu.getPrType())).append(unitSeparator);
                contents.append(appendTags("dt", plu.getPrDesc())).append(unitSeparator);
                contents.append(appendTags("u#", plu.getPrBarPref())).append(unitSeparator);
                contents.append(appendTags("up", plu.getPrRawBarC())).append(unitSeparator); // barcode upc num

                if (plu.getPrNum() != null) {
                    int leadingZeros = 4 - (int) (Math.log10(plu.getPrNum()) + 1);
                    contents.append("pn");
                    for (int i = 0; i < leadingZeros; i++) {
                        contents.append("0");
                    }
                    contents.append(plu.getPrNum()).append(unitSeparator);
                } else {
                    contents.append("pn").append(unitSeparator);
                }

                contents.append(appendTags("vn", plu.getPrBarVenN())).append(unitSeparator);
                contents.append(appendTags("Ec", plu.getPrEANCd())).append(unitSeparator);
                contents.append(appendTags("bc", plu.getPrByCnt())).append(unitSeparator);
//                contents.append("fb").append().append(unitSeparator);
                contents.append(appendTags("u$", plu.getPrPrice())).append(unitSeparator);
//                contents.append("fp").append().append(unitSeparator);
                contents.append(appendTags("pm", plu.getPrPrMod())).append(unitSeparator);
                contents.append(appendTags("xp", plu.getPrExcPr())).append(unitSeparator);
//                contents.append("Dt").append().append(unitSeparator); // discount type
                contents.append(appendTags("dp", plu.getPrDscPr1())).append(unitSeparator);
                contents.append(appendTags("D2", plu.getPrDscPr2())).append(unitSeparator);
                contents.append(appendTags("D3", plu.getPrDscPr3())).append(unitSeparator);
                contents.append(appendTags("ta", plu.getPrTare())).append(unitSeparator);
//                contents.append("fT").append().append(unitSeparator);
//                contents.append("pt").append().append(unitSeparator); // proportional tare

                contents.append("nw");
                if (plu.getPrNetWt() == null) {
                    // random weight = 1, by count = 3, fluid ounces = 4
                    if (plu.getPrType() == 1 || plu.getPrType() >= 3) {
                        contents.append(0);
                    // fixed weight = 2; this needs some kind of value
                    } else if (plu.getPrType() == 2) {
                        contents.append(1);
                    }
                } else {
                    contents.append(plu.getPrNetWt());
                }
                contents.append(unitSeparator);

                contents.append(appendTags("sl", plu.getPrShlfLfD())).append(unitSeparator);
                contents.append(appendTags("SL", plu.getPrShlfLfH())).append(unitSeparator);
                contents.append(appendTags("pl", plu.getPrProdLfD())).append(unitSeparator);
                contents.append(appendTags("PL", plu.getPrProdLfH())).append(unitSeparator);
//                contents.append("PS").append().append(unitSeparator);
//                contents.append("Pi").append().append(unitSeparator);
//                contents.append("PP").append().append(unitSeparator);
//                contents.append("us").append().append(unitSeparator);
                contents.append(appendTags("rc", plu.getClNum())).append(unitSeparator);
                contents.append(appendTags("l1", plu.getLbNum1())).append(unitSeparator);
                contents.append(appendTags("p1", plu.getPrLblRot1())).append(unitSeparator);
                contents.append(appendTags("l2", plu.getLbNum2())).append(unitSeparator);
                contents.append(appendTags("p2", plu.getPrLblRot2())).append(unitSeparator);
                contents.append(appendTags("l3", plu.getLbNum3())).append(unitSeparator);
                contents.append(appendTags("p3", plu.getPrLblRot3())).append(unitSeparator);
                contents.append(appendTags("L1", plu.getPrLblPlc1())).append(unitSeparator);
                contents.append(appendTags("L2", plu.getPrLblPlc2())).append(unitSeparator);
                contents.append(appendTags("L3", plu.getPrLblPlc3())).append(unitSeparator);
//                contents.append("ps").append().append(unitSeparator); // offset points
                contents.append(appendTags("im", plu.getPrInMode())).append(unitSeparator);
//                contents.append("g#").append().append(unitSeparator); // graphic num
                contents.append(appendTags("G1", plu.getPrGrNm1())).append(unitSeparator);
                contents.append(appendTags("G2", plu.getPrGrNm2())).append(unitSeparator);
                contents.append(appendTags("G3", plu.getPrGrNm3())).append(unitSeparator);
                contents.append(appendTags("G4", plu.getPrGrNm4())).append(unitSeparator);
                contents.append(appendTags("r#", plu.getTxNum())).append(unitSeparator); // expanded text num
                contents.append(appendTags("s#", plu.getSMNum())).append(unitSeparator); // special message num
//                contents.append("n#").append().append(unitSeparator); // product note
                contents.append(appendTags("N#", plu.getNtNum())).append(unitSeparator); // nutrifact data
//                contents.append("m#").append().append(unitSeparator); // marquee text num
//                contents.append("y5").append().append(unitSeparator);
//                contents.append("y6").append().append(unitSeparator);
//                contents.append("y7").append().append(unitSeparator);
//                contents.append("y8").append().append(unitSeparator);
//                contents.append("y9").append().append(unitSeparator);
//                contents.append("pf").append().append(unitSeparator); // plu flags
//                contents.append("tf").append().append(unitSeparator); // totals flags
//                contents.append("xt").append().append(unitSeparator); // exception price tare
//                contents.append("i1").append().append(unitSeparator);
                contents.append(appendTags("n1", plu.getPrName1())).append(unitSeparator);
                contents.append(appendTags("n2", plu.getPrName2())).append(unitSeparator);
//                contents.append("C#").append().append(unitSeparator);
//                contents.append("S#").append().append(unitSeparator);
//                contents.append("cb").append().append(unitSeparator); // cool category num
//                contents.append("ci").append().append(unitSeparator); // cool pretext num
//                contents.append("c7").append().append(unitSeparator); // cool default text num
//                contents.append("c8").append().append(unitSeparator); // cool text num
//                contents.append("c9").append().append(unitSeparator); // cool short list first link
//                contents.append("cc").append().append(unitSeparator); // cool tracking text
//                contents.append("cf").append().append(unitSeparator); // cool verify forced
//                contents.append("co").append().append(unitSeparator); // cool required
//                contents.append("cs").append().append(unitSeparator); // cool short list num
                contents.append(appendTags("Pp", plu.getPrPrPlT())).append(unitSeparator);
//                contents.append("Bg").append().append(unitSeparator);
//                contents.append("Bf").append().append(unitSeparator);
//                contents.append("Bn").append().append(unitSeparator);
                contents.append(appendTags("cM", plu.getPrCutTestMeatSpecSheetGroupNum())).append(unitSeparator);
                contents.append(appendTags("T2", plu.getPrTareB())).append(recordSeparator);
            }
        }
        char[] lineFeed = {0xa};
        String replaced = contents.toString().replaceAll("\n", new String(lineFeed));
        StringBuilder newContents = new StringBuilder(replaced);

        String filePath;
        String fileName;
        if (fileNameOverride == null) {
            fileName = "plu_data_" + LocalDateTime.now() + ".ht";
        } else {
            fileName = fileNameOverride + ".ht";
        }
        if (isWindows()) {
            filePath = "C:\\Users\\Public\\Documents\\hte\\repository\\" + fileName;
        } else {
            filePath = "/home/hobart/repository/" + fileName;
        }
        try (FileWriter writer = new FileWriter(filePath)) {
            writer.write(newContents.toString());
        }

        return filePath;
    }

    private static String appendTags(String tag, Object value) {
        if (value != null) {
            return tag + value;
        } else {
            return tag;
        }
    }

    private static Session createScaleSSHSession(String ipAddress, boolean isMainSession) {
        try {
            // SSH connection setup
            JSch jsch = new JSch();
            Session newSession = jsch.getSession("root", ipAddress, 22);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");

            // Connecting to the scale...
            newSession.setPassword("ho1bart2");
            newSession.setConfig(config);
            newSession.setTimeout(10000); // Ten seconds
            newSession.connect();

            if (isMainSession) {
                currentSessionIPAddress = ipAddress;
            }
            return newSession;
        } catch (Exception e) {
            if (isMainSession) {
                currentSessionIPAddress = "";
            }
            return null;
        }
    }

    public interface AsyncExecutor <T> {
        void execute();
    }
    public static <T extends AsyncExecutor<T>> void startAsyncThread(T toExecute) {
        Thread thread = new Thread(() -> {
            toExecute.execute();
        });
        thread.start();
    }
}
