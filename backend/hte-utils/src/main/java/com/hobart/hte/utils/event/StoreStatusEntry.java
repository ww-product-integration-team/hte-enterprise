package com.hobart.hte.utils.event;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "storeStatusLog")
public class StoreStatusEntry {
    @Id
    @Column(columnDefinition = "TIMESTAMP(3)")
    private Timestamp timestamp;
    private String storeUuid;
    private boolean inSync;
    private boolean deleteEvent;
    private boolean event1;
    private boolean event2;
    private boolean event3;

    public StoreStatusEntry() {
    }

    public StoreStatusEntry(String storeUuid, boolean inSync, boolean deleteEvent) {
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.storeUuid = storeUuid;
        this.inSync = inSync;
        this.deleteEvent = deleteEvent;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getStoreUuid() {
        return storeUuid;
    }

    public void setStoreUuid(String storeUuid) {
        this.storeUuid = storeUuid;
    }

    public boolean isInSync() {
        return inSync;
    }

    public void setInSync(boolean inSync) {
        this.inSync = inSync;
    }

    public boolean isDeleteEvent() {
        return deleteEvent;
    }

    public void setDeleteEvent(boolean deleteEvent) {
        this.deleteEvent = deleteEvent;
    }

    public boolean isEvent1() {
        return event1;
    }

    public void setEvent1(boolean event1) {
        this.event1 = event1;
    }

    public boolean isEvent2() {
        return event2;
    }

    public void setEvent2(boolean event2) {
        this.event2 = event2;
    }

    public boolean isEvent3() {
        return event3;
    }

    public void setEvent3(boolean event3) {
        this.event3 = event3;
    }
}
