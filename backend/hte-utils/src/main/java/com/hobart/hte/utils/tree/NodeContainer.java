package com.hobart.hte.utils.tree;

import java.util.*;

public class NodeContainer {
    private Map<String, ScaleNode> nodes;
    private NodeStatus nodeStatus;

    public NodeContainer(Map<String, ScaleNode> nodes, NodeStatus nodeStatus) {
        this.nodes = nodes;
        this.nodeStatus = nodeStatus;
    }

    public void addNode(ScaleNode sn) {
        if (nodes == null) {
            nodes = new HashMap<>();
        }
        nodes.put(sn.getId(), sn);
    }

    public Map<String, ScaleNode> getNodes() {
        return nodes;
    }

    public void generateNodeStatus(){
        //Combine each of the child node statuses into it's parent
        for(ScaleNode node: nodes.values()){
            nodeStatus.AddChildStatus(node.getNodeStatus());
        }
    }

    public void setNodes(Map<String, ScaleNode> nodes) {
        this.nodes = nodes;
    }

    public NodeStatus getNodeStatus() {
        return nodeStatus;
    }

    public void setNodeStatus(NodeStatus nodeStatus) {
        this.nodeStatus = nodeStatus;
    }
}
