package com.hobart.hte.utils.access;

import com.hobart.hte.utils.model.EntityType;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Schema(description = "This class represents a domain and any of it's child permissions")
@Entity
@Table(name = "hteDomain")
public class HTeDomain {

    public HTeDomain(String i, EntityType type, String parentId) {
        super();
        this.domainId = i;
        this.type = type.toString();
        this.parentId = parentId;
    }

    public HTeDomain() {

    }

    @Id @Column(columnDefinition = "VARCHAR(36)")
    private String domainId;
    private String type;
    @Column(columnDefinition = "VARCHAR(36)")
    private String parentId;

    public String getDomainId() {return domainId;}

    public void setDomainId(String domainId) {this.domainId = domainId;}

    public EntityType getType() {return EntityType.valueOf(type);}

    public void setType(EntityType type) {this.type = type.toString();}

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String children) {
        this.parentId = children;
    }

    @Override
    public String toString() {
        return "HTeDomain{" +
                "domainId='" + domainId + '\'' +
                ", type='" + type + '\'' +
                ", parentId='" + parentId + '\'' +
                '}';
    }
}
