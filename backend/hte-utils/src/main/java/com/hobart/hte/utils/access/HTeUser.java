package com.hobart.hte.utils.access;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hobart.hte.utils.config.AppConfig;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Schema(description = "This class represents a scale with its basic information.")
@Entity
@Table(name = "hteUser")
public class HTeUser {

    @Id
    @Schema(description = "A generated number that represents the user")
    private String id;
    @Schema(description = "First name of the user")
    private String firstName;
    @Schema(description = "Last name of the user")
    private String lastName;
    @Schema(description = "Email/Username of the user")
    private String username;
    @Schema(description = "Password")
    private String password;
    @Schema(description = "Phone number for possible SMS alert messages")
    private String phoneNumber;
    @Schema(description = "General access level that indicates a users general permissions")
    private Integer accessLevel;
    @Schema(description = "JSON Object of Roles")
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<HTeRole> roles = new ArrayList<>();
    @ManyToOne
    @JoinColumn(name="domainId")
    private HTeDomain domain;
    @Schema(description = "Indicates whether the user is able to perform actions")
    private Boolean enabled;
    @Schema(description = "Date account was created")
    private Timestamp dateCreated;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    @JsonProperty(value = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAccessLevel(Integer accessLevel){
        this.accessLevel = accessLevel;
    }

    public Integer getAccessLevel(){
        return accessLevel;
    }

    public Collection<HTeRole> getRoles() {
        return roles;
    }

    public void setRoles(Collection<HTeRole> roles) {
        this.roles = roles;
    }

    public boolean addRole(HTeRole role){
        for (HTeRole nextRole : roles) {
            if (Objects.equals(nextRole.getId(), role.getId())) {
                return false;
            }
        }
        try{
            roles.add(role);
        }catch (Exception ex) {
            return false;
        }

        return true;
    }

    public boolean removeRole(HTeRole role){
        return roles.removeIf(nextRole -> Objects.equals(nextRole.getId(), role.getId()));
    }

    public HTeDomain getDomain() {return domain;}

    public void setDomain(HTeDomain domain) {this.domain = domain;}

    public Boolean getEnabled() {return enabled;}

    public void setEnabled(Boolean enabled) {this.enabled = enabled;}

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getDateCreated() {return dateCreated;}

    public void setDateCreated(Timestamp dateCreated) {this.dateCreated = dateCreated;}

    @JsonIgnore
    public boolean isAdmin(){
        return id.equals(AppConfig.HtAdminId);
    }

    @Override
    public String toString() {
        return "HTeUser{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", accessLevel=" + accessLevel +
                ", roles=" + roles +
                ", domain=" + domain +
                '}';
    }
}
