package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Wireless {
	private String macAddress;
	private String ip4Address;
	private String ip6Address;
	private String dhcpServer;
	private String subnetMask;
	private String defaultGateway;
	private String channel;
	private boolean autoLinkRate;
	/**
	 * "AUTO" | "MBS_PER_SECOND",
	 */
	private String linkRate;
	private String signalStrenght;
	private Adapter adapter;
	private Authentication authentication;
	private boolean isConnected;

	@JsonProperty(value = "macAddress")
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@JsonProperty(value = "ip4Address")
	public String getIp4Address() {
		return ip4Address;
	}

	public void setIp4Address(String ip4Address) {
		this.ip4Address = ip4Address;
	}

	@JsonProperty(value = "ip6Address")
	public String getIp6Address() {
		return ip6Address;
	}

	public void setIp6Address(String ip6Address) {
		this.ip6Address = ip6Address;
	}

	@JsonProperty(value = "channel")
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	@JsonProperty(value = "isAutoLinkRate")
	public boolean isAutoLinkRate() {
		return autoLinkRate;
	}

	public void setAutoLinkRate(boolean autoLinkRate) {
		this.autoLinkRate = autoLinkRate;
	}

	@JsonProperty(value = "linkRate")
	public String getLinkRate() {
		return linkRate;
	}

	public void setLinkRate(String linkRate) {
		this.linkRate = linkRate;
	}

	@JsonProperty(value = "signalStrenght")
	public String getSignalStrenght() {
		return signalStrenght;
	}

	public void setSignalStrenght(String signalStrenght) {
		this.signalStrenght = signalStrenght;
	}

	@JsonProperty(value = "adapter")
	public Adapter getAdapter() {
		return adapter;
	}

	public void setAdapter(Adapter adapter) {
		this.adapter = adapter;
	}

	@JsonProperty(value = "authentication")
	public Authentication getAuthentication() {
		return authentication;
	}

	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
	}

	public String getDhcpServer() {
		return dhcpServer;
	}

	public void setDhcpServer(String dhcpServer) {
		this.dhcpServer = dhcpServer;
	}

	public String getSubnetMask() {
		return subnetMask;
	}

	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}

	public String getDefaultGateway() {
		return defaultGateway;
	}

	public void setDefaultGateway(String defaultGateway) {
		this.defaultGateway = defaultGateway;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nWireless [macAddress=");
		builder.append(macAddress);
		builder.append(", ip4Address=");
		builder.append(ip4Address);
		builder.append(", ip6Address=");
		builder.append(ip6Address);
		builder.append(", dhcpServer=");
		builder.append(dhcpServer);
		builder.append(", subnetMask=");
		builder.append(subnetMask);
		builder.append(", defaultGateway=");
		builder.append(defaultGateway);
		builder.append(", channel=");
		builder.append(channel);
		builder.append(", autoLinkRate=");
		builder.append(autoLinkRate);
		builder.append(", linkRate=");
		builder.append(linkRate);
		builder.append(", signalStrenght=");
		builder.append(signalStrenght);
		builder.append(", adapter=");
		builder.append(adapter);
		builder.append(", authentication=");
		builder.append(authentication);
		builder.append(", isConnected=");
		builder.append(isConnected);
		builder.append("]");
		return builder.toString();
	}
}
