package com.hobart.hte.utils.device.stats;

public class DeviceCountPerOnlineStatus {
	private Long total;

	public DeviceCountPerOnlineStatus(Long total) {
		super();
		this.total = total;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

}
