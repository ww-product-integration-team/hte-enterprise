package com.hobart.hte.utils.heartbeat;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.hobart.hte.utils.file.HobFiles;

import java.sql.Timestamp;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "eventType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ScaleConfigEvent.class, name = "SCALE_CONFIG_EVENT"),
        @JsonSubTypes.Type(value = DownloadEvent.class, name = "SYNC_CONFIG"),
        @JsonSubTypes.Type(value = DeleteEvent.class, name = "DELETE")
})
public class ScaleEvent {
    /**
     * whether is to download an HT or a .package.tgz file
     */
    private String eventType;
    private String uuid;
    private Timestamp timestamp;
    /**
     * the url the scale will use to notify is done with the action
     */
    private String callbackUrl;

    /**
     *
     * @param eventType
     * @param uuid
     * @param callbackUrl
     */
    public ScaleEvent(String eventType, String uuid, String callbackUrl) {
        super();
        this.eventType = eventType;
        this.uuid = uuid;
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.callbackUrl = callbackUrl;
    }

    @JsonProperty(value = "eventType")
    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}