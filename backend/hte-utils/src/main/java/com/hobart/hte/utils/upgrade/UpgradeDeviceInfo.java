package com.hobart.hte.utils.upgrade;

public class UpgradeDeviceInfo {

    private String upgradeId;

    private String deviceId;

    private Boolean canConnect;

    private Boolean hasFile;

    private String targetFirmware;

    public String getUpgradeId() {
        return upgradeId;
    }

    public void setUpgradeId(String upgradeId) {
        this.upgradeId = upgradeId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getCanConnect() {
        return canConnect;
    }

    public void setCanConnect(Boolean canConnect) {
        this.canConnect = canConnect;
    }

    public Boolean getHasFile() {
        return hasFile;
    }

    public void setHasFile(Boolean hasFile) {
        this.hasFile = hasFile;
    }

    public String getTargetFirmware() {
        return targetFirmware;
    }

    public void setTargetFirmware(String targetFirmware) {
        this.targetFirmware = targetFirmware;
    }
}
