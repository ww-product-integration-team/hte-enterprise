package com.hobart.hte.utils.event;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hobart.hte.utils.model.SyncStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "deviceStatusLog")
public class DeviceStatusEntry {
    @Id
    @Column(columnDefinition = "TIMESTAMP(3)")
    private Timestamp timestamp;
    private String deviceUuid;
    private boolean inSync;
    private SyncStatus logStatus;
    private boolean deleteEvent;
    private boolean event1;
    private boolean event2;
    private boolean event3;
    private boolean event4;

    public DeviceStatusEntry() {
    }

    public DeviceStatusEntry(String deviceUuid, boolean inSync) {
        super();
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.deviceUuid = deviceUuid;
        this.inSync = inSync;
    }

    public DeviceStatusEntry(String deviceUuid, boolean inSync, SyncStatus logStatus, boolean deleteEvent) {
        super();
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.deviceUuid = deviceUuid;
        this.inSync = inSync;
        this.logStatus = logStatus;
        this.deleteEvent = deleteEvent;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public boolean isInSync() {
        return inSync;
    }

    public Integer isInSyncInt(){
        if(inSync){
            return 1;
        }
        else{
            return 0;
        }
    }

    public void setInSync(boolean inSync) {
        this.inSync = inSync;
    }

    public SyncStatus getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(SyncStatus logStatus) {
        this.logStatus = logStatus;
    }

    public boolean isDeleteEvent() {
        return deleteEvent;
    }

    public void setDeleteEvent(boolean deleteEvent) {
        this.deleteEvent = deleteEvent;
    }

    public boolean isEvent1() {
        return event1;
    }

    public void setEvent1(boolean event1) {
        this.event1 = event1;
    }

    public boolean isEvent2() {
        return event2;
    }

    public void setEvent2(boolean event2) {
        this.event2 = event2;
    }

    public boolean isEvent3() {
        return event3;
    }

    public void setEvent3(boolean event3) {
        this.event3 = event3;
    }

    public boolean isEvent4() {
        return event4;
    }

    public void setEvent4(boolean event4) {
        this.event4 = event4;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DeviceSyncStatus [timestamp=");
        builder.append(timestamp);
        builder.append(", deviceUuid=");
        builder.append(deviceUuid);
        builder.append(", inSync=");
        builder.append(inSync);
        builder.append("]");
        return builder.toString();
    }
}

