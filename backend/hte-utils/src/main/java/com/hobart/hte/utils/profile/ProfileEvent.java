package com.hobart.hte.utils.profile;

import com.hobart.hte.utils.store.StoreEvent;

import java.util.ArrayList;
import java.util.List;

public class ProfileEvent extends StoreEvent {

    private List<ProfileResponse> profiles;

    public ProfileEvent() {
        super();
        this.setEventType("SYNC_PROFILE");
        this.profiles = new ArrayList<ProfileResponse>();
    }

    public List<ProfileResponse> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<ProfileResponse> profiles) {
        this.profiles = profiles;
    }
}
