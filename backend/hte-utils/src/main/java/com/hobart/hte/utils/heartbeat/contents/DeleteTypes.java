package com.hobart.hte.utils.heartbeat.contents;

public enum DeleteTypes {
    flashkey, label, plu, operator, config
}
