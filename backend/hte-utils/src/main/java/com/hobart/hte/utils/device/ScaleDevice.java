package com.hobart.hte.utils.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

@Schema(description = "This class represents a scale with its basic information.")
@Entity
@Table(name = "scaleDevice")
public class ScaleDevice  {

    @Transient
    @JsonIgnore
    private static final String invisibleScaleName = "InvisibleScale-JwFqp4bqiFzRLcFVV3qf";

    @Id
    @Schema(description = "A UUID number to identify the scale in the system.")
    private String deviceId;
    @Schema(description = "The scale IP address of the scale, this is also a unique identifier.")
    private String ipAddress;
    @Schema(description = "The corresponding scale hostname currently set up.")
    private String hostname;
    @Schema(description = "The scale serial number, this field is returned by the scale.")
    private long serialNumber;
    @Schema(description = "The scale model, this value is returned by the scale, can be empty.")
    private String scaleModel;
    @Schema(description = "The number of the store where the scale is installed.")
    private String storeId;
    @Schema(description = "The number of department where the scale is located and assigned.")
    private String deptId;
    @Schema(description = "The country code where the scale is installed. For instance: US, MX, CA, CR, CL, etc.")
    private String countryCode;
    @Schema(description = "The number of the building")
    private String buildNumber;
    @Schema(description = "This is the scale application version, also known as firmware. This value is returned by the scale.")
    private String application;
    @Schema(description = "The version of the scale operating system.")
    private String operatingSystem;
    @Schema(description = "The version of the System controller of the scale.")
    private String systemController;
    @Schema(description = "The version of the bootloader of the scale.")
    private String loader;
    @Schema(description = "Version of the SM backend module.")
    private String smBackend;
    @Schema(description = "This is the last time the scale reported back to the system with a heartbeat.")
    private Timestamp lastReportTimestampUtc;
    @Schema(description = "The description of the reason the scale connected to the service. For instance: INITIAL, REBOOT, SHUTDOWN, MANUAL, INTERVAL.")
    private String lastTriggerType;
    @Schema(description = "This is the identifier of the profile the scale will be associated to.")
    private String profileId;
    @Schema(description = "Indicates whether the scale is a primary or secondary scale. Depending on this the scale will host packaget.tgz files or not for the other scales.")
    private Boolean isPrimaryScale;
    @Schema(description = "Determines if the scale is enabled or not. If not enabled the scale won't update it's status and won't receive any update.")
    private Boolean enabled;
    @Schema(description = "The number of items loaded on the scale currently.")
    private Integer pluCount;
    @Schema(description = "Number of total labels ever printed in the scale, this depends on the data stored in the scale/mainboard.")
    private Integer totalLabelsPrinted;
    @Transient
    private String upgradeId;
    @Transient
    private String weigherPrecision;
    @Transient
    private String labelStockSize;
    @Schema(description = "Indicates whether a profile was recently updated; alters heartbeat logic to avoid false positives in checksum logs")
    private Boolean profileRecentlyUpdated;
    @Schema(description = "This is the variable to contain the info.log file pulled from the scale.")
    private String healthInfoLog;

    public ScaleDevice(String device_uuid, ScaleInfo si) {
        this.deviceId = device_uuid;
        this.serialNumber = si.getSerialNumber();
        this.scaleModel = si.getScaleModel();
        this.storeId = si.getStoreId();
        this.deptId = si.getDeptId();
        this.countryCode = si.getCountryCode();
        this.profileId = si.getProfileId();
        this.buildNumber = si.getBuildNumber();
        this.application = si.getApplication();
        this.operatingSystem = si.getOperatingSystem();
        this.systemController = si.getSystemController();
        this.loader = si.getLoader();
        this.smBackend = si.getSmBackendVersion();
        this.isPrimaryScale = false;
        this.enabled = true;
        if (si.gethealthInfoLog() == null) {
            this.healthInfoLog = "";
            si.sethealthInfoLog(""); // Stops the HeartbeatService methods from freaking out on older app hook versions
        } else if (si.gethealthInfoLog().length() > 12500) {
            this.healthInfoLog = si.gethealthInfoLog().substring(si.gethealthInfoLog().length()-12500);
        } else {
            this.healthInfoLog = si.gethealthInfoLog();
        }
    }
    public ScaleDevice(String deviceId, String ipAddress, String hostname, Long serialNumber, String scaleModel, String storeId,
                       String deptId, String countryCode, String buildNumber, String application, String operatingSystem,
                       String systemController, String loader, String smBackend,  String lastTriggerType,String profileId,
                       boolean isPrimaryScale, boolean enabled, Integer pluCount, Integer totalLabelsPrinted,
                       boolean profileRecentlyUpdated, String labelStockSize, String weigherPrecision){
        this.deviceId = deviceId;
        this.ipAddress = ipAddress;
        this.hostname = hostname;
        this.lastTriggerType = lastTriggerType;
        this.pluCount = pluCount;
        this.serialNumber = serialNumber;
        this.scaleModel = scaleModel;
        this.storeId = storeId;
        this.deptId = deptId;
        this.countryCode = countryCode;
        this.profileId = profileId;
        this.buildNumber = buildNumber;
        this.application = application;
        this.operatingSystem = operatingSystem;
        this.systemController = systemController;
        this.loader = loader;
        this.smBackend = smBackend;
        this.isPrimaryScale = isPrimaryScale;
        this.enabled = enabled;
        this.totalLabelsPrinted = totalLabelsPrinted;
        this.profileRecentlyUpdated = profileRecentlyUpdated;
//        this.lastReportTimestampUtc = lastReportTimestampUtc;
        if (healthInfoLog == null) {
            this.healthInfoLog = "";
        } else if (healthInfoLog.length() > 12500) {
            this.healthInfoLog = healthInfoLog.substring(healthInfoLog.length()-12500);
        } else {
            this.healthInfoLog = healthInfoLog;
        }
        this.weigherPrecision = weigherPrecision;
        this.labelStockSize = labelStockSize;
    }

    public ScaleDevice() {}

    public ScaleDevice(String storeId) {
        this.storeId = storeId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceUuid) {
        this.deviceId = deviceUuid;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getScaleModel() {
        return scaleModel;
    }

    public void setScaleModel(String scaleModel) {
        this.scaleModel = scaleModel;
    }

    public String getStoreId() {return storeId;}

    public void setStoreId(String storeId) {this.storeId = storeId;}

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {this.deptId = deptId;}

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getSystemController() {
        return systemController;
    }

    public void setSystemController(String systemController) {
        this.systemController = systemController;
    }

    public String getLoader() {
        return loader;
    }

    public void setLoader(String loader) {
        this.loader = loader;
    }

    public String getSmBackend() {
        return smBackend;
    }

    public void setSmBackend(String smBackend) {
        this.smBackend = smBackend;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getLastReportTimestampUtc() {
        return lastReportTimestampUtc;
    }

    public void setLastReportTimestampUtc(Timestamp lastReportTimestampUtc) {
        this.lastReportTimestampUtc = lastReportTimestampUtc;
    }

    public String getLastTriggerType() {
        return lastTriggerType;
    }

    public void setLastTriggerType(String lastTriggerType) {
        this.lastTriggerType = lastTriggerType;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public Boolean getIsPrimaryScale() {
        return isPrimaryScale;
    }

    public void setIsPrimaryScale(Boolean isPrimaryScale) {
        this.isPrimaryScale = isPrimaryScale;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getPluCount() {
        return pluCount;
    }

    public void setPluCount(Integer pluCount) {
        this.pluCount = pluCount;
    }

    public Integer getTotalLabelsPrinted() {
        return totalLabelsPrinted;
    }

    public void setTotalLabelsPrinted(Integer totalLabelsPrinted) {
        this.totalLabelsPrinted = totalLabelsPrinted;
    }

    public String getUpgradeId() {
        return upgradeId;
    }

    public void setUpgradeId(String upgradeId) {
        this.upgradeId = upgradeId;
    }

    public String getWeigherPrecision() {
        return weigherPrecision;
    }

    public void setWeigherPrecision(String weigherPrecision) {
        this.weigherPrecision = weigherPrecision;
    }

    public String getHealthInfoLog(){ return healthInfoLog; }

    public Boolean getProfileRecentlyUpdated() {
        return profileRecentlyUpdated;
    }

    public void setProfileRecentlyUpdated(Boolean profileRecentlyUpdated) {
        this.profileRecentlyUpdated = profileRecentlyUpdated;
    }

    public void setHealthInfoLog(String healthInfoLog){
		if (healthInfoLog == null) {
			this.healthInfoLog = "";
		} else if(healthInfoLog.length() > 12500){
            this.healthInfoLog = healthInfoLog.substring(healthInfoLog.length() - 12500);
        } else {
            this.healthInfoLog = healthInfoLog;
        }
    }

    public String getLabelStockSize() {
        return labelStockSize;
    }

    public void setLabelStockSize(String labelStockSize) {
        this.labelStockSize = labelStockSize;
    }

    @Transient
    @JsonIgnore
    public static String getInvisibleScaleName(){
        return invisibleScaleName;
    }

    @Transient
    @JsonIgnore
    public boolean isInvisibleScale(){
        if(hostname == null){return false;}
        return hostname.equals(invisibleScaleName);
    }

    // Sets some default values for fields we can't extrapolate on import (e.g., CSV/XML import)
    // The fewer potential null pointers, the better, right?
    public void setImportDefaults(ScaleDevice device) {

        device.setBuildNumber("UNKNOWN");
        device.setCountryCode("");
        device.setLoader("UNKNOWN");
        device.setOperatingSystem("UNKNOWN");
        device.setPluCount(0);
        device.setSystemController("");
        device.setTotalLabelsPrinted(0);
    }
    public boolean equals(ScaleDevice scale) {
        try {
            return this.deviceId.equals(scale.getDeviceId());
        } catch (Exception e){
            return false;
        }
    }

    /**
     *
     * @param prop prop to see if a scaleDevice has this property
     * @return
     */
    public boolean has(String  prop) {
        List<String> scaleProps = Arrays.asList("deviceId","ipAddress","hostname","serialNumber","scaleModel","storeId",
                "deptId","application", "operatingSystem", "systemController", "loader", "smBackend", "lastReportTimestampUtc",
                "lastTriggerType", "profileId","isPrimaryScale",  "enabled", "pluCount", "totalLabelsPrinted", "healthInfoLog",
                "primaryScale");
        return scaleProps.contains(prop);
    }
    @Override
    public String toString() {
        return "ScaleDevice [deviceId=" + deviceId + ", ipAddress=" + ipAddress + ", hostname=" + hostname
                + ", serialNumber=" + serialNumber + ", scaleModel=" + scaleModel + ", storeId=" + storeId
                + ", deptId=" + deptId + ", countryCode=" + countryCode + ", buildNumber=" + buildNumber
                + ", application=" + application + ", operatingSystem=" + operatingSystem + ", systemController="
                + systemController + ", loader=" + loader + ", smBackend=" + smBackend + ", lastReportTimestampUtc="
                + lastReportTimestampUtc + ", lastTriggerType=" + lastTriggerType + ", profileId=" + profileId
                + ", isPrimaryScale=" + isPrimaryScale + ", enabled=" + enabled + ", pluCount=" + pluCount
                + ", totalLabelsPrinted=" + totalLabelsPrinted
                + "]";
    }
//    @Override
//    public String toString() {
//        return "ScaleDevice [deviceId=" + deviceId + ", ipAddress=" + ipAddress + ", hostname=" + hostname
//                + ", serialNumber=" + serialNumber + ", scaleModel=" + scaleModel + ", storeId=" + storeId
//                + ", deptId=" + deptId + ", countryCode=" + countryCode + ", buildNumber=" + buildNumber
//                + ", application=" + application + ", operatingSystem=" + operatingSystem + ", systemController="
//                + systemController + ", loader=" + loader + ", smBackend=" + smBackend + ", lastReportTimestampUtc="
//                + lastReportTimestampUtc + ", lastTriggerType=" + lastTriggerType + ", profileId=" + profileId
//                + ", isPrimaryScale=" + isPrimaryScale + ", enabled=" + enabled + ", pluCount=" + pluCount
//                + ", totalLabelsPrinted=" + totalLabelsPrinted + ", healthInfoLog=" +healthInfoLog
//                + "]";
//    }
}

