package com.hobart.hte.utils.device;

import com.hobart.hte.utils.store.StoreEvent;

import java.util.ArrayList;
import java.util.List;

public class DeviceEvent extends StoreEvent {

    private List<ScaleDevice> scales;

    public DeviceEvent() {
        super();
        this.setEventType("SYNC_DEVICE");
        this.scales = new ArrayList<ScaleDevice>();
    }

    public List<ScaleDevice> getScales() {
        return scales;
    }

    public void setScales(List<ScaleDevice> scales) {
        this.scales = scales;
    }

}
