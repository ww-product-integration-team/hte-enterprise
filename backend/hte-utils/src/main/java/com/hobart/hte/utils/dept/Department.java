package com.hobart.hte.utils.dept;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "depts")
public class Department {
	@Id @Column(columnDefinition = "VARCHAR(36)")
	private String deptId;
	private String deptName1;
	private String deptName2;
	private String defaultProfileId;
	@Column(columnDefinition = "VARCHAR(36)")
	private String requiresDomainId;

	public String getdeptId() {return deptId;}

	public void setdeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getDeptName1() {
		return deptName1;
	}

	public void setDeptName1(String deptName1) {
		this.deptName1 = deptName1;
	}

	public String getDeptName2() {
		return deptName2;
	}

	public void setDeptName2(String deptName2) {
		this.deptName2 = deptName2;
	}

	public String getDefaultProfileId() {
		return defaultProfileId;
	}

	public void setDefaultProfileId(String defaultProfileId) {
		this.defaultProfileId = defaultProfileId;
	}

	public String getRequiresDomainId() {return requiresDomainId;}

	public void setRequiresDomainId(String requiresDomainId) {this.requiresDomainId = requiresDomainId;}
}
