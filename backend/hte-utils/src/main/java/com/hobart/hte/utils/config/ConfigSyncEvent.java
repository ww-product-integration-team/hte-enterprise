package com.hobart.hte.utils.config;

import com.hobart.hte.utils.store.StoreEvent;

import java.util.ArrayList;
import java.util.List;

public class ConfigSyncEvent extends StoreEvent {
    private List<ConfigEntry> configs;

    public ConfigSyncEvent() {
        super();
        this.setEventType("SYNC_CONFIG");
        this.configs = new ArrayList<ConfigEntry>();
    }

    public List<ConfigEntry> getConfigs() {
        return configs;
    }

    public void setConfigs(List<ConfigEntry> configs) {
        this.configs = configs;
    }
}
