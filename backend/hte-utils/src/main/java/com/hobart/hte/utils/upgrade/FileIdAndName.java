package com.hobart.hte.utils.upgrade;

public class FileIdAndName {
    
    private String fileId;
    
    private String filename;

    public FileIdAndName(String fileId, String filename) {
        this.fileId = fileId;
        this.filename = filename;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
