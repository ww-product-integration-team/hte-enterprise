package com.hobart.hte.utils.model;

public enum EntityType {
    SCALE, DEPARTMENT, STORE, REGION, BANNER, ENTITY, PROFILE, BATCH, GROUP, WRAPPER
}
