package com.hobart.hte.utils.model;

public enum ScaleStatusType {
    UNKNOWN(0), ONLINE(1), WARNING(2), ERROR(3), DISABLED(4);

    private final int precedence;
    private ScaleStatusType(int precedence) {
        this.precedence = precedence;
    }

    public int getValue() {
        return precedence;
    }


}
