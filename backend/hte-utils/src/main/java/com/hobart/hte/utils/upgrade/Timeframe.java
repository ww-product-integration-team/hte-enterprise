package com.hobart.hte.utils.upgrade;

public class Timeframe {

    private String uploadPrimaryStart;
    private String uploadPrimaryEnd;
    private String uploadSecondaryStart;
    private String uploadSecondaryEnd;
    private String upgradeTimeStart;
    private String upgradeTimeEnd;

    public String getUploadPrimaryStart() {
        return uploadPrimaryStart;
    }

    public void setUploadPrimaryStart(String uploadPrimaryStart) {
        this.uploadPrimaryStart = uploadPrimaryStart;
    }

    public String getUploadPrimaryEnd() {
        return uploadPrimaryEnd;
    }

    public void setUploadPrimaryEnd(String uploadPrimaryEnd) {
        this.uploadPrimaryEnd = uploadPrimaryEnd;
    }

    public String getUploadSecondaryStart() {
        return uploadSecondaryStart;
    }

    public void setUploadSecondaryStart(String uploadSecondaryStart) {
        this.uploadSecondaryStart = uploadSecondaryStart;
    }

    public String getUploadSecondaryEnd() {
        return uploadSecondaryEnd;
    }

    public void setUploadSecondaryEnd(String uploadSecondaryEnd) {
        this.uploadSecondaryEnd = uploadSecondaryEnd;
    }

    public String getUpgradeTimeStart() {
        return upgradeTimeStart;
    }

    public void setUpgradeTimeStart(String upgradeTimeStart) {
        this.upgradeTimeStart = upgradeTimeStart;
    }

    public String getUpgradeTimeEnd() {
        return upgradeTimeEnd;
    }

    public void setUpgradeTimeEnd(String upgradeTimeEnd) {
        this.upgradeTimeEnd = upgradeTimeEnd;
    }
}
