package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Wired {
	private String macAddress;
	private String ip4Address;
	private String ip6Address;
	private String dhcpServer;
	private String subnetMask;
	private String defaultGateway;
	private String connectionSpeed;
	private boolean isConnected;

	@JsonProperty(value = "macAddress")
	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@JsonProperty(value = "ip4Address")
	public String getIp4Address() {
		return ip4Address;
	}

	public void setIp4Address(String ip4Address) {
		this.ip4Address = ip4Address;
	}

	@JsonProperty(value = "ip6Address")
	public String getIp6Address() {
		return ip6Address;
	}

	public void setIp6Address(String ip6Address) {
		this.ip6Address = ip6Address;
	}

	@JsonProperty(value = "connectionSpeed")
	public String getConnectionSpeed() {
		return connectionSpeed;
	}

	public void setConnectionSpeed(String connectionSpeed) {
		this.connectionSpeed = connectionSpeed;
	}

	public String getDhcpServer() {
		return dhcpServer;
	}

	public void setDhcpServer(String dhcpServer) {
		this.dhcpServer = dhcpServer;
	}

	public String getSubnetMask() {
		return subnetMask;
	}

	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}

	public String getDefaultGateway() {
		return defaultGateway;
	}

	public void setDefaultGateway(String defaultGateway) {
		this.defaultGateway = defaultGateway;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nWired [macAddress=");
		builder.append(macAddress);
		builder.append(", ip4Address=");
		builder.append(ip4Address);
		builder.append(", ip6Address=");
		builder.append(ip6Address);
		builder.append(", dhcpServer=");
		builder.append(dhcpServer);
		builder.append(", subnetMask=");
		builder.append(subnetMask);
		builder.append(", defaultGateway=");
		builder.append(defaultGateway);
		builder.append(", connectionSpeed=");
		builder.append(connectionSpeed);
		builder.append(", isConnected=");
		builder.append(isConnected);
		builder.append("]");
		return builder.toString();
	}
}
