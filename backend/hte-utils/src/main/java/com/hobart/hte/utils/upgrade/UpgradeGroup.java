package com.hobart.hte.utils.upgrade;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.util.List;

@Schema(description="")
@Entity
@Table(name="upgradeGroup")
public class UpgradeGroup {

    public enum Status {
        ENABLED, DISABLED, COMPLETED
    }

    @Id
    @Schema(description="")
    private String groupId;

    @Schema(description="")
    private String groupName;

    @Schema(description="")
    private String description;

    @Enumerated(EnumType.STRING)
    @Schema(description="")
    private Status status;

    private boolean batchGroup;

    @Transient
    private List<UpgradeDevice> scales;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status.name();
    }

    public void setStatus(String status) {
        this.status = Status.valueOf(status);
    }

    public List<UpgradeDevice> getScales() {
        return scales;
    }

    public void setScales(List<UpgradeDevice> scales) {
        this.scales = scales;
    }

    public boolean getBatchGroup() {
        return batchGroup;
    }

    public void setBatchGroup(boolean batchGroup) {
        this.batchGroup = batchGroup;
    }
}
