package com.hobart.hte.utils.autoAssign;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.persistence.*;
import java.io.Serializable;

@Schema(description="")
@Entity
@IdClass(AutoAssign.class)
@Table(name = "autoAssignmentRules")
public class AutoAssign implements Serializable {

    public AutoAssign() { super(); }
    @Id @Column(columnDefinition = "VARCHAR(15)") // An IP address will be this many characters at most
    private String ipAddressStart;
    @Id @Column(columnDefinition = "VARCHAR(15)")
    private String ipAddressEnd;
    @Id
    private String banner;
    @Id
    private String region;
    @Id
    private String store;
    @Id
    private String dept;
    @Id
    private Boolean enabled;

    // Java-only instance fields for when users specify how to handle overlapping rules via the frontend.
    // Don't touch!
    @Transient
    private Boolean disableFlag;
    @Transient
    private Boolean deleteFlag;

    public AutoAssign(String ipAddressStart, String ipAddressEnd) {
        this.ipAddressStart = ipAddressStart;
        this.ipAddressEnd = ipAddressEnd;
    }
    public AutoAssign(String ipAddressStart, String ipAddressEnd, String banner, String region, String store, String dept) {
        this.ipAddressStart = ipAddressStart;
        this.ipAddressEnd = ipAddressEnd;
        this.banner = banner;
        this.region = region;
        this.store = store;
        this.dept = dept;
        this.enabled = false;
    }

    public String getIpAddressStart() {
        return ipAddressStart;
    }

    public void setIpAddressStart(String ipStart) {
        ipAddressStart = ipStart;
    }

    public String getIpAddressEnd() {
        return ipAddressEnd;
    }

    public void setIpAddressEnd(String ipEnd) {
        ipAddressEnd = ipEnd;
    }

    public String getBanner() { return banner; }

    public void setBanner(String b) { banner = b; }

    public String getRegion() { return region; }

    public void setRegion(String r) { region = r; }

    public String getStore() { return store; }

    public void setStore(String s) {
        store = s;
    }

    public String getDept() { return dept; }

    public void setDept(String d) {
        dept = d;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getDisableFlag() { return disableFlag; }

    public void setDisableFlag(Boolean flag) { this.disableFlag = flag; }

    public Boolean getDeleteFlag() { return deleteFlag; }

    public void setDeleteFlag(Boolean flag) { this.deleteFlag = flag; }

    public boolean equals(AutoAssign otherRule) {
        if (otherRule != null &&
                ipAddressStart.equals(otherRule.getIpAddressStart()) &&
                ipAddressEnd.equals(otherRule.getIpAddressEnd()) &&
                banner.equals(otherRule.getBanner()) &&
                region.equals(otherRule.getRegion()) &&
                store.equals(otherRule.getStore()) &&
                dept.equals(otherRule.getDept())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "IP Range: " + ipAddressStart + "-" + ipAddressEnd + "\n" +
                "Banner: " + banner + "\n" +
                "Region: " + region + "\n" +
                "Store: " + store + "\n" +
                "Department: " + dept + "\n" +
                "Status: " + enabled;
    }
}
