package com.hobart.hte.utils.device;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ScaleInfo {
    private long serialNumber;
    private String scaleModel;
    private String appHookVersion;
    private String storeId;
    private String deptId;
    private String countryCode;
    private String profileId;
    private String buildNumber;
    private String application;
    private String operatingSystem;
    private String systemController;
    private String loader;
    private String smBackendVersion;
    private List<String> features;
    private List<String> licenses;
    private Boolean isPrimary;
    private String scaleSynchType;
    private String healthInfoLog;

    public ScaleInfo() {
    }

    public ScaleInfo(ScaleDevice sd) {
        this.serialNumber = sd.getSerialNumber();
        this.storeId = sd.getStoreId();
        this.deptId = sd.getDeptId();
        this.countryCode = sd.getCountryCode();
        this.scaleModel = sd.getScaleModel();
        this.profileId = sd.getProfileId();
        this.buildNumber = sd.getBuildNumber();
        this.application = sd.getApplication();
        this.operatingSystem = sd.getOperatingSystem();
        this.systemController = sd.getSystemController();
        this.loader = sd.getLoader();
        this.smBackendVersion = sd.getSmBackend();
		if (sd.getHealthInfoLog() == null) {
			this.healthInfoLog = "";
			sd.setHealthInfoLog("");
		} else if(sd.getHealthInfoLog().length() > 12500) {
            this.healthInfoLog = sd.getHealthInfoLog().substring(sd.getHealthInfoLog().length()-12500);
        } else {
            this.healthInfoLog = sd.getHealthInfoLog();
        }
    }

    @JsonProperty(value = "serialNumber")
    public long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty(value = "scaleModel")
    public String getScaleModel() {
        return scaleModel;
    }

    public void setScaleModel(String scaleModel) {
        this.scaleModel = scaleModel;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getDeptId() {return deptId;}

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    @JsonProperty(value = "buildNumber")
    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    @JsonProperty(value = "application")
    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    @JsonProperty(value = "operatingSystem")
    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    @JsonProperty(value = "systemController")
    public String getSystemController() {
        return systemController;
    }

    public void setSystemController(String systemController) {
        this.systemController = systemController;
    }

    @JsonProperty(value = "loader")
    public String getLoader() {
        return loader;
    }

    public void setLoader(String loader) {
        this.loader = loader;
    }

    @JsonProperty(value = "smBackend")
    public String getSmBackendVersion() {
        return smBackendVersion;
    }

    public void setSmBackendVersion(String smBackendVersion) {
        this.smBackendVersion = smBackendVersion;
    }

    @JsonProperty(value = "features")
    public List<String> getFeatures() {
        return features;
    }

    public void setFeatures(List<String> features) {
        this.features = features;
    }

    public String gethealthInfoLog(){
        if (healthInfoLog != null) {
            return healthInfoLog;
        }
        else {
            return "";
        }
    }

    public void sethealthInfoLog(String healthInfoLog){
		if (healthInfoLog == null) {
			this.healthInfoLog = "";
		} else if(healthInfoLog.length() > 12500){
            this.healthInfoLog = healthInfoLog.substring(healthInfoLog.length() - 12500);
        } else {
            this.healthInfoLog = healthInfoLog;
        }
    }

    @JsonProperty(value = "licenses")
    public List<String> getLicenses() {
        if(licenses == null){
            return new ArrayList<>();
        }
        return licenses;
    }

    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    public String getAppHookVersion() {
        return appHookVersion;
    }

    public void setAppHookVersion(String appHookVersion) {
        this.appHookVersion = appHookVersion;
    }

    public Boolean getIsPrimary() { return isPrimary; }

    public void setIsPrimary(String isPrimary) { // Take argument as a string, but translate it to a bool in the object
        // If the scale is standalone or primary, respectively
        if (isPrimary == null || isPrimary.equals("-1") || isPrimary.equals("0") || isPrimary.equals("1")) {
            this.isPrimary = true;
        } else if (isPrimary.equals("2")) { // If the scale is secondary
            this.isPrimary = false;
        } else {this.isPrimary = false;} // I once saw an FS report some bogus value, and now I'm paranoid.
    }

    public String getScaleSynchType() { return scaleSynchType; }

    public void setScaleSynchType(String scaleSynchType) { this.scaleSynchType = scaleSynchType; }

    @Override
    public String toString() {
        String hasHealthLog = "false";
        if (!healthInfoLog.isEmpty()) {
            hasHealthLog = "true";
        }
        return "ScaleInfo [serialNumber=" + serialNumber + ", scaleModel=" + scaleModel + ", storeId=" + storeId
                + ", deptId=" + deptId + ", countryCode=" + countryCode + ", buildNumber=" + buildNumber
                + ", application=" + application + ", operatingSystem=" + operatingSystem + ", systemController="
                + systemController + ", loader=" + loader + ", smBackendVersion=" + smBackendVersion + ", features="
                + features + "isPrimary=" + isPrimary + "hasHealthLog=" + hasHealthLog + "]";
    }
//    @Override
//    public String toString() {
//        String hasHealthLog = "false";
//        if (healthInfoLog.length() > 0) {
//            hasHealthLog = "true";
//        }
//        return "ScaleInfo [serialNumber=" + serialNumber + ", scaleModel=" + scaleModel + ", storeId=" + storeId
//                + ", deptId=" + deptId + ", countryCode=" + countryCode + ", buildNumber=" + buildNumber
//                + ", application=" + application + ", operatingSystem=" + operatingSystem + ", systemController="
//                + systemController + ", loader=" + loader + ", smBackendVersion=" + smBackendVersion + ", features="
//                + features + ", licenses=" + licenses + "isPrimary=" + isPrimary
//                + "hasHealthLog=" + hasHealthLog
//                + "]";
//    }
}
