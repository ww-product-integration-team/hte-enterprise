package com.hobart.hte.utils.upgrade;

public class DeptAndBatchIds {

    private String deptId;

    private String batchId;

    public DeptAndBatchIds(String deptId, String batchId) {
        this.deptId = deptId;
        this.batchId = batchId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
}
