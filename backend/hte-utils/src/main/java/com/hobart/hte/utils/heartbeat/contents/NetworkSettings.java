package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NetworkSettings {
	private String hostname;
	private String connectionType;
	private Wired wired;
	private Wireless wireless;
	private Dns dns;
	private Snmp snmp;

	@JsonProperty(value = "hostname")
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	@JsonProperty(value = "connectionType")
	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	@JsonProperty(value = "wired")
	public Wired getWired() {
		return wired;
	}

	public void setWired(Wired wired) {
		this.wired = wired;
	}

	@JsonProperty(value = "wireless")
	public Wireless getWireless() {
		return wireless;
	}

	public void setWireless(Wireless wireless) {
		this.wireless = wireless;
	}

	@JsonProperty(value = "dns")
	public Dns getDns() {
		return dns;
	}

	public void setDns(Dns dns) {
		this.dns = dns;
	}

	@JsonProperty(value = "snmp")
	public Snmp getSnmp() {
		return snmp;
	}

	public void setSnmp(Snmp snmp) {
		this.snmp = snmp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nNetworkSettings [hostname=");
		builder.append(hostname);
		builder.append(", connectionType=");
		builder.append(connectionType);
		builder.append(", wired=");
		builder.append(wired);
		builder.append(", wireless=");
		builder.append(wireless);
		builder.append(", dns=");
		builder.append(dns);
		builder.append(", snmp=");
		builder.append(snmp);
		builder.append("]");
		return builder.toString();
	}
}
