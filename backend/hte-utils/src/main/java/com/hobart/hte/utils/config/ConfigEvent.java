package com.hobart.hte.utils.config;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "configEvent")
public class ConfigEvent {
    @Id
    private String idfileEvent;
    /*-
     *  0 - undefined
     *  1 - initial event
     *  2 - scheduled
     *  3 - per scale
     *  4 - per department
     *  5 - per store
     */
    private int eventType;
    private String shortDesc;
    private Timestamp startOn;
    private Timestamp finishOn;
    private String storeId;
    private String deptId;
    private String uuidFile;
    private String deviceUuid;
    private Timestamp creationTimestamp;

    public String getIdfileEvent() {
        return idfileEvent;
    }

    public void setIdfileEvent(String idfileEvent) {
        this.idfileEvent = idfileEvent;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Timestamp getStartOn() {
        return startOn;
    }

    public void setStartOn(Timestamp startOn) {
        this.startOn = startOn;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Timestamp getFinishOn() {
        return finishOn;
    }

    public void setFinishOn(Timestamp finishOn) {
        this.finishOn = finishOn;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getUuidFile() {
        return uuidFile;
    }

    public void setUuidFile(String uuidFile) {
        this.uuidFile = uuidFile;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public Timestamp getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Timestamp creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    @Override
    public String toString() {
        return "ConfigEvent [idfileEvent=" + idfileEvent + ", eventType=" + eventType + ", shortDesc=" + shortDesc
                + ", startOn=" + startOn + ", finishOn=" + finishOn + ", storeNumber=" + storeId + ", deptNumber="
                + deptId + ", uuidFile=" + uuidFile + ", deviceUuid=" + deviceUuid + "]";
    }

}

