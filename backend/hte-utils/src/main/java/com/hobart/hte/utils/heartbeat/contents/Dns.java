package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dns {
	private boolean autoDns;
	private String dnsServer1;
	private String dnsServer2;
	private String dnsServer3;
	private String dnsSuffix1;
	private String dnsSuffix2;
	private String dnsSuffix3;
	private String dnsSuffix4;
	private String dnsSuffix5;
	private String dnsSuffix6;

	@JsonProperty(value = "autoDns")
	public boolean isAutoDns() {
		return autoDns;
	}

	public void setAutoDns(boolean autoDns) {
		this.autoDns = autoDns;
	}

	@JsonProperty(value = "dnsServer1")
	public String getDnsServer1() {
		return dnsServer1;
	}

	public void setDnsServer1(String dnsServer1) {
		this.dnsServer1 = dnsServer1;
	}

	@JsonProperty(value = "dnsServer2")
	public String getDnsServer2() {
		return dnsServer2;
	}

	public void setDnsServer2(String dnsServer2) {
		this.dnsServer2 = dnsServer2;
	}

	@JsonProperty(value = "dnsServer3")
	public String getDnsServer3() {
		return dnsServer3;
	}

	public void setDnsServer3(String dnsServer3) {
		this.dnsServer3 = dnsServer3;
	}

	@JsonProperty(value = "dnsSuffix1")
	public String getDnsSuffix1() {
		return dnsSuffix1;
	}

	public void setDnsSuffix1(String dnsSuffix1) {
		this.dnsSuffix1 = dnsSuffix1;
	}

	@JsonProperty(value = "dnsSuffix2")
	public String getDnsSuffix2() {
		return dnsSuffix2;
	}
	
	public void setDnsSuffix2(String dnsSuffix2) {
		this.dnsSuffix2 = dnsSuffix2;
	}

	@JsonProperty(value = "dnsSuffix3")
	public String getDnsSuffix3() {
		return dnsSuffix3;
	}

	public void setDnsSuffix3(String dnsSuffix3) {
		this.dnsSuffix3 = dnsSuffix3;
	}

	@JsonProperty(value = "dnsSuffix4")
	public String getDnsSuffix4() {
		return dnsSuffix4;
	}

	public void setDnsSuffix4(String dnsSuffix4) {
		this.dnsSuffix4 = dnsSuffix4;
	}

	@JsonProperty(value = "dnsSuffix5")
	public String getDnsSuffix5() {
		return dnsSuffix5;
	}

	public void setDnsSuffix5(String dnsSuffix5) {
		this.dnsSuffix5 = dnsSuffix5;
	}

	@JsonProperty(value = "dnsSuffix6")
	public String getDnsSuffix6() {
		return dnsSuffix6;
	}

	public void setDnsSuffix6(String dnsSuffix6) {
		this.dnsSuffix6 = dnsSuffix6;
	}
}
