package com.hobart.hte.utils.store;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.hobart.hte.utils.config.ConfigSyncEvent;
import com.hobart.hte.utils.dept.DepartmentEvent;
import com.hobart.hte.utils.device.DeviceEvent;
import com.hobart.hte.utils.event.LogEvent;
import com.hobart.hte.utils.profile.ProfileEvent;
import com.hobart.hte.utils.service.ServiceEvent;

import java.util.UUID;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "eventType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = LogEvent.class, name = "SYNC_LOG"),
        @JsonSubTypes.Type(value = DepartmentEvent.class, name = "SYNC_DEPT"),
        @JsonSubTypes.Type(value = ProfileEvent.class, name = "SYNC_PROFILE"),
        @JsonSubTypes.Type(value = DeviceEvent.class, name = "SYNC_DEVICE"),
        @JsonSubTypes.Type(value = ConfigSyncEvent.class, name = "SYNC_CONFIG"),
        @JsonSubTypes.Type(value = ServiceEvent.class, name = "SYNC_SERVICE"),
})
public class StoreEvent {
    /**
     * whether is to sync a file, a list of departments, list of profiles, devices,
     * etc.
     */
    private String eventType;
    private String uuid;

    public StoreEvent() {
        uuid = UUID.randomUUID().toString();
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
