package com.hobart.hte.utils.model;

public enum RequestOperationResult {
    ERROR, SUCCESS
}