package com.hobart.hte.utils.event;

import com.hobart.hte.utils.event.DeviceStatusEntry;
import com.hobart.hte.utils.store.StoreEvent;

import java.util.ArrayList;
import java.util.List;

public class LogEvent extends StoreEvent {

    public LogEvent() {
        super();
        this.setEventType("SYNC_LOG");
        logs = new ArrayList<DeviceStatusEntry>();
    }

    List<DeviceStatusEntry> logs;

    public List<DeviceStatusEntry> getLogs() {
        return logs;
    }

    public void setLogs(List<DeviceStatusEntry> logs) {
        this.logs = logs;
    }

}

