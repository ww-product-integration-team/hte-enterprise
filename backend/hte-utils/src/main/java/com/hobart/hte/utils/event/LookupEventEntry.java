package com.hobart.hte.utils.event;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "lookupEvent")
public class LookupEventEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int eventId;
	private Timestamp timestamp;
	private String scaleIP;
	private int plu;

	public LookupEventEntry() {
		super();
	}

	public LookupEventEntry(String scaleIP, int plu) {
		super();
		this.timestamp = new Timestamp(System.currentTimeMillis());
		this.scaleIP = scaleIP;
		this.plu = plu;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getScaleIP() {
		return scaleIP;
	}

	public void setScaleIP(String scaleIP) {
		this.scaleIP = scaleIP;
	}

	public int getPlu() {
		return plu;
	}

	public void setPlu(int plu) {
		this.plu = plu;
	}
}
