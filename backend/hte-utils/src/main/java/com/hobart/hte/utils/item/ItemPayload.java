package com.hobart.hte.utils.item;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemPayload {

	private int PrID;
	private int PrNum;
	// 0 - RW, 1 - FW, 2 - BC, 3 - FO
	private int PrType;
	private int DeptNum;
	private String PrDesc;
	// no decimal numbers to be more universal
	private Integer PrPrice;
	private Integer ClNum;
	private String ExpText;
	private String SpcMessage;
	private Integer LbNum1;
	private Integer LbNum2;
	private Integer PrExcPr;
	private Integer PrDscMe;
	private Integer PrDscPr1;
	private Integer PrTare;
	private Integer PrTareB;
	private Integer PrNetWt;
	private Integer PrByCnt;
	private Integer PrShlfLfD;
	private Integer PrProdLfD;
	private Integer PrShlfLfH;
	private Integer PrProdLfH;
	private Integer PrShlfLfM;
	private Integer PrProdLfM;
	private String PrName1;
	private String PrName2;
	private Integer PrBarPref;
	private Integer PrBarProN;
	private Integer PrBarVenN;
	private String PrEANCd;
	private String PrRawBarC;
	private String PrGrNm1;
	private String PrImage;
	// Nutrifact id 
	private Integer nFNum;
	// current COOL text field
	@JsonProperty("lXNum")
	private Integer LXNum;
	// default COOL text field
	@JsonProperty("dLXNum")
	private Integer DLXNum;
	// COOL list key field
	@JsonProperty("lLID")
	private Integer LLID;
	// Force COOL review field
	private Boolean PrVCOLTxt;
	// COOL required field
	private Boolean PrCOLReq;

	public ItemPayload() {
	}

	public ItemPayload(HobItem item) {
		this.PrID = item.getPrID();
		this.PrNum = item.getPrNum();
		this.PrType = item.getPrType();
		this.PrDesc = item.getPrDesc();
		this.DeptNum = item.getDeptNum();
		this.PrPrice = item.getPrPrice();
		this.LbNum1 = item.getLbNum1();
		this.LbNum2 = item.getLbNum2();
		this.ClNum = item.getClNum();
		this.PrTare = item.getPrTare();
		this.PrTareB = item.getPrTareB();
		this.PrByCnt = item.getPrByCnt();
		this.PrExcPr = item.getPrExcPr();
		this.PrDscMe = item.getPrDscMe();
		this.PrDscPr1 = item.getPrDscPr1();
		this.PrShlfLfD = item.getPrShlfLfD();
		this.PrProdLfD = item.getPrProdLfD();
		this.PrShlfLfH = item.getPrShlfLfH();
		this.PrProdLfH = item.getPrProdLfH();
		this.PrShlfLfM = item.getPrShlfLfM();
		this.PrProdLfM = item.getPrProdLfM();
		this.PrName1 = item.getPrName1();
		this.PrName2 = item.getPrName2();
		this.PrBarPref = item.getPrBarPref();
		this.PrBarProN = item.getPrBarProN();
		this.PrBarVenN = item.getPrBarVenN();
		this.PrEANCd = item.getPrEANCd();
		this.PrRawBarC = item.getPrRawBarC();
		this.PrGrNm1 = item.getPrGrNm1();
		this.PrImage = item.getPrImage();
		this.nFNum = item.getNFNum();
		this.LXNum = item.getLXNum();
		this.DLXNum = item.getDLXNum();
		this.LLID = item.getLLID();
		this.PrVCOLTxt = item.isPrVCOLTxt();
		this.PrCOLReq = item.isPrCOLReq(); 
		// text fields will be added separately
	}

	public int getPrID() {
		return PrID;
	}

	public void setPrID(int prID) {
		PrID = prID;
	}

	public int getPrNum() {
		return PrNum;
	}

	public void setPrNum(int prNum) {
		PrNum = prNum;
	}

	public int getPrType() {
		return PrType;
	}

	public void setPrType(int prType) {
		PrType = prType;
	}

	public int getDeptNum() {
		return DeptNum;
	}

	public void setDeptNum(int deptNum) {
		DeptNum = deptNum;
	}

	public String getPrDesc() {
		return PrDesc;
	}

	public void setPrDesc(String prDesc) {
		PrDesc = prDesc;
	}

	public Integer getPrPrice() {
		return PrPrice;
	}

	public void setPrPrice(Integer prPrice) {
		PrPrice = prPrice;
	}

	public Integer getClNum() {
		return ClNum;
	}

	public void setClNum(Integer clNum) {
		ClNum = clNum;
	}

	public String getExpText() {
		return ExpText;
	}

	public void setExpText(String expText) {
		ExpText = expText;
	}

	public String getSpcMessage() {
		return SpcMessage;
	}

	public void setSpcMessage(String spcMessage) {
		SpcMessage = spcMessage;
	}

	public Integer getLbNum1() {
		return LbNum1;
	}

	public void setLbNum1(Integer lbNum1) {
		LbNum1 = lbNum1;
	}

	public Integer getLbNum2() {
		return LbNum2;
	}

	public void setLbNum2(Integer lbNum2) {
		LbNum2 = lbNum2;
	}

	public Integer getPrExcPr() {
		return PrExcPr;
	}

	public void setPrExcPr(Integer prExcPr) {
		PrExcPr = prExcPr;
	}

	public Integer getPrDscMe() {
		return PrDscMe;
	}

	public void setPrDscMe(Integer prDscMe) {
		PrDscMe = prDscMe;
	}

	public Integer getPrDscPr1() {
		return PrDscPr1;
	}

	public void setPrDscPr1(Integer prDscPr1) {
		PrDscPr1 = prDscPr1;
	}

	public Integer getPrTare() {
		return PrTare;
	}

	public void setPrTare(Integer prTare) {
		PrTare = prTare;
	}

	public Integer getPrTareB() {
		return PrTareB;
	}

	public void setPrTareB(Integer prTareB) {
		PrTareB = prTareB;
	}

	public Integer getPrNetWt() {
		return PrNetWt;
	}

	public void setPrNetWt(Integer prNetWt) {
		PrNetWt = prNetWt;
	}

	public Integer getPrByCnt() {
		return PrByCnt;
	}

	public void setPrByCnt(Integer prByCnt) {
		PrByCnt = prByCnt;
	}

	public Integer getPrShlfLfD() {
		return PrShlfLfD;
	}

	public void setPrShlfLfD(Integer prShlfLfD) {
		PrShlfLfD = prShlfLfD;
	}

	public Integer getPrProdLfD() {
		return PrProdLfD;
	}

	public void setPrProdLfD(Integer prProdLfD) {
		PrProdLfD = prProdLfD;
	}

	public Integer getPrShlfLfH() {
		return PrShlfLfH;
	}

	public void setPrShlfLfH(Integer prShlfLfH) {
		PrShlfLfH = prShlfLfH;
	}

	public Integer getPrProdLfH() {
		return PrProdLfH;
	}

	public void setPrProdLfH(Integer prProdLfH) {
		PrProdLfH = prProdLfH;
	}

	public Integer getPrShlfLfM() {
		return PrShlfLfM;
	}

	public void setPrShlfLfM(Integer prShlfLfM) {
		PrShlfLfM = prShlfLfM;
	}

	public Integer getPrProdLfM() {
		return PrProdLfM;
	}

	public void setPrProdLfM(Integer prProdLfM) {
		PrProdLfM = prProdLfM;
	}

	public String getPrName1() {
		return PrName1;
	}

	public void setPrName1(String prName1) {
		PrName1 = prName1;
	}

	public String getPrName2() {
		return PrName2;
	}

	public void setPrName2(String prName2) {
		PrName2 = prName2;
	}

	public Integer getPrBarPref() {
		return PrBarPref;
	}

	public void setPrBarPref(Integer prBarPref) {
		PrBarPref = prBarPref;
	}

	public Integer getPrBarProN() {
		return PrBarProN;
	}

	public void setPrBarProN(Integer prBarProN) {
		PrBarProN = prBarProN;
	}

	public Integer getPrBarVenN() {
		return PrBarVenN;
	}

	public void setPrBarVenN(Integer prBarVenN) {
		PrBarVenN = prBarVenN;
	}

	public String getPrEANCd() {
		return PrEANCd;
	}

	public void setPrEANCd(String prEANCd) {
		PrEANCd = prEANCd;
	}

	public String getPrRawBarC() {
		return PrRawBarC;
	}

	public void setPrRawBarC(String prRawBarC) {
		PrRawBarC = prRawBarC;
	}

	public String getPrGrNm1() {
		return PrGrNm1;
	}

	public void setPrGrNm1(String prGrNm1) {
		PrGrNm1 = prGrNm1;
	}

	public String getPrImage() {
		return PrImage;
	}

	public void setPrImage(String prImage) {
		PrImage = prImage;
	}

	public Integer getnFNum() {
		return nFNum;
	}

	public void setnFNum(Integer nFNum) {
		this.nFNum = nFNum;
	}

	public Integer getLXNum() {
		return LXNum;
	}

	public void setLXNum(Integer lXNum) {
		LXNum = lXNum;
	}

	public Integer getDLXNum() {
		return DLXNum;
	}

	public void setDLXNum(Integer dLXNum) {
		DLXNum = dLXNum;
	}

	public Integer getLLID() {
		return LLID;
	}

	public void setLLID(Integer lLID) {
		LLID = lLID;
	}

	public Boolean getPrVCOLTxt() {
		return PrVCOLTxt;
	}

	public void setPrVCOLTxt(Boolean prVCOLTxt) {
		PrVCOLTxt = prVCOLTxt;
	}

	public Boolean getPrCOLReq() {
		return PrCOLReq;
	}

	public void setPrCOLReq(Boolean prCOLReq) {
		PrCOLReq = prCOLReq;
	}
}
