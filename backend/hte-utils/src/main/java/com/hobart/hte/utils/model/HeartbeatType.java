package com.hobart.hte.utils.model;

public enum HeartbeatType {
    INITIAL, INTERVAL, MANUAL, SCALE_POWER_UP, SCALE_POWER_DOWN, WAS_OFFLINE;

}
