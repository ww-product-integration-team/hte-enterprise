package com.hobart.hte.utils.event;

import org.springframework.data.util.Pair;

public class EventConfig {

    //Events
    //==================================================================================================================

    //Logging
    public static Pair<Integer, Pair<String, EventImportance>> LoggingEvent = Pair.of(1, Pair.of("Logging Event", EventImportance.NOTIFY));

    //Download
    public static Pair<Integer, Pair<String, EventImportance>> ScaleDownload = Pair.of(1000, Pair.of("Scale File Download", EventImportance.NOTIFY));
    public static Pair<Integer, Pair<String, EventImportance>> DownloadNoIpError = Pair.of(1001, Pair.of("Scale Download Error: No IP Address", EventImportance.ERROR));
    public static Pair<Integer, Pair<String, EventImportance>> DownloadDisabledStoreError = Pair.of(1002, Pair.of("Scale Download Error: Disabled Store", EventImportance.WARNING));
    public static Pair<Integer, Pair<String, EventImportance>> DownloadNoStoreError = Pair.of(1003, Pair.of("Scale Download Error: Unknown Store", EventImportance.ERROR));
    public static Pair<Integer, Pair<String, EventImportance>> DownloadNoProfileError = Pair.of(1004, Pair.of("Scale Download Error: Unknown Profile", EventImportance.ERROR));
    public static Pair<Integer, Pair<String, EventImportance>> StoreDownload = Pair.of(1005, Pair.of("Store File Download", EventImportance.NOTIFY));

    //Heartbeat
    public static Pair<Integer, Pair<String, EventImportance>> NewDeviceLogged = Pair.of(1100, Pair.of("New Device Reported", EventImportance.NOTIFY));
    public static Pair<Integer, Pair<String, EventImportance>> DeviceHeartbeat = Pair.of(1101, Pair.of("Device Heartbeat", EventImportance.NOTIFY));

    //Store Heartbeat
    public static Pair<Integer, Pair<String, EventImportance>> StoreHeartbeat = Pair.of(1201, Pair.of("Store Heartbeat", EventImportance.NOTIFY));
    public static Pair<Integer, Pair<String, EventImportance>> StoreNotRegistered = Pair.of(1202, Pair.of("Store Heartbeat Failed, Not Registered", EventImportance.ERROR));
    public static Pair<Integer, Pair<String, EventImportance>> NewStoreReported = Pair.of(1203, Pair.of("New Store Reported", EventImportance.NOTIFY));
    public static Pair<Integer, Pair<String, EventImportance>> WrongStoreAssignment = Pair.of(1204, Pair.of("Incorrect Store Assignment", EventImportance.WARNING));


    //Default Rules
    //==================================================================================================================
    public static EventRule Rule_LoggingEvent = new EventRule(LoggingEvent.getFirst(), true, false, true);

    public static EventRule Rule_ScaleDownload = new EventRule(ScaleDownload.getFirst(),true,false,false);
    public static EventRule Rule_DownloadNoIpError = new EventRule(DownloadNoIpError.getFirst(),true,false,false);
    public static EventRule Rule_DownloadDisabledStoreError = new EventRule(DownloadDisabledStoreError.getFirst(),true,false,false);
    public static EventRule Rule_DownloadNoStoreError = new EventRule(DownloadNoStoreError.getFirst(),true,false,false);
    public static EventRule Rule_DownloadNoProfileError = new EventRule(DownloadNoProfileError.getFirst(),true,false,false);

    public static EventRule Rule_StoreDownload = new EventRule(StoreDownload.getFirst(),true,true,false);

    //Heartbeat
    public static EventRule Rule_NewDeviceLogged = new EventRule(NewDeviceLogged.getFirst(),true,false,false);
    public static EventRule Rule_DeviceHeartbeat = new EventRule(DeviceHeartbeat.getFirst(),false,false,false);

    //Store Heartbeat
    public static EventRule Rule_StoreHeartbeat = new EventRule(StoreHeartbeat.getFirst(),false,false,false);
    public static EventRule Rule_StoreNotRegistered = new EventRule(StoreNotRegistered.getFirst(),true,false,false);
    public static EventRule Rule_NewStoreReported = new EventRule(NewStoreReported.getFirst(),true,false,false);
    public static EventRule Rule_WrongStoreAssignment = new EventRule(WrongStoreAssignment.getFirst(),true,false,false);


}
