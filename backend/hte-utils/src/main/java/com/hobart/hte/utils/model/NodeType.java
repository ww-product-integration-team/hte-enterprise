package com.hobart.hte.utils.model;

public enum NodeType {

    /*
    Type of node used in Asset List.
     */
    BANNER, REGION, STORE, DEPT, SCALE
}
