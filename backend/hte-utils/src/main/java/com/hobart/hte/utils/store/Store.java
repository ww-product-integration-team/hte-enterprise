package com.hobart.hte.utils.store;

import com.hobart.hte.utils.heartbeat.HealthCheck;
import com.hobart.hte.utils.heartbeat.StoreHeartbeatRequest;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceType;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "store")
public class Store {
	@Id
	private String storeId;
	private String storeName;
	private String address;
	private String regionId;
	private String customerStoreNumber;
	private Boolean enabled;
	private String ipAddress;
	private String hostname;
	private String triggerType;
	private String url;
	@OneToMany(fetch = FetchType.EAGER)
	private List<Service> services;
	private Timestamp lastRebootTimestampUtc;
	private Timestamp lastCommunicationTimestampUtc;
	private Timestamp lastItemChangeReceivedTimestampUtc;
	private Timestamp lastFetchPluOffsetTimestampUtc;
	//null value indicates use of default app setting, otherwise acts independently
	private String sendStoreInfo; //null | "true" | "false" | "Custom Store Name \n Entered Here"

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getCustomerStoreNumber() {
		return customerStoreNumber;
	}

	public void setCustomerStoreNumber(String customerStoreNumber) {
		this.customerStoreNumber = customerStoreNumber;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getTriggerType() {
		return triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Service> getServices() {
		return services;
	}

	public Service getService(ServiceType serviceType){
		for(Service s : services){
			if(s.getServiceType() == serviceType){
				return s;
			}
		}
		return null;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public Timestamp getLastRebootTimestampUtc() {
		return lastRebootTimestampUtc;
	}

	public void setLastRebootTimestampUtc(Timestamp lastRebootTimestampUtc) {
		this.lastRebootTimestampUtc = lastRebootTimestampUtc;
	}

	public Timestamp getLastCommunicationTimestampUtc() {
		return lastCommunicationTimestampUtc;
	}

	public void setLastCommunicationTimestampUtc(Timestamp lastCommunicationTimestampUtc) {
		this.lastCommunicationTimestampUtc = lastCommunicationTimestampUtc;
	}

	public Timestamp getLastItemChangeReceivedTimestampUtc() {
		return lastItemChangeReceivedTimestampUtc;
	}

	public void setLastItemChangeReceivedTimestampUtc(Timestamp lastItemChangeReceivedTimestampUtc) {
		this.lastItemChangeReceivedTimestampUtc = lastItemChangeReceivedTimestampUtc;
	}

	public Timestamp getLastFetchPluOffsetTimestampUtc() {
		return lastFetchPluOffsetTimestampUtc;
	}

	public void setLastFetchPluOffsetTimestampUtc(Timestamp lastFetchPluOffsetTimestampUtc) {
		this.lastFetchPluOffsetTimestampUtc = lastFetchPluOffsetTimestampUtc;
	}

	public String getSendStoreInfo() {
		return sendStoreInfo;
	}

	public void setSendStoreInfo(String sendStoreInfo) {
		this.sendStoreInfo = sendStoreInfo;
	}
	public void disableSendStoreInfo(){this.sendStoreInfo = "false";}

	public void updateStatus(StoreHeartbeatRequest shbr) {
		this.setLastCommunicationTimestampUtc(shbr.getHealthCheck().getLastCommunicationTimestampUtc());
		this.setLastFetchPluOffsetTimestampUtc(shbr.getHealthCheck().getLastFetchPluOffsetTimestampUtc());
		this.setLastItemChangeReceivedTimestampUtc(shbr.getHealthCheck().getLastItemChangeReceivedTimestampUtc());
		this.setLastRebootTimestampUtc(shbr.getHealthCheck().getLastRebootTimestampUtc());
		this.setHostname(shbr.getHostname());
		this.setIpAddress(shbr.getIpAddress()); // TBD
		this.setTriggerType(shbr.getTriggerType());
		this.setUrl(shbr.getUrl());
		this.setServices(shbr.getServices());
	}

	public StoreHeartbeatRequest toStoreHeartbeat(){
		StoreHeartbeatRequest request = new StoreHeartbeatRequest();

		HealthCheck healthCheck = new HealthCheck();
		healthCheck.setLastCommunicationTimestampUtc(lastCommunicationTimestampUtc);
		healthCheck.setLastFetchPluOffsetTimestampUtc(lastFetchPluOffsetTimestampUtc);
		healthCheck.setLastRebootTimestampUtc(lastRebootTimestampUtc);
		healthCheck.setLastItemChangeReceivedTimestampUtc(lastItemChangeReceivedTimestampUtc);

		request.setStoreId(storeId);
		request.setUrl(url);
		request.setServices(services);
		request.setHealthCheckObject(healthCheck);
		request.setHostname(hostname);
		request.setTriggerType(triggerType);
		request.setTimestampUTC(new Timestamp(System.currentTimeMillis()));
		request.setIpAddress(ipAddress);
		return request;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Store [storeId=");
		builder.append(storeId);
		builder.append(", storeName=");
		builder.append(storeName);
		builder.append(", address=");
		builder.append(address);
		builder.append(", regionId=");
		builder.append(regionId);
		builder.append(", customerStoreNumber=");
		builder.append(customerStoreNumber);
		builder.append(", ipAddress=");
		builder.append(ipAddress);
		builder.append(", hostname=");
		builder.append(hostname);
		builder.append(", triggerType=");
		builder.append(triggerType);
		builder.append(", endpoint=");
		builder.append(url);
		builder.append(", lastRebootTimestampUtc=");
		builder.append(lastRebootTimestampUtc);
		builder.append(", lastCommunicationTimestampUtc=");
		builder.append(lastCommunicationTimestampUtc);
		builder.append(", lastItemChangeReceivedTimestampUtc=");
		builder.append(lastItemChangeReceivedTimestampUtc);
		builder.append(", lastFetchPluOffsetTimestampUtc=");
		builder.append(lastFetchPluOffsetTimestampUtc);
		builder.append("]");
		return builder.toString();
	}

}

