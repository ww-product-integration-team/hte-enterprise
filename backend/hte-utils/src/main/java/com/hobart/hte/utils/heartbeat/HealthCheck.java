package com.hobart.hte.utils.heartbeat;

import java.sql.Timestamp;

public class HealthCheck {
    private Timestamp lastRebootTimestampUtc;
    private Timestamp lastCommunicationTimestampUtc;
    private Timestamp lastItemChangeReceivedTimestampUtc;
    private Timestamp lastFetchPluOffsetTimestampUtc;

    // Getter Methods

    public Timestamp getLastRebootTimestampUtc() {
        return lastRebootTimestampUtc;
    }

    public Timestamp getLastCommunicationTimestampUtc() {
        return lastCommunicationTimestampUtc;
    }

    public Timestamp getLastItemChangeReceivedTimestampUtc() {
        return lastItemChangeReceivedTimestampUtc;
    }

    public Timestamp getLastFetchPluOffsetTimestampUtc() {
        return lastFetchPluOffsetTimestampUtc;
    }

    // Setter Methods

    public void setLastRebootTimestampUtc(Timestamp lastRebootTimestampUtc) {
        this.lastRebootTimestampUtc = lastRebootTimestampUtc;
    }

    public void setLastCommunicationTimestampUtc(Timestamp lastCommunicationTimestampUtc) {
        this.lastCommunicationTimestampUtc = lastCommunicationTimestampUtc;
    }

    public void setLastItemChangeReceivedTimestampUtc(Timestamp lastItemChangeReceivedTimestampUtc) {
        this.lastItemChangeReceivedTimestampUtc = lastItemChangeReceivedTimestampUtc;
    }

    public void setLastFetchPluOffsetTimestampUtc(Timestamp lastFetchPluOffsetTimestampUtc) {
        this.lastFetchPluOffsetTimestampUtc = lastFetchPluOffsetTimestampUtc;
    }
}
