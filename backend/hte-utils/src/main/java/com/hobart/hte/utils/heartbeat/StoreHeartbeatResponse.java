package com.hobart.hte.utils.heartbeat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.store.StoreEvent;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class StoreHeartbeatResponse {
    private String storeId;
    private Timestamp responseTimeStamp;
    private String ipAddress;
    private String url;
    private List<Service> storeServices; //Services that only belong to the store, NOT HQ
    private String hostname;
    private String triggerType;
    private Timestamp timestampUTC;
    private String storeName;
    private String storeAddress;
    private String storeNumber;
    private String regionName;
    private Boolean enabled;
    ArrayList<StoreEvent> eventLogs = new ArrayList<StoreEvent>();

    public StoreHeartbeatResponse(){
        super();
    }

    public StoreHeartbeatResponse(StoreHeartbeatRequest shbr) {
        this.setHostname(shbr.getHostname());
        this.setIpAddress(shbr.getIpAddress());
        this.setStoreId(shbr.getStoreId());
        this.setTriggerType(shbr.getTriggerType());
        this.setResponseTimeStamp(new Timestamp(System.currentTimeMillis()));
        this.setUrl(shbr.getUrl());
        this.setTimestampUTC(new Timestamp(System.currentTimeMillis()));
    }

    public String getStoreId() {
        return storeId;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getResponseTimeStamp() {
        return responseTimeStamp;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getUrl() {
        return url;
    }

    public List<Service> getStoreServices() {
        return storeServices;
    }

    public void setStoreServices(List<Service> storeServices) {
        this.storeServices = storeServices;

    }

    public String getHostname() {
        return hostname;
    }

    public String getTriggerType() {
        return triggerType;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getTimestampUTC() {
        return timestampUTC;
    }

    public String getStoreName() {
        return storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public String getRegionName() {
        return regionName;
    }

    // Setter Methods

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public void setResponseTimeStamp(Timestamp responseTimeStamp) {
        this.responseTimeStamp = responseTimeStamp;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setTriggerType(String triggerType) {
        this.triggerType = triggerType;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTimestampUTC(Timestamp timestampUTC) {
        this.timestampUTC = timestampUTC;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public ArrayList<StoreEvent> getEventLogs() {
        return eventLogs;
    }

    public void setEventLogs(ArrayList<StoreEvent> eventLogs) {
        this.eventLogs = eventLogs;
    }

    public void addEvent(StoreEvent se) {
        this.eventLogs.add(se);
    }

    public void setStoreInfo(Store store) {
        this.setStoreAddress(store.getAddress());
        this.setStoreName(store.getStoreName());
        this.setStoreNumber(store.getCustomerStoreNumber());
        this.setRegionName(store.getRegionId());
        this.setEnabled(store.getEnabled());
    }

    public Store toStore(){
        Store newStore = new Store();
        newStore.setStoreId(storeId);
        newStore.setStoreName(storeName);
        newStore.setAddress(storeAddress);
        newStore.setRegionId(regionName);
        newStore.setCustomerStoreNumber(storeNumber);
        newStore.setEnabled(enabled);
        newStore.setServices(storeServices);
        return newStore;
    }
}

