package com.hobart.hte.utils.device;

import com.hobart.hte.utils.file.HobFiles;
import com.hobart.hte.utils.heartbeat.ScaleHeartbeatRequest;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "scaleSettings")
public class ScaleSettings {
    @Id
    private String deviceId;
    private String macAddress;
    private String ipAddress;
    private String appHookVersion;
    private Timestamp lastRebootTimestampUtc;
    private Timestamp lastCommunicationTimestampUtc;
    private Timestamp lastFetchPluOffsetTimestampUtc;
    private String licenses;
    private String features;
    private String storeInformation;
    private String storeGraphicName;
    private String scaleMode;
    private String scaleItemEntryMode;
    private String labelStockSize;
    private boolean hasLoadCell;
    private String weigherPrecision;
    private Timestamp weigherCalibratedTimestampUtc;
    private Timestamp weigherConfiguredTimestampUtc;
    private boolean hasCustomerDisplay;
    private String timezone;
    private String timeKeeper;
    private String frequency;
    private String dayOfWeek;
    private String timeOfDay;
    private String antiMeridian;
    private String timestamp;
    @Column(columnDefinition = "TEXT")
    private String currentFiles;
    @Transient
    private List<String> healthLog;
    @Transient
    private String storeName;
    @Transient
    private String storeNumber;

    public ScaleSettings() {

    }

    public ScaleSettings(String device, ScaleHeartbeatRequest hbr) {
        try {
            this.deviceId = device;
            this.ipAddress = hbr.getIpAddress();
            if (hbr.getHealthCheck().getLastRebootTimestampUtc() != null)
                this.lastRebootTimestampUtc = Timestamp.from(hbr.getHealthCheck().getLastRebootTimestampUtc());
            if (hbr.getHealthCheck().getLastCommunicationTimestampUtc() != null)
                this.lastCommunicationTimestampUtc = Timestamp
                        .from(hbr.getHealthCheck().getLastCommunicationTimestampUtc());
            if (hbr.getHealthCheck().getLastItemChangeReceivedTimestampUtc() != null)
                this.lastFetchPluOffsetTimestampUtc = Timestamp
                        .from(hbr.getHealthCheck().getLastItemChangeReceivedTimestampUtc());

            StringBuilder sb = new StringBuilder();
            String[] split = hbr.getScaleInfo().getLicenses().toString().split("\\n");
            for (String s : split) {
                if (s.startsWith("SCALE_")) {
                    sb.append(s.trim()).append(",");
                }
            }
            this.appHookVersion = hbr.getScaleInfo().getAppHookVersion();
            this.licenses = sb.toString();
            if(hbr.getScaleInfo().getFeatures() != null){
            this.features = hbr.getScaleInfo().getFeatures().toString().replaceAll("(\\[|\\]| )", "");
            } else {
                this.features = "";
            }
            if (hbr.getScaleSettings() != null) {
                this.storeInformation =
                        hbr.getScaleSettings().getStoreName() + ", " + hbr.getScaleSettings().getStoreNumber();
                this.storeGraphicName = hbr.getScaleSettings().getStoreGraphicName();
                this.scaleMode = hbr.getScaleSettings().getScaleMode();
                this.scaleItemEntryMode = hbr.getScaleSettings().getScaleItemEntryMode();
                this.labelStockSize = hbr.getScaleSettings().getLabelStockSize();
                this.hasLoadCell = hbr.getScaleSettings().isHasLoadCell();
                this.weigherPrecision = hbr.getScaleSettings().getWeigherPrecision();
                if (hbr.getScaleSettings().getWeigherCalibratedTimestampUtc() != null)
                    this.weigherCalibratedTimestampUtc = hbr.getScaleSettings().getWeigherCalibratedTimestampUtc();
                if (hbr.getScaleSettings().getWeigherConfiguredTimestampUtc() != null)
                    this.weigherConfiguredTimestampUtc = hbr.getScaleSettings().getWeigherConfiguredTimestampUtc();
                this.hasCustomerDisplay = hbr.getScaleSettings().isHasCustomerDisplay();
                this.timezone = hbr.getScaleSettings().getTimezone();
                this.timeKeeper = hbr.getScaleSettings().getTimeKeeper();
                this.timestamp = hbr.getScaleSettings().getTimestamp();
                this.frequency = hbr.getScaleSettings().getFrequency();
                this.dayOfWeek = hbr.getScaleSettings().getDayOfWeek();
                this.timeOfDay = hbr.getScaleSettings().getTimeOfDay();
                this.antiMeridian = hbr.getScaleSettings().getAntiMeridian();
            }
            this.macAddress = "|";
            if (hbr.getNetworkSettings() != null) {
                if (hbr.getNetworkSettings().getWired() != null && hbr.getNetworkSettings().getWired().getMacAddress() != null) {
                    this.macAddress = hbr.getNetworkSettings().getWired().getMacAddress() + this.macAddress;
                }
                if (hbr.getNetworkSettings() != null && hbr.getNetworkSettings().getWireless() != null && hbr.getNetworkSettings().getWireless().getMacAddress() != null) {
                    this.macAddress += hbr.getNetworkSettings().getWireless().getMacAddress();
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("Error with heartbeat request");
        }
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMacAddress() {return macAddress;}

    public void setMacAddress(String macAddress) {this.macAddress = macAddress;}

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getAppHookVersion() {
        return appHookVersion;
    }

    public void setAppHookVersion(String appHookVersion) {
        this.appHookVersion = appHookVersion;
    }

    public Timestamp getLastRebootTimestampUtc() {
        return lastRebootTimestampUtc;
    }

    public void setLastRebootTimestampUtc(Timestamp lastRebootTimestampUtc) {
        this.lastRebootTimestampUtc = lastRebootTimestampUtc;
    }

    public Timestamp getLastCommunicationTimestampUtc() {
        return lastCommunicationTimestampUtc;
    }

    public void setLastCommunicationTimestampUtc(Timestamp lastCommunicationTimestampUtc) {
        this.lastCommunicationTimestampUtc = lastCommunicationTimestampUtc;
    }

    public Timestamp getLastFetchPluOffsetTimestampUtc() {
        return lastFetchPluOffsetTimestampUtc;
    }

    public void setLastFetchPluOffsetTimestampUtc(Timestamp lastFetchPluOffsetTimestampUtc) {
        this.lastFetchPluOffsetTimestampUtc = lastFetchPluOffsetTimestampUtc;
    }

    public String getLicenses() {
        return licenses;
    }

    public void setLicenses(String licenses) {
        this.licenses = licenses;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getStoreInformation() {
        return storeInformation;
    }

    public String getStoreGraphicName() {
        return storeGraphicName;
    }

    public void setStoreGraphicName(String storeGraphicName) {
        this.storeGraphicName = storeGraphicName;
    }

    public String getScaleMode() {
        return scaleMode;
    }

    public void setScaleMode(String scaleMode) {
        this.scaleMode = scaleMode;
    }

    public String getScaleItemEntryMode() {
        return scaleItemEntryMode;
    }

    public void setScaleItemEntryMode(String scaleItemEntryMode) {
        this.scaleItemEntryMode = scaleItemEntryMode;
    }

    public String getLabelStockSize() {
        return labelStockSize;
    }

    public void setLabelStockSize(String labelStockSize) {
        this.labelStockSize = labelStockSize;
    }

    public boolean isHasLoadCell() {
        return hasLoadCell;
    }

    public void setHasLoadCell(boolean hasLoadCell) {
        this.hasLoadCell = hasLoadCell;
    }

    public String getWeigherPrecision() {
        return weigherPrecision;
    }

    public void setWeigherPrecision(String weigherPrecision) {
        this.weigherPrecision = weigherPrecision;
    }

    public Timestamp getWeigherCalibratedTimestampUtc() {
        return weigherCalibratedTimestampUtc;
    }

    public void setWeigherCalibratedTimestampUtc(Timestamp weigherCalibratedTimestampUtc) {
        this.weigherCalibratedTimestampUtc = weigherCalibratedTimestampUtc;
    }

    public Timestamp getWeigherConfiguredTimestampUtc() {
        return weigherConfiguredTimestampUtc;
    }

    public void setWeigherConfiguredTimestampUtc(Timestamp weigherConfiguredTimestampUtc) {
        this.weigherConfiguredTimestampUtc = weigherConfiguredTimestampUtc;
    }

    public boolean isHasCustomerDisplay() {
        return hasCustomerDisplay;
    }

    public void setHasCustomerDisplay(boolean hasCustomerDisplay) {
        this.hasCustomerDisplay = hasCustomerDisplay;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimeKeeper() {
        return timeKeeper;
    }

    public void setTimeKeeper(String timeKeeper) {
        this.timeKeeper = timeKeeper;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getTimeOfDay() {
        return timeOfDay;
    }

    public void setTimeOfDay(String timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    public String getAntiMeridian() {
        return antiMeridian;
    }

    public void setAntiMeridian(String antiMeridian) {
        this.antiMeridian = antiMeridian;
    }

    public String getCurrentFiles() {return currentFiles;}

    public List<String> getHealthLog() {
        return healthLog;
    }

    public void setHealthLog(List<String> healthLog) {
        this.healthLog = healthLog;
    }

    public String getStoreName() {return storeName;}

    public String getStoreNumber() {return storeNumber;}

    public HashMap<String, String> getCurrentFileMap(){
        HashMap<String, String> fileMap = new HashMap<>();
        if(this.currentFiles == null){return fileMap;}
        String[] files = this.currentFiles.split("\\s*,\\s*");
        for(String file: files){
            List<String> thisFile = Arrays.asList(file.split("\\s*=\\s*"));
            if(thisFile.size() == 2){
                fileMap.put(thisFile.get(0), thisFile.get(1));
            }

            if(file.equals("pending")){
                fileMap.put("pending", "true");
            }

            if(file.equals("profileChanged")){
                fileMap.put("profileChanged", "true");
            }

            if(file.equals("newProfile")){
                fileMap.put("newProfile", "true");
            }
        }
        return fileMap;
    }

    public void setCurrentFiles(String currentFiles) {this.currentFiles = currentFiles;}

    public void addCurrentFilesItem(String fileItem){
        String newStr = "";
        if(this.currentFiles != null){newStr = this.currentFiles;}
        newStr += "," + fileItem;
        this.currentFiles = newStr;
    }

    public void setCurrentFiles(List<HobFiles> currentFiles) {
        StringBuilder newFileStr = new StringBuilder();
        String prefix = "";//Set Prefix to empty originally to skip haveing a comma first
        for(HobFiles file : currentFiles){
            newFileStr.append(prefix);
            prefix=",";
            newFileStr.append(file.getFileName());
            newFileStr.append("=");
            newFileStr.append(file.getFileChecksum());
        }
        this.currentFiles = newFileStr.toString();

    }

    public boolean has(String  prop) {
        List<String> settingsProps = Arrays.asList(
        "deviceId",
        "macAddress",
        "ipAddress",
        "appHookVersion",
        "lastRebootTimestampUtc",
        "lastCommunicationTimestampUtc",
        "lastFetchPluOffsetTimestampUtc",
        "licenses",
        "features",
        "storeInformation",
        "storeGraphicName",
        "scaleMode",
        "scaleItemEntryMode",
        "labelStockSize",
        "hasLoadCell",
        "weigherPrecision",
        "weigherCalibratedTimestampUtc",
        "weigherConfiguredTimestampUtc",
        "hasCustomerDisplay",
        "timezone",
        "timeKeeper",
        "currentFiles",
        "healthLog"
        );
        return settingsProps.contains(prop);
    }

    @Override
    public String toString() {
        return "ScaleSettings{" +
                "deviceId='" + deviceId + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", appHookVersion='" + appHookVersion + '\'' +
                ", lastRebootTimestampUtc=" + lastRebootTimestampUtc +
                ", lastCommunicationTimestampUtc=" + lastCommunicationTimestampUtc +
                ", lastFetchPluOffsetTimestampUtc=" + lastFetchPluOffsetTimestampUtc +
                ", licenses='" + licenses + '\'' +
                ", features='" + features + '\'' +
                ", storeGraphicName='" + storeGraphicName + '\'' +
                ", storeInformation='" + storeInformation + '\'' +
                ", storeName='" + storeName + '\'' +
                ", storeNumber='" + storeNumber + '\'' +
                ", scaleMode='" + scaleMode + '\'' +
                ", scaleItemEntryMode='" + scaleItemEntryMode + '\'' +
                ", labelStockSize='" + labelStockSize + '\'' +
                ", hasLoadCell=" + hasLoadCell +
                ", weigherPrecision='" + weigherPrecision + '\'' +
                ", weigherCalibratedTimestampUtc=" + weigherCalibratedTimestampUtc +
                ", weigherConfiguredTimestampUtc=" + weigherConfiguredTimestampUtc +
                ", hasCustomerDisplay=" + hasCustomerDisplay +
                ", timezone='" + timezone + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", timeKeeper='" + timeKeeper + '\'' +
                ", frequency='" + frequency + '\'' +
                ", dayOfWeek='" + dayOfWeek + '\'' +
                ", timeOfDay='" + timeOfDay + '\'' +
                ", antiMeridian='" + antiMeridian + '\'' +
                ", currentFiles='" + currentFiles + '\'' +
                '}';
    }
}
