package com.hobart.hte.utils.tree;

import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.file.HobFiles;
import com.hobart.hte.utils.model.NodeType;
import com.hobart.hte.utils.model.ScaleStatusType;
import com.hobart.hte.utils.model.SyncStatus;
import com.hobart.hte.utils.store.Store;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Schema(description = "This class represents a nodeStatus with its basic information to be used in the asset list")
@Entity
@Table(name = "nodeStatus")
public class NodeStatus {
    private static final Logger log = LogManager.getLogger(NodeStatus.class);

    @Id
    @Schema(description = "The node's Id.")
    private String deviceId;
    @Schema(description = "The node's parent Id.")
    private String parentId;

    @Enumerated(EnumType.STRING)
    @Schema(description = "The node's parent Id.")
    private NodeType nodeType;
    @Enumerated(EnumType.STRING)
    @Schema(description = "A UUID number to identify the scale in the system.")
    private ScaleStatusType status = ScaleStatusType.UNKNOWN;
    @Enumerated(EnumType.STRING)
    @Schema(description = "The sync status of the node")
    private SyncStatus sync = SyncStatus.NA;
    @Schema(description = "The sync message fo the node")
    private String syncMessage = "";
    @Schema(description = "")
    private String message = "";
    @Schema(description = "The number of child scales online ")
    private Integer numOnline = 0;
    @Schema(description = "The number of child scales error ")
    private Integer numError = 0;
    @Schema(description = "The number of child scales warning ")
    private Integer numWarning = 0;
    @Schema(description = "The number of child scales disabled ")
    private Integer numDisabled = 0;
    @Schema(description = "The number of child scales unknown ")
    private Integer numUnknown = 0;

    //==================================================================================================================
    //Scale Constructor
    //==================================================================================================================
    public NodeStatus(ScaleDevice device) {
        this.deviceId = device.getDeviceId();
        this.nodeType = NodeType.SCALE;
        this.parentId = device.getStoreId() + "|" + device.getDeptId();
    }
    //==================================================================================================================
    //Store Constructor
    //==================================================================================================================
    public NodeStatus(Store store, List<ScaleDevice> storeScales){
        double timeSince = 1234;
        this.deviceId = store.getStoreId();
        this.nodeType = NodeType.STORE;
        this.parentId = store.getRegionId();
        if(store.getLastCommunicationTimestampUtc() != null){
            timeSince = (new Date().getTime() - store.getLastCommunicationTimestampUtc().getTime())/8.64e+7;
        }


        //ALL DISABLED CHECKS HERE
        //=============================================================================================
        if(!store.getEnabled()){
            status = ScaleStatusType.DISABLED;
            message = "Store is disabled and is not communicating with the server.";
            return;
        }

        status = ScaleStatusType.UNKNOWN;
        if(store.getIpAddress() == null || store.getIpAddress().isEmpty() ||
                store.getUrl() == null || store.getUrl().isEmpty()){
            message= "Store Server is not configured; no endpoint found.";
            return;
        }

        //ALL ERROR CHECKS HERE
        //=============================================================================================
        if(timeSince > 2){
            if((int)timeSince == 1234){
                message= "Could not find any report records for store, please make sure that store is online and connected to server.";
            }
            else{
                message= "Store has not reported for " + (int)timeSince + " day(s).";
            }

            return;
        }

        //ALL WARNING CHECKS HERE
        //=============================================================================================
        status = ScaleStatusType.WARNING;

        if(timeSince > 1){
            message= "Store has not reported in the last 24 hours.";
            return;
        }

        //ALL ONLINE CHECKS HERE
        //=============================================================================================
        if(timeSince <= 1){
            status = ScaleStatusType.ONLINE;
            message = "Store server is online and reporting.";
            return;
        }

        //Default to Error
        //=============================================================================================
        status = ScaleStatusType.ERROR;
        message= "Store is currently in an error state.";
    }

    public NodeStatus() {

    }

    /* Elided code from the NodeStatus constructor that only takes a ScaleDevice argument.
    Previously, this method would presume that a "timeSince" of 1 (i.e., one day) or more was "away", and a timeSince
    of 2 or more was offline. With the newer away- and offline-threshold configs, this is dreadfully obsolete.

    We can't access the configuration repository class from this project, so we call this method from a project
    that can -- namely ui-manager -- to figure out whether the scale is meeting its report time expectations.
     */
    public void setScaleReportStatus(ScaleDevice device, String awayTime, String offlineTime) {
        double timeSince = 1234;

        double awayThreshold = Double.parseDouble(awayTime) / 24.0;
        double offlineThreshold = Double.parseDouble(offlineTime) / 24.0;

        if(device.getLastReportTimestampUtc() != null){
            timeSince = (new Date().getTime() - device.getLastReportTimestampUtc().getTime()) / 8.64e+7;
        }
        double timeSinceHours = timeSince * 24;

        //DISABLED CHECKS HERE
        //=============================================================================================
        if(!device.isEnabled()){
            status = ScaleStatusType.DISABLED;
            numDisabled = 1;
            message = "Scale is disabled and will not communicate with the server.";
            return;
        }

        //ERROR CHECKS HERE
        //=============================================================================================
        if(timeSince > offlineThreshold){
            status = ScaleStatusType.ERROR;
            numError = 1;
            if((int)timeSince == 1234){
                message= "Could not find any report records for this scale. Ensure that the scale is online and is connected to the server.";
            }
            else {
                if (timeSince > 1) {
                    message= "Scale has exceeded the server's " + offlineTime + "-hour tolerance and is now considered offline.\n" +
                            "Scale has not reported in " + (int)timeSince + " day(s).";
                } else {
                    message= "Scale has exceeded the server's " + offlineTime + "-hour tolerance and is now considered offline.\n" +
                            "Scale has not reported in " + (int)timeSinceHours + " hour(s).";
                }

            }

            return;
        }

        //ALL WARNING CHECKS HERE
        //=============================================================================================

        if(device.getProfileId() == null || device.getProfileId().equals(AppConfig.DefaultProfileId)){
            status = ScaleStatusType.WARNING;
            numWarning = 1;
            message= "Scale has not been assigned a profile.";
            return;
        }

        if(timeSince > awayThreshold){
            status = ScaleStatusType.WARNING;
            numWarning = 1;
            message= "Scale has not met its " + awayTime + "-hour report deadline.";
            return;
        }

        //ALL ONLINE CHECKS HERE
        //=============================================================================================
        if (timeSince <= awayThreshold){
            status = ScaleStatusType.ONLINE;
            numOnline = 1;
            message = "Scale is online and reporting.";
            return;
        }

        //Default to Error
        //=============================================================================================
        status = ScaleStatusType.ERROR;
        message= "Scale is currently in an error state.";
    }


    public void AddChildStatus(NodeStatus childStatus){

        numOnline += childStatus.getNumOnline();
        numError += childStatus.getNumError();
        numWarning += childStatus.getNumWarning();
        numDisabled += childStatus.getNumDisabled();
        numUnknown += childStatus.getNumUnknown();

    }
    // Used to set the time sync warning message
    // This is required bc a SQL query is necessary and is already called beforehand
//    public void setTimeSyncStatus(ScaleSettings settings){
//        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy");
//        Timestamp now = Timestamp.from(Instant.now());
//        try{
//            Timestamp scaleTimestamp;
//            if (settings.getTimestamp() == null) {
//                scaleTimestamp = settings.getLastCommunicationTimestampUtc();
//            } else {
//                Date parsedDate = dateFormat.parse(settings.getTimestamp());
//                scaleTimestamp = new Timestamp(parsedDate.getTime());
//            }
//            log.debug(Instant.now());
//            log.debug(scaleTimestamp);
//            long timeDiff = now.getTime() - scaleTimestamp.getTime();
//            if(timeDiff < 0)
//                timeDiff = timeDiff * -1;
//            if(TimeUnit.MILLISECONDS.toHours(timeDiff) > 2){
//                log.debug("Scale is out of sync with enterprise by:", TimeUnit.MILLISECONDS.toHours(timeDiff), "hours.");
//                message= "Scale's time is not in-sync with Enterprise.";
//                status = ScaleStatusType.WARNING;
//                numWarning = 1;
//                if(numOnline == 1){
//                    numOnline = 0;
//                }
//            }
//        } catch (Exception e){
//            log.error("Unable to parse timestamp");
//        }
//    }


    public ScaleStatusType getStatus() {
        return status;
    }

    public void setStatus(ScaleStatusType status) {
        this.status = status;
    }

    private void setSync(List<HobFiles> profileFiles, HashMap<String, String> currentFiles){
        SyncStatus newStatus = SyncStatus.SYNCED;
        this.syncMessage = "Configurations match; device is in sync with its current profile.";
        if(profileFiles == null || currentFiles == null){
            this.sync = SyncStatus.NOT_SYNCED;
            this.syncMessage = "Could not find a current configuration of the scale. This scale may have not completed a heartbeat or its profile no longer exists.";
            return;
        }

        boolean isPending = currentFiles.get("pending") != null;
        boolean profileChanged = currentFiles.get("profileChanged") != null;
        boolean newProfile = currentFiles.get("newProfile") != null;
        currentFiles.remove("pending");
        currentFiles.remove("profileChanged");
        currentFiles.remove("newProfile");

        for(HobFiles file : profileFiles){
            String currentChksum = currentFiles.remove(file.getFileName());

            //If file doesn't exist in current checksum or there is a checksum mismatch
            //Indicate out of sync
            if(!file.getFileChecksum().equals(currentChksum)){
                //log.info("File checksums don't match {}: {} == {}", file.getFileName(), currentChksum, file.getFileChecksum());
                this.syncMessage = "Scale's reported configuration does not match its assigned profile!";
                newStatus = SyncStatus.NOT_SYNCED;
            }
        }

        //If we have more have files cached than are in the current profile, i.e. someone deleted a file.
        // Then we are also considered out of sync
        if(!currentFiles.isEmpty()){
            this.syncMessage = "Scale's reported configuration does not match its assigned profile!";
            newStatus = SyncStatus.NOT_SYNCED;
        }

        //If profile change has occurred then we want to indicate that the scale has observered the changes
        //However if the scale is still in-sync (i.e. The profile change has a delayed starting file) then we should display
        //These pending messages
        if (isPending && newStatus == SyncStatus.NOT_SYNCED) {
            //Current status is pending i.e. profile has changed and have not received a hb since change indicate status as pending
            this.sync = SyncStatus.PENDING;
            this.syncMessage = "Scale has retrieved the updated profile configuration and is working on installing the update.";
            return;
        }

        if (profileChanged && newStatus == SyncStatus.NOT_SYNCED){
            this.sync = SyncStatus.NOT_SYNCED;
            this.syncMessage = "Scale has not completed a heartbeat since the profile update. The scale's configuration should update on the next heartbeat.";
            return;
        }

        if (newProfile && newStatus == SyncStatus.NOT_SYNCED){
            this.sync = SyncStatus.NOT_SYNCED;
            this.syncMessage = "Scale has not completed a heartbeat since changing to the new profile. The scale's configuration should update on the next heartbeat.";
            return;
        }



        this.sync = newStatus;
    }

    public void setScaleSync(ScaleDevice device, ScaleSettings ss, List<HobFiles> profileFiles){
        if(!device.isEnabled()){
            setSync(SyncStatus.NA);
            setSyncMessage("Device is disabled, unable to perform a sync check!");
        }
        else if(ss == null){
            setSync(SyncStatus.NOT_SYNCED);
            setSyncMessage("Could not locate sync information! Please make sure the scale is online and connected to the server.");
        }
        else{
            setSync(profileFiles, ss.getCurrentFileMap());

            // Problematic code below.
            // TODO: Fix this someday. The current implementation is shaky and we've had customers freak out over it.
            /*
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy");
            Timestamp now = Timestamp.from(Instant.now());
            try{
                Timestamp scaleTimestamp;
                if (ss.getTimestamp() == null) {
                    scaleTimestamp = ss.getLastCommunicationTimestampUtc();
                } else {
                    Date parsedDate = dateFormat.parse(ss.getTimestamp());
                    scaleTimestamp = new Timestamp(parsedDate.getTime());
                }
                log.debug(Instant.now());
                log.debug(scaleTimestamp);
                long timeDiff = now.getTime() - scaleTimestamp.getTime();
                if(timeDiff < 0)
                    timeDiff = timeDiff * -1;
                if(TimeUnit.MILLISECONDS.toHours(timeDiff) > 2){
                    log.debug("Scale is out of sync with enterprise by:", TimeUnit.MILLISECONDS.toHours(timeDiff), "hours.");
                    this.sync = SyncStatus.NOT_SYNCED;
                    return;
                }
            } catch (Exception e){
                log.error("Unable to parse timestamp");
            }
             */
        }
    }
    public String getDeviceId() {return deviceId; }
    public void setDeviceId(String deviceId) { this.deviceId = deviceId; }
    public NodeType getNodeType(){ return nodeType; }

    public void setNodeType(NodeType nodeType){ this.nodeType = nodeType; }

    public String getParentId() {return parentId; }

    public void setParentId(String parentId) { this.parentId = parentId; }

    public SyncStatus getSync() {
        return sync;
    }

    public void setSync(SyncStatus sync) {
        this.sync = sync;
    }

    public String getSyncMessage() {return syncMessage;}

    public void setSyncMessage(String syncMessage) {this.syncMessage = syncMessage;}

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNumOnline() {
        return numOnline;
    }

    public void setNumOnline(Integer numOnline) {
        this.numOnline = numOnline;
    }

    public Integer getNumError() {
        return numError;
    }

    public void setNumError(Integer numError) {
        this.numError = numError;
    }

    public Integer getNumWarning() {
        return numWarning;
    }

    public void setNumWarning(Integer numWarning) {
        this.numWarning = numWarning;
    }

    public Integer getNumDisabled() {
        return numDisabled;
    }

    public void setNumDisabled(Integer numDisabled) {
        this.numDisabled = numDisabled;
    }

    public Integer getNumUnknown() {
        return numUnknown;
    }

    public void setNumUnknown(Integer numUnknown) {
        this.numUnknown = numUnknown;
    }
}