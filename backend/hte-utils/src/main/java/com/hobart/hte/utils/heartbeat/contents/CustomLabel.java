package com.hobart.hte.utils.heartbeat.contents;

public class CustomLabel {
	private int labelNumber;
	private String labelName;
	private String version;

	public int getLabelNumber() {
		return labelNumber;
	}

	public void setLabelNumber(int labelNumber) {
		this.labelNumber = labelNumber;
	}

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "CustomLabel [labelNumber=" + labelNumber + ", labelName=" + labelName + ", version=" + version + "]";
	}
}
