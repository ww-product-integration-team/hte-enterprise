package com.hobart.hte.utils.event;

public enum EventImportance {
    NOTIFY, WARNING, ERROR
}
