package com.hobart.hte.utils.heartbeat;

import com.hobart.hte.utils.store.Store;

import java.util.HashMap;
import java.util.UUID;

public class ScaleConfigEvent extends ScaleEvent{

    /**
     * Content of the Event, can contain any Config Tag ex. storeId, storeNam
     *
     * NOTE: Must follow naming convention found in the config table in the HT scale
     */
    private HashMap<String, String> fields;

    /**
     * @param callbackUrl
     */
    public ScaleConfigEvent(String callbackUrl) {
        super("SCALE_CONFIG_EVENT", UUID.randomUUID().toString(), callbackUrl);
        this.fields = new HashMap<>();
    }

    public ScaleConfigEvent(String callbackUrl, HashMap<String, String> fields) {
        super("SCALE_CONFIG_EVENT", UUID.randomUUID().toString(), callbackUrl);
        this.fields = fields;
    }

    public ScaleConfigEvent(String callbackUrl, Store store, boolean appSendStoreInfo, String appCustomStoreName) {
        super("SCALE_CONFIG_EVENT", UUID.randomUUID().toString(), callbackUrl);
        this.fields = new HashMap<>();

        /*If the Store sendStoreInfo entry is null that means that it listens to the App Configuration of the same name
        *
        * i.e. Add store id if store config is null and app config is null or store has a custom entry of "true" which indicates
        * it wants to use the default formatting but does not listen to the app config
        */
        if(store.getSendStoreInfo() == null || store.getSendStoreInfo().equals("true")){
            //Need to nest this check, otherwise nullException will get hit when appSendStoreInfo is false
            if(store.getSendStoreInfo() == null && !appSendStoreInfo){return;}
            if(store.getStoreId() != null){
                this.fields.put("storeId", store.getCustomerStoreNumber());
            }

            if(appCustomStoreName == null){
                this.fields.put("storeName", store.getStoreName() + "\n" + store.getAddress());
            }
            else{
                this.fields.put("storeName", appCustomStoreName);
            }
        }
        /*If the entry is not equal to null or "true" that means this store has a custom configuration and thus does not
        * listen to the app configuration. It means that the custom configuration is in the "sendStoreInfo"
        */
        else{
            if(!store.getSendStoreInfo().equals("false")){
                if(store.getStoreId() != null){
                    this.fields.put("storeId", store.getStoreId());
                }

                this.fields.put("storeName", store.getStoreName());
            }
        }

    }

    public HashMap<String, String> getFields() {
        return fields;
    }

    public void setFields(HashMap<String, String> fields) {
        this.fields = fields;
    }

    public void appendFields(String key, String value){
        this.fields.put(key, value);
    }
}
