package com.hobart.hte.utils.file;

import java.io.File;

public class HobFiles {
    private String fileId;
    private String fileName;
    private FileType fileType;
    private long fileSize;
    private String fileVersion;
    private Integer startRebootTime;
    private Integer endRebootTime;



    public void setEndRebootTime(Integer endRebootTime) {
        this.endRebootTime = endRebootTime;
    }

    /**
     * Checksum values are MD5 encoding.
     */
    private String fileChecksum;

    public HobFiles(){
        super();
    }

    public HobFiles(String fileId, String fileName, long fileSize, String fileVersion, String fileChecksum, FileType fileType) {
        super();
        this.fileId = fileId;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileVersion = fileVersion;
        this.fileChecksum = fileChecksum;
        this.fileType = fileType;
    }

    public HobFiles(FileForEvent ffe) {
        this.fileId = ffe.getFileId();
        this.fileName = ffe.getFilename();
        this.fileSize = ffe.getSize();
        this.fileVersion = ffe.getFileVersion();
        this.fileChecksum = ffe.getChecksum();
        this.fileType = ffe.getFileType();
        this.startRebootTime = ffe.getStartRebootTime();
        this.endRebootTime = ffe.getEndRebootTime();
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileVersion() {
        return fileVersion;
    }

    public void setFileVersion(String fileVersion) {
        this.fileVersion = fileVersion;
    }

    public String getFileChecksum() {
        return fileChecksum;
    }

    public void setFileChecksum(String fileChecksum) {
        this.fileChecksum = fileChecksum;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }
    public Integer getStartRebootTime() {
        return startRebootTime;
    }

    public void setStartRebootTime(Integer startRebootTime) {
        this.startRebootTime = startRebootTime;
    }

    public Integer getEndRebootTime() {
        return endRebootTime;
    }

    @Override
    public String toString() {
        return "HobFiles [fileName=" + fileName + ", fileSize=" + fileSize + ", fileVersion=" + fileVersion
                + ", fileChecksum=" + fileChecksum + "startRebootTime=" + startRebootTime
                + "endRebootTime=" + endRebootTime + "]";
    }
}
