package com.hobart.hte.utils.profile;

import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.lang.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Schema(description="Java representation of checksumLogs database table")
@Entity
@IdClass(ChecksumLog.class)
@Table(name="checksumLogs")
public class ChecksumLog implements Serializable {
    @Id
    private String scaleIp;
    @Id
    private String date;
    private String logEntry;

    public String getScaleIp() {
        return scaleIp;
    }

    public void setScaleIp(String scaleIp) {
        this.scaleIp = scaleIp;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLogEntry() {
        return logEntry;
    }

    public void setLogEntry(String logEntry) {
        this.logEntry = logEntry;
    }
}
