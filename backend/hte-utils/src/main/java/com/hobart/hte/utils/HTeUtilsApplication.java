package com.hobart.hte.utils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HTeUtilsApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(HTeUtilsApplication.class, args);
        System.out.println("HTe Enterprise \n\n||  HTe Utils 0.0.1 ||\n\n--= ITW FEG Hobart 2022(c) =--");


    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HTeUtilsApplication.class);
    }
}
