package com.hobart.hte.utils.model;

public enum SyncStatus {

    /*
    NA - Log received directly from scale no need for sync
    NOT_SYNCED - Logs have not yet been shared with HQ
    PENDING - NOTUSED
    SYNCED - Logs have been shared to HQ
     */
    NA, NOT_SYNCED, PENDING, SYNCED, UNKNOWN
}
