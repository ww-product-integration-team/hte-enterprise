package com.hobart.hte.utils.banner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "banner")
public class Banner {
    @Id @Column(columnDefinition = "VARCHAR(36)")
    private String bannerId;
    private String bannerName;

    public Banner() {
        super();
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    public boolean has(String prop) {
        List<String> properties = Arrays.asList("bannerId","bannerName");
        return properties.contains(prop);
    }
}
