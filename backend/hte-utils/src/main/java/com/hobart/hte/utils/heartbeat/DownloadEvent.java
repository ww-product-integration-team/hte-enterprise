package com.hobart.hte.utils.heartbeat;

import com.hobart.hte.utils.file.HobFiles;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DownloadEvent extends ScaleEvent{
    /**
     * the url where the scale will download the file from the server
     */
    private String downloadUrl;
    private String backupDownloadUrl;

    /**
     * list of files to download in the event
     */
    private List<HobFiles> files;

    /**
     *
     * @param backupDownloadUrl
     * @param downloadUrl
     * @param callbackUrl
     */
    public DownloadEvent(String downloadUrl, String backupDownloadUrl, String callbackUrl) {
        super("SYNC_CONFIG", UUID.randomUUID().toString(), callbackUrl);
        this.backupDownloadUrl = backupDownloadUrl;
        this.downloadUrl = downloadUrl;
        this.files = new ArrayList<HobFiles>();
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getBackupDownloadUrl() {
        return backupDownloadUrl;
    }

    public void setBackupDownloadUrl(String backupDownloadUrl) {
        this.backupDownloadUrl = backupDownloadUrl;
    }

    //@JsonProperty(value = "localBackupFileInfo")
    public List<HobFiles> getFiles() {
        return files;
    }

    public void setFiles(List<HobFiles> files) {
        this.files = files;
    }

    public void addFile(HobFiles hobFile) {
        if (this.files != null) {
            this.files.add(hobFile);
        }
    }

}
