package com.hobart.hte.utils.dashboard;

import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.CombinedScaleDataInt;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.tree.ScaleNode;

import java.util.ArrayList;
import java.util.List;

public class DashboardData {
    List<Banner> banners;
    List<Region> regions;
    List<Store> stores;
    List<String> depts;
    List<CombinedScaleDataInt> assignedScales;
    List<CombinedScaleDataInt> unassignedScales;
    public DashboardData(){
        this.banners = new ArrayList<>();
        this.regions = new ArrayList<>();
        this.stores = new ArrayList<>();
        this.depts = new ArrayList<>();
        this.assignedScales = new ArrayList<>();
        this.unassignedScales = new ArrayList<>();
    }

    public List<Banner> getBanners() {
        return banners;
    }

    public void setBanners(List<Banner> banners) {
        this.banners = banners;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    public List<String> getDepts() {
        return depts;
    }

    public void setDepts(List<String> depts) {
        this.depts = depts;
    }

    public List<CombinedScaleDataInt> getAssignedScales() {
        return assignedScales;
    }

    public void setAssignedScales(List<CombinedScaleDataInt> assignedScales) {
        this.assignedScales = assignedScales;
    }

    public List<CombinedScaleDataInt> getUnassignedScales() {
        return unassignedScales;
    }

    public void setUnassignedScales(List<CombinedScaleDataInt> unassignedScales) {
        this.unassignedScales = unassignedScales;
    }

    public void addBanners(Iterable<Banner> banners){
        for( Banner b : banners) {
            this.banners.add(b);
        }
    }
    public void addBanners(Banner b){
        this.banners.add(b);
    }
    public void addRegions(Iterable<Region> regions){
        for ( Region r : regions) {
            this.regions.add(r);
        }
    }
    public void addRegions(Region r){
        this.regions.add(r);
    }
    public void addStores(Iterable<Store> stores){
        for( Store s : stores) {
            this.stores.add(s);
        }
    }
    public void addStores(Store s){
        this.stores.add(s);
    }
    public void addDepts(List<String> depts){
        for(String d : depts){
            if(!this.depts.contains(d)) {
                this.depts.add(d);
            }
        }
    }

    public void addDepts(Iterable<Department> depts){
        for(Department d : depts){
            if(!this.depts.contains(d.getdeptId())) {
                this.depts.add(d.getdeptId());
            }
        }
    }

    public void addDepts(String  d){
        if(!this.depts.contains(d)) {
            this.depts.add(d);
        }
    }

    public void addAssignedScales(Iterable<CombinedScaleDataInt> assignedScales){
        for( CombinedScaleDataInt s : assignedScales) {
            this.assignedScales.add(s);
        }
    }

    public void addAssignedScales(CombinedScaleDataInt scale){
        this.assignedScales.add(scale);
    }

    public void addUnassignedScales(Iterable<CombinedScaleDataInt> unassignedScales){
        for( CombinedScaleDataInt s : unassignedScales) {
            this.unassignedScales.add(s);
        };
    }
}