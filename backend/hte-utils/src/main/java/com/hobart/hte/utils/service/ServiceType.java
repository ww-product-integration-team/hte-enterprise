package com.hobart.hte.utils.service;

public enum ServiceType {
    HEARTBEAT, ITEM_LOOKUP, STORE_CLIENT, STORE_SERVER, UI_MANAGER;

    public String getName(){
        switch (this){
            case HEARTBEAT:
                return "Heartbeat Manager";
            case ITEM_LOOKUP:
                return "Item Lookup";
            case STORE_CLIENT:
                return "Store Client";
            case STORE_SERVER:
                return "Store Manager";
            case UI_MANAGER:
                return "HTe Enterprise Manager";
            default:
                return "Unknown Service";
        }
    }
}
