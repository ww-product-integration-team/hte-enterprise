package com.hobart.hte.utils.tree;

import com.hobart.hte.utils.autoAssign.AutoAssign;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

@Schema(description = "This class is to save to the db the key pair between a store and its depts")
@Entity
@Table(name = "storeDeptPairs")
@IdClass(StoreDeptPair.class)
public class StoreDeptPair implements Serializable {
    @Id
    @Schema(description = "The store Id.")
    private String storeId;
    @Id
    @Schema(description = "The dept  Id.")
    private String deptId;
    public StoreDeptPair(){}
    public StoreDeptPair(String storeId, String deptId){
        this.storeId = storeId;
        this.deptId = deptId;
    }

    public String getStoreId(){return this.storeId;}
    public void setStoreId(String storeId){this.storeId = storeId;}
    public String getDeptId(){return this.deptId;}
    public void setDeptId(String deptId){this.deptId = deptId;}

}

