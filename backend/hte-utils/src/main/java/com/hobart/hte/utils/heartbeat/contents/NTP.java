package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NTP {
	private String server;
	private int frequency;
	private int dayOfWeek;
	private String timeOfDay;
	/**
	 * e.g. America/Chicago
	 */
	private String timezone;
	/**
	 * AM/PM
	 */
	private String antiMeridian;

	@JsonProperty(value = "server")
	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	@JsonProperty(value = "frequency")
	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	@JsonProperty(value = "dayOfWeek")
	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	@JsonProperty(value = "timeOfDay")
	public String getTimeOfDay() {
		return timeOfDay;
	}

	public void setTimeOfDay(String timeOfDay) {
		this.timeOfDay = timeOfDay;
	}

	@JsonProperty(value = "timezone")
	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@JsonProperty(value = "antiMeridian")
	public String getAntiMeridian() {
		return antiMeridian;
	}

	public void setAntiMeridian(String antiMeridian) {
		this.antiMeridian = antiMeridian;
	}

}
