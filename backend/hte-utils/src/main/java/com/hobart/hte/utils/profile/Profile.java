package com.hobart.hte.utils.profile;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Time;
import java.sql.Timestamp;

@Schema(description = "This class represents a profile with its basic information. A profile associates a group of configuration files for Hobart scales.")
@Entity
@Table(name = "profile")
public class Profile {
    @Id
    @Column(columnDefinition = "VARCHAR(36)")
    private String profileId;
    private String name;
    private String description;
    private Timestamp lastUpdate;
    private String requiresDomainId;
    private String zoneId;

    //Needs to follow the format
    //"[Interval] [Day] [Time] [Items]"
    //ex: Daily 1 2 flashkeys,plu, operator
    public Profile(){};
    public Profile(String profileId, String name, String description, Timestamp lastUpdate){
        this.profileId = profileId;
        this.name = name;
        this.description = description;
        this.lastUpdate = lastUpdate;
        this.requiresDomainId = "87588758-8758-8758-8758-875887588758";
    }

    private String deleteSyncConfig;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getRequiresDomainId() {
        return requiresDomainId;
    }

    public void setRequiresDomainId(String requiresDomainId) {
        this.requiresDomainId = requiresDomainId;
    }

    public String getDeleteSyncConfig() {
        return deleteSyncConfig;
    }

    public void setDeleteSyncConfig(String deleteSyncConfig) {
        this.deleteSyncConfig = deleteSyncConfig;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
