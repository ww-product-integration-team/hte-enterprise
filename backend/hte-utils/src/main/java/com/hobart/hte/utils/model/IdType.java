package com.hobart.hte.utils.model;

public enum IdType {
	IPADDRESS, UUID, USERNAME, FILENAME
}
