package com.hobart.hte.utils.socket;

//Internal Enum for Async running state
public enum AsyncState {
    IDLE,
    RUNNING,
    CANCELLED
}
