package com.hobart.hte.utils.heartbeat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleInfo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ScaleHeartbeatResponse {
    private String deviceUuid;
    private String responseUuid;
    private Timestamp responseTimeStamp;
    private ScaleInfo scaleInfo;
    private List<ScaleEvent> events;

    public ScaleHeartbeatResponse(ScaleHeartbeatRequest hbr) {
        scaleInfo = hbr.getScaleInfo(); // new ScaleInfo();
        events = new ArrayList<>();

        setResponseUuid(hbr.getMessageUuid());
        setResponseTimeStamp(new Timestamp(System.currentTimeMillis()));
    }

    public ScaleHeartbeatResponse(String messageUuid, ScaleDevice sd) {
        this.deviceUuid = sd.getDeviceId();
        this.scaleInfo = new ScaleInfo(sd);
        events = new ArrayList<>();
        setResponseUuid(messageUuid);
        setResponseTimeStamp(new Timestamp(System.currentTimeMillis()));
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public String getResponseUuid() {
        return responseUuid;
    }

    public void setResponseUuid(String responseUuid) {
        this.responseUuid = responseUuid;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getResponseTimeStamp() {
        return responseTimeStamp;
    }

    public void setResponseTimeStamp(Timestamp responseTimeStamp) {
        this.responseTimeStamp = responseTimeStamp;
    }

    public ScaleInfo getScaleInfo() {
        return scaleInfo;
    }

    public void setScaleInfo(ScaleInfo scaleInfo) {
        this.scaleInfo = scaleInfo;
    }

    public void setScaleInfoProfile(String profile) {this.scaleInfo.setProfileId(profile);}

    public void setScaleInfoStore(String store) {this.scaleInfo.setStoreId(store);}

    public void setScaleInfoDept(String dept) {this.scaleInfo.setDeptId(dept);}

    public List<ScaleEvent> getEvents() {
        return events;
    }

    public void setEvents(List<ScaleEvent> events) {
        this.events = events;
    }

    public void addEvents(List<ScaleEvent> newEvents) {
        this.events.addAll(newEvents);
    }

    public void addEvent(ScaleEvent newEvent) {
        if (this.events != null) {
            this.events.add(newEvent);
        }
    }

    public String toString() {
        String output = String.format("Heartbeat response info: [%s]", this.scaleInfo);
        return output;
    }
}