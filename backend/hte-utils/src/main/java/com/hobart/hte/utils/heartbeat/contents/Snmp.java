package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Snmp {
	private boolean isEnabled;
	private int variablesPort;
	private String trapsIp;
	private int trapsPort;

	@JsonProperty(value = "isEnabled")
	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	@JsonProperty(value = "variablesPort")
	public int getVariablesPort() {
		return variablesPort;
	}

	public void setVariablesPort(int variablesPort) {
		this.variablesPort = variablesPort;
	}

	@JsonProperty(value = "trapsIp")
	public String getTrapsIp() {
		return trapsIp;
	}

	public void setTrapsIp(String trapsIp) {
		this.trapsIp = trapsIp;
	}

	@JsonProperty(value = "trapsPort")
	public int getTrapsPort() {
		return trapsPort;
	}

	public void setTrapsPort(int trapsPort) {
		this.trapsPort = trapsPort;
	}
}
