package com.hobart.hte.utils.tree;

import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.utils.store.Store;
import io.swagger.v3.oas.annotations.media.Schema;

import java.beans.Transient;
import java.util.*;

import static com.hobart.hte.utils.config.AppConfig.DefaultProfileId;

@Schema(description = "Class that represents a node in the tree view for the web interface.")
public class ScaleNode {
    @Schema(description = "A unique id for the node, could be the same UUID used in the database or a sequential number.")
    private String id;
    @Schema(description = "Depending on the type, this could be the name of the Region or department. In the case of a scale, the IP address. In the case of a department, its corresponding name.", example = "North region | 10.3.128.59 | Deli | Store 1410 Troy, OH")
    private String name;
    @Schema(description = "Additional information about this node. Could be scale model, store name/number, etc.", example = "HTi-7LS")
    private String text;

    @Schema(description = "banner, region | store | department | scale", example = "scale")
    private String type;

    @Schema(description = "List of nodes corresponding to regions, stores, departments or scales with this same ScaleNode structure.")
    private Map<String, ScaleNode> children;

    @Schema(description = "List of nodes corresponding to regions, stores, departments or scales with this same ScaleNode structure.")
    private boolean hasChildren;
    @Schema(description = "Status of the node, how many scales are offline, what are the current status messages?")
    private NodeStatus nodeStatus;

    @Schema(description = "List of batches this node is connected to. Only applies to stores, departments, and scales.")
    private List<String> batchIds;

    @Schema(description = "Properties corresponding only to the scale device, this will be only populated in the scale nodes.")
    private ScaleDevice properties;

    @Schema(description="Each ID of the tree structure leading to the specific entity")
    private List<String> path;
    private String filterId;
    private String profileId;

    public ScaleNode(String id, String name, String text, String type) {
        super();
        this.id = id;
        this.name = name;
        this.text = text;
        this.type = type;
        this.batchIds = new ArrayList<>();
        this.filterId = "";
    }

    public ScaleNode(Banner b) {
        this.id = b.getBannerId();
        this.name = b.getBannerName();
        this.type = "banner";
        this.children = new LinkedHashMap<>();
        this.batchIds = new ArrayList<>();
        this.filterId = "";
    }

    public ScaleNode(Region r) {
        this.id = String.valueOf(r.getRegionId());
        this.name = r.getRegionName();
        this.type = "region";
        this.children = new LinkedHashMap<>();
        this.batchIds = new ArrayList<>();
        this.filterId = "";
    }

    public ScaleNode(Store s) {
        this.id = String.valueOf(s.getStoreId());
        this.name = s.getStoreName();
        this.text = s.getCustomerStoreNumber();
        this.type = "store";
        this.children = new LinkedHashMap<>();
        this.batchIds = new ArrayList<>();
        this.filterId = "";
    }

    public ScaleNode(ScaleDevice s) {
        this.id = s.getDeviceId();
        this.name = s.getIpAddress();
        if (s.getLastReportTimestampUtc() == null) {
            this.text = "";
        } else {
            this.text = s.getLastReportTimestampUtc().toString();
        }
        this.type = "scale";
        this.properties = s;
        this.children = new LinkedHashMap<>();
        this.nodeStatus = new NodeStatus(s);
        this.hasChildren = false;
        this.batchIds = new ArrayList<>();
        this.filterId = "";
    }

    public ScaleNode(Department d) {
        this.id = String.valueOf(d.getdeptId());
        this.name = d.getDeptName1();
        this.text = d.getDeptName2();
        this.type = "dept";
        this.children = new LinkedHashMap<>();
        this.batchIds = new ArrayList<>();
        this.filterId = "";
        if (d.getDefaultProfileId() != null && !d.getDefaultProfileId().equals(DefaultProfileId)) {
            this.profileId = d.getDefaultProfileId();
        }
    }
    public ScaleNode(){

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setNodeStatus(NodeStatus nodeStatus) {
        this.nodeStatus = nodeStatus;
    }

    public NodeStatus getNodeStatus() {
        return nodeStatus;
    }

    public List<String> getBatchIds() {
        return batchIds;
    }

    public void setBatchIds(List<String> batchIds) {
        this.batchIds = batchIds;
    }

    public Map<String, ScaleNode> getChildren() {
        return children;
    }

    public void setChildren(Map<String, ScaleNode> children) {
        this.children = children;
    }

    public void addNode(NodeContainer nodeContainer) {
        nodeStatus = nodeContainer.getNodeStatus();
        if (children == null) {
            children = new HashMap<>();
        }
        children.putAll(nodeContainer.getNodes());
    }

    public ScaleDevice getProperties() {
        return properties;
    }

    public void setProperties(ScaleDevice properties) {
        this.properties = properties;
    }
    public void setHasChildren(boolean hasChildren) { this.hasChildren = hasChildren;}
    public boolean getHasChildren(){ return this.hasChildren; }
    public List<String> getPath() {
        return path;
    }

    public void setPath(List<String> path) {
        this.path = path;
    }
    public void  updatePathWithFilter(String filterId ){
        List<String> acc = new ArrayList<>();
        for(String nodeId : this.path) {
            if(!nodeId.contains("~")) {
                acc.add(filterId + "~" + nodeId);
            } else {
                acc.add(nodeId);
            }
        }
        this.path = acc;
    }

    public String getFilterId() { return  filterId;}

    public  void setFilterId(String filterId){ this.filterId = filterId;}

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
}