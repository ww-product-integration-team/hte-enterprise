package com.hobart.hte.utils.upgrade;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.sql.Timestamp;

@Schema(description="")
@Entity
@Table(name="upgradeBatch")
public class UpgradeBatch {

    @Id
    @Schema(description="")
    private String batchId;

    @Schema(description="")
    private String fileId;

    @Schema(description="")
    private String description;

    @Schema(description="")
    private Timestamp uploadStart;

    @Schema(description="")
    private Timestamp uploadEnd;

    @Schema(description="")
    private Timestamp upgradeStart;

    @Schema(description="")
    private Timestamp upgradeEnd;

    @Schema(description="")
    private String name;

    @Schema(description="")
    private Integer afterUpgradeCheckIn;

    @Transient
    private String fileName;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getUploadStart() {
        return uploadStart;
    }

    public void setUploadStart(Timestamp uploadStart) {
        this.uploadStart = uploadStart;
    }

    public Timestamp getUploadEnd() {
        return uploadEnd;
    }

    public void setUploadEnd(Timestamp uploadEnd) {
        this.uploadEnd = uploadEnd;
    }

    public Timestamp getUpgradeStart() {
        return upgradeStart;
    }

    public void setUpgradeStart(Timestamp upgradeStart) {
        this.upgradeStart = upgradeStart;
    }

    public Timestamp getUpgradeEnd() {
        return upgradeEnd;
    }

    public void setUpgradeEnd(Timestamp upgradeEnd) {
        this.upgradeEnd = upgradeEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getAfterUpgradeCheckIn() {return afterUpgradeCheckIn;}

    public void setAfterUpgradeCheckIn(Integer afterUpgradeCheckIn) {this.afterUpgradeCheckIn = afterUpgradeCheckIn;}
}
