package com.hobart.hte.utils.file;

public enum FileType {
    FEATURE, CONFIG, DEBIAN, INSTALL_IMAGE, TARBALL, LICENSE,
    IMAGE_CATEGORY, IMAGE_PRODUCT, IMAGE_PLANOGRAM, IMAGE_SELFSERVICE, IMAGE_SOFTKEY, IMAGE_SPECIAL, IMAGE_LAYOUT, IMAGE_ONLINE_ORDERING, IMAGE_NOW_SERVING,
    VIDEO, AUDIO, PLAYLIST, LAYOUT, FONT, DOCUMENT, UNKNOWN;
}
