package com.hobart.hte.utils.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hteCustomer")
public class Customer {
	@Id
	@Column(columnDefinition = "VARCHAR(36)")
	public String customerUUID;
	public String customerName;
	public String apiKey;
	public String apiSecret;
	@Column(columnDefinition = "TIMESTAMP")
	public String createdAt;
	@Column(columnDefinition = "TIMESTAMP")
	public String updatedAt;
	@Column(columnDefinition = "TINYINT")
	public String deleted;
}
