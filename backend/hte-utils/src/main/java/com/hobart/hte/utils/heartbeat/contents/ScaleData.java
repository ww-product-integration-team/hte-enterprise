package com.hobart.hte.utils.heartbeat.contents;

import java.time.Instant;

public class ScaleData {
	private String storeId;
	private String storeName;
	private String storeGraphicName;
	private String deptId;
	private String countryCode;
	private String scaleMode;
	private String scaleItemEntryMode;
	private String labelStockSize;
	private boolean hasLoadCell;
	private String weigherPrecision;
	private Instant weigherCalibratedTimestampUtc;
	private Instant weigherConfiguredTimestampUtc;
	private boolean hasCustomerDisplay;
	private String timezone;
	private String timeKeeper;

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreGraphicName() {
		return storeGraphicName;
	}

	public void setStoreGraphicName(String storeGraphicName) {
		this.storeGraphicName = storeGraphicName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getScaleMode() {
		return scaleMode;
	}

	public void setScaleMode(String scaleMode) {
		this.scaleMode = scaleMode;
	}

	public String getScaleItemEntryMode() {
		return scaleItemEntryMode;
	}

	public void setScaleItemEntryMode(String scaleItemEntryMode) {
		this.scaleItemEntryMode = scaleItemEntryMode;
	}

	public String getLabelStockSize() {
		return labelStockSize;
	}

	public void setLabelStockSize(String labelStockSize) {
		this.labelStockSize = labelStockSize;
	}

	public boolean isHasLoadCell() {
		return hasLoadCell;
	}

	public void setHasLoadCell(boolean hasLoadCell) {
		this.hasLoadCell = hasLoadCell;
	}

	public String getWeigherPrecision() {
		return weigherPrecision;
	}

	public void setWeigherPrecision(String weigherPrecision) {
		this.weigherPrecision = weigherPrecision;
	}

	public Instant getWeigherCalibratedTimestampUtc() {
		return weigherCalibratedTimestampUtc;
	}

	public void setWeigherCalibratedTimestampUtc(Instant weigherCalibratedTimestampUtc) {
		this.weigherCalibratedTimestampUtc = weigherCalibratedTimestampUtc;
	}

	public Instant getWeigherConfiguredTimestampUtc() {
		return weigherConfiguredTimestampUtc;
	}

	public void setWeigherConfiguredTimestampUtc(Instant weigherConfiguredTimestampUtc) {
		this.weigherConfiguredTimestampUtc = weigherConfiguredTimestampUtc;
	}

	public boolean isHasCustomerDisplay() {
		return hasCustomerDisplay;
	}

	public void setHasCustomerDisplay(boolean hasCustomerDisplay) {
		this.hasCustomerDisplay = hasCustomerDisplay;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTimeKeeper() {
		return timeKeeper;
	}

	public void setTimeKeeper(String timeKeeper) {
		this.timeKeeper = timeKeeper;
	}

	@Override
	public String toString() {
		return "ScaleSettings [storeNumber=" + storeId + ", storeName=" + storeName + ", storeGraphicName="
				+ storeGraphicName + ", deptNumber=" + deptId + ", countryCode=" + countryCode + ", scaleMode="
				+ scaleMode + ", scaleItemEntryMode=" + scaleItemEntryMode + ", labelStockSize=" + labelStockSize
				+ ", hasLoadCell=" + hasLoadCell + ", weigherPrecision=" + weigherPrecision
				+ ", weigherCalibratedTimestampUtc=" + weigherCalibratedTimestampUtc
				+ ", weigherConfiguredTimestampUtc=" + weigherConfiguredTimestampUtc + ", hasCustomerDisplay="
				+ hasCustomerDisplay + ", timezone=" + timezone + ", timeKeeper=" + timeKeeper + "]";
	}
}
