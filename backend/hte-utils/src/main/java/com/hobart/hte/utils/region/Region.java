package com.hobart.hte.utils.region;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name ="region")
public class Region {
    @Id
    @Column(columnDefinition = "VARCHAR(36)")
    private String regionId;
    private String regionName;
    @Column(columnDefinition = "VARCHAR(36)")
    private String bannerId;

    public Region() {

    }

    public String getRegionId() {return regionId;}

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public boolean has(String prop) {
        List<String> properties = Arrays.asList("regionId","regionName","bannerId");
        return properties.contains(prop);
    }
}
