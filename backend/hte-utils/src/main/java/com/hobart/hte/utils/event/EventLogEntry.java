package com.hobart.hte.utils.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hobart.hte.utils.model.EntityType;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "eventLog")
public class EventLogEntry {
    @Id
    @Column(columnDefinition = "TIMESTAMP(3)")
    private Timestamp timestamp;
    @Schema(description = "The id of the event, constant value for each place an event is added")
    private Integer eventId;
    @Schema(description = "The id of the entity that is the subject of the event")
    private String entityId;
    @Schema(description = "The type of the entity that is the subject of the event, i.e. Banner, Store, etc. ")
    private EntityType entityType;
    @Schema(description = "The type of event: Notification, Error, Warning")
    private EventImportance type;
    @Schema(description = "Has the event been read by the user?")
    private Boolean opened;
    @Schema(description = "Has the event been flagged by the user or the server?")
    private Boolean flagged;
    @Schema(description = "Should the user be notified when this event occurs")
    private Boolean muted;
    @Column(columnDefinition = "TEXT")
    @Schema(description = "Any notes about the status of the event, e.g. Event was read by user at time")
    private String statusStr;
    @Schema(description = "List of timestamps for this same event that have been triggered, limit 10")
    private String occurrences;
    @Schema(description = "Title of the event")
    private String event;
    @Column(columnDefinition = "TEXT")
    @Schema(description = "Description of the event")
    private String message;

    public EventLogEntry() {
    }

    public EventLogEntry(Integer eventId, String entityId, EntityType entityType, EventImportance type, Boolean muted, String event, String message) {
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.eventId = eventId;
        this.entityId = entityId;
        this.entityType = entityType;
        this.type = type;
        this.event = event;
        this.message = message;
        this.opened = false;
        this.flagged = false;
        this.muted = muted;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public EventImportance getType() {
        return type;
    }

    public void setType(EventImportance type) {
        this.type = type;
    }

    public Boolean getOpened() {
        return opened;
    }

    public void setOpened(Boolean opened) {
        this.opened = opened;
    }

    public Boolean getFlagged() {
        return flagged;
    }

    public Boolean isFlagged() {
        return flagged;
    }

    public void setFlagged(Boolean flagged) {
        this.flagged = flagged;
    }

    public Boolean getMuted() {
        return muted;
    }

    public void setMuted(Boolean muted) {
        this.muted = muted;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(String occurrences) {
        this.occurrences = occurrences;
    }

    public void appendOccurrence(Timestamp timestamp){
        String[] times = occurrences.split(",");
        StringBuilder newStr = new StringBuilder();
        newStr.append(timestamp.toString());
        for(int i = 0; i < 9; i++){
            if(i+1 > times.length){
                break;
            }
            newStr.append(",");
            newStr.append(times[i]);
        }
        occurrences = newStr.toString();
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonIgnore
    public boolean canBeUpdated(EventLogEntry dbEvent) {
        return this.getEventId().equals(dbEvent.getEventId()) && this.getEntityId().equals(dbEvent.getEntityId()) &&
                this.getEntityType().equals(dbEvent.getEntityType()) && this.getType().equals(dbEvent.getType()) &&
                this.getMuted().equals(dbEvent.getMuted()) && this.getMessage().equals(dbEvent.getMessage()) &&
                this.getOccurrences().equals(dbEvent.getOccurrences()) && this.getEvent().equals(dbEvent.getEvent());
    }
}
