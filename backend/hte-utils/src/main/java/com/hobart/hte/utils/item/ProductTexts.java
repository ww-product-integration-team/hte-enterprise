package com.hobart.hte.utils.item;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "productTexts")
public class ProductTexts {
	@EmbeddedId
	private TextId textId;
	private String ProdTextDesc;

	public TextId getTextId() {
		return textId;
	}

	public void setTextId(TextId textId) {
		this.textId = textId;
	}

	public String getProdTextDesc() {
		return ProdTextDesc;
	}

	public void setProdTextDesc(String prodTextDesc) {
		ProdTextDesc = prodTextDesc;
	}
}
