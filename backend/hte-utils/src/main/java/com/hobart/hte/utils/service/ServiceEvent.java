package com.hobart.hte.utils.service;
import com.hobart.hte.utils.store.StoreEvent;

import java.util.ArrayList;
import java.util.List;

public class ServiceEvent extends StoreEvent {
    
    List<Service> services;
    Boolean clearServices;

    public ServiceEvent() {
        super();
        this.setEventType("SYNC_SERVICE");
        services = new ArrayList<Service>();
        clearServices = true;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public Boolean getClearServices() {
        return clearServices;
    }

    public void setClearServices(Boolean clearServices) {
        this.clearServices = clearServices;
    }
}
