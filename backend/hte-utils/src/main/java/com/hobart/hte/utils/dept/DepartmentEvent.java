package com.hobart.hte.utils.dept;

import com.hobart.hte.utils.store.StoreEvent;

import java.util.ArrayList;
import java.util.List;

public class DepartmentEvent extends StoreEvent {
    List<Department> departments;

    public DepartmentEvent() {
        this.setEventType("SYNC_DEPT");
        departments = new ArrayList<Department>();
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}