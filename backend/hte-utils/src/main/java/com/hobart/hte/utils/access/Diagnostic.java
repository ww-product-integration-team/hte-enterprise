package com.hobart.hte.utils.access;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

import java.time.Instant;

@Schema(description = "Diagnosics")
@Entity
@Table(name = "hteCustomerDiagnostics")
public class Diagnostic {

	public Diagnostic(Integer id, String customerUUID, Integer numScales, Double avgLabelsPrinted,
			Integer numScalesOffline, Integer numScalesOnline,
			Instant dateCreated) {
		super();
		this.id = id;
		this.customerUUID = customerUUID;
		this.numScales = numScales;
		this.numScalesOffline = numScalesOffline;
		this.numScalesOnline = numScalesOnline;
		this.dateCreated = dateCreated;
	}

	public Diagnostic() {
	}

	@Id
	public Integer id;
	private String customerUUID;
	private Integer numScales;
	private Double avgLabelsPrinted;
	private Integer numScalesOffline;
	private Integer numScalesOnline;
	private Instant dateCreated;

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomerUUID() {
		return customerUUID;
	}

	public Integer getNumScales() {
		return numScales;
	}

	public Double getAvgLabelsPrinted() {
		return avgLabelsPrinted;
	}

	public Integer getNumScalesOffline() {
		return numScalesOffline;
	}

	public Integer getNumScalesOnline() {
		return numScalesOnline;
	}

	public Instant getDateCreated() {
		return dateCreated;
	}

	public void setCustomerUUID(String customerUUID) {
		this.customerUUID = customerUUID;
	}

	public void setNumScales(Integer numScales) {
		this.numScales = numScales;
	}

	public void setAvgLabelsPrinted(Double avgLabelsPrinted) {
		this.avgLabelsPrinted = avgLabelsPrinted;
	}

	public void setNumScalesOffline(Integer numScalesOffline) {
		this.numScalesOffline = numScalesOffline;
	}

	public void setNumScalesOnline(Integer numScalesOnline) {
		this.numScalesOnline = numScalesOnline;
	}

	public void setDateCreated(Instant dateCreated) {
		Timestamp ts = Timestamp.from(Instant.now());
		Instant instant = ts.toInstant();
		this.dateCreated = instant;
	}

	@Override
	public String toString() {
		return "Diagnostic{" +
				"id='" + id + '\'' +
				"customerUUID='" + customerUUID + '\'' +
				", numScales='" + numScales + '\'' +
				", avgLabelsPrinted='" + avgLabelsPrinted + '\'' +
				", numScalesOffline='" + numScalesOffline + '\'' +
				", numScalesOnline='" + numScalesOnline + '\'' +
				", dateCreated'" + dateCreated + '\'' +
				'}';
	}

}