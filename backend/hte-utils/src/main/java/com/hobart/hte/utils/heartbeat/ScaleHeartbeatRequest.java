package com.hobart.hte.utils.heartbeat;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hobart.hte.utils.device.ScaleInfo;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.file.HobFiles;
import com.hobart.hte.utils.heartbeat.contents.*;
import com.hobart.hte.utils.heartbeat.contents.ScaleHealthCheck;


public class ScaleHeartbeatRequest {
	private String deviceUuid;
	private String ipAddress;
	private String hostname;
	private String messageUuid;
	private Instant timestampUTC;
	/**
	 * INITIAL | INTERVAL | MANUAL | SCALE_POWER_UP | SCALE_POWER_DOWN | WAS_OFFLINE
	 */
	private String triggerType;
	private ScaleHealthCheck healthCheck;
	private ScaleInfo scaleInfo;
	private ScaleSettings scaleSettings;
	private NetworkSettings networkSettings;
	private List<PeripheralConnected> peripheralsConnected;
	private List<CustomLabel> customLabels;
	private UsageToday usageToday;
	private List<Disk> disks;
	private List<HobFiles> files;

	public String getDeviceUuid() {
		return deviceUuid;
	}

	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getMessageUuid() {
		return messageUuid;
	}

	public void setMessageUuid(String messageUuid) {
		this.messageUuid = messageUuid;
	}

//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
	public Instant getTimestampUTC() {
		return timestampUTC;
	}

	public void setTimestampUTC(Instant timestampUTC) {
		this.timestampUTC = timestampUTC;
	}

	public String getTriggerType() {
		return triggerType;
	}

	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	@JsonProperty(value = "healthCheck")
	public ScaleHealthCheck getHealthCheck() {
		return healthCheck;
	}

	public void setHealthCheck(ScaleHealthCheck healthCheck) {
		this.healthCheck = healthCheck;
	}

	@JsonProperty(value = "scaleInfo")
	public ScaleInfo getScaleInfo() {
		return scaleInfo;
	}

	public void setScaleInfo(ScaleInfo scaleInfo) {
		this.scaleInfo = scaleInfo;
	}

	@JsonProperty(value = "scaleSettings")
	public ScaleSettings getScaleSettings() {
		return scaleSettings;
	}

	public void setScaleSettings(ScaleSettings scaleSettings) {
		this.scaleSettings = scaleSettings;
	}

	@JsonProperty(value = "networkSettings")
	public NetworkSettings getNetworkSettings() {
		return networkSettings;
	}

	public void setNetworkSettings(NetworkSettings networkSettings) {
		this.networkSettings = networkSettings;
	}

	@JsonProperty(value = "peripheralsConnected")
	public List<PeripheralConnected> getPeripheralsConnected() {
		return peripheralsConnected;
	}

	public void setPeripheralsConnected(List<PeripheralConnected> peripheralsConnected) {
		this.peripheralsConnected = peripheralsConnected;
	}

	@JsonProperty(value = "customLabels")
	public List<CustomLabel> getCustomLabels() {
		return customLabels;
	}

	public void setCustomLabels(List<CustomLabel> customLabels) {
		this.customLabels = customLabels;
	}

	@JsonProperty(value = "usageToday")
	public UsageToday getUsageToday() {
		return usageToday;
	}

	public void setUsageToday(UsageToday usageToday) {
		this.usageToday = usageToday;
	}

	@JsonProperty(value = "disks")
	public List<Disk> getDisks() {
		return disks;
	}

	public void setDisks(List<Disk> disks) {
		this.disks = disks;
	}

	@JsonProperty(value = "localBackupFileInfo")
	public List<HobFiles> getFiles() {
		return files;
	}

	public void setFiles(List<HobFiles> files) {
		this.files = files;
	}

	public HashMap<String, String> generateFileMap(){
		HashMap<String, String> ret = new HashMap<>();
		if (files == null) {
			files = new ArrayList<HobFiles>();
		}

		for(HobFiles file : files){
			ret.put(file.getFileName(), file.getFileChecksum());
		}
		return ret;
	}

	@Override
	public String toString() {
		return "HeartbeatRequest [deviceUuid=" + deviceUuid + ", ipAddress=" + ipAddress + ", hostname=" + hostname
				+ ", messageUuid=" + messageUuid + ", timestampUTC=" + timestampUTC + ", triggerType=" + triggerType
				+ ", \nhealthCheck=" + healthCheck +  ", \nscaleSettings=" + scaleSettings + ", \nscaleInfo=" + scaleInfo
				+ "\nfiles=" + getFiles() + "\n]";
	}

//	@Override
//	public String toString() {
//		return "HeartbeatRequest [deviceUuid=" + deviceUuid + ", ipAddress=" + ipAddress + ", hostname=" + hostname
//				+ ", messageUuid=" + messageUuid + ", timestampUTC=" + timestampUTC + ", triggerType=" + triggerType
//				+ ", \nhealthCheck=" + healthCheck +  ", \nscaleSettings=" + scaleSettings + ", \nscaleInfo=" + scaleInfo
//				+ ", \nnetworkSettings=" + networkSettings + ", \nperipheralsConnected=" + peripheralsConnected
//				+ ", \ncustomLabels=" + customLabels + ", \nusageToday=" + usageToday + ", \ndisks=" + disks
//				+ "\nfiles=" + getFiles() + "\n]";
//	}
}
