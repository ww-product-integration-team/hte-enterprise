package com.hobart.hte.utils.file;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.sql.Timestamp;

@Entity
@Table(name = "hobfile")
public class  FileForEvent {
    @Id
    private String fileId;
    private String filename;
    private String shortDesc;
    private FileType fileType;
    private String fileVersion;
    private long size;
    private String checksum;
    private Timestamp uploadDate;
    private String profileId;
    private Timestamp startDate;
    private Timestamp endDate;
    private boolean enabled;
    private Integer startRebootTime;
    private Integer endRebootTime;

    public Integer getStartRebootTime() {
        return startRebootTime;
    }
    @Transient
    private String content;

    /**
     * default constructor
     */
    public FileForEvent() {
    }

    /**
     *
     * @param uuid
     * @param filename
     * @param size
     * @param checksum
     */
    public FileForEvent(String uuid, String filename, long size, String checksum) {
        super();
        this.fileId = uuid;
        this.filename = filename;
        this.size = size;
        this.checksum = checksum;
    }

    public FileForEvent(FileForEvent newFileData) {
        this.fileId = newFileData.getFileId();
        this.filename = newFileData.getFilename();
        this.shortDesc = newFileData.getShortDesc();
        this.fileType = newFileData.getFileType();
        this.fileVersion = newFileData.getFileVersion();
        this.size = newFileData.getSize();
        this.checksum = newFileData.getChecksum();
        this.uploadDate = newFileData.getUploadDate();
        this.profileId = newFileData.getProfileId();
        this.startDate = newFileData.getStartDate();
        this.endDate = newFileData.getEndDate();
        this.enabled = newFileData.isEnabled();
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public String getFileVersion() {
        return fileVersion;
    }

    public void setFileVersion(String fileVersion) {
        this.fileVersion = fileVersion;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Timestamp uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setStartRebootTime(Integer startRebootTime) {
        this.startRebootTime = startRebootTime;
    }

    public Integer getEndRebootTime() {
        return endRebootTime;
    }

    public void setEndRebootTime(Integer endRebootTime) {
        this.endRebootTime = endRebootTime;
    }
}
