package com.hobart.hte.utils.heartbeat.contents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Adapter {
	private String ssid;
	/**
	 * "SECURITY_OPEN" | "SECURITY_SHARED_KEY" | "SECURITY_WPA_PERSONAL" |
	 * "SECURITY_WPA_PEAP" | "SECURITY_WPA_LEAP" | "SECURITY_WPA_EAP_TLS" |
	 * "SECURITY_WPA2_PERSONAL" | "SECURITY_WPA2_PEAP" | "SECURITY_WPA2_LEAP" |
	 * "SECURITY_WPA2_EAP_TLS" | "SECURITY_802X1_PEAP" | "SECURITY_802X1_LEAP"
	 */
	private String securityMethod;
	/**
	 * "WEP" | "TKIP" | "AES" | "NONE"
	 */
	private String encryption;

	@JsonProperty(value = "ssid")
	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	@JsonProperty(value = "securityMethod")
	public String getSecurityMethod() {
		return securityMethod;
	}

	public void setSecurityMethod(String securityMethod) {
		this.securityMethod = securityMethod;
	}

	@JsonProperty(value = "encryption")
	public String getEncryption() {
		return encryption;
	}

	public void setEncryption(String encryption) {
		this.encryption = encryption;
	}
}
