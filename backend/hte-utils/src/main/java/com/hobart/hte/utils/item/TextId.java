package com.hobart.hte.utils.item;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TextId implements Serializable {

	private static final long serialVersionUID = 1L;
	private int deptNum;
	/*-
	0 - psttUnknown,
	1 - psttExpandedText,
	2 - psttSpecialMessage,
	3 - psttProductNote,
	4 - psttMarqueeText,
	5 - psttProductText5, // CookingInstructionsDev
	6 - psttProductText6, // RecipeDev
	7 - psttProductText7, // ExpandedText2Dev
	8 - psttProductText8, // SuggestiveSellingDev
	9 - psttProductText9, // AllergensDev
	 */
	private int prodTextType;
	private int prodTextNum;

	public TextId() {
		
	}
	
	/**
	 * 
	 * @param deptNum
	 * @param prodTextType
	 * @param prodTextNum
	 */
	public TextId(int deptNum, int prodTextType, int prodTextNum) {
		super();
		this.deptNum = deptNum;
		this.prodTextType = prodTextType;
		this.prodTextNum = prodTextNum;
	}

	public int getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(int deptNum) {
		this.deptNum = deptNum;
	}

	public int getProdTextType() {
		return prodTextType;
	}

	public void setProdTextType(int prodTextType) {
		this.prodTextType = prodTextType;
	}

	public int getProdTextNum() {
		return prodTextNum;
	}

	public void setProdTextNum(int prodTextNum) {
		this.prodTextNum = prodTextNum;
	}
}
