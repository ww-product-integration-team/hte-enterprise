package com.hobart.hte.utils.heartbeat.contents;

import java.time.Instant;

public class ScaleHealthCheck {
	private Instant lastRebootTimestampUtc;
	private Instant lastCommunicationTimestampUtc;
	private Instant lastItemChangeReceivedTimestampUtc;
	private int pluRecordCount;
	private Integer totalLabelsPrinted;

	public Instant getLastRebootTimestampUtc() {
		return lastRebootTimestampUtc;
	}

	public void setLastRebootTimestampUtc(Instant lastRebootTimestampUtc) {
		this.lastRebootTimestampUtc = lastRebootTimestampUtc;
	}

	public Instant getLastCommunicationTimestampUtc() {
		return lastCommunicationTimestampUtc;
	}

	public void setLastCommunicationTimestampUtc(Instant lastCommunicationTimestampUtc) {
		this.lastCommunicationTimestampUtc = lastCommunicationTimestampUtc;
	}

	public Instant getLastItemChangeReceivedTimestampUtc() {
		return lastItemChangeReceivedTimestampUtc;
	}

	public void setLastItemChangeReceivedTimestampUtc(Instant lastItemChangeReceivedTimestampUtc) {
		this.lastItemChangeReceivedTimestampUtc = lastItemChangeReceivedTimestampUtc;
	}

	public int getPluRecordCount() {
		return pluRecordCount;
	}

	public void setPluRecordCount(int pluRecordCount) {
		this.pluRecordCount = pluRecordCount;
	}

	public Integer getTotalLabelsPrinted() {
		return totalLabelsPrinted;
	}

	public void setTotalLabelsPrinted(Integer totalLabelsPrinted) {
		this.totalLabelsPrinted = totalLabelsPrinted;
	}

	@Override
	public String toString() {
		return "HealthCheck [lastRebootTimestampUtc=" + lastRebootTimestampUtc + ", lastCommunicationTimestampUtc="
				+ lastCommunicationTimestampUtc + ", lastItemChangeReceivedTimestampUtc="
				+ lastItemChangeReceivedTimestampUtc + ", pluRecordCount=" + pluRecordCount + ", totalLabelsPrinted="
				+ totalLabelsPrinted + "]";
	}
}
