package com.hobart.hte.utils.upgrade;

import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.util.HteTools;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.sql.Timestamp;

@Schema(description = "")
@Entity
@Table(name = "upgradeDevice")
public class UpgradeDevice {

    public enum Status {
        unassigned, awaiting_upload, awaiting_primary, ready_to_upload, upload_in_progress, upload_completed_no_errors, upload_completed_errors,
        upload_failed, upload_now, file_uploaded_awaiting_upgrade, ready_to_upgrade, upgrade_in_progress, post_install_process,
        upgrade_completed_no_errors, upgrade_completed_errors, upgrade_failed, upgrade_now
    }

    @Id
    @Schema(description = "")
    private String upgradeId;

    @Schema(description = "")
    private String deviceId;

    @Schema(description = "")
    private String batchId;

    @Schema(description = "")
    private Timestamp lastUpdate;

    @Schema(description="")
    private String scaleModel;

    @Schema(description = "")
    private String application;

    @Schema(description = "")
    private String bootloader;

    @Schema(description="")
    private String fpc;

    @Enumerated(EnumType.STRING)
    @Schema(description = "")
    private Status status;

    @Schema(description="")
    private String statusText;

    @Schema(description="")
    private Timestamp nextCheck;

    @Schema(description="")
    private String groupId;

    @Transient
    private String ipAddress;

    @Transient
    private Boolean primaryScale;

    @Transient
    private Boolean enabled;

    @Transient
    private String hostname;

    @Transient
    private Integer pluCount;

    @Transient
    private String assignedStore;

    @Transient
    private String assignedDept;

    @Transient
    private Timestamp lastReport;

    private String fileSelected;

    public UpgradeDevice() {}

    public UpgradeDevice(ScaleDevice scale) {
        this.deviceId = scale.getDeviceId();
        this.lastReport = scale.getLastReportTimestampUtc();
        this.application = scale.getApplication();
        this.bootloader = scale.getLoader();
        this.ipAddress = scale.getIpAddress();
        this.primaryScale = scale.getIsPrimaryScale();
        this.enabled = scale.isEnabled();
        this.pluCount = scale.getPluCount();
        this.hostname = scale.getHostname();
        this.assignedStore = scale.getStoreId();
        this.assignedDept = scale.getDeptId();
        this.scaleModel = scale.getScaleModel();
    }

    public String getUpgradeId() {
        return upgradeId;
    }

    public void setUpgradeId(String upgradeId) {
        this.upgradeId = upgradeId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getScaleModel() {
        return scaleModel;
    }

    public void setScaleModel(String scaleModel) {
        this.scaleModel = scaleModel;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getBootloader() {
        return bootloader;
    }

    public void setBootloader(String bootloader) {
        this.bootloader = bootloader;
    }

    public String getFpc() {
        return fpc;
    }

    public void setFpc(String fpc) {
        this.fpc = fpc;
    }

    public String getStatus() {
        return status.name();
    }

    public void setStatus(String status) {
        this.status = Status.valueOf(status);
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Timestamp getNextCheck() {
        return nextCheck;
    }

    public void setNextCheck(Timestamp nextCheck) {
        this.nextCheck = nextCheck;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Boolean isPrimaryScale() {
        return primaryScale;
    }

    public void setPrimaryScale(Boolean primaryScale) {
        this.primaryScale = primaryScale;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getPluCount() {
        return pluCount;
    }

    public void setPluCount(Integer pluCount) {
        this.pluCount = pluCount;
    }

    public String getAssignedStore() {
        return assignedStore;
    }

    public void setAssignedStore(String assignedStore) {
        this.assignedStore = assignedStore;
    }

    public String getAssignedDept() {
        return assignedDept;
    }

    public void setAssignedDept(String assignedDept) {
        this.assignedDept = assignedDept;
    }

    public Timestamp getLastReport() {
        return lastReport;
    }

    public void setLastReport(Timestamp lastReport) {
        this.lastReport = lastReport;
    }

    public String getFileSelected() {
        return fileSelected;
    }

    public void setFileSelected(String fileSelected) {
        this.fileSelected = fileSelected;
    }

    public void assignFields(ScaleDevice scale) {
        if (HteTools.isNullorEmpty(this.ipAddress) || !HteTools.isNullorEmpty(scale.getIpAddress()))
            this.ipAddress = scale.getIpAddress();
        if (this.primaryScale == null || scale.getIsPrimaryScale() != null)
            this.primaryScale = scale.getIsPrimaryScale();
        if (this.enabled == null || scale.isEnabled() != null)
            this.enabled = scale.isEnabled();
        if (this.pluCount == null || scale.getPluCount() != null)
            this.pluCount = scale.getPluCount();
        if (HteTools.isNullorEmpty(this.hostname) || !HteTools.isNullorEmpty(scale.getHostname()))
            this.hostname = scale.getHostname();
        if (HteTools.isNullorEmpty(this.assignedStore) || !HteTools.isNullorEmpty(scale.getStoreId()))
            this.assignedStore = scale.getStoreId();
        if (HteTools.isNullorEmpty(this.assignedDept) || !HteTools.isNullorEmpty(scale.getDeptId()))
            this.assignedDept = scale.getDeptId();
        if (HteTools.isNullorEmpty(this.scaleModel) || !HteTools.isNullorEmpty(scale.getScaleModel()))
            this.scaleModel = scale.getScaleModel();
        if (this.lastReport == null || scale.getLastReportTimestampUtc() != null)
            this.lastReport = scale.getLastReportTimestampUtc();
        if (HteTools.isNullorEmpty(this.deviceId) || !HteTools.isNullorEmpty(scale.getDeviceId()))
            this.deviceId = scale.getDeviceId();
        if (HteTools.isNullorEmpty(this.application) || !HteTools.isNullorEmpty(scale.getApplication()))
            this.application = scale.getApplication();
        if (HteTools.isNullorEmpty(this.bootloader) || !HteTools.isNullorEmpty(scale.getLoader()))
            this.bootloader = scale.getLoader();
    }
}
