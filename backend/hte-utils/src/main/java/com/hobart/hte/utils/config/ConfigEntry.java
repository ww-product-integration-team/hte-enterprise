package com.hobart.hte.utils.config;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "appConfigs")
public class ConfigEntry {
    @Id
    private String CoName;
    private String CoValue;

    public ConfigEntry() {
    }

    public ConfigEntry(String coName, String coValue) {
        CoName = coName;
        CoValue = coValue;
    }

    public String getCoName() {
        return CoName;
    }

    public void setCoName(String coName) {
        CoName = coName;
    }

    public String getCoValue() {
        return CoValue;
    }

    public void setCoValue(String coValue) {
        CoValue = coValue;
    }

}
