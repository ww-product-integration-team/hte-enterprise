package com.hobart.hte.utils.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.sql.Timestamp;
import java.util.Date;
public class CombinedScaleData extends ScaleDevice {
    private String weigherPrecision;
    private String labelStockSize;
    private String appHookVersion;

    public CombinedScaleData() {}

    public CombinedScaleData(String labelStockSize, String weigherPrecision, String appHookVersion) {
        this.weigherPrecision = weigherPrecision;
        this.labelStockSize = labelStockSize;
        this.appHookVersion = appHookVersion;
    }

    public String getWeigherPrecision() {
        return weigherPrecision;
    }

    public void setWeigherPrecision(String weigherPrecision) {
        this.weigherPrecision = weigherPrecision;
    }

    public String getLabelStockSize() {
        return labelStockSize;
    }

    public void setLabelStockSize(String labelStockSize) {
        this.labelStockSize = labelStockSize;
    }

    public String getAppHookVersion() {
        return appHookVersion;
    }

    public void setAppHookVersion(String appHookVersion) {
        this.appHookVersion = appHookVersion;
    }
}
