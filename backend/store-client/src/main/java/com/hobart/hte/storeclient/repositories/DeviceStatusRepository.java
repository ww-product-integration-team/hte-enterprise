package com.hobart.hte.storeclient.repositories;

import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.event.DeviceStatusEntry;
import com.hobart.hte.utils.model.SyncStatus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

public interface DeviceStatusRepository extends CrudRepository<DeviceStatusEntry, String> {

    List<DeviceStatusEntry> findByLogStatus(SyncStatus syncStatus);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "DELETE FROM DeviceStatusEntry entry WHERE entry.deviceUuid = :id AND entry.timestamp > SUBTIME(:timestamp, 1.0) AND entry.timestamp < SUBTIME(:timestamp, -1.0)")
    void deleteExistingEntry(@Param("timestamp")Timestamp timestamp, @Param("id") String id);
}
