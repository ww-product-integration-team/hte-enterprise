package com.hobart.hte.storeclient.repositories;

import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface ServiceRepository extends CrudRepository<Service, String> {
    List<Service> findByDatabaseId(String databseId);

    @Modifying(clearAutomatically = true)
    @Transactional
    void deleteAllByDatabaseId(String databaseId);

    List<Service> findByDatabaseIdAndServiceType(String databaseId, ServiceType serviceType);
}
