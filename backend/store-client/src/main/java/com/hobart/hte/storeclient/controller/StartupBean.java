package com.hobart.hte.storeclient.controller;

import com.hobart.hte.storeclient.repositories.ConfigAppRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartupBean {

    public static String projectVersion = "1.0.2";
    public static String buildDate = "01/22/2024";

    private static final Logger log = LogManager.getLogger(StartupBean.class);

    @Autowired
    AboutService aboutService;

    @Autowired
    ConfigAppRepository configRepo;

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {

        //Setup the log location
        ConfigEntry logPath = configRepo.findById(AppConfig.logRootPath).orElse(null);
        if(logPath != null && logPath.getCoValue() != null){
            System.setProperty("APP_LOG_ROOT", logPath.getCoValue());
            ((org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false)).reconfigure();
        }

        aboutService.getServiceInfo(true);

        log.info("   _____ _                    _____ _ _            _   ");
        log.info("  / ____| |                  / ____| (_)          | |  ");
        log.info(" | (___ | |_ ___  _ __ ___  | |    | |_  ___ _ __ | |_ ");
        log.info("  \\___ \\| __/ _ \\| '__/ _ \\ | |    | | |/ _ \\ '_ \\| __|");
        log.info("  ____) | || (_) | | |  __/ | |____| | |  __/ | | | |_ ");
        log.info(" |_____/ \\__\\___/|_|  \\___|  \\_____|_|_|\\___|_| |_|\\__|");
        log.info(" ========================================================");
        log.info(" ITW FEG Hobart 2023(c)                        (v"+ projectVersion+")");
        log.info("[Build Date: " + buildDate + "]");
    }
}