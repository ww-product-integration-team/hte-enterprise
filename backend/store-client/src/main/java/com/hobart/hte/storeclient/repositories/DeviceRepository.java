package com.hobart.hte.storeclient.repositories;


import com.hobart.hte.utils.device.ScaleDevice;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeviceRepository extends CrudRepository<ScaleDevice, String> {
	ScaleDevice findByIpAddress(String ipAddress);

	List<ScaleDevice> findByStoreId(String storeId);

	List<ScaleDevice> findByHostnameNot(String hostname);
}