package com.hobart.hte.storeclient.controller;

import com.hobart.hte.storeclient.repositories.*;
import com.hobart.hte.storeclient.scheduledtasks.StoreHeartbeat;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.model.HeartbeatType;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceType;
import com.hobart.hte.utils.store.Store;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

@RestController
@RequestMapping("/store")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class StoreController {
    private static final Logger log = LogManager.getLogger(StoreController.class);

    @Autowired
    StoreRepository storeRepo;

    @Autowired
    DeviceRepository deviceRepo;

    @Autowired
    ConfigAppRepository configRepo;

    @Autowired
    ProfileRepository profileRepo;

    @Autowired
    DepartmentRepository deptRepo;

    @Autowired
    StoreHeartbeat storeHeartbeat;

    @Autowired
    ServiceRepository serviceRepo;

    @Operation(summary = "Get Store Information", description = "Return information")
    @GetMapping(path = "/store", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getStore() {Iterable<Store> findAll = storeRepo.findAll();
        return new ResponseEntity<>(findAll, HttpStatus.OK);
    }

    @Parameters({
            @Parameter(name = "ip", description = "IP address of the scale to query, if no Ip provided a list with all scales will be returned. (Optional)", in = ParameterIn.QUERY, example = "10.3.128.65", schema = @Schema(implementation = String.class)) })
    @GetMapping(path = "/scale", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getScale(@RequestParam(name = "ip", required = false) String scaleIp) {
        OperationStatusModel result = new OperationStatusModel("GetScale");
        if (scaleIp == null) {
            Iterable<ScaleDevice> allScales = deviceRepo.findByHostnameNot(ScaleDevice.getInvisibleScaleName());
            return new ResponseEntity<>(allScales, HttpStatus.OK);
        }

        try{
            ScaleDevice scale = deviceRepo.findByIpAddress(scaleIp);
            return new ResponseEntity<>(scale, HttpStatus.OK);
        }
        catch (Exception e){
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Error grabbing scale: " + e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    @Operation(summary = "Get Store Config Information", description = "Return information about where store is pointing to")
    @GetMapping(path = "/service", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getServices() {

        return new ResponseEntity<>(serviceRepo.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Set store service URL/Service", description = "Allows the user to create a temporary store server endpoint")
    @PostMapping(path = "/service/store-server", produces = {MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> setStoreConfig(@RequestParam String storeServerURL) {
        OperationStatusModel result = new OperationStatusModel("Set Store Server Config");

        if(storeServerURL == null) {
            log.error("No Store Server URL found!");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("URL cannot be null!");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try{
            URL url = new URL(storeServerURL);
            url.toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            log.error("Invalid Headquarters URL provided! " + storeServerURL);

            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Invalid URL provided!");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        List<Service> storeServerServices = serviceRepo.findByDatabaseIdAndServiceType(AppConfig.DefaultHqDatabaseId, ServiceType.STORE_SERVER);

        Service storeService = null;
        if(storeServerServices.size() == 0){
            log.info("setStoreConfig: No store server record found, making a temporary");
            storeService = new Service(AppConfig.DefaultHqDatabaseId, ServiceType.STORE_SERVER, "Temporary",
                    storeServerURL, "Unknown", "Unknown");
        }
        else{
            if(storeServerServices.size() > 1){
                log.warn("WARNING: Multiple store server services found! Defaulting to first service");
            }
            storeService = storeServerServices.get(0);
        }

        storeService.setUrl(storeServerURL);

        try {
            serviceRepo.save(storeService);
        } catch (Exception e) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Could not set new store config: " + e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("HQ Store Endpoint set successfully to: " + storeServerURL);
        log.info("Store Config set successfully!\n    URL: {}",storeServerURL);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Performs a manual heartbeat", description = "Allows the user to force the store to perform an additional heartbeat")
    @PostMapping(path = "/heartbeat", produces = {MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> performManualHeartbeat(@RequestParam(required = false) Boolean clearData) {
        OperationStatusModel result = new OperationStatusModel("Perform Heartbeat");
        if(clearData == null){
            clearData = false;
        }
        try{
            result = storeHeartbeat.handleHearbeat(HeartbeatType.MANUAL, clearData);
        }
        catch (Exception e){
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Could not perform heartbeat! " + e.getMessage());
            log.error("Could not perform heartbeat! " + e.getMessage());
        }
        if(result.getResult().equals(RequestOperationResult.ERROR.name())){
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Sets the interval for store heartbeats", description = "Allows the user to change the frequency of the store heartbeat")
    @PostMapping(path = "/reportInterval", produces = {MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<OperationStatusModel> changeReportInterval(@RequestParam Integer newInterval) {
        OperationStatusModel result = new OperationStatusModel("Set Store Config");
        ConfigEntry current_interval = configRepo.findById(AppConfig.storeHbInterval).orElse(new ConfigEntry(AppConfig.storeHbInterval, "5"));

        switch (newInterval){
            case 1:
            case 5:
            case 10:
            case 30:
            case 60:
            case 120:
                current_interval.setCoValue(newInterval.toString());
                break;
            default:
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Invalid interval request, interval options include: 1,5,10,30,60,120");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            configRepo.save(current_interval);
            storeHeartbeat.restartCron();
        } catch (Exception e) {
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Could not set new store heartbeat interval: " + e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Store Heartbeat interval set successfully!");
        log.info("Store Config set successfully!\n    Now sending heartbeats every: {} minutes",current_interval.getCoValue());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Operation(summary = "Get one or all departments.", description = "If an Id is provided it will return the data for that corresponding department, otherwise it will return a list of all the available departments.")
    @Parameters({
            @Parameter(name = "id", description = "Identificator of the department (Optional).", in = ParameterIn.QUERY, schema = @Schema(implementation = String.class)) })
    @GetMapping(path = "/dept", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getDepartment(@RequestParam(name = "id", required = false) String id) {
        if (id == null) {
            Iterable<Department> findAll = deptRepo.findAll();
            return new ResponseEntity<>(findAll, HttpStatus.OK);
        }

        Department one_dept = deptRepo.findById(id).get();
        return new ResponseEntity<>(one_dept, HttpStatus.OK);
    }

    // ------------
    // Profiles
    // ------------
    @Operation(summary = "Returns the information of the given profile from the database.", description = "Returns all the information related to the the given profile Id.")
    @GetMapping(path = "/profile", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> getProfile(@RequestParam(name = "profileId", required = false) String id) {
        if (id == null) {
            Iterable<Profile> findAll = profileRepo.findAll();
            return new ResponseEntity<>(findAll, HttpStatus.OK);
        }

        Profile profile = profileRepo.findById(id).orElse(null);
        return new ResponseEntity<>(profile, HttpStatus.OK);
    }
}
