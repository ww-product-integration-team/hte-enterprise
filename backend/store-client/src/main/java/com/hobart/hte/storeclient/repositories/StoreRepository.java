package com.hobart.hte.storeclient.repositories;

import com.hobart.hte.utils.store.Store;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StoreRepository extends CrudRepository<Store, String> {
	List<Store> findByRegionId(String regionId);

	Boolean existsByStoreNameIgnoreCase(String name);

	Store findByStoreIdAndRegionId(String storeId, String regionId);
}
