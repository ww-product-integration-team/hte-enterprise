package com.hobart.hte.storeclient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EntityScan(basePackages = {"com.hobart.hte.utils"})
public class HteStoreClientApplication extends SpringBootServletInitializer {
    private static final Logger log = LogManager.getLogger(HteStoreClientApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(HteStoreClientApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HteStoreClientApplication.class);
    }
}