package com.hobart.hte.storeclient.repositories;

import com.hobart.hte.utils.profile.Profile;
import org.springframework.data.repository.CrudRepository;

public interface ProfileRepository extends CrudRepository<Profile, String> {
	boolean existsByProfileIdIgnoreCase(String profileId);

	boolean existsByNameIgnoreCase(String name);

	Profile findByNameIgnoreCase(String name);
}
