package com.hobart.hte.storeclient.scheduledtasks;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.hobart.hte.storeclient.repositories.*;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.config.ConfigSyncEvent;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.dept.DepartmentEvent;
import com.hobart.hte.utils.device.DeviceEvent;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.event.DeviceStatusEntry;
import com.hobart.hte.utils.event.LogEvent;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.heartbeat.StoreHeartbeatRequest;
import com.hobart.hte.utils.heartbeat.StoreHeartbeatResponse;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.model.*;
import com.hobart.hte.utils.profile.ProfileEvent;
import com.hobart.hte.utils.profile.ProfileResponse;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceEvent;
import com.hobart.hte.utils.service.ServiceType;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.store.StoreEvent;

import com.hobart.hte.utils.util.HteTools;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import javax.annotation.PostConstruct;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class StoreHeartbeat {

    private static final Logger log = LogManager.getLogger(StoreHeartbeat.class);
    private static String REPOSITORY_PATH = null;
    private String interval = "0 */5 * * * *";
    private int maxDelay = 0;
    private ScheduledFuture<?> currentTask = null;

    @Autowired
    private Environment environment;

    @Autowired
    StoreRepository storeRepo;

    @Autowired
    DepartmentRepository deptRepo;

    @Autowired
    ProfileRepository profileRepo;

    @Autowired
    DeviceRepository deviceRepo;

    @Autowired
    FileRepository fileRepo;

    @Autowired
    ConfigAppRepository configRepo;

    @Autowired
    DeviceStatusRepository deviceLogRepo;

    @Autowired
    ServiceRepository serviceRepo;

    @Autowired
    ThreadPoolTaskSchedulerConfig taskScheduler;

    @PostConstruct
    public void init() {
        log.info("==== STARTUP HEARTBEAT ===== ");
        try{
            syncLocalFiles();
            handleHearbeat(HeartbeatType.WAS_OFFLINE, false);
        }
        catch (Exception e){
            log.error("ERROR - sending startup heartbeat " + e.getMessage());
        }
        REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
                .orElse(new ConfigEntry("", "")).getCoValue();
        restartCron();
    }

    class HeartbeatTask implements Runnable{

        public HeartbeatTask(){
        }

        @Override
        public void run() {
            Random rand = new Random(); //instance of random class
            int delaySeconds = rand.nextInt(maxDelay);
            log.debug("TIME DELAY is : " + delaySeconds + " interval is: " + interval);

            try{
                TimeUnit.SECONDS.sleep(delaySeconds);
                syncLocalFiles();
                handleHearbeat(HeartbeatType.INTERVAL, false);
            }
            catch (Exception e){
                log.error("ERROR running scheduled exception! " + e.getMessage());
            }
        }
    }

    //Save the response code from the heartbeat to determine how to handle it.
    AtomicReference<HttpStatus> responseCode = new AtomicReference<>(HttpStatus.ACCEPTED);
    AtomicReference<String> fileDownloadURL = new AtomicReference<>(null);
    AtomicReference<List<ProfileResponse>> fileEvents = new AtomicReference<>(new ArrayList<>());

    public StoreHeartbeatRequest generateRequest(HeartbeatType type, boolean deleteData){
        Iterable<Store> stores = storeRepo.findAll();

        String httpMethod = configRepo.findById(AppConfig.httpMethod)
                .orElse(new ConfigEntry(AppConfig.httpMethod, "http"))
                .getCoValue();
        if(!httpMethod.equals("https")){ //If http method is not https then it must be http
            httpMethod = "http";
        }

        String hostname, port, ipAddress, url = null;
        try {
            //Get The hostname
            hostname = InetAddress.getLocalHost().getHostName();
            //Grab the port or retrieve the default if one could not be found
            port = environment.getProperty("local.server.port");
            if(port == null){
                log.info("Port not found, Defaulting to port default port");
                port = configRepo.findById(AppConfig.defaultPort)
                        .orElse(new ConfigEntry(AppConfig.defaultPort, "8080"))
                        .getCoValue();
                try{ //Verify the config is a valid integer i.e. valid port number
                    Integer.parseInt(port);
                }
                catch (Exception ex){
                    log.error("Default Port configuration is not valid! Current value : {}", port);
                    port = "8080";
                }
            }
            //Grab the store's IP address
            ipAddress = HttpUtils.getIPAddress();
            url = httpMethod + "://" + ipAddress + ":" + port;

        }
        catch (Exception e){
            log.error("ERROR Retrieving Network information! " + e.getMessage());
            return null;
        }

        ConfigEntry databaseId = configRepo.findById(AppConfig.databaseId).orElse(null);
        if(databaseId == null || databaseId.getCoValue().isEmpty() || databaseId.getCoValue() == null){
            log.error("No Valid Database ID found!");
            return null;
        }
        if(type == HeartbeatType.INITIAL || !stores.iterator().hasNext() || storeRepo.existsById(HeartbeatType.INITIAL.name())){ //INITIAL
            log.info("INITIAL START : No Store information in database");

            Store initialStore = new Store();
            initialStore.setIpAddress(ipAddress);
            initialStore.setHostname(hostname);
            initialStore.setTriggerType("INITIAL");
            initialStore.setUrl(url);
            initialStore.setServices(serviceRepo.findByDatabaseId(databaseId.getCoValue()));
            initialStore.setLastRebootTimestampUtc(new Timestamp(System.currentTimeMillis()));
            initialStore.setLastCommunicationTimestampUtc(new Timestamp(System.currentTimeMillis()));
            initialStore.setLastItemChangeReceivedTimestampUtc(null);
            initialStore.setLastFetchPluOffsetTimestampUtc(null);

            //Save the store with temp storeId
            initialStore.setStoreId(HeartbeatType.INITIAL.name());
            storeRepo.save(initialStore);

            StoreHeartbeatRequest request = initialStore.toStoreHeartbeat();
            request.setStoreId(null);
            return request;
        }
        //TODO make cases for MANUAL and WAS_OFFLINE heartbeats
        else{ //INTERVAL, WAS_OFFLINE, MANUAL anything else
            Store store = stores.iterator().next();
            store.setTriggerType(type.name());
            store.setLastCommunicationTimestampUtc(new Timestamp(System.currentTimeMillis()));

            if(type == HeartbeatType.WAS_OFFLINE){
                store.setLastRebootTimestampUtc(new Timestamp(System.currentTimeMillis()));
            }

            store.setIpAddress(ipAddress);
            store.setHostname(hostname);
            store.setUrl(url);
            store.setServices(serviceRepo.findByDatabaseId(databaseId.getCoValue()));
            try {
                storeRepo.save(store);
            } catch (Exception ex) {
                log.error("ERROR Saving interval store update: " + ex.getMessage());
                return null;
            }
            StoreHeartbeatRequest request = store.toStoreHeartbeat();
            request.setDeleteEvent(deleteData);
            Iterable<FileForEvent> dbFiles = fileRepo.findAll();
            ArrayList<FileForEvent> files = new ArrayList<>();
            for(FileForEvent currentFile : dbFiles){
                //Create a hashmap to check if files have been deleted on HQ server
                files.add(currentFile);
            }
            request.setFiles(files);
            List<DeviceStatusEntry> logs = deviceLogRepo.findByLogStatus(SyncStatus.NOT_SYNCED);
            if(!logs.isEmpty()) {
                LogEvent newLogEvent = new LogEvent();
                newLogEvent.setLogs(logs);
                ArrayList<StoreEvent> eventLogs = new ArrayList<>();
                eventLogs.add(newLogEvent);
                request.setEventLogs(eventLogs);


                //Send updated scale device
                ArrayList<ScaleDevice> updatedDevices = new ArrayList<>();
                ArrayList<String> addedDevices = new ArrayList<>();
                for(DeviceStatusEntry log : logs){
                    ScaleDevice device = deviceRepo.findById(log.getDeviceUuid()).orElse(null);
                    if(device != null && !addedDevices.contains(device.getDeviceId())){
                        updatedDevices.add(device);
                        addedDevices.add(device.getDeviceId());
                    }
                }
                request.setLastHeartbeat(updatedDevices);
            }
            return request;
        }
    }

    private boolean handleHeartbeatResponse(String rspBody, HttpStatus rspStatus, Boolean clearData){
        log.info(rspStatus.toString());
        fileEvents.set(null);
        if(rspStatus.is2xxSuccessful()){
            try {
                StoreHeartbeatResponse rsp = new ObjectMapper().readValue(rspBody, StoreHeartbeatResponse.class);

                if(clearData){
                    try{
                        log.info("Clearing store database before heartbeat data");
                        deviceRepo.deleteAll();
                        deptRepo.deleteAll();
                        profileRepo.deleteAll();
                        fileRepo.deleteAll();
                    }
                    catch (Exception e){
                        log.error("ERROR - clearing database data " + e.getMessage());
                    }

                }


                updateStore(rsp.toStore());
                for(StoreEvent event: rsp.getEventLogs()){
                    if(event instanceof DeviceEvent){updateStoreDevices((DeviceEvent) event);}
                    else if (event instanceof DepartmentEvent){updateStoreDepts((DepartmentEvent) event);}
                    else if (event instanceof ProfileEvent){updateStoreProfiles((ProfileEvent) event);}
                    else if (event instanceof LogEvent){updateLogs((LogEvent) event);}
                    else if (event instanceof ConfigSyncEvent){updateConfig((ConfigSyncEvent) event);}
                    else if (event instanceof ServiceEvent){updateServices((ServiceEvent) event);}
                    else{log.error("WARNING - unhandled event type : " + event.toString());}
                }

            } catch (JsonProcessingException e) {
                log.error("Error processing server response {}: " + e.getMessage(), rspStatus.toString());
                return false;
            }
            return true;
        }
        else if (rspStatus.is4xxClientError()){
            try {
                OperationStatusModel tmp = new ObjectMapper().readValue(rspBody, OperationStatusModel.class);
                log.info(tmp.toString());
            } catch (JsonProcessingException e) {
                log.error("Error processing server response {}: " + e.getMessage(), rspStatus.toString());
            }
            return false;
        }
        else if (rspStatus.is5xxServerError()){
            log.error("SERVICE REQUEST FAILED: Please check if requested server is operational");
            return false;
        }
        else{
            log.error("Unhandled heartbeat response {}", rspStatus.toString());
            return false;
        }
    }

    public void updateServices(ServiceEvent serviceEvent){
        log.info("SERVICE EVENT");

        if(serviceEvent.getClearServices()){
            //Clear all HQ Services on table
            serviceRepo.deleteAllByDatabaseId(AppConfig.DefaultHqDatabaseId);
        }

        try{
            serviceRepo.saveAll(serviceEvent.getServices());
        }
        catch (Exception e){
            log.error("Could not save available Remote Services!");
        }
    }

    public void updateConfig(ConfigSyncEvent hqConfigs){
        log.info("CONFIG EVENT");

        for(ConfigEntry config : hqConfigs.getConfigs()){
            try{
                configRepo.save(config);
            }
            catch (Exception e){
                log.error("Could not save configuration from HQ: " + e.getMessage());
            }
        }
    }

    public void updateStore(Store storeRsp){
        Iterable<Store> stores = storeRepo.findAll();
        if(stores.iterator().hasNext()){
            //merge(rsp.toStore(), stores.iterator().next());
            try{
                //Delete the initial store after a response is found
                Store initialStore = storeRepo.findById(HeartbeatType.INITIAL.name()).orElse(null);
                if(initialStore != null){

                    initialStore.setServices(new ArrayList<>());
                    storeRepo.save(initialStore);
                    storeRepo.deleteById(HeartbeatType.INITIAL.name());
                    storeRepo.save(storeRsp);
                }
                else{
                    Store mergedStore = HteTools.mergeObjects(storeRsp, stores.iterator().next());
                    storeRepo.save(mergedStore);
                }

            }
            catch (Exception e){
                log.error("Error saving merged store: " + e.getMessage());
            }
        }
        else{
            try{
                storeRepo.save(storeRsp);
            }
            catch (Exception e){
                log.error("Error saving initial merged store: " + e.getMessage());
            }
        }
    }

    public void updateStoreDevices(DeviceEvent event){
        log.info("DEVICE EVENT");
        for(ScaleDevice device : event.getScales()){
            try{
                ScaleDevice currentDevice = deviceRepo.findById(device.getDeviceId()).orElse(null);
                //If the store has a more recent device report timestamp don't overwrite it
                if(currentDevice == null || currentDevice.getLastReportTimestampUtc() == null ||
                        currentDevice.getLastReportTimestampUtc().compareTo(device.getLastReportTimestampUtc()) < 0) {
                    deviceRepo.save(device);
                }
            }
            catch (Exception e){
                log.error("Error saving device {} : {}",device.getIpAddress(), e.getMessage());
            }
        }
    }

    public void updateStoreDepts(DepartmentEvent event){
        log.info("DEPT EVENT");
        for(Department dept : event.getDepartments()){
            try{
                deptRepo.save(dept);
            }
            catch (Exception e){
                log.error("Error saving device {} : {}",dept.getDeptName1(), e.getMessage());
            }
        }
    }

    public void updateStoreProfiles(ProfileEvent event){
        log.info("PROFILE EVENT");
        fileEvents.set(new ArrayList<>());
        for(ProfileResponse profileRsp : event.getProfiles()){
            try{

                // validate target path to save file in
                File profileDir = Paths.get(REPOSITORY_PATH, profileRsp.getProfileId()).toFile();
                if (!profileDir.exists()) {
                    if (!profileDir.mkdirs()) {
                        log.error("unable to create profile directory: {}", profileDir.getAbsolutePath());
                        return;
                    } else {
                        log.debug("directory created!");
                    }
                }


                profileRepo.save(profileRsp.toProfile());
                List<ProfileResponse> events = fileEvents.get();
                events.add(profileRsp);
                fileEvents.set(events);
                fileDownloadURL.set(profileRsp.getDownloadUrl());
            }
            catch (Exception e){
                log.error("Error saving profile {} : {}",profileRsp.getName(), e.getMessage());
            }
        }
    }

    private void updateLogs(LogEvent logEvent){
        log.info("LOG EVENT");
        for(DeviceStatusEntry entry : logEvent.getLogs()){
            log.info("Log Synced: " + entry.getTimestamp() + " " + entry.getDeviceUuid());

            try{
                //Since Millis get stripped in transit, delete any entries with similar timestamps and the same deviceID
                deviceLogRepo.deleteExistingEntry(entry.getTimestamp(), entry.getDeviceUuid());
                deviceLogRepo.save(entry);
            }
            catch (Exception e){
                log.error("Could not save DeviceStatusEntry after sync : \n" + entry.toString());
            }
        }
    }

    public String downloadFiles(String downloadURL, List<FileForEvent> filesToDownload, String profileName){
        String downloadStatus = "";

        final String trustStorePath = configRepo.findById(AppConfig.caCertsPath)
                                                .orElse(new ConfigEntry("", "/etc/ssl/certs/java/cacerts"))
                                                .getCoValue();
        final String trustStorePassword = configRepo.findById(AppConfig.caCertsPassword)
                                                    .orElse(new ConfigEntry("", "changeit"))
                                                    .getCoValue();
        final KeyStore trustStore = createKeyStore(trustStorePath, trustStorePassword);

        WebClient fileWebClient = null;
        try {
            final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);

            final SslContext sslContext = SslContextBuilder.forClient()
                    .trustManager(trustManagerFactory)
                    .build();

            final HttpClient httpClient = HttpClient.create()
                    .secure(ssl -> {
                        ssl.sslContext(sslContext);
                    });

            fileWebClient = WebClient.builder()
                    .clientConnector(new ReactorClientHttpConnector(httpClient))
                    .baseUrl(downloadURL)
                    .exchangeStrategies(ExchangeStrategies.builder()
                            .codecs(configurer ->
                                    configurer.defaultCodecs()
                                            .maxInMemorySize(2 * 1024)
                            )
                            .build())
                    .build();
        }
        catch(Exception ex){
            log.error("Error building fileWebClient: " + ex.getMessage());
            return "SSL Error could not download files: " + ex.getMessage();
        }



        if(filesToDownload.isEmpty()){
            log.debug("Nothing new to download for Profile: " + profileName);
        }
        else{
            log.info("Starting Download(s) for Profile: " + profileName + " from: " + downloadURL);
        }
        for(FileForEvent file:filesToDownload){
            try {
                log.info("Filename: " + file.getFilename() + " id:"+file.getFileId());
                Flux<DataBuffer> dataBuffer = fileWebClient
                        .get()
                        .uri("?id="+file.getFileId())
                        .retrieve()
                        .bodyToFlux(DataBuffer.class)
                        .timeout(Duration.ofMillis(10000));

                Path path = Paths.get(REPOSITORY_PATH, profileName, file.getFilename());
                DataBufferUtils.write(dataBuffer, path, StandardOpenOption.CREATE).share().block();
                downloadStatus += "Downloaded File : " + file.getFilename() + "\n";
                fileRepo.save(file);
            }
            catch (WebClientResponseException e){
                log.error("Could not download file " + e.getMessage());
                FileForEvent oldFile = fileRepo.findById(file.getFileId()).orElse(null);
                if(oldFile != null){
                    //Since the download didn't complete, erase the checksum to try again the next attempt
                    oldFile.setChecksum("");
                    fileRepo.save(oldFile);
                }
                downloadStatus += "ERROR Downloading File : " + file.getFilename() + "\n";
            }

        }

        fileEvents.set(new ArrayList<>());
        return downloadStatus;
    }

    private String checkFileEvents(){
        String fileStatus = "";

        if(fileEvents.get() == null){
            log.info("CheckFileEvents: Heartbeat failed, no files to update");
            fileStatus = "CheckFileEvents: Heartbeat failed, no files to update";
            return fileStatus;
        }

        List<FileForEvent> filesToDownload = new java.util.ArrayList<>(Collections.emptyList());
        //Current files on the database
        Iterable<FileForEvent> dbFiles = fileRepo.findAll();
        HashMap<String, String> currentFiles = new HashMap<>();
        for(FileForEvent currentFile : dbFiles){
            //Create a hashmap to check if files have been deleted on HQ server
            currentFiles.put(currentFile.getFileId(), currentFile.getChecksum());
        }

        for(ProfileResponse response : fileEvents.get()){
            filesToDownload = new ArrayList<>();
            //For each profile check each of the included files checksums
            for(FileForEvent file : response.getListOfFiles()){
                //FileForEvent existingFile = fileRepo.findById(file.getFileId()).orElse(null);
                String existingFileChkSum = currentFiles.get(file.getFileId());
                //If there isn't an entry for the file or the checksums differ
                if(existingFileChkSum == null || !existingFileChkSum.equals(file.getChecksum())){
                    filesToDownload.add(file);
                }
                //Update the hashmap with all the files sent, indicating that this file is currently in the profile
                //and doesn't need to be deleted from the store's current repository
                currentFiles.put(file.getFileId(), "Saved");
            }
            //Downlaod all the new files for the profile
            fileStatus += downloadFiles(fileDownloadURL.get(), filesToDownload, response.getProfileId());
        }

        //Clean up the database with all of the
        for(String fileId : currentFiles.keySet()){
            if(!currentFiles.get(fileId).equals("Saved")){
                log.info("Found an outdated file no longer included in the current profile!");
                log.info("Deleting file: " + fileId);
                try{
                    fileRepo.deleteById(fileId);
                }
                catch (Exception e){
                    log.error("ERROR Something went wrong deleting file: " + e.getMessage());
                }
            }
        }
        return fileStatus;
    }

    public void restartCron(){
        getScheduledInterval(); //Update with the new interval
        CronTrigger cronInterval = new CronTrigger(interval);
        log.info("Attempting to restart heartbeat task");

        if(currentTask == null){
            currentTask = taskScheduler.threadPoolTaskScheduler().schedule(new HeartbeatTask(), cronInterval);
        }
        else{
            boolean success = currentTask.cancel(true);

            if(currentTask.isDone() && success){
                currentTask = taskScheduler.threadPoolTaskScheduler().schedule(new HeartbeatTask(), cronInterval);
            }
            else{
                log.error("Could not set new Heartbeat task!!");
            }
        }




    }

    private void getScheduledInterval(){
        String dbInterval = configRepo.findById(AppConfig.storeHbInterval).orElse(new ConfigEntry("","5")).getCoValue();
        if(dbInterval == null){dbInterval = "5";}
        switch (dbInterval){
            case "1":
                interval = "0 */1 * * * *";
                maxDelay = 1;
                break;
            case "10":
                interval = "0 */10 * * * *";
                maxDelay = 10;
                break;
            case "30":
                interval = "0 */30 * * * *";
                maxDelay = 30;
                break;
            case "60":
                interval = "0 0 * * * *";
                maxDelay = 60;
                break;
            case "120":
                interval = "0 0 */2 * * *";
                maxDelay = 120;
                break;
            default:
                interval = "0 */5 * * * *";
                maxDelay = 5;
                break;
        }
        //Seconds between HB - the max time it takes for a HB
        maxDelay = (maxDelay *60) - 15;

    }


    private Mono<String> createHeartbeat(StoreHeartbeatRequest request)
    {
        List<Service> storeServerServices = serviceRepo.findByDatabaseIdAndServiceType(AppConfig.DefaultHqDatabaseId, ServiceType.STORE_SERVER);
        if(storeServerServices.size() == 0){
            log.error("ERROR: No store server record found!");
            return null;
        }
        if(storeServerServices.size() > 1){
            log.warn("WARNING: Multiple store server services found! Defaulting to first service");
        }

        String storeServerURL = storeServerServices.get(0).getUrl();
        if(storeServerURL == null) {
            log.error("No Valid URL found!");
            return null;
        }

        try{
            URL url = new URL(storeServerURL);
            url.toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            log.error("Invalid Headquarters URL provided! " + storeServerURL);
            return null;
        }

        final String trustStorePath = configRepo.findById(AppConfig.caCertsPath)
                .orElse(new ConfigEntry("", "/etc/ssl/certs/java/cacerts"))
                .getCoValue();
        final String trustStorePassword = configRepo.findById(AppConfig.caCertsPassword)
                .orElse(new ConfigEntry("", "changeit"))
                .getCoValue();
        final KeyStore trustStore = createKeyStore(trustStorePath, trustStorePassword);

        try{
            final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);

            final SslContext sslContext = SslContextBuilder.forClient()
                    .trustManager(trustManagerFactory)
                    .build();

            final HttpClient httpClient = HttpClient.create()
                    .secure(ssl -> {
                        ssl.sslContext(sslContext);
                    });


            WebClient webClient = WebClient.builder()
                    .clientConnector(new ReactorClientHttpConnector(httpClient))
                    .baseUrl(storeServerURL)
                    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            return webClient.post()
                    .uri("/heartbeat/")
                    .body(Mono.just(request), StoreHeartbeatRequest.class)
                    .retrieve()
                    .onStatus(
                            HttpStatus::is1xxInformational,
                            clientResponse -> {
                                responseCode.set(clientResponse.statusCode());
                                log.error("Informational Server Response");
                                return Mono.empty();
                            }
                    )
                    .onStatus(
                            HttpStatus::is2xxSuccessful,
                            clientResponse -> {
                                responseCode.set(clientResponse.statusCode());
                                return Mono.empty();
                            }
                    )
                    .onStatus(
                            HttpStatus::is3xxRedirection,
                            clientResponse -> {
                                responseCode.set(clientResponse.statusCode());
                                log.error("Unexpected redirection response");
                                return Mono.empty();
                            }
                    )
                    .onStatus(HttpStatus::isError,
                            clientResponse -> {
                                responseCode.set(clientResponse.statusCode());
                                //log.error("SERVICE REQUEST FAILED: Please check if requested server is operational");
                                return Mono.empty();
                            }
                    )
                    .bodyToMono(String.class)
                    .timeout(Duration.ofMillis(9000))
                    .doOnError(error -> {
                        fileEvents.set(null);
                        log.error("Error in Heartbeat: {}", error.getMessage());
                    });
        } catch (Exception e){
            log.error("Could not perform Heartbeat request : " + e.getMessage());
            return null;
        }

    }

    private static KeyStore createKeyStore(final String keyStoreLocation, final String keyStorePassword) {

        try (FileInputStream fis = new FileInputStream(keyStoreLocation)) {

            final KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());

            ks.load(fis, keyStorePassword.toCharArray());

            return ks;

        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //Make sure that we have all of the files listed in our DB
    private void syncLocalFiles(){
        Iterable<FileForEvent> files = fileRepo.findAll();
        HashMap<String, Boolean> activeFiles = new HashMap<>();

        //Delete Older Folders/Files that are no longer in the database
        //=================================================================
        //Create hashmap of all files in database
        //identifying files by profile/filename combination
        for (FileForEvent tmp : files) {
            activeFiles.put(tmp.getProfileId() + "/" + tmp.getFilename(), true);
        }
        log.debug("======= Syncing local files and database ==========");
        File[] contents = new File(REPOSITORY_PATH).listFiles();
        if (contents != null) {
            for (File f : contents) {
                if (!Files.isSymbolicLink(f.toPath())) {
                    if(f.isDirectory()){
                        File[] profileContents = new File(REPOSITORY_PATH + f.getName()).listFiles();
                        if (profileContents != null) {
                            for (File p : profileContents) {
                                if (!Files.isSymbolicLink(p.toPath()) && p.isFile()) {
                                    if(activeFiles.get(f.getName() + "/" + p.getName()) == null){
                                        try{
                                            boolean delSuccess = p.delete();
                                            if(!delSuccess){throw new IOException();}
                                            log.info("Deleted hanging file : " + p.getName());
                                        }
                                        catch (Exception e){
                                            log.error("Could not delete file : " + p.getName());
                                        }
                                    }
                                }
                            }

                            //Update the list of files in profile folder
                            profileContents = new File(REPOSITORY_PATH + f.getName()).listFiles();
                            if(profileContents != null && profileContents.length == 0){
                                //Delete the profile folder if it's empty
                                File profileDir = new File(REPOSITORY_PATH + f.getName());
                                try{
                                    boolean delSuccess = profileDir.delete();
                                    if(!delSuccess){throw new IOException();}
                                    log.info("Deleted empty profile folder : " + profileDir.getName());
                                }
                                catch (Exception e){
                                    log.error("Could not delete profile folder : " + profileDir.getName());
                                }
                            }
                        }
                    }
                    else{
                        log.error("ERROR - Found file at directory level in store repository! Leaving alone for now...");
                    }
                }
            }
        }

        //If we are missing a file that the database says we have, we need to remove it from the database, so that the next
        //heartbeat will pick it up
        //=================================================================
        for (FileForEvent dbFile : files) {
            try {
                File file = new File(REPOSITORY_PATH + dbFile.getProfileId() + "/" + dbFile.getFilename());
                if (!file.exists()) {
                    log.info("Found a missing file: " + dbFile.getFilename() + ", removing from db list of downloaded files");
                    fileRepo.deleteById(dbFile.getFileId());
                }
            } catch (Exception e) {
                log.error("Error syncing local files : " + e.getMessage());
            }
        }

        log.debug("======= Finished Syncing ==========");
    }

    public OperationStatusModel handleHearbeat(HeartbeatType type, Boolean clearSavedData) throws InterruptedException{
        OperationStatusModel result = new OperationStatusModel("Send Store Heartbeat");
        log.info("======================================= ");
        StoreHeartbeatRequest request = generateRequest(type, clearSavedData);
        if(request == null){
            log.error("Something went wrong could not generate request");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Something went wrong could not generate request");
            return result;
        }
        Mono<String> heartbeat = createHeartbeat(request);
        if(heartbeat == null){
            log.error("Something went wrong could not create heartbeat");
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Something went wrong could not create heartbeat");
            return result;
        }
        AtomicReference<Boolean> hbRspStatus = new AtomicReference<>(false);
        heartbeat.subscribe(response -> {
            hbRspStatus.set(handleHeartbeatResponse(response, responseCode.get(), clearSavedData));
        });

        /*TODO downloads won't work directly chained to heartbeat request
        need to find a better method than waiting an arbitrary amount of time for the hearbeat to end
        * */
        TimeUnit.SECONDS.sleep(10);
        if(!hbRspStatus.get()){
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Could not complete heartbeat!");
            return result;
        }
        String downloadStatus = checkFileEvents();
        result.setResult(RequestOperationResult.SUCCESS.name());
        result.setErrorDescription("Download Status: \n" + downloadStatus);
        return result;
    }

}