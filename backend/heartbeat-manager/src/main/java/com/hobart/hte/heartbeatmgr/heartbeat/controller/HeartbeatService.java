package com.hobart.hte.heartbeatmgr.heartbeat.controller;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import javax.servlet.http.HttpServletRequest;

import com.hobart.hte.heartbeatmgr.config.AboutService;
import com.hobart.hte.heartbeatmgr.filemgr.repositories.ProfileRepository;
import com.hobart.hte.heartbeatmgr.heartbeat.repositories.*;
import com.hobart.hte.heartbeatmgr.helpers.NodeHelper;
import com.hobart.hte.heartbeatmgr.log.repository.ConfigAppRepository;
import com.hobart.hte.utils.banner.Banner;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.device.ScaleSettings;
import com.hobart.hte.utils.event.EventConfig;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.file.HobFiles;
import com.hobart.hte.utils.heartbeat.*;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.region.Region;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceType;
import com.hobart.hte.utils.store.Store;
import com.hobart.hte.utils.tree.ScaleNode;
import com.hobart.hte.utils.tree.StoreDeptPair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.hobart.hte.heartbeatmgr.filemgr.repositories.FileRepository;
import com.hobart.hte.heartbeatmgr.log.LogService;

@RestController
@RequestMapping("/heartbeat")
public class HeartbeatService {

	private static final Logger log = LogManager.getLogger(HeartbeatService.class);

	@Autowired
	private Environment environment;
	@Autowired
	DeviceRepository device_repo;
	@Autowired
	FileRepository file_repo;
	@Autowired
	LogService logService;
	@Autowired
	ScaleSettingsRepository SSRepo;
	@Autowired
	BannerRepository banner_repo;
	@Autowired
	RegionRepository region_repo;
	@Autowired
	StoreRepository storeRepo;
	@Autowired
	DepartmentRepository dept_repo;
	@Autowired
	ConfigAppRepository configRepo;
	@Autowired
	ProfileRepository profileRepo;
	@Autowired
	AutoAssignRepository assignRepo;
	@Autowired
	NodeStatusRepository nodeStatusRepo;
	@Autowired
	DomainHelper domainHelper;
	@Autowired
	AboutService aboutService;
	@Autowired
	StoreDeptPairsRepository storeDeptPairsRepository;

	private boolean scaleInSync = false;

	@PostMapping(path = "/", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ScaleHeartbeatResponse receiveScaleHeartbeat(@RequestBody ScaleHeartbeatRequest hbr, HttpServletRequest request)
			throws ResponseStatusException {

		log.info("--- Start <3 ---");
		LocalDateTime startHeartbeat = LocalDateTime.now();
		if (request != null) {
			log.debug("client IP address: {}", request.getRemoteAddr());
			log.debug("client request uri: {}", request.getRequestURI());
			log.debug("client remote host: {}", request.getRemoteHost());
			log.debug("client request url: {}", request.getRequestURL());
		}
		log.debug("Request content length: {}", request.getContentLength());

		log.debug("--Heartbeat request <3 ---");
		log.debug(hbr.toString());
		log.debug("-----\n");

		if (hbr.getFiles() == null) {
			hbr.setFiles(new ArrayList<>());
		}

		// Validate scale info data received
		if (hbr.getScaleInfo() == null) {
			log.error("Missing all scale info section");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Missing scale serial number field, this section is required");
		}
		if ((hbr.getIpAddress() == null || hbr.getIpAddress().isEmpty()) && isValidIP(hbr.getIpAddress())) {
			log.info("The IP address is missing or incorrect, this is a required field");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"The IP address is missing or incorrect, this is a required field");
		}

		if(!HttpUtils.isValidInet4Address(hbr.getIpAddress())){
			log.info("Invalid IP address supplied, currently only supporting IPv4 addresses");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Invalid IP address supplied, currently only supporting IPv4 addresses");
		}

		ScaleDevice scaleDevice = findScaleDevice(hbr);

		if(scaleDevice.getProfileId() == null){
			scaleDevice.setProfileId(AppConfig.DefaultProfileId);
		}

		logService.logEvent(EventConfig.DeviceHeartbeat, scaleDevice.getDeviceId(), EntityType.SCALE,
				scaleDevice.getIpAddress() + " with hostname: " + scaleDevice.getHostname() + " Reported");

		//Set scale store to unassigned if the scale has null
		if(scaleDevice.getStoreId() == null || scaleDevice.getDeptId() == null){
			scaleDevice.setStoreId(AppConfig.DefaultUnassignedId);
			scaleDevice.setDeptId(AppConfig.DefaultUnassignedId);
		}


		String operationMode = configRepo.findById(AppConfig.heartbeatOperation).orElse(new ConfigEntry("", "HQ")).getCoValue();
		if(operationMode == null){operationMode = "HQ";}
		if(operationMode.equals("STORE")){
			//If operating as Store server, check to make sure store is enabled
			Iterable<Store> stores = storeRepo.findAll();
			Store store = stores.iterator().next();
			if(store == null || !store.getEnabled()){
				log.info("WARNING : Store is not enabled, rejecting download requests");
				throw new ResponseStatusException(HttpStatus.NOT_FOUND,
						"Store is not enabled");
			}
		}

		ScaleHeartbeatResponse resp = new ScaleHeartbeatResponse(hbr.getMessageUuid(), scaleDevice);

		/*
		Find the respective names for the IDs (profile, store, and department) in the response object
		and convert them to their user-facing names for the app hook database.

		Bit of a ghastly way to do this -- long-term, maybe we should just construct this object with the names and
		not the IDs, plus it's a misnomer since these aren't "IDs" anymore -- but on the other hand:
			a. I don't see the need to uproot who-knows-how-many lines of backend and app hook code at present
			b. Until this writing, these values haven't been used/evaluated by the app hook at all
			c. To an operator or a technician looking at the config tool GUI, these IDs would be utter gibberish
			d. Also, as of this writing, this is the only utilization of this object in the whole project
		*/
		resp.setScaleInfoProfile(device_repo.findScaleProfileNameById(scaleDevice.getProfileId(), scaleDevice.getDeviceId()));
		resp.setScaleInfoStore(device_repo.findScaleStoreNameById(resp.getScaleInfo().getStoreId(), scaleDevice.getDeviceId()));
		resp.setScaleInfoDept(device_repo.findScaleDeptNameById(resp.getScaleInfo().getDeptId(), scaleDevice.getDeviceId()));

		resp.setDeviceUuid(scaleDevice.getDeviceId());

		//Only send events to the device if it enabled
		if (scaleDevice.isEnabled()) {
			//Get Files that belong to the device's profile
			//===================================================================
			if (scaleDevice.getProfileId() != null && !scaleDevice.getProfileId().isEmpty()) {
				log.info("retrieving files for: scale {}, profile {}", scaleDevice.getIpAddress(),
						scaleDevice.getProfileId());
				if(operationMode.equals("STORE") && (scaleDevice.getProfileId().equals(AppConfig.DefaultProfileId))){
					log.info("Scale is reporting to the incorrect store server and should not receive download events from store server");
				}else{
					ScaleEvent ret_files = retrieveFilesforProfile(scaleDevice, hbr.generateFileMap());
					if(!scaleInSync)
						resp.addEvent(ret_files);
				}
			}

			//Add a delete event if applicable
			//===================================================================
			if(shouldSendDelete(scaleDevice.getProfileId())){
				ConfigEntry toDelete = configRepo.findById(AppConfig.scaleDeleteItems).orElse(new ConfigEntry("", ""));
				Profile scaleProfile = profileRepo.findById(scaleDevice.getProfileId()).orElse(null);
				if(scaleProfile != null && scaleProfile.getDeleteSyncConfig() != null){
					//If Profile has its own sync config
					String[] config = scaleProfile.getDeleteSyncConfig().split(" ");
					if(config.length >= 3) {
						String[] types = IntStream.range(3, config.length)
								.mapToObj(i -> config[i])
								.toArray(String[]::new);
						StringBuilder sb = new StringBuilder();
						for (Object obj : types)
							sb.append(obj.toString()).append(" ");

						toDelete.setCoValue(sb.toString());
					}
					else{
						log.error("ERROR - invalid delete config on profile {}", scaleProfile.getName());
					}
				}

				log.info("Sending delete command! Deleting: {}", toDelete.getCoValue());
				resp.addEvent(new DeleteEvent(toDelete, ""));
			}

			//Add Store information
			//======================================================================
			ScaleEvent storeInfoEvent = retrieveStoreInformation(scaleDevice);
			if(storeInfoEvent != null){
				resp.addEvent(storeInfoEvent);
			}

		} else {
			log.info("scale is disabled and won't receive any update events");
		}

		scaleDevice.setLastReportTimestampUtc(new Timestamp(System.currentTimeMillis()));
		// update a sync status field based on the differences found and log the discrepancy

		if (hbr.getScaleSettings() != null) {
			ScaleSettings ss = new ScaleSettings(scaleDevice.getDeviceId(), hbr);
			//setScaleTimeSync(ss);
			ss.setCurrentFiles(hbr.getFiles());
			//log.info("scale settings object to save:  [{}]",ss.getIpAddress());

			//Move to Pending if the profile has been changed
			ScaleSettings currentScaleSettings = SSRepo.findById(scaleDevice.getDeviceId()).orElse(null);
			if(currentScaleSettings != null && (currentScaleSettings.getCurrentFileMap().get("profileChanged") != null
					|| currentScaleSettings.getCurrentFileMap().get("newProfile") != null )){
				ss.addCurrentFilesItem("pending");
			}

			SSRepo.save(ss);
		}

		device_repo.save(scaleDevice);
		try {
			ScaleNode scaleNode = new ScaleNode(scaleDevice);
			NodeHelper nodeHelper = new NodeHelper(banner_repo, region_repo, storeRepo, dept_repo, device_repo, SSRepo, file_repo, nodeStatusRepo);
			nodeHelper.updateNodeStatus(scaleNode);
		} catch (Exception e){
			log.debug("Could not update scale node status" + e.getMessage());
		}

		logDeviceStatus(hbr.getDeviceUuid(), scaleInSync, shouldSendDelete(scaleDevice.getProfileId()));

		LocalDateTime endHeartbeat = LocalDateTime.now();
		log.info("Returning response...");
		log.info("--= Done <3 =--\n");
		log.info("Heartbeat processing time: {}", ChronoUnit.MILLIS.between(startHeartbeat, endHeartbeat));

		return resp;
	}

	/**
	 * Search if the scale is defined in the database, otherwise create a default
	 * configuration
	 * 
	 * @param hbr The heartbeat request received from the scale
	 * @return
	 */
	private ScaleDevice findScaleDevice(ScaleHeartbeatRequest hbr) {
		String device_uuid;
		String heartbeatAddress = hbr.getIpAddress();
		String heartbeatId = hbr.getDeviceUuid();

		String ipSubnet = getSubnetFromIpv4Address(heartbeatAddress);
		Integer ipHost = getHostFromIpv4Address(heartbeatAddress);

		ScaleDevice device = null;
		if(hbr.getDeviceUuid() != null){
			//If the scale already has an ID see if it is registered under that ID first
			device = device_repo.findById(hbr.getDeviceUuid()).orElse(null);
		}

		if(device == null){
			//If the scale doesn't have a UUID or the UUID couldn't be found check to see if we have the scale registered under an IP address
			//i.e. a user may have accidentally cleared the scales UUID
			try {
				device = device_repo.findByIpAddress(heartbeatAddress);
			}
			catch (Exception e) {
				log.debug("An error occurred while trying to find device by IP address:");
				log.debug(e);
				deleteDuplicateAssets(heartbeatAddress);

				device = device_repo.findByIpAddress(heartbeatAddress);
			}
		}

		if (device == null) {
			// -----------------------------------
			// Not in DB, completely new device.
			// We'll first attempt to see if it maps to anything in our "assignment logic table", but otherwise--
			// -- Keep the scale in the "lobby" --
			// and it will stay there until a user assigns the scale to the proper location or
			// receives default configurations
			// -----------------------------------

			// generate a new UUID for the device
			if (hbr.getDeviceUuid() != null && isValidUUID(hbr.getDeviceUuid())) {
				/*If the scale already has a UUID but we don't have a record for it (someone may have deleted the
				 scaleDevice entry), use the scale's UUID to prevent a mismatch since a scale will not overwrite
				 its ID once it's been given
				 */
				device_uuid = hbr.getDeviceUuid();
				logService.logEvent(EventConfig.NewDeviceLogged, device_uuid, EntityType.SCALE,
						"A new device reported for the first time, using device's existing ID.");
			}
			else {
				device_uuid = UUID.randomUUID().toString();
				logService.logEvent(EventConfig.NewDeviceLogged, device_uuid, EntityType.SCALE,
						"A new device reported for the first time, generated a unique id for device.");
			}

			// Code that runs if a scale does *not* exist in the database yet
			try {
			List<String> assignmentMatch = Arrays.asList(assignRepo.findMatchingSubnet(ipSubnet, ipHost).split(","));

			if (!assignmentMatch.isEmpty()) {
				validateStructure(assignmentMatch); // We probably shouldn't try this *all* the time...
			}

			String matchedStoreId = assignRepo.findMatchingStore(assignmentMatch.get(3));
			String matchedDeptId = assignRepo.findMatchingDept(assignmentMatch.get(4));
			String matchedProfileId = assignRepo.findMatchingProfile(assignmentMatch.get(4));

			device = new ScaleDevice(device_uuid, hbr.getScaleInfo());
			device.setIpAddress(hbr.getIpAddress());

			if (!matchedStoreId.isEmpty()
					&& !matchedDeptId.isEmpty()
					&& !assignmentMatch.isEmpty()
					&& isValidUUID(matchedProfileId)
					&& assignRepo.isEnabled(ipSubnet)) {

				device.setStoreId(matchedStoreId);
				device.setDeptId(matchedDeptId);
				device.setCountryCode("");
				device.setProfileId(matchedProfileId);
				log.debug("A new UUID [{}] was matched to the logic table and assigned to the new scale.", device_uuid);}

			} catch (NullPointerException nullPointer) {
				log.debug("Null pointer! No matching assignment rule identified. Making default-configured scale.");
				device = new ScaleDevice(device_uuid, hbr.getScaleInfo());

				device.setIpAddress(heartbeatAddress);
				device.setStoreId(AppConfig.DefaultUnassignedId);
				device.setDeptId(AppConfig.DefaultUnassignedId);
				device.setCountryCode("");
				device.setProfileId(AppConfig.DefaultProfileId);
				// ToDo: validate if the UUID is not assigned already
				log.debug("A new UUID [{}] was assigned to the new default-config scale.", device_uuid);
			}
		}

		// Code that runs if the scale *does* exist in the database
		else {
			device_uuid = hbr.getDeviceUuid();
			log.debug("Scale found with uuid: {}", device_uuid);
			log.debug("Reviewing scale {} against logic table...", device.getHostname());

			try {
				List<String> assignmentMatch = Arrays.asList(assignRepo.findMatchingSubnet(ipSubnet, ipHost).split(","));

				String matchedStoreId = assignRepo.findMatchingStore(assignmentMatch.get(3));
				String matchedDeptId = assignRepo.findMatchingDept(assignmentMatch.get(4));
				String matchedProfileId = assignRepo.findMatchingProfile(assignmentMatch.get(4));

				// Simplified this a little bit as of 10/14/2024. Previously, this only checked for unassigned scales.
				// Instead, we'll just evaluate the assigned store; that way, we still capture unassigned scales, *and*
				// we'll catch a potential edge case wherein a scale is somehow in the wrong store, which would be...
				// highly problematic.
				if (assignRepo.isEnabled(ipSubnet) && !device.getStoreId().equals(matchedStoreId)) {
					log.debug("Match for existing scale {} found: {}", device.getHostname(), assignmentMatch);

					// Investigate the current asset structure per the rules; recreate assets as needed
					validateStructure(assignmentMatch);

					device.setStoreId(matchedStoreId);
					device.setDeptId(matchedDeptId);
					device.setProfileId(matchedProfileId);
				}
				// This catch is mostly for the sake of assignmentMatch returning null
			} catch (NullPointerException npe) {
				log.debug("No auto-assignment match found for scale {}", device.getHostname());
			}
		}
        log.debug("Device: {}", device);
		hbr.getScaleInfo().setIsPrimary(hbr.getScaleInfo().getScaleSynchType());

		configureScaleFromHeartbeat(device, hbr);

		// Handle any "duplicate" scales that may exist in the asset tree.
		// Costco uncovered this, ostensibly from removing the hook from scales and reapplying the hook.
		// This will generate a new UUID for the scale, which will then cause it to report as though it were a new unit.
		if (multipleAssetsUsingThisAddress(heartbeatAddress)) {
			deleteDuplicateAssets(heartbeatAddress);
		}

		return device;
	}

	private void configureScaleFromHeartbeat(ScaleDevice device, ScaleHeartbeatRequest hbr) {
		log.debug("Setting up scale...");
		device.setHostname(hbr.getHostname());
		device.setIpAddress(hbr.getIpAddress());
		device.setIsPrimaryScale(hbr.getScaleInfo().getIsPrimary());

		device.setSerialNumber(hbr.getScaleInfo().getSerialNumber());
		device.setScaleModel(hbr.getScaleInfo().getScaleModel());
		device.setApplication(hbr.getScaleInfo().getApplication());
		device.setBuildNumber(hbr.getScaleInfo().getBuildNumber());
		device.setLoader(hbr.getScaleInfo().getLoader());
		device.setOperatingSystem(hbr.getScaleInfo().getOperatingSystem());
		device.setSystemController(hbr.getScaleInfo().getSystemController());
		device.setSmBackend(hbr.getScaleInfo().getSmBackendVersion());

		device.setLabelStockSize(hbr.getScaleSettings().getLabelStockSize());
		device.setWeigherPrecision(hbr.getScaleSettings().getWeigherPrecision());
		device.setLastTriggerType(hbr.getTriggerType());
		device.setPluCount(hbr.getHealthCheck().getPluRecordCount());
		device.setTotalLabelsPrinted(hbr.getHealthCheck().getTotalLabelsPrinted());
		device.setHealthInfoLog(hbr.getScaleInfo().gethealthInfoLog());

		log.debug("Fully configured device: " + device);
	}

	/**
	 * Determines whether there are multiple assets sharing an IP address.
	 * @param ipAddress The IP address of the scale(s) to assess.
	 * @return True if there is more than one scale at this address; false otherwise.
	 */
	private boolean multipleAssetsUsingThisAddress(String ipAddress) {
		int totalRules = SSRepo.getTotalScalesForAddress(ipAddress);
        return totalRules > 1;
	}

	/**
	 * Discerns which asset reported most recently, given an IPv4 address, and deletes any assets that reported earlier.
	 * @param ipAddress The IP address of the scale to assess.
	 */
	void deleteDuplicateAssets(String ipAddress) {
		// Figure out the "maximum" -- most recent -- heartbeat timestamp for all assets at this address.
		Timestamp mostRecentCommTime = device_repo.mostRecentCommunicationByIPAddress(ipAddress);

		List<ScaleDevice> allScalesUnderThisAddress = device_repo.findScalesByIpAddress(ipAddress);
		int scaleTotal = allScalesUnderThisAddress.size();
		boolean alreadyFoundLatest = false;

		// KILL the imposters -- and their ScaleSettings too.
		// Doing this with a traditional loop to avoid ConcurrentModificationException (Java sucks).
		for (int i = 0; i < scaleTotal; i++) {
			ScaleDevice candidateDevice = allScalesUnderThisAddress.get(i);
			ScaleSettings candidateDeviceSettings = SSRepo.findByDeviceId(candidateDevice.getDeviceId());
			boolean mostRecentComm = mostRecentCommTime.equals(candidateDevice.getLastReportTimestampUtc());

			if (!mostRecentComm || alreadyFoundLatest) {
				device_repo.delete(candidateDevice);
				SSRepo.delete(candidateDeviceSettings);
			}
			else {
				// We found at least one entry with the latest timestamp.
				// Just for safety, flag this occurrence and eliminate any other objects with matching times.
				alreadyFoundLatest = true;
			}
		}
	}

	/**
	 * Validates if the given string is an IP address or not.
	 * 
	 * @param txt The string to validate
	 * @return True if this is a valid IP address, False otherwise
	 */
	private boolean isValidIP(String txt) {
		return java.util.regex.Pattern.matches("([0-2]?[0-9]?[0-9]\\.){3}?([0-2]?[0-9]?[0-9])", txt);
	}

	// A method used exclusively by the auto-assignment/heartbeat logic to inspect the current asset structure--
	// and recreate missing banners, regions, stores, and departments as needed
	private void validateStructure(List<String> rulesData) {
		String bannerDomain = "", regionDomain = "", storeDomain = "", deptDomain = "";

		String newBannerID = UUID.randomUUID().toString();
		String newRegionID = UUID.randomUUID().toString();
		String newStoreID = UUID.randomUUID().toString();
		String newDeptID = UUID.randomUUID().toString();
		String newProfileID = UUID.randomUUID().toString();

		final String rulesBanner = rulesData.get(5);
		final String rulesRegion = rulesData.get(6);
		final String rulesStore = rulesData.get(3);
		final String rulesDept = rulesData.get(4);

		if (!banner_repo.existsByBannerNameIgnoreCase(rulesBanner)) {
			Banner newBanner = new Banner();
			newBanner.setBannerId(newBannerID);
			newBanner.setBannerName(rulesBanner);

			bannerDomain = newBanner.getBannerId();

			try {
				domainHelper.saveDomain(newBanner.getBannerId(), EntityType.BANNER, AppConfig.DefaultAdminId);
				banner_repo.save(newBanner);
			} catch (Exception ex) {
				log.debug("error while adding banner: {}", ex.getMessage());
			}
		}

		if (!region_repo.existsByRegionNameIgnoreCase(rulesRegion)) {
			Region newRegion = new Region();
			newRegion.setRegionId(newRegionID);
			newRegion.setRegionName(rulesRegion);
			newRegion.setBannerId(bannerDomain);

			regionDomain = newRegion.getRegionId();

			// Course-correct if we have a null/empty banner domain
			if (newRegion.getBannerId() != null && !newRegion.getBannerId().isEmpty()) {
				log.debug("Banner validated for " + rulesRegion);
			} else {
				newRegion.setBannerId(banner_repo.findBannerDomainFromBannerName(rulesBanner));
			}

			// Save the object to the DB
			try {
				domainHelper.saveDomain(newRegion.getRegionId(), EntityType.REGION, newRegion.getBannerId());
				region_repo.save(newRegion);
				log.debug("adding new region: {}", newRegion.getRegionId());
			} catch (Exception ex) {
				log.debug("error while creating/updating region: {}", ex.getMessage());
			}
		}

		if (!storeRepo.existsByStoreNameIgnoreCase(rulesStore)) {
			Store newStore = new Store();
			newStore.setStoreId(newStoreID);
			newStore.setAddress("");
			newStore.setCustomerStoreNumber("");
			newStore.setStoreName(rulesStore);
			newStore.setRegionId(regionDomain);

			storeDomain = newStore.getStoreId();

			// Course-correct if we have a null/empty region domain
			if (newStore.getRegionId() != null && !newStore.getRegionId().isEmpty()) {
				newStore.setRegionId(regionDomain);
				log.debug("Region validated for " + rulesStore);
			} else {
				newStore.setRegionId(region_repo.findRegionDomainFromRegionName(rulesRegion));
				log.debug("Region ID was null or empty, created new one: " + newStore.getRegionId());
			}

			// Save the object to the DB
			try {
				domainHelper.saveDomain(newStore.getStoreId(), EntityType.STORE, newStore.getRegionId());
				if (newStore.getEnabled() == null) {newStore.setEnabled(true);}
				storeRepo.save(newStore);
				log.debug("adding new store: {}", newStore.getStoreId());
			} catch (Exception ex) {
				log.debug("error while creating/updating store: {}", ex.getMessage());
			}
		} else {
			Store foundStore = storeRepo.findByStoreNameIgnoreCase(rulesStore);
			storeDomain = foundStore.getStoreId();
		}

		if (!dept_repo.existsByDeptName1IgnoreCase(rulesDept)) {
			Department newDepartment = new Department();
			newDepartment.setdeptId(newDeptID);
			newDepartment.setRequiresDomainId(AppConfig.DefaultAdminId);
			newDepartment.setDeptName1(rulesDept);

			deptDomain = newDepartment.getdeptId();
			try {
				domainHelper.saveDomain(newDepartment.getdeptId(), EntityType.DEPARTMENT, newDepartment.getRequiresDomainId());
				dept_repo.save(newDepartment);
				storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, deptDomain));
				log.debug("adding new department: {}", newDepartment.getdeptId());
			} catch (Exception ex) {
				log.debug("error while creating/updating department: {}", ex.getMessage());
			}
			// check if corresponding profile exists.
			// A matching profile is required for a scale to be auto assigned.
			if(!profileRepo.existsByNameIgnoreCase(rulesDept)){
				Profile p = new Profile(newProfileID, rulesDept, "This was created by autoassign rules.",new Timestamp(System.currentTimeMillis()));
				profileRepo.save(p);
			}
		} else {
			Department dept = dept_repo.findByDeptName1IgnoreCase(rulesDept);
			if (!storeDeptPairsRepository.existsByStoreIdAndDeptId(storeDomain, dept.getdeptId())) {
				storeDeptPairsRepository.save(new StoreDeptPair(storeDomain, dept.getdeptId()));
			}
			if (!profileRepo.existsByNameIgnoreCase(rulesDept)) {
				Profile p = new Profile(newProfileID, rulesDept, "This was created by autoassign rules.",new Timestamp(System.currentTimeMillis()));
				profileRepo.save(p);
			}
		}

	}
	private void setScaleTimeSync(ScaleSettings scaleSettings){

		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy");
		Timestamp now = Timestamp.from(Instant.now());
		try{
			Timestamp scaleTimestamp;
			if (scaleSettings.getTimestamp() == null) {
				scaleTimestamp = scaleSettings.getLastCommunicationTimestampUtc();
			} else {
				Date parsedDate = dateFormat.parse(scaleSettings.getTimestamp());
				scaleTimestamp = new Timestamp(parsedDate.getTime());
			}
			log.debug(Instant.now());
			log.debug(scaleTimestamp);
			long timeDiff = now.getTime() - scaleTimestamp.getTime();
			if(timeDiff < 0)
				timeDiff = timeDiff * -1;
			if(TimeUnit.MILLISECONDS.toHours(timeDiff) > 2){
				log.debug("Scale is out of sync with enterprise");
				scaleInSync = false;
			}
		} catch (Exception e){
			log.error("Unable to parse timestamp");
		}
	}
	/**
	 * 
	 * @param device
	 * @return
	 */
	private ScaleEvent retrieveFilesforProfile(ScaleDevice device, HashMap<String, String> scaleFiles) {
		List<String> service_path = getDownloadUrl(device.getStoreId()); // + "/hte/services/event";
		log.debug("full path to download: {}", service_path);
		DownloadEvent arr_se = new DownloadEvent(service_path.get(0), service_path.get(1), "");

		log.debug("getting files for profile id: {}", device.getProfileId());
		List<FileForEvent> allFilesInProfile = file_repo.findByProfileIdAndEnabled(device.getProfileId(), true);

		Timestamp now = Timestamp.from(Instant.now());
		scaleInSync = true;

		for (FileForEvent ffe : allFilesInProfile) {
			if ((ffe.getStartDate() == null && ffe.getEndDate() == null)
					|| (ffe.getStartDate().before(now) && ffe.getEndDate() == null)
					|| (ffe.getStartDate().before(now) && ffe.getEndDate().after(now))) {
				log.info("file is available to use, file= " + ffe.getFilename());
				arr_se.addFile(new HobFiles(ffe));
			}

			//If the scale doesn't have any files and the profile has files (i.e. we're in the loop, the profile has files)
			//Then the scale is not in sync
			if(scaleFiles == null || scaleFiles.isEmpty()){
				scaleInSync = false;
			}

			if (scaleFiles != null) {
				//Find the corresponding file saved on the scale
				String scaleChksum = scaleFiles.getOrDefault(ffe.getFilename(), "");
				if(scaleChksum.equals(ffe.getChecksum())) {
					//If there is a match remove the match from the map
					scaleFiles.remove(ffe.getFilename());
				}
			}
		}

		//If the scale files is null or it isn't empty
		//Previous loop removes all the matching files/checksums
		//ergo if there are files remaining then there is a mismatch between
		// scale and server and the scale is out of sync
		if(scaleFiles == null || !scaleFiles.isEmpty()){
			scaleInSync = false;
		}

		log.debug("retrieved files in profile: {}, files to sync: {}", allFilesInProfile.size(),
				arr_se.getFiles().size());
		log.debug("is the scale in sync? {}", scaleInSync);
		return arr_se;
	}

	private ScaleEvent retrieveStoreInformation(ScaleDevice device){

		//First we need to determine if the setting is set to true
		ConfigEntry sendStoreInfo = configRepo.findById(AppConfig.sendStoreInfo).orElse(new ConfigEntry(null, "false"));
		String customStoreName = configRepo.findById(AppConfig.customStoreName).orElse(new ConfigEntry(null, null)).getCoValue();

		boolean appSendInfo = sendStoreInfo.getCoValue().equals("true");

		//If the device is unassigned do not send and store information
		if(device.getStoreId().equals(AppConfig.DefaultUnassignedId)){return null;}

		//Next we need to find the Device's store configuration
		Store deviceStore = storeRepo.findById(device.getStoreId()).orElse(null);
		if(deviceStore == null){return null;}

		ScaleConfigEvent scaleConfigEvent = new ScaleConfigEvent("", deviceStore, appSendInfo, parseCustomStoreName(deviceStore, customStoreName));
		if(scaleConfigEvent.getFields().isEmpty()){return null;}

		return scaleConfigEvent;
	}

	private Boolean shouldSendDelete(String profileId){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());

		String interval = null, day = null, time = null;
		Profile scaleProfile = profileRepo.findById(profileId).orElse(null);
		if(scaleProfile != null && scaleProfile.getDeleteSyncConfig() != null){
			//If Profile has it's own sync config
			String[] config = scaleProfile.getDeleteSyncConfig().split(" ");
			if(config.length >= 3) {
				interval = config[0];
				day = config[1];
				time = config[2];
			}
			else{
				log.error("ERROR - invalid delete config on profile {}", scaleProfile.getName());
			}
		}
		else{
			interval = configRepo.findById(AppConfig.scaleDeleteInterval).orElse(new ConfigEntry(null, null)).getCoValue();
			day = configRepo.findById(AppConfig.scaleDeleteDay).orElse(new ConfigEntry(null, null)).getCoValue();
			time = configRepo.findById(AppConfig.scaleDeleteTime).orElse(new ConfigEntry(null, null)).getCoValue();
		}

		if(interval == null || day == null || time == null){
			log.error("ERROR - Could not grab scale delete config");
			return false;
		}

		int dayInt, timeInt;
		try{
			dayInt = Integer.parseInt(day);
			timeInt = Integer.parseInt(time);
		}
		catch (Exception e){
			log.error("ERROR - Invalid delete interval or time configuration");
			return false;
		}
		long nextDeleteTime = new Date().getTime();
		nextDeleteTime -= TimeUnit.MINUTES.toMillis(calendar.get(Calendar.MINUTE) + 1);
		int daysLeft, hoursLeft;
		switch (interval.toLowerCase()){

			case "never":
				return false;
			case "daily":
				if(timeInt < 0 || timeInt > 23){
					log.error("ERROR - invalid time config for delete config: {}", timeInt);
					return false;
				}
				hoursLeft = timeInt - calendar.get(Calendar.HOUR_OF_DAY);
				nextDeleteTime += TimeUnit.HOURS.toMillis(hoursLeft);
				break;
			case "weekly":

				if(timeInt < 0 || timeInt > 23){
					log.error("ERROR - invalid time config for delete config: {}", timeInt);
					return false;
				}

				if(dayInt < 1 || dayInt > 7){
					log.error("ERROR - invalid day config for delete config: {}", dayInt);
					return false;
				}

				daysLeft = dayInt - calendar.get(Calendar.DAY_OF_WEEK);
				nextDeleteTime += TimeUnit.DAYS.toMillis(daysLeft);

				hoursLeft = timeInt - calendar.get(Calendar.HOUR_OF_DAY);
				nextDeleteTime += TimeUnit.HOURS.toMillis(hoursLeft);
				break;
			case "monthly":
				if(timeInt < 0 || timeInt > 23){
					log.error("ERROR - invalid time config for delete config: {}", timeInt);
					return false;
				}

				if(dayInt < 1 || dayInt > 7){
					log.error("ERROR - invalid day config for delete config: {}",dayInt);
					return false;
				}

				daysLeft = dayInt - calendar.get(Calendar.DAY_OF_WEEK);

				//Days here relate to first day of the week of that month i.e. first Wednesday of the month
				//If
				if(daysLeft + calendar.get(Calendar.DAY_OF_MONTH) > 7 || daysLeft < 0){
					//Need to calculate next months occurrence
					daysLeft = YearMonth.now().lengthOfMonth() - calendar.get(Calendar.DAY_OF_MONTH);
					Timestamp bleh = new Timestamp(nextDeleteTime + TimeUnit.DAYS.toMillis(daysLeft));

					//The day of the week of the end of the month 1(Sunday) - 7(Saturday)
					int toAdd, eom = (daysLeft % 7 + calendar.get(Calendar.DAY_OF_WEEK)) % 7;
					//If the day of the week of the end of the month is greater than our desired day
					//Then we need to add the remaining days of the week i.e.
					//If the month ends on a friday(6) and we want to hit the next monday(2) we need to add
					//1[7-friday(6)] day to make up the difference otherwise just adding the dayInt will put us on a tuesday
					if(eom > dayInt){
						toAdd = 7-eom;
					}
					//If the day of the week of the end of the month is less than our desired day
					//Then we need to subtract the remaining days of the week i.e.
					//If the month ends on a tuesday(3) and we want to hit the next wednesday(4) we need to subtract
					//tuesday(3) to make up the difference otherwise just adding the dayInt will over
					else{
						toAdd = -eom;
					}

					daysLeft += dayInt + toAdd;
				}

				nextDeleteTime += TimeUnit.DAYS.toMillis(daysLeft);

				hoursLeft = timeInt - calendar.get(Calendar.HOUR_OF_DAY);
				nextDeleteTime += TimeUnit.HOURS.toMillis(hoursLeft);
				break;
			default:
				log.error("ERROR - Unrecognized delete interval: {}", interval);
				return false;
		}
		long currentTime = new Date().getTime();
		Timestamp start = new Timestamp(nextDeleteTime);
		Timestamp end = new Timestamp(nextDeleteTime + TimeUnit.HOURS.toMillis(1));
		log.debug("Delete Window is {} -> {}", start, end);
		return currentTime > start.getTime() && currentTime < end.getTime();
	}

	/**
	 * 
	 * @return
	 */
	private List<String> getDownloadUrl(String storeId) {
		final String fileMgrEndpoint = "filemgr/download";

		//Add the download URL Of the store server first if it exists
		//============================================================================
		List<String> urlArr = new ArrayList<>();
		Store store = null;
		if(storeId != null){
			store = storeRepo.findById(storeId).orElse(null);
		}

		//If there is a server and it is enabled add it to the first slot on the list
		if(store != null && store.getService(ServiceType.HEARTBEAT) != null && store.getEnabled()){
			urlArr.add(store.getService(ServiceType.HEARTBEAT).getUrl() + fileMgrEndpoint);
		}

		//Add the download URL Of the backup server, this will always be the HQ URL but if we
		//are operating as a store then we need to grab it a different way
		//============================================================================
		String operationMode = configRepo.findById(AppConfig.heartbeatOperation).orElse(new ConfigEntry("", "HQ")).getCoValue();
		//If working as a store need to grab the download endpoint of HQ
		Service hqHbService = null;
		String url = null;
		if(operationMode.equals("STORE")){
			//TODO Need to grab the HQ URL when acting as the store, otherwise this method will return the same URL twice as a store
		}else{
//			hqHbService = aboutService.getServiceInfo(false);
			String httpMethod = configRepo.findById(AppConfig.httpMethod).orElse(new ConfigEntry(AppConfig.httpMethod, "http")).getCoValue();
			String ipAddress = configRepo.findById(AppConfig.fallbackIp).orElse(new ConfigEntry(AppConfig.fallbackIp, AboutService.FALLBACK_IP)).getCoValue();
			String port = configRepo.findById(AppConfig.defaultPort).orElse(new ConfigEntry(AppConfig.defaultPort, "8080")).getCoValue();
			url = httpMethod + "://" + ipAddress + ":" + port + "/scale-service/";
		}

//		if(hqHbService != null){
//			urlArr.add(hqHbService.getUrl() + fileMgrEndpoint);
		if (url != null) {
			urlArr.add(url + fileMgrEndpoint);
		}
		else{
			urlArr.add("");
		}

		//Finally if there is only one url in the list add an empty for the second
		//i.e. there is no backup download.
		if(urlArr.size() < 2){
			urlArr.add("");
		}

		return urlArr;
	}

	public static boolean isValidUUID(String uuid){
		return uuid.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
		//prev implementation "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
	}

	private String parseCustomStoreName(Store store, String customConfig){
		/*Used to parse out the custom configuration set by the application
		 * String is the same except you can insert custom store variables with the '$' character
		 *
		 * ex: $storeName #$storeId Located in \n $address \n Hello World
		 */
		if(customConfig == null){return null;}

		try{
			String repStoreName = customConfig.replace("$storeName", store.getStoreName());
			String repStoreId = repStoreName.replace("$storeId", store.getStoreId());
			return repStoreId.replace("$address", store.getAddress());
		}
		catch (Exception ex){
			log.error("Error Parsing out custom store Name configuration ");
			return null;
		}
	}

	/**
	 * 
	 * @return
	 */
	@GetMapping("/test")
	public String testService() {
		log.info("the test endpoint has been invoked!");

		return ("Scale-service is online!");
	}

	private void logDeviceStatus(String uuid, boolean inSync, boolean sentDelete) {
		logService.logDeviceStatus(uuid, inSync, sentDelete);
	}

	private String getSubnetFromIpv4Address(String address) {
		return address.substring(0, address.lastIndexOf("."));
	}

	private Integer getHostFromIpv4Address(String address) {
		return Integer.valueOf(address.substring(address.lastIndexOf(".")).replace(".", ""));
	}
}
