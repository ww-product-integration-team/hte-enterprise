package com.hobart.hte.heartbeatmgr.log.repository;

import com.hobart.hte.utils.config.ConfigEntry;
import org.springframework.data.repository.CrudRepository;

public interface ConfigAppRepository extends CrudRepository<ConfigEntry, String> {

}
