package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.access.HTeDomain;
import org.springframework.data.repository.CrudRepository;

public interface DomainRepository extends CrudRepository<HTeDomain, String> {
    HTeDomain findByDomainId(String id);
}
