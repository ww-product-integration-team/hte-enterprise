package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.autoAssign.AutoAssign;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AutoAssignRepository extends CrudRepository<AutoAssign, String>  {

    Boolean existsByIpAddressStart(String ip_start);
    Boolean existsByIpAddressEnd(String ip_end);

    @Query(value = "SELECT SUBSTRING_INDEX(ipAddressStart, \".\", 3) as subnet\n" +
            "    ,ipAddressStart\n" +
            "    ,ipAddressEnd\n" +
            "    ,store\n" +
            "    ,dept\n" +
            "    ,banner\n" +
            "    ,region\n" +
            "FROM autoAssignmentRules\n" +
            "WHERE ipAddressStart REGEXP :ipSubnet\n" +
            "AND :ipHost >= CAST(SUBSTRING_INDEX(ipAddressStart, \".\", -1) AS UNSIGNED)\n" +
            "AND :ipHost <= CAST(SUBSTRING_INDEX(ipAddressEnd, \".\", -1) AS UNSIGNED)\n" +
            "AND enabled = 1 LIMIT 1",
            nativeQuery = true)
    String findMatchingSubnet(@Param("ipSubnet") String ipSubnet, @Param("ipHost") Integer ipHost);

    @Query(value = "SELECT enabled\n" +
            "FROM autoAssignmentRules\n" +
            "WHERE ipAddressStart REGEXP :ipSubnet\n" +
            "LIMIT 1", nativeQuery = true)
    Boolean isEnabled(@Param("ipSubnet") String ipSubnet);

    @Query(value = "SELECT storeID \n" +
            "FROM store\n" +
            "WHERE storeName = :storeName\n" +
            "LIMIT 1", nativeQuery = true)
    String findMatchingStore(@Param("storeName") String storeName);

    @Query(value = "SELECT deptId\n" +
            "FROM depts\n" +
            "WHERE deptName1 = :deptName\n" +
            "LIMIT 1", nativeQuery = true)
    String findMatchingDept(@Param("deptName") String deptName);

    @Query(value = "SELECT profileID\n" +
            "FROM profile\n" +
            "WHERE name = :profileName\n" +
            "LIMIT 1", nativeQuery = true)
    String findMatchingProfile(@Param("profileName") String profileName);
}
