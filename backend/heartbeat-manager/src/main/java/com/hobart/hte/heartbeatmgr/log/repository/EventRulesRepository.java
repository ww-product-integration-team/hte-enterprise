package com.hobart.hte.heartbeatmgr.log.repository;

import com.hobart.hte.utils.event.EventRule;
import com.hobart.hte.utils.file.FileForEvent;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;

public interface EventRulesRepository extends CrudRepository<EventRule, String> {
}
