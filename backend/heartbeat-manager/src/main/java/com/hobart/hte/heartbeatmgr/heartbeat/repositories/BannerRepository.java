package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.banner.Banner;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface BannerRepository extends CrudRepository<Banner, String> {
	
    Boolean existsByBannerNameIgnoreCase(String name);

    @Query(value = "SELECT bannerID\n" +
            "FROM banner\n" +
            "WHERE bannerName = :bannerName\n" +
            "LIMIT 1", nativeQuery = true)
    String findBannerDomainFromBannerName(@Param("bannerName") String region);
}
