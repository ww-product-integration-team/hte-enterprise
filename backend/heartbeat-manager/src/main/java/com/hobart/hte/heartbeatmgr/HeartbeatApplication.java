package com.hobart.hte.heartbeatmgr;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.boot.devtools.restart.Restarter;


@SpringBootApplication
@EntityScan(basePackages = {"com.hobart.hte.utils"})
public class HeartbeatApplication extends SpringBootServletInitializer {
	private static final Logger log = LogManager.getLogger(HeartbeatApplication.class);
	private static ConfigurableApplicationContext context;

	public static void main(String[] args) {
		context = SpringApplication.run(HeartbeatApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(HeartbeatApplication.class);
	}
}
