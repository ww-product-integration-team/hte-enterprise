package com.hobart.hte.heartbeatmgr.log.repository;

import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServiceRepository extends CrudRepository<Service, String> {

    List<Service> findByDatabaseIdAndServiceType(String databaseId, ServiceType serviceType);

}