package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.region.Region;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface RegionRepository extends CrudRepository<Region, String>{
	List<Region> findByBannerId(String bannerId);
	Boolean existsByRegionNameIgnoreCase(String name);

	// A query that tries to find a region's parent banner, assuming one exists
	// This is banking on the user not having identically named regions
	// If two or more regions share the same name, this will return nothing due to the aggregation clause
	@Query(value = "SELECT bannerID\n" +
			"FROM region\n" +
			"WHERE regionName = :regionName\n" +
			"GROUP BY bannerID\n" +
			"HAVING COUNT(*) = 1\n", nativeQuery = true)
	String findBannerIDByRegionName(@Param("regionName") String region);

	// Similar idea to the query above
	@Query(value = "SELECT regionID\n" +
			"FROM region\n" +
			"WHERE regionName = :regionName\n" +
			"LIMIT 1", nativeQuery = true)
	String findRegionDomainFromRegionName(@Param("regionName") String region);
}
