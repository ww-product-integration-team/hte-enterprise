package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.access.HTeUser;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<HTeUser, Long>{

    List<HTeUser> findByIdNot(String id);
    HTeUser findById(String id);
    void deleteById(String id);
    Boolean existsById(String id);

    HTeUser findByUsername(String username);
    Boolean existsByUsername(String username);
}
