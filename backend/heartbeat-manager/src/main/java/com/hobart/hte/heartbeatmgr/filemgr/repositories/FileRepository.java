package com.hobart.hte.heartbeatmgr.filemgr.repositories;

import java.util.List;
import java.util.Optional;

import com.hobart.hte.utils.file.FileForEvent;
import org.springframework.data.repository.CrudRepository;


public interface FileRepository extends CrudRepository<FileForEvent, String> {
	List<FileForEvent> findAllByProfileId(String profileId);
	Optional<FileForEvent> findByFilenameAndProfileId(String filename, String profileId);
	boolean existsByFilenameAndProfileId(String filename, String profileId);
	
	List<FileForEvent> findByProfileIdAndEnabled(String profileId, boolean enabled);
	List<FileForEvent> findByFilename(String filename);
}
