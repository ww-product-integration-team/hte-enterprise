package com.hobart.hte.heartbeatmgr.log;

import com.hobart.hte.heartbeatmgr.log.repository.ConfigAppRepository;
import com.hobart.hte.heartbeatmgr.log.repository.DeviceStatusLogRepo;
import com.hobart.hte.heartbeatmgr.log.repository.EventLogRepository;
import com.hobart.hte.heartbeatmgr.log.repository.EventRulesRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.event.DeviceStatusEntry;
import com.hobart.hte.utils.event.EventImportance;
import com.hobart.hte.utils.event.EventLogEntry;
import com.hobart.hte.utils.event.EventRule;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.model.SyncStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import com.hobart.hte.heartbeatmgr.filemgr.FileController;

@Service
public class LogService {

	private static final Logger log = LogManager.getLogger(LogService.class);

	@Autowired
	EventLogRepository eventLogRepo;

	@Autowired
	DeviceStatusLogRepo deviceStatusRepo;

	@Autowired
	ConfigAppRepository configRepo;

	@Autowired
	EventRulesRepository eventRulesRepo;

	private EventRule findRule(Integer eventId, String entityId){
		//First we need to see if there is a custom rule for this entity and event combo
		EventRule customRule = eventRulesRepo.findById(entityId + "_" + eventId.toString()).orElse(null);
		if(customRule != null){
			return customRule;
		}

		//Next we need to find the generic rule for the given event id
		EventRule generalRule = eventRulesRepo.findById(eventId.toString()).orElse(null);
		if(generalRule == null){
			log.error("Could not find rule for eventID " + eventId);
			//Fall back to logging all
			generalRule = new EventRule(1234, true, true, false);
		}

		return generalRule;
	}

	public boolean logEvent(Pair<Integer, Pair<String, EventImportance>> event, String deviceId, EntityType entityType, String message) {
		EventRule rule = findRule(event.getFirst(), deviceId);
		EventLogEntry deviceLogEntry = new EventLogEntry(event.getFirst(), deviceId, entityType, event.getSecond().getSecond(), rule.getShouldMute(), event.getSecond().getFirst(), message);
		EventLogEntry existingLog = null;
		if(deviceId != null){
			existingLog = eventLogRepo.findLatestEntryofType(deviceId, event.getFirst());
		}

		//If there is no log
		deviceLogEntry.setOccurrences("");

		//Update occurrence if the notification is unread otherwise make a new entry
		if(existingLog != null && !existingLog.getOpened()){
			existingLog.appendOccurrence(deviceLogEntry.getTimestamp());
			deviceLogEntry = existingLog;
		}

		//Clearing older logs in the system
		eventLogRepo.deleteOldEvents();

		if(rule.getShouldLog()){
			try {
				eventLogRepo.save(deviceLogEntry);
			} catch (Exception ex) {
				log.error("error while trying to save in log: {}", ex.getMessage());
				return false;
			}
		}
		else{
			return false;
		}

		return true;
	}

	public boolean logDeviceStatus(String deviceId, boolean inSync, boolean sentDelete) {
		try {
			ConfigEntry operation = configRepo.findById(AppConfig.heartbeatOperation).orElse(new ConfigEntry(AppConfig.heartbeatOperation, "HQ"));
			if(operation.getCoValue().equals("HQ")){
				deviceStatusRepo.save(new DeviceStatusEntry(deviceId, inSync, SyncStatus.NA, sentDelete));
			}
			else{
				deviceStatusRepo.save(new DeviceStatusEntry(deviceId, inSync, SyncStatus.NOT_SYNCED, sentDelete));
			}

			//Clearing older logs in the system
			deviceStatusRepo.deleteOldLogs();

		} catch (Exception ex) {
			log.error("error while trying to save in log: {}", ex.getMessage());
			return false;
		}
		return true;
	}
}
