package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import java.sql.Timestamp;
import java.util.List;

import com.hobart.hte.utils.device.ScaleDevice;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface DeviceRepository extends CrudRepository<ScaleDevice, String> {
	ScaleDevice findByIpAddress(String ipAddress);

	List<ScaleDevice> findByStoreId(String storeId);

	List<ScaleDevice> findByStoreIdAndDeptId(String storeId, String deptId);

	List<ScaleDevice> findByProfileId(String profileId);

	boolean existsByIpAddress(String ipAddress);
/*
	@Query("SELECT new com.hobart.hte.entities.DeviceCountPerFirmware(c.application, COUNT(c.application)) "
			+ "FROM ScaleDevice AS c GROUP BY c.application ORDER BY c.application DESC")

	@Query("SELECT new com.hobart.hte.entities.DeviceCountPerModel(c.scaleModel, COUNT(c.scaleModel)) "
			+ "FROM ScaleDevice AS c GROUP BY c.scaleModel ORDER BY c.scaleModel DESC")
	List<DeviceCountPerModel> countTotalDevicesByModel();
*/
	List<ScaleDevice> findByLastReportTimestampUtcAfter(Timestamp timeago);

	List<ScaleDevice> findByLastReportTimestampUtcBefore(Timestamp timeago);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "update scaleDevice s set s.profileId = :new_profile where s.profileId = :current_profile", nativeQuery = true)
	int updateScaleDeviceSetProfileIdForProfileId(@Param("new_profile") String new_profile,
			@Param("current_profile") String current_profile);

	/*
	@Query("SELECT new com.hobart.hte.entities.DeviceCountPerOnlineStatus(COUNT(c.scaleModel)) "
			+ "FROM ScaleDevice AS c WHERE c.lastReportTimestampUtc BETWEEN :t1 AND :t2 GROUP BY c.scaleModel ORDER BY c.scaleModel DESC")
	List<DeviceCountPerOnlineStatus> countTotalDevicesOnlineStatus(@Param("t1") Timestamp t1,
			@Param("t2") Timestamp t2);
			*/

	@Query("SELECT sd FROM ScaleDevice AS sd LEFT JOIN Store AS s ON s.storeId = sd.storeId LEFT JOIN Region AS r " +
			"ON r.regionId = s.regionId LEFT JOIN Banner AS b ON b.bannerId = r.bannerId WHERE b.bannerId = :bannerId")
	List<ScaleDevice> findByBannerId(@Param("bannerId") String bannerId);

	@Query("SELECT sd FROM ScaleDevice AS sd LEFT JOIN Store AS s ON s.storeId = sd.storeId LEFT JOIN Region AS r " +
			"ON r.regionId = s.regionId WHERE r.regionId = :regionId")
	List<ScaleDevice> findByRegionId(@Param("regionId") String regionId);

	@Query(value = "SELECT p.name FROM profile p INNER JOIN scaleDevice sd ON p.profileId = sd.profileId " +
			"WHERE sd.profileId = :profileId AND sd.deviceId = :deviceId LIMIT 1", nativeQuery = true)
	String findScaleProfileNameById(@Param("profileId") String profileID, @Param("deviceId") String deviceId);

	@Query(value = "SELECT st.storeName FROM store st INNER JOIN scaleDevice sd ON st.storeId = sd.storeId " +
			"WHERE sd.storeID = :storeId AND sd.deviceId = :deviceId LIMIT 1", nativeQuery = true)
	String findScaleStoreNameById(@Param("storeId") String storeID, @Param("deviceId") String deviceId);

	@Query(value = "SELECT d.deptName1 FROM depts d INNER JOIN scaleDevice sd ON d.deptId = sd.deptId " +
			"WHERE sd.deptId = :deptId AND sd.deviceId = :deviceId LIMIT 1", nativeQuery = true)
	String findScaleDeptNameById(@Param("deptId") String deptID, @Param("deviceId") String deviceId);

	@Query(value = "SELECT MAX(lastReportTimestampUTC) " +
			"FROM scaleDevice " +
			"WHERE ipAddress = :ipAddress", nativeQuery = true)
	Timestamp mostRecentCommunicationByIPAddress(@Param("ipAddress") String ipAddress);

	// Only used for cleanup. Don't use this outside of its niche utility in HeartbeatService.
	List<ScaleDevice> findScalesByIpAddress(String ipAddress);
}