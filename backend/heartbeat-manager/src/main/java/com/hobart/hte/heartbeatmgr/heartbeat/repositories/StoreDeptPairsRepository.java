package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.tree.StoreDeptPair;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StoreDeptPairsRepository extends CrudRepository<StoreDeptPair, String>{
    List<StoreDeptPair> findByStoreId(String storeId);
    List<StoreDeptPair> findByDeptId(String deptId);
    Boolean existsByStoreIdAndDeptId(String storeId, String deptId);

    void deleteByStoreId(String storeId);
    @Transactional
    void deleteByStoreIdAndDeptId(String storeId, String deptId);
}
