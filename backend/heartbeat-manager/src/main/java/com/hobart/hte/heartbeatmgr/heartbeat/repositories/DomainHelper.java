package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.heartbeatmgr.filemgr.repositories.ProfileRepository;
import com.hobart.hte.utils.access.HTeDomain;
import com.hobart.hte.utils.access.HTeUser;
import com.hobart.hte.utils.dept.Department;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.profile.Profile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

import static com.hobart.hte.utils.config.AppConfig.*;

@Service
public class DomainHelper {
    private static final Logger log = LogManager.getLogger(DomainHelper.class);

    public DomainHelper() {
    }

    @Autowired
    DomainRepository domain_repo;

    @Autowired
    UserRepository user_repo;

    @Autowired
    DepartmentRepository dept_repo;

    @Autowired
    ProfileRepository profile_repo;


    public static boolean isDefaultUUID(String uuid){
        switch (uuid){
            case DefaultAdminId:
            case DefaultUnassignedId:
            case DefaultProfileId:
            case DefaultAllAccessDomain:
                return true;
            default:
                return false;
        }
    }

    public static boolean isValidUUID(String uuid){
        return uuid.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
        //prev implementation "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
    }

    public boolean hasDomainPermission(String UserDomainID, String entityDomainID){
        //Add override for get requests and others that don't require a domainID

        if(entityDomainID != null && entityDomainID.equals(DefaultAllAccessDomain)){return true;}
        //If user has not been assigned a domain, then they have zero privileges
        if(UserDomainID != null && UserDomainID.equals(DefaultUnassignedId)){return false;}

        HTeDomain entity = domain_repo.findByDomainId(entityDomainID);
        if(entity == null){
            log.error("Could not find UserDomain, domain permission denied:" + entityDomainID);
            return false;
        }
        else{
            if(entity.getDomainId().trim().equals(UserDomainID.trim())){
                return true;
            }
            else{
                return hasDomainPermission(UserDomainID, entity.getParentId());
            }
        }
    }

    public boolean hasChild(String entityDomainId){
        Iterable<HTeDomain> allDomains = domain_repo.findAll();
        for (HTeDomain next : allDomains) {
            if(next.getParentId() == null){return false;}
            if (next.getParentId().equals(entityDomainId)) {
                return true;
            }
        }
        return false;
    }

    //Reassign users who have 'soon to be deleted' domain id to unassigned so that the delete can succeed
    public void safeDelete(String domainId) throws Exception{

        //Make Sure any users attached to this get reassigned
        HTeDomain unassignedDomain = domain_repo.findByDomainId(DefaultUnassignedId);
        if(unassignedDomain == null){throw new IOException("Error deleting domain, Domain Table not properly initialized");}

        Iterable<HTeUser> allUsers = user_repo.findAll();
        for (HTeUser user : allUsers) {
            if(Objects.equals(user.getDomain().getDomainId(), domainId)){
                //If a user has the domain about to be deleted then reassign that domain to unassigned
                log.info("SafeDelete - Reassigning User: " + user.getUsername() + " to unassigned");
                user.setDomain(unassignedDomain);
                user_repo.save(user);
            }
        }

        //Make sure any departments attached to this domain get reassigned
        Iterable<Department> allDepts = dept_repo.findAll();
        for (Department dept: allDepts) {
            if(Objects.equals(dept.getRequiresDomainId(), domainId)){
                //If a user has the domain about to be deleted then reassign that domain to unassigned
                log.info("SafeDelete - Reassigning Department: " + dept.getDeptName1() + " to unassigned");
                dept.setRequiresDomainId(unassignedDomain.getDomainId());
                dept_repo.save(dept);
            }
        }

        //Make sure any profiles attached to this domain get reassigned
        Iterable<Profile> allProfiles = profile_repo.findAll();
        for (Profile profile: allProfiles) {
            if(Objects.equals(profile.getRequiresDomainId(), domainId)){
                //If a user has the domain about to be deleted then reassign that domain to unassigned
                log.info("SafeDelete - Reassigning Profile: " + profile.getName() + " to unassigned");
                profile.setRequiresDomainId(unassignedDomain.getDomainId());
                profile_repo.save(profile);
            }
        }

        //After all users domains have been reset
        try {
            domain_repo.deleteById(domainId);
        }
        catch (Exception ex){
            throw new IOException(ex.getMessage());
        }


    }


    public void saveDomain(String id, EntityType type, String parentid) throws IOException {
        //If actual ID is invalid then throw an error
        if(id == null || !isValidUUID(id) || isDefaultUUID(id)){
            throw new IOException("Invalid Domain ID cannot save");
        }

        //Now need to check if parent ID is valid and exists
        if(parentid != null && isValidUUID(id) && !isDefaultUUID(id) && domain_repo.findByDomainId(parentid) != null){
            domain_repo.save(new HTeDomain(id, type, parentid));
        }
        else{ //If parent ID is invalid for whatever reason then set it to unassigned domain
            throw new IOException("Invalid Parent Domain ID cannot save");
        }
    }

    public boolean isRegisteredHobartAdmin(String username){
        HTeUser user = user_repo.findByUsername(username);
        if(user == null){return false;}

        return user.isAdmin();
    }

}
