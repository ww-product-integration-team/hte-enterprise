package com.hobart.hte.heartbeatmgr.log.repository;

import java.sql.Timestamp;
import java.util.List;

import com.hobart.hte.utils.event.EventLogEntry;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface EventLogRepository extends CrudRepository<EventLogEntry, Timestamp> {

    List<EventLogEntry> findByEntityIdAndEventId(String entityId, Integer eventId);

    //Find the latest event in the log that has the same event id and entity id, will only every return 1 entry or null
    @Transactional
    @Query(value="SELECT e1 FROM EventLogEntry e1 WHERE e1.timestamp = (SELECT MAX(e2.timestamp) FROM EventLogEntry e2 WHERE e2.entityId = :entity_id and e2.eventId = :event_id)")
    EventLogEntry findLatestEntryofType(@Param("entity_id") String entity_id,
                                                  @Param("event_id") Integer event_id);


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "DELETE FROM EventLogEntry entry WHERE entry.timestamp < SUBDATE(CURRENT_DATE, 180)")
    void deleteOldEvents();

}
