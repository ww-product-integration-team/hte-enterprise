package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.store.Store;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface StoreRepository extends CrudRepository<Store, String> {
	List<Store> findByRegionId(String regionId);

	Boolean existsByStoreNameIgnoreCase(String name);

	Store findByStoreNameIgnoreCase(String storeName);

	Store findByStoreIdAndRegionId(String storeId, String regionId);

	Store findByIpAddress(String ipAddress);

	// A query that tries to find a store's parent region, assuming one exists
	// This is banking on the user not having identically named stores
	// If two or more stores share the same name, this will return nothing due to the aggregation clause
	@Query(value = "SELECT regionID\n" +
			"FROM store\n" +
			"WHERE storeName = :storeName\n" +
			"GROUP BY regionID\n" +
			"HAVING COUNT(*) = 1\n" +
			"LIMIT 1", nativeQuery = true)
	String findRegionIDFromStoreName(@Param("storeName") String store);
}
