package com.hobart.hte.heartbeatmgr.config;

import com.hobart.hte.heartbeatmgr.log.repository.ConfigAppRepository;
import com.hobart.hte.heartbeatmgr.log.repository.ServiceRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Service
public class AboutService {

    private static final Logger log = LogManager.getLogger(AboutService.class);

    @Autowired
    private Environment environment;

    @Autowired
    ConfigAppRepository configRepo;

    @Autowired
    ServiceRepository serviceRepo;

    // WINDOWS
    public static final String FALLBACK_IP = "10.3.128.150";
    // LINUX
//    public static final String FALLBACK_IP = "10.3.195.20";

    public Service getServiceInfo(Boolean save) {
        OperationStatusModel result = new OperationStatusModel("GetServiceInfo");

        //RELEASE: "scale-service";
        //DEVELOPMENT: "";
        String endpoint = "scale-service";

        //Grab the database Id, and if one doesn't exist generate one
        ConfigEntry databaseId = configRepo.findById(AppConfig.databaseId).orElse(null);
        if(databaseId == null || databaseId.getCoValue() == null){
            log.info("Writing Database ID, you should only see this message one time ever!");
            databaseId = new ConfigEntry(AppConfig.databaseId, UUID.randomUUID().toString());
            configRepo.save(databaseId);
        }

        ConfigEntry httpTestScaleService = configRepo.findById(AppConfig.httpRequestCheckScaleService).orElse(null);
        if (httpTestScaleService == null || httpTestScaleService.getCoValue() == null) {
            log.info("Writing scale-service endpoint test URL...");
            httpTestScaleService = new ConfigEntry(AppConfig.httpRequestCheckScaleService,
                    "http://localhost:8080/scale-service/heartbeat/test");
            configRepo.save(httpTestScaleService);
        }

        //Grab the operation mode for this HB manager
        String operationMode = configRepo.findById(AppConfig.heartbeatOperation).orElse(new ConfigEntry("", "Unknown")).getCoValue();

        /*
            1. Try to discover the current port
            2. If that fails (Using Tomcat) then use the default port in the config
            3. If that fails default to 8080
         */
        String port = environment.getProperty("local.server.port");
        if(port == null){
            log.info("Port not found, Defaulting to port default port");
            port = configRepo.findById(AppConfig.defaultPort)
                    .orElse(new ConfigEntry(AppConfig.defaultPort, "8080"))
                    .getCoValue();
            try{ //Verify the config is a valid integer i.e. valid port number
                Integer.parseInt(port);
            }
            catch (Exception ex){
                log.error("Default Port configuration is not valid! Current value : {}", port);
                port = "8080";
            }
        }

        //Get the current HTTP method
        String httpMethod = configRepo.findById(AppConfig.httpMethod)
                .orElse(new ConfigEntry(AppConfig.httpMethod, "http"))
                .getCoValue();
        if(!httpMethod.equals("https")){ //If http method is not https then it must be http
            httpMethod = "http";
        }

        // Set fallback ip if it is not already set
        ConfigEntry fallbackIp = configRepo.findById(AppConfig.fallbackIp).orElse(null);
        if (fallbackIp == null || fallbackIp.getCoValue() == null) {
            log.info("Writing fallbackIp. You should only see this message one time ever!");
            fallbackIp = new ConfigEntry(AppConfig.fallbackIp, FALLBACK_IP);
            configRepo.save(fallbackIp);
        }
        //Grab the IP Address
        String ipAddress = "";
        try {
            ipAddress = HttpUtils.getIPAddress();
        } catch (Exception ex) {
            log.error("Error grabbing server IP address: " + ex.getMessage());
            ipAddress = fallbackIp.getCoValue();
        }

        //Compile All networking elements together for final URL
        String url = httpMethod + "://" + ipAddress + ":" + port + "/";
        if(endpoint != ""){
            url += endpoint + "/";
        }

        List<Service> existingService = serviceRepo.findByDatabaseIdAndServiceType(databaseId.getCoValue(), ServiceType.HEARTBEAT);
        Service service = null;
        if(existingService.size() > 0){
            if(existingService.size() > 1){
                log.error("FATAL ERROR: Ambiguous service definitions! Ignoring for now and using the first services id");
            }
            service = existingService.get(0);
            service.setOperationMode(operationMode);
            service.setUrl(url);
            service.setVersion(StartupBean.projectVersion);
            service.setBuildDate(StartupBean.buildDate);
        }
        else{
            log.info("Service definition not found! Writing now...");
            service = new Service(databaseId.getCoValue(), ServiceType.HEARTBEAT,
                    operationMode, url, StartupBean.projectVersion, StartupBean.buildDate);
        }

        service.setLastRestart(new Timestamp(System.currentTimeMillis()));

        if(save){
            try{
                serviceRepo.save(service);
            }
            catch (Exception ex){
                log.error("Could not save service info! " + ex.getMessage());
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Could not save service info! " + ex.getMessage());
                return null;
            }
        }

        return service;
    }
}