package com.hobart.hte.heartbeatmgr.heartbeat.repositories;

import com.hobart.hte.utils.access.HTeUser;
import com.hobart.hte.utils.tree.NodeStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NodeStatusRepository extends CrudRepository<NodeStatus, String>{
    Iterable<NodeStatus> findByParentId(String parentId);
}
