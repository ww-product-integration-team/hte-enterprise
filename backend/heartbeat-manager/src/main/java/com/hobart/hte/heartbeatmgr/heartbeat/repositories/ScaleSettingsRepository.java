package com.hobart.hte.heartbeatmgr.heartbeat.repositories;


import com.hobart.hte.utils.device.ScaleSettings;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;

public interface ScaleSettingsRepository extends CrudRepository<ScaleSettings, String> {

	ScaleSettings findByIpAddress(String id);

	ScaleSettings findByDeviceId(String deviceId);

	@Query(value = "SELECT COUNT(*) FROM scaleSettings\n" +
			"WHERE ipAddress = :ipAddress", nativeQuery = true)
	int getTotalScalesForAddress(@Param("ipAddress") String ipAddress);

}
