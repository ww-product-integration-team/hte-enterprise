package com.hobart.hte.heartbeatmgr.filemgr;

import java.net.MalformedURLException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.hobart.hte.heartbeatmgr.heartbeat.repositories.DeviceRepository;
import com.hobart.hte.heartbeatmgr.heartbeat.repositories.StoreRepository;
import com.hobart.hte.heartbeatmgr.log.LogService;
import com.hobart.hte.heartbeatmgr.log.repository.ConfigAppRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.device.ScaleDevice;
import com.hobart.hte.utils.event.EventConfig;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.model.EntityType;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.store.Store;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import com.hobart.hte.heartbeatmgr.filemgr.repositories.FileRepository;
import com.hobart.hte.heartbeatmgr.filemgr.repositories.ProfileRepository;

@RestController
@RequestMapping("/filemgr")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class FileController {

	private static String REPOSITORY_PATH = null;
//	private static final String UPGRADE_REPOSITORY = REPOSITORY_PATH + File.separator + "upgrades";
	private static final Logger log = LogManager.getLogger(FileController.class);

	@Autowired
	FileRepository fileRepo;
	@Autowired
	ProfileRepository profileRepo;
	@Autowired
	LogService logService;
	@Autowired
	ConfigAppRepository configRepo;
	@Autowired
	StoreRepository storeRepo;
	@Autowired
	DeviceRepository deviceRepo;

	@PostConstruct
	public void init() {
		REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
				.orElse(new ConfigEntry("", "")).getCoValue();
	}

	// download file
	@Operation(summary = "Returns a file from the repository.", description = "Returns the file identified with the given file Id. This entry point is for a scale to download a .ht file containing configuration information.")
	@Parameters({
			@Parameter(name = "id", description = "The Id of the file to download, this is a UUID must exists in the database. Otherwise an error will return.", in = ParameterIn.QUERY, example = "dacfe5bf-ce8d-4621-94e1-f49740530873", schema = @Schema(implementation = String.class) )})
	@GetMapping("/download")
	public ResponseEntity<?> handleFileDownload(@RequestParam(name = "id") String id, HttpServletRequest request) {

		String requestor_ip = HttpUtils.getRequestIP(request);
		System.out.println(requestor_ip);

		ScaleDevice requestingDevice = deviceRepo.findByIpAddress(requestor_ip);
		Store requestingStore = storeRepo.findByIpAddress(requestor_ip);
		if(requestingDevice == null && requestingStore == null){
			log.error("ERROR - can not find requesting device : " + requestor_ip + " refusing download request");
			logService.logEvent(EventConfig.DownloadNoIpError, requestor_ip, EntityType.SCALE,  "Download request from: " + requestor_ip +
					"was rejected because no record could be found with given ip address");
			return new ResponseEntity<>("Can not find requesting device in record", HttpStatus.NOT_FOUND);
		}

		log.debug("handleFileDownload(): ip address from requestor: {}", requestor_ip);

		String operationMode = configRepo.findById(AppConfig.heartbeatOperation).orElse(new ConfigEntry("", "HQ")).getCoValue();
		if(operationMode != null && operationMode.equals("STORE")){
			//If operating as Store server, check to make sure store is enabled
			Iterable<Store> stores = storeRepo.findAll();
			Store store = stores.iterator().next();
			if(store == null){
				log.info("ERROR : Store record was not found");
				logService.logEvent(EventConfig.DownloadNoStoreError, requestor_ip, EntityType.SCALE,
						"No record could be found for the requested store by scale: " + requestor_ip);
				return new ResponseEntity<>("Requested store record was not found", HttpStatus.NOT_FOUND);
			}
			
			if(!store.getEnabled()){
				log.info("WARNING : Store is not enabled, rejecting download requests");
				logService.logEvent(EventConfig.DownloadDisabledStoreError, store.getStoreId(), EntityType.SCALE,
						"Download request from :" + requestor_ip + "was rejected because store is disabled!");
				return new ResponseEntity<>("Store is not enabled", HttpStatus.NOT_FOUND);
			}
		}

		if (fileRepo.existsById(id)) {
			FileForEvent toDownload = fileRepo.findById(id).get();
			Profile profile = profileRepo.findById(toDownload.getProfileId()).orElse(null);
			if(profile == null){
				if(requestingDevice != null){
					logService.logEvent(EventConfig.DownloadNoProfileError, requestingDevice.getDeviceId(), EntityType.SCALE, "Could " +
							"not find corresponding profile for file : " + toDownload.getFilename() + "with profile ID : " +  toDownload.getProfileId());
				}
				else{
					logService.logEvent(EventConfig.DownloadNoProfileError, requestingStore.getStoreId(), EntityType.STORE, "Could " +
							"not find corresponding profile for file : " + toDownload.getFilename() + "with profile ID : " +  toDownload.getProfileId());
				}
				log.error("ERROR - can not find corresponding profile for file : " + toDownload.getFilename() + "with profile ID : " +  toDownload.getProfileId());
				return new ResponseEntity<>("Can not find corresponding profile for file", HttpStatus.NOT_FOUND);
			}
			String subdir = toDownload.getProfileId();
			MediaType mediaType = MediaType.TEXT_PLAIN;
			// String mediaType = "text/plain";
			if (toDownload.getFilename().endsWith(".package.tgz") || toDownload.getFilename().endsWith(".deb")) {
				subdir = "upgrades";
				mediaType = MediaType.APPLICATION_OCTET_STREAM;
				// mediaType = "application/octet-stream";
			}

			Path path = Paths.get(REPOSITORY_PATH, subdir, toDownload.getFilename());

			log.debug("file requested: {}", path.toString()); // .getFileName().toString());
			log.debug("file size: {} bytes", toDownload.getSize());

			if (Files.exists(path)) {
				Resource resource = null;
				try {
					resource = new UrlResource(path.toUri());
				} catch (MalformedURLException e) {
					log.error(e.getMessage());
					return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
				}

				log.debug("The file requested by {} was found, returning [{}], profile id: {}", requestor_ip,
						path.getFileName().toString(), toDownload.getProfileId());
				// logging to DB what file, when it was downloaded and who did it.
				if(requestingDevice != null){
					logService.logEvent(EventConfig.ScaleDownload, requestingDevice.getDeviceId(), EntityType.SCALE, "Scale: " + requestingDevice.getIpAddress() + "downloading file: " + toDownload.getFilename()
							+ ", profileId: " + toDownload.getProfileId());
				}
				else{
					logService.logEvent(EventConfig.StoreDownload, requestingStore.getStoreId(), EntityType.STORE, "Store: " + requestingStore.getIpAddress() + "downloading file: " + toDownload.getFilename()
							+ ", profileId: " + toDownload.getProfileId());
				}

				return ResponseEntity.ok()
						// .contentType(MediaType.parseMediaType(contentType)) .TEXT_PLAIN
						.contentType(mediaType).header(HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename=\"" + resource.getFilename() + "\"")
						.body(resource);
			} else {
				log.info("file not found, nothing returned");
			}
		}

		log.info("The file was not found!");
		return ResponseEntity.notFound().build();
	}
}
