package com.hobart.hte.heartbeatmgr.log.repository;

import java.sql.Timestamp;

import com.hobart.hte.utils.event.DeviceStatusEntry;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;


public interface DeviceStatusLogRepo extends CrudRepository<DeviceStatusEntry, Timestamp> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "DELETE FROM DeviceStatusEntry entry WHERE entry.timestamp < SUBDATE(CURRENT_DATE, 60)")
    void deleteOldLogs();

}
