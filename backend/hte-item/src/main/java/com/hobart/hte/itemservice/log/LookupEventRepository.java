package com.hobart.hte.itemservice.log;

import java.sql.Timestamp;

import com.hobart.hte.utils.event.LookupEventEntry;
import org.springframework.data.repository.CrudRepository;

public interface LookupEventRepository extends CrudRepository<LookupEventEntry, Timestamp> {

}
