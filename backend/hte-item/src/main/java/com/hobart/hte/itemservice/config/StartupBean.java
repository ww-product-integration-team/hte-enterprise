package com.hobart.hte.itemservice.config;

import com.hobart.hte.itemservice.repositories.ConfigAppRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartupBean {

    public static String projectVersion = "2.0.0";
    public static String buildDate = "07/31/2024";
    private static final Logger log = LogManager.getLogger(StartupBean.class);

    @Autowired
    AboutService aboutService;

    @Autowired
    ConfigAppRepository configRepo;
    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {

        //Setup the log location
        ConfigEntry logPath = configRepo.findById(AppConfig.logRootPath).orElse(null);
        if(logPath != null && logPath.getCoValue() != null){
            System.setProperty("APP_LOG_ROOT", logPath.getCoValue());
            ((org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false)).reconfigure();
        }

        aboutService.getServiceInfo(true);

        log.info("┏━━┳┓╋╋╋╋╋╋┏━━┓╋╋╋╋╋╋╋┏┓");
        log.info("┗┃┃┫┗┳━┳━━┓┃━━╋━┳┳┳━┳━╋╋━┳━┓");
        log.info("┏┃┃┫┏┫┻┫┃┃┃┣━━┃┻┫┏┻┓┃┏┫┃━┫┻┫");
        log.info("┗━━┻━┻━┻┻┻┛┗━━┻━┻┛╋┗━┛┗┻━┻━┛");
        log.info(" =============================");
        log.info(" ITW FEG Hobart 2024(c)       (v"+ projectVersion+")");
        log.info(" [Build Date: " + buildDate + "]");
    }
}
