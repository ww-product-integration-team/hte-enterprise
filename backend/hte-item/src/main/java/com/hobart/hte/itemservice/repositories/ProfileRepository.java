package com.hobart.hte.itemservice.repositories;

import com.hobart.hte.utils.profile.Profile;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProfileRepository extends CrudRepository<Profile, String> {
    boolean existsByProfileIdIgnoreCase(String profileId);
    boolean existsByNameIgnoreCase(String name);
    Profile findByNameIgnoreCase(String name);

    @Query(value = "SELECT profileID\n" +
            "FROM profile\n" +
            "WHERE name = :profileName\n" +
            "LIMIT 1;", nativeQuery = true)
    String findProfileIDByProfileName (@Param("profileName") String profileName);

    @Query(value = "SELECT profileRecentlyUpdated\n" +
            "FROM scaleDevice\n" +
            "WHERE ipAddress = :ipAddress", nativeQuery = true)
    Boolean scaleProfileUpdatedStatus (@Param("ipAddress") String ipAddress);
}