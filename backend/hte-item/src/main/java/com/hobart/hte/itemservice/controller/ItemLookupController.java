package com.hobart.hte.itemservice.controller;

import javax.servlet.http.HttpServletRequest;

import com.hobart.hte.itemservice.PluImport.PluImport;
import com.hobart.hte.itemservice.repositories.*;
import com.hobart.hte.utils.event.EventConfig;
import com.hobart.hte.utils.event.EventImportance;
import com.hobart.hte.utils.item.HobItem;
import com.hobart.hte.utils.item.ItemPayload;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.util.HteTools;
import com.jcraft.jsch.ConfigRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hobart.hte.itemservice.log.LogService;

import java.util.List;

@RestController
@RequestMapping("/itemservice")
public class ItemLookupController {

	private static final Logger log = LogManager.getLogger(ItemLookupController.class);

	@Autowired
	ItemService itemservice;
	@Autowired
	LogService logService;
	@Autowired
	ItemRepository itemRepo;
	@Autowired
	ProfileRepository profileRepo;
	@Autowired
	FileRepository fileRepo;
	@Autowired
	ConfigAppRepository configRepo;

	@GetMapping("/test")
	public String getStatus() {
		return "Hobart Item service up and running.";
	}

	/**
	 * 
	 * @param plu     Number of the item to lookup
	 * @param dept    Department number where the item is located (Optional)
	 * @param store   The number of the store the PLU belongs to (Optional)
	 * @param request HTTP headers and extra information
	 * @return
	 */
	@Parameters({
			@Parameter(name = "plu", description = "The product number to lookup, any value in the 1-999999 interval.", in = ParameterIn.QUERY, example = "4011", schema = @Schema(implementation = Integer.class)),
			@Parameter(name = "dept", description = "(Optional) The department the product belongs to", in = ParameterIn.QUERY, example = "0", schema = @Schema(implementation = Integer.class)),
			@Parameter(name = "store", description = "(Optional) The store number the product belongs to", in = ParameterIn.QUERY, example = "0", schema = @Schema(implementation = Integer.class))
	})
	@Operation(summary = "Item lookup", description = "This endpoint is used by scales to retrieve the information from a specific product in the given department/store combination.")
	@GetMapping(path = "/getOneItem", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<?> fetchItem(@RequestParam(name = "plu") Integer plu,
			@RequestParam(name = "dept", required = false) Integer dept,
			@RequestParam(name = "store", required = false) Integer store, HttpServletRequest request) {

		log.info("\n-----------------------------");
		if (request != null) {
			log.debug("client IP address: {}", request.getRemoteAddr());
			log.debug("client request uri: {}", request.getRequestURI());
			log.debug("client remote host: {}", request.getRemoteHost());
			log.debug("client request url: {}", request.getRequestURL());
			logService.logLookupEvent(request.getRemoteAddr(), plu);
		}
		log.info("plu requested: {}, department number: {}, store number: {}", plu, dept, store);

		/*-
		 * ToDo: 
		 *   -   make configurable how the user wants to search for the product. i.e.
		         using department number, or not
		     -   Validate no duplicated data is received from the database. i.e. same item in different stores 
		 */
		ItemPayload payload = itemservice.retrieveItem(plu, dept, store);

		if (payload != null) {
			log.info("plu description: {}", payload.getPrDesc());
			return new ResponseEntity<>(payload, HttpStatus.OK);
		} else {
			log.info("plu not found!");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@Operation(summary="Read JSON to import PLUs", description="Accepts a JSON object containing PLU information")
	@Parameters({
			@Parameter(name="plus", description="JSON format of plus to import")
	})
	@PostMapping(path="/jsonPluImport", consumes={MediaType.APPLICATION_JSON_VALUE}, produces={MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> importPlus(@RequestBody PluImportData jsonData) {
		OperationStatusModel result = new OperationStatusModel("Import PLU JSON");
		log.info("Begin Import PLU JSON");
		logService.logEvent(EventConfig.LoggingEvent, null, null, "Starting async upload of PLU JSON data");

		PluImport pluImport = new PluImport(jsonData, result, itemRepo, profileRepo, fileRepo, configRepo, logService);
		HteTools.startAsyncThread(pluImport);

		/*
		* maybe move validation of plus into springboot
		* make asyncronous DONE
		* make sure fields from word file match with expected inputs DONE-ish
		* need to handle nutrifacts and expanded texts (this one for sure)
		* add entry to eventlog table to say when uploading finished and when starting
		* add more logging throughout DONE
		* add proper filtering for zones and products on products page
		* tell all scales of a profile to heartbeat by port 6000 after assigning new plu file
		* add validation to make sure price is greater than 0 DONE
		* */

		if (result.getResult() == null) {
			log.info("Finish Import PLU JSON");
			result.setResult(RequestOperationResult.SUCCESS.name());
			result.setErrorDescription("Successfully read plu json. Please check logs for any products not saved");
			return new ResponseEntity<>(result, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(result, pluImport.resultStatus);
		}
	}
}
