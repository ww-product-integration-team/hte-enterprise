package com.hobart.hte.itemservice.repositories;

import com.hobart.hte.utils.item.HobItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ItemRepository extends CrudRepository<HobItem, Integer> {
	HobItem findByPrNumAndDeptNum(int prNum, int deptNum);
	HobItem findByPrNum(int prNum);

	@Transactional
	void deleteAllByPrIDNotIn(List<Integer> toNotDelete);
}
