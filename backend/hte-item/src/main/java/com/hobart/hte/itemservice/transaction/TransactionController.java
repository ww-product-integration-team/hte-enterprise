package com.hobart.hte.itemservice.transaction;

import com.hobart.hte.itemservice.repositories.TransRepository;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.transaction.TransEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@RestController
@RequestMapping("/transservice")
@CrossOrigin(origins = "*", allowedHeaders = "*") // added to let react reach the service correctly
public class TransactionController {

    private static final Logger log = LogManager.getLogger(TransactionController.class);

    @Autowired
    TransRepository transRepo;

    @PostMapping(path = "/uploadTransactions", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> fetchItemTransactions(@RequestBody ArrayList<TransEntry> transl,
                                                   HttpServletRequest request) {

        log.info("receiving transaction data...");
        log.info("client connecting is: {}", request.getRemoteAddr());

        try {
            transRepo.saveAll(transl);
            log.info("{} records saved successfully", transl.size());
        } catch (Exception ex) {
            log.info("error while saving transactions: {}", ex.toString());
            OperationStatusModel op = new OperationStatusModel("saving transactions", ex.toString());
            return new ResponseEntity<>(op, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
