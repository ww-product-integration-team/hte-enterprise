package com.hobart.hte.itemservice.config;

import com.hobart.hte.itemservice.repositories.ConfigAppRepository;
import com.hobart.hte.itemservice.repositories.ServiceRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.http.HttpUtils;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.service.Service;
import com.hobart.hte.utils.service.ServiceType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@org.springframework.stereotype.Service
public class AboutService {

    private static final Logger log = LogManager.getLogger(AboutService.class);

    @Autowired
    private Environment environment;

    @Autowired
    ConfigAppRepository configRepo;

    @Autowired
    ServiceRepository serviceRepo;


    public Service getServiceInfo(Boolean save) {
        OperationStatusModel result = new OperationStatusModel("GetServiceInfo");

        //RELEASE: "hte-item";
        //DEVELOPMENT: "";
        String endpoint = "hte-item";

        //Grab the database Id, and if one doesn't exist generate one
        ConfigEntry databaseId = configRepo.findById(AppConfig.databaseId).orElse(null);
        if(databaseId == null || databaseId.getCoValue() == null){
            log.info("Writing Database ID, you should only see this message one time ever!");
            databaseId = new ConfigEntry(AppConfig.databaseId, UUID.randomUUID().toString());
            configRepo.save(databaseId);
        }

        //Grab the operation mode for this HB manager
        String operationMode = "N/A";


        /*
            1. Try to discover the current port
            2. If that fails (Using Tomcat) then use the default port in the config
            3. If that fails default to 8080
         */
        String port = environment.getProperty("local.server.port");
        if(port == null){
            log.info("Port not found, Defaulting to port default port");
            port = configRepo.findById(AppConfig.defaultPort)
                    .orElse(new ConfigEntry(AppConfig.defaultPort, "8080"))
                    .getCoValue();
            try{ //Verify the config is a valid integer i.e. valid port number
                Integer.parseInt(port);
            }
            catch (Exception ex){
                log.error("Default Port configuration is not valid! Current value : {}", port);
                port = "8080";
            }
        }

        //Get the current HTTP method
        String httpMethod = configRepo.findById(AppConfig.httpMethod)
                .orElse(new ConfigEntry(AppConfig.httpMethod, "http"))
                .getCoValue();
        if(!httpMethod.equals("https")){ //If http method is not https then it must be http
            httpMethod = "http";
        }

        //Grab the IP Address
        String ipAddress = "";
        try {
            ipAddress = HttpUtils.getIPAddress();
        } catch (Exception ex) {
            log.error("Error grabbing server IP address: " + ex.getMessage());
            ipAddress = "localhost";
        }

        //Compile All networking elements together for final URL
        String url = httpMethod + "://" + ipAddress + ":" + port + "/";
        if(endpoint != ""){
            url += endpoint + "/";
        }

        List<Service> existingService = serviceRepo.findByDatabaseIdAndServiceType(databaseId.getCoValue(), ServiceType.ITEM_LOOKUP);
        Service service = null;
        if(existingService.size() > 0){
            if(existingService.size() > 1){
                log.error("FATAL ERROR: Ambiguous service definitions! Ignoring for now and using the first services id");
            }
            service = existingService.get(0);
            service.setOperationMode(operationMode);
            service.setUrl(url);
            service.setVersion(StartupBean.projectVersion);
            service.setBuildDate(StartupBean.buildDate);
        }
        else{
            log.info("Service definition not found! Writing now...");
            service = new Service(databaseId.getCoValue(), ServiceType.ITEM_LOOKUP,
                    operationMode, url, StartupBean.projectVersion, StartupBean.buildDate);
        }

        service.setLastRestart(new Timestamp(System.currentTimeMillis()));

        if(save){
            try{
                serviceRepo.save(service);
            }
            catch (Exception ex){
                log.error("Could not save service info! " + ex.getMessage());
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("Could not save service info! " + ex.getMessage());
                return null;
            }
        }


        return service;
    }

}
