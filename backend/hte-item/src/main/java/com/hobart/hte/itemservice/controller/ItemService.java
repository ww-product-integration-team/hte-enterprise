package com.hobart.hte.itemservice.controller;

import com.hobart.hte.utils.item.HobItem;
import com.hobart.hte.utils.item.ItemPayload;
import com.hobart.hte.utils.item.ProductTexts;
import com.hobart.hte.utils.item.TextId;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.hobart.hte.itemservice.repositories.ItemRepository;
import com.hobart.hte.itemservice.repositories.ProductTextsRepository;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService {

	private static final Logger log = LogManager.getLogger(ItemService.class);

	@Autowired
	ItemRepository itemRepo;

	@Autowired
	ProductTextsRepository expTxtRepo;

	/**
	 * Note: In this code it is assumed the product text fields will use the same
	 * number as the PLU this will change depending how and who populates the data
	 * in the DB
	 * 
	 * @param plu
	 * @param department
	 * @param store
	 * @return
	 */
	public ItemPayload retrieveItem(int plu, Integer department, Integer store) {

		// ToDo: make configurable how the user wants to search for the product. i.e.
		// using dept number, or not

		HobItem item = null;
		if (department != null) {
			item = itemRepo.findByPrNumAndDeptNum(plu, department);
		} else {
			// ToDo: need to make sure only one entry is returned
			item = itemRepo.findByPrNum(plu);
		}

		if (item != null) {

			ItemPayload itemPayload = new ItemPayload(item);

			// retrieve expanded text
			if (item.getTxNum() != null && item.getTxNum() > 0) {
				ProductTexts txt = retrieveText(department, 1, plu);
				itemPayload.setExpText(txt.getProdTextDesc());
			}

			// retrieve special message
			if (item.getSMNum() != null && item.getSMNum() > 0) {
				ProductTexts txt = retrieveText(department, 2, plu);
				itemPayload.setSpcMessage(txt.getProdTextDesc());
			}

			return itemPayload;
		} else {
			log.info("plu {} not found", plu);
		}
		return null;
	}

	/**
	 * 
	 * @param dept
	 * @param textType
	 * @param plu
	 * @return
	 */
	public ProductTexts retrieveText(Integer dept, int textType, int plu) {
		int tmpd = (dept == null) ? 0 : dept;

		TextId idToRetrieve = new TextId(tmpd, textType, plu);
		try {
			ProductTexts text = expTxtRepo.findById(idToRetrieve).get();
			return text;
		} catch (Exception ex) {
			log.info("The text type [{}] was not found for PLU# {}", textType, plu);
			return null;
		}
	}

	//moved to Transactions Controller on ui-manager
	// kept in case routing is created from ui-manager to hte-item
	@GetMapping(path="/getProds", produces={MediaType.APPLICATION_JSON_VALUE})
	public List<HobItem> getProds(){
		log.debug("Getting Prodcuts");
		List<HobItem> prods = new ArrayList<>();
		itemRepo.findAll().forEach(prods::add);
		return prods;
	}

}
