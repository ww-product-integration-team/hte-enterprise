package com.hobart.hte.itemservice.repositories;

import com.hobart.hte.utils.event.EventRule;
import org.springframework.data.repository.CrudRepository;

public interface EventRulesRepository extends CrudRepository<EventRule, String> {
}
