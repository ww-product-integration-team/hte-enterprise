package com.hobart.hte.itemservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
//@EnableDiscoveryClient
@EnableScheduling
//@ComponentScan(basePackages = "com.hobart.hte.itemservice, com.hobart.hte.translator, com.hobart.hte.repositories")
@EntityScan(basePackages = {"com.hobart.hte.utils"})
public class HteItemApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(HteItemApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(HteItemApplication.class);
	}
}
