package com.hobart.hte.itemservice.repositories;

import com.hobart.hte.utils.event.EventLogEntry;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

public interface EventLogRepository extends CrudRepository<EventLogEntry, Timestamp> {

    List<EventLogEntry> findByEntityIdAndEventId(String entityId, Integer eventId);

    //Find the latest event in the log that has the same event id and entity id, will only every return 1 entry or null
    @Transactional
    @Query(value="SELECT e1 FROM EventLogEntry e1 WHERE e1.timestamp = (SELECT MAX(e2.timestamp) FROM EventLogEntry e2 WHERE e2.entityId = :entity_id and e2.eventId = :event_id)")
    EventLogEntry findLatestEntryofType(@Param("entity_id") String entity_id,
                                        @Param("event_id") Integer event_id);


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "DELETE FROM EventLogEntry entry WHERE entry.timestamp < SUBDATE(CURRENT_DATE, 180)")
    void deleteOldEvents();

    List<EventLogEntry> findByTimestampBetween(Timestamp start, Timestamp end);

    List<EventLogEntry> findByTimestampBetweenAndEntityId(Timestamp start, Timestamp end, String entityId);

    List<EventLogEntry> findByTimestampBetweenAndEventId(Timestamp start, Timestamp end, Integer eventId);

    List<EventLogEntry> findByEntityId(String entityId);

    List<EventLogEntry> findByEventId(Integer eventId);

    List<EventLogEntry> findByTimestampBetweenAndEntityIdAndEventId(Timestamp start, Timestamp end, String entityId, Integer eventId);

    Boolean existsByEntityId(String entityId);

    Boolean existsByEventId(Integer eventId);

    Boolean existsByEntityIdAndEventId(String entityId, Integer eventId);

    @Modifying(clearAutomatically = true)
    @Transactional
    void deleteByEntityId(String entityId);

    @Modifying(clearAutomatically = true)
    @Transactional
    void deleteByEventId(Integer eventId);

    Integer countByOpened(Boolean opened);
}