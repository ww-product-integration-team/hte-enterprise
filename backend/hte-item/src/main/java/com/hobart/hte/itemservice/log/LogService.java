package com.hobart.hte.itemservice.log;

import com.hobart.hte.itemservice.repositories.EventLogRepository;
import com.hobart.hte.itemservice.repositories.EventRulesRepository;
import com.hobart.hte.utils.event.EventImportance;
import com.hobart.hte.utils.event.EventLogEntry;
import com.hobart.hte.utils.event.EventRule;
import com.hobart.hte.utils.event.LookupEventEntry;
import com.hobart.hte.utils.model.EntityType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service
public class LogService {

	private static final Logger log = LogManager.getLogger(LogService.class);
	@Autowired
	LookupEventRepository lookupEventRepo;
	@Autowired
	EventLogRepository eventLogRepo;
	@Autowired
	EventRulesRepository eventRulesRepo;

	public boolean logLookupEvent(String ip, int plu) {
		LookupEventEntry entry = new LookupEventEntry(ip, plu);
		try {
			lookupEventRepo.save(entry);
		} catch (Exception ex) {
			log.debug("error while saving lookup log: {}", ex.getMessage());
		}
		return true;
	}

	public boolean logEvent(Pair<Integer, Pair<String, EventImportance>> event, String deviceId, EntityType entityType, String message) {

		EventRule rule = findRule(event.getFirst(), deviceId);
		EventLogEntry deviceLogEntry = new EventLogEntry(event.getFirst(), deviceId, entityType, event.getSecond().getSecond(), rule.getShouldMute(), event.getSecond().getFirst(), message);
		EventLogEntry existingLog = null;
		if(deviceId != null){
			existingLog = eventLogRepo.findLatestEntryofType(deviceId, event.getFirst());
		}

		//If there is no log
		deviceLogEntry.setOccurrences("");

		//Update occurrence if the notification is unread otherwise make a new entry
		if(existingLog != null && !existingLog.getOpened()){
			existingLog.appendOccurrence(deviceLogEntry.getTimestamp());
			deviceLogEntry = existingLog;
		}

		//Clearing older logs in the system
		eventLogRepo.deleteOldEvents();

		if(rule.getShouldLog()){
			try {
				eventLogRepo.save(deviceLogEntry);
			} catch (Exception ex) {
				log.error("error while trying to save in log: {}", ex.getMessage());
				return false;
			}
		}
		else{
			return false;
		}

		return true;
	}
	private EventRule findRule(Integer eventId, String entityId){
		//First we need to see if there is a custom rule for this entity and event combo
		EventRule customRule = eventRulesRepo.findById(entityId + "_" + eventId.toString()).orElse(null);
		if(customRule != null){
			return customRule;
		}

		//Next we need to find the generic rule for the given event id
		EventRule generalRule = eventRulesRepo.findById(eventId.toString()).orElse(null);
		if(generalRule == null){
			log.error("Could not find rule for eventID " + eventId);
			//Fall back to logging all
			generalRule = new EventRule(1234, true, true, false);
		}

		return generalRule;
	}
}
