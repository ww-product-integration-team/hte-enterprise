package com.hobart.hte.itemservice.controller;

import com.hobart.hte.utils.item.HobItem;

import java.sql.Timestamp;
import java.util.List;

public class PluImportData {

    private boolean deleteAll;
    private String profileName;
    private Timestamp startDate;
    private Timestamp endDate;
    private String fileNameOverride;
    private List<HobItem> products;

    public boolean getDeleteAll() {
        return deleteAll;
    }

    public void setDeleteAll(boolean deleteAll) {
        this.deleteAll = deleteAll;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public String getFileNameOverride() {
        return fileNameOverride;
    }

    public void setFileNameOverride(String fileNameOverride) {
        this.fileNameOverride = fileNameOverride;
    }

    public List<HobItem> getProducts() {
        return products;
    }

    public void setProducts(List<HobItem> products) {
        this.products = products;
    }
}
