package com.hobart.hte.itemservice.repositories;

import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.upgrade.FileIdAndName;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface FileRepository extends CrudRepository<FileForEvent, String> {
    List<FileForEvent> findAllByProfileId(String profileId);
    Optional<FileForEvent> findByFilenameAndProfileId(String filename, String profileId);
    boolean existsByFilenameAndProfileId(String filename, String profileId);

    List<FileForEvent> findByProfileIdAndEnabled(String profileId, boolean enabled);
    List<FileForEvent> findByFilename(String filename);

    @Query("SELECT new com.hobart.hte.utils.upgrade.FileIdAndName(fileId, filename) FROM FileForEvent " +
            "WHERE fileId IN (:fileIds)")
    List<FileIdAndName> findFilenamesByFileIds(@Param("fileIds") List<String> fileIds);

    @Transactional
    void deleteByProfileId(String profileId);
}
