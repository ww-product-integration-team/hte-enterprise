package com.hobart.hte.itemservice.PluImport;

import com.hobart.hte.itemservice.controller.ControllerEndpoint;
import com.hobart.hte.itemservice.controller.PluImportData;
import com.hobart.hte.itemservice.log.LogService;
import com.hobart.hte.itemservice.repositories.ConfigAppRepository;
import com.hobart.hte.itemservice.repositories.FileRepository;
import com.hobart.hte.itemservice.repositories.ItemRepository;
import com.hobart.hte.itemservice.repositories.ProfileRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.event.EventConfig;
import com.hobart.hte.utils.file.FileForEvent;
import com.hobart.hte.utils.file.FileType;
import com.hobart.hte.utils.item.HobItem;
import com.hobart.hte.utils.model.OperationStatusModel;
import com.hobart.hte.utils.model.RequestOperationResult;
import com.hobart.hte.utils.profile.Profile;
import com.hobart.hte.utils.util.HteTools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PluImport extends ControllerEndpoint<String> implements HteTools.AsyncExecutor<PluImport> {
    private final String REPOSITORY_PATH;

    private final Logger log = LogManager.getLogger(PluImport.class);

    private final PluImportData jsonData;
    private final OperationStatusModel result;
    private final ItemRepository itemRepo;
    private final ProfileRepository profileRepo;
    private final FileRepository fileRepo;
    private final LogService logService;

    public HttpStatus resultStatus;

    public PluImport(PluImportData jsonData, OperationStatusModel result, ItemRepository itemRepo, ProfileRepository profileRepo,
                     FileRepository fileRepo, ConfigAppRepository configRepo, LogService logService) {
        this.jsonData = jsonData;
        this.result = result;
        this.itemRepo = itemRepo;
        this.profileRepo = profileRepo;
        this.fileRepo = fileRepo;
        this.logService = logService;

        this.REPOSITORY_PATH = configRepo.findById(AppConfig.repositoryPath)
                .orElse(new ConfigEntry("", "")).getCoValue();
    }

    public void execute() {
        // validation
        log.info("Validating inputs");
        if (jsonData.getProducts() == null || jsonData.getProducts().isEmpty()) {
            log.error("No plus provided");
            result.setErrorDescription("No plus provided");
            result.setResult(RequestOperationResult.ERROR.name());
            resultStatus = HttpStatus.BAD_REQUEST;
            return;
        }
        if (HteTools.isNullorEmpty(jsonData.getProfileName())) {
            log.error("No profile name provided");
            result.setErrorDescription("No profile name provided");
            result.setResult(RequestOperationResult.ERROR.name());
            resultStatus = HttpStatus.BAD_REQUEST;
            return;
        }

        // save plus to database
        log.info("Saving json import products");
        List<Integer> toNotDelete = new ArrayList<>();
        jsonData.getProducts().forEach(plu -> {
            if (validateProduct(plu)) {
                HobItem found = null;
                try {
                    found = itemRepo.findByPrNumAndDeptNum(plu.getPrNum(), plu.getDeptNum());
                } catch (NullPointerException e) {
                    log.info("Item with product number '{}' not found in database", plu.getPrNum());
                }
                if (found != null) {
                    plu.setPrID(found.getPrID());
                }
                toNotDelete.add(plu.getPrID());
                itemRepo.save(plu);
            } else {
                log.error("PLU failed validation");
            }
        });

        // delete old PLUs not included in import
        if (jsonData.getDeleteAll()) {
            log.info("Deleting existing products not imported");
            itemRepo.deleteAllByPrIDNotIn(toNotDelete);
        }

        // later need to create two files
            // one of before changes and one after
        log.info("Creating .ht file of products");
        String filePath;
        try {
            List<HobItem> items = new ArrayList<>();
            itemRepo.findAll().forEach(items::add);
            filePath = HteTools.createHTFile(items, jsonData.getFileNameOverride());
        } catch (IOException ex) {
            log.error("Exception thrown while writing .ht file: {}", ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Exception thrown while writing .ht file: " + ex.getMessage());
            resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return;
        }

        log.info("Moving .ht file to provided profile");
        Profile profile = profileRepo.findByNameIgnoreCase(jsonData.getProfileName());
        String profileFilePath;
        String profileFileName;
        if (HteTools.isWindows()) {
            profileFileName = filePath.substring(filePath.lastIndexOf("\\") + 1);
            profileFilePath = filePath.substring(0, filePath.lastIndexOf("\\")) + "\\" + profile.getProfileId() + "\\" + profileFileName;
        } else {
            profileFileName = filePath.substring(filePath.lastIndexOf("/") + 1);
            profileFilePath = filePath.substring(0, filePath.lastIndexOf("/")) + "/" + profile.getProfileId() + "/" + profileFileName;
        }
        File changedLocation = new File(profileFilePath);
        File profileDir = Paths.get(REPOSITORY_PATH, profile.getProfileId()).toFile();
        if (!profileDir.exists()) {
            if (!profileDir.mkdirs()) {
                log.error("unable to create directory: {}", profileDir.getAbsolutePath());
                result.setResult(RequestOperationResult.ERROR.name());
                result.setErrorDescription("unable to create directory: " + profileDir.getAbsolutePath());
                resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                return;
            } else {
                log.debug("directory created!");
            }
        }
        try {
            Path toProfileFilePath = Paths.get(profileFilePath);
            if (Files.exists(toProfileFilePath)) {
                Files.delete(toProfileFilePath);
                fileRepo.delete(fileRepo.findByFilename(profileFileName).get(0));
            }
            Files.move(Paths.get(filePath), toProfileFilePath);
        } catch (IOException ex) {
            log.error("Exception thrown while moving .ht file: {}", ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Exception thrown while moving .ht file: " + ex.getMessage());
            resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return;
        }

        log.info("Creating database record of new .ht file");
        FileForEvent file = new FileForEvent();
        try {
            MessageDigest md5Digest = MessageDigest.getInstance("MD5");
            String checksum = HteTools.getFileChecksum(md5Digest, changedLocation);
            file.setChecksum(checksum);
        } catch (NoSuchAlgorithmException ex) {
            log.error("Exception thrown while generating md5 hash: {}", ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Exception thrown while generating md5 hash: " + ex.getMessage());
            resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return;
        } catch (IOException ex) {
            log.error("Exception thrown while calculating .ht file checksum: {}", ex.getMessage());
            result.setResult(RequestOperationResult.ERROR.name());
            result.setErrorDescription("Exception thrown while calculating .ht file checksum: " + ex.getMessage());
            resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return;
        }
        file.setFileId(UUID.randomUUID().toString());
        file.setShortDesc("PLUs");
        file.setSize(changedLocation.length());
        file.setFilename(profileFileName);
        file.setFileVersion("1.0");
        file.setProfileId(profile.getProfileId());
        file.setUploadDate(Timestamp.from(Instant.now()));
        file.setEnabled(true);
        file.setFileType(FileType.CONFIG);
        if (jsonData.getStartDate() == null) {
            log.warn("No start date provided for .ht file. Defaulting to right now");
            file.setStartDate(Timestamp.from(Instant.now()));
        } else {
            file.setStartDate(jsonData.getStartDate());
        }
        file.setEndDate(jsonData.getEndDate());
        fileRepo.save(file);

        logService.logEvent(EventConfig.LoggingEvent, null, null, "Finished async upload of PLU JSON data");
    }

    private boolean validateProduct(HobItem plu) {
        // try out using springboot validation of these fields
        if (plu.getPrNum() == null) {
            log.error("[PLU: {}] The product number is null", plu.getPrID());
            return false;
        }
        if (plu.getDeptNum() == null) {
            log.error("[PLU: {}] The product department number is null", plu.getPrID());
            return false;
        }
        if (plu.getPrDesc() == null) {
            log.error("[PLU: {}] The product description is null", plu.getPrID());
            return false;
        }
        if (plu.getPrPrice() == null || plu.getPrPrice() <= 0) {
            log.error("[PLU: {}] The product price is invalid", plu.getPrID());
            return false;
        }
        if (plu.getPrType() == null) {
            log.error("[PLU: {}] The product type is null", plu.getPrID());
            return false;
        }
        if (plu.getPrType() == 2 && (plu.getPrNetWt() == null || plu.getPrNetWt() < 0)) {
            log.error("[PLU: {}] The product net weight is invalid for a Fixed Weight product", plu.getPrID());
            return false;
        }

        return true;
    }
}
