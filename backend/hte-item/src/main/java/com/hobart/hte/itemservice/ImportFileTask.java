package com.hobart.hte.itemservice;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.regex.Pattern;

import com.hobart.hte.itemservice.repositories.ConfigAppRepository;
import com.hobart.hte.utils.config.AppConfig;
import com.hobart.hte.utils.config.ConfigEntry;
import com.hobart.hte.utils.model.HeartbeatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hobart.hte.itemservice.translator.HobStdTranslator;

import javax.annotation.PostConstruct;

@Component
public class ImportFileTask {
	private static final Logger log = LoggerFactory.getLogger(ImportFileTask.class);

//	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	private static File import_path = null;
	private static File done_path = null;

	// ?: part of pattern does not do groupings and only matches
	// uuid regex pattern:
	public static final String filename_regex =
			"[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}_[a-zA-Z0-9]+\\.(?:(?:ht)|(?:deb))";

	@Autowired
	private HobStdTranslator stdTrans;

	@Autowired
	ConfigAppRepository configRepo;

	@PostConstruct
	public void init() {
		import_path = getFile(AppConfig.importPath);
		done_path = getFile(AppConfig.donePath);
	}

	private File getFile(String configKey){
		ConfigEntry pathEntry = configRepo.findById(configKey).orElse(null);
		if(pathEntry == null || pathEntry.getCoValue().equals("") || pathEntry.getCoValue() == null){
			log.error("Could not find path entry for key: " + configKey);
			return null;
		}
		return new File(pathEntry.getCoValue());
	}

	/**
	 * Execute every minute
	 */
	@Scheduled(cron = "0 */1 * * * *")
	public void reportCurrentTime() {
		// uncomment only for debug purposes: log.info("time to check if any file exists in the path...");

		String[] fileNames = new String[0];
		try {
			fileNames = Objects.requireNonNull(import_path.list());
		} catch (NullPointerException e) {
			log.debug("Null pointer exception caught reading file names for transaction upload");
		}

		if (fileNames.length != 0) {
			log.debug("files found: {}", fileNames.length);
			for (String f : fileNames) {
				log.info("> {}", f);
				f = f.toLowerCase();
				if (f.endsWith(".std")) {
					log.info("starting to translate file to ht");
					// process STD file and translate to .ht file
					stdTrans.processStdFile(Paths.get(import_path.toString(), f).toString());

					try {
						Files.move(Paths.get(import_path.toString(), f), Paths.get(done_path.toString(), f),
								StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException ex) {
						log.error("an error occurred while attempting to move files: {}", ex.toString());
						ex.printStackTrace();
					}

				} else if (Pattern.matches(filename_regex, f)) {
					System.out.println("matches pattern");
					// ToDo: if the profile is valid, move the file to the right profile directory
					// and create an entry on the DB
				} else {
					System.out.println("file doesn't match any pattern, discarding file: " + f);
					try {
						Files.move(Paths.get(import_path.toString(), f), Paths.get(done_path.toString(), f),
								StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException ex) {
						log.error("an error occurred while attempting to move files: {}", ex.toString());
					}
				}
			}
		} else {
			log.debug("no files found in path, waiting for next cycle");
		}
	}
}
