package com.hobart.hte.itemservice.repositories;

import java.sql.Timestamp;
import java.util.List;

import com.hobart.hte.utils.transaction.TransEntry;
import org.springframework.data.repository.CrudRepository;

public interface TransRepository extends CrudRepository<TransEntry, Integer> {

	/**
	 * 
	 * @param t1
	 * @param t2
	 * @return
	 */
	List<TransEntry> findByTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(Timestamp t1, Timestamp t2);

	/**
	 * 
	 * @param list_scales
	 * @param t1
	 * @param t2
	 * @return
	 */
	List<TransEntry> findByScaleIdInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(List<String> list_scales,
			Timestamp t1, Timestamp t2);

	/**
	 * 
	 * @param list_plu
	 * @param t1
	 * @param t2
	 * @return
	 */
	List<TransEntry> findByTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(List<Integer> list_plu,
			Timestamp t1, Timestamp t2);

	/**
	 * 
	 * @param list_scales List of scales to include in the query
	 * @param list_plu    List of the PLU numbers to include in the query
	 * @param t1          Starting timestamp interval
	 * @param t2          Ending timestamp interval
	 * @return A list of the transactions that match the given filters
	 */
	List<TransEntry> findByScaleIdInAndTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(
			List<String> list_scales, List<Integer> list_plu, Timestamp t1, Timestamp t2);
}
