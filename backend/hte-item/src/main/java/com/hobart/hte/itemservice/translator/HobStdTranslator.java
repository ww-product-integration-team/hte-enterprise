package com.hobart.hte.itemservice.translator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.hobart.hte.utils.item.HobItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hobart.hte.itemservice.repositories.ItemRepository;

@Service
public class HobStdTranslator {

	private static final Logger log = LoggerFactory.getLogger(HobStdTranslator.class);

	public static final char US = 31, RS = 30, ETX = 3, ETB = 23, GS = 29, LF = 0xa;
	private int storeNbr = 0;

	@Autowired
	ItemRepository itemrepo;

	/**
	 * Process file, it is expected to include in the header the number of the
	 * "store"/department to translate and store in the database
	 * 
	 * @param importFile Path to the file to process
	 * @return Number of records translated, >0 successful, <0 an error occurred.
	 */
	public int processStdFile(String importFile) {
		int nrec = 0;

		log.info("opening file [{}] to translate", importFile);
		try {
			BufferedReader br = new BufferedReader(new FileReader(importFile));
			String tmp;

			while ((tmp = br.readLine()) != null) {
				if (tmp.startsWith("OpTypeExecuteOneTask")) {
					storeNbr = getIntValue(tmp, "TkDept");
					// TODO: get the rule how is the department field is combined with
					// store/department to save properly in the DB
					log.info("store number: {}", storeNbr);
				} else if (tmp.startsWith("OpTypeChangeOneItem") || tmp.startsWith("OpTypeWriteOneItem")) {
					HobItem tmp_item = processItemStdRecord(tmp);

					HobItem findPr = itemrepo.findByPrNumAndDeptNum(tmp_item.getPrNum(), tmp_item.getDeptNum());
					if (findPr != null) {
						// update primary key
						tmp_item.setPrID(findPr.getPrID());
					}
					itemrepo.save(tmp_item);
				}
				// TODO: add support for expanded text and other text fields
				// TODO: add support for nutri data and other PLU related fields (only the nutri
				// number is supported, it is expected that nutri data should come in a .ht
				// file)
				nrec++;
			}

			br.close();
		} catch (IOException ioex) {
			log.info("an error occurred while procesing file: {}", ioex.toString());
			return -1;
		}
		log.info("{} records translated...", nrec);

		return nrec;
	}

	/**
	 * Parse Hobart Custom Standard tagged record and return an object to save in
	 * the database
	 * 
	 * @param rec
	 * @return
	 */
	public HobItem processItemStdRecord(String rec) {
		HobItem item = new HobItem();
		String stmp;
		int plunum;

		plunum = getIntValue(rec, "PLU#__");
		item.setPrNum(plunum);
		item.setDeptNum(storeNbr);
		item.setPrByCnt(getIntValue(rec, "ByCont"));

		item.setClNum(getIntValue(rec, "RptCls"));
		item.setPrDesc(getValue(rec, "ItemDs").replace("<BR>", "\n"));
		item.setPrRawBarC(getValue(rec, "BarCod"));
		int itmp = this.getIntValue(rec, "Price_");
		item.setPrPrice(itmp);

		stmp = getValue(rec, "ItmTyp");
		switch (stmp) {
		case "Random Weight":
			itmp = 1;
			break;
		case "Fixed Weight":
			itmp = 2;
			break;
		case "By Count":
			itmp = 3;
			break;
		case "Fluid Ounces":
			itmp = 4;
		}
		item.setPrType(itmp);

		item.setPrBarPref(getIntValue(rec, "BarCNS"));

		item.setPrNetWt(getIntValue(rec, "NetWt_0"));
		item.setPrTare(getIntValue(rec, "Tare01"));
		item.setPrTareB(getIntValue(rec, "Tare02"));

		item.setPrShlfLfD(getIntValue(rec, "ShlfLf"));
		item.setPrProdLfD(getIntValue(rec, "PrdLif"));
		item.setTxNum(getIntValue(rec, "ExTxt#"));

		item.setLbNum1(getIntValue(rec, "Lbl_01"));
		item.setLbNum2(getIntValue(rec, "Lbl_02"));
		item.setPrGrNm1(getValue(rec, "Grphc#"));

		item.setNtNum(getIntValue(rec, "Nutr#_"));
		item.setDLXNum(getIntValue(rec, "CDfTx#"));
		item.setLLID(getIntValue(rec, "CList#"));
		item.setLXNum(getIntValue(rec, "CText#"));
		item.setPrCOLReq(getBoolValue(rec, "CoolRq"));
		item.setPrVCOLTxt(getBoolValue(rec, "FcdCoo"));

		return item;
	}

	/**
	 * 
	 * @param sb
	 * @param tag
	 * @return the value corresponding to the given tag, otherwise null
	 */
	public String getValue(String sb, String tag) {
		int pos = sb.indexOf(tag);
		if (pos == -1) {
			return null;
		}
		String value = sb.substring(pos + tag.length());
		try {
			pos = value.indexOf(US);
			if (pos > 0) {
				value = value.substring(0, pos);
			}
		} catch (Exception e) {
			log.info("Error while parsing [{}] tag in record: {}", tag, sb);
			return null;
		}
		return value;
	}

	/**
	 * 
	 * @param tmp
	 * @param tag
	 * @return
	 */
	private Integer getIntValue(String tmp, String tag) {
		String stmp = getValue(tmp, tag);

		if (stmp == null) {
			return null;
		}

		int pos = stmp.indexOf(".");
		if (pos > 0) {
			stmp = stmp.replace(".", "");
		}

		try {
			return Integer.valueOf(stmp);
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 
	 * @param tmp
	 * @param tag
	 * @return
	 */
	private Boolean getBoolValue(String tmp, String tag) {
		String stmp = getValue(tmp, tag);

		try {
			return Boolean.valueOf(stmp.trim().toLowerCase());
		} catch (Exception ex) {
			return null;
		}
	}
}
