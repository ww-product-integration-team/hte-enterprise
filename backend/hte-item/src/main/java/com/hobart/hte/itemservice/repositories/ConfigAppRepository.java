package com.hobart.hte.itemservice.repositories;

import com.hobart.hte.utils.config.ConfigEntry;
import org.springframework.data.repository.CrudRepository;

public interface ConfigAppRepository extends CrudRepository<ConfigEntry, String> {

}