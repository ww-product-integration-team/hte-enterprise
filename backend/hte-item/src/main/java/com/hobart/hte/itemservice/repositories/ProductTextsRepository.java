package com.hobart.hte.itemservice.repositories;

import com.hobart.hte.utils.item.ProductTexts;
import com.hobart.hte.utils.item.TextId;
import org.springframework.data.repository.CrudRepository;

public interface ProductTextsRepository extends CrudRepository<ProductTexts, TextId>{
	
}
