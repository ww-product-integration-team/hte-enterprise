package com.hobart.hte.itemservice;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hobart.hte.utils.item.ProductTexts;
import com.hobart.hte.utils.item.TextId;
import com.hobart.hte.utils.transaction.TransEntry;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.hobart.hte.itemservice.repositories.ProductTextsRepository;
import com.hobart.hte.itemservice.repositories.TransRepository;

@SpringBootTest
class HteItemApplicationTests {

	@Autowired
	ProductTextsRepository expTxtRepo;

	@Autowired
	TransRepository transRepo;

	@Test
	void contextLoads() {
	}

	@Test
	public void loadExpandedText() {
//		TextId idToRetrieve = new TextId(0, 1, 4011);
//		ProductTexts expTxt = expTxtRepo.findById(idToRetrieve).get();
//		System.out.println("text: " + expTxt.getProdTextDesc());
	}

	@Test
	public void findByScaleList() {
		ArrayList<String> scale_list = new ArrayList<String>();
		scale_list.add("10.3.128.128");
		scale_list.add("10.3.128.129");
		scale_list.add("10.3.127.131");

		Timestamp ts = Timestamp.valueOf("2022-08-23 10:10:10.0");
		Timestamp te = Timestamp.valueOf("2022-09-23 10:10:10.0");

		List<TransEntry> findByScaleId = transRepo
				.findByScaleIdInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(scale_list, ts, te);

		System.out.println("number of elements returned: " + findByScaleId.size());
		for (TransEntry t : findByScaleId) {
			System.out.println(t.getScaleId());
		}

	}

	@Test
	void findByItemList() {
		System.out.println("starting test, search by PLU list");
		ArrayList<Integer> plu_list = new ArrayList<Integer>();
		plu_list.add(125);
		plu_list.add(7504);

		Timestamp ts = Timestamp.valueOf("2022-08-23 10:10:10.0");
		Timestamp te = Timestamp.valueOf("2022-09-23 10:10:10.0");

		List<TransEntry> findByPlu = transRepo.findByTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(plu_list,
				ts, te);

		System.out.println("number of elements returned: " + findByPlu.size());
		for (TransEntry t : findByPlu) {
			System.out.println(t.getScaleId());
		}
	}

	@Test
	void findByByScaleAndItemListAndBetweenDates() {
		System.out.println("starting test, search by PLU and item list");

		ArrayList<String> scale_list = new ArrayList<String>();
		scale_list.add("10.3.128.128");
		scale_list.add("10.3.128.129");
		scale_list.add("10.3.127.111");

		ArrayList<Integer> plu_list = new ArrayList<Integer>();
		plu_list.add(125);
		plu_list.add(7504);

		Timestamp ts = Timestamp.valueOf("2022-08-23 10:10:10.0");
		Timestamp te = Timestamp.valueOf("2022-09-23 10:10:10.0");

		System.out.println("start: " + ts.toString());
		System.out.println("end: " + te.toString());

		List<TransEntry> findBy = transRepo.findByScaleIdInAndTrPrNumInAndTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(
				scale_list, plu_list, ts, te);

		System.out.println("number of elements returned: " + findBy.size());
		for (TransEntry t : findBy) {
			System.out.printf("%d, %s, %d, %s\n", t.getTransID(), t.getScaleId(), t.getTrPrNum(), t.getTrDtTm());
		}
	}

	// http://10.3.128.86:8080/transservice/excelExport?end=09-07-2022&start=08-30-2022
	
	@Test
	void findBetweenDates() {
		System.out.println("starting test, search between dates");

		String start = "08-20-2022";
		String end = "09-06-2022";

		Timestamp ts = validateDate(start);
		Timestamp te = validateDate(end);

		System.out.println("start: " + ts.toString());
		System.out.println("end: " + te.toString());

		List<TransEntry> findBy = transRepo.findByTrDtTmGreaterThanEqualAndTrDtTmLessThanEqual(ts, te);
		System.out.println("number of elements returned: " + findBy.size());
		for (TransEntry t : findBy) {
			System.out.printf("%d, %s, %d, %s\n", t.getTransID(), t.getScaleId(), t.getTrPrNum(), t.getTrDtTm());
		}
	}

	private Timestamp validateDate(String dateString) {
		try {		
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			Date parsedDate = dateFormat.parse(dateString);
			System.out.println("date temp: " + parsedDate.toString());
			
			Timestamp timestamp = new Timestamp(parsedDate.getTime());
			return timestamp;
		} catch (Exception e) {
			return null;
		}
	}
}
