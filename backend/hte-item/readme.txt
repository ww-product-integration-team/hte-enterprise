[07/13/22]
To convert the application to a web service you need to uncomment the following things: 

-------------------------
        pom.xml
-------------------------
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>

-------------------------
 application.properties 
-------------------------
server.port=0
spring.application.name=hte-item-service
eureka.client.service-url.defaultZone=http://10.3.177.149:8010/eureka

-------------------------
 HteItemApplication.java
-------------------------

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@EnableDiscoveryClient


