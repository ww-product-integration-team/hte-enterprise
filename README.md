# HTe-Enterprise Cloud Notes
This document provides a basic Quick Start documentation for the HTe-Enterprise Cloud implementation. 

This project's goal was to implement the HTe-Enterprise codebase to the cloud via Azure.

## Cloud Setup
HTe-Enterprise Cloud's code lives on an instance within the PIT's control on Azure.
Some touchpoints are as follows:

1. Web DNS records (A records) for **hte-enterprise.com** is pointed at this instance.
2. The public IP address for this instance is **172.174.94.193**.
3. The primary user for this instance is **azureuser**. This user has root/sudo access.
4. The login script for this user sudo's them in and changes to /home/azureuser/hte/backend/ui-manager.
5. Once into this folder, the user can type **git status** to see what branch is checked out, and any out-of-sync edits performed.
6. The current branch of **hte-cloud** is checked out, and all proper permissions are set up.

The PEM file has already been provided to PIT. to ssh with this, you will type: **ssh azureuser@172.174.94.193 -i <PEM FILE>**

## Building
If a build is needed, you will need to:

1. **cd ../hte-utils**
2. **mvn clean install**
3. **cd ../ui-manager**
4. Run as described below.

## Running
The codebase is currently set up as a screen, so you can see realtime logging as you would locally. To do this:

1. Log in as listed above.
2. Type **screen -x** and enter.
3. This attaches you to the screen, and you'll see the code running.
4. **IMPORTANT:** to exit the screen, either close the terminal window (actually click it to close it) or **type CTRL+A, then CTRL+D**. 

**_If you CTRL+C or exit some other way, it will kill the screen and the process running the code._**

To run the code from a fresh screen session or from the main command line for the user, type **mvn clean install tomcat9:run**

## Accessing the API via Swagger
The Swagger API implementation can be accessed at: http://hte-enterprise.com/hte-enterprise/swagger-ui/index.html

