import { alpha, makeStyles} from '@material-ui/core/styles';


// const drawerWidth = 240;
// const breakPoint = 900;

// Style for NavBar.jsx
// const navBarStyle = makeStyles((theme) => ({
//     grow: {
//         flexGrow: 1,
//     },
//     appBar: {
//       zIndex: theme.zIndex.drawer + 2,
//       backgroundColor: '#203140',
//     },
//     inputRoot: {
//         color: 'inherit',
//     },
//     inputInput: {
//         padding: theme.spacing(1, 1, 1, 0),
//         // vertical padding + font size from searchIcon
//         paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
//         transition: theme.transitions.create('width'),
//         width: '100%',
//         [theme.breakpoints.up(breakPoint)]: {
//             width: '20ch',
//         },
//     },
//     sectionDesktop: {
//         display: 'none',
//         [theme.breakpoints.up(breakPoint)]: {
//             display: 'flex',
//         },
//     },
//     sectionMobile: {
//         display: 'flex',
//         [theme.breakpoints.up(breakPoint)]: {
//             display: 'none',
//         },
//     },
// }));

// Style for SideBar.jsx
// const sideBarStyle = makeStyles((theme) => ({
//     root: {
//       display: 'flex',
//       backgroundColor: theme.palette.background.paper,
//     },
//     drawer: {
//       width: drawerWidth,
//       flexShrink: 0,
//     },
//     drawerPaper: { 
//       width: drawerWidth,
//     },
//     drawerContainer: {
//       overflow: 'auto',
//       paddingTop: 15,
//     },
//     dropDown: {
//         width: "100%",
//         maxWidth: drawerWidth,
//     },
//     nested: {
//         paddingLeft: theme.spacing(4)
//     },

//     sectionDesktop: {
//         display: 'none',
//         [theme.breakpoints.up(breakPoint)]: {
//             display: 'flex',
//         },
//         backgroundColor: theme.palette.background.paper,
//     },
//     sectionMobile: {
//         display: 'flex',
//         flexDirection: 'column',
//         width: 100,
//         [theme.breakpoints.up(breakPoint)]: {
//             display: 'none',
//         },
//         zIndex:1200
//     },

//     customHoverFocus: {
//       "&:hover": { backgroundColor: "#ba4735" },
//       backgroundColor: theme.palette.grey[400],
//       boxShadow: 25,
//       margin: "10px 25px 10px 25px",
//     }

//   }));


// Style for AssetList.jsx
const assetListStyle = makeStyles((theme) => ({


    root: {
        // color: theme.palette.text.primary,

        "&&.Mui-selected > .MuiTreeItem-content": {
            background: theme.palette.grey[200]
        },

        //When mouse hovering the item, format content class
        "&&.MuiTreeItem-root > .MuiTreeItem-content:hover": {
            background: theme.palette.grey[300]
        },

        //Make sure label does not highlight when hovering only contentview
        "&&.MuiTreeItem-root > .MuiTreeItem-content:hover > .MuiTreeItem-label": {
            background: "transparent"
        },

        "&&.MuiTreeItem-root.Mui-selected > .MuiTreeItem-content .MuiTreeItem-label:hover": {
            background: "transparent" 
        },

        //Make sure label does not highlight when selected only contentview
        '&&.Mui-selected > .MuiTreeItem-content > .MuiTreeItem-label':{
            backgroundColor: 'transparent'
        },

        '&&.MuiTreeItem-root.Mui-selected > .MuiTreeItem-content .MuiTreeItem-label':{
            backgroundColor: 'transparent'
        },

        //Reset view as hover mo
        "@media (hover: none)": {
            backgroundColor: "transparent"
        },

    },

    content: {
        color: theme.palette.text.secondary,
        borderRadius: 5,
        margin: 2,
        paddingRight: theme.spacing(2),
        fontWeight: theme.typography.fontWeightMedium,
        '$expanded > &': {
            fontWeight: theme.typography.fontWeightRegular,
        },
        background: props => props.highlight ? alpha('#ba4735',0.50)  : theme.palette.grey[200],
        transition: props => props.fade ? "background-color 1s linear": undefined
    },

    group: {
        marginLeft: props => '30px !important',
        '& $content': {
            paddingLeft: theme.spacing(2),
        },
    },
    expanded: {},
    label: {
        backgroundColor: 'transparent',
    },
    labelRoot: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 0), // First number changes the height of each row
    },
    accordionRoot:{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
    },
    accordionRootCol:{
        display: 'flex',
        flexDirection: 'column',
        flexBasis: '100%',
        flex: '1',
    },
    labelIcon: {
        marginRight: theme.spacing(1),
        padding:5,
        width:32,
        height: 32,
        borderRadius:4,
        display:'flex',
        backgroundColor: 'transparent'
    },
    labelText: {
        fontWeight: 'inherit',
        flexGrow: 1,
        paddingLeft: 10,
    },
}));

const accordionStyle = makeStyles((theme) => ({
  root: {
    width:'100%',
    border: 'none',
    boxShadow: 'none',
    backgroundColor: 'transparent',
    backgroundImage: 'none',
    padding: 0,
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },

    // '&$content':{
    //   margin:'11px',
    // },

    "&.MuiAccordionSummary-root": {
        

      background: 'green'
    },

  },
  expanded: {},
}));


export { assetListStyle, accordionStyle };