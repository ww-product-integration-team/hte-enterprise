import { makeStyles } from '@material-ui/core/styles';

// TODO:  Get rid of this file once all tables are made using "TableTemplate.jsx" 
//            still being used by old tables that aren't using TableTemplate.jsx yet

const useToolbarStyles = makeStyles((theme) => ({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.text.primary,
            backgroundColor: '#73cfec',
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1',
      fontWeight: theme.typography.fontWeightBold,
    },
    description: {
      flex: '2',
      fontSize: 18,
      textAlign: "center",
    },
    actions: {
      color: theme.palette.text.secondary,
    },
  }));

  const enhancedTableStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    head: {
        color: theme.palette.common.white,
        '&:hover': {
            color: '#ddd'//theme.palette.action.hover,
        },
        minWidth: 150,
    },
    smallHead: {
      color: theme.palette.common.white,
      '&:hover': {
          color: '#ddd'//theme.palette.action.hover,
      },
      minWidth: 70,
  },
    MuiTableCell: {
        paddingCheckbox: {
          root: {
            padding: theme.spacing(2),
          },
        },
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
      overflowX: 'auto',
    },
    table: {
      width: '100%',
      minWidth: 700,
    },

    cell: {
      padding: theme.spacing(0),
    },

    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }));


  export { useToolbarStyles, enhancedTableStyles }