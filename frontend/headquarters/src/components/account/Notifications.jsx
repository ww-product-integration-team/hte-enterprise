import {useState, useEffect, useMemo } from 'react';
import { Form } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import MaterialReactTable from 'material-react-table';
import Box from '@material-ui/core/Box';

import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Breadcrumb } from '@themesberg/react-bootstrap';
import { faHome } from '@fortawesome/free-solid-svg-icons';

import { useAppSelector } from "../../state/index"
import { ProfileAPI, EventAPI } from '../api';
import { MenuItem, ListItemIcon, Button } from '@mui/material';
import { Delete, Mail } from '@mui/icons-material';

/* ==============   IMPORTANT NOTE    ==============

This entire component is out of date. It is not using the TableTemplate yet, and is purely for show.
We want to have something here for actual notifications at some point, but not super important now.
    ex. Scale x in Store Y is offline at [timeStamp]

NOW IN DEVELOPMENT
*/

export const Notifications = (props) => {
  const { profileId } = props;
  const [forceViewUpdate, setForceViewUpdate] = useState(false) // Used to refresh the DOM because I can't figure out any other way
  const notificationData = useAppSelector((state) => state.event.events)

  useEffect(() => {
    setForceViewUpdate(!forceViewUpdate)
  }, [notificationData])

  // Redux
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(ProfileAPI.fetchProfiles(Date.now()))
    dispatch(EventAPI.fetchEvents(Date.now()+1))
  }, [profileId]);

  const columns = useMemo(() => [
    {
      accessorKey: 'event',
      header: 'Event'
    },
    {
      accessorKey: 'message',
      header: 'Message',
      minSize: 700
    },
    {
      accessorFn: (cell) => new Date(cell.timestamp),
      header: 'Date',
      Cell: ({cell}) => cell.getValue()?.toLocaleString()
    }
  ])

  return (
    <>
    <div className="mb-4 mb-md-0">
      <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
          <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
          <Breadcrumb.Item active>Notifications</Breadcrumb.Item>
      </Breadcrumb>

      <h2>Notifications</h2>
      <p className="mb-4">The notification center for all changes and updates.</p>

      <Form className="form-control">
        <div>
          <MaterialReactTable
            columns={columns}
            data={notificationData}
            enableRowActions
            enableRowSelection
            enableStickyHeader
            enableDensityToggle={false}
            enableFullScreenToggle={false}
            initialState={{
              density: 'compact'
            }}
            onHoveredRowChange={{
              sx: {borderColor: "red"}
            }}
            muiExpandAllButtonProps={{
              sx: {color: 'white'}
            }}
            muiSelectAllCheckboxProps={{
              sx: {color: 'white'}
            }}
            muiTableProps={{
              sx: {tableLayout: 'fixed'}
            }}
            muiTableHeadCellFilterTextFieldProps={{
              sx: {color: 'white'}
            }}
            muiTableBodyCellProps={({row}) =>({
              sx: !row.original.opened ? 
              {fontWeight: 'bold', backgroundColor: 'white'}
              :
              {fontWeight: 'normal', backgroundColor: '#f2f2f2'}
            })}
            renderDetailPanel={({row}) => (
              <div className="container-fluid">
                <div className="row mt-3">
                  <div className="col-2"></div>
                  <Box className="boxBold col-1">Entity Id:</Box>
                  <Box className="boxRegular col-8">{row.original.entityId}</Box>
                </div>
                <div className="row">
                  <div className="col-2"></div>
                  <Box className="boxBold col-1">Entity Type:</Box>
                  <Box className="boxRegular col-8">{row.original.entityType}</Box>
                </div>
                <div className="row">
                  <div className="col-2"></div>
                  <Box className="boxBold col-1">Event Type:</Box>
                  <Box className="boxRegular col-8">{row.original.type}</Box>
                </div>
                <div className="row mb-3">
                  <div className="col-2"></div>
                  <Box className="boxBold col-1">Status:</Box>
                  <Box className="boxRegular col-8">{row.original.statusStr}</Box>
                </div>
              </div>
            )}
            renderRowActionMenuItems={({closeMenu, row}) => [
              <MenuItem
                key={0}
                onClick={() => {
                  row.original.opened = !row.original.opened
                  dispatch(EventAPI.updateEvents(Date.now(), row.original))
                  setForceViewUpdate(!forceViewUpdate)
                  closeMenu()
                }}
              >
                <ListItemIcon>
                  <Mail />
                </ListItemIcon>
                Mark/Unmark Read
              </MenuItem>,
              <MenuItem
                key={1}
                onClick={() => {
                  dispatch(EventAPI.deleteEvents(Date.now(), row.original.timestamp))
                  closeMenu()
                }}
              >
                <ListItemIcon>
                  <Delete />
                </ListItemIcon>
                Delete
              </MenuItem>
            ]}
            positionToolbarAlertBanner="bottom"
            renderTopToolbarCustomActions={({table}) => {
              const handleDelete = () => {
                table.getSelectedRowModel().flatRows.map(row => {
                  dispatch(EventAPI.deleteEvents(Date.now(), row.original.timestamp))
                })
              }
              const handleMarkRead = () => {
                table.getSelectedRowModel().flatRows.map(row => {
                  row.original.opened = !row.original.opened
                  dispatch(EventAPI.updateEvents(Date.now(), row.original))
                  setForceViewUpdate(!forceViewUpdate)
                })
              }

              return (
                <div style={{display: 'flex', gap: '1rem'}}>
                  <Button
                    disabled={!table.getIsSomeRowsSelected()}
                    onClick={handleMarkRead}
                    variant='contained'
                    startIcon={<Mail />}
                  >
                    Mark/Unmark Read
                  </Button>
                  <Button
                    color='error'
                    disabled={!table.getIsSomeRowsSelected()}
                    onClick={handleDelete}
                    variant='contained'
                    startIcon={<Delete />}
                  >
                    Delete
                  </Button>
                </div>
              )
            }}
          />
        </div>
      </Form>
    </div>
    </>
  );
}

export default Notifications;

