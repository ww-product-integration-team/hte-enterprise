
import { useState} from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import Alert from '@mui/material/Alert';
import { Email } from '@mui/icons-material';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { Col, Form, Card, Container } from '@themesberg/react-bootstrap';
import { FormInput } from '../forms/FormHelper';
import logger from '../utils/logger';


export default function ForgotPassword() {

    interface ForgotPasswordForm{
        email : string
    }

  const { register, handleSubmit,  formState: {errors} } = useForm<ForgotPasswordForm>();
  const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend
  const [emailAlert, setEmailAlert] = useState([false, "Something went wrong!"]) // If passwords don't match

  const handleForgotPassword = handleSubmit(async (data : ForgotPasswordForm) => {    // When the Submit button is pressed
    logger.info("Log in POST request data: ", data)

    setShowAlert([false, "Something went wrong!"])
    setEmailAlert([false, "Something went wrong!"])

    // TODO: 
    //    Have the backend send the email (Find out how to do this in general)
    //    Find way to generate code to reset password or other method of security so random hacker guy doesn't change everyone's passwords

    // This is the data that will be sent to the backend to to verify account
    //    if exists:        Send email to user-provided email address
    //    if doesn't exist: setEmailAlert([true, "There is no email address on record that matched what was provided."])


    logger.info("updated Log in POST request Data: ", data)
  })

  return (
    <Container>
      <br />
      <br />

      <Col xs={12} className="d-flex align-items-center justify-content-center">
        <div className="signin-inner my-3 my-lg-0 bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
          <h2 className="text-center text-md-center mb-4 mt-md-0 mb-0">Forgot your password?</h2>
          <p className="mb-4">Enter your email and we will send you a code to reset your password.</p>
          <Form onSubmit={handleForgotPassword}>
            <Form.Group id="email" className="mb-4">
              <FormInput
                autoFocus
                label="Email Address: "
                id="email"
                name="email"
                type="email"
                placeholder="example@company.com"
                register={register}
                validation={{
                  required: "Please enter your email address",
                  maxLength: { value: 60, message: "You exceeded the max email length"}
                }}
                error={errors.email}
              />
            </Form.Group>

            {/* Error logging in */}
            {emailAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{emailAlert[1]}</Alert> : null}

            {/* Error logging in */}
            {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

            <div className="formButtons">
              <button className="w-100 formButton1">
                <Email />
                <span> Recover Password </span>
              </button>
            </div>
          </Form>

          <p className="d-flex justify-content-center align-items-center mt-4">
            <Card.Link as={Link} to="/Login" className="text-black fw-bold">
              <FontAwesomeIcon icon={faAngleLeft} className="me-2" />Back to Login Page
            </Card.Link>
          </p>
        </div>
      </Col>
    </Container>
  );
};
