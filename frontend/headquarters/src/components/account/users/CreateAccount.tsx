// CreateAccount.jsx = Admin creating an account for someone else with pre-set roles and domain permissions

import { useState} from "react";
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome} from '@fortawesome/free-solid-svg-icons';
import { Col, Row, Card, Form, FormCheck, Button, ButtonGroup, Breadcrumb} from '@themesberg/react-bootstrap';
import { PersonAdd, Help} from '@material-ui/icons';
import { FormSelect, FormInput } from '../../forms/FormHelper';
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux"
import {IconButton, Collapse, Tooltip, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import CloseIcon from '@mui/icons-material/Close';

import {BannerAPI, StoreAPI} from '../../api/index';
import { DEFAULT_ADMIN_DOMAIN_ID } from "../../../state/actions/accessActions"
import { LoginAPI } from '../../api/index'
import logger from "../../utils/logger";
import { useRspHandler } from "../../utils/ResponseProvider";
import { useAppSelector } from "../../../state";
import { HTeRole } from "../../../types/access/AccessTypes";
import { ResponseType } from "../../../types/storeTypes";

// =================== CATEGORY DATA ===================
function createData(name : string, accessLevelId : number) : HTeRole{
    return{
        id:accessLevelId,
        name:name,
        description:""
    }
    
}

// =================== CATEGORY DATA ===================

export const CreateAccount = () => {
    const { register, handleSubmit, unregister,  formState: {errors} } = useForm({
        shouldUnregister: true,
        mode: 'all'
    });

    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend or account already exists
    const [showSuccess, setShowSuccess] = useState([false, "Something went wrong!"]) // Successful account creation
    const [currentLevel, setCurrentLevel] = useState(0);         // Current Access Level in Select tag 
    const [currentDomain, setCurrentDomain] = useState(0);         // Current Access Level in Select tag
    const {callbacks, addCallback} = useRspHandler();
    const [open, setOpen] = useState(true);
    const [enabled, setEnabled] = useState(true)
    const [formType, setFormType] = useState("password")

    const dispatch = useDispatch();
    const currentUser = useAppSelector((state) => state.access.account.accessLevel);
    const individualRoles = useAppSelector((state) => state.access.roles);
    const [filteredRoles, setFilteredRoles] = useState<HTeRole[]>([]);         // Current Access Level in Select tag 

    if (filteredRoles.length === 0) {
        Object.values(individualRoles).map((role, index) => {
            role.id < 4000
            &&   setFilteredRoles(filteredRoles => [...filteredRoles, role])
        })
    }

    let selectedRoles : HTeRole[] = []
    let accessLevels : HTeRole[] = []
    let accessDomains : HTeRole[] = []

    if (currentUser > 3000) {
        accessLevels = [
            createData('Admin', 4),
            createData('Banner Manager', 3),
            createData('Store Manager', 2),
            createData('Operator', 1),
            createData('Custom', 0),
        ]

        accessDomains = [
            createData('Admin Level', 2),
            createData('Banner', 1),
            createData('Store', 0),
        ]
    }
    else {
        accessLevels = [
            createData('Banner Manager', 3),
            createData('Store Manager', 2),
            createData('Operator', 1),
            createData('Custom', 0),
        ]

        accessDomains = [
            createData('Banner', 1),
            createData('Store', 0),
        ]
    }

    let bannerList = useAppSelector((state) => state.asset.banners);
    let storeDict = useAppSelector((state) => state.asset.stores);

    // Remove the default banner from select tag
    bannerList = bannerList.filter(banner => 
        banner.bannerId !== "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
    )

    // Remove the default store from select tag
    let storeList = Object.values(storeDict)
    storeList = storeList.filter(store => 
        store.storeId !== "cccccccc-cccc-cccc-cccc-cccccccccccc"
    )

    const handleView = (e : React.ChangeEvent<HTMLSelectElement>) => {
        const accessIndex = accessLevels[e.target.options.selectedIndex - 1].id;
        logger.info("Access Level (CreateAccount): ", accessIndex)

        setCurrentLevel(accessIndex);
        if (accessIndex !== 0)  setCurrentDomain(-1)    // Reset the custom domain level when deselected
    };

    const handleViewDomain = (e : React.ChangeEvent<HTMLSelectElement>) => {
        const accessIndex2 = accessDomains[e.target.options.selectedIndex - 1].id;
        logger.info("Access Index 2 (Create Account): ", accessIndex2)

        setCurrentDomain(accessIndex2);
    };

    const disableKey = (e) => {
        const ev = e ? e : window.event;
     
        if (ev) {
            if   (ev.preventDefault)     ev.preventDefault();
            else                         ev.returnValue = false;         
        }    
    };

    const handleCreate = async (data) => {
        logger.info("Filtered Roles: ", filteredRoles)
        logger.info("Update request data: (Create Account) ", data)
        setShowAlert([false, "Something went wrong!"])
        setShowSuccess([false, "Something went wrong!"])

        // Copy and reverse the array for our usage
        let reversedLevels = accessLevels.slice(0)
        reversedLevels.reverse()

        let updatedData;
        let updatedPhoneNumber = ""
        if (data.phone !== "")    updatedPhoneNumber = data.phone.replaceAll(/[a-zA-Z`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
        switch (reversedLevels[currentLevel].name) {
            case 'Admin':
                updatedData = {
                    accessLevel: 4000,
                    domain: {
                      domainId: DEFAULT_ADMIN_DOMAIN_ID
                    },
                    enabled: data.enabled,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    password: data.password,
                    phoneNumber: updatedPhoneNumber,
                    username: data.email
                }
                break

            case 'Banner Manager':
                logger.info("Actions for Banner Managers")
                updatedData = {
                    accessLevel: 3000,
                    domain: {
                      domainId: data.bannerSelected
                    },
                    enabled: data.enabled,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    password: data.password,
                    phoneNumber: updatedPhoneNumber,
                    username: data.email
                }
                break

            case 'Store Manager':
                logger.info("Actions for Store Managers")
                updatedData = {
                    accessLevel: 2000,
                    domain: {
                      domainId: data.storeSelectedManager
                    },
                    enabled: data.enabled,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    password: data.password,
                    phoneNumber: updatedPhoneNumber,
                    username: data.email
                }
                break

            case 'Operator':
                logger.info("Actions for Operators")
                updatedData = {
                    accessLevel: 1000,
                    domain: {
                      domainId: data.storeSelectedOperator
                    },
                    enabled: data.enabled,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    password: data.password,
                    phoneNumber: updatedPhoneNumber,
                    username: data.email
                }
                break

            case 'Custom':
                logger.info("Actions for Custom roles")
                filteredRoles.map((role, index) => {
                    if (data.customRoles[index])    selectedRoles.push(role)
                })

                if (selectedRoles.length === 0) {
                    setShowAlert([true, "Please select at least one custom permission for this account."])
                }
                
                else {
                    let userDomain = data.domainSelected ? data.domainSelected : DEFAULT_ADMIN_DOMAIN_ID

                    updatedData = {
                        accessLevel: 0,
                        domain: {domainId: userDomain},
                        roles: selectedRoles,
                        enabled: data.enabled,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        password: data.password,
                        phoneNumber: updatedPhoneNumber,
                        username: data.email
                    }
                }
                break

            default: 
                return (null)
        }

        logger.info("updatedData (CreateAccount): ", updatedData)
        function onSuccess(response : ResponseType){
            logger.info("Backend Response (CreateAccount): ", response);
            setShowAlert([false, "Something went wrong!"])
            setShowSuccess([true, "Account created successfully! "])
            setOpen(true)
            setCurrentLevel(0)
            setEnabled(true)
            unregister(`customRoles[]`)

            dispatch(BannerAPI.fetchBanners(Date.now()))
            dispatch(StoreAPI.fetchStores(Date.now()+1))
        }
        function onFail(response : ResponseType){
            logger.error("Create Account not successful", response);
            setOpen(false)
            setShowSuccess([false, "Something went wrong!"])

            if (response.response.errorDescription === "That email address is already registered in the system.") {
                setShowAlert([true, "This email address already exists."])
            }

            else {
                setShowAlert([true, response.response.errorDescription])
            }
        }

        if (updatedData) {
            const newID = Date.now()
            if(addCallback(callbacks, newID, "Create User Account", onSuccess, onFail)){
                dispatch(LoginAPI.createAccount(newID, updatedData))  
            }
        }
    };
    
    return (
    <div className="accountContainer">
        <Row className="align-items-center justify-content-center">
            <div className="mb-4 mb-md-0 ">
                <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/AccountProfile'}}>Profile</Breadcrumb.Item>
                    <Breadcrumb.Item active>Create User Account</Breadcrumb.Item>
                </Breadcrumb>

                <h2>Create User Account</h2>
                <p className="mb-4">Create a new account for other users here. You can also set specific permissions here as well.</p>
            
                <div className="d-flex justify-content-between">

                {/* Manage Users */}
                <ButtonGroup className="assetBar">
                    <Button className="actionBar1" id="actionBar1" size="sm"> 
                        <Link className="buttonLink" id="actionBar1" to = "/UserManagement">
                            <PersonAdd />
                            Manage Users
                        </Link>
                    </Button>
                </ButtonGroup>
                </div>            
            </div>

            <Card border="light" className="bg-white shadow-sm mb-4 form-control">
                <Card.Body>
                    <h4 className="mb-4">General information</h4>
                    <Form className="form-control" onSubmit={handleSubmit(handleCreate)}>
                        <Row>
                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="First Name "
                                    id="firstName"
                                    name="firstName"
                                    type="text"
                                    placeholder="Enter a first name"
                                    register={register}
                                    validation={{
                                        required: "Please enter a first name",
                                        maxLength: { value: 60, message: "You exceeded the max first name length"}
                                    }}
                                    error={errors.firstName}
                                />

                            </Col>

                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="Last Name "
                                    id="lastName"
                                    name="lastName"
                                    type="text"
                                    placeholder="Enter a last name"
                                    register={register}
                                    validation={{
                                        required: "Please enter a last name",
                                        maxLength: { value: 60, message: "You exceeded the max name length"}
                                    }}
                                    error={errors.lastName}
                                />
                            </Col>
                        </Row>

                        <Row>
                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="Email Address "
                                    id="email"
                                    name="email"
                                    type="email"
                                    placeholder="example@company.com"
                                    register={register}
                                    validation={{
                                        required: "Please enter an email address",
                                        maxLength: { value: 60, message: "You exceeded the max email length"}
                                    }}
                                    error={errors.email}
                                />
                            </Col>

                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="Password: "
                                    id="password"
                                    name="password"
                                    type={formType}
                                    placeholder="Password"
                                    register={register}
                                    validation={{
                                    required: "Please enter a password",
                                    minLength: { value: 8,  message: "Password must have at least 8 characters"},
                                    maxLength: { value: 60, message: "You exceeded the max password length"}
                                    }}
                                    error={errors.password}
                                    setFormType={setFormType}
                                />                                
                            </Col>

                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="Phone Number (optional)"
                                    id="phone"
                                    name="phone"
                                    type="tel"
                                    placeholder="999-999-9999"
                                    register={register}
                                    validation={{
                                        // required: "Please enter a phone number",
                                        maxLength: { value: 16, message: "You exceeded the max phone number length"}
                                    }}
                                    error={errors.phone}
                                />
                            </Col>

                            <Col md={6} className="mb-3">
                                <FormCheck  type="checkbox" className="d-flex mb-2 tableTemplate">
                                    <FormCheck.Input defaultChecked id="enabled" className="me-2" 
                                        onClick={(e: any) => setEnabled(e.target.checked)} 
                                        {...register("enabled")}
                                    />
                                    <FormCheck.Label htmlFor="enabled">
                                        Enable Account
                                        <Tooltip className="info" title={<Typography> Disabling an account prevents user from logging in with those credentials</Typography>}>
                                            <IconButton aria-label="enableAccount">
                                                <Help />
                                            </IconButton>
                                        </Tooltip>
                                    </FormCheck.Label>
                                </FormCheck>

                                {errors.enabled && <p className="formError">{errors.enabled.message}</p>}
                            </Col>
                        </Row>

                        <hr />

                        <h4 className="my-4" id="accessLevels">Access Level / Permissions</h4>
                        <Row>
                            <Col sm={4} lg={4} className="mb-3">       

                                {/* TODO?: Use React-Hook-Form Controller for consistency */}
                                <Form.Label>Access level for account:</Form.Label>     
                                <Form.Select required defaultValue="" id="accessLevel1" 
                                    onChange={(e) => handleView(e as React.ChangeEvent<HTMLSelectElement>)}
                                    onKeyDown={(e) => disableKey(e)}
                                    // {...register("accessLevel", {required: "Please select an Access Level"})}
                                >
                                    <option value="" disabled>--Select an Access Level--</option>
                                    
                                    {accessLevels.map(level => (
                                        <option key={level.id} value={level.id}>
                                            {level.name}
                                        </option>
                                    ))}
                                </Form.Select>

                                {/* {errors.accessLevel && <p className="formError">{errors.accessLevel.message}</p>} */}

                                <br />
                                
                                {currentLevel === 3 &&      // Banner Manager
                                <>  
                                    <FormSelect
                                        label="Banner: "
                                        id="bannerSelected"
                                        name="bannerSelected"
                                        placeholder="--Select a Banner--"
                                        dataset={bannerList}
                                        register={register}
                                        objId="bannerId"
                                        objName="bannerName"
                                        disabled={false}
                                        onlyValue={undefined}
                                        validation={{required: "Please select a banner for this manager"}}
                                        error={errors.bannerSelected}
                                    />
                                </>}                                
                                
                                {currentLevel === 2 &&      // Store Manager
                                <>  
                                    <FormSelect
                                        label="Store: "
                                        id="storeSelectedManager"
                                        name="storeSelectedManager"
                                        placeholder="--Select a Store--"
                                        dataset={storeList}
                                        register={register}
                                        objId="storeId"
                                        objName="storeName"
                                        disabled={false}
                                        onlyValue={undefined}
                                        validation={{required: "Please select a store for this manager"}}
                                        error={errors.storeSelectedManager}
                                    />
                                </>}

                                {currentLevel === 1 &&      // Store Operator
                                <>  
                                    <FormSelect
                                        label="Store: "
                                        id="storeSelectedOperator"
                                        name="storeSelectedOperator"
                                        placeholder="--Select a Store--"
                                        dataset={storeList}
                                        register={register}
                                        objId="storeId"
                                        objName="storeName"
                                        disabled={false}
                                        onlyValue={undefined}
                                        validation={{required: "Please select a store for this operator"}}
                                        error={errors.storeSelectedOperator}
                                    />
                                </>}

                                {currentLevel === 0 &&      // Custom
                                <>  
                                    {/* TODO?: Use React-Hook-Form Controller for consistency */}
                                    <Form.Label>Domain Level:</Form.Label>     
                                    <Form.Select required defaultValue="" id="domainLevel" 
                                        onChange={(e) => handleViewDomain(e as React.ChangeEvent<HTMLSelectElement>)}
                                        onKeyDown={(e) => disableKey(e)}
                                        // {...register("accessLevel", {required: "Please select an Access Level"})}
                                    >
                                        <option value="" disabled>--Select a Domain Level--</option>
                                        
                                        {accessDomains.map(domain => (
                                            <option key={domain.id} value={domain.id}>
                                                {domain.name}
                                            </option>
                                        ))}
                                    </Form.Select>

                                    {/* {errors.accessLevel && <p className="formError">{errors.accessLevel.message}</p>} */}

                                    {currentDomain === 2 &&     // Admin Level
                                        <div className="fw-bold">NOTE: This gives the user visibility to everything on the site</div>
                                    }

                                    <br />

                                    {currentDomain === 1 &&     // Banner
                                        <FormSelect
                                            label="Banner: "
                                            id="domainSelected"
                                            name="domainSelected"
                                            placeholder="--Select a Banner--"
                                            dataset={bannerList}
                                            register={register}
                                            objId="bannerId"
                                            objName="bannerName"
                                            disabled = {false}
                                            onlyValue = {undefined}
                                            validation={{required: "Please select a banner for this manager"}}
                                            error={errors.domainSelected}
                                        />
                                    }

                                    {currentDomain === 0 &&     // Store
                                        <FormSelect
                                            label="Store: "
                                            id="domainSelected"
                                            name="domainSelected"
                                            placeholder="--Select a Store--"
                                            dataset={storeList}
                                            register={register}
                                            objId="storeId"
                                            objName="storeName"
                                            disabled = {false}
                                            onlyValue = {undefined}
                                            validation={{required: "Please select a store for this operator"}}
                                            error={errors.domainSelected}
                                        />
                                    }

                                </>}
                            
                                <div className="formButtons">
                                    <button className="formButton1 createButton" type="submit">
                                        <PersonAdd />
                                        <span> Create Account </span> 
                                    </button>
                                </div>

                                <div className="formButtons">
                                    <button className="formButton1 createButton" type="reset" onClick={() => unregister("customRoles[]")}>
                                        <PersonAdd />
                                        <span> RESET </span> 
                                    </button>
                                </div>

                                {/* Error creating account */}
                                {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}
                                
                                {/* Success creating account */}
                                {showSuccess[0] ? 
                                <Collapse in={open}>
                                    <Alert
                                        action={
                                            <IconButton aria-label="close" color="inherit" size="small"
                                                onClick={() => {setOpen(false)}}>
                                                <CloseIcon fontSize="inherit" />
                                            </IconButton>}
                                    >
                                    {showSuccess[1]}
                                        <div>
                                        <br /> 

                                            <Card.Link as={Link} to="/UserManagement" className="fw-bold">
                                                {` Click here to manage users `}
                                            </Card.Link>
                                        </div>
                                    </Alert>
                                </Collapse>
                                : null}
                            </Col>
                            
                            {/* widthChange === Override the CSS for width */}
                            <Col sm={8} lg={8} className="form-control widthChange">
                                {currentLevel !== 0 
                                ?   <div>
                                        <div className={currentLevel === 4 ? "levelSelected" : ""}>
                                            <FormCheck.Label>Admin:</FormCheck.Label>
                                            <li>Full access to the Asset Tree View</li>
                                            <li>Includes all features listed below</li>
                                            <li>Upload / Delete new and existing licenses for HTe</li>
                                            <li>Includes all features listed below</li>
                                            <li>Manage all users</li>
                                            <br />
                                        </div>

                                        <div className={currentLevel === 3 ? "levelSelected" : ""}>
                                            <FormCheck.Label>Banner Manager:</FormCheck.Label>
                                            <li>Full access to their assigned banner in the Asset Tree</li>
                                            <li>Full access to the Scale Profiles page</li>
                                            <li>Ability to upgrade frontend and backend systems</li>
                                            <br />
                                        </div>

                                        <div className={currentLevel === 2 ? "levelSelected" : ""}>
                                            <FormCheck.Label>Store Manager:</FormCheck.Label>
                                            <li>Ability to assign existing Scale Profiles to departments in the Asset Tree in their assigned store</li>
                                            <li>Ability to add / modifify / delete existing HTe operators with a lower access level in their assigned store</li>
                                            <li>Ability to assign and reassign scales to different departments in their assigned store</li>
                                            <li>Ability to enable / disable scales in their assigned store</li>
                                            <li>Includes all features listed below</li>
                                            <br />
                                        </div>

                                        <div className={currentLevel === 1 ? "levelSelected" : ""}>
                                            <FormCheck.Label>Operator:</FormCheck.Label>
                                            <li>Ability to view current status of scales in their assigned store</li>
                                            <li>Ability to send reboot command to a scale in their assigned store</li>
                                            <li>Ability to send manual heartbeat to a scale in their assigned store</li>
                                            <li>Ability to perform status check (ping) to a scale in their assigned store</li>
                                        </div>
                                    </div>

                                :   <div>
                                        <FormCheck.Label style={{fontWeight: "bold", color: "black"}}>Select the custom permissions for this account</FormCheck.Label>
                                        <div className="roleContainer tableTemplate">

                                        {filteredRoles.map((role, index) => (
                                            <div key={`${role}-${index}`}>
                                                <FormCheck type="checkbox" className="d-flex p-1" >
                                                <FormCheck.Label className="px-0 customCheckbox">
                                                    <FormCheck.Input 
                                                        defaultChecked={false}
                                                        {...register(`customRoles[${index}]`)}
                                                    />
                                                        {role.name}

                                                    <Tooltip className="info" title={<Typography> {role.description} </Typography>}>
                                                        <IconButton aria-label="Role Descriptions">
                                                            <Help />
                                                        </IconButton>
                                                    </Tooltip>
                                                </FormCheck.Label>
                                                </FormCheck>
                                            </div>
                                        ))}
                                        </div>
                                        {errors.customRoles && <p className="formError customRoleError">{errors.customRoles.message}</p>}

                                    </div>
                                }

                            </Col>
                        </Row>
                    </Form>
                </Card.Body>
            </Card>
        </Row>
    </div>
    );
};

export default CreateAccount;