import { useState} from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import { Edit, Delete, Block } from '@mui/icons-material';
import { Col, Row, Form, FormCheck } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import { FormSelect, FormInput } from '../../forms/FormHelper';
import { Help } from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { AccessAPI} from '../../api/index'
import { DEFAULT_ADMIN_DOMAIN_ID, USER_ENABLE_DISABLE } from "../../../state/actions/accessActions"
import { LoginAPI } from '../../api/index'
import AccessControl from "../../common/AccessPermissions"
import logger from '../../utils/logger';
import { useRspHandler } from '../../utils/ResponseProvider';
import { useAppSelector } from '../../../state';
import { HTeRole, HTeUser } from '../../../types/access/AccessTypes';
import { Store } from '../../../types/asset/AssetTypes';
import { ResponseType } from '../../../types/storeTypes';


interface CreateDataType{
    name: string, 
    accessLevelId : number, 
    accessLevelValue ?: number | string
}

function createData(name :string, accessLevelId :number, accessLevelValue?:number|string) : CreateDataType{
    if (accessLevelValue === null)  return { name, accessLevelId }
    else                            return { name, accessLevelId, accessLevelValue };
}

interface EditUserProps{
    title:string
    openPopup : boolean
    setOpenPopup : (open: boolean) => void
    currentUser : HTeUser
}

interface EditUserForm{
    firstName: string
    lastName : string
    email: string
    phoneNumber: string
    customRoles: number[]
    updatedPassword : string | undefined

    enabled : boolean
    changePassword: boolean

    domainSelected: string
    bannerSelected : string
    storeSelectedManager: string
    storeSelectedOperator: string
}

// Edit User
const EditUser = (props : EditUserProps) => {
    const { register, handleSubmit, reset,  formState: {errors} } = useForm<EditUserForm>();
    const { title, openPopup, setOpenPopup, currentUser } = props;
    const dispatch = useDispatch();
    const [showAlert, setShowAlert]   = useState([false, "Something went wrong!"])
    const [changePassword, setChangePassword] = useState(false)         
    const [enabled, setEnabled] = useState(true)
    const {callbacks, addCallback} = useRspHandler();


    // User that's accessing the form
    const currentAccount = useAppSelector((state) => state.access.account);

    // User that's being changed
    const [currentAccessLevel, setCurrentAccessLevel] = useState(currentUser.accessLevel);         // Current Access Level in Select tag 
    const individualRoles = useAppSelector((state) => state.access.roles);     // All roles including 4000+ level
    const [filteredRoles, setFilteredRoles] = useState<HTeRole[]>([]);                 // All roles below 4000 level 

    if (filteredRoles.length === 0) {
        Object.values(individualRoles).map((role, index) => {
            role.id < 4000
            &&   setFilteredRoles(filteredRoles => [...filteredRoles, role])
        })
    }
    
    let tmpDomain = -1
    if (currentUser.domain.type === 'ENTITY')       tmpDomain = 2
    else if (currentUser.domain.type === 'BANNER')  tmpDomain = 1
    else if (currentUser.domain.type === 'STORE')   tmpDomain = 0
    else                                            tmpDomain = -1

    const [currentDomain, setCurrentDomain] = useState(tmpDomain);         // Current Access Level in Select tag 

    const domains = useAppSelector((state) => state.access.domains);
    let bannerList = useAppSelector((state) => state.asset.banners);
    let storeDict = useAppSelector((state) => state.asset.stores);

    // Remove the default banner and store from select tag
    bannerList = bannerList.filter(banner => banner.bannerId !== "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa")
    let storeList = Object.values(storeDict)
    storeList = storeList.filter(store => store.storeId !== "cccccccc-cccc-cccc-cccc-cccccccccccc")

    let selectedRoles : HTeRole[] = []
    let accessLevels : CreateDataType[]= []
    let accessDomains : CreateDataType[]= []
    let hasRole : boolean               // Used to check roles by default if user has them already
    let bannerLimit : number         // bannerLimit is for bannerManagers editing other accounts in their table 
    let storeLimit : number            // storeLimit is for storeManagers editing other accounts in their table 
    let filteredStoreList : Store[] = []

    currentAccount.domain.type === "BANNER" 
        ? bannerLimit = bannerList.map(r => {return r.bannerId}).indexOf(currentAccount.domain.domainId) 
        : bannerLimit = -1

    currentAccount.domain.type === "STORE" 
        ? storeLimit = storeList.map(r => {return r.storeId}).indexOf(currentUser.domain.domainId) 
        : storeLimit = -1

    Object.keys(domains).forEach(key => {
        if (domains[key].type === "BANNER" && currentAccount.domain.domainId === key) {
            Object.keys(storeDict).some(sKey => {
                let parent = domains[sKey].parentId ?? ""
                let grandParent = domains[parent].parentId ?? ""
                if (grandParent && grandParent === key) {
                    let filteredStore = storeList.find(store => store.storeId === sKey)
                    if (filteredStore) {
                        filteredStoreList.push(filteredStore)
                    }
                }
            })
        }
    })

    if (currentAccount.accessLevel >= 5000) {
        accessLevels = [
            createData('Hobart', 5, 5000),
            createData('Admin', 4, 4000),
            createData('Banner Manager', 3, 3000),
            createData('Store Manager', 2, 2000),
            createData('Operator', 1, 1000),
            createData('Custom', 0, 0),
        ]

        accessDomains = [
            createData('Admin Level', 2, 'ENTITY'),
            createData('Banner', 1, 'BANNER'),
            createData('Store', 0, 'STORE'),
        ]
    }
    else if (currentAccount.accessLevel >= 4000) {
        accessLevels = [
            createData('Admin', 4, 4000),
            createData('Banner Manager', 3, 3000),
            createData('Store Manager', 2, 2000),
            createData('Operator', 1, 1000),
            createData('Custom', 0, 0),
        ]

        accessDomains = [
            createData('Admin Level', 2, 'ENTITY'),
            createData('Banner', 1, 'BANNER'),
            createData('Store', 0, 'STORE'),
        ]
    }
    else if (currentAccount.accessLevel >= 3000) {
        accessLevels = [
            createData('Banner Manager', 3, 3000),
            createData('Store Manager', 2, 2000),
            createData('Operator', 1, 1000),
            createData('Custom', 0, 0),
        ]

        accessDomains = [
            createData('Banner', 1, 'BANNER'),
            createData('Store', 0, 'STORE'),
        ]
    }

    else {
        accessLevels = [
            createData('Store Manager', 2, 2000),
            createData('Operator', 1, 1000),
        ]

        accessDomains = [
            createData('Store', 0, 'STORE'),
        ]
    }

    const defaultValues = {
        firstName: currentUser.firstName,
        lastName: currentUser.lastName,
    };

    const handleView = (e : any) => {
        const accessIndex = accessLevels[e.target.options.selectedIndex - 1].accessLevelId;
        setCurrentAccessLevel(accessIndex)
        // logger("accessIndex1: ", accessIndex)
    };

    const handleViewDomain = (e: any) => {
        const accessIndex2 = accessDomains[e.target.options.selectedIndex - 1].accessLevelId;
        setCurrentDomain(accessIndex2);
        // logger("accessIndex2: ", accessIndex2)
    };

    const disableKey = (e : any) => {
        const ev = e ? e : window.event;
     
        if (ev) {
            if   (ev.preventDefault)     ev.preventDefault();
            else                         ev.returnValue = false;         
        }    
    };


    const handleCloseDialog = (event : any) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);

        event.stopPropagation()
        reset(defaultValues);
    }

    const handleAdd = handleSubmit(async (data : EditUserForm) => {    // When the Submit button is pressed
        logger.info("POST request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        if (data.customRoles) {
            filteredRoles.map((role, index) => {
                if (data.customRoles[index])    selectedRoles.push(role)
            })
        }



        logger.info("selectedRoles in submit: ", selectedRoles)    

        let updatedPhoneNumber = ""
        if (data.phoneNumber !== "")    updatedPhoneNumber = data.phoneNumber.replaceAll(/[a-zA-Z`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
        let updatedData : Partial<HTeUser> = {
            id: currentUser.id,
            firstName: data.firstName,
            lastName: data.lastName,
            username: currentUser.username,
            phoneNumber: updatedPhoneNumber
        };
        
        switch (currentAccessLevel) {
            case 5:     // Hobart
            case 5000:
                logger.info("Actions for Hobart level, don't need this for normal usage though")
                updatedData.accessLevel = 5000
                updatedData.domain = domains[DEFAULT_ADMIN_DOMAIN_ID]
                break
            case 4:     // Admin
            case 4000:
                logger.info("Actions for Admins")
                updatedData.accessLevel = 4000
                updatedData.domain = domains[DEFAULT_ADMIN_DOMAIN_ID]
                break

            case 3:     // Banner Manager
            case 3000:
                logger.info("Actions for Banner Managers")
                updatedData.accessLevel = 3000
                updatedData.domain = domains[data.bannerSelected]
                break

            case 2:     // Store Manager
            case 2000:
                logger.info("Actions for Store Manager")
                updatedData.accessLevel = 2000
                updatedData.domain = domains[data.storeSelectedManager]
                break

            case 1:     // Operator
            case 1000:
                logger.info("Actions for Operators")
                updatedData.accessLevel = 1000
                updatedData.domain = domains[data.storeSelectedOperator]
                break

            case 0:
                logger.info("Actions for Custom roles")

                if (selectedRoles.length === 0) {
                    setShowAlert([true, "Please select at least one custom permission for this account."])
                }
                
                else {
                    let userDomain = data.domainSelected ? data.domainSelected : DEFAULT_ADMIN_DOMAIN_ID
                    updatedData.accessLevel = 0
                    updatedData.domain = domains[userDomain]
                }
                break

            default: 
                break
        }

        if (data.updatedPassword) {
            const tempPass = {password: data.updatedPassword}
            logger.info("tempPass (UserForm): ", tempPass)
            updatedData = Object.assign(updatedData, tempPass)
        }

        logger.info("updated POST request Data [UPDATED DATA]: ", updatedData)

        function onSuccess(response : ResponseType){
            logger.info("Backend Response (User Form): ", response);
            reset(defaultValues);
            setShowAlert([false, "Something went wrong!"])
            setOpenPopup(false);
            setEnabled(true)

            let id = Date.now()
            dispatch(AccessAPI.getUserInfo(id))
            dispatch(AccessAPI.getRoles(id +1))
            dispatch(AccessAPI.getDomains(id +2))
        }
        function onFail(response : ResponseType){
            logger.error("Backend Response (User Form): ", response);
            setShowAlert([true, "Error trying to update file. Please try again later."])
        }

        if (updatedData) {
            const reqEditID = Date.now()

            if(addCallback(callbacks, reqEditID, "Edit User", onSuccess, onFail)){
                dispatch(LoginAPI.createAccount(reqEditID, updatedData)) 
            }
                  
            if (data.enabled !== undefined)   dispatch(LoginAPI.enableUser(reqEditID, currentUser, data.enabled))
            if (currentAccount.id !== currentUser.id && currentAccount.accessLevel > 2000)   dispatch(LoginAPI.assignRoles(reqEditID, currentUser, selectedRoles))
        }
    })
    
    return (
        <>
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title} - <span style={{fontWeight: "bold"}}>{`${currentUser.firstName} ${currentUser.lastName}`}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                <Row>
                    <Col md={6} className="mb-3">
                    <FormInput
                        defaultValue={currentUser.firstName}
                        label="First Name: "
                        id="firstName"
                        name="firstName"
                        type="text"
                        placeholder="Enter a first name"
                        register={register}
                        validation={{ 
                            required: "Please enter a first name", 
                            maxLength: { value: 60, message: "You exceeded the max name length"}
                        }}
                        error={errors.firstName}
                    />
                    </Col>

                    <Col md={6} className="mb-3">
                    <FormInput
                        defaultValue={currentUser.lastName}
                        label="Last Name: "
                        id="lastName"
                        name="lastName"
                        type="text"
                        placeholder="Enter a last name"
                        register={register}
                        validation={{ 
                            required: "Please enter a last name", 
                            maxLength: { value: 60, message: "You exceeded the max name length"}
                        }}
                        error={errors.lastName}
                    />  
                    </Col>
                </Row>

                <Row>
                    <Col md={6} className="mb-3">
                    <FormInput
                        disabled    // Do not allow users to change email, we don't support it as of now
                        defaultValue={currentUser.username}
                        label="Email Address: "
                        id="email"
                        name="email"
                        type="email"
                        register={register}
                        validation={{ 
                            maxLength: { value: 60, message: "You exceeded the max email length"}
                        }}
                        error={errors.email}
                    />
                    </Col>

                    <Col md={6} className="mb-3">
                    <FormInput
                        defaultValue={currentUser.phoneNumber}
                        label="Phone Number (optional): "
                        id="phoneNumber"
                        name="phoneNumber"
                        type="tel"
                        placeholder="Not set"
                        pattern="[0-9]{10}"
                        register={register}
                        validation={{
                            minLength: { value: 10, message: "Please enter a valid phone number length"},
                            maxLength: { value: 16, message: "You exceeded the max phone number length"}
                        }}
                        error={errors.phoneNumber}
                    />
                    </Col>


                    <Col md={6} className="mb-3">
                    {/* Check if current account can change current user's password */}
                    {(currentAccount.domain.type !== currentUser.domain.type || currentAccount.id === currentUser.id) 
                    && currentAccount.domain.domainId === DEFAULT_ADMIN_DOMAIN_ID ?
                    <>
                    <FormCheck type="checkbox" className="d-flex tableTemplate" >
                        <FormCheck.Label className="customCheckbox">
                            <FormCheck.Input className="me-2" onClick={(e : React.FormEvent<HTMLInputElement>) => setChangePassword(e.currentTarget.checked)} {...register("changePassword")}/>
                            Change User's Password
                        </FormCheck.Label>
                    </FormCheck>
                    <br />
                    {changePassword ?
                        <FormInput
                            label="Password: "
                            id="updatedPassword"
                            name="updatedPassword"
                            type="password"
                            placeholder="New Password"
                            register={register}
                            validation={{
                            required: "Please enter a password",
                            minLength: { value: 8,  message: "Password must have at least 8 characters"},
                            maxLength: { value: 60, message: "You exceeded the max password length"}
                            }}
                            error={errors.updatedPassword}
                        />   : null
                    }
                    </> : null}                
                    </Col>

                    {/* Role 1007 (UserEnableDisable) */}
                    {currentAccount.id !== currentUser.id
                    ?   <AccessControl
                            user = {currentAccount}
                            requiredRole = {individualRoles[USER_ENABLE_DISABLE]}
                            requiredDomain = {currentUser.domain}
                        >
                            <Col md={6} className="mb-3">
                                <FormCheck  type="checkbox" className="d-flex tableTemplate">
                                    <FormCheck.Input defaultChecked={currentUser.enabled} id="enabled" className="me-2" 
                                        onClick={(e : React.FormEvent<HTMLInputElement>) => setEnabled(e.currentTarget.checked)} 
                                        {...register("enabled")}
                                    />
                                    <FormCheck.Label htmlFor="enabled">
                                        Enable Account
                                        <Tooltip className="info" title={<Typography> Disabling an account prevents user from logging in with those credentials</Typography>}>
                                            <IconButton aria-label="enableAccount">
                                                <Help />
                                            </IconButton>
                                        </Tooltip>
                                    </FormCheck.Label>
                                </FormCheck>

                                {errors.enabled && <p className="formError">{errors.enabled.message}</p>}
                            </Col>
                        </AccessControl>
                    :   null}
                </Row>

                    <Form.Label>Access level for account:</Form.Label>     
                    <Form.Select required defaultValue={currentAccessLevel} id="accessLevel" 
                        onChange={(e) => handleView(e)}
                        onKeyDown={(e) => disableKey(e)}
                        // {...register("accessLevel", {required: "Please select an Access Level"})}
                    >
                        <option value="" disabled>--Select an Access Level--</option>
                        
                        {accessLevels.map(level => (
                            <option key={level.accessLevelId} value={level.accessLevelValue}>
                                {level.name}
                            </option>
                        ))}
                    </Form.Select>

                    {(currentAccessLevel === 5 || currentAccessLevel >= 5000) &&        // Hobart
                    <></>}

                    {(currentAccessLevel === 4  || currentAccessLevel === 4000) &&      // Admin
                    <>
                        <span className="fw-bold">NOTE: This gives the user visibility to everything on the site</span>
                        <br />    
                    </>}

                    <br />

                    {(currentAccessLevel === 3 || currentAccessLevel === 3000) &&      // Banner Manager
                    <>  
                        <FormSelect
                            customdefault={currentUser.domain.type === "BANNER" ? currentUser.domain.domainId : null} 
                            label="Banner: "
                            id="bannerSelected"
                            name="bannerSelected"
                            placeholder="--Select a Banner--"
                            dataset={currentAccount.domain.type === "BANNER" 
                                ? [bannerList[bannerLimit]] 
                                : bannerList}
                            register={register}
                            objId="bannerId"
                            objName="bannerName"
                            disabled = {false}
                            onlyValue = {undefined}
                            validation={{required: "Please select a banner for this manager"}}
                            error={errors.bannerSelected}
                        />
                    </>}  

                    {(currentAccessLevel === 2 || currentAccessLevel === 2000) &&      // Store Manager
                    <>  
                        <FormSelect
                            customdefault={currentUser.domain.type === "STORE" ? currentUser.domain.domainId : null} 
                            label="Store: "
                            id="storeSelectedManager"
                            name="storeSelectedManager"
                            placeholder="--Select a Store--"
                            dataset={currentAccount.domain.type === "BANNER" 
                                ? filteredStoreList
                                : currentAccount.domain.type === "STORE"
                                    ? [storeList[storeLimit]] 
                                    : storeList}
                            register={register}
                            objId="storeId"
                            objName="storeName"
                            disabled = {false}
                            onlyValue = {undefined}
                            validation={{required: "Please select a store for this manager"}}
                            error={errors.storeSelectedManager}
                        />
                    </>}

                    {(currentAccessLevel === 1 || currentAccessLevel === 1000) &&      // Store Operator
                    <>  
                        <FormSelect
                            customdefault={currentUser.domain.type === "STORE" ? currentUser.domain.domainId : null} 
                            label="Store: "
                            id="storeSelectedOperator"
                            name="storeSelectedOperator"
                            placeholder="--Select a Store--"
                            dataset={currentAccount.domain.type === "BANNER" 
                                ? filteredStoreList
                                : currentAccount.domain.type === "STORE"
                                    ? [storeList[storeLimit]] 
                                    : storeList}
                            register={register}
                            objId="storeId"
                            objName="storeName"
                            disabled = {false}
                            onlyValue = {undefined}
                            validation={{required: "Please select a store for this operator"}}
                            error={errors.storeSelectedOperator}
                        />
                    </>}

                    {currentAccessLevel === 0 &&      // Custom
                    <>  
                        {/* TODO?: Use React-Hook-Form Controller for consistency */}
                        <Form.Label>Domain Level:</Form.Label>     
                        <Form.Select required defaultValue={currentUser.domain.type} id="domainLevel" 
                            onChange={(e) => handleViewDomain(e)}
                            onKeyDown={(e) => disableKey(e)}
                            // {...register("accessLevel", {required: "Please select an Access Level"})}
                        >
                            <option value="" disabled>--Select a Domain Level--</option>
                            
                            {accessDomains.map(domain => (
                                <option key={domain.accessLevelId} value={domain.accessLevelValue}>
                                    {domain.name}
                                </option>
                            ))}
                        </Form.Select>

                        {/* {errors.accessLevel && <p className="formError">{errors.accessLevel.message}</p>} */}

                        {currentDomain === 2 &&     // Admin Level
                            <div className="fw-bold">NOTE: This gives the user visibility to everything on the site</div>
                        }

                        <br />

                        {currentDomain === 1 &&     // Banner
                        <>
                            <FormSelect
                                customdefault={currentUser.domain.type === "BANNER" ? currentUser.domain.domainId : null} 
                                label="Banner: "
                                id="domainSelected"
                                name="domainSelected"
                                placeholder="--Select a Banner--"
                                dataset={currentAccount.domain.type === "BANNER" 
                                    ? [bannerList[bannerLimit]] 
                                    : bannerList}
                                register={register}
                                objId="bannerId"
                                objName="bannerName"
                                disabled = {false}
                                onlyValue = {undefined}
                                validation={{required: "Please select a banner for this manager"}}
                                error={errors.domainSelected}
                            />
                            <br />
                        </>}

                        {currentDomain === 0 &&     // Store
                        <>
                            <FormSelect
                                customdefault={currentUser.domain.type === "STORE" ? currentUser.domain.domainId : null} 
                                label="Store: "
                                id="domainSelected"
                                name="domainSelected"
                                placeholder="--Select a Store--"
                                dataset={currentAccount.domain.type === "BANNER" 
                                    ? filteredStoreList
                                    : currentAccount.domain.type === "STORE"
                                        ? [storeList[storeLimit]] 
                                        : storeList}
                                register={register}
                                objId="storeId"
                                objName="storeName"
                                disabled = {false}
                                onlyValue = {undefined}
                                validation={{required: "Please select a store for this operator"}}
                                error={errors.domainSelected}
                            />
                            <br />
                        </>}
                    </>}

                    {/* Check current account to see if they should see the role selector */}
                    {/* if currentUser === (hobart || admin) ==> hide the roles, they shouldn't need them */}
                    {currentAccount.id !== currentUser.id && (currentAccount.accessLevel >= 3000 && 
                        (currentAccessLevel !== 4 && currentAccessLevel !== 5 && currentAccessLevel !== 4000 && currentAccessLevel !== 5000)
                        || (currentAccount.accessLevel === 0 && currentAccount.domain.type !== "STORE"))
                    &&
                    <>
                    <FormCheck.Label>Add Roles to User (optional)</FormCheck.Label>
                    <div className="roleContainer tableTemplate">

                    {filteredRoles.map((role, index) => (
                        <div>
                            {hasRole = currentUser.roles.some(r => r.id === role.id) ? true : false} 

                            <FormCheck key={`${role}-${index}`} type="checkbox" className="d-flex p-1" >
                            <FormCheck.Label className="px-0 customCheckbox">
                                <FormCheck.Input defaultChecked={hasRole} 
                                    {...register(`customRoles.${index}`)}
                                />
                                {role.name}

                                <Tooltip className="info" title={<Typography> {role.description} </Typography>}>
                                    <IconButton aria-label="Role Descriptions">
                                        <Help />
                                    </IconButton>
                                </Tooltip>
                            </FormCheck.Label>
                            </FormCheck>
                        </div>
                    ))}
                    </div></>}

                    <br />

                    {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <Edit />
                            <span> Edit User </span>
                        </button>

                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
        </>     
    );
}

interface DeleteUserProps{
    title: string 
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    currentUser: HTeUser | undefined
}

// Delete User
let delId = null
function DeleteUser(props : DeleteUserProps) {
    const { handleSubmit } = useForm();
    // const { title, openPopup, setOpenPopup, fileName, fileDescription, fileId, profileName } = props;
    const { title, openPopup, setOpenPopup, currentUser } = props;
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();

    logger.info("(DeleteUser) currentUser " , currentUser)

    const handleCloseDialog = (event) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);

        event.stopPropagation()
    }

    const handleDelete = async (data) => {
        logger.info("DELETE request data: (Delete User) ", data)
        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response){
            logger.info("Backend Response (DELETE User): ", response);
            setShowAlert([false, "Something went wrong!"])
            setOpenPopup(false);
            let id = Date.now()
            dispatch(AccessAPI.getUserInfo(id+1))
            dispatch(AccessAPI.getRoles(id+2))
            dispatch(AccessAPI.getDomains(id+3))
        }
        function onFail(response){
            logger.error("Backend Response (DELETE User): ", response);
            setShowAlert([true, "Error trying to delete account. Please try again later."])
        }
        if(addCallback(callbacks, newID, "Delete Account", onSuccess, onFail)){
            dispatch(LoginAPI.deleteAccount(newID, currentUser ))
        }        
    };

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title} - <span style={{fontWeight: "bold"}}>{currentUser ? currentUser.username : "Unknown"}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>
                    <div className="formSpacing">
                        <Form.Label id="confirmDelete" style={{color: "black"}}>Are you sure you want to delete the following User:</Form.Label>
                        <p>Username: <span style={{fontWeight: "bold"}}>{currentUser ? currentUser.username : "User Not Found"}</span></p>
                        <p>Name:     <span style={{fontWeight: "bold"}}>{currentUser ? `${currentUser.firstName} ${currentUser.lastName}` : "User Not Found"}</span></p>
                        
                        <br />
                        
                        <Alert className="m-1" variant="outlined" severity="error">
                            <p className="fw-bold"> Warning: Any user that is deleted cannot be recovered. </p>
                        </Alert>

                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleSubmit(handleDelete)} type="submit" value="Delete File" >
                                <Delete />
                                <span> Delete User </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

// Display Roles
// let displayRoleId = null
function DisplayRoles(props) {
    // const { handleSubmit } = useForm();
    const { title, openPopup, setOpenPopup, currentUser } = props;
    // const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])

    logger.info("currentUser in displayRole" , currentUser)

    const handleCloseDialog = (event) => {
        // setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);

        event.stopPropagation()
    }

    

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title} - <span style={{fontWeight: "bold"}}>{currentUser.username}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control">
                    <div className="formSpacing">

                        {currentUser.roles.length === 0 
                            ?  <p className="fw-bold" style={{color: "black"}}>User does not currently have any roles</p>
                            :   currentUser.roles.map((role, index) => (
                                <>
                                    <p className="fw-bold" style={{color: "black"}}>{role.name}</p>
                                    <li key={index}>{role.description}</li>
                                    <br />
                                </>
                            ))
                        }

                        <br />

                        {/* {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null} */}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}


export { EditUser, DeleteUser, DisplayRoles };
