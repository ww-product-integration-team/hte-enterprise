import { useState, useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom'
import { useDispatch } from "react-redux"
import { Button, ButtonGroup, Breadcrumb} from '@themesberg/react-bootstrap';
import { PersonAdd } from "@mui/icons-material";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { AccessAPI} from "../../api/index"
import { TableTemplate, TableTemplateHeadCellType } from "../../common/TableTemplate";
import { EditUser, DeleteUser, DisplayRoles } from './UserForm';
import { DEFAULT_ADMIN_DOMAIN_ID } from "../../../state/actions/accessActions"
import logger from '../../utils/logger';
import { useAppSelector } from '../../../state';
import { HTeUser } from '../../../types/access/AccessTypes';

// Column Names
const headCells : TableTemplateHeadCellType[] = [
  { id: "actions", narrow: false, disablePadding: true, label: 'Actions', mandatory: true},
  { id: 'firstName', narrow: false, disablePadding: false, label: 'First Name' , mandatory: false},
  { id: 'lastName', narrow: false, disablePadding: false, label: 'Last Name' , mandatory: false},
  { id: "username", narrow: false, disablePadding: false, label: 'Email Address' , mandatory: true},
  { id: "phoneNumber", narrow: false, disablePadding: false, label: 'Phone Number' , mandatory: false},
  { id: "enabled", narrow: true, disablePadding: false, label: 'Enabled' , mandatory: false},
  { id: "accessLevel", narrow: false, disablePadding: false, label: 'Access Level' , mandatory: false},
  { id: "domain", narrow: false, disablePadding: false, label: 'Assigned Domain' , mandatory: false},
  { id: "roles", narrow: false, disablePadding: false, label: 'Roles' , mandatory: false},
  { id: "dateCreated", narrow: false, disablePadding: false, label: 'Date Created' , mandatory: false},
];

interface UserManagementProps{

}

export const UserManagement = (props : UserManagementProps) => {

  const [openUserEdit, setOpenUserEdit] = useState(false);              // PUT Request
  const [openUserDelete, setOpenUserDelete] = useState(false);          // DELETE Request
  const [openUserRoles, setOpenUserRoles] = useState(false);          // Display Roles
  const [selectedUser, setSelectedUser] = useState<HTeUser>();                   // User to be edited
  const [filteredUserList, setFilteredUserList] = useState<HTeUser[]>([]);         // List of users that is visable to the current user

  const history = useHistory();
  const dispatch = useDispatch();
  const userList = useAppSelector((state) => state.access.users);
  const domains = useAppSelector(state=>state.access.domains)
  const currentUser = useAppSelector((state) => state.access.account);
  const loggedIn = useAppSelector((state) => state.access.loggedIn);

  // useState that contains array of dispatched URLs
  const [dispatchedUrls, setDispatchedUrls] = useState<any>([AccessAPI.getUserInfo, AccessAPI.getRoles, AccessAPI.getDomains])

  let tableName = "Users";          // Name of the current profile
  let tableDescription = `This table contains all users within your domain `;   // Description of the current profile
  // let tableDescription = `This table contains all users within the following domain: ${currentUser.domain} `;   // Description of the current profile

  
  const handleUserEdit = (file :  HTeUser) => {
    setSelectedUser(file);
    setOpenUserEdit(true);
  };

  const handleDeleteUser = (file : HTeUser) => {
    setSelectedUser(file);
    setOpenUserDelete(true);
  };

  const handleDisplayRoles = (file : HTeUser) => {
    setSelectedUser(file);
    setOpenUserRoles(true);
  };

  const addUser = (user: HTeUser, tempReplacement) => {
    if (filteredUserList.some(r => JSON.stringify(r) === JSON.stringify(user))) {
      // Already in the list (unmodified), do nothing
    } else if (filteredUserList.some(r => JSON.stringify(r) !== JSON.stringify(user) && r.id === user.id)) {
      // Already in the list (modified), replace existing entry with new information
      tempReplacement = filteredUserList.map(r => r.id).indexOf(user.id)
      const tempo = [...filteredUserList]
      tempo.splice(tempReplacement, 1, user)
      setFilteredUserList(tempo)
    } else {
      // Not in the list, add to the list
      setFilteredUserList(filteredUserList => [...filteredUserList, user])
    }
  }

  const handlePermissionCheck = () => {
    try {
      let tempReplacement 
      
      // TODO: (Check 1003 [manageOperators] and 2004 [manageSupervisors] at some point)

      if (currentUser.accessLevel >= 2000 || currentUser.accessLevel === 0) {   // Check if at least a store manager or custom
        if (currentUser.domain.type === "ENTITY" && currentUser.domain.domainId === DEFAULT_ADMIN_DOMAIN_ID) {
          setFilteredUserList(userList)
        }

        // if adding Region Domains, add check between Banner and Store
        if (currentUser.domain.type === "BANNER") {
          logger.info("handlePermissionCheck")

          Object.keys(domains).forEach(key => {
            userList.forEach(user => {
              let grandparent = user.domain.parentId ? domains[user.domain.parentId].parentId : ""
              if (domains[key].type === "BANNER") {
                if ((user.domain.type === "BANNER" && user.domain.domainId === key) || (user.domain.type === "STORE" && grandparent === key)) {
                  addUser(user, tempReplacement)
                }
              } else if (domains[key].type === "STORE") {
                if (user.domain.type === "STORE" && grandparent === key) {
                  addUser(user, tempReplacement)
                }
              }
            })
          })
        } else if (currentUser.domain.type === "STORE") {
          Object.keys(domains).forEach(key => {
            if (domains[key].type === "STORE") {
              userList.forEach(user => {
                if (user.domain.type === "STORE" && user.domain.domainId === key) {
                  addUser(user, tempReplacement)
                }
              })
            }
          })
        }
      } else {
        logger.error("not successful: User does not have the required Access Level");
        history.push("/NotPermitted")
      }
    }

    catch (error) {
      // The catch block gets ran on page load, bootleg check to see if user is logged in
      if (loggedIn === false) {
        history.push("Login")
      }
    }
  }

  useEffect(() => {
    setFilteredUserList([])
    handlePermissionCheck()
    logger.error("filteredList: ", filteredUserList)
  }, [userList]);

  useEffect(() => {
    let id = Date.now()
    dispatch(AccessAPI.getUserInfo(id))
    dispatch(AccessAPI.getRoles(id + 1))
    dispatch(AccessAPI.getDomains(id + 2))
    handlePermissionCheck()
  }, []);

  useEffect(() => {
    if (filteredUserList.length === 0) handlePermissionCheck()
  }, [filteredUserList]);

  return (
    <> 
    <div className="mb-4 mb-md-0">
        <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
            <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
            <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/AccountProfile'}}>Profile</Breadcrumb.Item>
            <Breadcrumb.Item active>User Management</Breadcrumb.Item>
        </Breadcrumb>

        <h2>User Management</h2>
        <p className="mb-4">Here are all of the user accounts in the system.</p>

        <div className="d-flex justify-content-between">

          {/* Create New Account */}
          <ButtonGroup className="assetBar">
              <Button className="actionBar1" id="actionBar1" size="sm"> 
                  <Link className="buttonLink" id="actionBar1" to = "/CreateAccount">
                      <PersonAdd />
                      Create New Account
                  </Link>
              </Button>
          </ButtonGroup>
        </div>
    </div>

    <div className="form-control">
      <TableTemplate 
        defaultOrderBy="firstName"                  // Default ordering by this column name
        handleEditFile={handleUserEdit}             // Edit File function for ViewHT
        handleDeleteFile={handleDeleteUser}         // Delete File function for ViewHT
        displayRoles={handleDisplayRoles}           // Display a user's roles button for UserManagement.jsx
        headCells={headCells}                       // Column headers
        dataSet={filteredUserList}                  // Array of all data from backend
        recordId="id"                               // Unique Key for each record in table
        tableName={tableName}                       // Header / Name
        tableDescription={tableDescription}         // Subheader
        dispatchedUrls={dispatchedUrls}             // What is dispatched
        searchPlaceholder="Search for a user"       // Placeholder in Search Bar
        currentUser={currentUser}                   // Account currently logged in
        customRowsPerPage={25}                      // # of rows shown by default
        // searchFilter={["firstName", "lastName", "username"]}   // This was a test to search multiple columns
      />

      <div className="viewHT">
        <>
        {openUserEdit && selectedUser ?
          <EditUser
            title="Edit User"
            openPopup={openUserEdit}
            setOpenPopup={setOpenUserEdit}
            currentUser={selectedUser}
          />
          : null
        }
        
        {openUserDelete &&
          <DeleteUser 
            title="Delete Account"
            openPopup={openUserDelete}
            setOpenPopup={setOpenUserDelete}
            currentUser={selectedUser}
          />
        }

        {openUserRoles &&
          <DisplayRoles 
            title="User's Current Roles"
            openPopup={openUserRoles}
            setOpenPopup={setOpenUserRoles}
            currentUser={selectedUser}
          />
        }
        </>

      </div>
    </div>
    </>
  );
}

export default UserManagement;
