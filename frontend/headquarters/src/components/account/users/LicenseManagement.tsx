import { useDispatch } from "react-redux"
import VirtualTable, { HeadCellTypes } from "../../common/virtualTable/VirtualTable"
import { useAppSelector } from "../../../state"
import { AccessAPI, LicenseAPI } from "../../api"
import { Breadcrumb, Button } from "react-bootstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Link } from "react-router-dom"
import { faHome } from "@fortawesome/free-solid-svg-icons"
import { PostAdd, Place, Cloud, CloudDownload } from "@material-ui/icons"
import { useEffect, useState } from "react"
import { Dialog, DialogContent, DialogTitle, Typography } from "@material-ui/core"
import { RequestEnterpriseLicense } from "../../licenses/LicenseForm"
import { License } from "../../../types/access/HTeLicense"
import { ButtonGroup, Col, Row } from "@themesberg/react-bootstrap"
import { ResponseType } from "../../../types/storeTypes"
import { useRspHandler } from "../../utils/ResponseProvider"
import logger from "../../utils/logger"
import { notify } from "reapop"

const licenseHeadCells: HeadCellTypes[] = [
    {id: "checkmark", label: "", sorted: false, searchable: false, isShowing: true, mandatory: true},
    {id: "fileName", label: "File", sorted: false, searchable: true, isShowing: true, mandatory: true},
    {id: "ownerEmail", label: "Owner Email", sorted: false, searchable: true, isShowing: true, mandatory: false},
    {id: "fullName", label: "Owner Name", sorted: false, searchable: true, isShowing: true, mandatory: false},
    {id: "numUsers", label: "# of Users", sorted: false, searchable: false, isShowing: true, mandatory: false},
    {id: "numScales", label: "# of Scales", sorted: false, searchable: false, isShowing: true, mandatory: false},
    {id: "productNames", label: "Products", sorted: false, searchable: true, isShowing: true, mandatory: false},
    {id: "activationDate", label: "Start Date", sorted: true, searchable: false, isShowing: true, mandatory: true},
    {id: "timeoutDate", label: "End Date", sorted: true, searchable: false, isShowing: true, mandatory: true}
]

const LicenseManagement = (props) => {
    const dispatch = useDispatch()
    const { callbacks, addCallback } = useRspHandler()
    const account = useAppSelector(state=>state.access.account)
    useEffect(() => {
        dispatch(AccessAPI.getEnterpriseLicense())
        LicenseAPI.setEmail(account.username)
        dispatch(LicenseAPI.getLicenses(Date.now()))
    }, [])
    const licenses = useAppSelector(state=>state.access.licenses)
    const cloudLicenses = useAppSelector(state=>state.access.cloudLicenses)
    
    let localLicesnesClone = structuredClone(licenses)
    let cloudLicensesClone = structuredClone(cloudLicenses)
    const addTimeout = (toModify: License[]) => {
        for (let license of toModify) {
            let actDate = new Date(license.activationDate)
            let timeoutDate = actDate.getTime() + (license.timeout * 8.64e7)
            license["timeoutDate"] = timeoutDate
        }
    }
    addTimeout(localLicesnesClone)
    addTimeout(cloudLicensesClone)

    const [selected, setSelected] = useState<string[]>([])
    const [cloudSelected, setCloudSelected] = useState<string[]>([])
    const [tableView, setTableView] = useState<"local" | "cloud">("local")
    const [requestDialog, setRequestDialog] = useState(false)
    const [licenseInfo, setLicenseInfo] = useState()

    useEffect(() => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully saved cloud license")
            setLicenseInfo(undefined)
        }
        function onFail(response: ResponseType) {
            logger.error("Error saving cloud license")
            setLicenseInfo(undefined)
        }
        if (licenseInfo) {
            let blob = new Blob([licenseInfo], {type: "text/plain"})
            const fileData = new FormData()
            let cloudLicense = cloudLicenses.find(lic => lic.licenseId === cloudSelected[0])
            fileData.append('file', blob, cloudLicense?.fileName)

            const newID = Date.now()
            if (addCallback(callbacks, newID, "Save License File", onSuccess, onFail)) {
                dispatch(AccessAPI.installLicense(newID, fileData, false))
            }
        }
    }, [licenseInfo])

    const downloadLicense = () => {
        if (cloudSelected.length > 1 || cloudSelected.length === 0) {
            logger.info("Please select only one cloud license to download")
            dispatch(notify("Please select only one cloud license to download", "warning"))
            return
        }

        function onSuccess(response: ResponseType) {
            logger.info("Successfully downloaded cloud license: " + cloudSelected[0])
            setLicenseInfo(response.response)
        }
        function onFail(response: ResponseType) {
            logger.error("Error downloading cloud license: " + cloudSelected[0])
        }

        let cloudLicense = cloudLicenses.find(lic => lic.licenseId === cloudSelected[0])

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Download Cloud License File", onSuccess, onFail)) {
            if (cloudLicense) {
                dispatch(LicenseAPI.downloadLicense(newID, cloudLicense.fileName))
            }
        }
    }

    return (
        <>
            <Row>
                <Col>
                    <div className="assetBar">
                        <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                            <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                            <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/AccountProfile'}}>Profile</Breadcrumb.Item>
                            <Breadcrumb.Item active>License Management</Breadcrumb.Item>
                        </Breadcrumb>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h2>License Management</h2>
                    <p className="mb-4">Here are all the licenses used for HTe Enterprise.</p>
                </Col>
            </Row>
            <Row style={{marginBottom: "1rem"}}>
                <Col xs="4">
                    <ButtonGroup>
                        <Button onClick={()=>setRequestDialog(true)} className="actionBar1" id="actionBar1" size="sm">
                            <PostAdd style={{ marginRight: "5px" }}/>
                            Request New License
                        </Button>
                        <Button onClick={()=>downloadLicense()} className="actionBar1" id="actionBar1" size="sm">
                            <CloudDownload style={{ marginRight: "5px" }}/>
                            Download Cloud License
                        </Button>
                    </ButtonGroup>
                </Col>
                <Col>
                    <ButtonGroup>
                        <Button onClick={()=>setTableView("local")} id="actionBar2" variant="outline-primary" size="sm">
                            <Place style={{ marginRight: "5px" }}/>
                            Local Licenses
                        </Button>
                        <Button onClick={()=>setTableView("cloud")} id="actionBar2" variant="outline-primary" size="sm">
                            <Cloud style={{ marginRight: "5px" }}/>
                            Cloud Licenses
                        </Button>
                    </ButtonGroup>
                </Col>
            </Row>

            {tableView === "local" ? 
                <VirtualTable
                    tableName="licenseManagementTable"
                    saveKey="licenseManagementTableHead"
                    dataSet={localLicesnesClone}
                    headCells={licenseHeadCells}
                    dataIdentifier="licenseId"
                    initialSortedBy={{name: ''}}
                    dispatchedUrls={[AccessAPI.getEnterpriseLicense]}
                    selectedRows={selected}
                    setSelected={setSelected}
                />
            :
                <VirtualTable
                    tableName="cloudLicenseManagementTable"
                    saveKey="cloudLicenseManagementTableHead"
                    dataSet={cloudLicensesClone}
                    headCells={licenseHeadCells}
                    dataIdentifier="licenseId"
                    initialSortedBy={{name: ''}}
                    dispatchedUrls={[LicenseAPI.getLicenses]}
                    selectedRows={cloudSelected}
                    setSelected={setCloudSelected}
                />
            }

            <Dialog open={requestDialog} onClose={()=>setRequestDialog(false)} fullWidth={true} maxWidth="lg">
                <DialogTitle>
                    <Typography variant="h6" component="div">Request a License</Typography>
                </DialogTitle>
                <DialogContent>
                    <RequestEnterpriseLicense />
                </DialogContent>
            </Dialog>
        </>
    )
}

export default LicenseManagement