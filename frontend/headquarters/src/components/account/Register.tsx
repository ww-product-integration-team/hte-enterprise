// Register.jsx = This file is now used as "Request HTe Enterprise License"

import { Link } from 'react-router-dom';
import { Col, Card, Container } from '@themesberg/react-bootstrap';
import { RequestEnterpriseLicense } from '../licenses/LicenseForm';


// let reqRegisterID = null
export default function Register() {

  return (
    <Container>
      <br />
      <br />      
      
      <Col xs={12} className="d-flex align-items-center justify-content-center">
        <div className="mb-4 mb-lg-0 bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-1000">
          <h2 className="text-center text-md-center mb-4 mt-md-0 mb-0">Request an HTe Enterprise License</h2>

          <RequestEnterpriseLicense />

          <div className="d-flex justify-content-center align-items-center mt-4">
            <span>
              Already have an account?
              <Card.Link as={Link} to="/Login" className="fw-bold">
                {` Login here `}
              </Card.Link>
            </span>
          </div>

          <div className="d-flex justify-content-center align-items-center mt-4">
            <span>
              Already have a license?
              <Card.Link as={Link} to="/UploadHTeLicense" className="fw-bold">
                {` Upload here `}
              </Card.Link>
            </span>
          </div>
        </div>
      </Col>

    </Container>
  );
};
