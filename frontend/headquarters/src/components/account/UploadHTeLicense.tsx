// UploadHTeLicense.jsx = This file is used as "Upload HTe Enterprise License"

import {useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Col, Card, Container } from '@themesberg/react-bootstrap';
import { UploadEnterpriseLicense } from '../licenses/LicenseForm';


// let reqRegisterID = null
export default function UploadHTeLicense() {

    return (
        <Container>
            <br />
            <br />     

            <Col xs={12} className="d-flex align-items-center justify-content-center">
                <div className="mb-4 mb-lg-0 bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                    <h2 className="text-center text-md-center mb-4 mt-md-0 mb-0">Install an HTe Enterprise License</h2>

                    <UploadEnterpriseLicense />

                    <div className="d-flex justify-content-center align-items-center mt-4">
                        <span>
                            Already have an account?
                            <Card.Link as={Link} to="/Login" className="fw-bold">
                                {` Login here `}
                            </Card.Link>
                        </span>
                    </div>

                    <div className="d-flex justify-content-center align-items-center mt-4">
                        <span>
                            Need a license?
                            <Card.Link as={Link} to="/RequestLicense" className="fw-bold">
                                {` Request one here `}
                            </Card.Link>
                        </span>
                    </div>
                </div>
            </Col>
        </Container>
    );
};








