
import {useState } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import Alert from '@mui/material/Alert';
import RefreshIcon from '@mui/icons-material/Refresh';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { Col, Form, Card, Container } from '@themesberg/react-bootstrap';
import { FormInput } from '../forms/FormHelper';
import logger from '../utils/logger';


export default function ResetPassword() {
    interface ResetPasswordForm{
        password: string,
        confirmPassword: string, 
        email: string, 
    }

    const { register, handleSubmit, formState: {errors} } = useForm<ResetPasswordForm>();
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend
    const [passwordAlert, setPasswordAlert] = useState([false, "Something went wrong!"]) // If passwords don't match

    const handleReset = handleSubmit(async (data : ResetPasswordForm) => {    // When the Submit button is pressed
        logger.info("(ResetPassword) Register POST request data: ", data)
        setShowAlert([false, "Something went wrong!"])
        setPasswordAlert([false, "Something went wrong!"])

        // TODO: Need to pre-fill Email address box, or remove email address box so people can't reset passwords for other accounts.

        if (data.password !== data.confirmPassword) {
        setPasswordAlert([true, "Passwords don't match."])
        }

        else {
        // This is the data that will be sent
        const updatedData = {
            email: data.email,
            password: data.password
        }

        logger.info("(ResetPassword) updated register POST request Data: ", updatedData)
        // reqResetPasswordID = Date.now()
        // dispatch(AccountAPI.resetPassword(reqResetPasswordID, updatedData))
        }
    })
    return (
        <Container>
        <br />
        <br />

        <Col xs={12} className="d-flex align-items-center justify-content-center">
            <div className="mb-4 mb-lg-0 bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
            <h2 className="text-center text-md-center mb-4 mt-md-0 mb-0">Reset Password</h2>

            <Form className="mt-4" onSubmit={handleReset}>
                <Form.Group id="email" className="mb-4">
                <FormInput
                    autoFocus
                    label="Email Address: "
                    id="email"
                    name="email"
                    type="email"
                    placeholder="example@company.com"
                    register={register}
                    validation={{
                    required: "Please enter your email address",
                    maxLength: { value: 60, message: "You exceeded the max email length"}
                    }}
                    error={errors.email}
                />
                </Form.Group>

                <Form.Group id="password" className="mb-4">
                <FormInput
                    label="Password: "
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Password"
                    register={register}
                    validation={{
                    required: "Please enter a password",
                    maxLength: { value: 60, message: "You exceeded the max password length"}
                    }}
                    error={errors.password}
                />
                </Form.Group>

                <Form.Group id="confirmPassword" className="mb-4">
                <FormInput
                    label="Confirm Password: "
                    id="confirmPassword"
                    name="confirmPassword"
                    type="password"
                    placeholder="Confirm Password"
                    register={register}
                    validation={{
                    required: "Please enter your password again",
                    maxLength: { value: 60, message: "You exceeded the max password length"}
                    }}
                    error={errors.confirmPassword}
                />
                </Form.Group>

                {/* When passwords don't match */}
                {passwordAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{passwordAlert[1]}</Alert> : null}

                {/* Error creating account */}
                {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                <div className="formButtons">
                <button className="w-100 formButton1">
                    <RefreshIcon />
                    <span> Reset Password </span>
                </button>
                </div>
            </Form>

            <p className="d-flex justify-content-center align-items-center mt-4">
                <Card.Link as={Link} to="/Login" className="text-black fw-bold">
                <FontAwesomeIcon icon={faAngleLeft} className="me-2" />Back to Login Page
                </Card.Link>
            </p>
            </div>
        </Col>
        </Container>
    );
};
