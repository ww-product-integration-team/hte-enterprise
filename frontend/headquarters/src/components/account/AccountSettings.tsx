
import { useState} from "react";
import { useDispatch } from "react-redux"
import { Link } from 'react-router-dom'
import { useForm } from "react-hook-form";
import { Col, Row, Card, Form, Button, ButtonGroup, Breadcrumb} from '@themesberg/react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome} from '@fortawesome/free-solid-svg-icons';
import { Save, Edit} from '@material-ui/icons';
import Alert from '@mui/material/Alert';

import logger from "../utils/logger";
import { LoginAPI, AccessAPI } from '../api/index'
import { useRspHandler } from '../utils/ResponseProvider';
import { FormInput } from '../forms/FormHelper';
import { useAppSelector } from "../../state";
import { ResponseType } from "../../types/storeTypes";

export const AccountSettings = () => {

    interface AccountForm {
        firstName : string,
        lastName : string,
        email : string, 
        phone : string
    }
    
    const { register, handleSubmit, formState: {errors} } = useForm<AccountForm>();
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();

    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend or account already exists
    const [showSuccess, setShowSuccess] = useState([false, "Something went wrong!"]) // Successful account creation
    
    const currentAccount = useAppSelector((state) => state.access.account);

    const handleUpdate = handleSubmit(async (data : AccountForm) => {
        logger.info("(Account Settings) Update request data:", data)
        setShowAlert([false, "Something went wrong!"])
        setShowSuccess([false, "Something went wrong!"])

        let updatedPhoneNumber = ""
        if (data.phone !== "")    updatedPhoneNumber = data.phone.replaceAll(/[a-zA-Z`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')

        let updatedData = {
            id: currentAccount.id,
            firstName: data.firstName,
            lastName: data.lastName,
            username: data.email,
            phoneNumber: updatedPhoneNumber,
            accessLevel: currentAccount.accessLevel,
            domain: currentAccount.domain,
            roles: currentAccount.roles,
            enabled: currentAccount.enabled
        }

        const reqUpdateID : number = Date.now()
        
        // ==================   CALLBACK RELATED CODE    ==================
        // onSuccess() and onFail() are executed depending on if the API call was successful or not.

        function onSuccess(response : ResponseType) {
            logger.info("(AccountSettings) Backend Response: ", response);
            setShowAlert([false, "Something went wrong!"])
            setShowSuccess([true, "Account updated successfully! "])

            dispatch(AccessAPI.getUserInfo(Date.now()))
        }

        function onFail(response : ResponseType) {
            logger.info("(AccountSettings) Update not successful:", response);
            setShowSuccess([false, "Something went wrong!"])

            if (response.response.errorDescription === "That email address is already registered in the system.") {
                setShowAlert([true, "This email address already exists."])
            }

            else {
                setShowAlert([true, "Error trying to update account. Please try again later."])
            }
        }

        if (addCallback(callbacks, reqUpdateID, "Update User Information", onSuccess, onFail)){
            dispatch(LoginAPI.createAccount(reqUpdateID, updatedData))    
        }
    });


    return (
    <div className="accountContainer">
        <Row className="align-items-center justify-content-center">
            <div className="mb-4 mb-md-0 ">
                <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/AccountProfile'}}>Profile</Breadcrumb.Item>
                    <Breadcrumb.Item active>Account Settings</Breadcrumb.Item>
                </Breadcrumb>

                <h2>Account Settings</h2>
                <p className="mb-4">Update your account's personal information here.</p>

                <div className="d-flex justify-content-between">
                      <ButtonGroup className="assetBar">
                        <Button id="actionBar1" variant="outline-primary" size="sm"> 
                            <Link id="actionBar1" className="buttonLink" to = "/ChangePassword">
                                <Edit />
                                Change Password
                            </Link>
                        </Button>
                    </ButtonGroup>
                </div>
            </div>

            <Card border="light" className="bg-white shadow-sm mb-4 form-control">
                <Card.Body>
                    <h3 className="mb-4">General information</h3>
                    <Form className="form-control" onSubmit={handleUpdate}>
                        <Row>
                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="First Name "
                                    id="firstName"
                                    name="firstName"
                                    type="text"
                                    placeholder="Enter your first name"
                                    register={register}
                                    validation={{
                                        required: "Please enter your first name",
                                        maxLength: { value: 60, message: "You exceeded the max first name length"}
                                    }}
                                    error={errors.firstName}
                                />

                            </Col>

                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="Last Name "
                                    id="lastName"
                                    name="lastName"
                                    type="text"
                                    placeholder="Enter your last name"
                                    register={register}
                                    validation={{
                                        required: "Please enter your last name",
                                        maxLength: { value: 60, message: "You exceeded the max name length"}
                                    }}
                                    error={errors.lastName}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="Email Address "
                                    id="email"
                                    name="email"
                                    type="email"
                                    placeholder="example@company.com"
                                    register={register}
                                    validation={{
                                        required: "Please enter your email address",
                                        maxLength: { value: 60, message: "You exceeded the max email length"}
                                    }}
                                    error={errors.email}
                                />

                            </Col>
                            <Col md={6} className="mb-3">
                                <FormInput
                                    label="Phone Number (optional)"
                                    id="phone"
                                    name="phone"
                                    type="tel"
                                    placeholder="999-999-9999"
                                    register={register}
                                    validation={{
                                        // required: "Please enter a phone number",
                                        maxLength: { value: 16, message: "You exceeded the max phone number length"}
                                    }}
                                    error={errors.phone}
                                />
                            </Col>
                        </Row>

                        {/* Error creating account */}
                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}


                        {showSuccess[0] ? <Alert className="m-1" variant="filled" severity="success">{showSuccess[1]}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" type="submit">
                                <Save />
                                <span> Save All </span> 
                            </button>
                        </div>
                    </Form>
                </Card.Body>
            </Card>
        </Row>
    </div>
    );
};

export default AccountSettings;