import { useState, useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import Alert from '@mui/material/Alert';
import { Login as LoginIcon } from '@mui/icons-material';
import { Col, Form, Card, Container } from '@themesberg/react-bootstrap';
import { FormInput } from '../forms/FormHelper';
import {LoginAPI, AccessAPI, BannerAPI, RegionAPI, StoreAPI, DeptAPI, ProfileAPI, ConfigAPI, PricingZoneAPI} from '../api/index'
import { useDispatch} from "react-redux";
import "./Login.css";

// Below import depends on how/where Angel implements the endpoint 
// import { FileAPI } from '../api/index'
import logger from '../utils/logger';
import { AccessActions, ConfigActions, store, useAppSelector } from '../../state';
import { useRspHandler } from '../utils/ResponseProvider';
import { ResponseType } from '../../types/storeTypes';
import { add, format, intervalToDuration } from 'date-fns';

export default function Login() {

    const { register, handleSubmit,  formState: {errors} } = useForm<LoginForm>();
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend
    const [passwordAlert, setPasswordAlert] = useState([false, "Something went wrong!"]) // If passwords don't match
    const [isLoggingIn, setLoggingIn] = useState(false)
    const history = useHistory();
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();

    useEffect(() =>{
        if(store.getState().access.loggedIn){
            dispatch(AccessActions.setAccessTokens({}, "LOGOUT"))
            dispatch(AccessActions.checkTokenStatus())
        }
    },[])

    interface LoginForm{
        email: string, 
        password : string
    }

    async function initialDataGrab(data : LoginForm){

        function delay(time : number) {
            return new Promise(resolve => setTimeout(resolve, time));
        }

        dispatch(AccessAPI.getAccountInfo(Date.now(), data.email))
        await delay(100)
        dispatch(AccessAPI.getRoles())
        await delay(100)
        dispatch(AccessAPI.getUserInfo())
        await delay(100)
        dispatch(AccessAPI.getDomains())
        await delay(100)
        dispatch(AccessAPI.getEnterpriseLicense())
        await delay(100)
        dispatch(BannerAPI.fetchBanners())
        await delay(100)
        dispatch(RegionAPI.fetchRegions())
        await delay(100)
        dispatch(StoreAPI.fetchStores())
        await delay(100)
        dispatch(DeptAPI.fetchDepartment())
        await delay(100)
        dispatch(ProfileAPI.fetchProfiles())
        await delay(100)
        dispatch(PricingZoneAPI.getPricingZones())
        await delay(100)
        dispatch(ConfigAPI.fetchServices())
        await delay(100)
        dispatch(ConfigAPI.getConfiguration(Date.now()+1, ["zonePricing"], ConfigActions.setZonePricing))
        dispatch(ConfigAPI.getConfiguration(Date.now()+1, ["sendStoreInfo"], ConfigActions.setStoreConfig))
        dispatch(AccessActions.checkTokenStatus())
        await delay(100)
        history.push("/")
    }
    const handleLogin = handleSubmit(async (data: LoginForm) => {   // When the Submit button is pressed
        logger.info("Log in POST request data: ", data)

        if(!(/\S+@\S+\.\S+/.test(data.email))){
            setShowAlert([true, "Please enter a valid email address"])
            return
        }


        let encodedDescription = encodeURIComponent(data.password)   // Allows special characters

        let reqLoginID = Date.now()


        function onSuccess(response : ResponseType){
            logger.info("Backend Response (Login Result): ", response);
            localStorage.setItem("email", data.email)
           
            initialDataGrab(data).then(() => {
                setLoggingIn(true)
            })
        }
        
        function onFail(response : ResponseType){
            logger.error("login not successful?", response.response);
            console.log("callbacked failed")
            // Server Error
            if (response.response.errorDescription === "Invalid server response!") {
            setShowAlert([true, "Error connecting to server. Please try again."])
            }

            // Server Error
            else if (response.response.errorDescription === "Could not login, user is not enabled") {
            setShowAlert([true, "This account is currently disabled. Contact an admin to enable your account."])
            }

            // Wrong login information
            else {
            response.response.errorDescription != null ? 
                    setShowAlert([true, response.response.errorDescription]) : 
                    setShowAlert([true, "Incorrect username / password combination. Please try again."])
            }
            setLoggingIn(false)
        }
        if(addCallback(callbacks, reqLoginID, "User Login", onSuccess, onFail, {})){
            setLoggingIn(true)
            dispatch(LoginAPI.requestLogin(reqLoginID, data.email, encodedDescription))
        }
  })

  // ==================   LICENSE STATUS   ==================
  // Calculate license status to see if it should be shown or not  
  const licenses = useAppSelector((state) => state.access.licenses);
  const [licenseInfo, setLicenseInfo] = useState(licenses[0])
  const [expirationDate, setExpirationDate] = useState("")
  const [notice, setNotice] = useState([false, "Something went wrong!"]) // If license duration is <= 30 days

  const handleLicenseNotice = () => {
    try {
      if (licenses.length !== 0) {
        // "LL format" = Month day, year (ex. March 28, 2023)
        setExpirationDate(format(add(Date.parse(licenses[0].activationDate), { days: licenses[0].timeout }), "PP"))

        if (expirationDate) {
          let interval = intervalToDuration({ start: Date.parse(expirationDate), end: Date.now() })
          if (Date.parse(expirationDate) < Date.now()) {
            setNotice([true, "Your licence is expired!"])
          }
          else if (!interval.years && !interval.months && (interval.days && interval.days <= 30 || !interval.days)) {
            setNotice([true, `# of days left on Enterprise license: ${interval.days}`])
          }
          else {
            setNotice([false, "Something went wrong!"])
          }
        }
      }
      else {
        setNotice([true, "Something went wrong!"])
      }
    }
    catch (error) {
      // Do nothing
    }
  }

  useEffect(() => {
    dispatch(AccessAPI.getEnterpriseLicense(Date.now()))
  }, [])

  useEffect(() => {
    handleLicenseNotice()
  }, [licenses, expirationDate])

  return (
    <Container>
        <>
      <br />
      <br />

      <Col xs={12} className="d-flex align-items-center justify-content-center">
        <div className="mb-4 mb-lg-0 bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
          <h2 className="text-center text-md-center mb-4 mt-md-0 mb-0">Log in to HTe Enterprise</h2>
          <Form className="mt-4" onSubmit={handleLogin}>
            <Form.Group id="email" className="mb-4">
              <FormInput
                autoFocus
                label="Email Address: "
                id="email"
                name="email"
                placeholder="example@company.com"
                register={register}
                validation={{
                    required: "Please enter your email address",
                    maxLength: { value: 60, message: "You exceeded the max email length"}
                }}
                error={errors.email}
              />
            </Form.Group>
          
            <Form.Group id="password" className="mb-4">
              <FormInput
                className="mb-2"
                label="Password: "
                id="password"
                name="password"
                type="password"
                placeholder="Password"
                register={register}
                validation={{
                  required: "Please enter a password",
                  maxLength: { value: 60, message: "You exceeded the max password length"}
                }}
                error={errors.password}
              />
            </Form.Group>
                
            {/* When provided info doesn't match what's in the database */}
            {passwordAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{passwordAlert[1]}</Alert> : null}

            {/* Error logging in */}
            {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

            <div className="formButtons">
              <button className="w-100 formButton1" data-testid='LoginButton'>
                {isLoggingIn ?
                    <div style={{display:"flex", justifyContent:"center"}}>
                    <div className='spinner'/>
                    <span style={{marginLeft: "1rem"}}> Logging in... </span>
                    </div> 
                     :
                    <>
                    <LoginIcon />
                    <span> Log In </span>
                    </>
                }
              </button>
            </div>
          </Form>

          <div className="justify-content-center align-items-center mt-4 text-align-center">
            <p>
              Need a license?
              <Card.Link as={Link} to="/RequestLicense" className="fw-bold" data-testid='RequestALicenseLink'>
                {` Request one here `}
              </Card.Link>
            </p>

            {/* License Duration Notification */}
            {notice[0] ? <Alert className="m-1" variant="filled" severity="warning">{notice[1]}</Alert> : null}

          </div>
        </div>
      </Col>
      </>
    </Container>
  );
};
