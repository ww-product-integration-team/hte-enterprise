import { useState, useEffect } from "react";
import { Link} from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome} from '@fortawesome/free-solid-svg-icons';
import { Col, Row, Card, Button, ButtonGroup, Breadcrumb} from '@themesberg/react-bootstrap';
import { PersonAdd } from "@mui/icons-material";
import AccessControl from "../common/AccessPermissions"
import {useDispatch } from "react-redux"
import { IconButton, Tooltip, Typography } from '@material-ui/core';
import { Help, AddCircle, Book } from '@material-ui/icons';
import { useAppSelector } from "../../state";
import { MANAGE_SUPERVISORS, MANAGE_OPERATORS } from "../../state/actions/accessActions"
import { StatisticWidget} from '../dashboard/widgets/Widgets'
import moment from 'moment';
import ScaleIcon from '../icons/ScaleIcon';
import PersonIcon from '@mui/icons-material/Person';
import EventIcon from '@mui/icons-material/Event';
import { AccessAPI } from '../api/index'
import { HTeRole, HTeUser } from "../../types/access/AccessTypes";
import { License } from "../../types/access/HTeLicense";
import { RequestLicense } from "../licenses/LicenseForm";


export const AccountSettings = () => {

    const dispatch = useDispatch();
    const [openRequest, setOpenRequest] = useState(false); 

    const appRoles : {[key: string] : HTeRole} = useAppSelector((state) => state.access.roles);
    const user : HTeUser= useAppSelector((state) => state.access.account);


    const licenses : License[] = useAppSelector((state) => state.access.licenses);
    const scaleCount = useAppSelector((state) => state.asset.scaleCount);
    const users : HTeUser[] = useAppSelector((state) => state.access.users);

    const [list, setList] = useState<any>([]);

    const updateList = () => {

        setList([
            {
                value1: scaleCount.assigned,
                value2: licenses[0].numScales, 
                multipleValues: true,
                title: "# of Scales Allocated",
                icon: ScaleIcon
            },
            {
                value1: users.length,
                value2: licenses[0].numUsers,    
                multipleValues: true,
                title: "# of Users Allocated",   
                icon: PersonIcon 
            },
            {
                value1: licenses.length !== 0 ? moment(licenses[0].activationDate).format("LL") : "",
                title: "License Activation Date",
                icon: EventIcon
            },
            {
                value1: licenses.length !== 0 ? moment(licenses[0].activationDate).add(licenses[0].timeout, 'days').format("LL") : "",
                expirationDate: true,
                title: "License Expiration Date",
                icon: EventIcon
            }
        ])
    }

    useEffect(() => {        
        if (licenses.length !== 0)   updateList()            // if license detected, display HTe Licensing Information
    }, [scaleCount, users, licenses]);

    useEffect(() => {        
        let id = Date.now()
        let userEmail = localStorage.getItem("email")
        if(userEmail){
            dispatch(AccessAPI.getAccountInfo(id, userEmail))
        }
        dispatch(AccessAPI.getUserInfo(id + 1))
    }, []);


    return (
    <div className="accountContainer">
        <Row className="align-items-center justify-content-center">
            <div className="mb-4 mb-md-0 ">
                <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                    <Breadcrumb.Item active>Profile</Breadcrumb.Item>
                </Breadcrumb>

                <h2>Your Profile</h2>

                <div className="d-flex justify-content-between mb-4">
                    <ButtonGroup className="assetBar">
                        <AccessControl
                            user = {user}
                            requiredRole = {appRoles[MANAGE_OPERATORS]}
                        >
                            <Button className="actionBar2" id="actionBar2" variant="outline-primary" size="sm"> 
                                <Link className="buttonLink" to = "/UserManagement">
                                    <PersonAdd />
                                    Manage Users
                                </Link>
                            </Button>
                        </AccessControl>
                        <Button className="actionBar2" id="actionBar2" variant="outline-primary" size="sm">
                            <Link className="buttonLink" to="/LicenseManagement">
                                <Book />
                                Manage Licenses
                            </Link>
                        </Button>
                    </ButtonGroup>

                    <AccessControl
                        user = {user}
                        requiredRole = {appRoles[MANAGE_SUPERVISORS]}
                    >
                        <ButtonGroup className="assetBar">
                            <Button id="actionBar1" size="sm" onClick={() => {setOpenRequest(true)}}>
                                <AddCircle />
                                Request HTe Business License
                            </Button>
                            <Button id="actionBar1" variant="outline-primary" size="sm"> 
                                <Link id="actionBar1" className="buttonLink" to = "/CreateAccount">
                                    <PersonAdd />
                                    Create New User
                                </Link>
                            </Button>
                        </ButtonGroup>
                    </AccessControl>
                </div>
            </div>

            <Row>
                <Col xs={6} md={6} xl={4} className="mb-3">
                    <Card border="light" className="shadow-sm">
                        <Card.Body className='form-control customBorder'>
                            <Col xl={12} className="text-xl-center align-items-center justify-content-xl-center mb-xl-0">
                                <h4 className="mb-3 text-align-center">Account Information</h4>
                            </Col>

                            <p><span className="fw-bold">Name: </span>{`${user.firstName} ${user.lastName}`}</p>
                            <p><span className="fw-bold">Email:</span> {user.username}</p>
                            {/* <hr /> */}
                            <p><span className="fw-bold">Account Created:   </span>{moment(user.dateCreated).format("LLL")}</p>
                            <p><span className="fw-bold">Access Level:      </span>{user.accessLevel}</p> 
                            <p className="tableTemplate"><span className="fw-bold">Roles:             
                                <Tooltip className="info" title={<Typography>Roles can help bypass access level requirements. </Typography>}>
                                    <IconButton aria-label="sync end date">
                                        <Help />
                                    </IconButton>
                                </Tooltip>                            
                            </span>
                                <br /> 
                                {user.roles.length !== 0 
                                ?   user.roles.map((role : HTeRole, index : number) => <li key={index}> {role.name} </li>)
                                :   <span>No custom roles assigned</span>}
                            </p> 
                        </Card.Body>
                    </Card>
                </Col>


                <Col xs={6} xl={8} className=" mb-3">
                    <StatisticWidget 
                        list={list} 
                    />
                </Col>

                {/* <Col xs={12} md={12} xl={12} className="mb-3">
                    <Card border="light" className="bg-white shadow-sm mb-4 form-control" style={{overflowWrap: 'normal'}}>
                        <Row>
                            <Col xl={12} className="text-xl-center align-items-center justify-content-xl-center mb-xl-0">
                                <h4 style={{textAlign: "center"}} className="mb-3">Overview</h4>
                            </Col>

                            <Col xs={12} xl={12} className="mb-3">
                                <p>What kind of information would we like to show here? Company information?</p>
                                <li>We can display # of HTe Enterprise accounts in organization</li>
                                <li>We can display # of scale licenses left</li>
                                <li>Enterprise license start/expiration date?</li>
                            </Col>
                        </Row>
                    </Card>
                </Col> */}
            </Row>
        </Row>

        {openRequest &&
            <RequestLicense
                title="Request a license for HTe"
                openPopup={openRequest}
                setOpenPopup = {setOpenRequest}
            />
        }

    </div>
    );
};

export default AccountSettings;