
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from "react-redux"
import { useForm } from "react-hook-form";
import Alert from '@mui/material/Alert';
import RefreshIcon from '@mui/icons-material/Refresh';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { Col, Form, Card, Container } from '@themesberg/react-bootstrap';
import { FormInput } from '../forms/FormHelper';
import logger from '../utils/logger';
import { AccessAPI } from '../api/index'
import { useRspHandler } from '../utils/ResponseProvider';
import { useAppSelector } from '../../state';
import { HTeUser } from '../../types/access/AccessTypes';
import { ResponseType } from '../../types/storeTypes';


export default function ChangePassword() {

    interface PasswordForm{
        newPassword: string, 
        confirmPassword: string
    }

    const { register, handleSubmit, formState: {errors} } = useForm<PasswordForm>();
    const dispatch = useDispatch();

    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend
    const [successAlert, setSuccessAlert] = useState([false, "Something went wrong!"]) // Checking if success + show temp password
    const [passwordAlert, setPasswordAlert] = useState([false, "Something went wrong!"]) // If passwords don't match

    const user : HTeUser = useAppSelector((state) => state.access.account);
    const {callbacks, addCallback} = useRspHandler();


    const handleChange = handleSubmit(async (data : PasswordForm) => {    // When the Submit button is pressed
        logger.info("(Change Password) POST request data: ", data)
        setShowAlert([false, "Something went wrong!"])
        setSuccessAlert([false, "Something went wrong!"])
        setPasswordAlert([false, "Something went wrong!"])

        function onSuccess(response : ResponseType){
            logger.info("Backend Response (EDIT File): ", response);
            setShowAlert([false, "Something went wrong!"])
            setSuccessAlert([true, response.response.errorDescription])
            setPasswordAlert([false, "Something went wrong!"])

            dispatch(AccessAPI.getUserInfo(Date.now()))
        }
        function onFail(response : ResponseType){
            logger.error("Backend Error (EDIT File): ", response);
            setShowAlert([true, "Error trying to update file. Please try again later."])
        }

        // if the new password and confirm password doesn't match
        if (data.newPassword !== data.confirmPassword) {
            setPasswordAlert([true, "New Password and Confirm Password don't match."])
        }

        else {
            const newID = Date.now()
            if(addCallback(callbacks, newID, "Change Password", onSuccess, onFail)){
                dispatch(AccessAPI.changePassword(newID, user.username, data.newPassword))
            }
        }
  })

  return (
    <Container>
      <br />
      <br />

      <Col xs={12} className="d-flex align-items-center justify-content-center">
        <div className="mb-4 mb-lg-0 bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
          <h2 className="text-center text-md-center mb-4 mt-md-0 mb-0">Change Password</h2>

          <Form className="mt-4" onSubmit={handleChange}>
            <Form.Group id="newPassword" className="mb-4">
              <FormInput
                label="New Password: "
                id="newPassword"
                name="newPassword"
                type="password"
                placeholder="New Password"
                register={register}
                validation={{
                  required: "Please enter a new password",
                  minLength: { value: 8,  message: "Password must have at least 8 characters"},
                  maxLength: { value: 60, message: "You exceeded the max password length"}
                }}
                error={errors.newPassword}
              />
            </Form.Group>

            <Form.Group id="confirmPassword" className="mb-4">
              <FormInput
                label="Confirm Password: "
                id="confirmPassword"
                name="confirmPassword"
                type="password"
                placeholder="Confirm Password"
                register={register}
                validation={{
                  required: "Please enter your new password again",
                  maxLength: { value: 60, message: "You exceeded the max password length"}
                }}
                error={errors.confirmPassword}
              />
            </Form.Group>

            {/* When passwords don't match */}
            {passwordAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{passwordAlert[1]}</Alert> : null}

            {/* Error changing password */}
            {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

            {/* Successful password change */}
            {successAlert[0] 
            ? <div className="licenseAlert">
                <Alert className="licenseAlert2 fw-bold" variant="filled" severity="success">{successAlert[1]}</Alert>
              </div>
            : null}

            <div className="formButtons">
              <button className="w-100 formButton1">
                <RefreshIcon />
                <span> Update Password </span>
              </button>
            </div>
          </Form>

          <p className="d-flex justify-content-center align-items-center mt-4">
            <Card.Link as={Link} to="/AccountSettings" className="text-black fw-bold">
              <FontAwesomeIcon icon={faAngleLeft} className="me-2" />Back to Account Settings
            </Card.Link>
          </p>
        </div>
      </Col>
    </Container>
  );
};
