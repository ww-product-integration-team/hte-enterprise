import * as types from "../responseTags";
import { AssetActions, AccessActions, RootStore} from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import { Dispatch } from "redux";
import { Store } from "../../../types/asset/AssetTypes";

const ENDPOINT = types.Endpoints.ui_controller

//==========================================================================
//Get list of stores
export const fetchStores = (id : number = Date.now(), storeId = "") => async (dispatch : Dispatch, getState : () => RootStore) => {
    let url = types.getURL(ENDPOINT, "store")
    if(storeId !== ""){
        url = url + "?id="+storeId
    }

    await axios.get(url, 
    {   headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        if(storeId === ""){
            dispatch(AssetActions.setStoreList(response.data))
        }
        else{
            dispatch(AssetActions.insertStoreList(response.data))
        }
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_STORE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("fetchStores", "Error", err) : err.response.data
        }))
    })
}



//==========================================================================
//Add a new store to tree
export const postStore = (id : number, data : Partial<Store>, domainId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: types.generateHeader(domainId),
        url: types.getURL(ENDPOINT, "store"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_STORE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
        fetchStores()
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_STORE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("PostStore", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Delete a store from the list
export const deleteStore = (id : number, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "store") + "?id="+storeId,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        fetchStores()
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_STORE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
        
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_STORE,
            action: types.ACTION_DEL,
            success: false,
            response : error.response == null ? types.getDefaultRsp("DeleteStore", "Error", error) : error.response.data
            }))
    })
}


export const executeManualStoreHeartBeat = (id : number, storeEndpoint : string, clearData : boolean, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let command = "executemanualstoreheartbeat"

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: storeEndpoint + "/store/heartbeat?clearData="+clearData,
        timeout: 30000          // 10 second timeout
    })  
        
    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("executeManualStoreHeartbeat", "Error", err) : err.response.data
        }))
    })
}

//Enable/Disable a scale
export const setStoreEnable = (id : number, storeId, enable) => async (dispatch : Dispatch, getState : () => RootStore) => {


    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "store/enable")
    url = url + "?enable="+enable+"&id="+storeId

    await axios({
        method: 'GET',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: url,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })

    .then(response => {
        dispatch(setResponse(
        {
            id: id,
            type: types.RSP_STORE,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
        fetchStores()
        
    })
    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("setStoreEnable", "Error", err) : err.response.data
        }))
    })
}