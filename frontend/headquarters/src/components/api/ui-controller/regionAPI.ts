import * as types from "../responseTags";
import { AssetActions, AccessActions, RootStore} from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import { Dispatch } from "redux";
import { Region } from "../../../types/asset/AssetTypes";

const ENDPOINT = types.Endpoints.ui_controller

//==========================================================================
//Get list of regions
export const fetchRegions = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    // fetchTodoByIdThunk is the "thunk function"
    await axios.get(types.getURL(ENDPOINT, "region"), 
    {   headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        dispatch(AssetActions.setRegionList(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
                id: id,
                type: types.RSP_REGION,
                action: types.ACTION_GET,
                success: false,
                response : err.response == null ? types.getDefaultRsp("fetchRegions", "Error", err) : err.response.data
            }
        ))
    })
}

//==========================================================================
//Add a new region to tree
export const postRegions = (id : number, data : Partial<Region>, domainId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: types.generateHeader(domainId),
        url: types.getURL(ENDPOINT, "region"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
                id: id,
                type: types.RSP_REGION,
                action: types.ACTION_POST,
                success: true,
                response : response.data
            }))
            fetchRegions()
    })

    .catch((error) => {
        dispatch(setResponse(
            {
                id: id,
                type: types.RSP_REGION,
                action: types.ACTION_POST,
                success: false,
                response : error.response == null ? types.getDefaultRsp("PostRegions", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Delete a region from the list
export const deleteRegion = (id : number, regionId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(regionId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "region") + "?id="+regionId,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
                id: id,
                type: types.RSP_REGION,
                action: types.ACTION_DEL,
                success: true,
                response : response.data
            }))
            fetchRegions()
    })
    .catch((error) => {
        dispatch(setResponse(
            {
                id: id,
                type: types.RSP_REGION,
                action: types.ACTION_DEL,
                success: false,
                response : error.response == null ? types.getDefaultRsp("DeleteRegion", "Error", error) : error.response.data
            }))
    })
}