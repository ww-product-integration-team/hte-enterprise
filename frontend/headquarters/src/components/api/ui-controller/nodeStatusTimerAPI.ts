import * as types from "../responseTags";
import { setResponse} from "../../../state/actions/endpointActions";
import axios from "axios";
import { AccessActions, RootStore, AssetActions} from "../../../state";
import { Dispatch } from "redux";

const ENDPOINT = types.Endpoints.ui_controller

export const getNodeStatusTimers = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios.get(types.getURL(ENDPOINT, "getNodeStatusTimers"), 
    {   
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: 30000
    })

    .then(response => {
        dispatch(AssetActions.setNodeStatusTimers(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_REFRESH,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getNodeStatusTimers", "Error", err) : err.response.data
        }))
    })
}