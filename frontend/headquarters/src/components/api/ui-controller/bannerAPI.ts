import * as types from "../responseTags";
import { AssetActions, AccessActions, RootStore } from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import { Dispatch } from "redux";
import { Banner } from "../../../types/asset/AssetTypes";
import {  BannerAPI } from "../index";


const ENDPOINT = types.Endpoints.ui_controller

//==========================================================================
//Get list of banners
export const fetchBanners = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    // fetchTodoByIdThunk is the "thunk function"
    await axios.get(types.getURL(ENDPOINT, "banner"), 
    { headers: {
        "Authorization": types.getAccessToken(),//New Banner's will always have admin as their parent domain
        "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
    },
    timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        dispatch(AssetActions.setBannerList(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_BANNER,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("fetchBanners", "Error", err) : err.response.data
        }))
    })
}

//==========================================================================
//Add a new banner to list
export const postBanners = (id : number, data : Partial<Banner>) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: types.generateHeader(data.bannerId ? data.bannerId : AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
        url: types.getURL(ENDPOINT, "banner"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        fetchBanners()
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_BANNER,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_BANNER,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("PostBanners", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Delete a banner from the list
export const deleteBanners = (id : number, bannerId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: types.generateHeader(bannerId),
        url: types.getURL(ENDPOINT, "banner") + "?id="+bannerId,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_BANNER,
            action: types.ACTION_DEL,
            success: true,
            response : response.data
            }))
        fetchBanners()
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_BANNER,
            action: types.ACTION_DEL,
            success: false,
            response : error.response == null ? types.getDefaultRsp("DeleteBanners", "Error", error) : error.response.data
            }))
    })
}