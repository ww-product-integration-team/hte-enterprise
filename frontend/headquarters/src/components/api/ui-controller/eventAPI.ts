import * as types from '../responseTags'
import axios from 'axios'
import { setResponse } from '../../../state/actions/endpointActions'
import { EventActions, RootStore, AccessActions } from '../../../state'
import { Dispatch } from 'redux'

const ENDPOINT = types.Endpoints.event_controller

export const fetchEvents = (id: number = Date.now(), timestampStart?: string, timestampEnd?: string, entityId?: string, eventId?: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    let url = types.getURL(ENDPOINT, buildUrl(timestampStart, timestampEnd, entityId, eventId))

    await axios.get(url, {
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_EVENT,
            action: types.ACTION_GET,
            success: true,
            response: types.getDefaultRsp("fetchEvents", "Success", null, "Event data recieved")
        }))
        dispatch(EventActions.setEvents(response.data))
    }).catch(err => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_EVENT,
            action: types.ACTION_GET,
            success: false,
            response: err.response == null ? types.getDefaultRsp("fetchEvents", "Error", err) : err.response.data
        }))
    })
}

function buildUrl(timestampStart?: string, timestampEnd?: string, entityId?: string, eventId?: number): string {
    if (timestampStart && timestampEnd && entityId && eventId) {
        return `getEvents?timestampStart=${timestampStart}&timestampEnd=${timestampEnd}&entityId=${entityId}&eventId=${eventId}`
    } else if (timestampStart && timestampEnd && entityId) {
        return `getEvents?timestampStart=${timestampStart}&timestampEnd=${timestampEnd}&entityId=${entityId}`
    } else if (timestampStart && timestampEnd && eventId) {
        return `getEvents?timestampStart=${timestampStart}&timestampEnd=${timestampEnd}&eventId=${eventId}`
    } else if (timestampStart && timestampEnd) {
        return `getEvents?timestampStart=${timestampStart}&timestampEnd=${timestampEnd}`
    } else if (entityId && eventId) {
        return `getEvents?entityId=${entityId}&eventId=${eventId}`
    } else if (entityId) {
        return `getEvents?entityId=${entityId}`
    } else if (eventId) {
        return `getEvents?eventId=${eventId}`
    } else {
        return "getEvents"
    }
}

export const updateEvents = (id: number = Date.now(), toUpdate: Event) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
            "Content-Type": "application/json"
        },
        url: types.getURL(ENDPOINT, "updateEvents"),
        data: toUpdate,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_EVENT,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_EVENT,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("updateEvents", "Error", error) : error.response.data
        }))
    })
}

/**
 * Deletes events by the provided arguments, only one of which is required
 * @param id 
 * @param timestamp deletes a specific event with the exact timestamp
 * @param entityId deletes all events with the provided entityId
 * @param eventId deletes all events with the provided eventId
 * @returns 
 */
export const deleteEvents = (id: number = Date.now(), timestamp: Date, entityId: string, eventId: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    let url = types.getURL(ENDPOINT, "deleteEvents")
    let inbetween = new Date(timestamp) // making sure local time is sent to backend
    let realDate = new Date(Date.UTC(inbetween.getFullYear(), inbetween.getMonth(), inbetween.getDate(), inbetween.getHours(), inbetween.getMinutes(), inbetween.getSeconds(), inbetween.getMilliseconds()))
    if (timestamp) {
        url += `?timestamp=${realDate.toISOString().slice(0, realDate.toISOString().length-1)}` // remove 'Z' at end of format
    } else if (entityId) {
        url += `?entityId=${entityId}`
    } else if (eventId) {
        url += `?eventId=${eventId}`
    } else {
        dispatch({
            id: id,
            type: types.RSP_EVENT,
            action: types.ACTION_DEL,
            success: false,
            response: "Cannot delete events when no identifiers are provided"
        })
        return
    }

    await axios({
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
            'Authorization': types.getAccessToken()
        },
        url: url,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_EVENT,
            action: types.ACTION_DEL,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_EVENT,
            action: types.ACTION_DEL,
            success: false,
            response: error.response == null ? types.getDefaultRsp("deleteEvents", "Error", error) : error.response.data
        }))
    })
}