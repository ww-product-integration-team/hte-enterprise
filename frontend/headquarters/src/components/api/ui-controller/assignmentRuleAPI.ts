import * as types from "../responseTags";
import { AccessActions, AssetActions, RootStore} from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import { Dispatch } from "redux";
import { AutoAssignRule } from "../../appSettings/ImportCSVForAssigner";

const ENDPOINT = types.Endpoints.ui_controller

// Pull data from the autoAssignmentRules table and give them back to the user
// Requested by Jeremy Kromholtz from Costco
// export const enableRules = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

//     await axios({
//         method: "POST",
//         headers: {
//             "Authorization": types.getAccessToken(),
//             "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
//         },
//         url: types.getURL(ENDPOINT, "enableAssignmentRules"),
//         timeout: types.DEFAULT_TIMEOUT
//     }).then(response => {
//         dispatch(setResponse({
//             id: id,
//             type: types.RSP_SCALE,
//             action: types.ACTION_POST,
//             success: true,
//             response: response.data
//         }))
//     }).catch(err => {                 // This catches any error and logs the message
//         dispatch(setResponse({
//             id: id,
//             type: types.RSP_SCALE,
//             action: types.ACTION_POST,
//             success: false,
//             response : err.response == null ? types.getDefaultRsp("enableRules", "Error", err) : err.response.data
//         }))
//     })
// }
//Sends individually created rule to the auto assigner table
export const postAutoAssignerRule = (
    id : number, data, ruleIPs?:string) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let url = types.getURL(ENDPOINT, "postAutoAssignerRule")+ "?id="
    if(ruleIPs)
        url = url  + ruleIPs
    else 
        url = url + ""
    await axios({
        method: 'POST',
        headers: {"Content-Type": "application/json",
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
                "Authorization": types.getAccessToken()},
        url: url,
        data: data,
        timeout: 30000
    })  
    .then((response) => {               
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("postAutoAssignerRule", "Error", error) : error.response.data
            }))
    })
}
//=============================================================================
// Disable scale assignment rules (these are enabled by default when imported)
// export const disableRules = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

//     await axios({
//         method: "POST",
//         headers: {
//             "Authorization": types.getAccessToken(),
//             "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
//         },
//         url: types.getURL(ENDPOINT, "disableAssignmentRules"),
//         timeout: types.DEFAULT_TIMEOUT
//     }).then(response => {
//         dispatch(setResponse({
//             id: id,
//             type: types.RSP_SCALE,
//             action: types.ACTION_POST,
//             success: true,
//             response: response.data
//         }))
//     }).catch(err => {                 // This catches any error and logs the message
//         dispatch(setResponse({
//             id: id,
//             type: types.RSP_SCALE,
//             action: types.ACTION_POST,
//             success: false,
//             response : err.response == null ? types.getDefaultRsp("disableRules", "Error", err) : err.response.data
//         }))
//     })
// }

export const getAutoAssignRules = (id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {
    
    await axios.get(types.getURL(ENDPOINT, 'autoAssignRules'), {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(AssetActions.setAutoAssignRules(response.data))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Get Auto Assign Rules", "Error", error) : error.response.data
        }))
    })
}

export const deleteAutoAssignerRule = (id : number, data : AutoAssignRule) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: {"Content-Type": "application/json",
        "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
        "Authorization": types.getAccessToken()},
        data: data,
        url: types.getURL(ENDPOINT, "deleteAutoAssignerRule"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_BANNER,
            action: types.ACTION_DEL,
            success: true,
            response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_BANNER,
            action: types.ACTION_DEL,
            success: false,
            response : error.response == null ? types.getDefaultRsp("DeleteAutoAssignerRules", "Error", error) : error.response.data
            }))
    })
}

export const downloadRules = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
  await axios({
      method: "GET",
      headers: {
          "Authorization": types.getAccessToken(),
          "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
      },
      url: types.getURL(ENDPOINT, "downloadAssignmentRules"),
      timeout: types.DEFAULT_TIMEOUT
  }).then(response => {
      dispatch(setResponse({
          id: id,
          type: types.RSP_RULES,
          action: types.ACTION_GET,
          success: true,
          response: response.data
      }))
  }).catch(err => {                 // This catches any error and logs the message
      dispatch(setResponse({
          id: id,
          type: types.RSP_RULES,
          action: types.ACTION_GET,
          success: false,
          response : err.response == null ? types.getDefaultRsp("downloadRules", "Error", err) : err.response.data
      }))
  })
}