import * as types from "../responseTags";
import { AssetActions, AccessActions } from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import { Dispatch } from "redux";
import { ScaleDevice } from "../../../types/asset/ScaleTypes";
import { lazyNode } from "../../../types/asset/TreeTypes";
import { Banner, Region, Store } from "../../../types/asset/AssetTypes";

const ENDPOINT = types.Endpoints.ui_controller

export const instantiateNodeStatus = (id: number = Date.now()) => async (dispatch: Dispatch) => {
    await axios({
        method: 'POST',
        headers: {"Content-Type": "multipart/form-data",
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "instantiateNodeStatus"),
        timeout: 6000000  // 100 min
    })  
    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("instantiateNodeStatus", "Error", error) : error.response.data
            }))
    })
}
/**
 * 
 * @param id = timestamp to allow callbacks to manage toast
 * @param stringPaths = gives the path of the current node so the backend and return its children
 * @returns = the dispatch returns the children nodes and appends them to the parent node (found by using stringPaths)
 */
export const fetchLazyTree = (id: number = Date.now(), stringPaths?: any) => async (dispatch: Dispatch) => {
    let url = "fetchLazyTree"
    if(stringPaths ){
        let paths = stringPaths.split("|").join("%7C")
        paths = paths.split(",")
        url += "?nodePath=" + paths.join("%2C")
    }
    await axios({
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, url),
        timeout: 100000
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response: types.getDefaultRsp("FetchLazyTree", "Success", null, "Section of lazy tree received")
        }))
        if(stringPaths){
            dispatch(AssetActions.insertLazyTree(response.data, stringPaths))
        }
        else {
            dispatch(AssetActions.setLazyTree(response.data))
        }
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("FetchLazyTree", "Error", error) : error.response.data
        }))
    })
}
/**
 * 
 * @param id = timestamp to allow callbacks to manage toast
 * @param expandedKeys = backend uses this to create the nodes that are currently open
 * @returns = the entire lazy tree formatted by backend based on the expanded keys
 */
export const refreshLazyTree = (id:number, expandedKeys : string[]) => async (dispatch: Dispatch) => {
    let url = "fetchLazyTree"
    let newKeys : string[] = []
    for(let key of expandedKeys){
        let newkey = key.split("|").join("%7C")
        newKeys.push(newkey.split(",").join("~"))
    }
    url += "?expandedKeys=" + newKeys.join("%2C")
    await axios({
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, url),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    }).then(response => {
        dispatch(AssetActions.setLazyTree(response.data))
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response: types.getDefaultRsp("RefreshLazyTree", "Success", null, "refresh of lazy tree received")
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("RefreshLazyTree", "Error", error) : error.response.data
        }))
    })
}

/**
 * 
 * @param path concatenated path of parent filter node
 * @example scale path => filterid~bannerdId,regionId,storeId,storeId|deptId,scaleid
 * @returns Cleaned filter node's path of the filter id so the backend can appropriately handle the node
 */
function cleanKeys(path: string | string[]) : string {
    let acc : string[] = []
    let pathArray : string[] = []

    if( typeof path ==  "string"){
        console.log("was string")
        pathArray = path.split(",")
    } else {
        console.log("was array")
        pathArray = path
    }

    for( let id of pathArray){
        if(id.includes("~")){
            acc.push(id.split("~")[1])
        } else {
            acc.push(id)
        }
    }
    console.log("ac:", acc)
    return acc.join(",");
}

/**
 * 
 * @param id = timestamp to allow callbacks to manage toast
 * @param path = concatenated path of parent filter node
 * @example scale path => filterid~bannerdId,regionId,storeId,storeId|deptId,scaleid
 * @returns = the dispatch returns the children nodes and appends them to the parent node (found by using stringPaths)
 */
export const fetchFilterTreePart = (id: number = Date.now(), filterId : string, filterPath?: string) => async (dispatch: Dispatch) => {
    let url = "fetchFilterPart"
    
    url += "?filterId=" + filterId
    let path = ""
    if(filterPath){
        console.log("uncleaned:", filterPath)
        // prep filterPath for URL request
        path = cleanKeys(filterPath)
        path = path.split("|").join("%7C")
        path = path.split(",").join("%2C")
        url += "&nodePath=" + path

    }
    await axios({
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, url),
        timeout: 100000
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response: types.getDefaultRsp("FetchLazyTree", "Success", null, "Section of lazy tree received")
        }))
        if(filterPath){
            dispatch(AssetActions.insertFilterLazyTree(response.data, filterPath))
        }
        else {
            for(let filterPartKey of Object.keys(response.data)){
                let key = filterPartKey
                let filterPart : {[key :string] : lazyNode} = {}
                filterPart[key] = response.data[key]
                dispatch(AssetActions.insertNewLazyFilterTree(filterPart))
            }
        }
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("FetchLazyTree", "Error", error) : error.response.data
        }))
    })
}
export const fetchFilteredAssets = (id: number) => async (dispatch: Dispatch) => {

    await axios.get(types.getURL(ENDPOINT, "filteredAssets"), {
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response: response.data
        }))
        dispatch(AssetActions.setFilteredAssets(response.data))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("FetchFilteredAssets", "Error", error) : error.response.data
        }))
    })
}
/**
 * 
 * @param id = timestamp to allow callbacks to manage toast
 * @param expandedKeys = backend uses this to create the nodes that are currently open
 * @returns = sets the returned tree map to the lazyfilterTree
 */
export const refreshLazyFilterTree = (id:number, expandedFilterKeys : string[]) => async (dispatch: Dispatch) => {
    let url = "refreshFilterTree"
    let newKeys : string[] = []

    if (expandedFilterKeys) {
        for(let key of expandedFilterKeys){
            let newkey = cleanKeys(key)
            newkey = newkey.split("|").join("%7C")
            newKeys.push(newkey.split(",").join("~"))
        }
    }
    url += "?expandedKeys=" + newKeys.join("%2C")
    
    await axios({
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, url),
        timeout: types.DEFAULT_TIMEOUT    *10      // 10 second timeout
    }).then(response => {
        dispatch(AssetActions.setLazyFilterTree(response.data))
        
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response: types.getDefaultRsp("RefreshLazyFilterTree", "Success", null, "refresh of lazy filter tree received")
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("RefreshLazyFilterTree", "Error", error) : error.response.data
        }))
    })
}
export const fetchDashboard = (id : number = Date.now(), 
    setBannerList :React.Dispatch<React.SetStateAction<Banner[]>>, setRegionList:React.Dispatch<React.SetStateAction<Region[]>>,
    setStoreList : React.Dispatch<React.SetStateAction<Store[]>>, setDepartmentKeyList : React.Dispatch<React.SetStateAction<string[]>>,
    setFilterList : React.Dispatch<React.SetStateAction<ScaleDevice[]>>, setUnnasignedList : React.Dispatch<React.SetStateAction<ScaleDevice[]>>,
    nodeId ?: string, nodeType ?:string,) => async(dispatch : Dispatch) =>{ 
        
    let url = "dashboard"
    if(nodeId?.includes("|")){
        nodeId = nodeId.split("|")[1] 
    }
    if(nodeId  && nodeType){
        url+="?nodeId="+nodeId + "&nodeType="+nodeType
    }
    await axios({
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, url),
        timeout: 30000
    }).then(response => {
       
        let dashboard = response.data
        switch(nodeType){ //only need to set data 
            case "BANNER":
                setRegionList(dashboard.regions)
                setStoreList(dashboard.stores)
                setDepartmentKeyList(dashboard.depts)
                setFilterList(dashboard.assignedScales)
                setUnnasignedList(dashboard.unassignedScales)
                break;
            case "REGION":
                setStoreList(dashboard.stores)
                setDepartmentKeyList(dashboard.depts)
                setFilterList(dashboard.assignedScales)
                setUnnasignedList(dashboard.unassignedScales)
                break;
            case "STORE":
                setDepartmentKeyList(dashboard.depts)
                setFilterList(dashboard.assignedScales)
                setUnnasignedList(dashboard.unassignedScales)
                break; 
            default:
                setBannerList(dashboard.banners)
                setRegionList(dashboard.regions)
                setStoreList(dashboard.stores)
                setDepartmentKeyList(dashboard.depts)
                setFilterList(dashboard.assignedScales)
                setUnnasignedList(dashboard.unassignedScales)
                break;
        }
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response: types.getDefaultRsp("dashboard", "Success", null, "fetch of dashboard succesful!")
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("dashboard", "Error", error) : error.response.data
        }))
    })
}
export const importTree = (id : number, data) => async (dispatch : Dispatch) => {
    // console.log("XML imported:", data);

    // clear redux data to ensure data is not being misportrayed in the frontend
    dispatch(AssetActions.setScaleCount({assigned: 0, unassigned: 0}))
    dispatch(AssetActions.setScaleList({}))
    dispatch(AssetActions.setDepartmentList({}))
    dispatch(AssetActions.setStoreList({}))
    dispatch(AssetActions.setRegionList([]))
    dispatch(AssetActions.setBannerList([]))
    dispatch(AssetActions.setLazyTree({}))
    dispatch(AssetActions.setLazyExpandedTreeItems([]))
    dispatch(AssetActions.setSelectedItems([]))
    dispatch(AssetActions.setUpgradeDevices([]))
    dispatch(AssetActions.setUpgradeFiles([]))
    dispatch(AssetActions.setGroups({}))
    dispatch(AssetActions.setReduxPrimaryScales([]))
    let url = "importTreeView?replaceAssetList=true";
    await axios({
        method: 'POST',
        headers: {"Content-Type": "application/json",
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, url),
        data: data,
        timeout: 1800000          // 30 min timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("ImportTree", "Error", error) : error.response.data
            }))
    })
}

// New API tailored for loading CSV data to the asset tree
// Use this *only* for loading an asset tree structure; the endpoint code is not extensible enough to do anything else with CSV files yet
// Also, be careful what you send using this -- there's very little validation on the backend, so make sure the file meets the expected structure
export const importCSVToTree = (id : number, data) => async (dispatch : Dispatch) => {
    dispatch(AssetActions.setScaleCount({assigned: 0, unassigned: 0}))
    dispatch(AssetActions.setScaleList({}))
    dispatch(AssetActions.setDepartmentList({}))
    dispatch(AssetActions.setStoreList({}))
    dispatch(AssetActions.setRegionList([]))
    dispatch(AssetActions.setBannerList([]))
    dispatch(AssetActions.setLazyTree({}))
    dispatch(AssetActions.setLazyExpandedTreeItems([]))
    dispatch(AssetActions.setSelectedItems([]))
    dispatch(AssetActions.setUpgradeDevices([]))
    dispatch(AssetActions.setUpgradeFiles([]))
    dispatch(AssetActions.setGroups({}))
    dispatch(AssetActions.setReduxPrimaryScales([]))  
    await axios({
        method: 'POST',
        headers: {"Content-Type": "multipart/form-data",
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "importCSVToTree"),
        data: data,
        timeout: 120000
    })  
    .then((response) => {               // After POST request, redirect user to homepage

        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("ImportCSVToTree", "Error", error) : error.response.data
            }))
    })
}
// New API for loading CSV to a "logic table" that dictates the auto-assignment for scales--
// based on permutations of IP ranges, banners, regions, stores, and departments.
// Expected data structure (in a given row): [IP Address Start, IP Address End, Banner, Region, Store, Dept]
// Very similar to the importCSVToTree API, but has a distinct use case, hence the different endpoint.
export const importCSVToAutoAssigner = (id : number, data) => async (dispatch : Dispatch) => {
    await axios({
        method: 'POST',
        headers: {"TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "importCSVToAutoAssigner"),
        data: data,
        timeout: 300000 // 5 min timer
    })  
    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("importCSVToAutoAssigner", "Error", error) : error.response.data
            }))
    })
}

export const pushOneOffFeature = (id : number, data, ipAddress) => async (dispatch : Dispatch) => {
    let url = "pushOneOffFeature?ipAddress=" + ipAddress;
    await axios({
        method: 'POST',
        headers: {"Content-Type": "multipart/form-data",
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, url),
        data: data,
        timeout: types.DEFAULT_TIMEOUT  * 3.5        // 35 second timeout
    })  
    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("pushOneOffFeature", "Error", "The scale could not be reached.") : error.response.data
            }))
    })
}

export const getSyncStatus = (id : number, graphId: string, scaleIds : string[] , range : string, type : string) => async (dispatch : Dispatch) => {
    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "getSyncStatus")
    url = url + "?range=" + range + "&type=" + type

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: url,
        data: scaleIds,
        timeout: types.DEFAULT_TIMEOUT * 2         // 20 second timeout
    })

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response : types.getDefaultRsp("getSyncData", "Success", null, "Sync data recieved")
        }))
        let payload = response.data
        payload.id = graphId
        // console.log(response.data)
        dispatch(AssetActions.setSyncObject(payload))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getSyncData", "Error", err) : err.response.data
        }))
    })
}

//============================================================================
// Enable scale assignment rules (these are enabled by default when imported)

//Retrieve the status of an asset, this object is generated for all items in the treeView but you may need it elsewhere
export const getNodeStatus = (id : number, nodeId : string, type = "DEVICE", parentId : string) => async (dispatch : Dispatch) => {
    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "nodeStatus")
    url = url + "?id=" + nodeId + "&nodeType=" + type
    await axios({
        method: 'GET',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(parentId), 
                "Authorization": types.getAccessToken()},
        url: url,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response : types.getDefaultRsp("getNodeStatus", "Success", "Status data recieved")
        }))
        let payload = response.data
        dispatch(AssetActions.setNodeObject(payload, nodeId))
    })

    .catch(err => {                 // This catches any error and logs the message
        
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getNodeStatus", "Error", err) : err.response.data
        }))
    })
}

export const deleteAllAssets = (id: number) => async (dispatch: Dispatch) => {

    await axios({
        method: 'DELETE',
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, 'deleteAllAssets'),
        timeout: 60000 // 1 min timeout
    }).then(response => {
        dispatch(AssetActions.setScaleCount({assigned: 0, unassigned: 0}))
        dispatch(AssetActions.setScaleList({}))
        dispatch(AssetActions.setDepartmentList({}))
        dispatch(AssetActions.setStoreList({}))
        dispatch(AssetActions.setRegionList([]))
        dispatch(AssetActions.setBannerList([]))
        dispatch(AssetActions.setLazyTree({}))
        dispatch(AssetActions.setLazyExpandedTreeItems([]))
        dispatch(AssetActions.setSelectedItems([]))
        dispatch(AssetActions.setUpgradeDevices([]))
        dispatch(AssetActions.setUpgradeFiles([]))
        dispatch(AssetActions.setGroups({}))
        dispatch(AssetActions.setReduxPrimaryScales([]))        
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_DEL,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_DEL,
            success: false,
            response: error.response == null ? types.getDefaultRsp("DeleteAllAssets", "Error", error) : error.response.data
        }))
    })
}

