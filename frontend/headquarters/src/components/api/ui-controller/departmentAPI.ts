import * as types from "../responseTags";
import { AssetActions, AccessActions, RootStore} from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import { Dispatch } from "redux";
import { Department } from "../../../types/asset/AssetTypes";

const ENDPOINT = types.Endpoints.ui_controller

//==========================================================================
//Get list of departments
export const fetchDepartment = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    // fetchTodoByIdThunk is the "thunk function"
    await axios.get(types.getURL(ENDPOINT, "dept"), 
    {   headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        dispatch(AssetActions.setDepartmentList(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("fetchDepartments", "Error", err) : err.response.data
        }))
    })
}

//==========================================================================
//Add a new dept to tree
export const postDepartment = (id : number, data : Partial<Department>, domainId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: types.generateHeader(domainId),
        url: types.getURL(ENDPOINT, "dept"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
        fetchDepartment()
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("PostDepartment", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Add a new dept to tree
export const assignDeptProfile = (id : number, deptId : string, deptDomainId : string, newProfileId : string, deptNodePath: string[]) => async (dispatch : Dispatch, getState : () => RootStore) => {
    if(deptId.includes("|")){
        deptId = deptId.split("|")[1]
    }
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(deptDomainId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "dept") + "/assignProfile?deptId="+deptId+"&profileId="+newProfileId,
        data: deptNodePath,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
            fetchDepartment()
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("AssignDeptProfile", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Adds an empty department to the tree, 
//NOTE: the department needs to already exist in the server
export const assignDept = (id : number, deptId : string, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    if(deptId.includes("|")){
        deptId = deptId.split("|")[1]
    }
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "dept") + "/assign?deptId="+deptId+"&storeId="+storeId,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
            fetchDepartment()
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("AssignDepartment", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Delete a department from the list
export const deleteDepartment = (id : number, dept : Department) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(dept.requiresDomainId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "dept") + "?id="+dept.deptId,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_DEL,
            success: true,
            response : response.data
            }))
            fetchDepartment()
            
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_DEL,
            success: false,
            response : error.response == null ? types.getDefaultRsp("DeleteDepartment", "Error", error) : error.response.data
            }))

    })
}


//==========================================================================
//Removes an instance of the department from the store
//NOTE: the department needs to already exist in the server
export const removeDept = (id : number, deptId : string, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    if(deptId.includes("|")){
        deptId = deptId.split("|")[1]
    }
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "dept") + "/remove?deptId="+deptId+"&storeId="+storeId,
        timeout: types.DEFAULT_TIMEOUT   * 10       // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
        fetchDepartment()
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("RemoveDepartment", "Error", error) : error.response.data
            }))
    })
}