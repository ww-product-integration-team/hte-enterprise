import * as types from "../responseTags";
import { setResponse} from "../../../state/actions/endpointActions";
import axios from "axios";
import { AccessActions, RootStore, AssetActions} from "../../../state";
import { Dispatch } from "redux";
import { filter } from "../../assetTreeView/Sidebar";

const ENDPOINT = types.Endpoints.ui_controller


export const fetchFilters = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    // fetchTodoByIdThunk is the "thunk function"
    await axios.get(types.getURL(ENDPOINT, "filter"), 
    {   
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        // data is formatted in backend
        dispatch(AssetActions.setFilters(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("fetchFilters", "Error", err) : err.response.data
        }))
    })
}

export const postFilter = (id: number, data:any)=> async (dispatch : Dispatch, getState : () => RootStore) =>{
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "filter"),
        data: data.map(filterPart=> { return {primaryKey: 0,  filterId: "Temp", prop:filterPart.prop, value: filterPart.values} }), // Will need to format filter Object
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
            
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("PostFilter", "Error", error) : error.response.data
            }))
    })
}
export const deleteFilter = (id : number, filterId?: string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    if(!filterId){
        filterId = "DELETEALL";
    }
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "filter") + "?filterId=" + filterId,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  
    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("delete Filter", "Error", err) : err.response.data
        }))
    })
}