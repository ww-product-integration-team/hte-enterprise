import * as types from "../responseTags";
import { setResponse} from "../../../state/actions/endpointActions";
import axios from "axios";
import { ScaleProfileActions, AccessActions, RootStore} from "../../../state";
import { Dispatch } from "redux";
import { Profile } from "../../../types/asset/ProfileTypes";


const ENDPOINT = types.Endpoints.ui_controller

//==========================================================================
//Get list of profiles
export const fetchProfiles = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    // fetchTodoByIdThunk is the "thunk function"
    await axios.get(types.getURL(ENDPOINT, "profile"), 
    {   headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        dispatch(ScaleProfileActions.setScaleProfile(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("fetchProfiles", "Error", err) : err.response.data
        }))
    })
}


//==========================================================================
//Add a new profile to the backend
export const postProfile = (id: number, data : Partial<Profile>, domainId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(domainId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "profile"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DEPT,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("PostProfile", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Add a new dept to tree
export const deleteProfile = (id : number, data, parameters, domainId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(domainId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "profile") + parameters,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_DEL,
            success: true,
            response : response.data
            }))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_DEL,
            success: false,
            response : error.response == null ? types.getDefaultRsp("DeleteProfile", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Get checksum logs
export const getChecksumLogs = (id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {
    await axios.get(types.getURL(ENDPOINT, "getChecksumLogs"), {
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_PROFILE_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response=>{
        dispatch(setResponse({
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_GET,
            success: true,
            response: response.data
        }))
        dispatch(ScaleProfileActions.setChecksumLogs(response.data))
    }).catch(error=>{
        dispatch(setResponse({
            id: id,
            type: types.RSP_PROFILE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetChecksumLogs", "Error", error) : error.response.data
        }))
    })
}