import * as types from "../responseTags";
import { AssetActions, AccessActions, RootStore } from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import logger from "../../utils/logger";
import { Dispatch } from "redux";
import { ScaleDevice } from "../../../types/asset/ScaleTypes";
import React from "react";

const ENDPOINT = types.Endpoints.ui_controller
//
//==========================================================================
export const fetchScales = (id : number = Date.now(), ipAddress = "",
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScaleData: React.Dispatch<React.SetStateAction<ScaleDevice[]>>
) => async (dispatch : Dispatch, getState : () => RootStore) => {
    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "scale")
    if(ipAddress !== ""){
        url = url + "?ip="+ipAddress
    }

    await axios.get(url,
    {   headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: types.DEFAULT_TIMEOUT * 100
    })

    .then(response => {
        if (setUnassignedScales && setAssignedScales) {
            setUnassignedScales(response.data.unassignedScales)
            setAssignedScales(response.data.assignedScales)
        }
        let noDuplicates = Array.from(new Set(response.data.combinedScaleData.map(scale => scale.deviceId)))
            .map(deviceId => response.data.combinedScaleData.find(scale => scale.deviceId === deviceId))
        setCombinedScaleData(noDuplicates)
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response: types.getDefaultRsp("fetchScales", "Success", null, "Successfully retrieved scale information")
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("fetchScales", "Error", err) : err.response.data
        }))
    })
}

//==========================================================================
//Get additional stats for the scales
export const getDeviceSettings = (id : number, ipAddress = "") => async (dispatch : Dispatch, getState : () => RootStore) => {


    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "deviceSettings")
    if(ipAddress !== ""){
        url = url + "?id="+ipAddress + '&idtype=IPADDRESS'
    }
    else{
        logger.error("(getDeviceSettings) Need an ipaddress")
        return
    }

    await axios.get(url,
    {   headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
        },
        timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        dispatch(AssetActions.setScaleDetails(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getDeviceSettings", "Error", err) : err.response.data
        }))
    })
}


//==========================================================================
//Add a new scale to tree
export const postScale = (id : number, data : Partial<ScaleDevice>, parentId : string,
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null, setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScales: React.Dispatch<React.SetStateAction<ScaleDevice[]>> | null
) => async (dispatch : Dispatch, getState : () => RootStore) => {
    if(data.deptId && data.deptId.includes("|")){
        data.deptId = data.deptId.split("|")[1]
    }
    await axios({
        method: 'POST',
        headers: types.generateHeader(parentId),
        url: types.getURL(ENDPOINT, "device"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
        if (setCombinedScales) {
            fetchScales(id+1, undefined, setAssignedScales, setUnassignedScales, setCombinedScales)
        }
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("PostScale", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
// Get Apphook versions on Scales
export const fetchApphook = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios.get(types.getURL(ENDPOINT, "appHook"), 
    { headers: {
        "Authorization": types.getAccessToken(),
        "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
    },
    timeout: types.DEFAULT_TIMEOUT
    })

    .then(response => {
        dispatch(AssetActions.setAppHookObject(response.data))
    })

    .catch(err => {  
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getDeviceSettings", "Error", err) : err.response.data
        }))
    })

}
//==========================================================================
//Add a new list of scales to tree
export const postScales = (id : number, data : ScaleDevice[], parentId : string,
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScaleData: React.Dispatch<React.SetStateAction<ScaleDevice[]>>
) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(parentId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "devices"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
        fetchScales(id+1, undefined, setAssignedScales, setUnassignedScales, setCombinedScaleData)
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("PostScales", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
//Delete a scale from the list
export const deleteScale = (id : number, scaleId : string, storeId : string,
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScaleData: React.Dispatch<React.SetStateAction<ScaleDevice[]>> | null
) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "device") + 
        "?id="+ scaleId + "&idtype=UUID",
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_DEL,
            success: true,
            response : response.data
            }))
        if (setCombinedScaleData) {
            fetchScales(id+1, undefined, setAssignedScales, setUnassignedScales, setCombinedScaleData)
        }
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_DEL,
            success: false,
            response : error.response == null ? types.getDefaultRsp("DeleteScale", "Error", error) : error.response.data
            }))
    })
}


//Enable/Disable a scale
export const setScaleEnable = (id : number, ipAddress : string, enable : boolean, storeId : string,
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScaleData: React.Dispatch<React.SetStateAction<ScaleDevice[]>> | null
) => async (dispatch : Dispatch, getState : () => RootStore) => {

    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "enablescale")
    url = url + "?enable="+enable+"&ip="+ipAddress

    await axios({
        method: 'GET',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: url,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })

    .then(response => {
        dispatch(setResponse(
        {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
        if (setCombinedScaleData) {
            fetchScales(id+1, undefined, setAssignedScales, setUnassignedScales, setCombinedScaleData)
        }
    })
    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("setScaleEnable", "Error", err) : err.response.data
        }))
    })
}

//Enable/Disable a scale
export const setPrimaryScale = (id : number, upgradeId : string, isPrimary : boolean, storeId : string,
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScaleData: React.Dispatch<React.SetStateAction<ScaleDevice[]>> | null
) => async (dispatch : Dispatch, getState : () => RootStore) => {

    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "device")
    url = url + "/setPrimary?isPrimary="+isPrimary+"&upgradeId="+upgradeId

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: url,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
        if (setCombinedScaleData) {
            fetchScales(id+1, undefined, setAssignedScales, setUnassignedScales, setCombinedScaleData)
        }
    }).catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse({
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("setPrimaryScale", "Error", err) : err.response.data
        }))
    })
}

//Assigns a manual scale profile
export const setScaleProfile = (id : number, scaleId : string, profileId : string, storeId : string,
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScaleData: React.Dispatch<React.SetStateAction<ScaleDevice[]>> | null
) => async (dispatch : Dispatch, getState : () => RootStore) => {

    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "device")
    url = url + "/assignProfile?scaleId="+scaleId + "&profileId="+profileId

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(storeId), 
                "Authorization": types.getAccessToken()},
        url: url,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })

    .then(response => {
        dispatch(setResponse(
        {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
        if (setCombinedScaleData) {
            fetchScales(id+1, undefined, setAssignedScales, setUnassignedScales, setCombinedScaleData)
        }
    })
    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("setScaleProfile", "Error", err) : err.response.data
        }))
    })
}

//Enable/Disable a scale
export const setScaleAssignment = (id : number, scales : string[], parentId : string, newStoreId : string, newDeptId : string,
    setAssignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setUnassignedScales: React.Dispatch<React.SetStateAction<number>> | null,
    setCombinedScaleData: React.Dispatch<React.SetStateAction<ScaleDevice[]>> | null
) => async (dispatch : Dispatch, getState : () => RootStore) => {
    if(newDeptId.includes("|")){
        newDeptId = newDeptId.split("|")[1]
    }
    // fetchTodoByIdThunk is the "thunk function"
    let url = types.getURL(ENDPOINT, "device")
    url = url + "/assignScale?deptId=" + newDeptId + "&storeId=" + newStoreId
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(parentId), 
                "Authorization": types.getAccessToken()},
        url: url,
        data: scales, 
        timeout: 10000          // 10 second timeout
    })

    .then(response => {
        dispatch(setResponse(
        {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
        if (setCombinedScaleData) {
            fetchScales(id+1, undefined, setAssignedScales, setUnassignedScales, setCombinedScaleData)
        }
    })
    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_SCALE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("setScaleAssignment", "Error", err) : err.response.data
        }))
    })
}
