import { Dispatch } from "redux"
import { AccessActions, RootStore } from "../../../state"
import * as types from "../responseTags"
import axios from "axios"
import { setResponse } from "../../../state/actions/endpointActions"

/* This file is used for getting license information from the Cloud Azure server */

const cloudServer = "http://hte-enterprise.com:8080/hte-enterprise/licensing/"
let accountEmail: string

export const setEmail = (email: string) => {
    accountEmail = email
}

export const getLicenses = (id: number = Date.now()) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios.get(cloudServer + "list?email=" + accountEmail , {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_GET,
            success: true,
            response: response.data
        }))
        dispatch(AccessActions.setCloudLicenses(response.data))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetLicenses", "Error", error) : error.response.data
        }))
        dispatch(AccessActions.setCloudLicenses([]))
    })
}

export const downloadLicense = (id: number = Date.now(), filename: string) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios.get(cloudServer + "download?licenseFilename=" + filename, {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_GET,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetLicenses", "Error", error) : error.response.data
        }))
    })
}
