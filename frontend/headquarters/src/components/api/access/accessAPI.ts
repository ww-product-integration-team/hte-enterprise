import * as types from "../responseTags";
import { setResponse } from "../../../state/actions/endpointActions";
import { AccessActions, RootStore} from "../../../state/index"
import axios from "axios";
import { Dispatch} from "redux";

const ENDPOINT : types.Endpoints = types.Endpoints.access_controller

//==========================================================================
//Get info of the user that is currently logged in
export const getAccountInfo = (id : number, username : string) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "user") + "?id="+username+"&idtype=USERNAME",
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })
    .then(response => {
        dispatch(AccessActions.setAccountInfo(response.data))
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getAccountInfo") : err.response.data
        }))
    })
}

//==========================================================================
//Get info of other users that are registered on the site
export const getUserInfo = (id : number = Date.now(), username ?: number) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let placeholder = ""
    if (username) {
        placeholder = `?id=${username}&idtype=USERNAME`
    }
    
    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "user") + placeholder,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })
    .then(response => {
        dispatch(AccessActions.setUserInfo(response.data))
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getUserInfo") : err.response.data
        }))
    })
}

//==========================================================================
//Update list of available roles
export const getRoles = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT,"roles"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })
    .then(response => {
        dispatch(AccessActions.setRoles(response.data))
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_ROLE,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_ROLE,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getRoles") : err.response.data
        }))
    })
}

//==========================================================================
//Update list of current domains
export const getDomains = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "domains"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })
    .then(response => {
        dispatch(AccessActions.setDomains(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_DOMAIN,
            action: types.ACTION_GET,
            success: false,
            response : err
        }))
    })
}


//==========================================================================
// Install a new enterprise license to the backend
export const installLicense = (id : number, fileData : any, createUser?: boolean) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let url = types.getURL(ENDPOINT, "license")
    if (createUser !== undefined) {
        url += "?createUser=" + createUser
    }

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        url: url,
        data: fileData,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    }).catch(err => {             // This catches any error and logs the message
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("installLicense") : err.response.data
        }))
    })
}


//==========================================================================
// Get HTe Enterprise License
export const getEnterpriseLicense = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "license"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    }).then(response => {
        dispatch(AccessActions.setHTeLicense(response.data))
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    }).catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse({
            id: id,
            type: types.RSP_LICENSE,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getLicense") : err.response.data
        }))
    })
}


//==========================================================================
// Change Password
export const changePassword = (id: number, username : string, newpassword: string) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let encodedPassword = encodeURIComponent(newpassword)   // Allows special characters
    let parameters = `?newPassword=${encodedPassword}&username=${username}`

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json',
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "user/changePassword") + parameters,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("changePassword") : err.response.data
        }))
    })
}