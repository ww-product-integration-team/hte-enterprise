import * as types from "../responseTags";
import { setResponse } from "../../../state/actions/endpointActions";
import { AccessActions, RootStore } from "../../../state/index"
import axios from "axios";
import { Dispatch } from "redux";
import { HTeUser } from "../../../types/access/AccessTypes";

const ENDPOINT = types.Endpoints.access_controller

//==========================================================================
// Used to Log in
export const requestLogin = (id : number, username : string, password : string) => async (dispatch : Dispatch, getState : () => RootStore) => {

    // TODO: need to hide username and password
    await axios({
        method: 'GET',
        url: types.getURL(ENDPOINT, "login") + "?username="+username + "&password="+password,
        timeout: types.DEFAULT_TIMEOUT * 10         // 10 second timeout
    }).then(response => {
        dispatch(AccessActions.setAccessTokens(response.data, "LOGIN"))
        dispatch(AccessActions.setIsRefreshingToken("false"))
        dispatch(setResponse({
            id: id,
            type: types.RSP_LOGIN,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    }).catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse({
            id: id,
            type: types.RSP_LOGIN,
            action: types.ACTION_POST,
            success: false,
            response : (!err.response || !err.response.data) ? types.getDefaultRsp("requestLogin") : err.response.data
        }))
    })
}


//==========================================================================
// Create account / Edit account (if provided an existing account ID)
export const createAccount = (id : number, data : Partial<HTeUser>) => async (dispatch : Dispatch, getState : () => RootStore) => {

    if(data.domain){
        await axios({
            method: 'POST',
            headers: {'Content-Type': 'application/json', 
                    "TargetDomain": types.getDomainToken(data.domain.parentId? data.domain.parentId : data.domain.domainId), 
                    "Authorization": types.getAccessToken()},
            url: types.getURL(ENDPOINT, "user"),
            data: data,
            timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
        })  
        .then(response => {
            // dispatch(AccessActions.setAccountInfo(response.data))
            dispatch(setResponse(
                {
                id: id,
                type: types.RSP_USER,
                action: types.ACTION_POST,
                success: true,
                response : response.data
            }))
        })

        .catch(err => {                 // This catches any error and logs the message
            dispatch(setResponse(
                {
                id: id,
                type: types.RSP_USER,
                action: types.ACTION_POST,
                success: false,
                response : err.response == null ? types.getDefaultRsp("createAccount", "Error", err) : err.response.data
            }))
        })
    }
    else {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_POST,
            success: false,
            response : types.getDefaultRsp("createAccount", "Error", "Domain ID was not provided") 
        }))
    }
}


//==========================================================================
// Delete Account
export const deleteAccount = (id : number, data : HTeUser | undefined) => async (dispatch : Dispatch, getState : () => RootStore) => {
    
    let placeholder = ""
    if (data)       placeholder = `?id=${data.id}&idtype=UUID`
    if (data) 
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(data.domain.parentId? data.domain.parentId : AccessActions.DEFAULT_ADMIN_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "user") + placeholder,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_DEL,
            success: true,
            response : response
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_DEL,
            success: false,
            response : err.response == null ? types.getDefaultRsp("deleteAccount", "Error", err) : err.response.data
        }))
    })
}


//==========================================================================
// Enable/Disable User
export const enableUser = (id : number, userId : HTeUser, enabled : boolean) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let placeholder = `?enabled=${enabled}&userId=${userId}`

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(userId.domain.parentId? userId.domain.parentId : AccessActions.DEFAULT_ADMIN_DOMAIN_ID ), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "user/enable") + placeholder,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("enableUser", "Error", err) : err.response.data
        }))
    })
}


//==========================================================================
// Assign Roles to account
export const assignRoles = (id : number, userId : HTeUser, roles) => async (dispatch : Dispatch, getState : () => RootStore) => {
    let roleList = `?`

    for (let i = 0; i < roles.length; i++)      roleList = roleList + `roleId=${roles[i].id}&`

    if (roleList !== `?`)       roleList = roleList + `userId=${userId}`
    else                        roleList = `?roleId=&userId=${userId}`

    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(userId.domain.parentId? userId.domain.parentId : AccessActions.DEFAULT_ADMIN_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "assignRole") + roleList,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  
    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_POST,
            success: true,
            response : response
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_USER,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("assignRoles", "Error", err) : err.response.data
        }))
    })
}


//==========================================================================
//Get new tokens if current ones have expired using the refresh token
export const requestRefresh = (id : number) => async (dispatch : Dispatch, getState : () => RootStore) => {
    // fetchTodoByIdThunk is the "thunk function"
    dispatch(AccessActions.setIsRefreshingToken('true'))
    await axios({
        method: 'GET',
        headers: {'Content-Type': 'application/json', 
                "Authorization": types.getRefreshToken()},
        url: types.getURL(ENDPOINT, "refreshToken"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  
    .then(response => {
        dispatch(AccessActions.setAccessTokens(response.data, "REFRESH"))
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_LOGIN,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
        dispatch(AccessActions.setIsRefreshingToken('false'))
        dispatch(AccessActions.checkTokenStatus())
    })
    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_LOGIN,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("requestRefresh", "Error", err) : err.response.data
        }))
        dispatch(AccessActions.setIsRefreshingToken('failed'))
        dispatch(AccessActions.checkTokenStatus())
    })
}