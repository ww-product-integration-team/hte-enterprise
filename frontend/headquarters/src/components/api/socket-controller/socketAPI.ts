import axios from "axios";
import { Dispatch } from "redux";
import { AccessActions, RootStore } from "../../../state";
import { setResponse } from "../../../state/actions/endpointActions";
import * as types from "../responseTags";

//==========================================================================
// Socket API
//==========================================================================
const ENDPOINT = types.Endpoints.socket_controller

//==========================================================================
// Get Fleet Status 
// Rest API post to instruct the server to send getStatusInfo requests to each of the requested IPs
// @param id - unique id for this request
// @param data - list of IPs to get status from
export const postFleetStatus = (id : number, data : string[]) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: types.generateHeader(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
        url: types.getURL(ENDPOINT, "fleetStatus"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  
    .then((response) => {
        dispatch(setResponse(
            {
                id: id,
                type: types.RSP_FLEET_STATUS,
                action: types.ACTION_POST,
                success: true,
                response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FLEET_STATUS,
            action: types.ACTION_POST,
            success: false,
            response : (!error.response || !error.response.data) ? types.getDefaultRsp("PostFleetStatus", "Error", error) : error.response.data
            }))
    })
}

//==========================================================================
// Cancel Fleet Status Scan
export const cancelFleetStatus = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: types.generateHeader(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
        url: types.getURL(ENDPOINT, "fleetStatus/cancel"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  
    .then((response) => {
        dispatch(setResponse(
            {
                id: id,
                type: types.RSP_FLEET_STATUS,
                action: types.ACTION_POST,
                success: true,
                response : response.data
            }))
    })
    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FLEET_STATUS,
            action: types.ACTION_POST,
            success: false,
            response : (!error.response || !error.response.data) ? types.getDefaultRsp("CancelFleetStatus", "Error", error) : error.response.data
            }))
    })
}