import * as types from "../responseTags";
import { setResponse } from "../../../state/actions/endpointActions";
import axios, { CancelTokenSource } from "axios";
import { AccessActions, AssetActions, RootStore} from "../../../state";
import logger from '../../utils/logger';
import { TransactionRequest } from '../../../types/transaction/transactionTypes';
import { Dispatch } from "redux";
import { DownloadItemOptions } from "../../scaleProfiles/fileDownload/Downloader";
import configData from "../../../config/config.json"

const CONFIG_PRODUCT = configData.SERVER_PRODUCT

function generateFullURL(fileInfo : TransactionRequest, service : string){
    let finalURL = fileInfo.url + CONFIG_PRODUCT + "transservice/"+service+"?start="+fileInfo.startDate+"&end="+fileInfo.endDate
    if(fileInfo.scaleIps && fileInfo.scaleIps.length > 0){
        finalURL+="&scale_ips="
        let prefix=""
        fileInfo.scaleIps.forEach((ip)=>{
            finalURL+=(prefix+ip)
            prefix="%2C"
        })
    }

    if(fileInfo.plus && fileInfo.plus.length > 0){
        finalURL+="&plu_list="
        let prefix=""
        fileInfo.plus.forEach((plu)=>{
            finalURL+=(prefix+plu)
            prefix="%2C"
        })
    }

    if(fileInfo.storeId){
        finalURL+="&store_ids="+fileInfo.storeId
    }

    if(fileInfo.deptId){
        finalURL+="&dept_ids="+fileInfo.deptId
    }

    return finalURL
}

export const downloadTransactions = (id : number, fileInfo : TransactionRequest, options : DownloadItemOptions, controller : CancelTokenSource) => async (dispatch : Dispatch, getState : () => RootStore) => {

    const finalURL = generateFullURL(fileInfo, "excelExport")

    await axios({
        method: 'GET',
        headers: {
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        responseType: "blob",
        url: finalURL,
        timeout: 120000,          // 120 second timeout
        cancelToken: controller.token,
        ...options
    })  

    .then((response) => {               // After POST request, re-render the page
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_GET,
            success: true,
            response : response.data,
            }))       
    })

    .catch((error) => {
        // User cancelled upload
        if (axios.isCancel(error)){
            logger.info("(transDownload) Request Cancelled")

            dispatch(setResponse(
            {
                id: id,
                type: types.RSP_FILEMGR,
                action: types.ACTION_GET,
                success: false,
                cancelled: true,
                response : error.response == null ? types.getDefaultRsp("Transaction Download", "Warning", null, "The request has been Cancelled!") : error.response.data
            }))
        }

        // Download failed for other reason
        else {
            dispatch(setResponse(
            {
                id: id,
                type: types.RSP_FILEMGR,
                action: types.ACTION_GET,
                success: false,
                // NOTE: Below error response is currently hardcoded
                response : error.response == null ? types.getDefaultRsp("Transaction Download", "Error", "The requested file could not be downloaded.", error) : error.response.data
            }))
        }
    })
}

//==========================================================================
//Get list of stores
export const fetchTransactions = (id : number, fileInfo : TransactionRequest, databaseId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {

    const finalURL = generateFullURL(fileInfo, "fetchTransactions")

    await axios({
        method: 'GET',
        headers: {
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: finalURL,
        timeout: types.DEFAULT_TIMEOUT,          // 10 second timeout
    })  

    .then(response => {
        dispatch(AssetActions.setTransactions(response.data, databaseId))
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_TRANS,
            action: types.ACTION_GET,
            success: true,
            response : {status: "Fetched transactions", records: response.data.list.length}
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_TRANS,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("fetchTransactions", "Error", err) : err.response.data
        }))
    })
}