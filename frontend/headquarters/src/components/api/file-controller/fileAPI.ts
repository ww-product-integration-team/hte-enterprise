import * as types from "../responseTags";
import { setResponse } from "../../../state/actions/endpointActions";
import axios, { CancelTokenSource } from "axios";
import { ScaleProfileActions, AccessActions, RootStore} from "../../../state";
import logger, { LogType } from '../../utils/logger';
import { Dispatch } from "redux";
import { DownloadItemOptions } from "../../scaleProfiles/fileDownload/Downloader";
import { setImageData } from "../../../state/actions/profileActions";
import { FileForEvent } from "../../../types/asset/FileTypes";


const ENDPOINT = types.Endpoints.file_controller
//==========================================================================
//Upload a new file to the backend
export const fileUpload = (
    id : number, 
    data, 
    parameters, 
    uploadPercent, 
    setUploadPercent, 
    setUploadProgress,
    controller
) => async (dispatch : Dispatch, getState : () => RootStore) => {

    // logger("(fileUpload) Controller: ", controller)
    // logger("(fileUpload) fileData in FileAPI.jsx: ", data)
    console.log(parameters)
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "upload") + parameters,
        data: data,
        timeout: 120000,          // 120 second timeout
        cancelToken: controller.token,
        onUploadProgress: function(progressEvent) {
            setUploadProgress(progressEvent)
            let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)

            if (uploadPercent < 100) {
                setUploadPercent(percentCompleted)
            }
        }
    })

    .then((response) => {               // After POST request, re-render the page
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    })

    .catch((error) => {
        // User cancelled upload
        if (axios.isCancel(error)){
            logger.info("(fileUpload) Request Cancelled")

            dispatch(setResponse(
                {
                id: id,
                type: types.RSP_FILEMGR,
                action: types.ACTION_POST,
                success: false,
                cancelled: true,
                response : error.response == null ? types.getDefaultRsp("FileUpload", "Warning", null, "Request has been Cancelled!") : error.response.data
            }))
        }

        // Upload failed for other reason
        else {
            logger.error("(fileUpload) Request Failed")

            dispatch(setResponse(
                {
                id: id,
                type: types.RSP_FILEMGR,
                action: types.ACTION_POST,
                success: false,
                response : error.response == null ? types.getDefaultRsp("FileUpload", "Error", error, "Error trying to upload file. Please try again later.") : error.response.data
            }))
        }
    })
}


//==========================================================================
//Update an existing file
export const fileUpdate = (id : number, data : FileForEvent) => async (dispatch : Dispatch, getState : () => RootStore) => {
    
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "update"),
        data: data,
        timeout: types.DEFAULT_TIMEOUT,          // 10 second timeout
    })  

    .then((response) => {               // After POST request, re-render the page
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_POST,
            success: true,
            response : response.data,
            }))

    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("FileUpdate", "Error", error) : error.response.data
            }))
    })
}


//==========================================================================
//Delete an existing file
export const fileDelete = (id : number, parameters : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'DELETE',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "delete") + parameters,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, re-render the page
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_DEL,
            success: true,
            response : response.data
            }))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_DEL,
            success: false,
            response : error.response == null ? types.getDefaultRsp("FileDelete", "Error", error) : error.response.data
            }))
    })
}


//==========================================================================
//Get files and feature files and store them in Redux Store separately
export const fetchFiles = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let featureFiles : FileForEvent[] = []
    let htFiles : FileForEvent[] = []
    let tgzFiles : FileForEvent[] = []
    let debFiles : FileForEvent[] = []
    let installImageFiles : FileForEvent[] = []
    let licFiles : FileForEvent[] = []
    let mediaFiles : FileForEvent[] = []

    await axios({
        method: 'GET',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "getFileInfo"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, re-render the page

        for (let i = 0; i < response.data.length; i++) {
            // Check if file returned is a .package.tgz file
            if (response.data[i].filename.slice(-12) === ".package.tgz") {
                featureFiles.push(response.data[i])
            }
            // Check if file returned is a .tgz file
            else if (response.data[i].filename.slice(-4) === ".tgz") {
                tgzFiles.push(response.data[i])
            }
            // Check if file returned is a .deb file
            else if (response.data[i].filename.slice(-4) === ".deb") {
                debFiles.push(response.data[i])
            }
            // Check if file returned is a .img.bz2 file
            else if (response.data[i].filename.slice(-8) === ".img.bz2") {
                installImageFiles.push(response.data[i])
            }
            // Check if file returned is a .lic file
            else if (response.data[i].filename.slice(-4) === ".lic") {
                licFiles.push(response.data[i])
            }
            // Check if file returned is a .ht or .label_tagged file
            else if(response.data[i].filename.slice(-3) === ".ht" ||
                response.data[i].filename.slice(-12) === ".label_tagged"){
                    htFiles.push(response.data[i])
            }    
            else{
                mediaFiles.push(response.data[i])
            }
        }
        
        dispatch(ScaleProfileActions.setConfigFiles(htFiles))
        dispatch(ScaleProfileActions.setFeatureFiles(featureFiles))
        dispatch(ScaleProfileActions.setTarballFiles(tgzFiles))
        dispatch(ScaleProfileActions.setDebianFiles(debFiles))
        dispatch(ScaleProfileActions.setInstallImageFiles(installImageFiles))
        dispatch(ScaleProfileActions.setMediaFiles(mediaFiles))
        dispatch(ScaleProfileActions.setLicenseFiles(licFiles))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_GET,
            success: false,
            response : error.response == null ? types.getDefaultRsp("FetchFiles", "Error", error) : error.response.data
            }))
    })
}


//==========================================================================
// For Feature Files (package.tgz files). 
// GET request that returns array of all entries of the given fileName between different scale profiles
export const fetchFileNames = (id : number, fileName : string) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "filenameInProfiles") + `?filename=${fileName}`,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_GET,
            success: true,
            response : response.data,
            }
        ))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_GET,
            success: false,
            response : error.response == null ? types.getDefaultRsp("FetchSameFileNames", "Error", error) : error.response.data
            }
        ))
    })
}


//==========================================================================
//Download a file using the file downloader
export const fileDownload = (id : number, fileId : string, options : DownloadItemOptions, controller : CancelTokenSource) => async (dispatch : Dispatch, getState : () => RootStore) => {
    
    let parameters = '?id=' + fileId
    await axios({
        method: 'GET',
        headers: {
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        responseType: "blob",
        url: types.getURL(ENDPOINT, "download") + parameters,
        timeout: 120000,          // 120 second timeout
        cancelToken: controller.token,
        ...options
    })  

    .then((response) => {               // After POST request, re-render the page
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_GET,
            success: true,
            response : response.data,
            }))       
    })

    .catch((error) => {
        // User cancelled upload
        if (axios.isCancel(error)){
            logger.info("(fileDownload) Request Cancelled")

            dispatch(setResponse(
            {
                id: id,
                type: types.RSP_FILEMGR,
                action: types.ACTION_GET,
                success: false,
                cancelled: true,
                response : error.response == null ? types.getDefaultRsp("FileDownload", "Warning", null, "The request has been Cancelled!") : error.response.data
            }))
        }

        // Upload failed for other reason
        else {
            dispatch(setResponse(
            {
                id: id,
                type: types.RSP_FILEMGR,
                action: types.ACTION_GET,
                success: false,
                // NOTE: Below error response is currently hardcoded
                response : error.response == null ? types.getDefaultRsp("FileDownload", "Error", error, "The requested file could not be downloaded.") : error.response.data
            }))
        }
    })
}


//==========================================================================
//Add a new store to tree
export const postLogs = (id : number, logList : LogType[]) => async (dispatch : Dispatch, getState : () => RootStore) => {
    await axios({
        method: 'POST',
        headers: {'Content-Type': 'application/json', 
                "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                "Authorization": types.getAccessToken()},
        url: types.getURL(ENDPOINT, "log"),
        data: logList,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then((response) => {               // After POST request, redirect user to homepage
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_POST,
            success: true,
            response : response.data
            }))
        localStorage.setItem(logger.logName, JSON.stringify([]))
    })

    .catch((error) => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_FILEMGR,
            action: types.ACTION_POST,
            success: false,
            response : error.response == null ? types.getDefaultRsp("UploadFrontendLogs", "Error", error) : error.response.data
            }))
    })
}


//==========================================================================
//Fetch Image from the server
export const fetchImage = (id : number, fileId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
        
        let parameters = '?fileId=' + fileId
        await axios({
            method: 'GET',
            headers: {
                    "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID), 
                    "Authorization": types.getAccessToken()},
            responseType: 'arraybuffer',
            url: types.getURL(ENDPOINT, "getImage") + parameters,
            timeout: types.DEFAULT_TIMEOUT,
        })  
    
        .then((response) => {     
            const base64 = 'data:;base64,' + window.btoa(
                new Uint8Array(response.data).reduce(
                  (data, byte) => data + String.fromCharCode(byte),
                  '',
                ),
            );
            dispatch(setImageData(fileId, base64))
        })
    
        .catch((error) => {
            
            dispatch(setResponse(
            {
                id: id,
                type: types.RSP_FILEMGR,
                action: types.ACTION_GET,
                success: false,
                // NOTE: Below error response is currently hardcoded
                response : (!error.response || !error.response.data)  ? types.getDefaultRsp("FileDownload", "Error", error, "The requested file could not be downloaded.") : error.response.data
            }))
            
        })
    }
