import { Dispatch } from "redux"
import { AccessActions, AssetActions, RootStore } from "../../../state"
import { setResponse } from "../../../state/actions/endpointActions"
import * as types from "../responseTags"
import axios from "axios"
import { Batch, Group, Timeframe, UpgradeDevice } from "../../upgradeManager/UpgradeManager"

const ENDPOINT = types.Endpoints.upgrade_controller

export const saveBatch = (batch: Batch, id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {
    
    let formattedBatch = {
        batchId: batch.batchId,
        name : batch.name,
        description : batch?.description,
        uploadStart : new Date(batch.uploadStart),
        uploadEnd : new Date(batch.uploadEnd),
        upgradeStart : new Date(batch.upgradeStart),
        upgradeEnd : new Date(batch.upgradeEnd),
        fileId: batch.fileId,
        fileName: batch.fileName,
        afterUpgradeCheckIn : batch.afterUpgradeCheckIn
    }
    
    await axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Authorization': types.getAccessToken()
        },
        url: types.getURL(ENDPOINT, "saveBatch"),
        data:formattedBatch,
        timeout: types.DEFAULT_TIMEOUT // 10 seconds
    }).then((response) => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("SaveBatch", "Error", error) : error.response.data
        }))
    })
}
export const getBatches = (id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios.get(types.getURL(ENDPOINT, "getBatches"), {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        timeout: 30000
    }).then(response => {
        dispatch(AssetActions.setBatches(response.data))

    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetBatches", "Error", error) : error.response.data
        }))
    })
}

export const deleteBatch = (batchId: string, id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Authorization': types.getAccessToken()
        },
        url: types.getURL(ENDPOINT, 'deleteBatch?batchId=' + batchId),
        data: batchId,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_DEL,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_DEL,
            success: false,
            response: error.response == null ? types.getDefaultRsp("DeleteBatch", "Error", error) : error.response.data
        }))
    })
}
export const saveGroup = (group: Group, id: number, batchUpgrade: boolean) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Authorization': types.getAccessToken()
        },
        url: types.getURL(ENDPOINT, "saveGroup?batchUpgrade=") + batchUpgrade,
        data: group,
        timeout: types.DEFAULT_TIMEOUT*30 // 10 seconds
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("CreateGroup", "Error", error) : error.response.data
        }))
    })
}

export const uploadFile = (file, id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Authorization': types.getAccessToken()
        },
        url: types.getURL(ENDPOINT, "uploadFile"),
        data: file,
        timeout: 600000, // 5 minutes
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("UploadFile", "Error", error) : error.response.data
        }))
    })
}

export const enableGroups = (groups: Group[], id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Authorization': types.getAccessToken()
        },
        url: types.getURL(ENDPOINT, "enableGroups"),
        data: groups,
        timeout: types.DEFAULT_TIMEOUT // 10 seconds
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("EnableGroups", "Error", error) : error.response.data
        }))
    })
}

export const updateBatchAssignments = (batchId: string, devices: UpgradeDevice[], id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: types.generateHeader(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
        url: types.getURL(ENDPOINT, "updateBatchAssignment?batchId=" + batchId),
        data: devices,
        timeout: types.DEFAULT_TIMEOUT
    }).then((response) => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("UpdateBatchAssignment", "Error", error) : error.response.data
        }))
    })
}

export const assignManualFile = (id: number, devices: UpgradeDevice[]) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: types.generateHeader(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
        url: types.getURL(ENDPOINT, "manualFileAssign"),
        data: devices,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("ManualFileAssign", "Error", error) : error.response.data
        }))
    })
}

export const getGroups = (id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios.get(types.getURL(ENDPOINT, "getGroups"), {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        timeout: 30000
    }).then(response => {
        dispatch(AssetActions.setGroups(response.data))
        let primaryScales: any[] = []
        for (let groupId of Object.keys(response.data)) {
            for (let scale of response.data[groupId].scales) {
                if (scale.primaryScale) {
                    primaryScales.push(scale.upgradeId)
                }
            }
        }
        dispatch(AssetActions.setReduxPrimaryScales(primaryScales))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetGroups", "Error", error) : error.response.data
        }))
    })
}

export const deleteGroup = (id: number, groupId: string) => async (dispatch: Dispatch, getState: () => RootStore) => {
    
    await axios({
        method: 'DELETE',
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Content-Type': 'application/json'
        },
        url: types.getURL(ENDPOINT, 'deleteGroup?groupId=') + groupId,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_DEL,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_DEL,
            success: false,
            response: error.response == null ? types.getDefaultRsp("DeleteGroup", "Error", error) : error.response.data
        }))
    })
}

export const getDevices = (id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios.get(types.getURL(ENDPOINT, "getDevices"), {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        timeout: 30000
    }).then(response => {
        dispatch(AssetActions.setUpgradeDevices(response.data))
        let primaries: string[] = []
        for (let upgradeId of Object.keys(response.data)) {
            if (response.data[upgradeId].primaryScale) {
                primaries.push(upgradeId)
            }
        }
        dispatch(AssetActions.setReduxPrimaryScales(primaries))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetDevices", "Error", error) : error.response.data
        }))
    })
}



export const getUtilityInfo = (id: number, devices: string[], batchUpgrade: boolean) => async (dispatch: Dispatch, getState: () => RootStore) => {

    let url = types.getURL(ENDPOINT, "utilitiesInfo?devices=") + devices + "&batchUpgrade=" + batchUpgrade

    await axios.get(url, {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT*devices.length // time is based on the number of requests to the backend
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetUpgradeInfo", "Error", error) : error.response.data
        }))
    })
}

export const uploadBatchFileToScales = (id: number, scales: string[]) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Content-Type': 'application/json'
        },
        url: types.getURL(ENDPOINT, 'uploadFilesToScales'),
        data: scales,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("UploadFilesToScales", "Error", error) : error.response.data
        }))
    })
}

export const upgradeScales = (id: number, scales: string[], upgradeFileDelete: boolean) => async (dispatch: Dispatch, getState: () => RootStore) => {
    await axios({
        method: 'POST',
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Content-Type': 'application/json'
        },
        url: types.getURL(ENDPOINT, 'upgradeDevices?upgradeFileDelete=') + upgradeFileDelete,
        data: scales,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("UpgradeDevices", "Error", error) : error.response.data
        }))
    })
}

export const manageTimeframes = (id: number, timeframe: Timeframe) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID),
            'Content-Type': 'application/json'
        },
        url: types.getURL(ENDPOINT, 'timeframe'),
        data: timeframe,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Set Timeframes", "Error", error) : error.response.data
        }))
    })
}

export const getTimeframes = (id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios.get(types.getURL(ENDPOINT, 'timeframe'), {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        let realTimes: Timeframe = { // The date doesn't matter, we just need the time
            uploadPrimaryStart: new Date(2000, 0, 1, response.data.uploadPrimaryStart, 0, 0).toISOString(),
            uploadPrimaryEnd: new Date(2000, 0, 1, response.data.uploadPrimaryEnd, 0, 0).toISOString(),
            uploadSecondaryStart: new Date(2000, 0, 1, response.data.uploadSecondaryStart, 0, 0).toISOString(),
            uploadSecondaryEnd: new Date(2000, 0, 1, response.data.uploadSecondaryEnd, 0, 0).toISOString(),
            upgradeTimeStart: new Date(2000, 0, 1, response.data.upgradeTimeStart, 0, 0).toISOString(),
            upgradeTimeEnd: new Date(2000, 0, 1, response.data.upgradeTimeEnd, 0, 0).toISOString()
        }
        dispatch(AssetActions.setTimeframes(realTimes))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Set Timeframes", "Error", error) : error.response.data
        }))
    })
}

export const uploadManualFileToScales = (id: number, filename: string, fileSize: number, ipAddresses: string[]) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            "Authorization": types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "uploadManualUpgradeFile?fileName=") + filename,
        data: ipAddresses,
        timeout: 3 * (types.DEFAULT_TIMEOUT * ipAddresses.length * (fileSize / 1000))
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Upload Manual Upgrade File", "Error", error) : error.response.data
        }))
    })
}

export const applyManualFileToScales = (id: number, filename: string, fileSize: number, ipAddresses: string[], upgradeFileDelete: boolean) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios({
        method: "POST",
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_BANNER_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "applyManualUpgradeFile?fileName=") + filename + "&upgradeFileDelete=" + upgradeFileDelete,
        data: ipAddresses,
        timeout: types.DEFAULT_TIMEOUT * ipAddresses.length * (fileSize / 1000)
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Apply Manual Upgrade File", "Error", error) : error.response.data
        }))
    })
}