import { format } from 'date-fns';
import configData from '../../config/config.json'
import { store } from '../../state';
import logger from '../utils/logger';

//Action Types
export const ACTION_GET = "ACTION_GET"
export const ACTION_POST = "ACTION_POST"
export const ACTION_DEL = "ACTION_DELETE"

//ResponseTypes
export const RSP_BANNER = "RSP_BANNER"
export const RSP_REGION = "RSP_REGION"
export const RSP_STORE = "RSP_STORE"
export const RSP_DEPT = "RSP_DEPT"
export const RSP_SCALE = "RSP_SCALE"
export const RSP_RULES = "RSP_RULES"
export const RSP_PROFILE = "RSP_PROFILE"
export const RSP_FILEMGR = "RSP_FILEMGR"
export const RSP_CONTROLLER = "RSP_CONTROLLER"
export const RSP_ACCOUNT = "RSP_ACCOUNT"
export const RSP_DASHBOARD = "RSP_DASHBOARD"
export const RSP_TRANS = "RSP_TRANS"
export const RSP_EVENT = "RSP_EVENT"
export const RSP_UPGRADE = "RSP_UPGRADE"
export const RSP_PRICING_ZONE = "RSP_PRICING_ZONE"

export const RSP_LOGIN = "RSP_LOGIN"
export const RSP_LOGOUT = "RSP_LOGOUT"
export const RSP_REFRESH = "RSP_REFRESH"
export const RSP_USER = "RSP_USER"
export const RSP_ROLE = "RSP_ROLE"
export const RSP_DOMAIN = "RSP_DOMAIN"
export const RSP_LICENSE = "RSP_LICENSE"
export const RSP_CONFIG = "RSP_CONFIG"

export const RSP_FLEET_STATUS = "RSP_FLEET_STATUS"

export const DEFAULT_TIMEOUT = 10000

export enum Endpoints {
    file_controller = "file-controller",
    heartbeat_service = "heartbeat-service",
    scale_controller = "scale-controller",
    statistics = "statistics",
    ui_controller = "ui-controller",
    access_controller = "access-controller",
    config_controller =  "config-controller",
    socket_controller = "socket-controller",
    messager = "messager",
    event_controller = "event-controller",
    upgrade_controller = "upgrade-controller",
    product_controller = "products-controller",
    license_controller = "license-controller",
    pricing_zone_controller = "pricing-zone-controller"
}

export function generateHeader(domainId : string){
    if(store.getState().config.inTestingMode){
        return {'Content-Type': 'application/json',
                "TargetDomain": getDomainToken(domainId), 
                "Authorization": getAccessToken(),
                "testingValidation": "crGFr5hGi58KEixP9BbB38BHb57aruAoj8GjwDCmYEkctLbhm"}
    }
    else{
        return {'Content-Type': 'application/json', 
                "TargetDomain": getDomainToken(domainId), 
                "Authorization": getAccessToken()}
    }
}

export function getDefaultRsp(operation : string, result : "Error" | "Success" | "Warning" = "Error", error ?: any,
                            errorDesc = "Something went wrong, please contact HT.Support!"){

    if(error){
        logger.error("Error in operation: " + operation + " ", error)
    }
    return {
        operation: operation, 
        result: result,
        errorDescription: errorDesc,
        timestamp: format(Date.now(), "Y-MM-dd'T'HH:mm:ss'Z'"),
        error : error
    }
}

export function getURL(service : Endpoints, endpoint : string) : string {
    let url : string;
    try{
        url = configData.SERVER_URL + ":" +
                configData.SERVER_PORT + "/" + 
                configData.SERVER_PRODUCT +
                configData.SERVICES[service] + "/" +
                endpoint

        if(endpoint === ""){
            url = url.substring(0, url.length - 1)
        }
        
        if(isValidHttpUrl(url)){
            return url
        }
        else{
            return "invalidURL"
        }

    }
    catch(_){
        return "invalidURL"
    }
}

export function getAccessToken() : string{

    let local = localStorage.getItem("tokens")
    if(!local){
        logger.error("getAccessToken - Could not get tokens from storage!")
        return "Bearer NoStorageAccess"
    }
    let tokens =  JSON.parse(local)
    if(tokens != null && tokens.access_token != null){
        return 'Bearer ' + tokens.access_token
    }
    else{
        return "Bearer NoTokenFound"
    }
}

export function getRefreshToken() : string{

    let local = localStorage.getItem("tokens")
    if(!local){
        logger.error("getRefreshToken - Could not get tokens from storage!")
        return "Bearer NoStorageAccess"
    }
    let tokens =  JSON.parse(local)
    if(tokens != null && tokens.refresh_token != null){
        return 'Bearer ' + tokens.refresh_token
    }
    else{
        return "Bearer NoTokenFound"
    }
}

export function getDomainToken(targetDomainId : string) : string{
    return "Target " + targetDomainId
}

export function getActionToken(actionName : string) : string{
  return "Commit " + actionName
}

function isValidHttpUrl(string : string) {
  let url : URL;
  
  try {
    url = new URL(string);
  } catch (_) {
    return false;  
  }

  return url.protocol === "http:" || url.protocol === "https:";
}