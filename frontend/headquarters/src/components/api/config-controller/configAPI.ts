import * as types from "../responseTags";
import { setResponse } from "../../../state/actions/endpointActions";
import { AccessActions, ConfigActions, RootStore } from "../../../state/index"
import axios from "axios";
import { Dispatch } from "redux";
import { DeleteEventConfig } from "../../../types/config/config";
import { StoreAction } from "../../../types/storeTypes";

const ENDPOINT = types.Endpoints.config_controller

//==========================================================================
//Get Current Delete Event 
export const getDeleteConfig = (id : number) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "deleteEvent"),
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })
    .then(response => {
        dispatch(ConfigActions.setDeleteEvent(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_GET,
            success: false,
            response : err
        }))
    })
}


//==========================================================================
//Update the current delete event configuration
export const setDeleteConfig = (id : number, eventConfig : DeleteEventConfig) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "deleteEvent"),
        data: eventConfig,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("setDeleteConfig", "Error", err) : err.response.data
        }))
    })
}


//==========================================================================
//Update the current away/offline hours configuration
export const setOfflineHours = (id : number, awayThreshold: number, offlineThreshold : number) => async (dispatch : Dispatch, getState : () => RootStore) => {

    let postUrl = types.getURL(ENDPOINT, "offlineAwayHours") + "?awayHours=" + awayThreshold + "&offlineHours=" + offlineThreshold;

    await axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        url: postUrl,
        data: null,
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })  

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_POST,
            success: false,
            response : err.response == null ? types.getDefaultRsp("setOfflineHoursConfig", "Error", err) : err.response.data
        }))
    })
}

//==========================================================================
//Get the current away/offline hours configuration
export const getOfflineHours = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "offlineAwayHours"), 
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })
    .then(response => {
        dispatch(ConfigActions.setOfflineConfig(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_GET,
            success: false,
            response : err
        }))
    })
}

//==========================================================================
//Get the current away/offline hours configuration
export const fetchServices = (id : number = Date.now()) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'GET',
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "services"), 
        timeout: types.DEFAULT_TIMEOUT          // 10 second timeout
    })
    .then(response => {
        dispatch(ConfigActions.setServicesList(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_GET,
            success: false,
            response : err
        }))
    })
}

//==========================================================================
//Get items from the Appconfig Table
export const getConfiguration = (id : number, configKeys : string[], action :  (a : any) => StoreAction) => async (dispatch : Dispatch, getState : () => RootStore) => {
    let url = types.getURL(ENDPOINT, "misc")

    let prefix = "?"
    configKeys.forEach((key) => {
        url+=prefix + "appConfigKeys=" + key
        prefix="&"
    })

    await axios.get(url, 
                    { headers: {
        "Authorization": types.getAccessToken(),
        "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ALL_ACCESS_DOMAIN_ID),
    }})
    .then(response => {
        dispatch(action(response.data))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_GET,
            success: false,
            response : !err.response || !err.response.data ? types.getDefaultRsp("getAppConfig") : err.response.data
        }))
    })
}

//==========================================================================
//Update the current away/offline hours configuration
export const setConfiguration = (id : number, configs: any) => async (dispatch : Dispatch, getState : () => RootStore) => {

    await axios({
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        url: types.getURL(ENDPOINT, "misc"),
        data: configs,
        timeout: 10000          // 10 second timeout
    })  

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_POST,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONFIG,
            action: types.ACTION_POST,
            success: false,
            response : (!err.response || !err.response.data) ? types.getDefaultRsp("setAppConfig") : err.response.data
        }))
    })
}