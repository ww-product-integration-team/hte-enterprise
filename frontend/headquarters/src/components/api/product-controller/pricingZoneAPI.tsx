import { Dispatch } from "redux"
import { AccessActions, AssetActions, RootStore } from "../../../state"
import { setResponse } from "../../../state/actions/endpointActions"
import * as types from "../responseTags"
import axios from "axios"
import { PricingZone } from "../../../types/asset/AssetTypes"

const ENDPOINT = types.Endpoints.pricing_zone_controller
export const getPricingZones = (id = Date.now(), zoneId?: string) => async (dispatch: Dispatch, getState: () => RootStore) => {
    let url = types.getURL(ENDPOINT, "getPricingZones")
    if (zoneId) {
        url += "?zoneId=" + zoneId
    }
    
    await axios.get(url, {
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_PROFILE_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_PRICING_ZONE,
            action: types.ACTION_GET,
            success: true,
            response: response.data
        }))
        dispatch(AssetActions.setPricingZones(response.data))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_PRICING_ZONE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Get Pricing Zones", "Error", error) : error.response.data
        }))
    })
}

export const savePricingZone = (id = Date.now(), zone: PricingZone) => async (dispatch: Dispatch, getState: () => RootStore) => {
    await axios({
        method: "POST",
        url: types.getURL(ENDPOINT, "saveZone"),
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_PROFILE_DOMAIN_ID)
        },
        data: zone,
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_PRICING_ZONE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_PRICING_ZONE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Get Pricing Zones", "Error", error) : error.response.data
        }))
    })
}

export const deletePricingZone = (id = Date.now(), zoneId: string) => async (dispatch: Dispatch, getState: () => RootStore) => {
    let url = types.getURL(ENDPOINT, "deleteZone") + "?zoneId=" + zoneId
    
    await axios({
        method: "DELETE",
        url: url,
        headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(AccessActions.DEFAULT_PROFILE_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_PRICING_ZONE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_PRICING_ZONE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("Get Pricing Zones", "Error", error) : error.response.data
        }))
    })
}