import { Dispatch } from "redux"
import { AccessActions, ProductsActions, RootStore } from "../../../state"

import { setResponse } from "../../../state/actions/endpointActions"
import * as types from "../responseTags"
import axios from "axios"
import { prod } from "../../prodsTable/prodsTable"
import { ResponsiveFontSizesOptions } from "@material-ui/core/styles/responsiveFontSizes"
import { type } from "os"

const ENDPOINT = types.Endpoints.product_controller
export const getProds = (id: number) => async (dispatch: Dispatch, getState: () => RootStore) => {

    await axios.get(types.getURL(ENDPOINT, "getProds"), {
        headers: {
            'Authorization': types.getAccessToken(),
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
        },
        timeout: types.DEFAULT_TIMEOUT
    }).then(response => {
        dispatch(ProductsActions.setProducts(response.data))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_GET,
            success: false,
            response: error.response == null ? types.getDefaultRsp("GetProdss", "Error", error) : error.response.data
        }))
    })
}

export const postProd = (id : number, prod : prod) => async (dispatch: Dispatch, getState: () => RootStore) => {
    await axios({ 
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'TargetDomain': types.getDomainToken(AccessActions.DEFAULT_ADMIN_DOMAIN_ID),
            'Authorization': types.getAccessToken()
        },
        url: types.getURL(ENDPOINT, "postProd"),
        data: prod,
        timeout: types.DEFAULT_TIMEOUT // 10 seconds
    }).then((response) => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: true,
            response: response.data
        }))
    }).catch(error => {
        dispatch(setResponse({
            id: id,
            type: types.RSP_UPGRADE,
            action: types.ACTION_POST,
            success: false,
            response: error.response == null ? types.getDefaultRsp("PostProd", "Error", error) : error.response.data
        }))
    })

}
