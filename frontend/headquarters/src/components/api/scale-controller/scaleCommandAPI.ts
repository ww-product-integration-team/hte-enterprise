import * as types from "../responseTags";
import { RootStore } from "../../../state/index"
import { setResponse } from "../../../state/actions/endpointActions";
import axios from "axios";
import { Dispatch } from "redux";

const ENDPOINT = types.Endpoints.scale_controller

//==========================================================================
export const executeManualHeartBeat = (id : number, ipAddress : string, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    let command = "executemanualheartbeat"

    await axios.get(types.getURL(ENDPOINT, command) + "?ip="+ipAddress, 
            { headers: {
                "Authorization": types.getAccessToken(),
                "TargetDomain": types.getDomainToken(storeId),
                        },
            timeout: 35000,
        })
        
    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("executeManualHeartbeat", "Error", err) : err.response.data
        }))
    })
}

//==========================================================================
export const getScaleStatus = (id : number, ipAddress : string, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    let command = "getstatus"

    await axios.get(types.getURL(ENDPOINT, command) + "?ip="+ipAddress, 
                { headers: {
        "Authorization": types.getAccessToken(),
        "TargetDomain": types.getDomainToken(storeId),
                },
        timeout: types.DEFAULT_TIMEOUT,
    })

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("getScaleStatus", "Error", err) : err.response.data
        }))
    })
}

//==========================================================================
export const rebootDevice = (id : number, ipAddress : string, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    let command = "rebootdevice"
    await axios.get(types.getURL(ENDPOINT, command) + "?ip="+ipAddress, 
            { headers: {
                "Authorization": types.getAccessToken(),
                "TargetDomain": types.getDomainToken(storeId),
                        },
            timeout: types.DEFAULT_TIMEOUT})

    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("rebootDevice", "Error", err) : err.response.data
        }))
    })
}

//==========================================================================
export const deleteDeviceData = (id : number, ipAddress : string, storeId : string) => async (dispatch : Dispatch, getState : () => RootStore) => {
    let command = "deletedevicedata"

    await axios.get(types.getURL(ENDPOINT, command) + "?ip="+ipAddress, 
        { headers: {
            "Authorization": types.getAccessToken(),
            "TargetDomain": types.getDomainToken(storeId),
                    },
        timeout: 60000})



    .then(response => {
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: true,
            response : response.data
        }))
    })

    .catch(err => {                 // This catches any error and logs the message
        dispatch(setResponse(
            {
            id: id,
            type: types.RSP_CONTROLLER,
            cmd: command,
            action: types.ACTION_GET,
            success: false,
            response : err.response == null ? types.getDefaultRsp("deleteDeviceData", "Error", err) : err.response.data
        }))
    })
}