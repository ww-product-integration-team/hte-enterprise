import { useEffect, useState} from 'react';
import { useForm} from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import {AddCircle, Edit, Delete, Block} from '@mui/icons-material';
import { Form } from '@themesberg/react-bootstrap';
import { useDispatch} from "react-redux";

import { ProfileAPI } from '../api';
import { FormSelect, FormInput, HiddenDomainEntry } from './FormHelper';
import './formStyles.css';
import { AccessActions, store, useAppSelector } from '../../state';
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { Profile } from '../../types/asset/ProfileTypes';
import { ResponseType } from '../../types/storeTypes';

// Profile Form

// POST Request
interface PostProfileProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    setProfile?: React.Dispatch<React.SetStateAction<Profile | undefined>>
}
function PostProfile(props : PostProfileProps) {
    interface PostProfileForm{
        profileName: string
        UseAccountDomain : boolean
        profileDescription: string
        selectDomain: string
        ManualEntry : boolean
        zoneId: string
    }

    const { register, handleSubmit, reset,  setValue, formState: {errors} } = useForm<PostProfileForm>();
    const { title, openPopup, setOpenPopup } = props;
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();
    const profiles = useAppSelector((state) => state.profile.profiles);
    const pricingZones = useAppSelector(state=>state.asset.pricingZones)
    const [zones, setZones] = useState(pricingZones)
    const reduxZonePricing = useAppSelector(state=>state.config.zonePricing.zonePricing)
    const [zonePricing, setZonePricing] = useState(reduxZonePricing === "true")

    useEffect(() => {
        setZonePricing(reduxZonePricing === "true")
    }, [reduxZonePricing])
    useEffect(() => {
        setZones(pricingZones)
    }, [pricingZones])

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        profileName: "", 
        UseAccountDomain : false,
        profileDescription: "",
        selectDomain: "",
        ManualEntry: false
    };

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(false);
        reset(defaultValues);
    }

    const handleAdd = handleSubmit(async (data : PostProfileForm) => {    // When the Submit button is pressed
        logger.info("(ProfileForm) POST request data (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])

        //Get Domain ID for new ProfileID
        let requiresDomain = ""
        try{
            if(data.UseAccountDomain){
                requiresDomain = store.getState().access.account.domain.domainId
            }
            else{
                requiresDomain = data.selectDomain ?? AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID
            }
        }
        catch(e){
            requiresDomain = AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID
        }
        

        // Reset duplicateName to false onClick. 
        let duplicateName = false;

        // Check if name exists on database already
        for (let i=0; i < profiles.length; i++) {
            let tmp = data.profileName.replace(/[^a-zA-Z0-9]/g, "") // New Data
            let tmp2 = profiles[i].name.replace(/[^a-zA-Z0-9]/g, "")   // Database Data

            if (tmp === tmp2) {
                duplicateName = true
                logger.info("(ProfileForm) working! new data: ", data.profileName)
                logger.info("(ProfileForm) working! database: ", profiles[i].name)
            }
        }

        logger.info("(ProfileForm) duplicateName: ", duplicateName)
        const profileData : Profile = {
            profileId: "1",                 // Dummy ID, required for some reason though
            name: data.profileName,
            description: data.profileDescription,
            requiresDomainId: requiresDomain,
            lastUpdate: new Date().toISOString(),
            deleteSyncConfig: null,
            zoneId: data.zoneId
        }

        function onSuccess(response : ResponseType){
            logger.info("(ProfileForm) Backend Response (POST Profile): ", response);
            setOpenPopup(false);
            reset(defaultValues);
            dispatch(ProfileAPI.fetchProfiles())

            //Checks that errorDescription is a valid UUID string
            if(props.setProfile && response.response && response.response.errorDescription &&
                response.response.errorDescription?.match(/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[1-5][0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/)){

                profileData.profileId = response.response.errorDescription
                props.setProfile(profileData)
            }
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
            setShowAlert([true, "Error trying to add profile. Please try again later."])
        }

        if (!duplicateName) {
            
            const newID = Date.now()

            if(addCallback(callbacks, newID, "Post Profile", onSuccess, onFail)){
                dispatch(ProfileAPI.postProfile(newID, profileData, profileData.requiresDomainId))
            }
        }

        else {
            setShowAlert([true, "This name exists already."])
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormInput
                        label="Profile Name: "
                        id="profileName"
                        name="profileName"
                        type="text"
                        placeholder="Enter a profile name"
                        register={register}
                        validation={{ 
                            required: "Please enter a profile name", 
                            maxLength: { value: 45, message: "You exceeded the max profile name length"}
                        }}
                        error={errors.profileName}
                    />

                    <FormInput
                        label="Description: "
                        id="profileDescription"
                        name="profileDescription"
                        type="text"
                        placeholder="Enter a description"
                        register={register}
                        validation={{ 
                            required: "Please enter a brief description", 
                            maxLength: { value: 100, message: "You exceeded the max description length"}
                        }}
                        error={errors.profileDescription}
                    />

                    {zonePricing ?
                        <FormSelect 
                            label="Select a Zone"
                            id="zoneId"
                            name="zoneId"
                            placeholder="--Select a Zone--"
                            dataset={zones}
                            register={register}
                            objId="id"
                            objName="name"
                            validation={{}}
                            error={errors.zoneId}
                            onlyValue={undefined}
                            disabled={false}
                        />
                    :
                        null
                    }

                    <HiddenDomainEntry 

                        id="manualSelectDomain"
                        name="ManualDomainEntry"
                        setValue={setValue}
                        register={register}
                        validation={{
                            required: "Please enter a domain"
                        }}
                        error={errors.ManualEntry} placeholder={undefined} disableOther={undefined}
                    />

                    {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Add Profile </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}


interface EditProfileProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    profile: Profile
    onlyValue: boolean
    selectValue: string
}
// Edit Profile
function EditProfile(props : EditProfileProps) {
    interface EditProfileForm{
        profileName: string
        UseAccountDomain : boolean
        profileDescription: string
        selectDomain: string
        ManualEntry : boolean
        editProfile: string
        zoneId: string
    }

    const { register, handleSubmit, reset, setValue, formState: {errors} } = useForm<EditProfileForm>();
    const { title, openPopup, setOpenPopup, profile, onlyValue} = props;
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const dispatch = useDispatch();
    const profiles = useAppSelector((state) => state.profile.profiles);
    const {callbacks, addCallback} = useRspHandler();

    const pricingZones = useAppSelector(state=>state.asset.pricingZones)
    const [zones, setZones] = useState(pricingZones)
    const reduxZonePricing = useAppSelector(state=>state.config.zonePricing.zonePricing)
    const [zonePricing, setZonePricing] = useState(reduxZonePricing === "true")
    
    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        profileName: "", 
        UseAccountDomain : false,
        profileDescription: "",
        selectDomain: "",
        ManualEntry: false,
        editProfile: ""
    };

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(false);
        reset(defaultValues);
    }

    useEffect(() => {
        setZonePricing(reduxZonePricing === "true")
    }, [reduxZonePricing])
    useEffect(() => {
        let sorted = Object.fromEntries(Object.entries(pricingZones).sort(([,a],[,b]) => a.name.localeCompare(b.name, 'en', {numeric: true})))
        setZones(sorted)
    }, [pricingZones])

// =======================================================

    const handleAdd = handleSubmit(async (data : EditProfileForm) => {    // When the Submit button is pressed
        logger.info("(ProfileForm) POST request data (Edit Profile): ", data)
        setShowAlert([false, "Something went wrong!"])
        
        let duplicateName = false;                                  // Reset duplicateName to false onClick
        let updateSameName = false;                                 // If user is trying to edit a profile using the same name, different description
        let tmp = data.profileName.replace(/[^a-zA-Z0-9]/g, "")     // New Data w/o special characters

        //Get Domain ID for new ProfileID
        let requiresDomain = ""
        try{
            if(data.UseAccountDomain){
                requiresDomain = store.getState().access.account.domain.domainId
            }
            else{
                requiresDomain = data.selectDomain ?? AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID
            }
        }
        catch(e){
            requiresDomain = AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID
        }

        // This section is to ensure user can update a profile's description without updating the profile's name
        for (let i=0; i < profiles.length; i++) {
            let tmp2 = profiles[i].name.replace(/[^a-zA-Z0-9]/g, "")   // Database Data w/o special characters

            // if the IDs match, and if the names are the same, its an okay duplicate name
            if (profile.profileId === profiles[i].profileId) {  
                if (tmp === tmp2)   updateSameName = true;
            }

            // if the IDs don't match, and if the names are the same, it's a bad duplicate name
            else {  
                if (tmp === tmp2)   duplicateName = true;
            }
        }

        logger.info("(ProfileForm) updateSameName: ", updateSameName)
        logger.info("(ProfileForm) duplicateName: ", duplicateName)

        function onSuccess(response : ResponseType){
            logger.info("(ProfileForm) Backend Response (EDIT Profile): ", response);
            setOpenPopup(false);
            reset(defaultValues);

            dispatch(ProfileAPI.fetchProfiles())
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
            setShowAlert([true, "Error trying to edit profile. Please try again later."])
        }

        // No duplicate name, or updating same profile with different description
        if (!duplicateName || updateSameName) {

            const profileData : Partial<Profile> = {
                profileId: profile.profileId,                 
                name: data.profileName,
                description: data.profileDescription,
                requiresDomainId: requiresDomain,
                zoneId: zonePricing ? data.zoneId : undefined
            }

            const newID = Date.now()
            if(addCallback(callbacks, newID, "Edit Profile", onSuccess, onFail)){
                dispatch(ProfileAPI.postProfile(newID, profileData, requiresDomain))
            }
        }

        // Duplicate name from a different profile (error)
        else {
            setShowAlert([true, "This name exists already."])
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormSelect
                        label="Select a Profile to update: "
                        id="editProfile"
                        name="editProfile"
                        customdefault={profile.profileId}
                        placeholder={props.selectValue ? props.selectValue : "--Select a profile--"}
                        dataset={profiles}
                        register={register}
                        objId="profileId"
                        objName="name"
                        validation={{ required: "Please select a profile to edit" }}
                        error={errors.editProfile}
                        onlyValue={onlyValue ? onlyValue : false} disabled={true}
                    />
                    
                    <FormInput
                        label="New Profile Name: "
                        id="profileName"
                        name="profileName"
                        type="text"
                        defaultValue={profile.name}
                        placeholder="Enter a profile name"
                        register={register}
                        validation={{ 
                            required: "Please enter a profile name", 
                            maxLength: { value: 45, message: "You exceeded the max profile name length"}
                        }}
                        error={errors.profileName}
                    />

                    <FormInput
                        label="New Description: "
                        id="profileDescription"
                        name="profileDescription"
                        type="text"
                        defaultValue={profile.description}
                        placeholder="Enter a description"
                        register={register}
                        validation={{ 
                            required: "Please enter a brief description", 
                            maxLength: { value: 100, message: "You exceeded the max description length"}
                        }}
                        error={errors.profileDescription}
                    />

                    {zonePricing && 
                        <>
                            <Form.Label>Pricing Zone</Form.Label>
                            <Form.Select required defaultValue={zones[profile.zoneId] ? zones[profile.zoneId].id : undefined}
                                id="zoneId" {...register("zoneId")}>
                                <>
                                    <option value="">--Select a Zone--</option>
                                    {Object.keys(zones).map(zoneId => {
                                        return (
                                            <option key={zoneId} value={zoneId}>
                                                {zones[zoneId].name}
                                            </option>
                                        )
                                    })}
                                </>
                            </Form.Select>
                        </>
                    }

                    <HiddenDomainEntry 

                        id="manualSelectDomain"
                        name="ManualDomainEntry"
                        setValue={setValue}
                        register={register}
                        validation={{
                            required: "Please enter a domain",
                            maxLength: { value: 45, message: "You exceeded the max name length" }
                        }}
                        error={errors.ManualEntry} placeholder={undefined} disableOther={undefined}                    />

                    {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <Edit />
                            <span> Save Profile </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    );
}


interface DeleteProfileProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    profiles: Profile[]
    profile: Profile
    onlyValue: boolean
    selectValue: string
}
// DELETE Request
function DeleteProfile(props : DeleteProfileProps) {

    interface DeleteProfileProps{
        deleteProfile: string
    }

    const { register, handleSubmit, reset, formState: {errors} } = useForm<DeleteProfileProps>();
    const { title, openPopup, setOpenPopup, profiles, onlyValue, profile} = props;
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();


    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        deleteProfile: ""
    };

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(false);
        reset(defaultValues);
    }



    // =======================================================

    const handleDelete = handleSubmit(async (data : DeleteProfileProps) => {    // When the Submit button is pressed
        logger.info("(ProfileForm) DELETE request data: (Delete Profile) ", data)
        setShowAlert([false, "Something went wrong!"]);

        let profileToDelete = profiles.find(profile => 
            profile.profileId === data.deleteProfile
        )
        if(profileToDelete == null){
            setShowAlert([true, "Error: could not find desired profile!"]);
            return
        }

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            logger.info("(ProfileForm) Backend Response (DELETE Profile): ", response);
            setOpenPopup(false);
            reset(defaultValues);

            dispatch(ProfileAPI.fetchProfiles());
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Error trying to delete profile. Please try again later."]);
        }
    
        if(addCallback(callbacks, newID, "Delete Profile", onSuccess, onFail)){
            dispatch(ProfileAPI.deleteProfile(newID, data.deleteProfile, '?id='+ data.deleteProfile, profileToDelete.requiresDomainId))
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleDelete}>
                    <div className="formSpacing">
                        <Form.Label id="confirmDelete" style={{color: "black"}}>Are you sure you want to delete this profile?</Form.Label>
                        <FormSelect
                            id="deleteProfile"
                            name="deleteProfile"
                            placeholder={props.selectValue ? props.selectValue : "--Select a profile--"}
                            customdefault={profile.profileId}
                            dataset={profiles}
                            register={register}
                            objId="profileId"
                            objName="name"
                            validation={{ required: "Please select a profile to delete" }}
                            error={errors.deleteProfile}
                            onlyValue={onlyValue ? onlyValue : false} label={undefined} disabled={undefined}
                        /> 

                        <br />

                        <Alert className="m-1" variant="outlined" severity="error">
                            <p className="fw-bold"> Warning: Deleting this profile will delete all files in this profile</p>
                        </Alert>

                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleDelete} type="submit">
                                <Delete />
                                <span> Delete Profile </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

export { PostProfile, EditProfile, DeleteProfile };