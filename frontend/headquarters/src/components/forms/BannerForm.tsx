import { useState} from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import { Form } from '@themesberg/react-bootstrap';
import { FormInput} from './FormHelper';
import Alert from '@mui/material/Alert';
import { useDispatch} from "react-redux";
import { AssetAPI, BannerAPI } from "../api/index"

import './formStyles.css';
import logger from '../utils/logger';
// BANNER, Region, Store, Department, Scale
import {useRspHandler} from '../utils/ResponseProvider';
import { AddCircle, Block, Delete, Edit } from '@mui/icons-material';
import { Node, lazyNode } from '../../types/asset/TreeTypes';
import { AssetTypes } from '../../types/asset/AssetTypes';
import { ResponseType } from '../../types/storeTypes';
import { useAppSelector } from '../../state';

// POST Request
interface PostBannerProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
}
interface BannerForm{
    bannerName: string
}
const defaultValues = {
    bannerName: ""
};
function PostBanner(props : PostBannerProps) {

    const { register, handleSubmit, reset,  formState: {errors} } = useForm<BannerForm>();
    const { title, openPopup, setOpenPopup } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const lazyExpandedKeys = useAppSelector((state)=> state.asset.lazyExpanded)

    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
        reset(defaultValues);
    }

    const handleAdd = handleSubmit(async (data : BannerForm) => {    // When the Submit button is pressed
        logger.info("(PostBanner) POST request (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            setOpenPopup(null);
            reset(defaultValues);
            dispatch(BannerAPI.fetchBanners())
            dispatch(AssetAPI.refreshLazyTree(Date.now(), lazyExpandedKeys))
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
            setShowAlert([true, "Could not add Banner!"])
        }

        if(addCallback(callbacks, newID, "Post Banner", onSuccess, onFail)){
            dispatch(BannerAPI.postBanners(newID, {"bannerName" : data.bannerName}))
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormInput
                        label="Banner Name: "
                        id="createBanner"
                        name="bannerName"
                        type="text"
                        placeholder="Enter a banner name"
                        register={register}
                        validation={{ 
                            required: "Please enter a banner name", 
                            maxLength: { value: 45, message: "You exceeded the max banner name length"}
                        }}
                        error={errors.bannerName}
                    />

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Add Banner </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

// Edit Banner
// TODO: Grab the NodeID and put it into the URL to choose which node is being edited
interface PutBannerProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement: lazyNode | Node
}
function PutBanner(props : PutBannerProps) {
    const { register, handleSubmit, reset, formState: {errors} } = useForm<BannerForm>();
    const { title, openPopup, setOpenPopup, treeElement } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const lazyExpandedKeys = useAppSelector((state)=> state.asset.lazyExpanded)

    const handleCloseDialog = (event : any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
        reset(defaultValues);
    }

    const handleAdd = async (data : BannerForm) => {    // When the Submit button is pressed
        logger.info("(PutBanner) PUT request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response){
            setOpenPopup(null);
            reset(defaultValues);
            dispatch(BannerAPI.fetchBanners())
        }
        function onFail(response){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not edit banner!"])
        }
        if(addCallback(callbacks, newID, "Edit Banner", onSuccess, onFail, {successMsg: "Banner edited successfully!", failMsg: "Could not edit banner!", requestMsg: "Editing banner..."})){
            dispatch(BannerAPI.postBanners(newID, 
                {"bannerId" : treeElement.id,
                "bannerName" : data.bannerName}))
        }
    }
  
    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}: <b style={{color : "#ba4735"}}>{treeElement.name}</b>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleAdd)}>

                    <FormInput
                        label="Banner Name:" 
                        id="editBanner"
                        name="bannerName"
                        type="text"
                        placeholder={"Enter a banner name"}
                        register={register}
                        validation={{ 
                            required: "Please enter a banner name", 
                            maxLength: { value: 45, message: "You exceeded the max banner name length"}
                        }}
                        error={errors.bannerName}
                        defaultValue={treeElement.name?treeElement.name:""}
                    />

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}


                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleAdd)} type="submit">
                            <Edit />
                            <span> Update Banner </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

// DELETE Request
// TODO: Grab the NodeID and put it into the URL to choose which node is being deleted
interface DeleteBannerProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement: lazyNode | Node
}
function DeleteBanner(props : DeleteBannerProps) {
    const { handleSubmit} = useForm();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();
    const lazyExpandedKeys = useAppSelector((state)=> state.asset.lazyExpanded)
    const dispatch = useDispatch();

    const handleCloseDialog = (event : any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
    }

    const handleDelete = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(DeleteBanner) DELETE request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            setOpenPopup(null);
            dispatch(AssetAPI.refreshLazyTree(Date.now(), lazyExpandedKeys))

        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
            setShowAlert([true, "Could not delete banner!"])
        }
        if(addCallback(callbacks, newID, "Delete Banner", onSuccess, onFail)){
            dispatch(BannerAPI.deleteBanners(newID, treeElement.id))
        }
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>
                    <Form.Label id="confirmDelete">Are you sure you want to remove banner: <b style={{color : "#ba4735"}}>{treeElement.name}</b>?</Form.Label>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleDelete)} type="submit">
                            <Delete />
                            <span> Delete Banner </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

export { PostBanner, PutBanner, DeleteBanner };