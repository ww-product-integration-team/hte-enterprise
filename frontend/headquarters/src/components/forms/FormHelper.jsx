/*  FormHelper.jsx
    This file contains several components that can incorporate the Bootstrap UI, React-Hook-Form, and states
*/

import { Form, FormCheck} from '@themesberg/react-bootstrap';
import {IconButton, Tooltip, Typography } from '@material-ui/core';
import {useState } from "react";
import {Help, VisibilityOff} from '@material-ui/icons';
import { AccessActions, store } from '../../state';
import logger from '../utils/logger';
import { DEFAULT_UNASSIGNED_DOMAIN_ID } from '../../state/actions/accessActions';
import VisibilityIcon from '@material-ui/icons/Visibility';

/*
    -------------- What this template is expecting: --------------
    <FormInput
        label="First Name: "
        id="firstName"
        name="firstName"
        type="text"
        placeholder="Enter your name"
        register={register}
        validation={{ 
            required: "Please enter your name", 
            maxLength: { value: 50, message: "You exceeded the max name length"}
        }}
        error={errors.profileDescription}
    />
*/
export const FormInput = ({register, error, label, name, validation, ...inputProps}) => {
    const reusableRegister = register;
    const [viewingPassword, setViewingPassword] = useState(false)
    let setFormType = inputProps["setFormType"]

    const changeType = () => {
        return (
            <>
                {setFormType ?
                    viewingPassword ?
                        <IconButton disableRipple onClick={()=>{setViewingPassword(false); setFormType("password")}} title="Click to hide the password">
                            <VisibilityOff />
                        </IconButton>
                    :
                        <IconButton disableRipple onClick={()=>{setViewingPassword(true); setFormType("text")}} title="Click to see the password">
                            <VisibilityIcon />
                        </IconButton>
                : null}
            </>
        )
    }

    return (
        <>
            {label === "" ? null : 
                <>
                    <Form.Label>
                        {label}
                    </Form.Label>
                    {changeType()}
                </>
            }
            <Form.Control 
                // required 
                {...reusableRegister(name, validation)}
                {...inputProps}
            ></Form.Control>

            {error && <p className="formError">{error.message}</p>}
        </>
    );
}

/*
    -------------- What this template is expecting: --------------
    <FormSelect
        customdefault={currentFile.fileCategory} 
        label="File Category: "
        id="fileCategories"
        name="fileCategories"
        placeholder="--Select a Category--"
        dataset={categories}
        register={register}
        objId="fileId"
        objName="name"
        validation={{required: "Please select a category"}}
        error={errors.fileCategories}
    />
*/
export const FormSelect = ({register, error, label, name, validation, placeholder, objId, objName, disabled, onlyValue, ...selectProps}) => {
    const reusableRegister = register;
    const { dataset } = selectProps;
    const loneValue = onlyValue?onlyValue:false;
    let defaultValueIndex = "";                      // Index of the category
    let defaultValue = "";                      // Original default value
    defaultValue = selectProps.customdefault;   // Update default value if provided
    // Get the index of the fileCategory
    if (defaultValue !== null || defaultValue !== DEFAULT_UNASSIGNED_DOMAIN_ID) {
        Object.keys(dataset).forEach((index) => {
            if (dataset[index].name === defaultValue) return defaultValueIndex = dataset[index][objId]
            else if(dataset[index][objId] === defaultValue) return defaultValueIndex = index
            else if(defaultValue === DEFAULT_UNASSIGNED_DOMAIN_ID) return defaultValue = false
        })
    }
    let sortedData
    if (dataset instanceof Array) {
        sortedData = dataset.sort((a,b) => a[objName].localeCompare(b[objName], 'en', {numeric: true}))
    } else {
        sortedData = Object.fromEntries(Object.entries(dataset).sort(([,a],[,b]) => a[objName].localeCompare(b[objName], 'en', {numeric: true})))
    }
    return (
        <>
            <Form.Label>{label}</Form.Label>
            <Form.Select 
                // required
                disabled={disabled}
                {... disabled ? reusableRegister(name, {required: null}) : reusableRegister(name, validation)}
                {...selectProps}   
                defaultValue={defaultValue ? sortedData[defaultValueIndex][objId] : ""} 
            >
                {!loneValue && 
                    <option value="" disabled>{placeholder}</option>
                }
                
                {Object.keys(sortedData).map(key => (
                    <option key={sortedData[key][objId]} value={sortedData[key][objId]}>
                        {sortedData[key][objName]}   {/* This populates an 'option' for every profile.profileName in the call */}
                    </option>
                )).sort()}
            </Form.Select>

            {error && <p className="formError">{error.message}</p>}
        </>
    );
}

/*
    -------------- What this template is expecting: --------------
    <HiddenManualEntry 

        labelCheck="Manually add a scale"       // Label next to checkbox
        label="Scale IP Address: "              // Label for input box
        id="manualScaleInput"                   // id
        name="ManualEntry"                      // React-Hook-Form register name
        type="text"                             // input type
        placeholder="Enter a Scale IP Address"  // placeholder
        register={register}                     // pass the actual register to use
        validation={{                           // Error validation
            required: "Please enter a scale IP address", 
            maxLength: { value: 15, message: "You exceeded the max scale IP length"}
        }}
        error={errors.ManualEntry}              // Display the errors
    />
*/
export const HiddenManualEntry = ({register, error, labelCheck, label, name, validation, placeholder, disableOther, ...inputProps}) => {
    const reusableRegister = register;
    const [showManualAdd, setShowManualAdd] = useState(false)
    const onClick = (e) => {
        if(typeof disableOther == "function"){
            disableOther(e.target.checked)
        }
        
        setShowManualAdd(e.target.checked)
    }

    return(
        <div>
            <FormCheck type="checkbox" className="d-flex" style={{padding: inputProps.padding ? inputProps.padding : "0.5rem"}}>
                <FormCheck.Label className="customCheckbox"> 
                    <FormCheck.Input required id="asvb" className="me-2" onClick ={onClick} {...reusableRegister("ManualAdd")}/>
                    {labelCheck} 
                </FormCheck.Label>
            </FormCheck>
            {document.getElementById("asvb") == null ? null : 
                document.getElementById("asvb").checked === showManualAdd ? null : setShowManualAdd(document.getElementById("asvb").checked)}
            { showManualAdd ? 
                <FormInput 
                    label={label} 
                    name={name}
                    placeholder={placeholder}
                    register={register} 
                    validation={validation}
                    error={error}
                    {...inputProps}
                />
            : null }
        </div>
    )
}

export const HiddenDomainEntry = ({register, error, validation, placeholder, setValue, disableOther, ...inputProps}) => {
    const reusableRegister = register;
    const [showManualAdd, setShowManualAdd] = useState(true)
    const [domainType, setDomainType] = useState(null)
    const [entityList, setEntityList] = useState([])
    const [objTypes, setObjTypes] = useState(["objId", "objName"])
    const [selectDisabled, setSelectDisabled] = useState(true)

    const onClick = (e) => {
        if(typeof disableOther == "function"){
            disableOther(e.target.checked)
        }
        
        setShowManualAdd(e.target.checked)
    }


    let domainTypes = [{id:'ADMIN', name:'ADMIN'}, {id:'BANNER', name:'BANNER'}, {id:'REGION', name:'REGION'}, {id:'STORE', name:'STORE'}]

    const handleDomainTypeSelect = (e) => {
        const domainType = domainTypes[e.target.options.selectedIndex-1];

        setDomainType(domainType)



        switch(domainType.name){
            case "ADMIN":
                setSelectDisabled(true)
                setObjTypes(["adminId", "adminName"])
                setEntityList([{'adminId': AccessActions.DEFAULT_ADMIN_DOMAIN_ID, 'adminName': 'ADMIN'}])
                setValue('selectDomain', AccessActions.DEFAULT_ADMIN_DOMAIN_ID)
                new logger("(FormHelper) " + document.getElementById('selectDomain').selectedIndex + " " + document.getElementById('selectDomain').value + " " + document.getElementById('selectDomain').selected)
                break;
            case "BANNER":
                setObjTypes(["bannerId", "bannerName"])
                setEntityList(store.getState().asset.banners)
                setValue('selectDomain', store.getState().asset.banners[0].bannerId)
                setSelectDisabled(false)
                break;
            case "REGION":
                setObjTypes(["regionId", "regionName"])
                setEntityList(store.getState().asset.regions)
                setValue('selectDomain', store.getState().asset.regions[0].regionId)
                setSelectDisabled(false)
                break;
            case "STORE":
                setObjTypes(["storeId", "storeName"])
                setEntityList(store.getState().asset.stores)
                setValue('selectDomain', Object.values(store.getState().asset.stores)[0].storeId)
                setSelectDisabled(false)
                break;
            default:
                break
        }

    };

    const disableKey = (e) => {
        const ev = e ? e : window.event;
     
        if (ev) {
            if   (ev.preventDefault)     ev.preventDefault();
            else                         ev.returnValue = false;         
        } 
     }


    return(
        <div>
            <FormCheck type="checkbox" className="d-flex p-2">
                <FormCheck.Label className="customCheckbox"> 
                    <FormCheck.Input required id="useUserDomain" defaultChecked={true} className="me-2" onClick ={onClick} {...reusableRegister("UseAccountDomain")}/>
                    Use account domain
                    <Tooltip className="info" title={<Typography fontSize={12}> {"Assigns the entity to the current user's domain. Meaning the user and whoever has a higher domain permission will be allowed to modify this entity"} </Typography>}>
                        <IconButton aria-label="sync end date" style={{padding:0}}>
                            <Help />
                        </IconButton>
                    </Tooltip>
                </FormCheck.Label>

            </FormCheck>
            {document.getElementById("useUserDomain") == null ? null : 
                document.getElementById("useUserDomain").checked === showManualAdd ? null : setShowManualAdd(document.getElementById("useUserDomain").checked)}
            { !showManualAdd ?
                <div>
                    <Form.Label style={{marginTop:8}}>Please select a domain type</Form.Label>  
                    <Form.Select id="profileSelector" style={{backgroundColor: "transparent"} }
                        onChange={(e) => handleDomainTypeSelect(e)  }
                        onKeyDown={(e) => disableKey(e)}
                        onClick={(e)=> e.stopPropagation()}
                        value={domainType == null ? "" : domainType.name ?? ""}
                    >
                        <option value="" disabled>--Select a Domain Type--</option> : null

                        {domainTypes.map(type => (
                            <option key={type.id} value={type.id}>
                                {type.name}
                            </option>
                        ))}
                    </Form.Select>

                    <h1> </h1>
                    <FormSelect
                        required
                        label="Select domain: "
                        id="selectDomain"
                        name="selectDomain"
                        type="text"
                        placeholder="--Select a Domain--"
                        dataset={entityList}
                        register={register}
                        objId={objTypes[0]}
                        objName={objTypes[1]}
                        disabled={selectDisabled}
                        validation={{required: "Please select a specific domain"}}
                        error={error}
                    />
                </div>
            : null }
        </div>
    )
}
