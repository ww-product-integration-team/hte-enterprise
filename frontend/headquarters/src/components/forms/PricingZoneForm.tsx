import { Dialog, DialogContent, DialogTitle, Tooltip, Typography } from "@material-ui/core"
import React, { useEffect, useState } from "react"
import { Dropdown, DropdownButton, Form } from "react-bootstrap"
import { useForm } from "react-hook-form"
import { ResponseType } from "../../types/storeTypes"
import logger from '../utils/logger';
import { useRspHandler } from "../utils/ResponseProvider"
import { useDispatch } from "react-redux"
import { PricingZoneAPI } from "../api"
import { FormInput, FormSelect } from "./FormHelper"
import PaidIcon from '@mui/icons-material/Paid'
import { ButtonGroup } from "@themesberg/react-bootstrap"
import { Block, Delete } from "@material-ui/icons"
import { useAppSelector } from "../../state"
import { PricingZone } from "../../types/asset/AssetTypes"

interface ActionsProps {
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    setPopupType: React.Dispatch<React.SetStateAction<"create"|"edit"|"delete">>
}
export default function PricingZoneActions(props: ActionsProps) {
    const {setOpenPopup, setPopupType} = props

    return (
        <Tooltip placement="bottom" title={<Typography variant="body1" component="div">Use Pricing Zones to be assigned to Profiles and allow for different pricings of the same product</Typography>}>
            <Dropdown as={ButtonGroup} className="assetBar" style={{marginRight: "0.5rem"}}>
                <DropdownButton variant="outline-primary" id="actionBar2" size="sm" title={
                    <>
                        <PaidIcon/>
                        Pricing Zone Actions
                    </>
                }>
                    <Dropdown.Item id="Dropdown" onClick={()=>{
                        setPopupType("create")
                        setOpenPopup(true)
                    }}>
                        Create Pricing Zone
                    </Dropdown.Item>
                    <Dropdown.Item id="Dropdown" onClick={()=>{
                        setPopupType("edit")
                        setOpenPopup(true)
                    }}>
                        Edit Pricing Zone
                    </Dropdown.Item>
                    <Dropdown.Item id="Dropdown" onClick={()=>{
                        setPopupType("delete")
                        setOpenPopup(true)
                    }}>
                        Delete Pricing Zone
                    </Dropdown.Item>
                </DropdownButton>
            </Dropdown>
        </Tooltip>
    )
}

interface PricingZoneForm {
    id: string
    name: string
}
interface PostPricingZoneProps {
    title: string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
}
export function PostPricingZone(props: PostPricingZoneProps) {
    const {title, openPopup, setOpenPopup} = props
    const {register, handleSubmit, reset, formState: {errors}} = useForm<PricingZoneForm>()
    const {addCallback, callbacks} = useRspHandler()
    const dispatch = useDispatch()
    
    const handleCloseDialog = () => {
        setOpenPopup(false)
        reset({id: "", name: ""})
    }

    const handleAdd = handleSubmit(async data => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully posted pricing zone")
            handleCloseDialog()
            dispatch(PricingZoneAPI.getPricingZones())
        }
        function onFail(response: ResponseType) {
            logger.info("Failed to post pricing zone")
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Save Pricing Zone", onSuccess, onFail)) {
            dispatch(PricingZoneAPI.savePricingZone(newID, data))
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormInput
                        label="Zone Name"
                        name="name"
                        type="text"
                        placeholder="Enter a zone name"
                        register={register}
                        validation={{required: "Please enter a zone name",
                            maxLength: {value: 255, message: "The zone name is too long"}}}
                        error={errors.name}
                    />

                    <div className="formButtons">
                        <button className="formButton1" type="submit" onClick={handleAdd}>
                            <PaidIcon/>
                            <span> Add Pricing Zone</span>
                        </button>
                        <button className="formButton1" type="button" onClick={handleCloseDialog} value="Cancel">
                            <Block/>
                            <span> Cancel</span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

interface EditPricingZoneProps {
    title: string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
}
export function EditPricingZone(props: EditPricingZoneProps) {
    const defualtValues: PricingZone = {
        id: "",
        name: ""
    }

    const {title, openPopup, setOpenPopup} = props
    const {register, getValues, setValue, handleSubmit, reset, formState: {isDirty, errors}} = useForm<PricingZoneForm>({defaultValues: defualtValues})
    const {addCallback, callbacks} = useRspHandler()
    const dispatch = useDispatch()
    const reduxPricingZones = useAppSelector(state=>state.asset.pricingZones)
    const [zones, setZones] = useState(reduxPricingZones)

    useEffect(() => {
        setZones(reduxPricingZones)
    }, [reduxPricingZones])
    useEffect(() => {
        let value = getValues("id")
        let zoneName = zones[value] != null ? zones[value].name : ""
        setValue("name", zoneName, {shouldValidate: true, shouldDirty: true})
    }, [isDirty])
    
    const handleCloseDialog = () => {
        setOpenPopup(false)
        reset(defualtValues)
    }

    const handleEdit = handleSubmit(async data => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully edited pricing zone")
            handleCloseDialog()
            dispatch(PricingZoneAPI.getPricingZones())
        }
        function onFail(response: ResponseType) {
            logger.info("Failed to edit pricing zone")
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Save Pricing Zone", onSuccess, onFail)) {
            dispatch(PricingZoneAPI.savePricingZone(newID, data))
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleEdit}>
                    <FormSelect
                        label="Zone to Edit"
                        name="id"
                        placeholder="--Select a Zone--"
                        dataset={zones}
                        register={register}
                        objId="id"
                        objName="name"
                        validation={{required: "Please select a zone to edit"}}
                        error={errors.id}
                        disabled={false}
                        onlyValue={undefined}
                    />

                    <FormInput
                        label="Zone Name"
                        name="name"
                        type="text"
                        placeholder="--Select a Zone to view its name--"
                        register={register}
                        validation={{
                            required: "Please enter a zone name",
                            maxLength: {value: 255, message: "The zone name is too long"}
                        }}
                        error={errors.name}
                        disabled={!isDirty}
                    />

                    <div className="formButtons">
                        <button className="formButton1" type="submit" onClick={handleEdit}>
                            <Delete/>
                            <span> Edit Zone</span>
                        </button>
                        <button className="formButton1" type="button" onClick={handleCloseDialog} value="Cancel">
                            <Block/>
                            <span> Cancel</span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

interface DeletePricingZoneProps {
    title: string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
}
export function DeletePricingZone(props: DeletePricingZoneProps) {
    const {title, openPopup, setOpenPopup} = props
    const {register, handleSubmit, reset, formState: {errors}} = useForm()
    const {addCallback, callbacks} = useRspHandler()
    const dispatch = useDispatch()
    const reduxPricingZones = useAppSelector(state=>state.asset.pricingZones)
    const [zones, setZones] = useState(reduxPricingZones)

    useEffect(()=>{
        setZones(reduxPricingZones)
    }, [reduxPricingZones])
    
    const handleCloseDialog = () => {
        setOpenPopup(false)
        reset({id: ""})
    }

    const handleDelete = handleSubmit(async data => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully deleted pricing zone")
            handleCloseDialog()
            dispatch(PricingZoneAPI.getPricingZones())
        }
        function onFail(response: ResponseType) {
            logger.info("Failed to delete pricing zone")
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Save Pricing Zone", onSuccess, onFail)) {
            dispatch(PricingZoneAPI.deletePricingZone(newID, data.id))
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleDelete}>
                    <FormSelect
                        label="Zone to Delete"
                        name="id"
                        placeholder="--Select a Zone--"
                        dataset={zones}
                        register={register}
                        objId="id"
                        objName="name"
                        validation={{required: "Please select a zone to delete"}}
                        error={errors.id}
                        disabled={false}
                        onlyValue={undefined}
                    />

                    <div className="formButtons">
                        <button className="formButton1" type="submit" onClick={handleDelete}>
                            <Delete/>
                            <span> Delete Zone</span>
                        </button>
                        <button className="formButton1" type="button" onClick={handleCloseDialog} value="Cancel">
                            <Block/>
                            <span> Cancel</span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}