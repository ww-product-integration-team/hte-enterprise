import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography, IconButton, Tooltip} from '@material-ui/core';
import {AddCircle, Edit, Delete, Block} from '@mui/icons-material';
import './formStyles.css';
import { Form, FormCheck, Col, Row } from '@themesberg/react-bootstrap';
import { useState, useEffect} from "react";
import Alert from '@mui/material/Alert';
import { DeptAPI } from "../api/index"
import { useDispatch} from "react-redux";
import logger from "../utils/logger";
import DepartmentIcon from '@material-ui/icons/LocalGroceryStoreOutlined';
import { FormSelect, FormInput, HiddenDomainEntry } from './FormHelper';
import {Help} from '@material-ui/icons';
import {Link} from 'react-router-dom'
import { useRspHandler } from "../utils/ResponseProvider";
import { setExpandedTreeItems } from "../../state/actions/assetActions";
import { store } from "../../state";
import { useAppSelector } from "../../state";
import { AssetTypes, Department } from "../../types/asset/AssetTypes";
import { Node, lazyNode } from "../../types/asset/TreeTypes";
import { ResponseType } from "../../types/storeTypes";

interface CreateDepartmentProps{
    title: string, 
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
}

// Creating a new department
function CreateDepartment(props : CreateDepartmentProps) {
    interface CreateDepartmentForm{
        newDepartment: string
        departmentText : string
        defaultProfile : string
        UseAccountDomain: string
        selectDomain: string
        ManualEntry: string
    }

    const { register, handleSubmit, reset,  setValue, formState: {errors} } = useForm<CreateDepartmentForm>();
    const { title, openPopup, setOpenPopup } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const dispatch = useDispatch();
    const currentAccount = useAppSelector((state) => state.access.account);
    const profiles = useAppSelector((state) => state.profile.profiles);
    const {callbacks, addCallback} = useRspHandler();

    const defaultValues = {
        newDepartment: "",
        departmentText : "",
        defaultProfile : "",
        UseAccountDomain: "",
        selectDomain: "",
        ManualEntry: ""
    };

    const handleCloseDialog = (event ?: React.MouseEvent<HTMLElement, MouseEvent>) => {
        setShowAlert([false, "Something went wrong!"])
        if(event){ event.stopPropagation() }
        setOpenPopup(false);
        reset(defaultValues);
    }

    const handleAdd = handleSubmit(async (data: CreateDepartmentForm) => {    // When the Submit button is pressed
        logger.info("POST request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        // Data to be sent
        let newDepartment : Partial<Department> = {
            "deptName1" : data.newDepartment,
            "deptName2" : data.departmentText,
            "requiresDomainId": data.UseAccountDomain ? currentAccount.domain.domainId :  data.selectDomain
        }
        if(data.defaultProfile){
            newDepartment.defaultProfileId = data.defaultProfile
        }


        const newID = Date.now()
        function onSuccess(response : ResponseType){
            dispatch(DeptAPI.fetchDepartment())
            handleCloseDialog()
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add Department!"])
        }

        if(addCallback(callbacks, newID, "Post Department", onSuccess, onFail)){
            //Adding the stores domain as the domain of the entity that is being changed
            //i.e. Adding a new department to this store requires the stores domain permission
            dispatch(DeptAPI.postDepartment(newID, newDepartment, data.UseAccountDomain ? currentAccount.domain.domainId : data.selectDomain))
        }
        
    })

    return (
        <Dialog open={openPopup} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormInput
                        label="Department Name: "
                        id="newDepartment"
                        name="newDepartment"
                        type="text"
                        placeholder="Enter a department name"
                        register={register}
                        validation={{ 
                            required: "Please enter a department name", 
                            maxLength: { value: 45, message: "You exceeded the max name length"}
                        }}
                        error={errors.newDepartment}
                    />
                    <br />
                    <FormInput
                        label= "Department Text (optional):"
                        id="DepartmentText"
                        name="departmentText"
                        type="text"
                        placeholder="Enter a short description here"
                        register={register}
                        validation={{ 
                            maxLength: { value: 45, message: "You exceeded the max department name length"}
                        }}
                        error={errors.departmentText}
                    />
                    <br />
                    <FormSelect
                        label="Select a Default Scale Profile (optional): "
                        id="defaultProfile"
                        name="defaultProfile"
                        placeholder="--Select a profile--"
                        dataset={profiles}
                        register={register}
                        objId="profileId"
                        objName="name"
                        error={errors.defaultProfile} validation={undefined} disabled={undefined} onlyValue={undefined}/>
                    <br />
                    <HiddenDomainEntry 
                        id="manualSelectDomain"
                        name="ManualDomainEntry"
                        setValue={setValue}
                        register={register}
                        validation={{
                            required: "Please enter a domain"
                        }}
                        error={errors.ManualEntry} placeholder={undefined} disableOther={undefined}/>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}
                    
                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Create Department </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}


interface PostDepartmentProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    storeElement ?: lazyNode | Node
}

// Adding an existing department to a store
function PostDepartment(props : PostDepartmentProps) {
    interface PostDepartmentForm{
        selectStore : string
        addDepartment: string
    }

    const { register, handleSubmit, reset, formState: {errors} } = useForm<PostDepartmentForm>();
    const { title, openPopup, setOpenPopup, storeElement} = props;
    const [selectDisabled, setSelectDisabled] = useState(false)
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();


    const dispatch = useDispatch();
    const depts = useAppSelector((state) => state.asset.depts);
    const stores = useAppSelector((state) => state.asset.stores);
    
    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        selectStore : "",
        addDepartment: "",
    };

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if(event != null){ event.stopPropagation() }
        setOpenPopup(null);
        reset(defaultValues);
        setSelectDisabled(false)
    }

    const handleAdd = handleSubmit(async (data:PostDepartmentForm) => {    // When the Submit button is pressed
        logger.info("(DepartmentForm) POST request data: ", data)
        logger.info("(DepartmentForm) ", storeElement)
        setShowAlert([false, "Something went wrong!"])

        let storeId = data.selectStore
        let location = ['0']
        if(storeElement != null){
            if(!storeElement.id){
                setShowAlert([true, "ERROR Parsing out Department/Store Info"])
                return
            }
            else{
                storeId = storeElement.id
            }
        }

        if(!storeId || storeId === ""){
            setShowAlert([true, "ERROR Parsing out Department/Store Info"])
            return
        }
        

        let doesExist : boolean[]
        
        //====================================
        //Add Existing department, data.addDepartment

        //Grab Department infor based on the id passed in
        let dept = depts[data.addDepartment]

        if(!dept) {
            setShowAlert([true, "Invalid department type!"])
            return
        }

        //Check to make sure that this store doesn't already contain this department
        if(storeElement != null){
            let id = location[1] + "|" + dept.deptId
            doesExist = Object.keys(storeElement.children).map(objKey => {
                let obj = storeElement.children[objKey]
                return(obj.id === id)
                     
            })
        
            if(doesExist.includes(true)){
                setShowAlert([true, "Store already contains that department!"])
                return
            }
        }
        

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            dispatch(DeptAPI.fetchDepartment())
            if(storeElement != null){
                dispatch(setExpandedTreeItems([...store.getState().asset.expanded, storeElement.id]))
            }
            
            handleCloseDialog()
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add Department!"])
        }
        if(addCallback(callbacks, newID, "Assign Department", onSuccess, onFail)){
            dispatch(DeptAPI.assignDept(newID, dept.deptId, storeId))
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    {storeElement == null ?
                    <FormSelect
                            required
                            label="Select Store: "
                            id="selectStore"
                            name="selectStore"
                            type="text"
                            placeholder="--Select a store--"
                            dataset={Object.values(stores)}
                            register={register}
                            objId="storeId"
                            objName="storeName"
                            disabled={false}
                            validation={{ required: "Please select a store to add the department to" }}
                            error={errors.selectStore} onlyValue={undefined}                    /> : null}


                    <FormSelect
                        required
                        label="Select Department: "
                        id="addDepartment"
                        name="addDepartment"
                        type="text"
                        placeholder="--Select a Department--"
                        dataset={Object.values(depts)}
                        register={register}
                        objId="deptId"
                        objName="deptName1"
                        disabled={selectDisabled}
                        validation={{ required: "Please select a department to add" }}
                        error={errors.addDepartment} onlyValue={undefined}                    />

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}
                    
                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Add Department </span>
                        </button>

                        <Link to="/DeptManagement" style={{display: 'flex', flex: '1 1'}}>
                            <button className="formButton1" type="button" style={{flexGrow: '1'}}>
                                <DepartmentIcon />
                                <span> Create Department </span>
                            </button>
                        </Link>


                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

// Edit Department
interface PutDepartmentProps{
    title : string
    openPopup ?: boolean
    setOpenPopup ?: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement: lazyNode | Department | Node
}

function PutDepartment(props: PutDepartmentProps) {
    interface PutDepartmentForm{
        departmentName : string,
        departmentText : string,
        defaultProfile : string,
        UseAccountDomain : string,
        selectDomain : string,
        willUpdateDomain: boolean,
        ManualEntry : boolean
    }

    const { register, handleSubmit, reset, formState: {errors}, setValue} = useForm<PutDepartmentForm>();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const {callbacks, addCallback} = useRspHandler();


    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const dispatch = useDispatch();

    const profiles = useAppSelector((state) => state.profile.profiles);
    const depts = useAppSelector((state) => state.asset.depts);
    const currentAccount = useAppSelector((state) => state.access.account);
    const [updateDomain, setUpdateDomain] = useState(false)
    const [currentDept, setCurrentDepartment] = useState< Department | undefined>(undefined)
    function isDeptObj(a : any){
        return 'deptId' in a
    }

    // Come back to this to display current domain of department being edited
    // let currentDomain = store.getState().asset.tree.find((node) => node.id === treeElement.requiresDomainId)
    useEffect(()=>{
        let currentDept : Department | undefined = undefined
        if (!isDeptObj(treeElement)) {
            if((treeElement as lazyNode).id.includes("|")){
                currentDept = depts[(treeElement as lazyNode).id.split("|")[1]]
            } else {
                currentDept = depts[(treeElement as lazyNode).id]
            }
        }
        else{
            currentDept = depts[(treeElement as Department).deptId]
        }
        if(!currentDept){
            setShowAlert([true, "ERROR Parsing out Department/Store Info"])
        }
        setCurrentDepartment(currentDept)
    },[treeElement])

    useEffect(()=>{
        if(!isDeptObj(treeElement)){
            setValue("departmentName", (treeElement as lazyNode).name)
            setValue("departmentText", (treeElement as lazyNode).text ?? "")
            //setValue("defaultProfile", treeElement.defaultProfileId)
        }
        else {
            setValue("departmentName", (treeElement as Department).deptName1)
            setValue("departmentText", (treeElement as Department).deptName2)
            setValue("defaultProfile", (treeElement as Department).defaultProfileId)
        }
        
    },[treeElement])

    const defaultValues = {
        departmentName : "",
        departmentText : "",
        defaultProfile : "",
        UseAccountDomain : "",
        selectDomain : "",
        willUpdateDomain : false,
        ManualEntry: false
    };

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if(event != null){ event.stopPropagation() }
        if(setOpenPopup != null){
            setOpenPopup(null);
        }
        reset(defaultValues);
    }

    const handleAdd = handleSubmit(async (data : PutDepartmentForm) => {    // When the Submit button is pressed
        logger.info("(PutDepartment) PUT request data: ", data)
        setShowAlert([false, "Something went wrong!"])
        
        let newDepartment = {
            "deptId" : currentDept ? currentDept.deptId : "Unknown",
            "deptName1" : data.departmentName,
            "deptName2" : data.departmentText,
            "defaultProfileId": data.defaultProfile ? data.defaultProfile : (treeElement as Department).defaultProfileId,
            "requiresDomainId" : updateDomain === false 
                ? (treeElement as Department).requiresDomainId 
                : data.UseAccountDomain 
                    ? currentAccount.domain.domainId 
                    : data.selectDomain
        }

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            setUpdateDomain(false)
            handleCloseDialog()
            dispatch(DeptAPI.fetchDepartment())
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not edit Department!"])
        }

        if(addCallback(callbacks, newID, "Edit Department", onSuccess, onFail)){
            dispatch(DeptAPI.postDepartment(newID, newDepartment, newDepartment.requiresDomainId))
        }
    })

    return (
        <>
        {!openPopup ?
        <>
            <Form className="form-control" onSubmit={handleAdd}>
                <Row>
                    <Col md={6} className="mb-3">
                        <FormInput
                            label={"Current Department Name: "}
                            defaultValue={currentDept ? currentDept.deptName1 : null}
                            id="departmentName"
                            name="departmentName"
                            type="text"
                            placeholder="Enter a department name"
                            register={register}
                            validation={{ 
                                required: "Please enter a department name", 
                                maxLength: { value: 45, message: "You exceeded the max department name length"}
                            }}
                            error={errors.departmentName}
                        />
                        <br />
                    </Col>

                    <Col md={6} className="mb-3">
                        <FormInput
                            label= "Department Description (optional):"
                            defaultValue={currentDept ? currentDept.deptName2 : null}
                            id="departmentText"
                            name="departmentText"
                            type="text"
                            placeholder={(treeElement as lazyNode).text ? "Enter a description" : (treeElement as Department).deptName2 ? "Enter a description" : "Description not set"}
                            register={register}
                            validation={{ 
                                maxLength: { value: 45, message: "You exceeded the max department name length"}
                            }}
                            error={errors.departmentText}
                        />
                        <br />
                    </Col>

                    <Col md={6} className="mb-3">
                        <FormSelect
                                    customdefault={currentDept ? currentDept.defaultProfileId : null}
                                    label="Select a Default Scale Profile (optional): "
                                    id="defaultProfile"
                                    name="defaultProfile"
                                    placeholder="--Select a profile--"
                                    dataset={profiles}
                                    register={register}
                                    objId="profileId"
                                    objName="name"
                                    // validation={{required: "Please select a profile to edit"}}
                                    error={errors.defaultProfile} validation={undefined} disabled={undefined} onlyValue={undefined}                        />
                        <br />

                        <FormCheck  type="checkbox" className="d-flex mb-2 p-2 tableTemplate">
                            <FormCheck.Input id="update" className="me-2" 
                                onClick={(e) => setUpdateDomain(e.target.checked)} 
                                {...register("willUpdateDomain")}
                            />
                            <FormCheck.Label htmlFor="update">
                                Update Required Domain
                                <Tooltip className="info" title={<Typography> Updating the "Required Domain" changes who can see this department.</Typography>}>
                                    <IconButton aria-label="enableAccount">
                                        <Help />
                                    </IconButton>
                                </Tooltip>
                            </FormCheck.Label>
                        </FormCheck>
                    </Col>

                    <Col md={12} className="mb-3">
                        {updateDomain &&
                            <HiddenDomainEntry 
                                    id="manualSelectDomain"
                                    name="ManualDomainEntry"
                                    setValue={setValue}
                                    register={register}
                                    validation={{
                                        required: "Please enter a domain"
                                    }}
                                    error={errors.ManualEntry} placeholder={undefined} disableOther={undefined}                            />
                        }
                        
                    </Col>                    
                </Row>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <Edit />
                            <span> Update Department </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
        </> :

        <>
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title} - <span style={{fontWeight: "bold"}}>{(treeElement as lazyNode).name ? (treeElement as lazyNode).name : (treeElement as Department).deptName1}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                <Row>
                    <Col md={6} className="mb-3">
                        <FormInput
                            label={"Current Department Name: "}
                            defaultValue={currentDept ? currentDept.deptName1 : null}
                            id="departmentName"
                            name="departmentName"
                            type="text"
                            placeholder="Enter a department name"
                            register={register}
                            validation={{ 
                                required: "Please enter a department name", 
                                maxLength: { value: 45, message: "You exceeded the max department name length"}
                            }}
                            error={errors.departmentName}
                        />
                        <br />
                    </Col>

                    <Col md={6} className="mb-3">
                        <FormInput
                            label= "Department Description (optional):"
                            defaultValue={currentDept ? currentDept.deptName2 : null}
                            id="departmentText"
                            name="departmentText"
                            type="text"
                            placeholder={(treeElement as lazyNode).text ? "Enter a description" : (treeElement as Department).deptName2 ? "Enter a description" : "Description not set"}
                            register={register}
                            validation={{ 
                                maxLength: { value: 45, message: "You exceeded the max department name length"}
                            }}
                            error={errors.departmentText}
                        />
                        <br />
                    </Col>

                    <Col md={6}>
                            <FormSelect
                                            customdefault={currentDept ? currentDept.defaultProfileId : null}
                                            label="Select a Default Scale Profile (optional): "
                                            id="defaultProfile"
                                            name="defaultProfile"
                                            placeholder="--Select a profile--"
                                            dataset={profiles}
                                            register={register}
                                            objId="profileId"
                                            objName="name"
                                            // validation={{required: "Please select a profile to edit"}}
                                            error={errors.defaultProfile} validation={undefined} disabled={undefined} onlyValue={undefined}                            />
                            <br />

                            <FormCheck  type="checkbox" className="d-flex p-2 tableTemplate">
                                <FormCheck.Input id="update" className="me-2" 
                                    onClick={(e) => setUpdateDomain(e.target.checked)} 
                                    {...register("willUpdateDomain")}
                                />
                                <FormCheck.Label htmlFor="update">
                                    Update Required Domain
                                    <Tooltip className="info" title={<Typography> Updating the "Required Domain" changes who can see this department.</Typography>}>
                                        <IconButton aria-label="enableAccount">
                                            <Help />
                                        </IconButton>
                                    </Tooltip>
                                </FormCheck.Label>
                            </FormCheck>
                    </Col>

                    <Col md={12}>
                    {updateDomain &&
                        <HiddenDomainEntry 
                                            id="manualSelectDomain"
                                            name="ManualDomainEntry"
                                            setValue={setValue}
                                            register={register}
                                            validation={{
                                                required: "Please enter a domain"
                                            }}
                                            error={errors.ManualEntry} placeholder={undefined} disableOther={undefined}                        />
                    }   
                    </Col>                    
                </Row>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <Edit />
                            <span> Update Department </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
        </>
        }
        </>
    )
}
interface DeleteDepartmentProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    department: Department
    setCurrentDepartment : React.Dispatch<React.SetStateAction<Department | null>>
}
// DELETE Request
function DeleteDepartment(props : DeleteDepartmentProps) {
    const { handleSubmit} = useForm();
    const { title, openPopup, setOpenPopup, department, setCurrentDepartment} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();


    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);
        if(event != null){ 
            event.stopPropagation() 
        }
        else{setCurrentDepartment(null)}
        
    }

    const handleDelete = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(DeparmentForm) DELETE request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        //Deletion of child scales will be handled by the backend
        if(!department.deptId){
            setShowAlert([true, "Invalid department data type"])
            return
        }


        // TODO: No time to test the interaction of (deleting departments / scale profiles) + departments / scales being assigned to them.
        
        logger.info("Actual Deletion of Department")

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            dispatch(DeptAPI.fetchDepartment())
            handleCloseDialog()
            setCurrentDepartment(null)
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not remove Department!"])
        }
        if(addCallback(callbacks, newID, "Delete Department", onSuccess, onFail)){
            dispatch(DeptAPI.deleteDepartment(newID, department))
        }
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title} - <span style={{fontWeight: "bold"}}>{department.deptName1 ?? "Unknown"}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>

                    <div className="formSpacing">
                        <Form.Label id="confirmDelete" style={{color: "black"}}>Are you sure you want to delete the following department:</Form.Label>
                        <p>Department Name:     <span style={{fontWeight: "bold", color : "#ba4735"}}>{department.deptName1 ?? "Unknown"}</span></p>
                        <p>Description:         <span style={{fontWeight: "bold", color : "#ba4735"}}>{department.deptName2 ? department.deptName2 : "Not set"} </span></p>
                        
                        <br />
                        
                        <Alert className="m-1" variant="outlined" severity="error">
                            <p className="fw-bold"> Warning: Departments that are deleted cannot be recovered. They would have to be created again.</p>
                        </Alert>
                    </div>



                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleDelete)} type="submit">
                            <Delete />
                            <span> Delete Department </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

interface RemoveDepartmentProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement: lazyNode | Node
}
// Remove Request
function RemoveDepartment(props : RemoveDepartmentProps) {
    const { handleSubmit} = useForm();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();


    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if(event != null){ event.stopPropagation() }
        setOpenPopup(null);
    }

    const handleDelete = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(DeparmentForm) Rem request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        let deptId = ""
        let storeId = ""
        if (treeElement != null){
            if(!treeElement.id || !(treeElement as lazyNode).path[2]){
                setShowAlert([true, "ERROR Parsing out Department/Store Info"])
                return
            }
            deptId = treeElement.id;
            storeId = (treeElement as lazyNode).path[2];
        }
        else{
            setShowAlert([true, "Cannot remove department at this time, data mismatch"])
            return
        }
        

        // TODO: No time to test the interaction of (deleting departments / scale profiles) + departments / scales being assigned to them.
        
        logger.info("Removing Dept " + deptId + " from store: " + storeId)

        
        const newID = Date.now()
        function onSuccess(response : ResponseType){
            handleCloseDialog()
            dispatch(DeptAPI.fetchDepartment())
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not remove Department!"])
        }
    
        if(addCallback(callbacks, newID, "Remove Department", onSuccess, onFail)){
            dispatch(DeptAPI.removeDept(newID, deptId, storeId))
        }
        
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title} - <span style={{fontWeight: "bold"}}>{treeElement.name ? treeElement.name : "Unknown"}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>

                    <div className="formSpacing">
                        <Form.Label id="confirmDelete" style={{color: "black"}}>Are you sure you want to remove this department from this store? :</Form.Label>
                        <p>Department Name:     <span style={{fontWeight: "bold", color : "#ba4735"}}>{treeElement.name ? treeElement.name : "Unknown"}</span></p>
                        <p>Description:         <span style={{fontWeight: "bold", color : "#ba4735"}}>{treeElement.text ? treeElement.text : "Not set"} </span></p>
                        
                        <br />
                        
                        <Alert className="m-1" variant="outlined" severity="error">
                            <p className="fw-bold"> Warning: Any scales under this department will set as unassigned</p>
                        </Alert>
                    </div>



                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleDelete)} type="submit">
                            <Delete />
                            <span> Remove Department </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

export { CreateDepartment, PostDepartment, PutDepartment, DeleteDepartment, RemoveDepartment};