import { useForm} from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import { Form } from '@themesberg/react-bootstrap';
import { FormInput, FormSelect, HiddenManualEntry } from './FormHelper';
import {AddCircle, Edit, Delete, Block} from '@mui/icons-material';
import { useEffect, useState} from 'react';
import { useDispatch} from "react-redux";
import Alert from '@mui/material/Alert';
import { StoreAPI } from "../api/index" 
import './formStyles.css';
import logger from "../utils/logger";
import { useRspHandler } from "../utils/ResponseProvider";
import { store, useAppSelector } from "../../state";
import DoneIcon from '@mui/icons-material/Done';
import { Node, lazyNode } from "../../types/asset/TreeTypes";
import { AssetTypes, Store } from "../../types/asset/AssetTypes";
import { ResponseType } from "../../types/storeTypes";
import { setExpandedTreeItems } from "../../state/actions/assetActions";

// Banner, Region, STORE, Department, Scale

// POST Request

interface PostStoreProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement ?: lazyNode | Node
}
function PostStore(props : PostStoreProps) {
    interface PostStoreForm{
        storeId: string
        selectRegion: string
        storeAddress: string
        storeName: string
        ManualAdd: boolean
        ManualEntry: string
    }

    const { register, handleSubmit, reset, formState: {errors}, setError} = useForm<PostStoreForm>();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const regions = useAppSelector((state) => state.asset.regions);

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        storeId: "",
        selectRegion: "",
        storeAddress: "",
        storeName: "",
        ManualAdd: false,
        ManualEntry: "",
    };

    const handleCloseDialog = (event ?: any) => {
        if(event){event.stopPropagation()}
        setOpenPopup(null);
        reset(defaultValues);
        setShowAlert([false, "Something went wrong!"])
        if(document.getElementById("asvb")){
            (document.getElementById("asvb") as HTMLInputElement).checked = false;
        }
    }

    const handleAdd = handleSubmit(async (data) => {    // When the Submit button is pressed
        logger.info("(StoreForm) POST request data: ", data)
        logger.info("(StoreForm) treeElement", treeElement)

        let newStoreId = data.storeId //Customer provided store number
        let regionId = data.selectRegion
        if(treeElement != null){
            if(!treeElement.id){
                setShowAlert([true, "ERROR Parsing out Region/Store Info"])
                return
            }
            else{
                regionId = treeElement.id
            }
        }

        if(!regionId || regionId === ""){
            setShowAlert([true, "ERROR Parsing out Region/Store Info"])
            return
        }
        

        setShowAlert([false, "Something went wrong!"])

        //If user has entered in something and it's not a integer
        if((Number.isNaN(data.storeId) ||
            !Number.isInteger(parseInt(data.storeId))) 
            && data.storeId !== ""){
            setError("storeId", { type: "manual", message:"Store number must be a valid number" })
            return
        }


        const newID = Date.now()
        function onSuccess(response : ResponseType){
            setOpenPopup(null);
            reset(defaultValues);
            dispatch(StoreAPI.fetchStores())
            if(treeElement != null){
                dispatch(setExpandedTreeItems([...store.getState().asset.expanded, treeElement.id]))
            }

        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add store!"])
        }

        
        if(addCallback(callbacks, newID, "Post Store", onSuccess, onFail)){
            dispatch(StoreAPI.postStore(newID,
                {
                    "address": data.storeAddress,
                    "regionId": regionId,
                    "storeName": data.storeName,
                    "customerStoreNumber": newStoreId,
                    "ipAddress": data.ManualAdd ? data.ManualEntry : null
                }, regionId
            ))
        }
    })


    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>

                    {treeElement == null ?
                        <FormSelect
                            required
                            label="Select Region: "
                            id="selectRegion"
                            name="selectRegion"
                            type="text"
                            placeholder="--Select a Region--"
                            dataset={regions}
                            register={register}
                            objId="regionId"
                            objName="regionName"
                            disabled={false}
                            validation={{ required: "Please select a region to add the store to" }}
                            error={errors.selectRegion} 
                            onlyValue={undefined} 
                        /> 
                    : null}


                    <FormInput
                        label="Store Name: "
                        id="createStore"
                        name="storeName"
                        type="text"
                        placeholder="Enter a store name"
                        register={register}
                        validation={{ 
                            required: "Please enter a store name", 
                            maxLength: { value: 45, message: "You exceeded the max store name length"}
                        }}
                        error={errors.storeName}
                    />

                    <h1> </h1>
                    <FormInput
                        label="Address: (Optional)"
                        id="createStore"
                        name="storeAddress"
                        type="text"
                        placeholder="Enter an address"
                        register={register}
                        validation={{
                            maxLength: { value: 50, message: "You exceeded the max address length"}
                        }}
                        error={errors.storeAddress}
                    />
                    <h1> </h1>
                    <FormInput
                        label="Store Number: (Optional)"
                        id="createStore"
                        name="storeId"
                        type="text"
                        placeholder="Enter a number"
                        register={register}
                        validation={{
                            maxLength: { value: 10, message: "You exceeded the max allowable store number"}
                        }}
                        error={errors.storeId}
                    />


                    <HiddenManualEntry 

                        labelCheck="Setup Store Server"
                        label="Store server IP Address"
                        id="manualStoreServerSetup"
                        name="ManualEntry"
                        type="text"
                        placeholder="Enter a Valid IP Address"
                        register={register}
                        validation={{
                            required: "Please enter a valid IP address",
                            maxLength: { value: 15, message: "You exceeded the max scale IP length" }
                        }}
                        error={errors.ManualEntry} disableOther={undefined}                    />
                    


                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Add Store </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

function isStore(a: Store | lazyNode | Node){
    return (a as Store).storeId !== undefined;
}

// PUT Request
interface PutStoreProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement : Store | lazyNode | Node
}

function PutStore(props : PutStoreProps) {
    interface PutStoreForm{
        storeAddress : string
        storeName : string
        storeId : string
        ManualAdd : boolean
        ManualEntry : string
    }
    const { register, handleSubmit, reset, formState: {errors} } = useForm<PutStoreForm>();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const [currentStore, setCurrentStore] = useState<Store>();
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    useEffect(()=>{
        let currentStore : Store | undefined
        if(isStore(treeElement)){
            currentStore = treeElement as Store
        }
        else{
            if(!(treeElement as lazyNode).id){
                setShowAlert([true, "ERROR Parsing out Region/Store Info"])
            }
            else{
                currentStore = store.getState().asset.stores[(treeElement as lazyNode).id]
            }
            if(!currentStore){
                setShowAlert([true, "ERROR Parsing out Region/Store Info"])
            }
        }
        setCurrentStore(currentStore)
    },[treeElement])
   

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        input: "",
        ManualAdd: false,
    };

    const handleCloseDialog = (event ?: any) => {
        if(event){event.stopPropagation()}
        setOpenPopup(null);
        reset(defaultValues);
        setShowAlert([false, "Something went wrong!"])
        if(document.getElementById("asvb")){
            (document.getElementById("asvb") as HTMLInputElement).checked = false;
        }
        
    }

    const handleAdd = handleSubmit(async (data: PutStoreForm) => {    // When the Submit button is pressed
        logger.info("(StoreForm) PUT request data: ", data)

        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            handleCloseDialog()
            dispatch(StoreAPI.fetchStores())
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not edit store!"])
        }
    
        if(currentStore){
            if(addCallback(callbacks, newID, "Edit Store", onSuccess, onFail)){
                dispatch(StoreAPI.postStore(newID,
                    {
                        "address": data.storeAddress,
                        "regionId": currentStore.regionId,
                        "storeName": data.storeName,
                        "storeId": currentStore.storeId,
                        "customerStoreNumber": data.storeId,
                        "ipAddress": data.ManualAdd ? data.ManualEntry : null
                    }, currentStore.storeId
                ))
            }
        }
        else{
            setShowAlert([true, "Could not complete action!"])
        }
        
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}: <b style={{color : "#ba4735"}}>{currentStore?.storeName ?? "Unknown"}</b>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormInput
                        label="Store Name: "
                        id="createStore"
                        name="storeName"
                        type="text"
                        placeholder="Enter a store name"
                        register={register}
                        validation={{ 
                            required: "Please enter a store name", 
                            maxLength: { value: 45, message: "You exceeded the max region name length"}
                        }}
                        error={errors.storeName}
                        defaultValue={currentStore?currentStore.storeName:null}
                    />

                    <h1> </h1>
                    <FormInput
                        label="Address: (Optional)"
                        id="createStore"
                        name="storeAddress"
                        type="text"
                        placeholder="Enter an address"
                        register={register}
                        validation={{
                            maxLength: { value: 50, message: "You exceeded the max address length"}
                        }}
                        error={errors.storeAddress}
                        defaultValue={currentStore?currentStore.address:null}
                    />

                    <FormInput
                        label="Store Number: (Optional)"
                        id="createStore"
                        name="storeId"
                        type="text"
                        placeholder="Enter a number"
                        register={register}
                        validation={{
                            maxLength: { value: 10, message: "You exceeded the max allowable store number"}
                        }}
                        error={errors.storeId}
                        defaultValue={currentStore?currentStore.customerStoreNumber:null}
                    />

                    {currentStore && currentStore.ipAddress ?
                        <Form.Label style={{marginTop:"1rem"}}> <DoneIcon/> Store server IP already setup</Form.Label>
                    : 
                        <HiddenManualEntry 

                            labelCheck="Setup Store Server"
                            label="Store server IP Address"
                            id="manualStoreServerSetup"
                            name="ManualEntry"
                            type="text"
                            placeholder="Enter a Valid IP Address"
                            register={register}
                            validation={{
                                required: "Please enter a valid IP address",
                                maxLength: { value: 15, message: "You exceeded the max scale IP length" }
                            }}
                            error={errors.ManualEntry} disableOther={undefined}                        />
                    }

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}


                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <Edit />
                            <span> Update Store </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}


interface DeleteStoreProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement : lazyNode | Node
}
// DELETE Request
function DeleteStore(props : DeleteStoreProps) {
    const { handleSubmit} = useForm();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();
    const lazyTree = useAppSelector((state)=>state.asset.lazyTree)
    const dispatch = useDispatch();

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
    }

    const handleDelete = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(DeleteStore) DELETE request data: ", treeElement)
        
        if(!treeElement.id){
            setShowAlert([true, "Error Parsing out Region/Store Info"])
            return
        }

        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            setOpenPopup(null);
            dispatch(StoreAPI.fetchStores())
            // dispatch(AssetAPI.fetchLazyTree(Date.now(),(treeElement as lazyNode).path[1], lazyTree))
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not delete store!"])
        }

        if(addCallback(callbacks, newID, "Delete Store", onSuccess, onFail)){
            dispatch(StoreAPI.deleteStore(newID, treeElement.id))
        }
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>
                    <Form.Label id="confirmDelete">Are you sure you want to remove store: <b style={{color : "#ba4735"}}>{treeElement.name}</b>?</Form.Label>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleDelete)} type="submit">
                            <Delete />
                            <span> Delete Store </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

export { PostStore, PutStore, DeleteStore };