import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import { Form} from '@themesberg/react-bootstrap';
import { FormInput, FormSelect } from './FormHelper';
import { useState} from 'react';
import { useDispatch} from "react-redux";
import Alert from '@mui/material/Alert';
import {AddCircle, Edit, Delete, Block} from '@mui/icons-material';
import { RegionAPI } from "../api/index" 

import './formStyles.css';
import logger from "../utils/logger";
import { useRspHandler } from "../utils/ResponseProvider";
import { store, useAppSelector } from "../../state";
import { Node, lazyNode } from "../../types/asset/TreeTypes";
import { AssetTypes } from "../../types/asset/AssetTypes";
import { ResponseType } from "../../types/storeTypes";
import { setExpandedTreeItems } from "../../state/actions/assetActions";

// Banner, REGION, Store, Department, Scale

// POST Request
interface PostRegionProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement ?: lazyNode | Node
}

function PostRegion(props : PostRegionProps) {
    interface PostRegionForm{
        selectBanner: string
        regionName: string
    }

    const { register, handleSubmit, reset, formState: {errors} } = useForm<PostRegionForm>();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();
    const banners = useAppSelector((state) => state.asset.banners);

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        selectBanner: "",
        regionName: ""
    };

    const handleCloseDialog = (event ?: any) => {
        event.stopPropagation()
        setOpenPopup(null);
        reset(defaultValues);
    }

    const handleAdd = handleSubmit(async (data) => {    // When the Submit button is pressed
        logger.info("(RegionForm) POST request data: ", data)

        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            logger.info("(RegionForm) New Region posted: ");
            setOpenPopup(null);
            reset(defaultValues);
            dispatch(RegionAPI.fetchRegions())
            if(treeElement != null){
                dispatch(setExpandedTreeItems([...store.getState().asset.expanded, treeElement.id]))
            }
        }
        function onFail(response : ResponseType){
            logger.error("(RegionForm) Error response: ", response.response);
                setShowAlert([true, "Could not add Region!"])
        }

        let bannerId = treeElement == null ? data.selectBanner : treeElement.id
        if(bannerId == null){
            setShowAlert([true, "Error parsing banner Id!"])
            return
        }
        
        if(addCallback(callbacks, newID, "Post Region", onSuccess, onFail)){
            dispatch(RegionAPI.postRegions(newID,
                {
                    "bannerId": bannerId,
                    "regionName": data.regionName,
                }, bannerId
            ))
        }
    })

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>

                <Form className="form-control" onSubmit={handleAdd}>

                    {treeElement == null ?
                    <FormSelect
                            required
                            label="Select Banner: "
                            id="selectBanner"
                            name="selectBanner"
                            type="text"
                            placeholder="--Select a Banner--"
                            dataset={banners}
                            register={register}
                            objId="bannerId"
                            objName="bannerName"
                            disabled={false}
                            validation={{ required: "Please select a banner to add to" }}
                            error={errors.selectBanner} onlyValue={undefined}                    
                    /> : null}


                    <FormInput
                        label="Region Name: "
                        id="createRegion"
                        name="regionName"
                        type="text"
                        placeholder="Enter a region name"
                        register={register}
                        validation={{ 
                            required: "Please enter a region name", 
                            maxLength: { value: 45, message: "You exceeded the max region name length"}
                        }}
                        error={errors.regionName}
                    />

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Add Region </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}


interface PutRegionProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement : lazyNode | Node
}
// Edit Region
function PutRegion(props : PutRegionProps) {


    const { register, handleSubmit, reset, formState: {errors} } = useForm();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();

    const dispatch = useDispatch();

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        input: ""
    };

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
        reset({defaultValues});
    }

    const handleAdd = async (data) => {    // When the Submit button is pressed
        logger.info("(RegionForm) PUT request data: ", data)
        
        if(!treeElement.id && (treeElement as lazyNode).path[0]){
            setShowAlert([true, "Error Parsing out Banner/Region Info"])
            return
        }

        const regionName = {
            bannerId: (treeElement as lazyNode).path[0],
            regionId: treeElement.id,
            regionName: data.regionName
        }
        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            logger.info("(RegionForm) Region edited Succesfully");
            setOpenPopup(null);
            reset({defaultValues});
            dispatch(RegionAPI.fetchRegions())
        }
        function onFail(response : ResponseType){
            logger.error("(RegionForm) Error response: ",response.response);
            setShowAlert([true, "Could not edit Region!"])
        }
    
        if(addCallback(callbacks, newID, "Edit Region", onSuccess, onFail)){
            dispatch(RegionAPI.postRegions(newID, regionName, (treeElement as lazyNode).path[0]))
        }
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}: <b style={{color : "#ba4735"}}>{treeElement.name}</b>
                </Typography>
            </DialogTitle>
            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleAdd)}>
                    <FormInput
                        label="New Region Name:"
                        id="editRegion"
                        name="regionName"
                        type="text"
                        placeholder="Enter a region name"
                        register={register}
                        validation={{ 
                            required: "Please enter a region name", 
                            maxLength: { value: 45, message: "You exceeded the max region name length"}
                        }}
                        error={errors.regionName}
                        defaultValue={treeElement.name?treeElement.name:""}
                    />

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleAdd)} type="submit">
                            <Edit />
                            <span> Update Region </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

interface PutRegionProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    treeElement : lazyNode | Node
}
// DELETE Request
function DeleteRegion(props : PutRegionProps) {
    const { handleSubmit} = useForm();
    const { title, openPopup, setOpenPopup, treeElement} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
    }

    const handleDelete = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(RegionForm) DELETE request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        if(!treeElement.id){
            setShowAlert([true, "Error Parsing out Banner/Region Info"])
            return
        }

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            logger.info("(RegionForm) Region deleted Succesfully");
            setOpenPopup(null);
            dispatch(RegionAPI.fetchRegions())
        }
        function onFail(response : ResponseType){
            logger.info("(RegionForm) error response: ", response.response);
            setShowAlert([true, "Could not delete Region!"])
        }
    
        if(addCallback(callbacks, newID, "Delete Region", onSuccess, onFail)){
            dispatch(RegionAPI.deleteRegion(newID, treeElement.id))
        }
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>
                    <Form.Label id="confirmDelete">Are you sure you want to remove region: <b style={{color : "#ba4735"}}>{treeElement.name}</b>?</Form.Label>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleDelete)} type="submit">
                            <Delete />
                            <span> Delete Region </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

export { PostRegion, PutRegion, DeleteRegion };