import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import { useEffect, useState } from "react";
import { Block} from '@mui/icons-material';
import { Form} from '@themesberg/react-bootstrap';
import { ScaleAPI } from "../api/index" 
import './formStyles.css';
import { AccessActions, useAppSelector} from "../../state";
import VirtualTable from "../common/virtualTable/VirtualTable";
import { ScaleDevice } from '../../types/asset/ScaleTypes';

// Banner, Region, Store, Department, SCALE
//=================================================================================================
// POST Request
//=================================================================================================

const scaleHeadCells = [
    { id: 'ipAddress',        sorted: true,  searchable: true,  label: 'IP Address',          isShowing: true, mandatory: true},
    { id: 'hostname',         sorted: true,  searchable: true,  label: 'Hostname',            isShowing: true, mandatory: true},
    { id: 'scaleModel',       sorted: false, searchable: false, label: 'Scale Model',         isShowing: true, mandatory: true},
    { id: 'application',       sorted: false, searchable: false, label: 'Application Version',         isShowing: true, mandatory: false},
    { id: 'type',       sorted: false, searchable: false, label: 'Type',         isShowing: true, mandatory: false},
];

interface PostScaleProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    scales : ScaleDevice[]
}

function UnassignedScale(props : PostScaleProps) {
    const { title, openPopup, setOpenPopup, scales} = props;       // Passed from AssetList.jsx

    const [unassignedScales, setUnassignedStateScales] = useState<ScaleDevice[]>([])
    useEffect(() => {
        setUnassignedStateScales([...Object.values(scales).filter(scale =>
            scale.deviceId != null && 
            (scale.storeId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID || 
                scale.deptId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID ||
                scale.storeId === null || 
                scale.deptId === null)
        )])
    }, [scales, openPopup])
    let scaleCount = unassignedScales.length

    return (
        <>
            <Dialog open={openPopup} onClose={()=>setOpenPopup(false)} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
                <DialogTitle>
                    <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                        {title}
                    </Typography>
                </DialogTitle>

                <DialogContent>
                    <Form className="form-control" key={1}>      
                        <Typography variant="body1" component="div" style={{ flexGrow: 2 }}>
                             {"Number of unassigned scales: " + scaleCount}
                        </Typography>

                        <VirtualTable
                            dataSet={unassignedScales}
                            headCells={scaleHeadCells}
                            initialSortedBy={{ name: '' }}
                            dispatchedUrls={[ScaleAPI.fetchScales]}
                            maxHeight="300px"
                            dataIdentifier="deviceId"
                            saveKey="unassignedScalesTableHead" 
                            tableName={"UnassignedScalesTable"}
                            useToolbar = {false}
                        />
                        <div className="formButtons">
                        <button className="formButton1" onClick={()=>setOpenPopup(false)} type="button">
                            <Block />
                            <span> Close </span>
                        </button>
                    </div>
                    </Form>
                </DialogContent>
            </Dialog>
        </>
    )
}

export { UnassignedScale };