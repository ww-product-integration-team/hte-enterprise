import {useForm} from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import React, { useEffect, useState } from "react";
import Alert from '@mui/material/Alert';
import { FormSelect, HiddenManualEntry } from './FormHelper';
import { useDispatch} from "react-redux";
import {AddCircle, RemoveShoppingCartOutlined, Delete, Block} from '@mui/icons-material';
import { Form} from '@themesberg/react-bootstrap';
import {AssetAPI, ScaleAPI, ScaleCtrlAPI } from "../api/index" 
import './formStyles.css';
import AccessControl from "../common/AccessPermissions"
import { AccessActions, store, useAppSelector} from "../../state";
import {ASSIGN_SCALES } from "../../state/actions/accessActions"
import logger from "../utils/logger";
import { useRspHandler } from "../utils/ResponseProvider";
import VirtualTable from "../common/virtualTable/VirtualTable";
import { setExpandedTreeItems } from "../../state/actions/assetActions";
import { FormCheck } from "react-bootstrap";
import { ScaleDevice } from "../../types/asset/ScaleTypes";
import { AssetTypes} from "../../types/asset/AssetTypes";
import { ResponseType } from "../../types/storeTypes";
import { Node, lazyNode } from "../../types/asset/TreeTypes";

// Banner, Region, Store, Department, SCALE
//=================================================================================================
// POST Request
//=================================================================================================
const scaleHeadCells = [
    { id: "checkmark",        sorted: false, searchable: false, label: '',                    isShowing: true, mandatory: true},
    { id: 'ipAddress',        sorted: true,  searchable: true,  label: 'IP Address',          isShowing: true, mandatory: true},
    { id: 'hostname',         sorted: true,  searchable: true,  label: 'Hostname',            isShowing: true, mandatory: true},
    { id: 'scaleModel',       sorted: false, searchable: false, label: 'Scale Model',         isShowing: true, mandatory: true},
];

interface PostScaleProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    // scales: ScaleDevice[] | undefined
    setAssetScales: React.Dispatch<React.SetStateAction<ScaleDevice[]>>
    dept ?: lazyNode | Node
}

function PostScale(props : PostScaleProps) {
    interface PostScaleForm{
        selectStore: string, 
        selectDept: string,
        ManualAdd: boolean,
        ManualEntry: string
    }

    const { register, handleSubmit, reset, formState: {errors} } = useForm<PostScaleForm>();

    const stores = useAppSelector((state) => state.asset.stores);
    const depts = useAppSelector((state) => state.asset.depts);
    const lazyExpandedKeys = useAppSelector((state) => state.asset.lazyExpanded)
    
    const [selectedScales, setSelectedScales] = useState<string[]>([])
    const [unassignedScales, setUnassignedScales] = useState<ScaleDevice[]>([])
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const { title, openPopup, setOpenPopup, setAssetScales, dept } = props;       // Passed from AssetList.jsx
    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        selectStore: "", 
        selectDept: "",
        ManualAdd: false,
        ManualEntry: ""
    }
    // const newID = Date.now()
    // function onSuccess(response: ResponseType) {
    //     logger.info("Successfully fetched scales")
    // }
    // function onFail(response: ResponseType) {
    //     logger.error("Failed to fetch scales")
    // }
    // if (addCallback(callbacks, newID, "Fetch Scales", onSuccess, onFail)) {
        
    const [scales, setScales] = useState<ScaleDevice[]>([])
    useEffect(() => {
        dispatch(ScaleAPI.fetchScales(Date.now(), undefined, null, null, setScales))
    }, [])
    // }
    useEffect(() => {
    if (scales) {
        setUnassignedScales(scales.filter(scale => scale.deviceId != null && 
        (scale.storeId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID || 
            scale.deptId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID ||
            scale.storeId === null || 
            scale.deptId === null)))
    }
    },[scales])

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
        reset(defaultValues);
        setSelectedScales([])
    }

    const handleAdd = handleSubmit(async (data : PostScaleForm) => {    // When the Submit button is pressed

        setShowAlert([false, "Something went wrong!"])
        
        logger.info("(ScaleForm) POST request data: ", data)

        
        let storeID : string
        let departmentId : string
        if(dept != null){
            if(!(dept as lazyNode).path[2]){
                setShowAlert([true, "ERROR Parsing out Department/Store Info"])
                return
            }

            storeID = (dept as lazyNode).path[2]
            departmentId = dept.id
        }
        else{
            storeID = data.selectStore
            departmentId = data.selectDept
        }
        
        let department = depts[departmentId]
        let departmentProfileId = department != null ? department.defaultProfileId : AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID
        logger.info("(ScaleForm)" + storeID + "  " + departmentId)
        if(storeID == null || departmentId == null){
            setShowAlert([true, "ERROR Parsing out Department/Store Info"])
            return
        }
        async function touchScales(id :number, ipAddress : string, storeID: string){
            await dispatch(ScaleAPI.setScaleEnable(id, ipAddress, true, storeID, null, null, null))
            await dispatch(ScaleCtrlAPI.executeManualHeartBeat(id, ipAddress, storeID))
        }
        //Callback related
        async function onSuccess(response : ResponseType){
            // TODO - the execute manual heartbeats overload backend and result in other dispatches being timedout
            setOpenPopup(null);
            reset(defaultValues);
            const newID = Date.now()
            dispatch(ScaleAPI.fetchScales(newID+1, undefined, null, null, setAssetScales))
            await dispatch(AssetAPI.refreshLazyTree(Date.now(), lazyExpandedKeys)) // ensures refresh tree is set before all other apis
            if (data.ManualAdd) {
                dispatch(ScaleAPI.setScaleEnable(newID, data.ManualEntry, true, storeID, null, null, null))
                dispatch(ScaleCtrlAPI.executeManualHeartBeat(newID, data.ManualEntry, storeID))
            } else {
                unassignedScales.filter(scale => selectedScales.includes(scale.deviceId)).forEach(scale => {
                    touchScales(newID, scale.ipAddress, storeID)
                })
            }
            if(dept && dept.id){
                dispatch(setExpandedTreeItems([...store.getState().asset.expanded, dept.id]))
            }
            await dispatch(AssetAPI.refreshLazyTree(Date.now(), lazyExpandedKeys))
            setOpenPopup(null);
            setSelectedScales([]) 
        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add scale(s)!"])
        }

        if(data.ManualAdd === true){
            logger.info("(ScaleForm) Manually create/add scale : " + data.ManualEntry)

            const dataToAdd = {
                "deptId": departmentId,
                "ipAddress": data.ManualEntry,
                "storeId": storeID,
                "last24HourStatus": "XXXXXXXXXXXXXXXXXXXXXXXX",
                "profileId": departmentProfileId,
                "last24HourSync": "------------------------",
                "hostname": "Unknown",
                "enabled": false,
                "isPrimaryScale": false
            }

            const newID = Date.now()
            if(addCallback(callbacks, newID, "Manual Scale Add", onSuccess, onFail)){
                dispatch(ScaleAPI.postScale(newID, dataToAdd, storeID, null, null, null))
            }
        }
        else{

            if(selectedScales.length === 0){
                setShowAlert([true, "Please select a scale to add!"])
                return
            }

            const newID = Date.now()
            if(addCallback(callbacks, newID, "Assign Scale", onSuccess, onFail)){
                dispatch(ScaleAPI.setScaleAssignment(newID, selectedScales, storeID, storeID, departmentId, null, null, null))
                dispatch(AssetAPI.refreshLazyTree(newID,lazyExpandedKeys))
            }
        }
    })

    return (
        <>
            <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
                <DialogTitle>
                    <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                        {title}
                    </Typography>
                </DialogTitle>

                <DialogContent>
                    <Form className="form-control" key={1} onSubmit={handleAdd}>       
                        
                        {dept == null ?
                        <>
                            <FormSelect
                                required
                                label="Select Store: "
                                id="selectStore"
                                name="selectStore"
                                type="text"
                                placeholder="--Select a store--"
                                dataset={Object.values(stores)}
                                register={register}
                                objId="storeId"
                                objName="storeName"
                                disabled={false}
                                validation={{ required: "Please select a store to add the scale to" }}
                                error={errors.selectStore} 
                                onlyValue={undefined}
                            />

                            <FormSelect
                                    required
                                    label="Select Dept: "
                                    id="selectDept"
                                    name="selectDept"
                                    type="text"
                                    placeholder="--Select a department--"
                                    dataset={Object.values(depts)}
                                    register={register}
                                    objId="deptId"
                                    objName="deptName1"
                                    disabled={false}
                                    validation={{ required: "Please select a department to add the scale to" }}
                                    error={errors.selectDept} onlyValue={undefined}                            />
                        </>
                         : null}
                        
                        
                        <Typography variant="body1" component="div" style={{ flexGrow: 2 }}>
                            Unassigned Scales
                        </Typography>

                        <VirtualTable
                            dataSet={unassignedScales}
                            headCells={scaleHeadCells}
                            initialSortedBy={{ name: '' }}
                            dispatchedUrls={[ScaleAPI.fetchScales]}
                            maxHeight="300px"
                            dataIdentifier="deviceId"
                            setSelected={setSelectedScales}
                            saveKey="unassignedScalesTableHead" 
                            tableName={"UnassignedScalesTable"}
                        />

                        <HiddenManualEntry 

                            labelCheck="Manually add a scale"
                            label="Scale IP Address: "
                            id="manualScaleInput"
                            name="ManualEntry"
                            type="text"
                            placeholder="Enter a Scale IP Address"
                            register={register}
                            validation={{
                                required: "Please enter a scale IP address",
                                maxLength: { value: 15, message: "You exceeded the max scale IP length" }
                            }}
                            error={errors.ManualEntry} 
                            disableOther={undefined}
                        />

                        {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                        <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Add Scale(s) </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                    </Form>
                </DialogContent>
            </Dialog>
        </>
    )
}
//=================================================================================================
// DELETE Request
// TODO: Grab the NodeID and put it into the URL to choose which node is being deleted
//=================================================================================================
interface DeleteScaleProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    scaleProperties: ScaleDevice
    successCallback ?: (rsp : any) => void
    failCallback ?: (rsp : any) => void
}

function DeleteScale(props : DeleteScaleProps) {
    const { handleSubmit} = useForm();
    const { title, openPopup, setOpenPopup, scaleProperties, successCallback, failCallback} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    
    const {callbacks, addCallback} = useRspHandler();
    const roles = useAppSelector((state) => state.access.roles)
    const user = useAppSelector((state) => state.access.account)
    const domains = useAppSelector((state) => state.access.domains)
    const dispatch = useDispatch()

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null)
    }

    //Callback related
    function onSuccess(response : ResponseType){
		setOpenPopup(null)
        if(successCallback){
            successCallback(response)
        }
	}
	function onFail(response : ResponseType){
        logger.error("Delete Scale failed", response)
		response.response.errorDescription != null ? 
                setShowAlert([true, response.response.errorDescription]) : 
                setShowAlert([true, "Could not delete scale!"])
        if(failCallback){
            failCallback(response)
        }
	}

    const handleDelete = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(ScaleForm) DELETE request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        
        const newID = Date.now()
        if(addCallback(callbacks, newID, "Delete Scale", onSuccess, onFail)){
            dispatch(ScaleAPI.deleteScale(newID, scaleProperties.deviceId, scaleProperties.storeId, null, null, null))
            setOpenPopup(null)
        }
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle id="deleteScaleDialogForm">
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>
                    {/* TODO: Display error if the DELETE fails */}
                    <Form.Label id="confirmDelete">Are you sure you want to delete scale: <b style={{color : "#ba4735"}}>{scaleProperties.ipAddress}</b>? This will remove the scale from the database and you will need to manually add the scale back to the program.</Form.Label>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">

                    
                        <AccessControl
                            user={user}
                            requiredDomain={domains[scaleProperties.storeId]}
                            requiredRole={roles[ASSIGN_SCALES]}
                        >
                            <button className="formButton1" onClick={handleSubmit(handleDelete)} type="submit">
                                <Delete />
                                <span> Delete Scale </span>
                            </button>
                        </AccessControl>

                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>

                </Form>
            </DialogContent>
        </Dialog>
    )
}


//=================================================================================================
// Reassign Request
// Move the scale back to unassigned
//=================================================================================================
interface RemoveScaleProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<AssetTypes | null>>
    scaleProperties: ScaleDevice
    setScales?: React.Dispatch<React.SetStateAction<ScaleDevice[]>>
}
function RemoveScale(props : RemoveScaleProps) {
    const { handleSubmit} = useForm();
    const { title, openPopup, setOpenPopup, scaleProperties, setScales} = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    
    const {callbacks, addCallback} = useRspHandler();
    const roles = useAppSelector((state) => state.access.roles);
    const domains = useAppSelector((state) => state.access.account.domain);
    const user = useAppSelector((state) => state.access.account);
    const dispatch = useDispatch();

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(null);
    }

    //Callback related
    function onSuccess(response : ResponseType){
		setOpenPopup(null);
        if (setScales) {
            dispatch(ScaleAPI.fetchScales(Date.now(), undefined, null, null, setScales))
        }
	}
	function onFail(response : ResponseType){
        logger.info("Delete Scale failed", response)
		response.response.errorDescription != null ? 
                setShowAlert([true, response.response.errorDescription]) : 
                setShowAlert([true, "Could not delete scale!"])
	}

    const handleDelete = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(ScaleForm) DELETE request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        
        const newID = Date.now()
        if(addCallback(callbacks, newID, "Delete Scale", onSuccess, onFail)){
            dispatch(ScaleAPI.deleteScale(newID, scaleProperties.deviceId, scaleProperties.storeId, null, null, null))
            setOpenPopup(null);
        }
    }

    const handleRemove = async (data : {}) => {    // When the Submit button is pressed
        logger.info("(ScaleForm) REMOVE request data: ", data)
        setShowAlert([false, "Something went wrong!"])

        const newID = Date.now()
        const removedElement = scaleProperties.deviceId
        const parent = String(scaleProperties.storeId)
        if(addCallback(callbacks, newID, "Unassign Scale", onSuccess, onFail)){
            dispatch(ScaleAPI.setScaleAssignment(newID, [removedElement], parent, 
                                                    AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID, 
                                                    AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID, null, null, null))
        }
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            
            <DialogTitle id="deleteScaleDialogForm">
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleDelete)}>
                    {/* TODO: Display error if the DELETE fails */}
                    <Form.Label id="confirmDelete">Are you sure you want to remove this scale: <b style={{color : "#ba4735"}}>{scaleProperties.ipAddress}</b> from 
                    the department? This action will unassign the scale, <b>AND</b> reset the scale's current profile configuration. </Form.Label>

                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">

                    {/* Role 1002 (AssignScales) */}
                    <AccessControl
                        user={user}
                        requiredDomain={domains[scaleProperties.storeId]}
                        requiredRole={roles[ASSIGN_SCALES]}
                    >
                                <button className="formButton1" onClick={handleSubmit(handleRemove)} type="submit">
                                    <RemoveShoppingCartOutlined />
                                    <span> Remove Assignment </span>
                                </button>

                    </AccessControl>

                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>

                </Form>
            </DialogContent>
        </Dialog>
    )
}


interface AddScalesProps{
    scaleList : ScaleDevice[]
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
    parentSelectedScales : string[]
    setParentSelectedScales : React.Dispatch<React.SetStateAction<string[]>>
    setParentDepartment : React.Dispatch<React.SetStateAction<string | null>>
    setParentStore: null | React.Dispatch<React.SetStateAction<string | null>>
}
function AddScales(props : AddScalesProps){
    interface AddScalesForm{
        selectDept: string
        selectStore: string
    }

    const {scaleList, openPopup, setOpenPopup, parentSelectedScales, setParentSelectedScales, setParentDepartment, setParentStore} = props
    const { register, handleSubmit, formState: {errors} } = useForm<AddScalesForm>();

    const [showAlert, setShowAlert]                 = useState([false, "Something went wrong!"])    // Upload Error

    const [selectedScales, setSelectedScales] = useState(parentSelectedScales)    // Selected Scales
    const [additionalScales, setAdditionalScales] = useState(false)

    const stores = useAppSelector((state) => state.asset.stores);
    const depts = useAppSelector((state) => state.asset.depts);

    const handleCloseDialog = (event ?: any) => {
        setOpenPopup(false);
        setShowAlert([false, "Something went wrong!"])
        // setParentSelectedScales([])
        // setSelectedScales([])
        event.stopPropagation()
    }

    const handleAdd = handleSubmit((data : AddScalesForm) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);
        setParentSelectedScales(selectedScales)
        setParentDepartment(data.selectDept)
        if(setParentStore){
            setParentStore(data.selectStore)
        }
        
    })

    useEffect(() => {
        setSelectedScales(parentSelectedScales)
    }, [parentSelectedScales])

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {"Select Scales"}
                </Typography>
            </DialogTitle>

            <DialogContent>        
                <Form className="form-control" onSubmit={handleAdd}>
                    <div className="uploadFile">
                        <FormSelect
                            required
                            label="Select Store: "
                            id="selectStore"
                            name="selectStore"
                            type="text"
                            placeholder="--Select a store--"
                            dataset={Object.values(stores)}
                            register={register}
                            objId="storeId"
                            objName="storeName"
                            disabled={false}
                            error={errors.selectStore}
                            validation={undefined}
                            onlyValue={undefined}
                        />

                        <FormSelect
                            required
                            label="Select Dept: "
                            id="selectDept"
                            name="selectDept"
                            type="text"
                            placeholder="--Select a department--"
                            dataset={Object.values(depts)}
                            register={register}
                            objId="deptId"
                            objName="deptName1"
                            disabled={false}
                            error={errors.selectDept}
                            validation={undefined}
                            onlyValue={undefined}
                        />

                        <FormCheck type="checkbox" className="d-flex" style={{marginTop:"1rem"}}>
                            <FormCheck.Label className="customCheckbox"> 
                                <FormCheck.Input required id="asvb" className="me-2" defaultChecked={additionalScales} onClick ={() => {setAdditionalScales(!additionalScales)}}/>
                                Select Additional Scales
                            </FormCheck.Label>
                        </FormCheck>


                        {additionalScales ? 
                            <VirtualTable
                                tableName={"AdditionalScales"}
                                dataSet={scaleList}
                                useToolbar={false}
                                headCells={scaleHeadCells}
                                initialSortedBy={{ name: '' }}
                                dispatchedUrls={[]}
                                maxHeight="300px"
                                dataIdentifier="ipAddress"
                                selectedRows={selectedScales}
                                setSelected={setSelectedScales} 
                                saveKey={"AdditionalScaleTable"}
                            />
                        :null}
                        

                        {/* Error uploading file */}
                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

 

                        <div className="formButtons">
                            <button className="formButton1" disabled={false} onClick={handleAdd} type="submit">
                                <AddCircle />
                                <span> Done </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>     
                </Form>
            </DialogContent>
        </Dialog>
    );
}

export { PostScale, DeleteScale, RemoveScale, AddScales};