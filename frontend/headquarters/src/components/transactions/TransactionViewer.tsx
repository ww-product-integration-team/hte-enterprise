import { useState, useEffect } from 'react';
import { AddCircle, Help } from '@material-ui/icons';
import { useDispatch } from "react-redux"
import { Button, ButtonGroup, Form } from '@themesberg/react-bootstrap';
import format from 'date-fns/format';
import DatePicker from "react-datepicker";
import { HiddenManualEntry } from '../forms/FormHelper';
import { useForm } from 'react-hook-form';
import { AddScales } from '../forms/ScaleForm';
import { Alert, IconButton, ListItem, Tooltip, Typography } from '@mui/material';
import { FixedSizeList } from 'react-window';
import { ArrowDropDown, ArrowRight, Download, List, Paid, RemoveCircle, Scale } from '@mui/icons-material';
import { TransAPI } from '../api';
import { useRspHandler } from '../utils/ResponseProvider';
import VirtualTable, { HeadCellTypes } from '../common/virtualTable/VirtualTable';
import logger from '../utils/logger';
import { store, useAppSelector } from '../../state';
import { Department, Store } from '../../types/asset/AssetTypes';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { Service } from '../../types/config/services';
import { TransactionData, TransactionRequest } from '../../types/transaction/transactionTypes';
import { ResponseType } from '../../types/storeTypes';
import { AlertMessage, HideMessage, ShowErrorMessage, ShowWarningMessage } from '../../types/utils/utilTypes';

interface IPListProps {
    title: string
    data: string[]
    width?: number
    horzMargin?: string
    horzPadding?: string
    noDataMsg?: string
}
export const IPList = (props: IPListProps) => {
    const { title, data = [], width = 200, horzMargin = "1rem", horzPadding = "1rem", noDataMsg = "All Scales" } = props;

    function renderRow(props: { index: number, style: any }) {
        const { index, style } = props;

        return (
            <div style={style}>
                <ListItem style={{ padding: "0px" }} key={index} component="div" disablePadding>
                    <span style={{ width: "100%", textAlign: "center" }}>
                        <Typography style={{ width: "100%", textAlign: "center" }} noWrap={true}>
                            {data[index]}
                        </Typography>
                    </span>
                </ListItem>
            </div>
        );
    }

    return (
        <div style={{ backgroundColor: "#f2f2f2", marginLeft: horzMargin, marginRight: horzMargin, paddingLeft: horzPadding, paddingRight: horzPadding, borderRadius: "10px" }}>

            <Form.Label style={{ width: "100%", textAlign: "center" }}><b>{title}</b></Form.Label>

            {data.length === 0 ? <label className="noDataLabel">{noDataMsg}</label> :
                <FixedSizeList
                    height={136}
                    itemSize={20}
                    width={width}
                    itemCount={data.length}
                    overscanCount={5}
                >
                    {renderRow}
                </FixedSizeList>
            }
        </div>
    );
};

interface TransactionViewerProps {
    title: JSX.Element
    scaleList: ScaleDevice[]
    transactionService?: Service
    download: (file: any) => void
    transHeadCells: HeadCellTypes[]
    mode?: "STORE" | "HQ"
}

const TransactionViewer = (props: TransactionViewerProps) => {
    interface TransactionViewerForm {
        ManualAdd: boolean
        ManualEntry: string
    }

    const { title, scaleList, transactionService, download, transHeadCells, mode = "STORE" } = props
    const { register, handleSubmit, formState: { errors } } = useForm<TransactionViewerForm>();

    const [startDate, setStartDate] = useState(Date.now() - 8.64e+7);     // Start date
    const [endDate, setEndDate] = useState(Date.now());               // End date
    const [selectedScales, setSelectedScales] = useState<string[]>([])    // Selected Scales
    const [selectedStore, setSelectedStore] = useState<string | null>(null)
    const [selectedDept, setSelectedDept] = useState<string | null>(null)
    const [hideTable, setHideTable] = useState(true)
    const [openAdd, setOpenAdd] = useState(false)
    const transactions = useAppSelector((state) => state.asset.transactions);

    let storeObj: Store | null = null
    if (selectedStore != null) {
        storeObj = store.getState().asset.stores[selectedStore]
    }
    let deptObj: Department | null = null
    if (selectedDept != null) {
        deptObj = store.getState().asset.depts[selectedDept]
    }

    let transactionObject: TransactionData | null = null
    if (transactions && transactionService) {
        transactionObject = transactions[transactionService.databaseId];
    }

    const [statusAlert, setStatusAlert] = useState<AlertMessage>(HideMessage)

    // Redux
    const dispatch = useDispatch()
    const { callbacks, addCallback } = useRspHandler();

    const handleColor = () => {
        return "text-success"
    };

    useEffect(() => {
        if (!transactionUrl) {
            setStatusAlert(ShowWarningMessage("Could not find endpoint for transactions service."))
        }
    }, [])

    // Grab UI Manager URL
    let transactionUrl: string | null = null
    if (transactionService && transactionService.url && transactionService.url !== "") {
        transactionUrl = transactionService.url
    }

    function formatSubmission(data: TransactionViewerForm): TransactionRequest | void {
        setStatusAlert(HideMessage())
        let parsedPLUs: number[] = []
        if (data.ManualAdd && data.ManualEntry) {
            data.ManualEntry.split(",").forEach(element => {
                if (element !== "") {
                    const item = parseInt(element)
                    if (!item) {
                        setStatusAlert(ShowErrorMessage("Invalid value in list of PLUs"))
                        return
                    }
                    parsedPLUs.push(item)
                }
            });
        }


        if (startDate > endDate) {
            setStatusAlert(ShowErrorMessage("Start date must be before End date"))
            return
        }
        /*`
                if(endDate > Date.now()){
                    setStatusAlert(ShowErrorMessage("End date cannot be in the future"))
                    return
                }
        */
        if (!transactionUrl) {
            setStatusAlert(ShowErrorMessage("Cannot find Transaction Endpoint!"))
            return
        }

        if (statusAlert.show) {
            return
        }

        try {
            const fileInfo: TransactionRequest = {
                transData: true,
                url: transactionUrl,
                plus: parsedPLUs,
                scaleIps: selectedScales,
                startDate: format(startDate, 'MM-dd-yyyy'),
                endDate: format(endDate, 'MM-dd-yyyy'),
                storeId: selectedStore ? selectedStore : undefined,
                deptId: selectedDept ? selectedDept : undefined,
                filename: "TransData-" + format(startDate, "MMddyy") + "-" + format(endDate, "MMddyy") + ".xlsx"
            }


            return fileInfo
        }
        catch (e) {
            setStatusAlert(ShowErrorMessage("Could not parse transaction submission. If issue persists please contact HT Support."))
            return
        }

    }

    function parsePrice(price: number, isWeight = false) {
        let pricePerLb: string | null = null
        let str: string | null = null
        try {
            pricePerLb = (price / 100).toFixed(2)
            //Format with columns
            str = pricePerLb.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        catch (e) {
            logger.error("Error parsing price! ", price)
        }
        if (isWeight) {
            return pricePerLb ? str + "lbs" : "Error"
        }
        else {
            return pricePerLb ? "$" + str : "Error"
        }
    }

    function parseStore(stores: string[]) {
        if (!stores || stores.length === 0) { return "All Stores" }

        storeObj = store.getState().asset.stores[stores[0]]
        if (!storeObj) {
            return "Unknown"
        }

        return storeObj.storeName
    }

    function parseDept(depts: string[]) {
        if (!depts || depts.length === 0) { return "All Departments" }

        deptObj = store.getState().asset.depts[depts[0]]
        if (!deptObj) {
            return "Unknown"
        }

        return deptObj.deptName1
    }

    function onSuccess(response: ResponseType) {
        logger.info("Successfully fetched transactions")
    }

    function onFail(response: ResponseType) {
        logger.error("Error fetching transactions")
    }

    const handleView = handleSubmit((data: TransactionViewerForm) => {
        const fileInfo = formatSubmission(data)

        if (fileInfo && transactionService) {
            const id = Date.now()
            if (addCallback(callbacks, id, "Fetch Transactions", onSuccess, onFail, { requestMsg: "default", successMsg: "Successfully fetched transactions", failMsg: "default" })) {
                setHideTable(false)
                dispatch(TransAPI.fetchTransactions(id, fileInfo, transactionService.databaseId))
            }

        }

    })

    const handleDownload = (data) => {
        const fileInfo = formatSubmission(data)
        if (fileInfo) {
            download(fileInfo)
        }
    }


    return (
        <div style={{ marginTop: "1rem" }}>
            <Form className="form-control" onSubmit={handleView}>

                <div style={{ display: 'grid', gridTemplateColumns: "repeat(6, 1fr)", gap: "10px" }}>

                    <div style={{ gridColumn: "1/3", gridRow: "1" }}>
                        {title}
                    </div>

                    <div style={{ display: 'flex', width: '100%', gridColumn: "1/4", gridRow: "2" }}>

                        <div>
                            <Form.Label style={{ marginTop: "1rem" }}>Start Date:</Form.Label>
                            <DatePicker
                                className="form-control"
                                dateFormat="MMMM d, yyyy"
                                timeClassName={handleColor}
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                            />
                        </div>

                        <div style={{ marginTop: "0.5rem", marginLeft: "1rem" }}>
                            <Form.Label>End Date:</Form.Label>
                            <Tooltip className="info" title={<Typography fontSize={12}>To see all of today's transactions, set this date to one day in the future.</Typography>}>
                                <IconButton aria-label="enableAccount" disableRipple>
                                    <Help />
                                </IconButton>
                            </Tooltip>
                            <DatePicker
                                className="form-control"
                                dateFormat="MMMM d, yyyy"
                                timeClassName={handleColor}
                                selected={endDate}
                                onChange={(date) => setEndDate(date)}
                            />
                        </div>

                        <div style={{ marginLeft: "1rem", paddingTop: "1.2rem", alignSelf: "center" }}>
                            <HiddenManualEntry
                                labelCheck="Specify PLUs"
                                label=""
                                id="manualPLUs"
                                name="ManualEntry"
                                type="text"
                                placeholder="Enter a List of PLUs: 123,452,32"
                                register={register}
                                validation={{}}
                                padding="0rem 0rem 0.5rem 0rem"
                                error={errors.ManualEntry}
                                disableOther={undefined}
                            />
                        </div>

                    </div>

                    <div style={{ marginLeft: "auto", gridColumn: "4", gridRow: "1/3", display: "flex", flexDirection: "column" }}>
                        <ButtonGroup>
                            <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(e) => {
                                setOpenAdd(true)
                            }}>
                                <AddCircle />
                                Select Scales
                            </Button>
                        </ButtonGroup>

                        <AddScales
                            scaleList={scaleList}
                            openPopup={openAdd}
                            setOpenPopup={setOpenAdd}
                            parentSelectedScales={selectedScales}
                            setParentSelectedScales={setSelectedScales}
                            setParentDepartment={setSelectedDept}
                            setParentStore={mode === "STORE" ? null : setSelectedStore}
                        />

                        {selectedScales.length > 0 || selectedDept || selectedStore ?
                            <ButtonGroup style={{ marginTop: "1rem" }}>
                                <Button id="actionBar2" variant="filled" style={{ backgroundColor: "#FF4D4D" }} size="sm" onClick={(e) => {
                                    setSelectedScales([])
                                    setSelectedDept(null)
                                    setSelectedStore(null)
                                }}>
                                    <RemoveCircle />
                                    Clear
                                </Button>
                            </ButtonGroup>
                            : null}

                        <Form.Label><b>Department: {deptObj ? deptObj.deptName1 : "All"}</b></Form.Label>

                        {mode === "STORE" ? null :
                            <Form.Label><b>Store: {storeObj ? storeObj.storeName : "All"}</b></Form.Label>
                        }

                    </div>

                    <div style={{ gridColumn: "5", gridRow: "1/3", alignSelf: "center" }}>
                        <IPList
                            title="Selected Scales:"
                            data={selectedScales}
                        />
                    </div>

                    <div style={{ gridColumn: "6", gridRow: "1/3", alignSelf: "end", justifySelf: "right", display: "flex", flexDirection: "column" }}>
                        {/* <AccessControl
                            userPermissions={userPermissions}
                            userRoles={assignedDomain}
                            allowedPermissions={[0,4000]}
                            allowedRoles={DEFAULT_ADMIN_DOMAIN_ID}                
                        > */}
                        <Button id="actionBar1" variant="outline-primary" size="sm" disabled={!transactionUrl} onClick={handleView}>
                            <List style={{ marginRight: '8px' }} />
                            View
                        </Button>

                        <Button id="actionBar1" variant="outline-primary" size="sm" style={{ marginTop: "1rem" }} disabled={!transactionUrl} onClick={handleSubmit(handleDownload)}>
                            <Download style={{ marginRight: '8px' }} />
                            Download
                        </Button>

                        {transactionService && transactionObject ?
                            <Button className="minimizeButton" variant="link"
                                onClick={(e) => {
                                    setHideTable(!hideTable)
                                }}>

                                {hideTable ?
                                    <>
                                        <ArrowDropDown />
                                        Show Table
                                    </>
                                    :
                                    <>
                                        <ArrowRight />
                                        Hide Table
                                    </>

                                }

                            </Button>
                            : null

                        }


                        {/* </AccessControl>  */}
                    </div>

                    <div style={{ gridColumn: "1/7", gridRow: "3" }}>
                        {statusAlert.show ? <Alert className="m-1" variant="filled" severity={statusAlert.severity}>{statusAlert.message}</Alert> : null}
                    </div>

                </div>

                {!hideTable && transactionService && transactionObject ?
                    <>
                        <Form.Label style={{ fontSize: "1.25rem", marginBottom: "0rem" }}>Currently Viewing:</Form.Label>
                        <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', borderTop: "5px solid #66799e", paddingTop: "1rem", marginBottom: "1rem" }}>

                            <div style={{ alignSelf: "center", display: "flex", flexDirection: "column" }}>
                                <Form.Label style={{ textAlign: "left", color: "#203140" }}><b>From:</b> <b style={{ fontWeight: 500 }}>{transactionObject.start}</b></Form.Label>
                                <Form.Label style={{ textAlign: "left", color: "#203140" }}><b>To:</b> <b style={{ fontWeight: 500 }}>{transactionObject.end}</b></Form.Label>
                                <Form.Label style={{ textAlign: "left", color: "#203140" }}><b>Span:</b> <b style={{ fontWeight: 500 }}>{transactionObject.span}</b></Form.Label>
                                <Form.Label style={{ textAlign: "left", color: "#203140" }}><b>Stores:</b> <b style={{ fontWeight: 500 }}>{parseStore(transactionObject.stores)}</b></Form.Label>
                                <Form.Label style={{ textAlign: "left", color: "#203140" }}><b>Departments:</b> <b style={{ fontWeight: 500 }}>{parseDept(transactionObject.depts)}</b></Form.Label>
                            </div>

                            <IPList
                                title="Selected Scales:"
                                data={transactionObject.scales}
                                width={150}
                                horzPadding="0px"
                            />

                            <IPList
                                title="Selected PLUs:"
                                data={transactionObject.plus ? transactionObject.plus : []}
                                horzPadding="0px"
                                noDataMsg="All PLUs"
                            />

                            <div style={{ alignSelf: "center", display: "flex", flexDirection: "column" }}>
                                <Form.Label style={{ textAlign: "center", color: "#203140", paddingLeft: "1rem", paddingRight: "1rem" }}><Paid /> <b>Total Price:</b></Form.Label>
                                <Form.Label style={{ textAlign: "center", fontSize: "1.5rem" }}>{parsePrice(transactionObject.totalPrice)}</Form.Label>
                            </div>

                            <div style={{ alignSelf: "center", display: "flex", flexDirection: "column" }}>
                                <Form.Label style={{ textAlign: "center", color: "#203140", marginLeft: "1rem", paddingLeft: "1rem", paddingRight: "1rem" }}><Scale /> <b>Total Weight:</b></Form.Label>
                                <Form.Label style={{ textAlign: "center", fontSize: "1.5rem" }}>{parsePrice(transactionObject.totalWeight, true)}</Form.Label>
                            </div>
                        </div>

                        <VirtualTable
                            tableName={"virtualTransTable_" + transactionService.databaseId}
                            dataSet={transactionObject.list}
                            headCells={transHeadCells}
                            initialSortedBy={{ scaleId: "" }}
                            dispatchedUrls={[]}
                            dataIdentifier="TransID"
                            saveKey="transHeadCellsKey"
                        />
                    </>
                    : null}

            </Form>
        </div>
    );
}

export default TransactionViewer;