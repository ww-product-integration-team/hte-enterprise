import { useState} from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import { Block, Download} from '@mui/icons-material';
import { Form } from '@themesberg/react-bootstrap';
import DatePicker from "react-datepicker";
import VirtualTable from '../common/virtualTable/VirtualTable';
import { FormInput } from '../forms/FormHelper';
import format from 'date-fns/format';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { TransactionRequest } from '../../types/transaction/transactionTypes';


const scaleHeadCells = [
    { id: "checkmark",        sorted: false, searchable: false, label: '',                    isShowing: true, mandatory: true},
    { id: 'ipAddress',        sorted: true,  searchable: true,  label: 'IP Address',          isShowing: true, mandatory: true},
    { id: 'hostname',         sorted: true,  searchable: true,  label: 'Hostname',            isShowing: true, mandatory: true},
    { id: 'scaleModel',       sorted: false, searchable: false, label: 'Scale Model',         isShowing: true, mandatory: true},
];

interface DownloadTransactionFormProps{
    transactionUrl : string
    storeScales : ScaleDevice[]
    openPopup : boolean
    setOpenPopup : React.Dispatch<React.SetStateAction<boolean>>
    download : (file : any) => void
}

export default function DownloadTransactionForm(props: DownloadTransactionFormProps){
    const {transactionUrl, storeScales, openPopup, setOpenPopup, download} = props
    const { register, handleSubmit, formState: {errors} } = useForm();

    const [showAlert, setShowAlert]                 = useState([false, "Something went wrong!"])    // Upload Error

    const [startDate, setStartDate] = useState(Date.now() - 8.64e+7);     // Start date
    const [endDate, setEndDate] = useState(Date.now());               // End date
    const [selectedScales, setSelectedScales] = useState<string[]>([])    // Selected Scales

    const handleCloseDialog = (event : any) => {
        setOpenPopup(false);
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
    }

    const handleDownload = (data : any) => {
        setShowAlert([false, "Something went wrong!"])
        let parsedPLUs : number[] = []
        data.enterPLUs.split(",").forEach(element => {
            if(element !== ""){
                const item = parseInt(element)
                if(!item){
                    setShowAlert([true, "Invalid value in list of PLUs"])
                    return
                }
                parsedPLUs.push(item)
            }
            
        });

        if(startDate > endDate){
            setShowAlert([true, "Start date must be before End date"])
            return
        }

        if(endDate > Date.now()){
            setShowAlert([true, "End date cannot be in the future"])
            return
        }

        try{
            const fileInfo : TransactionRequest = {
                transData: true, 
                url: transactionUrl,
                plus: parsedPLUs,
                scaleIps: selectedScales,
                startDate: format(startDate, 'MM-dd-yyyy'),
                endDate: format(endDate, 'MM-dd-yyyy'),
                filename: "TransData-" + format(startDate, "MMddyy")+"-"+format(endDate, "MMddyy")+".xlsx"
            }

            
            download(fileInfo)
            setOpenPopup(false)
        }
        catch(e){
            setShowAlert([true, "Could not download file!, If issue persists please contact HT Support"])
            return
        }
    }

    const handleColor = () => {
        return "text-success"
    };

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {"Download Transactions"}
                </Typography>
            </DialogTitle>

            <DialogContent>        
                <Form className="form-control" onSubmit={handleSubmit(handleDownload)}>
                    <div className="uploadFile">

                    <Form.Label>Start Date:</Form.Label>
                        <DatePicker 
                            className="form-control" 
                            dateFormat="MMMM d, yyyy"
                            timeClassName={handleColor}
                            selected={startDate} 
                            onChange={(date) => setStartDate(date)} 
                        />
   
                        <Form.Label style={{marginTop:'1rem'}}>End Date:</Form.Label>
                        <DatePicker 
                            className="form-control" 
                            dateFormat="MMMM d, yyyy"
                            timeClassName={handleColor}
                            selected={endDate} 
                            onChange={(date) => setEndDate(date)} 
                        />

                        <Form.Label style={{marginTop:'1rem'}}>Select Scales (Optional) </Form.Label>
                        <VirtualTable
                            tableName = "transactionFormTable"
                            saveKey='transactionFormTable'
                            dataSet={storeScales}
                            useToolbar={false}
                            headCells={scaleHeadCells}
                            initialSortedBy={{name: '' }}
                            dispatchedUrls = {[]}
                            maxHeight = "300px"
                            dataIdentifier="ipAddress"
                            setSelected = {setSelectedScales}
                        />

                        <Form.Label style={{marginTop:'1rem'}}>List of PLUs (Optional)</Form.Label>
                        <FormInput
                            label=""
                            id="enterPLUs"
                            name="enterPLUs"
                            type="text"
                            placeholder="Ex: 1,34, 423"
                            register={register}
                            validation={{}}
                            error={errors.enterPLUs}
                        />
                        

                        {/* Error uploading file */}
                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

 

                        <div className="formButtons">
                            <button className="formButton1" disabled={false} onClick={handleSubmit(handleDownload)} type="submit">
                                <Download />
                                <span> Download File </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>     
                </Form>
            </DialogContent>
        </Dialog>
    );
}
    