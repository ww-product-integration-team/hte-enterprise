import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Breadcrumb, Form } from '@themesberg/react-bootstrap';
import { faHome } from '@fortawesome/free-solid-svg-icons';

import { useFileDownloader } from "../scaleProfiles/fileDownload/FileDownloader";
import ApartmentIcon from '@mui/icons-material/Apartment';
import TransactionViewer from './TransactionViewer';
import { Store as StoreIcon } from '@mui/icons-material';
import { Store } from '../../types/asset/AssetTypes';
import { HeadCellTypes } from "../common/virtualTable/VirtualTable";
import { TransactionRequest } from "../../types/transaction/transactionTypes";
import { IconButton, Tooltip, Typography } from '@mui/material';
import { Help } from '@mui/icons-material';
import AccessControl from '../common/AccessPermissions';
import * as AccessActions from '../../state/actions/accessActions';
import { useAppSelector } from '../../state';
import { useEffect, useState } from 'react';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { useDispatch } from 'react-redux';
import { ScaleAPI } from '../api';

// --------- End Temp Data ---------

// Column Names  
const transHeadCells : HeadCellTypes[] = [
    { id: "scaleId",     sorted: true, searchable: true, label: 'IP Address',                     isShowing: true, mandatory: true},
    { id: "trDtTm",      sorted: true, searchable: false, label: 'Transaction Date Time',          isShowing: true, mandatory: true},
    { id: "trTtVal",     sorted: true, searchable: false, label: 'Total Monetary Value',           isShowing: true, mandatory: true},    
    { id: "trOTtVl",     sorted: false, searchable: false, label: 'Old Total Monetary Value',       isShowing: false, mandatory: false},    
    { id: "deptNum",     sorted: false, searchable: false, label: 'Department Number',              isShowing: false, mandatory: false},    
    { id: "trPrNum",     sorted: true, searchable: true, label: 'Product Number',                 isShowing: true, mandatory: false},    
    { id: "trPrTyp",     sorted: false, searchable: false, label: 'Product Type',                   isShowing: false, mandatory: false},    
    { id: "trPrDes",     sorted: false, searchable: true, label: 'Description',                    isShowing: false, mandatory: false},    
    { id: "trTare",      sorted: false, searchable: false, label: 'Tare',                           isShowing: true, mandatory: true},
    { id: "trPrTare",    sorted: false, searchable: false, label: 'Proportional Tare',              isShowing: false, mandatory: false},    
    { id: "trByCount",   sorted: false, searchable: false, label: 'Product By Count',               isShowing: false, mandatory: false},    
    { id: "trFnlNtWt",   sorted: false, searchable: false, label: 'Final Net Weight',               isShowing: false, mandatory: false},    
    { id: "trFxWt",      sorted: false, searchable: false, label: 'Product Fixed Weight',           isShowing: false, mandatory: false},
    { id: "trLab1",      sorted: false, searchable: false, label: 'Label Type 1',                   isShowing: false, mandatory: false},
    { id: "trLab2",      sorted: false, searchable: false, label: 'Label Type 2',                   isShowing: false, mandatory: false},
    { id: "trLab3",      sorted: false, searchable: false, label: 'Label Type 3',                   isShowing: false, mandatory: false},
    { id: "trLab4",      sorted: false, searchable: false, label: 'Label Type 4',                   isShowing: false, mandatory: false},
    { id: "trOpNum",     sorted: false, searchable: false, label: 'Operator Number',                isShowing: true, mandatory: false},    
    { id: "trOpRFID",    sorted: false, searchable: false, label: 'Operator RFID',                  isShowing: false, mandatory: false},    
    { id: "trOLab1",     sorted: false, searchable: false, label: 'Old Label 1',                    isShowing: false, mandatory: false},    
    { id: "trOLab2",     sorted: false, searchable: false, label: 'Old Label 2',                    isShowing: false, mandatory: false},    
    { id: "trOLab3",     sorted: false, searchable: false, label: 'Old Label 3',                    isShowing: false, mandatory: false},    
    { id: "trOLab4",     sorted: false, searchable: false, label: 'Old Label 4',                    isShowing: false, mandatory: false},    
    { id: "trGrNm1",     sorted: false, searchable: false, label: 'Graphic Name 1',                 isShowing: false, mandatory: false},    
    { id: "trGrNm2",     sorted: false, searchable: false, label: 'Graphic Name 2',                 isShowing: false, mandatory: false},    
    { id: "trGrNm3",     sorted: false, searchable: false, label: 'Graphic Name 3',                 isShowing: false, mandatory: false},    
    { id: "trGrNm4",     sorted: false, searchable: false, label: 'Graphic Name 4',                 isShowing: false, mandatory: false},    
    { id: "trOGrNm1",    sorted: false, searchable: false, label: 'Old Graphic Name 1',             isShowing: false, mandatory: false},    
    { id: "trOGrNm2",    sorted: false, searchable: false, label: 'Old Graphic Name 2',             isShowing: false, mandatory: false},    
    { id: "trOGrNm3",    sorted: false, searchable: false, label: 'Old Graphic Name 3',             isShowing: false, mandatory: false},    
    { id: "trOGrNm4",    sorted: false, searchable: false, label: 'Old Graphic Name 4',             isShowing: false, mandatory: false},    
    { id: "transID",     sorted: true, searchable: false, label: 'Transaction ID',                 isShowing: false, mandatory: false},    
    { id: "trScID",      sorted: false, searchable: false, label: 'Scale ID',                       isShowing: false, mandatory: false},
    { id: "trID",        sorted: false, searchable: false, label: 'Transaction ID (Scale)',         isShowing: false, mandatory: false},
    { id: "ttnum",       sorted: false, searchable: false, label: 'Transaction Type Number',        isShowing: false, mandatory: false},
    { id: "trGrp",       sorted: false, searchable: false, label: 'Transaction Group',              isShowing: false, mandatory: false},
    { id: "trVoid",      sorted: false, searchable: false, label: 'Void Flag',                      isShowing: true, mandatory: false},
    { id: "trAPrdt",     sorted: false, searchable: false, label: 'Affects Production',             isShowing: false, mandatory: false},    
    { id: "trRdClr",     sorted: false, searchable: false, label: 'Read Clear Flag',                isShowing: false, mandatory: false},    
    { id: "trNmPck",     sorted: false, searchable: false, label: 'Number Packages',                isShowing: false, mandatory: false},    
    { id: "trSecLb",     sorted: false, searchable: false, label: 'Used Security Label',            isShowing: false, mandatory: false},    
    { id: "trCOOLF",     sorted: false, searchable: false, label: 'COOL Flag',                      isShowing: false, mandatory: false},    
    { id: "trClNum",     sorted: false, searchable: false, label: 'Report Class',                   isShowing: false, mandatory: false},    
    { id: "trPrPlT",     sorted: false, searchable: false, label: 'Planning Type',                  isShowing: false, mandatory: false},    
    { id: "trPrMod",     sorted: false, searchable: false, label: 'Price Modifier',                 isShowing: false, mandatory: false},    
    { id: "trBarPref",   sorted: false, searchable: false, label: 'Barcode Prefix',                 isShowing: false, mandatory: false},    
    { id: "trBarNum",    sorted: false, searchable: false, label: 'Barcode Number',                 isShowing: false, mandatory: false},    
    { id: "trUnPr",      sorted: false, searchable: false, label: 'Unit Price',                     isShowing: true, mandatory: false},
    { id: "trExPr",      sorted: false, searchable: false, label: 'Exception Price',                isShowing: false, mandatory: false},
    { id: "trFnlPr",     sorted: false, searchable: false, label: 'Final Price',                    isShowing: false, mandatory: false},    
    { id: "trFnlTare",   sorted: false, searchable: false, label: 'Final Tare',                     isShowing: false, mandatory: false},    
    { id: "trSfLfD",     sorted: false, searchable: false, label: 'Shelf Life Days',                isShowing: false, mandatory: false},    
    { id: "trSfLfH",     sorted: false, searchable: false, label: 'Shelf Life Hours',               isShowing: false, mandatory: false},    
    { id: "trSfLfM",     sorted: false, searchable: false, label: 'Shelf Life Minutes',             isShowing: false, mandatory: false},    
    { id: "trPrLfD",     sorted: false, searchable: false, label: 'Product Life Days',              isShowing: false, mandatory: false},    
    { id: "trPrLfH",     sorted: false, searchable: false, label: 'Product Life Hours',             isShowing: false, mandatory: false},    
    { id: "trPrLfM",     sorted: false, searchable: false, label: 'Product Life Minutes',           isShowing: false, mandatory: false},    
    { id: "trOpName",    sorted: false, searchable: false, label: 'Operator Name',                  isShowing: false, mandatory: false},    
    { id: "trOpChg",     sorted: false, searchable: false, label: 'Operator Change Type',           isShowing: false, mandatory: false},    
    { id: "trOBarDig",   sorted: false, searchable: false, label: 'Old Barcode Digit',              isShowing: false, mandatory: false},    
    { id: "trOUnPr",     sorted: false, searchable: false, label: 'Old Unit Price',                 isShowing: false, mandatory: false},    
    { id: "trOFnPr",     sorted: false, searchable: false, label: 'Old Final Price',                isShowing: false, mandatory: false},    
    { id: "trOTare",     sorted: false, searchable: false, label: 'Old Tare',                       isShowing: false, mandatory: false},    
    { id: "trOByCount",  sorted: false, searchable: false, label: 'Old By Count',                   isShowing: false, mandatory: false},    
    { id: "trOFxWt",     sorted: false, searchable: false, label: 'Old Fixed Weight',               isShowing: false, mandatory: false},    
    { id: "trOSfLfD",    sorted: false, searchable: false, label: 'Old Shelf Life Days',            isShowing: false, mandatory: false},    
    { id: "trOSfLfH",    sorted: false, searchable: false, label: 'Old Shelf Life Hours',           isShowing: false, mandatory: false},    
    { id: "trOSfLfM",    sorted: false, searchable: false, label: 'Old Shelf Life Minutes',         isShowing: false, mandatory: false},    
    { id: "trOPrLfD",    sorted: false, searchable: false, label: 'Old Product Life Days',          isShowing: false, mandatory: false},    
    { id: "trOPrLfH",    sorted: false, searchable: false, label: 'Old Product Life Hours',         isShowing: false, mandatory: false},    
    { id: "trOPrLfM",    sorted: false, searchable: false, label: 'Old Product Life Minutes',       isShowing: false, mandatory: false},    
    { id: "trCOTxt",     sorted: false, searchable: false, label: 'COOL Text',                      isShowing: false, mandatory: false},    
    { id: "trCOTrTxt",   sorted: false, searchable: false, label: 'COOL Tracking Text',             isShowing: false, mandatory: false},    
    { id: "trUTNum",     sorted: false, searchable: false, label: 'Cutting Test Number',            isShowing: false, mandatory: false},    
    { id: "trUTPercentOfPrimalWeight",  sorted: false, searchable: false, label: 'Cutting Test % Primal Weight',            isShowing: false, mandatory: false},    
    { id: "trUTPercentOfPrimalCost",    sorted: false, searchable: false, label: 'Cutting Test % Primal Cost',              isShowing: false, mandatory: false},
    { id: "trModeChange",               sorted: false, searchable: false, label: 'Mode Change',                             isShowing: false, mandatory: false},
    { id: "trSweetheartWeight",         sorted: false, searchable: false, label: 'Sweetheart Weight',                       isShowing: false, mandatory: false},
    
    // { id: "id", narrow: false, disablePadding: false, label: 'ID', mandatory: false },   // can use this to filter for Region / Banner this store belongs to
];


const Transactions = (props : any) => {

    //HQ Data
    
    const hqServices = useAppSelector((state) => state.config.services);
    const stores = useAppSelector((state) => state.asset.stores);
    const transactionService = hqServices.find(service => service.serviceType === "UI_MANAGER")
    const user = useAppSelector((state) => state.access.account);
    const appRoles = useAppSelector((state) => state.access.roles);
    const appDomain = useAppSelector((state) => state.access.domains)
    const [scales, setScales] = useState<ScaleDevice[]>([])
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(ScaleAPI.fetchScales(Date.now(), undefined, null, null, setScales))
    }, [])
    let allScales = scales.filter(scale => scale.deviceId)
    // Redux

    const [downloadFile, downloaderComponentUI] = useFileDownloader();
    const download = (file : TransactionRequest) => {
        if(downloadFile instanceof Function){
            downloadFile(file);
        }
    };

    function getStoreTransactionView(store : Store){
        if(!store || !store.services) return null
        const storeTransactionService = store.services.find(service => service.serviceType === "UI_MANAGER")
        if (storeTransactionService) {
            let storeScales = Object.values(allScales).filter((scale) => scale.storeId === store.storeId)
            return(
                <TransactionViewer
                    title={<Form.Label style={{fontSize:"1.5rem"}}><StoreIcon fontSize='large'/> {store.storeName} Transactions:</Form.Label>}
                    scaleList={storeScales}
                    transactionService={storeTransactionService}
                    download={download}
                    transHeadCells={transHeadCells}
                />
            )
        }
    }

    return (
        <>
        <AccessControl
            user = {user}
            requiredRole = {appRoles[AccessActions.DOWNLOAD_TRANSACTIONS]}
            pageRequest={true}
        >
        <div className="mb-4 mb-md-0">
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Transactions</Breadcrumb.Item>
            </Breadcrumb>

            <h2>Transactions</h2>
            <p className="mb-4">Download and view your transactions from multiple hubs.</p>


            <TransactionViewer
                title={<Form.Label style={{fontSize:"1.5rem"}}><ApartmentIcon fontSize='large'/> Headquarter Transactions:</Form.Label>}
                scaleList={allScales}
                transactionService={transactionService}
                download={download}
                transHeadCells={transHeadCells}
                mode="HQ"
            />

            {Object.values(stores).map((store,index) =>(
                getStoreTransactionView(store)
            ))}

            

        </div>
        {downloaderComponentUI}
        </AccessControl>
        </>   
    );
}

export default Transactions;

