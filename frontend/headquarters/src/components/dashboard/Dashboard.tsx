import { useEffect, useState, useMemo } from "react";
import { Col, Row, Card } from '@themesberg/react-bootstrap';
import { CounterWidgetSelect, CounterWidget, CircleChartWidget, SimpleScaleList} from "./widgets/Widgets";
import {useDispatch } from "react-redux"
import { AccessActions, SocketActions} from "../../state/index"
import "../styles/volt.scss";
import "../styles/index.css";
import ScaleIcon, { LargeBannerIcon, LargeRegionIcon, LargeStoreIcon} from '../icons/ScaleIcon';
import {DeptAPI, StoreAPI, ProfileAPI, ConfigAPI, AssetAPI} from '../api/index';
import DepartmentIcon from '@material-ui/icons/LocalGroceryStoreOutlined';
import StoreIcon from '@material-ui/icons/Store';
import {ScaleSyncGraph} from './widgets/ScalesValuesChart';
import { ConnectedTv } from "@mui/icons-material";
// import logger from "../utils/logger";
import {useAppSelector} from "../../state/store"
import moment from 'moment';
import Alert from '@mui/material/Alert';
import VirtualTable, { HeadCellTypes} from "../common/virtualTable/VirtualTable";
import { Banner, Department, Region, Store } from "../../types/asset/AssetTypes";
import { ScaleDevice, SyncData } from "../../types/asset/ScaleTypes";
// import SockJsClient from 'react-stomp';
// import { notify } from "reapop";
import { format } from "date-fns";
import { useRspHandler } from "../utils/ResponseProvider";
import { setDepartmentList } from "../../state/actions/assetActions";
import { sortIPv4 } from "../utils/utils";
import { CircularProgress } from "@mui/material";
import { Typography } from "@material-ui/core";

// If we wanted to name the tab at the top of browser
// document.title="HTe Enterprise - Dashboard"

// =================== FORMATTED DATA ===================
interface PieChartData{
    id: number, 
    label: string
    value: number
}

// Column Names used in TableTemplate
const storeHeadCells : HeadCellTypes[] = [
  { id: "type",             sorted: false, searchable: false, label: 'Type',            isShowing: true, mandatory: true},
  { id: 'storeName',        sorted: true, searchable: true, label: 'Store Name',      isShowing: true, mandatory: true },
  { id: 'assignedBanner',   sorted: true, searchable: true, label: 'Assigned Banner', isShowing: true, mandatory: true },
  { id: 'regionId',   sorted: true, searchable: true, label: 'Assigned Region', isShowing: true, mandatory: true },
  { id: 'ipAddress',        sorted: true, searchable: true, label: 'Ip Address', isShowing: true, mandatory: false },
  // { id: 'hostname',         sorted: true, searchable: true, label: 'Hostname', isShowing: false, mandatory: false }, // not a store field
  // { id: 'lastCommunicationTimestampUtc', sorted: true, searchable: false, label: 'Last Report', isShowing: false, mandatory: false }, // not a store field
  { id: 'options',          sorted: false, searchable: false, label: 'Details',         isShowing: true, mandatory: true },
  // { id: "id", narrow: false, disablePadding: false, label: 'ID', mandatory: false },   // can use this to filter for Region / Banner this store belongs to
];

const deptHeadCells : HeadCellTypes[] = [
  { id: "type",         sorted: false, searchable: false, label: 'Type',            isShowing: true, mandatory: true},
  { id: "deptName1",         sorted: true, searchable: true,   label: 'Department Name', isShowing: true, mandatory: true },
  // { id: "assignedStore",sorted: true, searchable: true,   label: 'Assigned Store',  isShowing: true, mandatory: false }, // Not working and not super practical - Corey
  // { id: "id", narrow: false, disablePadding: false, label: 'ID', mandatory: false },   // can use this to filter for Region / Banner this store belongs to
];

const scaleHeadCells : HeadCellTypes[] = [
  { id: "type",             sorted: false, searchable: false, label: 'Type',                isShowing: true, mandatory: true},
  { id: 'ipAddress',        sorted: true, searchable: true,  label: 'IP Address',          isShowing: true, mandatory: true},
  { id: 'storeId',    sorted: true, searchable: true,   label: 'Assigned Store',      isShowing: true, mandatory: false},
  { id: 'deptId',     sorted: true, searchable: true,   label: 'Assigned Department', isShowing: true, mandatory: false},
  { id: 'hostname',         sorted: true, searchable: true,   label: 'Host Name',           isShowing: true, mandatory: false },
  { id: 'application',      sorted: true, searchable: true,   label: 'Application Version', isShowing: true, mandatory: false },
  { id: 'totalLabelsPrinted',    sorted: true, searchable: false,  label: 'Labels Printed',      isShowing: false, mandatory: false },
  { id: 'pluCount',         sorted: true, searchable: false,  label: 'PLU Count',           isShowing: false, mandatory: false },
  { id: 'lastReportTimestampUtc', sorted: true, searchable: false, label: 'Last Report', isShowing: false, mandatory: false },
  { id: 'weigherPrecision', sorted: true, searchable: false, label: 'Weigher Precision', isShowing: false, mandatory: false },
  { id: 'options',          sorted: false, searchable: false, label: 'Details',             isShowing: true, mandatory: true},
  // { id: "id", narrow: false, disablePadding: false, label: 'ID', mandatory: false },   // can use this to filter for Region / Banner this store belongs to
];

function FilteredStoreTable(filteredStores : Store[], finalSelected : string){
    return(
    
        <Col xs={12} className="form-control mb-4">
            <h2><span style={{color: "#ba4735"}}> <StoreIcon />  {finalSelected} Stores </span></h2>

            <VirtualTable
                tableName = "virtualStoreTable"
                dataSet={filteredStores.sort((a, b) => a.storeName < b.storeName ? -1 : a.storeName > b.storeName ? 1 : 0)}
                headCells={storeHeadCells}
                initialSortedBy={{name: '' }}
                dispatchedUrls = {[StoreAPI.fetchStores]}
                saveKey="virtualStoreTableHead"
            />
        </Col>
    )
}

function FilteredDepartmentTable(filteredDepts : Department[], finalSelected : string){
    return(
        <Col xs={12} className="form-control mb-4">
            <h2><span style={{color: "#ba4735"}}> <DepartmentIcon />  {finalSelected} Departments </span></h2>

            <VirtualTable
                tableName = "virtualDeptTable"
                dataSet={filteredDepts.sort((a, b) => a.deptName1 < b.deptName1 ? -1 : a.deptName1 > b.deptName1 ? 1 : 0)}
                headCells={deptHeadCells}
                initialSortedBy={{name: '' }}
                dispatchedUrls = {[DeptAPI.fetchDepartment]}
                saveKey="virtualDeptTableHead"
            />

        </Col>
    )
}

function FilteredScaleTable(filteredScales : ScaleDevice[], finalSelected : string){
    return(
        <Col xs={12} className="form-control mb-4" >
            <h2><span style={{color: "#ba4735"}}> <ScaleIcon />  {finalSelected} Scales </span></h2>

            <VirtualTable
                tableName = "virtualScaleTable"
                dataSet={filteredScales.sort((a, b) => sortIPv4(a.ipAddress,b.ipAddress))}
                headCells={scaleHeadCells}
                initialSortedBy={{name: '' }}
                // dispatchedUrls = {[ScaleAPI.fetchScales]}
                saveKey="virtualScaleTableHead"
            />
            
      </Col>
    )
}

function parseScaleModels(filteredList : ScaleDevice[]){
    const modelDict = {}
    const modelList : PieChartData[] = []
    const updatedModelList : PieChartData[]= []
    let otherScales = 0
    filteredList.forEach((item) => {
        const scaleModel = item.scaleModel
        let modelName = !scaleModel ? "No Report" : scaleModel
        modelDict[modelName] = (!modelDict[modelName] ? 0 : modelDict[modelName]) + 1
    })

    // Populate inital array
    Object.keys(modelDict).map((key, index) => {
        modelList.push({id: index, label: key, value: modelDict[key]})
        return 0
    })

    // Sorts array in descending order
    modelList.sort((a,b) => (b.value > a.value) ? 1 : ((a.value > b.value) ? -1 : 0))

    // This map function displays the top 4 options + the others in one group
    modelList.map((scaleModel, index) => {
        if (index < 4) updatedModelList.push({id: index, label: scaleModel.label, value: scaleModel.value})
        else otherScales = otherScales + scaleModel.value
        return 0
    })

    // If there are not extra options
    if (otherScales > 0) {
        updatedModelList.push({id:4, label: "Other", value: otherScales})
    }

    return updatedModelList
}

function parseLabelsPrinted(filteredList: ScaleDevice[]) {
  const labelPrintDict = {}
  const labelPrintList: PieChartData[] = []
  const updatedLabelPrintList: PieChartData[] = []
  let otherLabelPrint = 0
  const groupLabelPrints = (numPrinted: number) => {
    const low = 300000
    const middle = 600000
    const high = 1000000

    if (numPrinted <= low) {
      return "0:300,000"
    } else if (numPrinted <= middle) {
      return "300,001:600,000"
    } else if (numPrinted <= high) {
      return "600,001:1,000,000"
    } else {
      return "1,000,000+"
    }
  }

  filteredList.forEach(item => {
    const labelsPrinted = item.totalLabelsPrinted
    let labelsPrintedStr = !labelsPrinted ? "No Report" : groupLabelPrints(labelsPrinted)
    labelPrintDict[labelsPrintedStr] = (!labelPrintDict[labelsPrintedStr] ? 0 : labelPrintDict[labelsPrintedStr]) + 1
  })

  Object.keys(labelPrintDict).map((key, index) => {
    labelPrintList.push({id: index, label: key, value: labelPrintDict[key]})
    return 0
  })

  labelPrintList.sort((a,b) => (b.value > a.value) ? 1 : ((a.value > b.value) ? -1 : 0))

  labelPrintList.map((labelsPrinted, index) => {
    if (index < 4) {
      updatedLabelPrintList.push({id: index, label: labelsPrinted.label, value: labelsPrinted.value})
    } else {
      otherLabelPrint = otherLabelPrint + labelsPrinted.value
    }
    return 0
  })

  if (otherLabelPrint > 0) {
    updatedLabelPrintList.push({id: 4, label: "Other", value: otherLabelPrint})
  }

  return updatedLabelPrintList
}

function parseLabelStockLoaded(filteredList: ScaleDevice[]) {
  const labelStockDict = {}
  const labelStockList: PieChartData[] = []
  const updatedLabelStockList: PieChartData[] = []
  let otherLabelStock = 0

  filteredList.forEach(item => {
    const labelStock = item.labelStockSize
    let labelStockStr = !labelStock ? "No Report" : labelStock
    labelStockDict[labelStockStr] = (!labelStockDict[labelStockStr] ? 0 : labelStockDict[labelStockStr]) + 1
  })

  Object.keys(labelStockDict).map((key, index) => {
    labelStockList.push({id: index, label: key, value: labelStockDict[key]})
    return 0
  })

  labelStockList.sort((a,b) => (b.value > a.value) ? 1 : ((a.value > b.value) ? -1 : 0))

  labelStockList.map((labelStocks, index) => {
    if (index < 4) {
      updatedLabelStockList.push({id: index, label: labelStocks.label, value: labelStocks.value})
    } else {
      otherLabelStock = otherLabelStock + labelStocks.value
    }
    return 0
  })

  if (otherLabelStock > 0) {
    updatedLabelStockList.push({id: 4, label: "Other", value: otherLabelStock})
  }

  return updatedLabelStockList
}

function parseScaleFirmwares(filteredList : ScaleDevice[]){
    const firmwareDict = {}
    const firmwareList : PieChartData[] = []
    const updatedFirmwareList : PieChartData[] = []
    let otherFirmwares = 0
    filteredList.forEach((item) => {
        const application = item.application
        let applicationStr = !application ? "No Report" : application
        firmwareDict[applicationStr] = (!firmwareDict[applicationStr] ? 0 : firmwareDict[applicationStr]) + 1
    })

    // Populate inital array
    Object.keys(firmwareDict).map((key, index) => {
        firmwareList.push({id: index, label: key, value: firmwareDict[key]})
        return 0
    })

    // Sorts array in descending order
    firmwareList.sort((a,b) => (b.value > a.value) ? 1 : ((a.value > b.value) ? -1 : 0))

    // This map function displays the top 4 options + the others in one group
    firmwareList.map((scaleModel, index) => {
        if (index < 4) updatedFirmwareList.push(
            {id: index, label: scaleModel.label, value: scaleModel.value}
            )
        else otherFirmwares = otherFirmwares + scaleModel.value
        return 0
    })

    // If there are not extra options
    if (otherFirmwares > 0) {
        updatedFirmwareList.push({id:4, label: "Other", value: otherFirmwares})
    }

    return updatedFirmwareList
}

const parseUptimeData = (uptimeData?: SyncData) => {
  const uptimeList: PieChartData[] = []
  
  if (uptimeData && Object.keys(uptimeData).length > 0) {
    // The math performed below is weird and may not be accurate

    // let heartbeatTotal = uptimeData.numHeartbeats.reduce((a,b) => a+b,0)
    // let syncTotal = uptimeData.numInSync.reduce((a,b) => a+b,0)
    // console.log(syncTotal, heartbeatTotal)
    // let allAvg = (heartbeatTotal + syncTotal) / (uptimeData.numHeartbeats.length + uptimeData.numInSync.length) * 100
    // let formattedAllAvg = Number.parseFloat(allAvg.toFixed(2))


    //stopped using the math above and are now using the data returned from the backend -> @Patrick 
    let uptime = +(uptimeData.uptime.split("%")[0])
     
    uptimeList.push({id: 0, label: "% Downtime", value: 100-uptime})
    uptimeList.push({id: 1, label: "% Uptime", value: uptime})
  }
  
  return uptimeList
}

const parseOffline = (filterList: ScaleDevice[]) => {
  const offlineDict = {Offline : 0, Online : 0}
  const offlineList: PieChartData[] = []
  const updatedOfflineList: PieChartData[] = []

  filterList.forEach(item => {
    const cutoff = Date.now() - 8.64e7
    let offline
    if (new Date(item.lastReportTimestampUtc).getTime() < cutoff) {
      offline = "Offline"
    } else {
      offline = "Online"
    }
    let offlineStr = !offline ? "No Report" : offline
    offlineDict[offlineStr] = (!offlineDict[offlineStr] ? 0 : offlineDict[offlineStr]) + 1
  })

  Object.keys(offlineDict).sort().map((key, index) => {
    offlineList.push({id: index, label: key, value: offlineDict[key]})
    return 0
  })

  // offlineList.sort((a,b) => (b.value > a.value) ? 1 : ((a.value > b.value) ? -1 : 0))

  offlineList.sort().map((offline, index) => {
    if (index < 4) {
      updatedOfflineList.push({id: index, label: offline.label, value: offline.value})
    }
    return 0
  })
  return offlineList
}

// =================== FORMATTED DATA ===================

export default function Dashboard() {

    // Redux
    const dispatch = useDispatch();
    const depts = useAppSelector((state) => state.asset.depts)
    const user = useAppSelector((state) => state.access.account);
    const reduxUptimeData = useAppSelector(state => state.asset.syncData)["dashboard"]
    const {callbacks, addCallback} = useRspHandler();

    const [uptimeData, setUpTimeData ] = useState<SyncData>();
    useEffect(()=>{
      setUpTimeData({...reduxUptimeData})
    },[reduxUptimeData])
    const [bannerList, setBannerList] = useState<Banner[]>([])
    const [bannerSelected, setBannerSelected] = useState("Total")
    const [finalSelected, setFinalSelected] = useState("All")
    const [assignedCount, setAssignedCount] = useState<number>(0)
    const [unassignedCount, setUnassignedCount] = useState<number>(0)
    //List of Scale devices included by the three dropdowns
    const [unassignedScales, setUnassignedScales] = useState<ScaleDevice[]>([])
    const [filterList, setFilterList] = useState<ScaleDevice[]>([])
    const [filterDeptKeyList, setFilterDeptKeyList] = useState<string[]>([])
    const [loadingPage, setLoadingPage] = useState(true)

    useEffect(() => {
      setUnassignedCount(unassignedScales.length)
    }, [unassignedScales])
    useEffect(() => {
      setAssignedCount(filterList.length)
    }, [filterList])
    
    const handleBannerSelect = (e : React.FormEvent<HTMLSelectElement> | number) => {
      setLoadingPage(true)
      let selectedIndex : number
      let id = Date.now()
      if(!(e as React.ChangeEvent<HTMLSelectElement>).target){
          selectedIndex = e as number
      }
      else{
          selectedIndex = (e as React.ChangeEvent<HTMLSelectElement>).target.options.selectedIndex-1
      }

      let selected = bannerList[selectedIndex]
      let newName = !selected ? 
      "Total" :  selected.bannerName
      function onSuccess(){
        setLoadingPage(false)
        setBannerSelected(newName)
      }
      function onFail(){
        setLoadingPage(false)
        console.log("Failed dashboard")
      }
      if(selected){
        if (addCallback(callbacks, id, "Fetch dashboard information", onSuccess, onFail)){
          setBannerSelected(newName)
          dispatch(AssetAPI.fetchDashboard(id, setBannerList, setRegionList, setStoreList, setFilterDeptKeyList, setFilterList, setUnassignedScales, selected.bannerId, "BANNER"))    
        }
      } else {
        if (addCallback(callbacks, id, "Fetch entire dashboard", onSuccess, onFail)){
          // Causes multi-selects to refresh to ensure toggles are in-sync
          setBannerList([])
          setRegionList([])
          setStoreList([])
          setDepartmentList({})
          dispatch(AssetAPI.fetchDashboard(id, setBannerList, setRegionList, setStoreList, setFilterDeptKeyList, setFilterList, setUnassignedScales))
        }
      }
  };

    const [regionList, setRegionList] = useState<Region[]>([])
    const [regionSelected, setRegionSelected] = useState<string>("Total")
    const handleRegionSelect = (e : React.FormEvent<HTMLSelectElement> | number) => {
      setLoadingPage(true)
      let selectedIndex : number
      let id = Date.now()
      if(!(e as React.ChangeEvent<HTMLSelectElement>).target){
          selectedIndex = e as number
      }
      else{
          selectedIndex = (e as React.ChangeEvent<HTMLSelectElement>).target.options.selectedIndex-1
      }


      let selected = regionList[selectedIndex]
      let newName = !selected ? 
      "Total" :  selected.regionName
      function onSuccess(){
        setLoadingPage(false)
        setRegionSelected(newName)
      }
      function onFail(){
        setLoadingPage(false)
        console.log("Failed dashboard")
      }
      if(selected){
        if (addCallback(callbacks, id, "fetch dashboard information", onSuccess, onFail)){
          setRegionSelected(newName)
          dispatch(AssetAPI.fetchDashboard(id, setBannerList, setRegionList, setStoreList, setFilterDeptKeyList,
             setFilterList, setUnassignedScales, selected.regionId, "REGION"))    
        }
      } else {
        if (addCallback(callbacks, id, "Fetch entire dashboard", onSuccess, onFail)){
          // Causes multi-selects to refresh to ensure toggles are in-sync
          setBannerList([])
          setRegionList([])
          setStoreList([])
          setDepartmentList({})
          dispatch(AssetAPI.fetchDashboard(id, setBannerList, setRegionList, setStoreList, setFilterDeptKeyList, setFilterList, setUnassignedScales))
        }
      }

  }

    const [storeList, setStoreList] = useState<Store[]>([])
    const [storeSelected, setStoreSelected] = useState<string>("Total")
    const handleStoreSelect = (e : React.FormEvent<HTMLSelectElement>) => {
      setLoadingPage(true)
      let selectedIndex : number
      let id = Date.now()
      selectedIndex = (e as React.ChangeEvent<HTMLSelectElement>).target.options.selectedIndex-1

      let selected = storeList[selectedIndex]
      let newName = !selected? 
      "Total" :  selected.storeName
      function onSuccess(){
        setLoadingPage(false)
        setStoreSelected(newName)
      }
      function onFail(){
        setLoadingPage(false)
        console.log("Failed dashboard")
      }
      if(selected){
        if (addCallback(callbacks, id, "fetch dashboard information", onSuccess, onFail)){
          setStoreSelected(newName)
          dispatch(AssetAPI.fetchDashboard(id, setBannerList, setRegionList, setStoreList, setFilterDeptKeyList, setFilterList, setUnassignedScales, selected.storeId, "STORE"))    
        }
      } else {
        if (addCallback(callbacks, id, "Fetch entire dashboard", onSuccess, onFail)){
          // Causes multi-selects to refresh to ensure toggles are in-sync
          setBannerList([])
          setRegionList([])
          setStoreList([])
          setDepartmentList({})
          dispatch(AssetAPI.fetchDashboard(id, setBannerList, setRegionList, setStoreList, setFilterDeptKeyList, setFilterList, setUnassignedScales))
        }
      }
  }

  useEffect(() => {
    bannerList.sort((a,b) => a.bannerName.localeCompare(b.bannerName, 'en', {numeric: true}))
  }, [bannerList])
  useEffect(() => {
    regionList.sort((a,b) => a.regionName.localeCompare(b.regionName, 'en', {numeric: true}))
  }, [regionList])
  useEffect(() => {
    storeList.sort((a,b) => a.storeName.localeCompare(b.storeName, 'en', {numeric: true}))
  }, [storeList])

    useEffect(()=>{
      if(depts && Object.keys(depts).length > 0)
        setFilterDeptList(filterDeptKeyList.map(deptKey => {return depts[deptKey]}))
    },[filterDeptKeyList])
    const [filterDeptList, setFilterDeptList] = useState<Department[]>([])

    // ==================   LICENSE STATUS   ==================
    // Calculate license status to see if it should be shown or not.

    const users = useAppSelector((state) => state.access.users);
    const licenses = useAppSelector((state) => state.access.licenses);
    const [notice, setNotice] = useState(false)
    const [expirationDate, setExpirationDate] = useState("")

    const handleLicenseNotice = () => {
        try {
            if (licenses.length !== 0) {

                // "PPP format" = Month day, year (ex. March 28th, 2023)
                setExpirationDate(format(new Date(new Date(licenses[0].activationDate).getTime() + (licenses[0].timeout * 86400000)), "PPP"))

                if (notice === false) {
                // Check number of users
                    if (licenses[0].numUsers  && ((users.length - 1) / licenses[0].numUsers > 0.9)) {
                        setNotice(true)
                    }                              

                    // Check number of scales
                    if (licenses[0].numScales && (assignedCount + unassignedCount) / licenses[0].numScales > 0.9) {
                        setNotice(true)
                    }                             

                    // Check expiration date of license
                    if (expirationDate && moment(expirationDate).diff(moment(), 'days') <= 30)  {
                        setNotice(true)
                    }                             
                }
            }

        } catch (error) {
        // Do nothing
        }
    }


  // ==================   UNASSIGNED DOMAIN ALERT   ==================
  // Check if user's domain is unassigned due to their domain getting deleted. Can add other alerts in this section

    const [unassignedAlert, setUnassignedAlert] = useState([false, "Currently assigned"]) // If user is unassigned due to domain getting deleted

    const handleAlerts = () => {
        try {
        setUnassignedAlert([false, "Currently assigned"])
        
        if (user.domain.domainId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID) {
            setUnassignedAlert([true, "Your account is currently not assigned to any domain (store, region, banner)."])
        }

        } catch (error) {
        // Do nothing
        }
    }

    // ====================================

    useEffect(() => {
        dispatch(AccessActions.checkTokenStatus())
        dispatch(ProfileAPI.fetchProfiles())
        dispatch(ConfigAPI.fetchServices())

        function onSuccess() {
          setLoadingPage(false)
        }
        function onFail() {
          setLoadingPage(false)
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Fetch Dashboard", onSuccess, onFail)) {
          dispatch(AssetAPI.fetchDashboard(newID, setBannerList, setRegionList, setStoreList, 
            setFilterDeptKeyList, setFilterList, setUnassignedScales,))
        }
        
        handleAlerts()
    }, []);
    useEffect(() => {
      const bannerSel =  document.getElementById('bannerSelect') as HTMLSelectElement;
      const regionSel =  document.getElementById('regionSelect') as HTMLSelectElement;
      const storeSel =  document.getElementById('storeSelect') as HTMLSelectElement;

      if(bannerSel && bannerSel.value !== ""){
          setFinalSelected(bannerSel.options[bannerSel.selectedIndex].text)
      }
      else if(regionSel && regionSel.value !== ""){
          setFinalSelected(regionSel.options[regionSel.selectedIndex].text)
      } 
      else if(storeSel && storeSel.value !== ""){
          setFinalSelected(storeSel.options[storeSel.selectedIndex].text)
      }
      else{
          setFinalSelected("All")
      }

  }, [bannerSelected, regionSelected, storeSelected])

    useEffect(() => {
        handleLicenseNotice()
    }, [licenses])

    const filteredStoreTable = useMemo(() => FilteredStoreTable(storeList, finalSelected), [storeList])
    const filteredDepartmentTable = useMemo(() => FilteredDepartmentTable(filterDeptList, finalSelected), [filterDeptList])
    const filteredScaleTable = useMemo(() => FilteredScaleTable(filterList, finalSelected), [filterList, finalSelected])

    // const onConnect = () => {
    //   logger.debug("Connected to websocket");
    //   dispatch(SocketActions.setSocketStatus("CONNECTED"))
    // }

    // const onDisconnect = () => {
    //     logger.debug("Disconnected from websocket");
    //     dispatch(notify('Disconnected from scan service!', 'error'))
    //     dispatch(SocketActions.setSocketStatus("DISCONNECTED"))
    // }

    // const onMessageReceived = msg => {
    //   if (typeof msg === 'object') {
    //     // from fleetstatus
    //     dispatch(SocketActions.setMessage(msg))
    //   } else if (typeof msg === 'number') {
    //     // from navbar
    //     dispatch(SocketActions.setMessage(msg))
    //   }
    // }

  return (
    <>
    <div className="mb-4 mb-md-0">
      <Row>
        <Col>
          <h2 className="mb-4">Dashboard Overview</h2>
        </Col>
        {loadingPage &&
          <Col>
            <CircularProgress />
            <Typography variant="body1" component="span" style={{marginLeft: "0.5rem", position: "relative", top: "-0.9rem"}}>Loading data. Please wait...</Typography>
          </Col>
        }

        {/* Unassigned Alert */}
        {unassignedAlert[0] ? <Alert className="mb-4" variant="filled" severity="error">{unassignedAlert[1]}</Alert> : null}

        {/* Weird interaction with order of rendering, must check license[0] to see if it exists first before it renders anything. If more licenses are stored eventually, will need to change solution */}
        {licenses[0] && notice
          ? <Col style={{maxWidth:'400px'}} className="mb-4">
              <Card border="light" className="shadow-sm" style={{padding: '0.1rem 0.1rem'}}>
                <Card.Body>
                  <Row>
                    <Col xl={12} className={"text-align-center fw-bold"}>
                    <h5>HTe Enterprise License Notice</h5>
                    </Col>

                    {/* ========= User Capacity Notice ========= */}
                    {licenses[0].numUsers && ((users.length - 1) / licenses[0].numUsers > 0.9) 
                    ? <Col xl={12}>
                        <h6 className={ // if # scales >= the limit, Red text.  if # scales > 90%, Orange text. 
                          `${(users.length - 1) / licenses[0].numUsers >= 1 
                            ? "color-red " 
                            : (users.length - 1) / licenses[0].numUsers > 0.9 
                              ? "color-orange " 
                              : ""} fw-bold mb-0 text-align-center`
                        }>
                          {`# of Users: ${(users.length - 1)} / ${licenses[0].numUsers}`}
                        </h6>
                      </Col>
                    : null}

                    {/* ========= Scale Capacity Notice ========= */}
                    {licenses[0].numScales && (assignedCount + unassignedCount) / licenses[0].numScales > 0.9
                    ? <Col xl={12}>
                        <h6 className={ // if # scales >= the limit, Red text.  if # scales > 90%, Orange text. 
                          `${(assignedCount + unassignedCount) / licenses[0].numScales >= 1 
                            ? "color-red " 
                            : (assignedCount + unassignedCount) / licenses[0].numScales > 0.9 
                              ? "color-orange " 
                              : ""} fw-bold mb-0 text-align-center`
                        }>
                          {`# of Scales: ${(assignedCount + unassignedCount)} / ${licenses[0].numScales}`}
                        </h6>
                      </Col>
                    : null}

                    {/* ========= License Duration Notice ========= */}
                    {expirationDate && moment(expirationDate).diff(moment(), 'days') <= 30 
                    ? <Col xl={12}>
                        <h6 className={`${moment(expirationDate).diff(moment(), 'days') <= 30 
                          ? "color-red " 
                          : ""} fw-bold mb-0 text-align-center`}
                        >
                          {`# of days left on Enterprise license: ${moment(expirationDate).diff(moment(), 'days')}`}
                        </h6>
                      </Col>
                    : null}
                  </Row>
                </Card.Body>
              </Card>
            </Col>
          : null}
      </Row>

      {/* NOT CURRENTLY USING, But can use these buttons for forming reports */}
      {/* <div className="d-flex justify-content-between">

        <ButtonGroup className="ml-auto mr-0 assetBar">
          <Button id="actionBar2" variant="outline-primary" size="sm"> 
            Share
          </Button>
          <Button id="actionBar2" variant="outline-primary" size="sm"> 
            Export
          </Button>
        </ButtonGroup>                
      </div> */}
    </div>
    { true ?
    <div className="dashboardView">
      <Row className="justify-content-md-center">
        <Col xs={12} sm={6} xl={4} className="mb-4">
          <CounterWidgetSelect
            category="Total Banners: "
            title={bannerList.length}
            label="View all banners below"
            dataset={bannerList.map(e => {return {id:e.bannerId, label : e.bannerName}})}
            icon={LargeBannerIcon}
            iconColor="shape-tertiary"
            onChange= {handleBannerSelect}
            selectID = {"bannerSelect"}
          />
        </Col>

        <Col xs={12} sm={6} xl={4} className="mb-4">
          <CounterWidgetSelect
            category= {bannerSelected + " Regions: "}
            title={regionList.length}
            label="View all regions below"
            dataset={regionList.map(e => {return {id:e.regionId, label : e.regionName}})}
            icon={LargeRegionIcon}
            iconColor="shape-tertiary"
            onChange={handleRegionSelect}
            selectID = {"regionSelect"}
          />
        </Col>

        <Col xs={12} sm={6} xl={4} className="mb-4">
          <CounterWidgetSelect
            category={regionSelected + " Stores: "}
            title={storeList.length}
            label="View all stores below"
            dataset={storeList.map(e => {return {id:e.storeId, label : e.storeName}})}
            icon={LargeStoreIcon}
            iconColor="shape-tertiary"
            onChange={handleStoreSelect}
            selectID = {"storeSelect"}
          />
        </Col>
      </Row>

      <Row className="justify-content-md-center">
        <Col xs={9} sm={4} xl={3} className="mb-4">
          <CounterWidget
            category="Total Scales: "
            operation="TOTAL"
            dataset={filterList}
            icon={ScaleIcon}
            dontShowPerc = {true}
            iconColor="shape-tertiary"
          />
        </Col>

        <Col xs={9} sm={4} xl={3} className="mb-4">
          <CounterWidget
            category="Online Scales: "
            operation="ONLINE"
            dataset={filterList}
            icon={ScaleIcon}
            iconColor="shape-tertiary"
          />
        </Col>

        <Col xs={9} sm={4} xl={3} className="mb-4">
          <CounterWidget
            category="Offline Scales: "
            operation="ERROR"
            dataset={filterList}
            icon={ScaleIcon}
            iconColor="shape-tertiary"
          />
        </Col>

        <Col xs={9} sm={4} xl={3} className="mb-4">
          <CounterWidget
            category="Disabled Scales: "
            operation="DISABLED"
            dataset={filterList}
            icon={ScaleIcon}
            iconColor="shape-tertiary"
          />
        </Col>

      </Row>

      <Row>
        <Col xs={12} sm={4} xl={4} className="mb-4">
          <CircleChartWidget
            title="Scale Models"
            data={parseScaleModels(filterList)} />
        </Col>

        <Col xs={12} sm={4} xl={4} className="mb-4">
          <CircleChartWidget
            title="Scale Firmware Versions"
            data={parseScaleFirmwares(filterList)} />
        </Col>

        <Col xs={12} sm={4} xl={4} className="mb-4">
          <CircleChartWidget
            title={"% Uptime (Online & In Sync) for " + uptimeData?.range}
            data={parseUptimeData(uptimeData)}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12} sm={4} xl={4} className="mb-4">
          <CircleChartWidget
            title="Labels Printed"
            data={parseLabelsPrinted(filterList)}
          />
        </Col>

        <Col xs={12} sm={4} xl={4} className="mb-4">
          <CircleChartWidget
            title="Label Stock Loaded"
            data={parseLabelStockLoaded(filterList)}
          />
        </Col>

        <Col xs={12} sm={4} xl={4} className="mb-4">
          <CircleChartWidget
            title="% Offline"
            data={parseOffline(filterList)}
          />
        </Col>
      </Row>

      <Row>
        <Col xs={12} sm={6} xl={6} className="mb-4">
            <SimpleScaleList
              title="Unassigned Scale(s)"
              data = {bannerSelected === "Total" && 
              regionSelected === "Total" &&
              storeSelected === "Total" ? unassignedScales : []}
            />
        </Col>

        <Col xs={12} sm={6} xl={6} className="mb-4">
            <SimpleScaleList
              title={finalSelected + " Offline Scale(s)"}
              data = {filterList}
              offlineList = {true}
            />
        </Col>

      </Row>

      <Row className="justify-content-md-center">
        <Col xs={12} sm={12} xl={12} className="mb-4">
          <Card border="light" className="shadow-sm">
            <Card.Body>
            <div id="offlineMonitor" className="d-none d-sm-block">

              <ScaleSyncGraph
                title={<h2><span style={{color: "#ba4735"}}> <ConnectedTv/> Status Monitor </span></h2>}
                graphId="dashboard"
                deviceList={filterList}
              />
            {/* <h3 className="mb-1">{title}</h3>     Ex. 345k, $50,000 */}
            </div> 
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <div className="dashboardView">
        <Row className="justify-content-md-center">
          <Col xs={12} sm={12} xl={12} className="mb-4">
            {filteredStoreTable}
            {filteredDepartmentTable}
            {filteredScaleTable}
          </Col>
        </Row>
      </div>
    </div> :
          <div className="d-flex justify-content-left align-items-center ">
            <div className='spinnerDashboard'/>
            <h2 style={{marginLeft: "1rem", marginTop: "1rem"}}> Loading Asset Data... </h2>
          </div>
          
  }

    {/* <SockJsClient 
      url={getURL(Endpoints.messager, "")}
      topics={['/topic/fleetStatus', '/topic/eventsCount']}
      onConnect={() => {onConnect()}}
      onDisconnect={() => {onDisconnect()}}
      onMessage={(msg : any) => {onMessageReceived(msg)}}
      debug={false}
    /> */}
    </>
  );
};
