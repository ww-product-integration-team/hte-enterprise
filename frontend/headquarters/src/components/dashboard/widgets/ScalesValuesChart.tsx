import { ButtonGroup, ToggleButton } from "@themesberg/react-bootstrap";
import { useDispatch} from "react-redux";
import { AssetAPI } from "../../api";
import { useEffect, useMemo, useState } from "react";
import { Bar } from "react-chartjs-2";
import StatusIndicator from "./StatusIndicator";
import { useAppSelector } from "../../../state";
import { ScaleDevice, SyncData } from "../../../types/asset/ScaleTypes";
import format from 'date-fns/format'

interface ScaleSyncGraphProps{
    title: JSX.Element, 
    graphId: string,
    deviceList: ScaleDevice[]
}

export const ScaleSyncGraph = (props : ScaleSyncGraphProps) => {
  const { title, graphId, deviceList} = props;
  
  const syncData = useAppSelector((state) => state.asset.syncData);
  const dispatch = useDispatch()
  const scaleValuesChart = useMemo(() => <ScalesValueChart syncData={syncData[graphId]}/>, [syncData[graphId]])
  const radios = [
    { name: 'Day', value: 'day' },
    { name: 'Week', value: 'week' },
    { name: 'Month', value: 'month' },
  ];

  const [numDevices, setNumDevices] = useState(Object.values(deviceList).filter((scale) => scale.deptId === graphId).length)

  useEffect(()=>{
    if(deviceList && deviceList.length > 0 && deviceList.length !== numDevices){
      setNumDevices(deviceList.length)
    }
  },[deviceList])

  const [radioValue, setRadioValue] = useState("day");
  useEffect(() => {
    let listOfIds : string[] = []
    if (deviceList.length > 0 && typeof deviceList[0] === 'object') {
      listOfIds = deviceList.map(a => a.deviceId)
    }
    dispatch(AssetAPI.getSyncStatus(Date.now(), graphId, listOfIds, radioValue, "graph"))
  }, [radioValue, numDevices])

  return(  
    <>
        <div style={{display:"flex", alignItems:"center"}}>
            <>
            {title}
            <h6 style={{marginLeft: "auto", marginBottom: "0"}}><span style={{color: "#ba4735"}}> Uptime: {syncData[graphId] ? syncData[graphId].uptime: "Unknown"} </span></h6>
            </>
        </div>
        {scaleValuesChart}
        <span style={{display: "flex"}}>
          <p style={{textAlign: 'left', marginBottom: '0px', flexGrow:1}}>
            <StatusIndicator style={{backgroundColor:'#ba4735'}}/> Scale heartbeats 
            <StatusIndicator style={{backgroundColor:'#73cfec', marginLeft: '14px'}}/> Scales in-sync 
          </p> 
          <ButtonGroup className="toggleBar">
            {radios.map((radio, idx) => (
              <ToggleButton
                key={`${idx}-${graphId}`}
                id={`radio-${idx}-${graphId}`}
                type="radio"
                size = "sm"
                variant={'outline-primary'}
                name="radio"
                value={radio.value}
                checked={radioValue.toLowerCase() === radio.value}
                onClick={(e) => setRadioValue(e.currentTarget.innerHTML)}
              >
                {radio.name}
              </ToggleButton>
            ))}
          </ButtonGroup>
        </span>
    </>
  )

}

export const ScalesValueChart = (props : {syncData: SyncData}) => {
    const { syncData } = props;
    if(!syncData){
      return(<label className="noDataLabel">No Data</label>)
    }

    let timeArray : string[] = []
    let prevDateStr = "" //Used for showing time on week graph
    for (let i = 0; i < syncData.numHeartbeats.length; i++) { 
      let time = (i * syncData.del) + syncData.start
      if(time <= syncData.end){
        switch(syncData.range){

            case "day":
            case "Day":
              if(timeArray.length < 26){
                timeArray.push(format(time, "ha"))
              }
              break;
            case "week":
            case "Week":
                if(timeArray.length < 30){
                    let dayStr = format(time, "PPpp")
                    if(prevDateStr === format(time, "MMM d")){
                        dayStr = format(time, "ha")
                    }

                    prevDateStr = format(time, "MMM d")

                    timeArray.push(dayStr);
              }
              break;
            default:
             
              if(timeArray.length < 30){
                timeArray.push(format(time, "MMM d"));
              }
              break;
        }
      }
      
    }

    let last24Hour = syncData.numHeartbeats.slice(0, timeArray.length)
    let last24HourSync =  syncData.numInSync.slice(0, timeArray.length)

    let last24HourObj : {meta: string, value: number}[] = []
    last24Hour.forEach(item =>
        last24HourObj.push({
            meta: "Reported: ",
            value: item
        })
    )

    let last24HourSyncObj : {meta: string, value: number}[] = []
    last24HourSync.forEach(item =>
        last24HourSyncObj.push({
            meta: "In-Sync:",
            value: item
        })
    )

    const scaleDataSet : {
        id: number,
        time: string,
        heartbeat: number,
        inSync: number
    }[] = []
    for(let i = 0; i<last24Hour.length; i++){
        scaleDataSet.push({
            id: i,
            time: timeArray[i],
            heartbeat: last24Hour[i],
            inSync: last24HourSync[i]
        })
    }

    const userData = {
        labels: scaleDataSet.map((data) => data.time),
        datasets: [
          {
            label: "Scales Reported",
            data: scaleDataSet.map((data) => data.heartbeat),
            backgroundColor: [
                "#ba4735"
            ],
            borderColor: "transparent",
            borderWidth: 2,
            borderRadius:5,

          },
          {
            label: "Scales In-Sync",
            data: scaleDataSet.map((data) => data.inSync),
            backgroundColor: [
                "#73cfec",
            ],
            borderColor: "transparent",
            borderWidth: 2,
          },
        ],
    };

    const options = {
        plugins: {
            legend: {
                display: false,
            }
        },

        scales: {
            x: {
                grid: {
                  display: false
                }
            },
            y:{
                max: syncData.numScales, 
                ticks:{
                    precision: 0,
                }
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        categoryPercentage: 0.5,
        barPercentage: 1.2
    }

    function BarChart({ chartData }) {
        return <Bar data={chartData} options={options}/>;
    }

    try{
        if(syncData.numScales > 0){
            return (
                <div style={{height:"300px"}}>
                    <BarChart chartData={userData} />
                </div>
            );
        }
        else{
            return(
                <label className="noDataLabel">No Data</label>
            )
        }
    }
    catch(e){
        return(
            <label className="noDataLabel">Error</label>
        )
    }
};
