import { Pie } from "react-chartjs-2";
import 'chart.js-plugin-labels-dv'
import ChartDataLabels from "chartjs-plugin-datalabels";
import { ChartOptions} from "chart.js";

interface CircleChartProps{
    series : {value : number}[]
    labels : string[]
}

export const CircleChart = (props : CircleChartProps) => {
    const { series = []} = props;
    const pieOptions : ChartOptions<"pie">= {
        plugins:{
            legend: {
                display: false,
                position: "right",
            },
            datalabels: {
                display: true,
                color: "white",
                formatter: (value, ctx) => {
                    let sum = 0;
                    let dataArr = ctx.chart.data.datasets[0].data;
                    dataArr.map(data => {
                        if(typeof data === "number"){
                            sum += data;
                        }
                        return null
                    });
                    let percentage = (value*100 / sum).toFixed(0)+"%";
                    return percentage;
                },
                align: 'start',
                offset: -40,
                font: {
                    weight: 600,
                    size:18,
                    family:"'Quicksand','Arial','sans-serif'"
                }
            
            }
        },
        radius: '90%',
        elements: {
          arc: {
            borderWidth: 2,
            borderColor: "white",
            hoverOffset: 10,
          },
        },
      };

    const data = {
        maintainAspectRatio: false,
        responsive: true,
        labels: props.labels,
        datasets: [
          {
            data: series,
            backgroundColor: ["#ba4735", "#73cfec", "#203140", "#963455", "grey"],
            hoverBackgroundColor: ["#ba4735", "#73cfec", "#203140", "#963455", "grey"]
          }
        ]
    };
    return <Pie data={data} options={pieOptions} plugins={[ChartDataLabels]}/>  
};
