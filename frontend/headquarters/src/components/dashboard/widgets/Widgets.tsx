
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faPaperclip, faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { Col, Row, Card, Image, Form} from '@themesberg/react-bootstrap';
import { CircleChart} from "./ChartTypes";
import ScaleIcon, { SmallScaleIcon } from '../../icons/ScaleIcon';
import moment from 'moment';

import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import { FixedSizeList } from 'react-window';
import { Link } from "react-router-dom";
import { alpha, Typography } from "@material-ui/core";
import { ScaleDevice } from '../../../types/asset/ScaleTypes';
import StatusIndicator from './StatusIndicator';

// Not currently displayed
export const ChoosePhotoWidget = (props) => {
  const { title, photo } = props;

  return (
    <Card border="light" className="bg-white shadow-sm mb-4">
      <Card.Body>
        <h5 className="mb-4">{title}</h5>
        <div className="d-xl-flex align-items-center">
          <div className="user-avatar xl-avatar">
            <Image fluid rounded src={photo} />
          </div>
          <div className="file-field">
            <div className="d-flex justify-content-xl-center ms-xl-3">
              <div className="d-flex">
                <span className="icon icon-md">
                  <FontAwesomeIcon icon={faPaperclip} className="me-3" />
                </span>
                <input type="file" />
                <div className="d-md-block text-start">
                  <div className="fw-normal text-dark mb-1">Choose Image</div>
                  <div className="text-gray small">JPG, GIF or PNG. Max size of 800K</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
};


// "Customers", "Revenue", "Banner + Store + Scales" use this widget
interface CounterWidgetSelectProps{
    iconColor: string
    category: string
    title: string | number
    label: string
    dataset : dataPair[]
    onChange : (e: React.FormEvent<HTMLSelectElement>) => void
    selectID : string
    icon : () => JSX.Element
}
export interface dataPair{
  id : string
  label : string
}
export const CounterWidgetSelect = (props : CounterWidgetSelectProps) => {
  const { iconColor, category, title, label, dataset, onChange, selectID} = props;
  const WidgetIcon = props.icon;
  // sort dataset
  dataset.sort((a,b) => a.label.localeCompare(b.label, 'en', {numeric: true}))
  return (
    <Card border="light" className="shadow-sm">
      <Card.Body>
        <Row className="d-block d-xl-flex align-items-center">
          <Col xl={3} className="text-xl-center d-flex align-items-center justify-content-xl-center mb-xl-0">
            <div className={`icon icon-shape icon-md icon-${iconColor} rounded me-2 me-sm-0`}>
            <WidgetIcon/>
            </div>
          </Col>
          
           <Col xs={12} xl={9} className="px-xl-0">
            <div className="d-none d-sm-block">
              <h2><span style={{color: "#ba4735"}}> {category} {title} </span></h2>  {/* Ex. Customers, Revenue */}
            
            {dataset && <>
            
              <Form.Label style= {{color: "grey"}}>{label}</Form.Label>     
              <Form.Select id={selectID} data-testid={selectID} className="form-control" required defaultValue=""
                onChange={(e) => onChange(e)}>
                <option value="">--View All--</option>
                
                {dataset.map(data => (
                  data.id != null ?
                  <option key={data.id} value={data.id}>
                    {data.label}
                  </option> : null
                ))}
              </Form.Select>    
            </>}
            {/* <h3 className="mb-1">{title}</h3>     Ex. 345k, $50,000 */}
            </div> 

            {/* <small>{period}, <FontAwesomeIcon icon={faGlobeEurope} size="xs" /> WorldWide</small>

            <div className="small mt-2">
              <FontAwesomeIcon icon={percentageIcon} className={`${percentageColor} me-1`} />
              <span className={`${percentageColor} fw-bold`}>
                {percentage}%
              </span> Since last month
            </div> */}
          </Col> 
        </Row>
      </Card.Body>
    </Card>
  );
};


interface CounterWidgetProps{
    iconColor: string
    category:  string
    title?: string | number
    operation : "TOTAL" | "ONLINE" | "WARNING" | "ERROR" | "DISABLED"
    dataset : ScaleDevice[]
    dontShowPerc ?: boolean
    icon : () => JSX.Element

}

export const CounterWidget = (props : CounterWidgetProps) => {
    const { iconColor, category, operation, dataset, dontShowPerc} = props;
    const WidgetIcon = props.icon;
    const scaleList = getScales();
    function getScales(){
        let scaleList : ScaleDevice[] = []
        dataset.forEach((scale => {
            if(scale.deviceId){
                switch(operation){
                    case "ONLINE":
                        let timeSinceOnline = Date.now() - new Date(scale.lastReportTimestampUtc).getTime()
                        if(timeSinceOnline < 8.64e+7 && (scale.enabled)){
                            scaleList.push(scale)
                        }
                        break;
                    case "DISABLED":
                        if(!scale.enabled){
                            scaleList.push(scale)
                        }
                        break;
                    case "ERROR":
                        if(!scale.lastReportTimestampUtc && (scale.enabled)){
                            scaleList.push(scale)
                            break
                        }
                        let timeSinceError = Date.now() - new Date(scale.lastReportTimestampUtc).getTime()
                        if(timeSinceError >  8.64e+7 && (scale.enabled)){
                            scaleList.push(scale)
                        }
                        break
                    case "TOTAL":
                        scaleList.push(scale)
                        break
                }
            }
            
        }))

        return scaleList
    }
    // Changes the percentages to be depicted as "0%" rather than "NaN%" if no scales are found
    let scaleListPercentage = scaleList.length > 0 ? scaleList.length / dataset.length * 100 : 0
    let scaleListPerc = scaleListPercentage % 1 ? scaleListPercentage.toFixed(2) : scaleListPercentage
  
    function statusIcon(){
        switch(operation){
            case "ONLINE":
                return(<StatusIndicator positive pulse style={{marginLeft:"1rem"}}></StatusIndicator>)
            case "WARNING":
                return(<StatusIndicator intermediary pulse style={{marginLeft:"1rem"}}></StatusIndicator>)
            case "DISABLED":
                return(<StatusIndicator pulse style={{marginLeft:"1rem", backgroundColor:'#a4abb0'}}></StatusIndicator>)
            case "TOTAL":
                return;
            default:
                return(<StatusIndicator negative pulse style={{marginLeft:"1rem"}}></StatusIndicator>)
        }
    }

    return (
      <Card border="light" className="shadow-sm">
        <Card.Body>
          <Row className="d-block d-xl-flex align-items-center">
            <Col xl={3} className="text-xl-center d-flex align-items-center justify-content-xl-center mb-xl-0">
              <div className={`icon icon-shape icon-md icon-${iconColor} rounded me-2 me-sm-0`}>
              <WidgetIcon/>

              {statusIcon()}
              </div>
              
              {/* no idea what this is */}
              {/* <div className="d-sm-none">
                <h5>{category}</h5>
                <h3 className="mb-1">{title}</h3>
              </div> */}
            </Col>
            
             <Col xs={12} xl={9} className="px-xl-0">
              <div className="d-none d-sm-block">
                <h2 style={{marginBottom:0}}><span style={{color: "#ba4735"}}> {category} {scaleList.length} </span></h2>  {/* Ex. Customers, Revenue */}
                {dontShowPerc ? null : <h4 style={{marginBottom:0}}><span style={{color: "#ba4735"}}>{scaleListPerc+"%"}</span></h4> }
              </div> 
            </Col> 
          </Row>
        </Card.Body>
      </Card>
    );
  };
  

// Used for "Scale Models" section
export const CircleChartWidget = (props) => {
    const { title, data = [] } = props;
    const series = data.map(d => d.value);  // Array of actual numerical values
    const labels = data.map(d => d.label);  // Array of the labels 
    return (
    <Card border="light" className="shadow-sm">
         <Card.Body>
            <Row>
                <h2 style={{textAlign: "center"}} className="mb-3">{title}</h2>
            </Row>
            <Row className="d-block d-xl-flex align-items-center">
            {series.length === 0 ? <label className="noDataLabel">No Data</label> :
            <>
            <Col xs={12} xl={8} className="text-xl-center d-flex align-items-center justify-content-center mb-3 mb-xl-0">
                <div style={{width: `80%`}}><CircleChart series={series} labels={labels} /></div>
            </Col>
            <Col xs={12} xl={4} className="px-xl-0">
                {data.map(d => (
                    <h4 key={`circle-element-${d.id}`} className={` text-gray ct-label-${d.id}`}>
                        <SmallScaleIcon />
                        {` ${d.label} - ${d.value}`}
                    </h4>
                ))}
            </Col>
          </>
          }
          
        </Row>
      </Card.Body>
    </Card>
  );
};


interface SimpleScaleListProps{
    title: string
    data: ScaleDevice[]
    yellowThreshold?: number
    redThreshold?: number
    offlineList?: boolean
}
export const SimpleScaleList = (props : SimpleScaleListProps) => {
  const { title, data = [], yellowThreshold = 10, redThreshold = 20, offlineList = false} = props;

  interface ListObj{
    lastReportTimestampUtc : string
    ipAddress: string
    hostname : string
    deviceId : string
    reportStr: string
  }

  let varData  : ListObj[] = []
  if(offlineList){
    const cutoff =  Date.now() - 8.64e+7

    data.forEach((scale) =>{
        if(new Date(scale.lastReportTimestampUtc).getTime() < cutoff){
            let newEntry : ListObj= {
                lastReportTimestampUtc : scale.lastReportTimestampUtc,
                ipAddress: scale.ipAddress,
                hostname : scale.hostname,
                deviceId : scale.deviceId,
                reportStr: ""
            }
            newEntry.lastReportTimestampUtc = scale.lastReportTimestampUtc
            newEntry.hostname = scale.hostname
            newEntry.deviceId = scale.deviceId
            let timeSince = Date.now() - new Date(scale.lastReportTimestampUtc).getTime()
            if(timeSince < 8.64e+7){
                newEntry.reportStr = Math.round(timeSince/3.6e+6) + " Hour(s) Ago"
            }
            else{
                newEntry.reportStr = Math.round(timeSince/8.64e+7) + " Day(s) Ago"
            }

            if(!scale.lastReportTimestampUtc){
                newEntry.reportStr = "Never"
            }

            varData.push(newEntry)
        }
    })

    varData.sort((a,b) => (new Date(a.lastReportTimestampUtc).getTime() < new Date(b.lastReportTimestampUtc).getTime()) ? -1: 1)
  
  }
  else{

    data.forEach((scale) =>{
        varData.push({
            lastReportTimestampUtc : scale.lastReportTimestampUtc,
            ipAddress: scale.ipAddress,
            hostname : scale.hostname,
            deviceId : scale.deviceId,
            reportStr: ""
        })
    })
  }


  let colorString = "#ff4d4d" //Red/Error
  if(varData.length < redThreshold){
    colorString = "#ffaa00" //Yellow/Warning
  }
  
  if(varData.length < yellowThreshold){
    colorString = "#4bd28f"
  }
  
  function renderRow(props) {
    const { index, style } = props;
  
    return (
      <ListItem style={style} key={index} component="div" disablePadding sx={{width:1}}>
        <Link
            to={{ pathname: '/AssetDetails', state: { scale: varData[index]} }}
            style = {{display: "flex", flex:1}}
        >
          <ListItemButton>
            <span style={{display: "flex", flex:1}}>

              <ScaleIcon/>

              <Typography style={{width: 125, marginLeft: "1rem"}} noWrap={true}>
              {varData[index].ipAddress}
              </Typography>

              <Typography style={{marginLeft: "1rem", flexGrow: 1, width:200}} noWrap={true}>
              <b style={{color : "#203140 "}}>{varData[index].hostname}</b>
              </Typography>

              {offlineList ? 
              <Typography style={{marginLeft: "1rem", flexGrow: 2}} noWrap={true}>
                <b style={{color : "#203140 "}}>{varData[index].reportStr}</b>
              </Typography>
              
              : null}

              <FontAwesomeIcon icon={faChevronRight} className="animate-left-3 me-3 ms-2" style={{fontSize: "1.5em"}} />



              
            </span>
          </ListItemButton>
        </Link>
      </ListItem>
    );
  }

  return (
    <Card border="light" className="shadow-sm">
      <Card.Body>
        <Row>
        {varData.length === 0 ?
                  <h2 style={{textAlign: "center"}} className="mb-3">{title}</h2> :
                  <div style={{width: '100%', backgroundColor: alpha(colorString, 0.25), borderRadius: 5}}>
                    <h2 style={{textAlign: "center", paddingTop: 8}} className="mb-3"><b style={{color : colorString}}>{varData.length}</b> {title}</h2>
                    </div>
        }
        </Row>
        <Row className="d-block d-xl-flex align-items-center">
          {varData.length === 0 ? <label className="noDataLabel">No Data</label> :
          <>
            <FixedSizeList
              height={300}
              itemSize={46}
              itemCount={varData.length}
              overscanCount={5}
            >
              {renderRow}
            </FixedSizeList>
          </>
          }
          
        </Row>
      </Card.Body>
    </Card>
  );
};


// "Acquisision" section uses this widget
export const StatisticWidget = (props : any) => {

  const { list } = props

  return (
    <Card border="light" className="shadow-sm">
      <Card.Body>
        <h4 className={"text-align-center"}>HTe Licensing Information</h4>
        <div className="d-block"></div>
        <Row>

        {list.map((item :any, index : number) => (
          <Col md={12} xl={6} className="px-0 mb-3" key={index}>
            <div className="d-flex align-items-center pt-3" key={index}>
              <div className="icon icon-shape icon-sm icon-shape-quaternary rounded me-3">
                <item.icon />
              </div>
              <div className="d-block">
                <h5 className="mb-0">{item.title}</h5>

                {item.multipleValues
                  // If over 90% of their allotted assets are used, show text in orange, if 100%, show text in red
                  ? <h3 className={`${item.value1 / item.value2 >= 1 ? "color-red " : item.value1 / item.value2 > 0.9 ? "color-orange " : ""} fw-bold mb-0`}>
                      {`${item.value1} / ${item.value2}`}
                    </h3>

                  // if passed "expirationDate", check if there are less than or equal to 30 days on the license. If yes, show text in red
                  : <h3 className={`${item.expirationDate && moment(item.value1).diff(moment(), 'days') <= 30 ? "color-red " : ""} fw-bold mb-0`}>
                      {item.value1}
                    </h3>
                }
              </div>
            </div>
          </Col>
        ))}
                  </Row>

      </Card.Body>
    </Card>
  )
};
