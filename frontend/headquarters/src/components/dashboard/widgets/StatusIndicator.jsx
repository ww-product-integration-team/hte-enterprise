import 'status-indicator/styles.css'


export default function StatusIndicator(props){

    if(props.positive){
        if(props.pulse){return(<status-indicator positive pulse style={props.style}/>)}
        else{return(<status-indicator positive style={props.style}/>)}
    }
    
    if(props.negative){
        if(props.pulse){return(<status-indicator negative pulse style={props.style}/>)}
        else{return(<status-indicator negative style={props.style}/>)}
    }

    if(props.intermediary){
        if(props.pulse){return(<status-indicator intermediary pulse style={props.style}/>)}
        else{return(<status-indicator intermediary style={props.style}/>)}
    }

    if(props.pulse){
        return(<status-indicator pulse style={props.style}/>)
    }
    

    return (<status-indicator style={props.style}/>)
}