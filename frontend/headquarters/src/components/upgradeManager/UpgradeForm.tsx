import { Dialog, DialogContent, DialogTitle, Typography } from "@material-ui/core"
import { Block, CheckCircle, Help } from "@mui/icons-material";
import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap"
import { useForm } from "react-hook-form";
import VirtualTable, { HeadCellTypes } from "../common/virtualTable/VirtualTable";
import { useAppSelector } from "../../state";
import { FormInput } from "../forms/FormHelper";
import { useHistory } from "react-router-dom";
import { Group, UpgradeDevice } from "./UpgradeManager";
import { useDispatch } from "react-redux";
import { ResponseType } from "../../types/storeTypes";
import { UpgradeAPI } from "../api";
import logger from "../utils/logger";
import { useRspHandler } from "../utils/ResponseProvider";
import { ScaleDevice } from "../../types/asset/ScaleTypes";
import { IconButton, Tooltip } from "@mui/material";

function UpgradeSelection(props: {openPopup: boolean, setOpenPopup, batchUpgrade: boolean, tableHeaders: HeadCellTypes[], selectedDevices: ScaleDevice[]}) {
    const { openPopup, setOpenPopup, batchUpgrade, tableHeaders, selectedDevices } = props

    const sortReduxData = (data: {[key: string]: any}, name: string) => {
        return Object.fromEntries(Object.entries(data).sort(([,a],[,b]) => a[name].localeCompare(b[name], 'en', {numeric: true})))
    }

    const { register, handleSubmit, reset, formState: {errors} } = useForm<SelectDevicesForGroupForm>();
    const history = useHistory();
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])
    // const [selected, setSelected] = useState<string[]>([...selectedScales]) // used for updating selectedScales
    const [selected, setSelected] = useState<string[]>([])
    // const [copiedSelectedDevices, setCopiedSelectedDevices] = useState<ScaleDevice[]>([...selectedDevices])
    const {callbacks, addCallback} = useRspHandler()
    const [selectedInDialog, setSelectedInDialog] = useState<ScaleDevice[]>([])
    const [firmwareVersions, setFirmwareVersions] = useState<{firmware: string}[]>([])
    const [modelNumbers, setModelNumbers] = useState<{modelNumber: string}[]>([])
    const reduxStores = useAppSelector(state => state.asset.stores)
    const [stores, setStores] = useState(sortReduxData(reduxStores, "storeName"))
    const reduxDepts = useAppSelector(state=>state.asset.depts)
    const [depts, setDepts] = useState(sortReduxData(reduxDepts, "deptName1"))
    const dispatch = useDispatch();

    useEffect(() => {
        // setCopiedSelectedDevices([...selectedDevices])
        let uniqueFirmwares = Array.from(new Set([...selectedDevices].map(device => device.application))).sort()
        let firmwares: {firmware: string}[] = []
        uniqueFirmwares.forEach(firmware => firmwares.push({firmware: firmware}))
        setFirmwareVersions(firmwares)
        let uniqueModels = Array.from(new Set([...selectedDevices].map(device => device.scaleModel))).filter(model => model != null).sort()
        let models: {modelNumber: string}[] = []
        uniqueModels.forEach(model => models.push({modelNumber: model}))
        setModelNumbers(models)
    }, [selectedDevices])

    useEffect(() => {
        let copiedSubset: ScaleDevice[] = []
        // for (let copied of copiedSelectedDevices) {
            // if (selected.includes(copied.deviceId)) {
            //     copiedSubset.push(copied)
        for (let select of selectedDevices) {
            if (selected.includes(select.deviceId)) {
                copiedSubset.push(select)
            }
        }
        setSelectedInDialog([...copiedSubset])
    // }, [selected, copiedSelectedDevices])
    }, [selected])

    useEffect(() => {
        let sortedStores = sortReduxData(reduxStores, "storeName")
        setStores(sortedStores)
    }, [reduxStores])
    useEffect(() => {
        let sortedDepts = sortReduxData(reduxDepts, "deptName1")
        setDepts(sortedDepts)
    }, [reduxDepts])

    interface SelectDevicesForGroupForm {
        groupName: string,
        description: string,
        firmwareVersion?: string,
        modelNumber?: string,
        storeId?: string
    }
    const defaultValues = {
        groupName: "",
        description: "",
        firmwareVersion: "",
        modelNumber: "",
        storeId: ""
    }

    const handleCloseDialog = (event) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);
        setSelected([])

        if(event != null){
            event.stopPropagation()
        }

        reset(defaultValues)
    }

    const convertToUpgradeDevices = (scales: ScaleDevice[], groupId: string) => {
        let upgradeDevices: UpgradeDevice[] = []
        scales.forEach(scale => {
            let upgradeDevice: UpgradeDevice = {
                upgradeId: "",
                deviceId: scale.deviceId,
                scaleModel: scale.scaleModel,
                bootloader: scale.loader,
                application: scale.application,
                fpc: "",
                status: "unassigned",
                statusText: "",
                nextCheck: "",
                lastUpdate: "",
                lastReport: scale.lastReportTimestampUtc,
                batchId: "",
                groupId: groupId,
                ipAddress: scale.ipAddress,
                primaryScale: scale.isPrimaryScale,
                enabled: scale.enabled,
                hostname: scale.hostname,
                pluCount: scale.pluCount,
                assignedStore: scale.storeId,
                assignedDept: scale.deptId,
                fileSelected: ""
            }
            upgradeDevices.push(upgradeDevice)
        })
        return upgradeDevices
    }

    const handleSelection = handleSubmit(async (data: SelectDevicesForGroupForm) => {
        // add Validation
        function onSuccess(response: ResponseType) {
            logger.info("Group saved successfully")
            history.push({pathname: "/UpgradeManager", state: [batchUpgrade]})
        }
        function onFail(response: ResponseType) {
            logger.info("Could not save group")
        }
        
        let group : Group = {
            groupId : data.groupName + "---id", // will be created by backend
            groupName : data.groupName,
            description: data.description,
            scales : selectedInDialog.length > 0 ? convertToUpgradeDevices(selectedInDialog, data.groupName + "---id") : convertToUpgradeDevices(/*copiedSelectedDevices*/selectedDevices, data.groupName + "---id"),
            isSelected : false,
            status : "DISABLED",
            batchGroup: batchUpgrade
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Save Group", onSuccess, onFail, {requestMsg:"Saving group...", successMsg:"Group successfully saved", failMsg:"default"})) {
            dispatch(UpgradeAPI.saveGroup(group, newID, batchUpgrade))
        }
    })

    // const filterDevices = (field: string, value) => {
    //     if (value.selectedIndex === 0) {
    //         // setCopiedSelectedDevices([...selectedDevices])
    //     } else {
    //         let filtered
    //         if (field === "storeId" || field === "deptId") {
    //             filtered = [...selectedDevices].filter(device => device[field] === value[value.selectedIndex].value)
    //         } else {
    //             filtered = [...selectedDevices].filter(device => device[field] === value[value.selectedIndex].label)
    //         }
    //         // setCopiedSelectedDevices(filtered)
    //     }
    // }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="lg" aria-labelledby='form-dialog-title'>
            <DialogTitle>
                <Typography variant="h5" component="div">
                    Select Devices to Upgrade
                </Typography>
                <Button id="actionBar2" variant="outline-primary" size="sm" onClick ={()=>{history.push({pathname: "/UpgradeManager", state: [batchUpgrade]})}}>
                    Skip to Upgrade Manager
                </Button>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSelection}>
                    <FormInput 
                        label="Group Name: "
                        id="groupName"
                        name="groupName"
                        type="text"
                        placeholder="Enter a group name"
                        register={register}
                        validation={{required: "Please enter a group name"}}
                        error={errors.groupName}
                    /> <br />

                    <FormInput 
                        label="Description: "
                        id="description"
                        name="description"
                        type="text"
                        placeholder="Enter a group description"
                        register={register}
                        validation={{}}
                        error={errors.description}
                    /> <br />
                    {/* {!batchUpgrade && <>
                        <Form.Label>
                            Filter by Firmware Version
                            <Tooltip className="info" title={
                                <Typography>Applied filters work in isolation and will need to be reset in order to be applied again</Typography>
                            }>
                                <IconButton disableRipple>
                                    <Help/>
                                </IconButton>
                            </Tooltip>
                        </Form.Label>
                        <Form.Select
                            id="firmwareVersion"
                            name="firmwareVersion"
                            onChange={e => filterDevices("application", e.currentTarget.options)}
                        >
                            <option value="">--Select a Firmware Version--</option>
                            {firmwareVersions.sort((a,b) => a.firmware === b.firmware ? 0 : a.firmware > b.firmware ? 1 : -1).map(firmware => {
                                return(
                                    <option key={firmware.firmware} value={firmware.firmware}>
                                        {firmware.firmware}
                                    </option>
                                )
                            })}
                        </Form.Select><br/>
                        <Form.Label>Filter by Model Number</Form.Label>
                        <Form.Select
                            id="modelNumber"
                            name="modelNumber"
                            onChange={e => filterDevices("scaleModel", e.currentTarget.options)}
                        >
                            <option value="">--Select a Model Number--</option>
                            {modelNumbers.sort((a,b) => a.modelNumber === b.modelNumber ? 0 : a.modelNumber > b.modelNumber ? 1 : -1).map(model => {
                                return(
                                    <option key={model.modelNumber} value={model.modelNumber}>
                                        {model.modelNumber}
                                    </option>
                                )
                            })}
                        </Form.Select><br/>
                        <Form.Label>Filter by Store</Form.Label>
                        <Form.Select
                            id="storeId"
                            name="storeName"
                            onChange={e => filterDevices("storeId", e.currentTarget.options)}
                        >
                            <option value="">--Select a Store--</option>
                            {Object.keys(stores).map(key => {
                                return(
                                    <option key={key} value={key}>
                                        {stores[key].storeName}
                                    </option>
                                )
                            })}
                        </Form.Select><br/>
                        <Form.Label>Filter by Department</Form.Label>
                        <Form.Select
                            id="deptId"
                            name="deptName1"
                            onChange={e => filterDevices("deptId", e.currentTarget.options)}
                        >
                            <option value="">--Select a Department--</option>
                            {Object.keys(depts).map(key => {
                                return(
                                    <option key={key} value={key}>
                                        {depts[key].deptName1}
                                    </option>
                                )
                            })}
                        </Form.Select><br/>
                    </>} */}

                    <VirtualTable
                        // dataSet={copiedSelectedDevices}
                        dataSet={selectedDevices}
                        headCells={tableHeaders}
                        initialSortedBy={{name: 'ipAddress'}}
                        dispatchedUrls={[]}
                        maxHeight="350px"
                        dataIdentifier="deviceId"
                        saveKey="selectedScalesForUpgradeTableHead"
                        tableName={"selectedScalesForUpgradeTable"}
                        selectedRows={selectedInDialog.length > 0 ? selected : /*copiedSelectedDevices*/selectedDevices.map((device)=>{return device.deviceId})}
                        setSelected={setSelected}
                        noSearchbar={true}
                    /> <br />

                    <Typography component="div" variant="h6" align="center" style={{flexGrow: 2}}>
                        Are these the devices you want to upgrade?
                    </Typography>
                        
                    <div className="formButtons">
                        <Button className="formButton1" onClick={handleSelection}>
                            <CheckCircle />
                            <span> Yes </span>
                        </Button>
                        <Button className="formButton1" onClick={handleCloseDialog}>
                            <Block />
                            <span> Cancel </span>
                        </Button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

export default UpgradeSelection
