import { useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
import { Button, Col, FormCheck, Row } from '@themesberg/react-bootstrap';
import { Dialog, DialogContent, DialogTitle, IconButton, Typography } from '@material-ui/core';
import { Form } from '@themesberg/react-bootstrap';
import { FormInput, FormSelect } from '../../forms/FormHelper';
import Alert from '@mui/material/Alert';
import { useDispatch } from "react-redux";
import { FileAPI, UpgradeAPI } from "../../api/index"
import logger from '../../utils/logger';
// BANNER, Region, Store, Department, Scale
import { useRspHandler } from '../../utils/ResponseProvider';
import { AddCircle, Block, Upload, Delete } from '@mui/icons-material';
import { ResponseType } from '../../../types/storeTypes';
import { AssetActions, useAppSelector } from '../../../state';
import { Batch } from '../UpgradeManager';
import { ArrowBack } from '@material-ui/icons';
import RedoIcon from '@mui/icons-material/Redo';
import EditIcon  from '@mui/icons-material/Edit';
import DatePicker from "react-datepicker";

import { parseISO, toDate } from 'date-fns'
import { Help } from '@material-ui/icons';
import { Tooltip } from '@mui/material';
import { FileForEvent, FileType, getValidFileExensions } from '../../../types/asset/FileTypes';

// POST Request

interface PostbatchProps {
    title: string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
}
interface UploadBatchFileProps {
    title: string
    setView: React.Dispatch<React.SetStateAction<"fileUpload" | "batchForm">>
    closeDialog: (event?) => void
}
export interface UploadForm {
    fileUpload: FileList | null
}
interface BatchForm {
    name: string
    description: string
    uploadStart: string
    uploadEnd: string
    upgradeStart: string
    upgradeEnd: string
    file: string
    afterUpgradeCheckIn: number // wait time for engine until scale check in
}
const defaultValues = {
    name: "",
    description: "",
    uploadTimeStart: "",
    uploadTimeEnd: "",
    upgradeTimeStart: "",
    upgradeTimeEnd: ""
};
const validateTimes = (uploadStart : Date, uploadEnd : Date, upgradeStart : Date, upgradeEnd : Date)=>{
    // starts occur before ends
    if(uploadStart.getTime() >= uploadEnd.getTime()){
        return "Invalid upload time! The end of upload time range can not be before or the same as the start time.";
    }
    //Comparing times bc users can only submit one date. Will change when multiple dates is allowed.
    if(upgradeStart.getTime() >= upgradeEnd.getTime()){
        return "Invalid upgrade time! The end of upgrade time range can not be before or the same as the start time.";
    }
    //Uploads does not overlap with ugprades
    if(upgradeStart.getTime() < uploadEnd.getTime()){
        return "Invalid batch times! Upgrade and upload times cannot overlap."
    }
    return "";
}

function PostBatch(props: PostbatchProps) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<BatchForm>();
    const reduxBatches = useAppSelector((state) => state.asset.batches)
    const [batches, setBatches] = useState((reduxBatches) ? { ...reduxBatches } : {})
    const { title, openPopup, setOpenPopup } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const [view, setView] = useState<"fileUpload" | "batchForm">("fileUpload")
    const [changeCheckInTime, setChangeCheckInTime] = useState(false)
    const [fileVersion, setFileVersion] = useState<string>("")
    const [uploadStart, setUploadStart] = useState<string | null>(null)
    const [uploadEnd, setUploadEnd] = useState<string | null>(null)
    const [upgradeDate, setUpgradeDate] = useState<string | null>(null)
    const [upgradeStartTime, setUpgradeStartTime] = useState<string | null>(null)
    const [upgradeEndTime, setUpgradeEndTime] = useState<string | null>(null)
    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();

    const serverFilesFeatures = useAppSelector(state => state.profile.featureFiles)
    const serverFilesDebs = useAppSelector(state=>state.profile.debianFiles)
    const serverFilesTars = useAppSelector(state=>state.profile.tarballFiles)
    const serverFilesInstallImgs = useAppSelector(state=>state.profile.installImageFiles)
    let serverFiles = serverFilesFeatures.concat(serverFilesDebs).concat(serverFilesTars).concat(serverFilesInstallImgs)
    let onlyValue = false
    if (serverFiles.length === 0) {
        onlyValue = true
    }
    const handleColor = () => {
        return "text-success"
    };
    const handleCloseDialog = (event?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if (event) { event.stopPropagation() }
        setOpenPopup(false);        
        setUploadStart(null)
        setUploadEnd(null)
        setUpgradeDate(null)
        setUpgradeStartTime(null)
        setUpgradeEndTime(null)
        reset(defaultValues);
    }
    const [infoDialog, setInfoDialog] = useState(false)
    const showInfoDialog = () => {
        const handleCloseDialog = (event?: any) => {
            setShowAlert([false, "Something went wrong!"])
            event.stopPropagation()
            setInfoDialog(false)
        }

        let infoText = 
        <>
            <div>Upload Times:</div>
            <p style={{"marginLeft":"2rem"}}>Upload times is the range of time when upgrade files will be sent to the appropriate scales.</p>
            <p style={{"marginLeft":"2rem"}}>When uploading files to scales, scales may run slower and consome store bandwidth.</p>
            <p style={{"marginLeft":"2rem"}}>To ensure no customer interference occurs, it is recommended to upload files when store activity is low.</p>
            <p style={{"marginLeft":"2rem"}}>Suggested hours may be:
                <ul>
                    <li>Days: Monday - Friday</li>
                    <li>Times: 11pm - 7pm</li>
                </ul>
            </p>
            <div>Upgrade Times:</div>
            <p style={{"marginLeft":"2rem"}}>Upgrade times is the range of time when scales can upgrade themselves.</p>
            <p style={{"marginLeft":"2rem"}}>When upgrading scales, scales will be decomissioned and offline during the upgrade process. It is encouraged to backup scale databases before upgrading.</p>
            <p style={{"marginLeft":"2rem"}}>Since scales are offline, select one day and a time range to upgrade selected scales to ensure.</p>
            <p style={{"marginLeft":"2rem"}}>Scale upgrades may take more than oneday.</p>
            <p style={{"marginLeft":"2rem"}}>Suggested hours may be:
                <ul>
                    <li>Days: Tuesday - Thursday</li>
                    <li>Times: 12am - 4am</li>
                </ul>
            </p>
        </>

        return (
            <Dialog open={infoDialog} onClose={handleCloseDialog} fullWidth={true} maxWidth="md">
                <DialogTitle>
                    <Typography variant="h6" component="div">Upgrading and Uploading Scale Overview</Typography>
                </DialogTitle>

                <DialogContent>
                    <Typography variant="body1" component="div">{infoText}</Typography>
                </DialogContent>
                
                <Button style={{"margin":"1rem auto"}} className="formButton1" onClick={handleCloseDialog}>
                    <span> Close </span>
                </Button>
            </Dialog>
        )
    }
    function parseFile(file: FileForEvent){
        let filename  = file.filename.toString()
        let fileVersion = filename.match(/\d+\.{1}\d+\.{1}\d+/)
        if(fileVersion){
            return fileVersion[0]
        }
        else
            return ""
    }
    const handleFileVersion = (e : React.FormEvent<HTMLSelectElement>)=>{
        let file = serverFiles[e.currentTarget.options.selectedIndex -1]
        let fileVersion : string = parseFile(file)
        setFileVersion(fileVersion)
    }

    const handleAdd = handleSubmit(async (data: BatchForm) => {    // When the Submit button is pressed
        logger.info("(PostBanner) POST request (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])
        let filtered = serverFiles.filter(file => {
            if (file.fileId === data.file) {
                return file
            }
        })
        if(!uploadStart){
            setShowAlert([true, "Please set an upload start time!"])
            return
        }
        if(!uploadEnd){
            setShowAlert([true, "Please set an upload end time!"])
            return
        }
        if(!upgradeDate){
            setShowAlert([true, "Please set an upgrade Date!"])
            return
        }
        if(!upgradeStartTime){
            setShowAlert([true, "Please set an upgrade Start Time!"])
            return
        }
        if(!upgradeEndTime){
            setShowAlert([true, "Please set an upgrade End Time!"])
            return
        }

        let upgradeStart = upgradeDate.toString().slice(0,16) + upgradeStartTime.toString().slice(16)
        let upgradeEnd = upgradeDate.toString().slice(0,16)+ upgradeEndTime.toString().slice(16)
        let checkTimes = validateTimes(new Date(uploadStart), new Date(uploadEnd), new Date(upgradeStart), new Date(upgradeEnd));
        if(checkTimes){
            setShowAlert([true, checkTimes]);
            return;
        }
        if(!filtered[0].fileId){
            setShowAlert([true, "Invalid File!"]);
            return;
        }
        
        let newBatch: Batch = {
            batchId: "",
            name: data.name,
            description: (data.description) ? data.description : "",
            uploadStart: uploadStart,
            uploadEnd: uploadEnd,
            upgradeStart: upgradeStart,
            upgradeEnd: upgradeEnd,
            fileId: filtered[0].fileId,
            fileName: filtered[0].filename,
            afterUpgradeCheckIn : data.afterUpgradeCheckIn ? data.afterUpgradeCheckIn : 40 // 40 min default
        }
        let batchkey = newBatch.batchId
        batches[batchkey] = newBatch
        const newID = Date.now()
        function onSuccess(response: ResponseType) {
            dispatch(UpgradeAPI.getBatches(newID))
            handleCloseDialog()
        }
        function onFail(response: ResponseType) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add Batch!"])
        }
        if (addCallback(callbacks, newID, "Post Batch", onSuccess, onFail)) {
            dispatch(UpgradeAPI.saveBatch(newBatch, newID))
        }
    })

    const handleBackToFile = () => {
        setView("fileUpload")
    }

    return (
        <>
        {(view === "fileUpload") ? 
            <UploadBatchFile
                title="Upload or Select a File for the Batch"
                setView={setView}
                closeDialog={handleCloseDialog}
            />
        :
        <>
            <Form className="form-control" onSubmit={handleAdd}>
                <FormInput
                    label="Batch Name: "
                    id="batchName"
                    name="name"
                    type="text"
                    placeholder="Enter a batch name"
                    register={register}
                    validation={{
                        required: "Please enter a batch name",
                        maxLength: { value: 45, message: "You exceeded the max batch name length" }
                    }}
                    error={errors.name}
                />
                <FormInput
                    label="Batch Description (optional): "
                    id="batchDescription"
                    name="description"
                    type="text"
                    placeholder="Enter a description"
                    register={register}
                    validation={{}}
                    error={errors.description}
                />

                <FormSelect
                    label="Current File: "
                    id="file"
                    name="file"
                    placeholder="--Select a File--"
                    dataset={serverFiles}
                    register={register}
                    objId="fileId"
                    objName="filename"
                    validation={{required: "Please select a file"}}
                    error={errors.file}
                    onlyValue={onlyValue}
                    disabled={undefined}
                    onChange={(e)=>{handleFileVersion(e)}}
                /> 
                <FormInput
                    label="File Version: "
                    id="fileVersion"
                    name="fileVersion"
                    type="text"
                    register={register}
                    validation={{}}
                    value = {fileVersion}
                    error={errors}
                    disabled
                    readOnly
                />
                <hr/>
                <Form.Label>Upload and Upgrade Times:</Form.Label>
                <Row>
                    <Col>
                        <Form.Label>Upload Start:</Form.Label>
                        <DatePicker
                            className="form-control" 
                            id = "uploadStart"
                            showTimeSelect
                            timeIntervals={1}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeClassName={handleColor}
                            selected={uploadStart ? uploadStart : ""} 
                            onChange={(date) => {setUploadStart(date)}}
                            error = {errors.uploadStart}
                        />
                    </Col>
                    <Col>
                        <Form.Label>Upload End:</Form.Label>
                        <DatePicker 
                            className="form-control" 
                            id = "uploadEnd"
                            showTimeSelect
                            timeIntervals={1}
                            dateFormat="MMMM d, yyyy h:mm aa"
                            timeClassName={handleColor}
                            selected={uploadEnd} 
                            onChange={(date) => setUploadEnd(date)} 
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Label>Upgrade Date:</Form.Label>
                        <DatePicker
                            className="form-control" 
                            id = "upgradeStartDate"
                            dateFormat="MMMM d, yyyy"
                            timeClassName={handleColor}
                            selected={upgradeDate} 
                            onChange={(date) => setUpgradeDate(date)} 
                        />
                    </Col>
                    <Col md = {3}>
                        <Form.Label>Upgrade Time Start:</Form.Label>
                        <DatePicker
                            selected={upgradeStartTime}
                            onChange={date => setUpgradeStartTime(date)}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={1}
                            timeCaption="Time"
                            dateFormat="h:mm aa"
                        />
                    </Col>
                    <Col md = {3}>
                        <Form.Label>Upgrade Time End:</Form.Label>
                        <DatePicker
                            selected={upgradeEndTime}
                            onChange={date => setUpgradeEndTime(date)}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={1}
                            timeCaption="Time"
                            dateFormat="h:mm aa"
                        />
                    </Col>
                </Row>
                <hr/> 
                <FormCheck  type="checkbox" className="d-flex mb-2 p-2 tableTemplate">
                    <FormCheck.Input className="me-2"                                                 
                        onClick={(e) => setChangeCheckInTime(e.target.checked)} 
                        checked = {changeCheckInTime}
                    />
                    <FormCheck.Label>
                        Change Wait Time
                    </FormCheck.Label>
                    <Tooltip  className="info" title={
                        <Typography>This value determines how long the upgrade engine waits before checking in on the scale. Default time is 40 minutes.</Typography>
                    }>
                        <IconButton size ="small" style = {{marginBottom: "5px"}} disableRipple>
                            <Help />
                        </IconButton>
                    </Tooltip>
                </FormCheck>
                {changeCheckInTime ? 
                    <FormInput
                        label="Wait Time till Check In:"
                        id="afterUpgradeCheckIn"
                        name="afterUpgradeCheckIn"
                        placeholder = "Check In Time"
                        type="number"
                        register={register}
                        validation={{}}
                        error={errors.afterUpgradeCheckIn}
                    />   
                    : null} 
                {shouldShowAlert[0] ? <><br/><Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> </>: null}
            <div className="formButtons">
                    <button className="formButton1" onClick={handleBackToFile} type="button">
                        <ArrowBack />
                        <span> Back to File Upload</span>
                    </button>
                    <button className="formButton1" onClick={handleAdd} type="submit">
                        <AddCircle />
                        <span> Add Batch </span>
                    </button>
                    <button className="formButton1" onClick={handleCloseDialog} type="button">
                        <Block />
                        <span> Cancel </span>
                    </button>
            </div>
                
                
            </Form>
            </>
        }
        </>
    )
}

function UploadBatchFile(props: UploadBatchFileProps) {

    const { register, handleSubmit, reset, formState: { errors } } = useForm<UploadForm>();
    const { title, setView, closeDialog } = props;
    const [ shouldShowAlert, setShowAlert ] = useState([false, "Something went wrong!"])
    
    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();
    useEffect(() => {
        dispatch(FileAPI.fetchFiles(Date.now()))
    }, [])

    const handleUpload = handleSubmit(async (data: UploadForm) => {
        if (!data.fileUpload || (data.fileUpload && data.fileUpload.length === 0)) {
            setShowAlert([true, "A file must be chosen for uploading. Please choose a file to upload."])
            return
        }

        const newID = Date.now()
        function onSuccess(response: ResponseType) {
            dispatch(FileAPI.fetchFiles(Date.now()))
            setView("batchForm")
        }
        function onFail(response: ResponseType) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add Batch!"])
        }

        if (data.fileUpload && addCallback(callbacks, newID, "Upload File", onSuccess, onFail)) {
            const fileData = new FormData()
            fileData.append('file', data.fileUpload[0])
            dispatch(UpgradeAPI.uploadFile(fileData, newID))
        }
    })

    return (
        <>
        <div>Upload a file or skip to create a batch</div><br/>
        <Form className="form-control" onSubmit={handleUpload}>
            <FormInput
                label="Upload a File: "
                id="fileUpload"
                name="fileUpload"
                type="file"
                accept={getValidFileExensions([FileType.FEATURE, FileType.DEBIAN, FileType.TARBALL, FileType.INSTALL_IMAGE])}
                register={register}
                validation={{}}
                error={errors.fileUpload}
            />            
        </Form>
        <div className="formButtons">
                <button className="formButton1" onClick={handleUpload} type="submit">
                    <Upload />
                    <span> Upload File</span>
                </button>
                <button className="formButton1" onClick={() => setView("batchForm")} type="button">
                    <RedoIcon />
                    <span> Skip to Batch Creation</span>
                </button>
                <button className="formButton1" onClick={event => {closeDialog(event); reset({fileUpload: null})}} type="button" value="Cancel">
                    <Block />
                    <span> Cancel</span>
                </button>
        </div>
        {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}
        </>
    )
}

function EditBatches(props: PostbatchProps) {
    const sortBatches = (btchs: {[key: string]: Batch}) => {
        return Object.fromEntries(Object.entries(btchs).sort(([,a],[,b]) => a.name.localeCompare(b.name, 'en', {numeric: true})))
    }

    const { register, handleSubmit, reset, formState: { errors } } = useForm<BatchForm>();
    const reduxBatches = useAppSelector((state) => state.asset.batches)
    const [batches, setBatches] = useState((reduxBatches) ? sortBatches({ ...reduxBatches }) : {})
    const [currentBatch, setCurrentBatch] = useState<Batch | null>(null)   
    // used to maintain batch times
    const [uploadStart, setUploadStart] = useState<string | null>(null)
    const [uploadEnd, setUploadEnd] = useState<string | null>(null)
    const [upgradeDate, setUpgradeDate] = useState<string | null>(null)
    //use the info below when upgrade window allows for multiple days:
    const [upgradeStartTime, setUpgradeStartTime] = useState<string | null>(null)
    const [upgradeEndTime, setUpgradeEndTime] = useState<string | null>(null)
    // Also, duplicate upload forms for upgrade forms

    const [changeCheckInTime, setChangeCheckInTime] = useState(false)
    const [openDelete, setOpenDelete] = useState(false)
    const { title, openPopup, setOpenPopup } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const [view, setView] = useState<"fileUpload" | "batchForm">("batchForm")

    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();

    const serverFilesFeatures = useAppSelector(state => state.profile.featureFiles)
    const serverFilesDebs = useAppSelector(state=>state.profile.debianFiles)
    const serverFilesTars = useAppSelector(state=>state.profile.tarballFiles)
    const serverFilesInstallImgs = useAppSelector(state=>state.profile.installImageFiles)
    let serverFiles = serverFilesFeatures.concat(serverFilesDebs).concat(serverFilesTars).concat(serverFilesInstallImgs)
    let onlyValue = false
    if (serverFiles.length === 0) {
        onlyValue = true
    }
    
    const handleColor = () => {
        return "text-success"
    };
    const newValues = {
        id: (currentBatch) ? currentBatch.batchId : "",
        name: (currentBatch) ? currentBatch.name : "",
        description: (currentBatch) ? currentBatch.description : "",
        uploadStart: (currentBatch) ? currentBatch.uploadStart : "",
        uploadEnd: (currentBatch) ? currentBatch.uploadEnd : "",
        upgradeStart: (currentBatch) ? currentBatch.upgradeStart : "",
        upgradeEnd: (currentBatch) ? currentBatch.upgradeEnd :"",
        fileId: currentBatch ? currentBatch.fileId : "",
        fileName: currentBatch ? currentBatch.fileName : "",
        status: "unassigned",
    };
    const defaultValues = {
        batchId: "",
        name: "",
        description: "",
        uploadStart: "",
        uploadEnd: "",
        upgradeStart: "",
        upgradeEnd: "",
        fileId: "",
        fileName: "",
        status: "",
        afterUpgradeCheckIn: 40
    };
    useEffect(() => {
        reset(defaultValues)
        reset(newValues)
    }, [currentBatch])

    const handleCloseDialog = (event?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if (event) { event.stopPropagation() }
        setOpenPopup(false);
        setCurrentBatch(null)
        setUploadStart(null)
        setUploadEnd(null)
        setUpgradeDate(null)
        setUpgradeStartTime(null)
        setUpgradeEndTime(null)
        reset(newValues)
    }
    const handleDelete = handleSubmit(async (data: BatchForm) => {
        const newID = Date.now()
        setOpenDelete(false)
        function onSuccess(response: ResponseType) {
            let tempBatches = { ...batches }
            if (currentBatch) {
                delete tempBatches[currentBatch.batchId]
                dispatch(AssetActions.setBatches({ ...tempBatches }))
            }
            setBatches(sortBatches({ ...tempBatches }))
            handleCloseDialog()
        }
        function onFail(response: ResponseType) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not delete batch"])
        }
        if (currentBatch && addCallback(callbacks, newID, "Delete Batch", onSuccess, onFail)) {
            dispatch(UpgradeAPI.deleteBatch(currentBatch.batchId, newID))
        }
    })
    const handleAdd = handleSubmit(async (data: BatchForm) => {    // When the Submit button is pressed
        logger.info("(PostBanner) POST request (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])
        let filtered = serverFiles.filter(file => {
            if (file.fileId === data.file) {
                return file
            }
        })

        let upgradeStart : string = currentBatch ? toDate(new Date(currentBatch.upgradeStart)).toString() : "" // never null (hopefully)
        let upgradeEnd : string = currentBatch ? toDate(new Date(currentBatch.upgradeEnd)).toString() : "" // never null (hopefully)
        if(uploadStart || uploadEnd || upgradeStartTime || upgradeEndTime || upgradeDate){
            if(upgradeDate){
                upgradeStart = upgradeDate.toString().slice(0,16) + upgradeStart.slice(16)
                upgradeEnd = upgradeDate.toString().slice(0,16) + upgradeEnd.slice(16)
            }
            if(upgradeStartTime){
                upgradeStart = upgradeStart.toString().slice(0,16)+ upgradeStartTime.toString().slice(16)
            }
            if(upgradeEndTime){
                upgradeEnd = upgradeEnd.toString().slice(0,16)+ upgradeEndTime.toString().slice(16)
            }
        }
        let checkTimes = validateTimes(
            new Date((uploadStart) ? uploadStart : (currentBatch) ? currentBatch.uploadStart : ""),
            new Date((uploadEnd) ? uploadEnd : (currentBatch) ? currentBatch.uploadEnd : "",), 
            new Date(upgradeStart), 
            new Date(upgradeEnd)
        )
        if(checkTimes){
            setShowAlert([true, checkTimes]);
            return;
        }
        //currentBatch must exist to allow form to be created
        let newBatch: Batch = {
            batchId: (currentBatch) ? currentBatch.batchId : "",
            name: (data.name) ? data.name : (currentBatch) ? currentBatch.batchId : "",
            description: (data.description) ? data.description : (currentBatch) ? currentBatch.description : "",
            uploadStart: (uploadStart) ? uploadStart : (currentBatch) ? currentBatch.uploadStart : "",
            uploadEnd: (uploadEnd) ? uploadEnd : (currentBatch) ? currentBatch.uploadEnd : "",
            upgradeStart: upgradeStart, // is naturally currentBatch.upgradeStart and then converted if necessary
            upgradeEnd : upgradeEnd, // is naturally currentBatch.upgradeEnd and then converted if necessary
            fileId: filtered.length > 0 && filtered[0].fileId && filtered[0].fileId !== "Unknown" ? filtered[0].fileId : currentBatch ? currentBatch.fileId : "",
            fileName: filtered.length > 0 && filtered[0].filename && filtered[0].filename !== "Unknown" ? filtered[0].filename : currentBatch ? currentBatch.fileName : "",
            afterUpgradeCheckIn : (data.afterUpgradeCheckIn) ? data.afterUpgradeCheckIn : currentBatch ? currentBatch.afterUpgradeCheckIn : 40
        }
        // Get file Id

        const newID = Date.now()
        function onSuccess(response: ResponseType) {
            dispatch(UpgradeAPI.getBatches(newID))
            handleCloseDialog()
        }
        function onFail(response: ResponseType) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not edit Batch!"])
        }
        if(addCallback(callbacks, newID, "Post Batch", onSuccess, onFail)){
            dispatch(UpgradeAPI.saveBatch(newBatch, newID))
        }
    })

    const handleView = (e: React.FormEvent<HTMLSelectElement>) => {
        let batchId = (e.currentTarget.value) ? e.currentTarget.value : ""
        if (batchId) {
            setCurrentBatch(batches[batchId.toString()])
            setUploadStart(null)
            setUploadEnd(null)
            setUpgradeDate(null)
            setUpgradeStartTime(null)
            setUpgradeEndTime(null)
        }
        else{
            setCurrentBatch(null)
            setUploadStart(null)
            setUploadEnd(null)
            setUpgradeDate(null)
            setUpgradeStartTime(null)
            setUpgradeEndTime(null)
        }
    }
    
    const handleBackToFile = () => {
        setView("fileUpload")
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="lg" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    Select Batch to Edit:
                </Typography>
            </DialogTitle>

            <DialogContent>
                <>{view === "fileUpload" ?
                    <UploadBatchFile
                        title="Upload or Select a File for the Batch"
                        setView={setView}
                        closeDialog={handleCloseDialog}
                    />
                :
                <>
                    <Form.Select
                        onChange={(e) => { handleView(e) }}
                    >
                        <>
                            {currentBatch ?
                                <option value={currentBatch.batchId}>{currentBatch.name}</option>
                            :
                                <option value="">--None--</option>
                            }
                            {Object.keys(batches).map((batch) => (
                                <>
                                    <option value={batch}>{batches[batch]["name"]}</option>
                                </>
                            ))}
                        </>
                    </Form.Select>
                    <br />

                    {(currentBatch && Object.keys(currentBatch)) ?
                        <Form className="form-control" onSubmit={handleAdd}>
                            <FormInput
                                label="Batch Name: "
                                id="batchName"
                                name="name"
                                type="text"
                                placeholder="Enter a batch name"
                                defaultValue={currentBatch.name}
                                register={register}
                                validation={{
                                    required: "Please enter a batch name",
                                    maxLength: { value: 45, message: "You exceeded the max batch name length" }
                                }}
                                error={errors.name}
                            />
                            <FormInput
                                label="Batch Description (optional): "
                                id="batchDescription"
                                name="description"
                                type="text"
                                placeholder="Enter a description"
                                defaultValue={currentBatch.description}
                                register={register}
                                validation={{}}
                                error={errors.description}
                            />
                            <hr/>
                            {/* <Form.Label>Upload and Upgrade Times:</Form.Label> */}
                            <Row>
                                <Col>
                                    <Form.Label>Upload Start:</Form.Label>
                                    <DatePicker
                                        className="form-control" 
                                        id = "uploadStart"
                                        showTimeSelect
                                        timeIntervals={1}
                                        dateFormat="MMMM d, yyyy h:mm aa"
                                        timeClassName={handleColor}
                                        selected={uploadStart? uploadStart : parseISO(currentBatch.uploadStart)} 
                                        onChange={(date) => setUploadStart(date)} 
                                        error = {errors.uploadStart}
                                    />
                                </Col>
                                <Col>
                                    <Form.Label>Upload End:</Form.Label>
                                    <DatePicker 
                                        className="form-control" 
                                        id = "uploadEnd"
                                        showTimeSelect
                                        timeIntervals={1}
                                        dateFormat="MMMM d, yyyy h:mm aa"
                                        timeClassName={handleColor}
                                        selected={uploadEnd ? uploadEnd : parseISO(currentBatch.uploadEnd)} 
                                        onChange={(date) => setUploadEnd(date)} 
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Form.Label>Upgrade Date:</Form.Label>
                                    <DatePicker
                                        className="form-control" 
                                        id = "upgradeStartDate"
                                        dateFormat="MMMM d, yyyy"
                                        timeClassName={handleColor}
                                        selected={upgradeDate ? upgradeDate : parseISO(currentBatch.upgradeStart)} 
                                        onChange={(date) => setUpgradeDate(date)} 
                                    />
                                </Col>
                                <Col md = {3}>
                                    <Form.Label>Upgrade Time Start:</Form.Label>
                                    <DatePicker
                                        selected={upgradeStartTime ? upgradeStartTime : parseISO(currentBatch.upgradeStart)}
                                        onChange={date => setUpgradeStartTime(date)}
                                        showTimeSelect
                                        showTimeSelectOnly
                                        timeIntervals={1}
                                        timeCaption="Time"
                                        dateFormat="h:mm aa"
                                    />
                                </Col>
                                <Col md = {3}>
                                    <Form.Label>Upgrade Time End:</Form.Label>
                                    <DatePicker
                                        selected={upgradeEndTime ? upgradeEndTime : parseISO(currentBatch.upgradeEnd)}
                                        onChange={date => setUpgradeEndTime(date)}
                                        showTimeSelect
                                        showTimeSelectOnly
                                        timeIntervals={1}
                                        timeCaption="Time"
                                        dateFormat="h:mm aa"
                                    />
                                </Col>
                            </Row>
                            <hr/> 
                            <Form.Label>Current File:</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder={currentBatch.fileName}
                                disabled
                                readOnly
                            />
                            <FormSelect
                                label="Select a Different File: "
                                id="file"
                                name="file"
                                placeholder="--Select a different file--"
                                dataset={serverFiles}
                                register={register}
                                objId="fileId"
                                objName="filename"
                                validation={{}}
                                error={errors.file}
                                onlyValue={onlyValue}
                                disabled={undefined}
                            />
                             <br/>    
                            <FormCheck  type="checkbox" className="d-flex mb-2 p-2 tableTemplate">
                                <FormCheck.Input className="me-2"                                                 
                                    onClick={(e) => setChangeCheckInTime(e.target.checked)} 
                                    checked = {changeCheckInTime}
                                />
                                <FormCheck.Label>
                                    Change Wait Time
                                </FormCheck.Label>
                                <Tooltip  className="info" title={
                                    <Typography>This value determines how long the upgrade engine waits before checking in on the scale. Default time is 40 minutes.</Typography>
                                }>
                                    <IconButton size ="small" style = {{marginBottom: "5px"}} disableRipple>
                                        <Help />
                                    </IconButton>
                                </Tooltip>
                            </FormCheck>
                            {changeCheckInTime ? 
                                <FormInput
                                    label="Wait Time Utill Next Check In:"
                                    id="afterUpgradeCheckIn"
                                    name="afterUpgradeCheckIn"
                                    placeholder = {currentBatch.afterUpgradeCheckIn}
                                    type="number"
                                    register={register}
                                    validation={{}}
                                    error={errors.afterUpgradeCheckIn}
                                />   
                                : null} 
                            {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                            <div className="formButtons">
                                <button className="formButton1" onClick={handleBackToFile} type="button">
                                    <ArrowBack />
                                    <span> Upload File</span>
                                </button>
                                <button className="formButton1" onClick={handleAdd} type="submit">
                                    <EditIcon />
                                    <span> Edit Batch </span>
                                </button>
                                <button className="formButton1" onClick={()=>{setOpenDelete(true)}} type="button">
                                    <Delete />
                                    <span> Delete Batch </span>
                                </button>
                                <button className="formButton1" onClick={handleCloseDialog} type="button">
                                    <Block />
                                    <span> Cancel </span>
                                </button>
                            </div>
                        </Form>
                        : null
                    }
                    <Dialog open = {openDelete}>
                        <DialogTitle> Delete Batch</DialogTitle>
                            <DialogContent>
                                <Form className="form-control" onSubmit={handleDelete}>
                                    <Form.Label id="confirmDelete">Are you sure you want to delete: <b style={{color : "#ba4735"}}>{currentBatch?.name}</b>?</Form.Label>
                                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}
                                    <div className="formButtons">
                                        <button className="formButton1" onClick={handleDelete} type="submit">
                                            <Delete style={{marginRight:5}}/>
                                            <span>Delete Batch</span>
                                        </button>
                                        <button className="formButton1" onClick={()=>{setOpenDelete(false)}} type="button">
                                            <Block style={{marginRight:10}}/>
                                            <span>Cancel</span>
                                        </button>
                                    </div>
                                </Form>
                        </DialogContent>
                    </Dialog>
                </>
                }
                </>
            </DialogContent>
        </Dialog>
    )

}


export { PostBatch, EditBatches };