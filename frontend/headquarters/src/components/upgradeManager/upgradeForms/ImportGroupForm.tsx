import {  useState,useEffect, useReducer} from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import {Upload, Block} from '@mui/icons-material';
import { Form, FormCheck } from '@themesberg/react-bootstrap';
import { useSelector, useDispatch } from "react-redux"
import { FormInput } from '../../forms/FormHelper';
import "react-datepicker/dist/react-datepicker.css";
import {store} from '../../../state/store'
import { handleResponse } from '../../../state/actions/endpointActions';
import { BannerAPI , RegionAPI, StoreAPI, DeptAPI, ScaleAPI} from "../../api/index"
import logger from '../../utils/logger';
import { useRspHandler } from '../../utils/ResponseProvider';


// =================== CATEGORY DATA ===================
let hadParsingError = false
let postArray = [] //Constructed array for sending the imported array to the
//function parseXML(xmlDoc, parentObj, currentID, shouldMerge = true) // Not applicable. Can be found in importForm

// Upload Files
function ImportGroup(props){
    //Currently used for one file to accpept a single batch
    // Will accepy a formatted .cvs

    const { register, handleSubmit, reset,  formState: {errors} } = useForm();
    const { title, openPopup, setOpenPopup} = props;
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();

    const dispatch = useDispatch();

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        input: "",
    };

    const handleCloseDialog = (event) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);

        if(event != null){
            event.stopPropagation()
        }
        
        reset({defaultValues});
    }

    // POST File Upload to API
    
    const handleImport = async (data) => {      // When the Submit button is pressed

        //HandleImport will need to be rempointed to a new endpoint

        // logger("(ImportForm) POST request (Just File): ", data.fileName[0]);
        
        // setShowAlert([false, "Something went wrong!"])
        
        // let file = data.fileName[0];
        // let reader = new FileReader();
        // let parser = new DOMParser();
        // let tree = []
        // reader.onload = function(progressEvent){    
        //     let xmlDoc = parser.parseFromString(this.result,"text/xml");
        //     hadParsingError = false
        //     postArray = []
        //     for(let item of xmlDoc.children){
        //         tree.push(parseXML(item, null ,1))
        //     }
            
            
        //     if(hadParsingError){
        //         setShowAlert([true, "Invalid file format!"])
        //         return
        //     }
        //     else{
        //         logger("(ImportForm) ", postArray)
        //         //dispatch(AssetActions.setTreeView(tree))
        //     }

        //     //Parsing was a success Now post to array
        //     //PostTree()
        //     logger("(ImportForm) ", postArray)
        //     const newID = Date.now()
        //     function onSuccess(response){
        //         logger("(ImportForm) Import Form Response:", response)
        //         handleCloseDialog()

        //         dispatch(BannerAPI.fetchBanners())
        //         dispatch(RegionAPI.fetchRegions())
        //         dispatch(ScaleAPI.fetchScales())
        //         dispatch(StoreAPI.fetchStores())
        //         dispatch(DeptAPI.fetchDepartment())
        //     }
        //     function onFail(response){
        //         logger("(ImportTree) Error importing tree", response, "ERROR")
        //         response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
        //         setShowAlert([true, "Error trying to import existing tree!"])
        //     }
        //     if(addCallback(callbacks, newID, "Import Tree", onSuccess, onFail,null)){
        //         dispatch(ScaleAPI.importTree(newID, postArray))
        //     }

        // };
        // reader.readAsText(file);
        

        
    }
        
    return (      
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="lg" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>
            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleImport)}>
                    <div className="uploadFile">
                        <FormInput
                            label="Upload a .csv file here: "
                            id="fileName"
                            name="fileName"
                            type="file"
                            accept=".csv" 
                            register={register}
                            validation={{required: "Please select a file to upload"}}
                            error={errors.fileName}
                        />
                        {/* Error importing file */}
                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleSubmit(handleImport)} type="submit">
                                <Upload />
                                <span> Import </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>     
                </Form>
        </DialogContent>
    </Dialog>
    );
}

export { ImportGroup };