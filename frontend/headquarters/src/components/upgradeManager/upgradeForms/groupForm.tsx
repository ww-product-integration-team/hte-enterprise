import { Typography } from "@material-ui/core"
import { Block } from "@mui/icons-material";
import { Edit, Help} from '@material-ui/icons';
import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap"
import { Tooltip } from '@mui/material';
import { useForm } from "react-hook-form";
import CheckIcon from '@mui/icons-material/Check';
import VirtualTable, { HeadCellTypes } from "../../common/virtualTable/VirtualTable";
import { useAppSelector } from "../../../state";
import { FormInput } from "../../forms/FormHelper";
import { Group, UpgradeDevice } from "../UpgradeManager";
import { useDispatch } from "react-redux";
import { UpgradeAPI } from "../../api/index"
import { ResponseType } from "../../../types/storeTypes"
import logger from "../../utils/logger";
import { useRspHandler } from "../../utils/ResponseProvider";

interface CreateGroupProps{
    setOpenPopup
    tableHeaders: HeadCellTypes[]
    selectedDevices: UpgradeDevice[]
    setView: React.Dispatch<React.SetStateAction<"IPrange"|"groupForm">>
    editedGroup?: Group
}

function CreateGroup(props : CreateGroupProps) {
    const { setOpenPopup, tableHeaders, selectedDevices, setView, editedGroup} = props
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler()
    const { register, handleSubmit, reset, formState: {errors} } = useForm<SelectDevicesForGroupForm>();
    const [selected, setSelected] = useState<string[]>([]) // used for updating selectedScales
    const [copiedSelectedDevices, setCopiedSelectedDevices] = useState<UpgradeDevice[]>([...selectedDevices])

    useEffect(() => {
        setCopiedSelectedDevices([...selectedDevices])
    }, [selectedDevices])

    const selectedScales = useAppSelector((state) => state.asset.upgradeSelected)

    interface SelectDevicesForGroupForm {
        groupName: string,
        description: string
    }
    const defaultValues = {
        groupName: editedGroup ? editedGroup.groupName : "",
        description: editedGroup ? editedGroup.description : ""
    }

    const handleCloseDialog = (event) => {
        setOpenPopup(false);
        setCopiedSelectedDevices([])
        if(event != null){
            event.stopPropagation()
        }

        reset(defaultValues)
    }

    const handleAddGroup = handleSubmit(async (data:SelectDevicesForGroupForm) => {
         let group : Group = {
            groupId : editedGroup ? editedGroup.groupId : data.groupName ,  // will be created by backend
            groupName : data.groupName ? data.groupName : editedGroup ? editedGroup.groupId : "" , 
            description : data.description ? data.description : editedGroup ? editedGroup.description : "" ,
            scales : copiedSelectedDevices.length ? [...copiedSelectedDevices] : editedGroup ? [...editedGroup.scales] : [],
            isSelected : false,
            status : editedGroup ? editedGroup.status : "DISABLED",
            batchGroup : true
         }
        //add Validation

        function onSuccess(response: ResponseType) {
            logger.info("Group saved successfully")
            reset(defaultValues)
            setOpenPopup(false);
            const newID = Date.now()
            dispatch(UpgradeAPI.getGroups(newID))
            dispatch(UpgradeAPI.getDevices(newID+1))
        }
        function onFail(response: ResponseType) {
            logger.info("Could not save group", response)
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Save Group", onSuccess, onFail, {requestMsg:"Saving group...", successMsg:"Successfully saved the group", failMsg:"default"})) {
            dispatch(UpgradeAPI.saveGroup(group, newID, true))
        }
    })

    // TODO: get selected scales from asset list
    // TODO: allow user to be able to modify selection once in popup

    return (
        <>
        <Form className="form-control" onSubmit={handleAddGroup}>
            <FormInput 
                label="Group Name: "
                id="groupName"
                name="groupName"
                type="text"
                defaultValue = {(editedGroup)?editedGroup.groupName : ""}
                placeholder="Enter a group name"
                register={register}
                validation={{required: "Please enter a group name"}}
                error={errors.groupName}
            /> <br />

            <FormInput 
                label="Description: "
                id="description"
                name="description"
                type="text"
                defaultValue = {(editedGroup)?editedGroup.description : ""}
                placeholder="Enter a group description"
                register={register}
                validation={{}}
                error={errors.description}
            /> <br />

            <VirtualTable
                dataSet= {selectedDevices ?  selectedDevices : editedGroup? editedGroup.scales :[] } // og code:  dataSet= {editedGroup ? editedGroup.scales : []}
                headCells={tableHeaders}
                initialSortedBy={{name: 'ipAddress'}}
                dispatchedUrls={[]}
                maxHeight="350px"
                dataIdentifier="upgradeId"
                saveKey="selectedScalesForUpgradeTableHead"
                tableName={"selectedScalesForUpgradeTable"}
                selectedRows={selectedScales}
                setSelected={setSelected}
                changeReduxUpgradeSelected={true}
            /> <br />

            <Typography component="div" variant="h6" align="center" style={{flexGrow: 2}}>
                Are these the devices you want to group?
                <Tooltip  style={{marginLeft:"2vh"}}className="info" title={
                        <Typography>If there should be scales, please select them beforehand or provide a valid IP range.</Typography>
                    }>
                    <Help />
                </Tooltip>
            </Typography>
            
        </Form>
            <div className="formButtons">
                <button className="formButton1" onClick={()=>{setView("IPrange")}} type="button">
                    <Edit />
                    <span> Select Scale IP Range </span>
                </button>
                <Button className="formButton1" onClick={handleAddGroup}>
                    <CheckIcon />
                    <span> Complete </span>
                </Button>
                <button className="formButton1" onClick={handleCloseDialog} type="button">
                    <Block />
                    <span> Cancel </span>
                </button>
            </div>
</>
    )
}


export default CreateGroup
