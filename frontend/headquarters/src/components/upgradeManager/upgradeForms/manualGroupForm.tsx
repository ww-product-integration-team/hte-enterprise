import { Dialog, DialogTitle, DialogContent, Typography, IconButton, Tooltip} from '@material-ui/core';
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Form } from '@themesberg/react-bootstrap';
import logger from '../../utils/logger';
import { lazyNode } from '../../../types/asset/TreeTypes';
import { FormInput, HiddenManualEntry } from '../../forms/FormHelper';
import { Alert } from '@mui/material';
import { AddCircle, Block, Help } from '@mui/icons-material';
import { validateIPv4 } from '../../utils/utils';
import { AlertMessage, ShowErrorMessage } from '../../../types/utils/utilTypes';
import CreateGroup from './groupForm';
import { UpgradeDevice, scaleHeadCellsUpgrade } from '../UpgradeManager';
import { useAppSelector } from '../../../state';
import { ScaleDevice } from '../../../types/asset/ScaleTypes';

/*Function to parse and validate a string containing a comma separated list of
    numbers and ranges of numbers
    @param input - The string to parse

    @returns list of numbers included in the string
*/
function parseRange(input: string) : number[] | AlertMessage{
    let output: number[] = []
    let inputArray = input.split(",")
    for (let i = 0; i < inputArray.length; i++) {
        // Check if the input is a range
        if (inputArray[i].includes("-")) {
            let rangeArray = inputArray[i].split("-")
            // Check if the range has 2 parts
            if (rangeArray.length !== 2) {
                logger.error("Invalid range: " + inputArray[i])
                return ShowErrorMessage("Invalid Exclude Range: " + inputArray[i])
            }
            // Check if the range is a number
            if (isNaN(parseInt(rangeArray[0])) || isNaN(parseInt(rangeArray[1]))) {
                logger.error("Invalid range: " + inputArray[i])
                return(ShowErrorMessage("Invalid Exclude Range: " + inputArray[i]))
            }
            // Check if the range is in the correct order
            if (parseInt(rangeArray[0]) > parseInt(rangeArray[1])) {
                logger.error("Invalid range: " + inputArray[i])
                return ShowErrorMessage("Invalid Exclude Range: " + inputArray[i])
            }
            // Add the range to the output
            for (let j = parseInt(rangeArray[0]); j <= parseInt(rangeArray[1]); j++) {
                output.push(j)
            }
        } else {
            // Check if the input is a number
            if (isNaN(parseInt(inputArray[i]))) {
                logger.error("Invalid number: " + inputArray[i])
                return ShowErrorMessage("Invalid Exclude Octet: " + inputArray[i])
            }
            output.push(parseInt(inputArray[i]))
        }
    }
    return output
}

/*Function to calculate full list of IP addresses based on the last two octets of the IP address
    @param startIP - The first IP address in the range
    @param endIP - The last IP address in the range
    @param excludeOctet3 - A comma separated list of numbers to exclude from the third octet

    @returns A full list of IP addresses in the range
*/
function calculateRange(startIP: string, endIP: string, excludeOctet3: number[] = []) {
    let startIPArray = startIP.split(".")
    let endIPArray = endIP.split(".")
    let scaleList: string[] = []
    // Calculate the range of IP addresses
    for (let i = parseInt(startIPArray[2]); i <= parseInt(endIPArray[2]); i++) {
        if(excludeOctet3.includes(i)) {continue}
        for (let j = parseInt(startIPArray[3]); j <= parseInt(endIPArray[3]); j++) {
            scaleList.push(startIPArray[0] + "." + startIPArray[1] + "." + i + "." + j)
        }
    }
    return scaleList
}


/*Form for the user to select a range of IP addresses to scan

    @param handleClose - The function to call when the user closes the form
    @param handleRangeUpdate - The function to call when the user submits the form

    @returns A form for the user to select a range of IP addresses to scan
*/
export const ManualCreateGroup = (props) => {
    const { openPopup, setOpenPopup} = props
    const lazyTree = useAppSelector(state=>state.asset.lazyTree)
    const [scaleList, setScaleList] = useState<string[]>([])
    const [view, setView] = useState<"IPrange" | "groupForm">("IPrange")

    interface ScanRangeForm {
        startIP: string,
        endIP: string,
        excludeOctet3: string,
        ManualAdd: boolean,
    }  
    const { register, handleSubmit, reset, formState: {errors} } = useForm<ScanRangeForm>();
    const [statusAlert, setStatusAlert] = useState<AlertMessage>({
        show: false,
        severity: "error",
        message:"",
    })

    const defaultValues = {startIP: "",endIP: ""}

    const handleCloseDialog = (event ?: React.MouseEvent<HTMLElement, MouseEvent>) => {
        setStatusAlert({show: false,severity: "error",message:"",})
        if(event) {event.stopPropagation()}
        setOpenPopup(false)
        reset(defaultValues)
    }
    function getLazyScales(lazyData: {[id: string]: lazyNode}, group: UpgradeDevice[]) {
        Object.keys(lazyData).forEach(key => {
            if (lazyData[key].children && lazyData[key].children.length) {
                group = getLazyScales(lazyData[key].children, group)
            } else if (lazyData[key].type === "scale" && lazyData[key].properties) {
                let device = lazyData[key].properties as ScaleDevice // This is necessary to prevent a compilation issue
                if (scaleList.includes(device.ipAddress)) {
                    group.push(createUpgradeDevice(device))
                }
            }
            return group
        })
        return group
    }
    const createUpgradeDevice = (item: ScaleDevice) => {
        let upgradeDevice: UpgradeDevice = {
            upgradeId: "",
            deviceId: item.deviceId,
            scaleModel: item.scaleModel,
            bootloader: item.loader,
            application: item.application,
            fpc: "",
            status: "unassigned",
            statusText: "",
            nextCheck: "",
            lastUpdate: "",
            lastReport: item.lastReportTimestampUtc,
            batchId: "",
            groupId: "",
            ipAddress: item.ipAddress,
            primaryScale: item.isPrimaryScale,
            enabled: item.enabled,
            hostname: item.hostname,
            pluCount: item.pluCount,
            assignedStore: item.storeId,
            assignedDept: item.deptId,
            fileSelected: ""
        }
        return upgradeDevice
    }

    const handleAddIP = handleSubmit((data : ScanRangeForm) => {
        logger.info("ScanRangeForm.tsx: handleAdd: data: ", data)
        setStatusAlert({show: false,severity: "error",message:"",})

        // Validate the IP addresses
        if (!validateIPv4(data.startIP)) {
            setStatusAlert({show: true,severity: "error",message:"The start IP address is invalid",})
            return
        }
        if (!validateIPv4(data.endIP)) {
            setStatusAlert({show: true,severity: "error",message:"The end IP address is invalid",})
            return
        }

        // Calculate list of addresses to exclude
        let excludeOctet3: any = undefined
        if(data.ManualAdd && data.excludeOctet3) {
            excludeOctet3 = parseRange(data.excludeOctet3)
            if(excludeOctet3.show) {
                setStatusAlert(excludeOctet3)
                return
            }
        }
        logger.debug("ScanRangeForm.tsx: handleAdd: excludeOctet3: ", excludeOctet3)
            

        // Calculate the list of IP addresses
        let scaleListTemp = calculateRange(data.startIP, data.endIP, excludeOctet3)
        // Check if the list of IP addresses is empty
        if (scaleListTemp.length === 0) {
            setStatusAlert({show: true,severity: "error",message:"There are zero scales in this range!"})
            return
        }

        if(scaleListTemp.length > 1000) {
            setStatusAlert({show: true,severity: "error",message:"There are too many scales in this range!"})
            return
        }
        setScaleList([...scaleListTemp])
        setView("groupForm")
        // handleRangeUpdate(scaleList)
        // Willl send dispatch to set scales within a group
    })


    return (
        <Dialog open={openPopup} fullWidth={true} maxWidth="lg" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    Select a Range of IPs to add to a group
                </Typography>
            </DialogTitle>
            <DialogContent>
            {(view === "IPrange")?
                    <>
                    <Form className="form-control" onSubmit={handleAddIP}>
                        <FormInput
                            label="Start Address: "
                            id="startIP"
                            name="startIP"
                            type="text"
                            placeholder="Enter the first IP address in the range"
                            register={register}
                            validation={{ 
                                required: "You must enter a start IP address", 
                                maxLength: { value: 15, message: "You exceeded the max IP address length" },
                            }}
                            error={errors.startIP}
                        />
                        <br />
                        <FormInput
                            label="End Address: "
                            id="endIP"
                            name="endIP"
                            type="text"
                            placeholder="Enter the last IP address in the range"
                            register={register}
                            validation={{
                                required: "You must enter an end IP address",
                                maxLength: { value: 15, message: "You exceeded the max IP address length" },
                            }}
                            error={errors.endIP}
                        />
                        <br />
                        
                        <HiddenManualEntry 
                            labelCheck=
                             {<>
                                Exclude Third Octet
                                <Tooltip className="info" style={{padding:0}} title={<Typography>Excludes a range of third octets from the range. Ex: 11-150 means that ip addresses between X.X.1.1 and X.X.150.255 will be excluded</Typography>}>
                                    <IconButton aria-label="enableAccount">
                                        <Help />
                                    </IconButton>
                                </Tooltip>
                            </>}
    
                            label="Store server IP Address"
                            id="excludeOctet3"
                            name="excludeOctet3"
                            type="text"
                            placeholder="Enter a range of octets to exclude ex. 1-10, 20-30"
                            register={register}
                            validation={{
                                required: "Please enter a range of octets to exclude",
                                maxLength: { value: 40, message: "You exceeded the max length" }
                            }}
                            error={errors.excludeOctet3} disableOther={undefined}
                        />
    
    
                        {statusAlert.show ? <Alert id="statusAlert" className="m-1" variant="filled" severity={statusAlert.severity}>{statusAlert.message}</Alert> : null}
                    </Form>
                    <div className="formButtons">
                            <button className="formButton1" onClick={handleAddIP} type="submit">
                                <AddCircle />
                                <span> Add Range </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button">
                                <Block />
                                <span> Cancel </span>
                            </button>
                            
                    </div>
                    </>
                    

                :
                <>
                    <CreateGroup
                        setOpenPopup={setOpenPopup}
                        tableHeaders={scaleHeadCellsUpgrade}
                        selectedDevices={getLazyScales(lazyTree, [])}
                        setView={setView}
                    />
                </>
                
            }
            </DialogContent>
        </Dialog>
    )
}

export const ManualEditGroup = (props) => {
    const { openPopup, setOpenPopup, group } = props
    const [scaleList, setScaleList] = useState<string[]>([])
    const [view, setView] = useState<"IPrange" | "groupForm">("groupForm")
    const selectedUpgrades = useAppSelector(state=>state.asset.upgradeSelected)
    const [upgradeSelected, setUpgradeSelected] = useState([...selectedUpgrades])
    const allScales = useAppSelector(state=>state.asset.scales)

    useEffect(() => {
        setUpgradeSelected([...selectedUpgrades])
    }, [selectedUpgrades])

    interface ScanRangeForm {
        startIP: string,
        endIP: string,
        excludeOctet3: string,
        ManualAdd: boolean,
    }  
    const { register, handleSubmit, reset, formState: {errors} } = useForm<ScanRangeForm>();
    const [statusAlert, setStatusAlert] = useState<AlertMessage>({
        show: false,
        severity: "error",
        message:"",
    })

    const defaultValues = {startIP: "",endIP: ""}

    const handleCloseDialog = (event ?: React.MouseEvent<HTMLElement, MouseEvent>) => {
        setStatusAlert({show: false,severity: "error",message:"",})
        if(event) {event.stopPropagation()}
        setOpenPopup(false)
        reset(defaultValues)
    }

    const handleAddIP = handleSubmit((data : ScanRangeForm) => {
        logger.info("ScanRangeForm.tsx: handleAdd: data: ", data)
        setStatusAlert({show: false,severity: "error",message:"",})

        // Validate the IP addresses
        if (!validateIPv4(data.startIP)) {
            setStatusAlert({show: true,severity: "error",message:"The start IP address is invalid",})
            return
        }
        if (!validateIPv4(data.endIP)) {
            setStatusAlert({show: true,severity: "error",message:"The end IP address is invalid",})
            return
        }

        // Calculate list of addresses to exclude
        let excludeOctet3: any = undefined
        if(data.ManualAdd && data.excludeOctet3) {
            excludeOctet3 = parseRange(data.excludeOctet3)
            if(excludeOctet3.show) {
                setStatusAlert(excludeOctet3)
                return
            }
        }
        logger.debug("ScanRangeForm.tsx: handleAdd: excludeOctet3: ", excludeOctet3)
            

        // Calculate the list of IP addresses
        let scaleListTemp = calculateRange(data.startIP, data.endIP, excludeOctet3)
        // Check if the list of IP addresses is empty
        if (scaleListTemp.length === 0) {
            setStatusAlert({show: true,severity: "error",message:"There are zero scales in this range!"})
            return
        }

        if(scaleListTemp.length > 1000) {
            setStatusAlert({show: true,severity: "error",message:"There are too many scales in this range!"})
            return
        }
        setScaleList([...scaleListTemp])
        setView("groupForm")
        // handleRangeUpdate(scaleList)
        // Willl send dispatch to set scales within a group
    })

    const getScalesToUse = () => {
        let scalesToUse: UpgradeDevice[] = []
        if (scaleList.length > 0) {
            for (let ipAddress of scaleList) {
                let foundScale = Object.values(allScales).find(scale => scale.ipAddress === ipAddress)
                if (foundScale !== undefined) {
                    scalesToUse.push(convertToUpgradeDevice(foundScale, group.groupId))
                }
            }
        } else {
            let currentScales = [...group.scales]
            for (let upgradeId of upgradeSelected) {
                scalesToUse.push(currentScales.find(scale => scale.upgradeId === upgradeId))
            }
        }
        return scalesToUse
    }
    const convertToUpgradeDevice = (scale: ScaleDevice, groupId: string) => {
        let upgradeDevice: UpgradeDevice = {
            upgradeId: "",
            deviceId: scale.deviceId,
            scaleModel: scale.scaleModel,
            bootloader: scale.loader,
            application: scale.application,
            fpc: "",
            status: "unassigned",
            statusText: "",
            nextCheck: "",
            lastUpdate: "",
            lastReport: scale.lastReportTimestampUtc,
            batchId: "",
            groupId: groupId,
            ipAddress: scale.ipAddress,
            primaryScale: scale.isPrimaryScale,
            enabled: scale.enabled,
            pluCount: scale.pluCount,
            hostname: scale.hostname,
            assignedStore: scale.storeId,
            assignedDept: scale.deptId,
            fileSelected: ""
        }
        return upgradeDevice
    }

    return (
        <Dialog open={openPopup} fullWidth={true} maxWidth="lg" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    Select a Range of IPs to add to a group
                </Typography>
            </DialogTitle>
            <DialogContent>
            {(view === "IPrange")?
                    <>
                    <Form className="form-control" onSubmit={handleAddIP}>
                        <FormInput
                            label="Start Address: "
                            id="startIP"
                            name="startIP"
                            type="text"
                            placeholder="Enter the first IP address in the range"
                            register={register}
                            validation={{ 
                                required: "You must enter a start IP address", 
                                maxLength: { value: 15, message: "You exceeded the max IP address length" },
                            }}
                            error={errors.startIP}
                        />
                        <br />
                        <FormInput
                            label="End Address: "
                            id="endIP"
                            name="endIP"
                            type="text"
                            placeholder="Enter the last IP address in the range"
                            register={register}
                            validation={{
                                required: "You must enter an end IP address",
                                maxLength: { value: 15, message: "You exceeded the max IP address length" },
                            }}
                            error={errors.endIP}
                        />
                        <br />
                        
                        <HiddenManualEntry 
                            labelCheck=
                             {<>
                                Exclude Third Octet
                                <Tooltip className="info" style={{padding:0}} title={<Typography>Excludes a range of third octets from the range. Ex: 11-150 means that ip addresses between X.X.1.1 and X.X.150.255 will be excluded</Typography>}>
                                    <IconButton aria-label="enableAccount">
                                        <Help />
                                    </IconButton>
                                </Tooltip>
                            </>}
    
                            label="Store server IP Address"
                            id="excludeOctet3"
                            name="excludeOctet3"
                            type="text"
                            placeholder="Enter a range of octets to exclude ex. 1-10, 20-30"
                            register={register}
                            validation={{
                                required: "Please enter a range of octets to exclude",
                                maxLength: { value: 40, message: "You exceeded the max length" }
                            }}
                            error={errors.excludeOctet3} disableOther={undefined}
                        />
    
    
                        {statusAlert.show ? <Alert id="statusAlert" className="m-1" variant="filled" severity={statusAlert.severity}>{statusAlert.message}</Alert> : null}
                    </Form>
                    <div className="formButtons">
                            <button className="formButton1" onClick={handleAddIP} type="submit">
                                <AddCircle />
                                <span> Add Range </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button">
                                <Block />
                                <span> Cancel </span>
                            </button>                      
                    </div>
                    </>
                    

                :
                <>
                    <CreateGroup
                        setOpenPopup={setOpenPopup}
                        tableHeaders={scaleHeadCellsUpgrade}
                        selectedDevices={getScalesToUse()}
                        setView={setView}
                        editedGroup={group}
                    />
                </>
                
            }
            </DialogContent>
        </Dialog>
    )
}