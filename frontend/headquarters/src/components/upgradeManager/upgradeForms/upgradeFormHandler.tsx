import { EditBatches, PostBatch } from "./batchForm"
import { Button, Card, Col, Row } from '@themesberg/react-bootstrap';

//Forms 
import { ManualCreateGroup } from "./manualGroupForm";
import { Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Typography } from "@material-ui/core";
import { Group, StyledVirtualTable, Timeframe, scaleHeadCellsUpgrade, manualUpgradeHeadCells } from "../UpgradeManager";
import { useDispatch } from "react-redux";
import { ScaleAPI, UpgradeAPI } from "../../api";
import { useRspHandler } from "../../utils/ResponseProvider";
import { ResponseType } from "../../../types/storeTypes";
import logger from '../../utils/logger';
import { useAppSelector } from "../../../state";
import { Help } from '@material-ui/icons';
import { Checkbox, FormControlLabel, Tooltip } from '@mui/material';
import { useEffect, useState } from "react";
import VirtualTable, { HeadCellTypes } from "../../common/virtualTable/VirtualTable";
import { LocalizationProvider, TimePicker } from "@mui/x-date-pickers";
import { Controller, useForm } from "react-hook-form";
import dayjs from 'dayjs';
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { notify } from "reapop";
import { ScaleDevice } from "../../../types/asset/ScaleTypes";
import { FileForEvent } from "../../../types/asset/FileTypes";

interface UpgradePopUpProps {
    show: boolean
    onHide: React.Dispatch<React.SetStateAction<boolean>>
    title: string
    type: string // "UploadFiles" | "UpgradeSecondary" | "AutomateUpload" | "DeleteFiles"
    groups: { [key: string]: Group }
    batchUpgrade: boolean
    hybridMultipath: boolean
}
interface ScaleInfoCheck {
    upgradeId: string
    deviceId: string
    ipAddress: string
    canConnect: boolean
    hasFile: boolean
    currentFirmware: string
    targetFirmware: string
}
interface TimeframeForm {
    uploadPrimaryStart: string
    uploadPrimaryEnd: string
    uploadSecondaryStart: string
    uploadSecondaryEnd: string
    upgradeTimeStart: string
    upgradeTimeEnd: string
}

export function UpgradeFormHandler(props: UpgradePopUpProps) {
    let { show: dialogShow, title, type, onHide, groups, batchUpgrade, hybridMultipath } = props
    const dispatch = useDispatch()
    const { callbacks, addCallback } = useRspHandler()

    const {control } = useForm<TimeframeForm>()
    
    const reduxFeatureFiles = useAppSelector(state=>state.profile.featureFiles)
    const reduxDebianFiles = useAppSelector(state=>state.profile.debianFiles)
    const reduxTarballFiles = useAppSelector(state=>state.profile.tarballFiles)
    const reduxInstallImageFiles = useAppSelector(state=>state.profile.installImageFiles)
    const [upgradeFiles, setUpgradeFiles] = useState<FileForEvent[]>([])

    const upgradeDevices = useAppSelector(state => state.asset.upgradeDevices)
    const selectedScalesRedux = useAppSelector(state => state.asset.upgradeSelected)
    const [selectedScales, setSelectedScales] = useState([...selectedScalesRedux])
    const primaries = useAppSelector(state => state.asset.primaryScales)
    const [primaryScales, setPrimaryScales] = useState([...primaries])
    const [checkInfo, setCheckInfo] = useState<ScaleInfoCheck[]>([])
    const [scales, setScales] = useState<ScaleDevice[]>([])
    useEffect(() => {
        setSelectedScales([...selectedScalesRedux])
    }, [selectedScalesRedux])
    useEffect(() => {
        setPrimaryScales([...primaries])
    }, [primaries])
    useEffect(() => {
        dispatch(ScaleAPI.fetchScales(Date.now()+1, undefined, null, null, setScales))
    }, [])
    useEffect(() => {
        let filesToAdd: FileForEvent[] = []
        reduxFeatureFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        reduxDebianFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        reduxTarballFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        reduxInstallImageFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        setUpgradeFiles(filesToAdd)
    }, [reduxFeatureFiles, reduxDebianFiles, reduxInstallImageFiles, reduxTarballFiles])

    function handleEnableGroups(groups: { [key: string]: Group }) {
        let selected: Group[] = []
        Object.keys(groups).forEach(key => {
            if (groups[key].isSelected) {
                selected.push(groups[key])
            }
        })

        function onSuccess(response: ResponseType) {
            logger.info("Groups successfully enabled")
            dispatch(UpgradeAPI.getGroups(Date.now()))
            dispatch(ScaleAPI.fetchScales(Date.now()+1, undefined, null, null, setScales))
        }
        function onFail(response: ResponseType) {
            logger.info("Failed to enable groups: " + response.response.error)
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Enable Groups", onSuccess, onFail, { requestMsg: "Enabling Groups...", successMsg: "Groups successfully enabled", failMsg: "Failed to enable groups" })) {
            dispatch(UpgradeAPI.enableGroups(selected, newID))
        }
    }

    const handleCheckInfo = () => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully retrieved scales' utility info")

            let infoResponse: ScaleInfoCheck[] = []
            for (let res of response.response) {
                let device = scales.filter(device => device.deviceId === res.deviceId)[0]
                let newInfo: ScaleInfoCheck = {
                    upgradeId: res.upgradeId,
                    deviceId: res.deviceId,
                    ipAddress: device.ipAddress,
                    canConnect: res.canConnect,
                    hasFile: res.hasFile,
                    currentFirmware: device ? device.application : "",
                    targetFirmware: res.targetFirmware ? res.targetFirmware : ""
                }
                infoResponse.push(newInfo)
            }
            setCheckInfo(infoResponse)
        }
        function onFail(response: ResponseType) {
            logger.error("Failed to get scales' utilty info")
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Get Utilities Info", onSuccess, onFail, { requestMsg: "Checking Scale Info...", successMsg: "Successfully retrieved scale info", failMsg: "Error retrieving scale info" })) {
            dispatch(UpgradeAPI.getUtilityInfo(newID, selectedScales, batchUpgrade))
        }
    }

    let toPrimaries = true
    let anyPrimaries = false

    const handleUpload = () => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully uploaded file to " + toPrimaries ? "primaries" : "secondaries")
            onHide(false)
            dispatch(UpgradeAPI.getGroups(Date.now()))
            dispatch(UpgradeAPI.getBatches(Date.now()+1))
            dispatch(UpgradeAPI.getDevices(Date.now()+2))
        }
        function onFail(response: ResponseType) {
            logger.error("Error uploading file to " + toPrimaries ? "primaries" : "secondaries")
        }

        let selectedIpAddresses: string[] = []
        for (let selected of selectedScales) {
            selectedIpAddresses.push(upgradeDevices[selected].ipAddress)
            if (upgradeDevices[selected] && upgradeDevices[selected].status !== "awaiting_upload" && upgradeDevices[selected].status !== "ready_to_upload") {
                if (!batchUpgrade && upgradeDevices[selected].status === "upload_failed") {
                    continue
                }
                dispatch(notify("Device " + upgradeDevices[selected].ipAddress + " is not in the correct state to initiate an upload.", "error"))
                return
            }
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Upload to " + toPrimaries ? "Primaries" : "Secondaries", onSuccess, onFail, { requestMsg: "Beginning file upload to scales...", successMsg: "File upload to scales successfully started", failMsg: "Error starting file upload to scales" })) {
            if (batchUpgrade) {
                dispatch(UpgradeAPI.uploadBatchFileToScales(newID, selectedScales))
            } else {
                let file = upgradeFiles.filter(file => file.fileId === upgradeDevices[selectedScales[0]].fileSelected)[0]
                dispatch(UpgradeAPI.uploadManualFileToScales(newID, file.filename, file.size, selectedIpAddresses))
            }
        }
    }

    const handleUpgrade = () => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully upgraded scales")
            onHide(false)
            dispatch(UpgradeAPI.getGroups(Date.now()))
            dispatch(UpgradeAPI.getBatches(Date.now()+1))
            dispatch(UpgradeAPI.getDevices(Date.now()+2))
        }
        function onFail(response: ResponseType) {
            logger.error("Error upgrading scales")
        }

        let selectedIpAddresses: string[] = []
        for (let selected of selectedScales) {
            selectedIpAddresses.push(upgradeDevices[selected].ipAddress)
            if (upgradeDevices[selected] && upgradeDevices[selected].status !== "file_uploaded_awaiting_upgrade" && upgradeDevices[selected].status !== "ready_to_upgrade") {
                if (!batchUpgrade && (upgradeDevices[selected].status === "upgrade_failed" || upgradeDevices[selected].status === "upload_failed")) {
                    continue;
                }
                dispatch(notify("Device " + upgradeDevices[selected].ipAddress + " is not in the correct state to initiate an upgrade.", "error"))
                return
            }
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Upgrade Scales", onSuccess, onFail, {requestMsg:"Starting scale upgrade...", successMsg:"Successfully started scale upgrade", failMsg:"Error starting scale upgrade"})) {
            if (batchUpgrade) {
                dispatch(UpgradeAPI.upgradeScales(newID, selectedScales, upgradeFileDelete))
            } else {
                let file = upgradeFiles.filter(file => file.fileId === upgradeDevices[selectedScales[0]].fileSelected)[0]
                dispatch(UpgradeAPI.applyManualFileToScales(newID, file.filename, file.size, selectedIpAddresses, upgradeFileDelete))
            }
        }
    }

    const handleTimeframes = () => {
        function onSuccess(response: ResponseType) {
            logger.info("Timeframes set successfully!")
            onHide(false)
        }
        function onFail(response: ResponseType) {
            logger.error("Error setting Timeframes")
        }

        let onlyHours = {...timeframe}
        onlyHours.uploadPrimaryStart = new Date(timeframe.uploadPrimaryStart).getHours().toString()
        onlyHours.uploadPrimaryEnd = new Date(timeframe.uploadPrimaryEnd).getHours().toString()
        onlyHours.uploadSecondaryStart = new Date(timeframe.uploadSecondaryStart).getHours().toString()
        onlyHours.uploadSecondaryEnd = new Date(timeframe.uploadSecondaryEnd).getHours().toString()
        onlyHours.upgradeTimeStart = new Date(timeframe.upgradeTimeStart).getHours().toString()
        onlyHours.upgradeTimeEnd = new Date(timeframe.upgradeTimeEnd).getHours().toString()

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Set Timeframes", onSuccess, onFail, {requestMsg:"Setting Timeframes...",successMsg:"Timeframes set successfully!",failMsg:"default"})) {
            dispatch(UpgradeAPI.manageTimeframes(newID, onlyHours))
        }
    }

    const [upgradeFileDelete, setUpgradeFileDelete] = useState(false)

    const timeframes = useAppSelector(state=>state.asset.timeframes)
    const [timeframe, setTimeframe] = useState<Timeframe>({...timeframes})
    const modifyTimeframe = (date, id: string) => {
        let temp = {...timeframe}
        temp[id] = date
        setTimeframe(temp)
    }
    useEffect(() => {
        setTimeframe({...timeframes})
    }, [timeframes])

    let devicesWithBatches
    let devicesWithManualFiles
    let groupedScales
    if (batchUpgrade) {
        devicesWithBatches = Object.values(upgradeDevices).filter(device => {if (device.batchId) return device ; return null}).map(device => device.upgradeId)
        groupedScales = Object.values(groups).flatMap(group => group.scales).filter(scale => scale && devicesWithBatches.includes(scale.upgradeId))
    } else {
        devicesWithManualFiles = Object.values(upgradeDevices).filter(device => {if (device.fileSelected) return device; return null}).map(device => device.upgradeId)
        groupedScales = Object.values(groups).flatMap(group => group.scales).filter(scale => scale && devicesWithManualFiles.includes(scale.upgradeId))
    }
    let primaryScaleDevices
    let secondaryScaleDevices
    switch (type) {
        case ("createBatch"):
            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >
                    <DialogTitle>
                        Create Batch
                    </DialogTitle>
                    <DialogContent>
                        <PostBatch
                            title="Create Batch"
                            openPopup={dialogShow}
                            setOpenPopup={onHide}
                        />
                    </DialogContent>
                </Dialog>

            );
        case ("editBatches"):
            return (
                <EditBatches
                    title="Edit batch"
                    openPopup={dialogShow}
                    setOpenPopup={onHide}
                />
            );
        // case ("importGroup"):
        //     return (
        //         <ImportGroup
        //             title="Import Group"
        //             openPopup={dialogShow}
        //             setOpenPopup={onHide}
        //         />
        //     );
        // case("exportGroup"):
        case ("createGroup"):
            return (
                <ManualCreateGroup
                    openPopup={dialogShow}
                    setOpenPopup={onHide}
                />
            )
        case ("checkScales"):
            const headers: HeadCellTypes[] = [
                { id: "ipAddress", sorted: false, searchable: false, label: "IP Address", isShowing: true, mandatory: true },
                { id: "canConnect", sorted: false, searchable: false, label: "Can Contact Scale", isShowing: true, mandatory: true },
                { id: "hasFile", sorted: false, searchable: false, label: "Scale Has File", isShowing: true, mandatory: true },
                { id: "currentFirmware", sorted: false, searchable: false, label: "Current Application", isShowing: true, mandatory: true },
                { id: "targetFirmware", sorted: false, searchable: false, label: "Target Application", isShowing: true, mandatory: true }
            ]
            const preHeaders: HeadCellTypes[] = [
                { id: 'ipAddress',        sorted: false, searchable: false,  label: 'IP Address',          isShowing: true, mandatory: true,  },
                ...batchUpgrade ? [{ id: 'batchId',    sorted: false, searchable: false,  label: 'Batch',          isShowing: true, mandatory: true,  }] : [],
                { id: 'assignedDept',     sorted: false, searchable: false,   label: 'Department', isShowing: true, mandatory: true, },
                { id: 'assignedStore',    sorted: false, searchable: false,   label: 'Store',      isShowing: true, mandatory: true, },
                { id: 'application',      sorted: false, searchable: false,   label: 'Application Version', isShowing: true, mandatory: true, },
                { id: 'hostname',         sorted: false, searchable: false,   label: 'Host Name',           isShowing: true, mandatory: true , },
            ]

            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >
                    <DialogTitle>
                        {title}
                        
                        <Tooltip  className="info" title={
                            <Typography>Click Check Info to get information on the scales below.</Typography>
                        }>
                            <IconButton size ="small"  disableRipple >
                                <Help />
                            </IconButton>
                        </Tooltip>
                    </DialogTitle>

                    <DialogContent>
                        {checkInfo.length > 0?
                        <Typography variant = "body1">Found the following information for the scales:</Typography>
                        :
                        <Typography variant = "body1">Click Check Info to get info for the follow scales:</Typography>
                        }
                        <Card>
                            {checkInfo.length > 0?
                            <>
                            {/*console.log is required for Check Scale Table to load properly*/}
                            {/*table populates with "null" values if console.log is removed, should be fixed if web socket is implemented*/}
                            {<>{console.log([...checkInfo])  }</>}
                                <VirtualTable
                                    tableName="scaleInfoCheck"
                                    dataSet={[...checkInfo]}
                                    headCells={headers}
                                    initialSortedBy={{ name: '' }}
                                    saveKey="scaleInfoCheckHead"
                                />
                            </>
                            :
                            <>
                                <VirtualTable
                                    tableName = "scalesToCheckInfo"
                                    dataSet={selectedScales.map((scaleId)=>{return upgradeDevices[scaleId]})}
                                    headCells={preHeaders}
                                    initialSortedBy={{name: ''}}
                                    saveKey="scalesToCheckInfo"
                                />
                            </>
                        }
                        </Card>
                    </DialogContent>
                    <DialogActions>
                            <Button className="formButton1" onClick={handleCheckInfo}>
                                Check Info
                            </Button>
                            <Button className="formButton1" onClick={() => { onHide(false); setCheckInfo([]) }}>
                                Cancel
                            </Button>
                    </DialogActions>
                </Dialog>
            )
        case ("enableGroups"):
            // Enable groups will enable groups, setUpgradeDevices with batchIds and set initial status
            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >
                    <DialogTitle>
                        {title}
                    </DialogTitle>

                    <DialogContent>
                        {StyledVirtualTable(groups, false, false, false, true)}
                    </DialogContent>
                    <DialogActions>
                        <Button className="formButton1" onClick={() => { handleEnableGroups(groups); onHide(false) }}>
                            Enable
                        </Button>
                        <Button className="formButton1" onClick={() => { onHide(false) }}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            )
        case ("uploadToPrimary"):
            toPrimaries = true
            anyPrimaries = false
            if (!hybridMultipath) {
                anyPrimaries = true
            } else if (primaryScales.length > 0) {
                anyPrimaries = true

                primaryScaleDevices = groupedScales.filter(scale => primaryScales.includes(scale.upgradeId))
            }

            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >
                    <DialogTitle>
                        {title}
                    </DialogTitle>

                    <DialogContent>
                        <VirtualTable
                            tableName="uploadToPrimaryTable"
                            dataSet={anyPrimaries ? primaryScaleDevices ? primaryScaleDevices : groupedScales : []}
                            headCells={batchUpgrade ? scaleHeadCellsUpgrade : manualUpgradeHeadCells}
                            initialSortedBy={{ name: '' }}
                            dataIdentifier="upgradeId"
                            saveKey="uploadToPrimaryTableHead"
                            selectedRows={selectedScales}
                            changeReduxUpgradeSelected={true}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button id="actionDropdown1" onClick={() => {handleUpload(); onHide(false)}}>
                            Upload
                        </Button>
                        <Button id="actionDropdown1" onClick={() => { onHide(false) }}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            )
        case ("uploadToSecondary"):
            toPrimaries = false
            anyPrimaries = false
            if (primaryScales.length > 0) {
                anyPrimaries = true

                secondaryScaleDevices = groupedScales.filter(scale => !primaryScales.includes(scale.upgradeId))
            }

            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >
                    <DialogTitle>
                        {title}
                    </DialogTitle>

                    <DialogContent>
                        <VirtualTable
                            tableName="uploadToSecondaryTable"
                            dataSet={anyPrimaries ? secondaryScaleDevices : groupedScales}
                            headCells={batchUpgrade ? scaleHeadCellsUpgrade : manualUpgradeHeadCells}
                            initialSortedBy={{ name: '' }}
                            dataIdentifier="upgradeId"
                            saveKey="uploadToSecondaryTableHead"
                            selectedRows={selectedScales}
                            changeReduxUpgradeSelected={true}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button id="actionDropdown1" onClick={() => {handleUpload(); onHide(false)}}>
                            Upload
                        </Button>
                        <Button id="actionDropdown1" onClick={() => { onHide(false) }}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            )
        case ("upgradePrimary"):
            toPrimaries = true
            anyPrimaries = false
            if (!hybridMultipath) {
                anyPrimaries = true
            } else if (primaryScales.length > 0) {
                anyPrimaries = true

                primaryScaleDevices = groupedScales.filter(scale => primaryScales.includes(scale.upgradeId))
            }

            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >
                    <DialogTitle>
                        {title}
                    </DialogTitle>

                    <DialogContent>
                        <FormControlLabel control={<Checkbox />} checked={upgradeFileDelete} onChange={(e)=>{setUpgradeFileDelete((e.target as any).checked)}} 
                        label="Delete Upgrade Files After Upgrade" style={{marginLeft: ".5rem"}}/>

                        <VirtualTable
                            tableName="upgradeToPrimaryTable"
                            dataSet={anyPrimaries ? primaryScaleDevices ? primaryScaleDevices : groupedScales : []}
                            headCells={batchUpgrade ? scaleHeadCellsUpgrade : manualUpgradeHeadCells}
                            initialSortedBy={{ name: '' }}
                            dataIdentifier="upgradeId"
                            saveKey="upgradeToPrimaryTableHead"
                            selectedRows={selectedScales}
                            changeReduxUpgradeSelected={true}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button id="actionDropdown1" onClick={() => {handleUpgrade(); onHide(false)}}>
                            Upgrade
                        </Button>
                        <Button id="actionDropdown1" onClick={() => { onHide(false) }}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            )
        case ("upgradeSecondary"):
            toPrimaries = false
            anyPrimaries = false
            if (primaryScales.length > 0) {
                anyPrimaries = true

                secondaryScaleDevices = groupedScales.filter(scale => !primaryScales.includes(scale.upgradeId))
            }

            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >
                    <DialogTitle>
                        {title}
                    </DialogTitle>

                    <DialogContent>
                    <VirtualTable
                            tableName="upgradeToSecondaryTable"
                            dataSet={anyPrimaries ? secondaryScaleDevices : groupedScales}
                            headCells={batchUpgrade ? scaleHeadCellsUpgrade : manualUpgradeHeadCells}
                            initialSortedBy={{ name: '' }}
                            dataIdentifier="upgradeId"
                            saveKey="upgradeToSecondaryTableHead"
                            selectedRows={selectedScales}
                            changeReduxUpgradeSelected={true}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button id="actionDropdown1" onClick={() => {handleUpgrade(); onHide(false)}}>
                            Upgrade
                        </Button>
                        <Button id="actionDropdown1" onClick={() => { onHide(false) }}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            )
        case "manageTimeframes":

            return(
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="md"
                    fullWidth={true}
                >
                    <DialogTitle>
                        {title}
                    </DialogTitle>

                    <DialogContent>
                        <label style={{fontSize:"1rem"}}>Uploading to Primaries Timeframe</label><br/><br/>
                        <Row>
                            <Col>
                                <Controller
                                    control={control}
                                    name="uploadPrimaryStart"
                                    render={() => (
                                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                                            <TimePicker
                                                label={"Start Time"}
                                                value={dayjs(timeframe.uploadPrimaryStart)}
                                                onChange={date=>modifyTimeframe(date, "uploadPrimaryStart")}
                                                views={['hours']}
                                            />
                                        </LocalizationProvider>
                                    )}
                                />
                            </Col>
                            <Col>
                                <Controller
                                    control={control}
                                    name="uploadPrimaryEnd"
                                    render={() => (
                                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                                            <TimePicker
                                                label={"End Time"}
                                                value={dayjs(timeframe.uploadPrimaryEnd)}
                                                onChange={date=>modifyTimeframe(date, "uploadPrimaryEnd")}
                                                views={['hours']}
                                            />
                                        </LocalizationProvider>
                                    )}
                                />
                            </Col>
                        </Row>
                        <hr/>
                        <label style={{fontSize:"1rem"}}>Uploading to Secondaries Timeframe</label><br/><br/>
                        <Row>
                            <Col>
                                <Controller
                                    control={control}
                                    name="uploadSecondaryStart"
                                    render={() => (
                                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                                            <TimePicker
                                                label={"Start Time"}
                                                value={dayjs(timeframe.uploadSecondaryStart)}
                                                onChange={date=>modifyTimeframe(date, "uploadSecondaryStart")}
                                                views={['hours']}
                                            />
                                        </LocalizationProvider>
                                    )}
                                />
                            </Col>
                            <Col>
                                <Controller
                                    control={control}
                                    name="uploadSecondaryEnd"
                                    render={() => (
                                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                                            <TimePicker
                                                label={"End Time"}
                                                value={dayjs(timeframe.uploadSecondaryEnd)}
                                                onChange={date=>modifyTimeframe(date, "uploadSecondaryEnd")}
                                                views={['hours']}
                                            />
                                        </LocalizationProvider>
                                    )}
                                />
                            </Col>
                        </Row>
                        <hr/>
                        <label style={{fontSize:"1rem"}}>Upgrading Timeframe</label><br/><br/>
                        <Row>
                            <Col>
                                <Controller
                                    control={control}
                                    name="upgradeTimeStart"
                                    render={() => (
                                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                                            <TimePicker
                                                label={"Start Time"}
                                                value={dayjs(timeframe.upgradeTimeStart)}
                                                onChange={date=>modifyTimeframe(date, "upgradeTimeStart")}
                                                views={['hours']}
                                            />
                                        </LocalizationProvider>
                                    )}
                                />
                            </Col>
                            <Col>
                                <Controller
                                    control={control}
                                    name="upgradeTimeEnd"
                                    render={() => (
                                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                                            <TimePicker
                                                label={"End Time"}
                                                value={dayjs(timeframe.upgradeTimeEnd)}
                                                onChange={date=>modifyTimeframe(date, "upgradeTimeEnd")}
                                                views={['hours']}
                                            />
                                        </LocalizationProvider>
                                    )}
                                />
                            </Col>
                        </Row>
                    </DialogContent>

                    <DialogActions>
                        <Button id="actionDropdown1" onClick={handleTimeframes}>Set Timeframes</Button>
                        <Button id="actionDropdown1" onClick={()=>onHide(false)}>Cancel</Button>
                    </DialogActions>
                </Dialog>
            )
        default:
            return (
                <Dialog
                    open={dialogShow}
                    onClose={onHide}
                    maxWidth="lg"
                    fullWidth={true}
                >

                    <DialogContent>
                        <Card>
                            <h1>
                                This form has not been created yet
                            </h1>
                        </Card>
                    </DialogContent>
                    <DialogActions>
                        <Button id="actionDropdown1" onClick={() => { onHide(false) }}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            )
    }
}