import { useEffect, useState} from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogTitleClassKey, DialogContent, DialogTitle, Typography } from '@material-ui/core';
import { Form } from '@themesberg/react-bootstrap';
import { FormInput } from '../../forms/FormHelper';
import Alert from '@mui/material/Alert';
import { useDispatch} from "react-redux";
import { BannerAPI , ScaleAPI} from "../../api/index"
import logger from '../../utils/logger';
// BANNER, Region, Store, Department, Scale
import {useRspHandler} from '../../utils/ResponseProvider';
import { AddCircle, Block, Delete, Edit } from '@mui/icons-material';
import { Node } from '../../../types/asset/TreeTypes';
import { AssetTypes } from '../../../types/asset/AssetTypes';
import { ResponseType } from '../../../types/storeTypes';
import { AssetActions, useAppSelector } from '../../../state';
import { Batch } from '../UpgradeManager';
import useEnhancedEffect from '@mui/material/utils/useEnhancedEffect';
import { HeadCellTypes } from '../../common/virtualTable/VirtualTable';
import VirtualTableProps from '../../common/virtualTable/VirtualTable';

// POST Request

interface PostFileProps{
    title : string
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
}
interface uploadFileForm{
    fileId: string
}

const defaultValues = {
    fileId: "",
};
export interface upgradeFile { 
    name : string
    type : string 
    startDate: string
    endDate: string
    uploadDate: string
}
export const upgradeFileHeadCells : HeadCellTypes[] = [        
    { id: "checkmark",            sorted: false, searchable: false, label: "checkmark",                 isShowing: true, mandatory: true,},
    { id: 'enabled',          sorted: false, searchable: false, label: 'Enabled / Disabled',        isShowing: false, mandatory: false, },
    { id: 'uploadDate',          sorted: false, searchable: false, label: 'Upload Date',        isShowing: false, mandatory: false, },
    { id: 'startDate',          sorted: false, searchable: false, label: 'Start Date',        isShowing: false, mandatory: false, },
    { id: 'endDate',          sorted: false, searchable: false, label: 'End Date',        isShowing: false, mandatory: false, },
    { id: 'enabled',          sorted: false, searchable: false, label: 'Enabled / Disabled',        isShowing: false, mandatory: false, },
    { id: 'size',          sorted: false, searchable: false, label: 'Size',        isShowing: false, mandatory: false, },
    { id: 'name',          sorted: false, searchable: false, label: 'File Name',        isShowing: false, mandatory: false, },
  ];
function UploadUpgradeFile(props : PostFileProps) {
    const { register, handleSubmit, reset,  formState: {errors} } = useForm<uploadFileForm>();
    const reduxBatches = useAppSelector((state)=> state.asset.batches)
    const [selectedFiles, setSelectedFiles] = useState<string[]>([])
    const [batches, setBatches] = useState((reduxBatches)? {...reduxBatches} : {})
    const reduxUpgradeFiles = useAppSelector((state)=>state.asset.upgradeFiles)
    const [upgradeFiles, setUpgradeFiles] = useState<upgradeFile[]>((reduxUpgradeFiles)? reduxUpgradeFiles: [])
    const { title, openPopup, setOpenPopup } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();

    const handleCloseDialog = (event ?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if(event){ event.stopPropagation() }
        setOpenPopup(false);
        reset(defaultValues);
    }
    
    const handleAdd = handleSubmit(async (data : uploadFileForm) => {    // When the Submit button is pressed
        logger.info("(PostBanner) POST request (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])
        let upgradeFile :upgradeFile = {
            name : "UpgradeFile",
            type : "upgradeFile",
            startDate: "8/11/2023",
            endDate:"8/11/2024",
            uploadDate: Date(),
        }
        dispatch(AssetActions.setUpgradeFiles({...upgradeFiles, ...upgradeFile}))
        reset({})
        handleCloseDialog()
            
        
        // more data validation
        // Get file Id


        const newID = Date.now()
        function onSuccess(response : ResponseType){
            setOpenPopup(false);
            reset(defaultValues);

        }
        function onFail(response : ResponseType){
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
            setShowAlert([true, "Could not add Batch!"])
        }
        
        // if(addCallback(callbacks, newID, "Post Banner", onSuccess, onFail)){
        //     // dispatch(BannerAPI.postBanners(newID, {"bannerName" : data.bannerName}))
        // }
    })
    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="lg" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                {/* <VirtualTableProps
                    tableName="upgradeFileTable"
                    dataSet={upgradeFiles? upgradeFiles : []}
                    headCells={upgradeFileHeadCells}
                    initialSortedBy={{ name: '' }}
                    dataIdentifier= "id"
                    saveKey="upgradeFileTableHead"    
                    selectedRows={selectedFiles}
                    setSelected={setSelectedFiles}
                /> */}
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormInput
                        label = "Choose Upgrade File"
                        id = "batchFileId"
                        name = "fileId"
                        type = "file"
                        accept=".package.tgz" 
                        register={register}
                        validation={{required: "Please select a file to upload"}}
                        error={errors.fileId}
                    />
                    {shouldShowAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <AddCircle />
                            <span> Add File </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}


export { UploadUpgradeFile };