//React-BootStrap
import React, { useState, useEffect } from 'react';
import { Button, ButtonGroup, Breadcrumb, Col, Row, Form } from '@themesberg/react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import { Link } from 'react-router-dom'

import { HeadCellTypes } from '../common/virtualTable/VirtualTable';
import { Checkbox, Dialog, DialogContent, DialogTitle, FormControlLabel, IconButton, Typography } from '@material-ui/core';

//Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import AutoAwesomeMotionIcon from '@mui/icons-material/AutoAwesomeMotion';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { Help } from '@material-ui/icons';
import { Tooltip } from '@mui/material';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
//Redux
import { ConfigActions, useAppSelector } from '../../state';
import { useDispatch } from "react-redux"

//APIs
import { ConfigAPI, FileAPI, UpgradeAPI } from "../api/index";
import { QuerablePromise } from '../assetDetails/ScaleDetails';

import StyledTableNode from '../assetTreeView/StyledTableItem'
import { UpgradeFormHandler } from './upgradeForms/upgradeFormHandler'
import AccessControl from '../common/AccessPermissions';
import * as AccessActions from '../../state/actions/accessActions'
import { FormInput } from '../forms/FormHelper';
import { useForm } from 'react-hook-form';
import { FileType, getValidFileExensions } from '../../types/asset/FileTypes';
import { UploadForm } from './upgradeForms/batchForm';
import { ResponseType } from '../../types/storeTypes';
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { notify } from 'reapop';
// import {  MakeQuerablePromise, TitleFormat } from '../utils/utils';
interface TitleProps {
    title: string
    children?: React.ReactNode
}
export function TitleFormat(props: TitleProps) {
    let title = props.title
    let Icon = props.children
    if (Icon)
        return (
            <div className="d-flex justify-content-between" >
                {Icon}
                <h6 style={{ fontWeight: "bold", marginTop: "2px", paddingInline: "5px" }}>{title}</h6>

            </div>
        )
    else
        return (
            <div className="d-flex justify-content-between" >
                <h6 style={{ fontWeight: "bold", marginTop: "2px", paddingInline: "5px" }}>{title}</h6>
                <ArrowDropDownIcon />
            </div>
        )
}

export function MakeQuerablePromise(promise) {
    // code was stolen from ScaleDetails.tsx
    if (promise.isFullfilled) return promise

    let isPending = true
    let isRejected = false
    let isFulfilled = false

    let result: QuerablePromise<any> = promise.then(
        function (v: any) {
            isFulfilled = true;
            isPending = false;
            return v;
        },
        function (e: any) {
            isRejected = true;
            isPending = false;
            throw e;
        }
    ) as QuerablePromise<any>

    result.isFullfilled = isFulfilled
    result.isPending = isPending
    result.isRejected = isRejected
    return result
}
export interface Batch {
    batchId: string
    name: string
    description?: string
    uploadStart: string
    uploadEnd: string
    upgradeStart: string
    upgradeEnd: string
    fileId: string
    fileName: string
    afterUpgradeCheckIn: number
}
export interface Group {
    groupId: string
    groupName: string
    description?: string
    scales: UpgradeDevice[]
    isSelected: boolean
    status: string
    batchGroup: boolean
}
export interface UpgradeDevice {
    upgradeId: string
    deviceId: string
    scaleModel: string
    batchId: string
    lastUpdate: string
    lastReport: string
    application: string
    bootloader: string
    fpc?: string
    status: string
    statusText: string
    nextCheck: string
    groupId: string
    ipAddress: string
    primaryScale: boolean
    enabled: boolean
    pluCount?: number
    hostname: string
    assignedStore?: string
    assignedDept?: string
    fileSelected: string
}
export interface Timeframe {
    uploadPrimaryStart: string
    uploadPrimaryEnd: string
    uploadSecondaryStart: string
    uploadSecondaryEnd: string
    upgradeTimeStart: string
    upgradeTimeEnd: string
}

export const scaleHeadCellsUpgrade: HeadCellTypes[] = [
    { id: "checkmarkUpgrade", sorted: false, searchable: false, label: "checkmark", isShowing: true, mandatory: true, },
    { id: "type", sorted: false, searchable: false, label: 'Type', isShowing: false, mandatory: true, },
    { id: 'ipAddress', sorted: false, searchable: false, label: 'IP Address', isShowing: true, mandatory: true, },
    { id: 'batchSelected', sorted: false, searchable: false, label: 'Batch', isShowing: true, mandatory: true, },
    { id: 'assignedDept', sorted: false, searchable: false, label: 'Assigned Department', isShowing: false, mandatory: false, },
    { id: 'assignedStore', sorted: false, searchable: false, label: 'Assigned Store', isShowing: false, mandatory: false, },
    { id: 'application', sorted: false, searchable: false, label: 'Application Version', isShowing: true, mandatory: false, },
    { id: 'enabled', sorted: false, searchable: false, label: 'Enabled / Disabled', isShowing: false, mandatory: false, },
    { id: 'hostname', sorted: false, searchable: false, label: 'Host Name', isShowing: false, mandatory: false, },
    { id: 'pluCount', sorted: false, searchable: false, label: 'PLU Count', isShowing: false, mandatory: false, },
    { id: 'lastReport', sorted: false, searchable: false, label: 'Last Report', isShowing: false, mandatory: false, },
    { id: 'bootloader', sorted: false, searchable: false, label: 'Boot Loader', isShowing: false, mandatory: false, },
    { id: 'lastUpdate', sorted: false, searchable: false, label: 'Last Update', isShowing: false, mandatory: false, },
    { id: 'status', sorted: true, searchable: false, label: 'Status', isShowing: true, mandatory: true },
    { id: 'statusText', sorted: false, searchable: false, label: 'Status Text', isShowing: true, mandatory: true },
    { id: 'nextCheck', sorted: false, searchable: false, label: 'Next Check', isShowing: false, mandatory: false },
    { id: 'fpc', sorted: false, searchable: false, label: 'FPC Version', isShowing: false, mandatory: false },
    { id: 'scaleModel', sorted: false, searchable: false, label: 'Scale Model', isShowing: false, mandatory: false },
]
export const manualUpgradeHeadCells: HeadCellTypes[] = [
    { id: "checkmarkUpgrade", sorted: false, searchable: false, label: "checkmark", isShowing: true, mandatory: true },
    { id: "type", sorted: false, searchable: false, label: "Type", isShowing: false, mandatory: false },
    { id: "ipAddress", sorted: true, searchable: true, label: "IP Address", isShowing: true, mandatory: true },
    { id: "application", sorted: false, searchable: true, label: "Application Version", isShowing: true, mandatory: true },
    { id: "fileSelected", sorted: false, searchable: false, label: "File to Upgrade", isShowing: true, mandatory: true },
    { id: "assignedDept", sorted: false, searchable: true, label: "Assigned Department", isShowing: false, mandatory: false },
    { id: "assignedStore", sorted: false, searchable: true, label: "Assigned Store", isShowing: false, mandatory: false },
    { id: 'enabled', sorted: false, searchable: false, label: 'Enabled / Disabled', isShowing: false, mandatory: false },
    { id: 'hostname', sorted: false, searchable: false, label: 'Host Name', isShowing: false, mandatory: false },
    { id: 'pluCount', sorted: false, searchable: false, label: 'PLU Count', isShowing: false, mandatory: false },
    { id: 'lastReport', sorted: false, searchable: false, label: 'Last Report', isShowing: false, mandatory: false },
    { id: 'bootloader', sorted: false, searchable: false, label: 'Boot Loader', isShowing: false, mandatory: false },
    { id: 'lastUpdate', sorted: false, searchable: false, label: 'Last Update', isShowing: false, mandatory: false },
    { id: 'status', sorted: true, searchable: false, label: 'Status', isShowing: true, mandatory: true },
    { id: 'statusText', sorted: false, searchable: false, label: 'Status Text', isShowing: true, mandatory: true },
    { id: 'fpc', sorted: false, searchable: false, label: 'FPC Version', isShowing: false, mandatory: false },
    { id: 'scaleModel', sorted: false, searchable: false, label: 'Scale Model', isShowing: false, mandatory: false }
]

export function StyledVirtualTable(groups: { [key: string]: Group }, filter?: boolean, selectedScales?: boolean, home?: boolean, batchUpgrade?: boolean) {
    let TableArray = [<div />]
    for (let groupKey of Object.keys(groups)) {
        let group = groups[groupKey]
        if (group && group["scales"]) {
            if (filter) {
                if (!group.isSelected)
                    continue
            }
            if (batchUpgrade) {
                if (!group.batchGroup) {
                    continue
                }
            } else {
                if (group.batchGroup) {
                    continue
                }
            }
            TableArray.push(
                <StyledTableNode
                    filterScales={(selectedScales) ? selectedScales : false}
                    group={group}
                    groupScales={group.scales}
                    batchUpgrade={batchUpgrade}
                />
            )
        }
    }

    if (TableArray.length === 0) {
        if (home)
            return ([
                <>
                    <br />
                    <Row >
                        <h2>No groups found!</h2>
                        <h5>To create a group, select and upgrade scales from Asset List, manually group scales or import a file.</h5>
                    </Row>
                </>]
            )
        else
            return ([
                <>
                    <br />
                    <Row >
                        <h2>No Selected Groups</h2>
                        <h5>Please select a group to continue.</h5>
                    </Row>
                </>]
            )
    }
    return TableArray
}



export function UpgradeManager(props) {
    const [batchUpgrade, setBatchUpgrade] = useState(props.location.state[0])
    const { register, handleSubmit, formState: { errors } } = useForm<UploadForm>()
    // Redux
    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();
    const user = useAppSelector((state) => state.access.account);
    const appRoles = useAppSelector((state) => state.access.roles);

    useEffect(() => {
        const newID = Date.now()
        dispatch(UpgradeAPI.getDevices(newID))
        dispatch(UpgradeAPI.getGroups(newID + 1))
        dispatch(UpgradeAPI.getBatches(newID + 2))
        dispatch(FileAPI.fetchFiles(newID + 3))
        dispatch(ConfigAPI.getConfiguration(newID + 4, ["hybridMultipath"], ConfigActions.setHybridMultipath))

        if (!batchUpgrade) {
            dispatch(ConfigAPI.setConfiguration(newID + 4, { "hybridMultipath": "false" }))
        }
    }, [])

    // dispatch(AssetActions.setBatches({}))
    // useStates
    const [modalShow, setModalShow] = React.useState(false);
    const [popUpValue, setPopUpValues] = useState({ title: "", type: "" })

    //groups    
    const reduxGroups = useAppSelector((state) => state.asset.groups)
    const [groups, setGroups] = useState((reduxGroups) ? { ...reduxGroups } : {})

    useEffect(() => {
        if (reduxGroups && reduxGroups.length) {
            setGroups({ ...reduxGroups })
        }
    }, [reduxGroups])
    useEffect(() => {
        let tempGroups = { ...reduxGroups }
        setGroups({ ...tempGroups })

        //     if(!Object.values(upgradeDevices).length){
        //         for(let group of Object.values(reduxGroups)){
        //             for (let upDevKey of Object.keys(upgradeDevices)) {
        //                 group.scales.map((scale)=>{
        //                     upgradeDevices[upDevKey] = {
        //                         upgradeId: upgradeDevices[upDevKey].upgradeId,
        //                         deviceId: scale.deviceId,
        //                         batchId: upgradeDevices[upDevKey].batchId,
        //                         scaleModel: upgradeDevices[upDevKey].scaleModel,
        //                         lastUpdate: upgradeDevices[upDevKey].lastUpdate,
        //                         lastReport: upgradeDevices[upDevKey].lastReport,
        //                         application: upgradeDevices[upDevKey].application,
        //                         bootloader: upgradeDevices[upDevKey].bootloader,
        //                         fpc: "",
        //                         status: upgradeDevices[upDevKey].status,
        //                         statusText: upgradeDevices[upDevKey].statusText,
        //                         nextCheck: upgradeDevices[upDevKey].nextCheck,
        //                         groupId: group.groupId,
        //                         ipAddress: upgradeDevices[upDevKey].ipAddress,
        //                         primaryScale: upgradeDevices[upDevKey].primaryScale,
        //                         enabled: upgradeDevices[upDevKey].enabled,
        //                         hostname: upgradeDevices[upDevKey].hostname,
        //                         pluCount: upgradeDevices[upDevKey].pluCount,
        //                         assignedStore: upgradeDevices[upDevKey].assignedStore,
        //                         assignedDept: upgradeDevices[upDevKey].assignedDept
        //                     }
        //                 })
        //             }
        //             setUpgradeDevices({...upgradeDevices})
        //         }
        //         dispatch(AssetActions.setUpgradeDevices({...upgradeDevices}))
        //     }
    }, [reduxGroups])

    const handleUpload = handleSubmit(async (data: UploadForm) => {
        if (!data.fileUpload || (data.fileUpload && data.fileUpload.length === 0)) {
            dispatch(notify("A file must be chosen for uploading. Please choose a file to upload.", "warning"))
            return
        }

        const newID = Date.now()
        function onSuccess(response: ResponseType) {
            logger.info("Successfully uploaded upgrade file to server")
            dispatch(FileAPI.fetchFiles())
        }
        function onFail(response: ResponseType) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add Batch!"])
        }

        if (data.fileUpload && addCallback(callbacks, newID, "Upload File", onSuccess, onFail)) {
            const fileData = new FormData()
            fileData.append('file', data.fileUpload[0])
            dispatch(UpgradeAPI.uploadFile(fileData, newID))
        }
    })

    const [refresher, setRefresher] = useState(false)
    useEffect(() => {
        setRefresher(true)
    }, [])
    useEffect(() => {
        if (refresher) {
            setRefresher(false)
            const refreshPromise: Promise<any> = new Promise((resolve, reject) => {
                setTimeout(() => resolve("done"), 30000)
            }).then(() => {
                const newID = Date.now()
                dispatch(UpgradeAPI.getGroups(newID))
                dispatch(UpgradeAPI.getDevices(newID + 1))
                dispatch(UpgradeAPI.getBatches(newID + 2))
            })

            let refreshStatus = MakeQuerablePromise(refreshPromise)
            refreshStatus.then(() => {
                setRefresher(true)
            })
        }
    }, [refresher])

    const [infoDialog, setInfoDialog] = useState(false)
    const [pageInfo, setPageInfo] = useState(false)
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const showInfoDialog = () => {
        const handleCloseDialog = (event?: any) => {
            setShowAlert([false, "Something went wrong!"])
            event.stopPropagation()
            setInfoDialog(false)
        }

        let infoText = <>
            {batchUpgrade && <>
                <div>Create Batch:</div>
                <p style={{ "marginLeft": "2rem" }}>This button will open a dialog asking for information to create a batch and a file to upgrade with.</p>
                <div>Manage Batches:</div>
                <p style={{ "marginLeft": "2rem" }}>This button will open a dialog to select the currently saved batches. Selecting one will expand the dialog to allow editing of all it's fields and file.</p>
                <div>Manage Groups:</div>
                <p style={{ "marginLeft": "2rem" }}>Contains the following buttons:
                    <ul>
                        {/* <li>Import Group</li>
                        <li>Export Group</li> */}
                        <li>Create Group</li>
                        <li>Enable Groups</li>
                    </ul>
                </p>
                {/* <div>Import Group:</div>
                <p style={{"marginLeft":"2rem"}}>Opens a file explorer to select a CSV file to import as a group.</p>
                <div>Export Group:</div>
                <p style={{"marginLeft":"2rem"}}>Requires at least one group to be selected. Will open a file explorer to find a location to export and the name of the file to create.</p> */}
                <div>Create Group:</div>
                <p style={{ "marginLeft": "2rem" }}>Opens a dialog to find scales using a range of IP Addresses. Then the dialog will transition to the normal group creation view with selecting scales and providing group information.</p>
                <div>Enable Groups:</div>
                <p style={{ "marginLeft": "2rem" }}>Opens a dialog showing all groups to select. Click the Enable button to enable the selected groups.</p></>
            }

            <div>Check Scale Info:</div>
            <p style={{ "marginLeft": "2rem" }}>Opens a dialog showing a table of scales in groups and their information such as if those scales are assigned a batch and if they have the file for upgrading. Make sure to have the scales you want to check selected and then click Check Info to gather this information.</p>
            {hybridMultipath ? <>
                <div>Upload Now:</div>
                <p style={{ "marginLeft": "2rem" }}>Contains the following buttons:
                    <ul>
                        <li>Upload Files to Primary Scales Now</li>
                        <li>Upload Files to Secondary Scales Now</li>
                    </ul>
                </p>
                <div>Upload Files to Primary Scales Now:</div>
                <p style={{ "marginLeft": "2rem" }}>Opens a dialog with a table showing only primary scales with batches assigned if any are set as primaries. If no primaries are set, then all grouped scales with assigned batches are shown. Click Upload to immediately begin sending the batch file to those selected scales.</p>
                <div>Upload Files to Secondary Scales Now:</div>
                <p style={{ "marginLeft": "2rem" }}>Opens a dialog with a table showing only secondary scales with batches assigned if any primary scales are set. If no primaries are set, then all grouped scales with assigned batches are shown. Click Upload to immediately begin sending the batch file to those selected scales.</p>
                <div>Upgrade Now:</div>
                <p style={{ "marginLeft": "2rem" }}>Contains the following buttons:
                    <ul>
                        <li>Upgrade Primary Scales</li>
                        <li>Upgrade Secondary Scales</li>
                    </ul>
                </p>
                <div>Upgrade Primary Scales Now:</div>
                <p style={{ "marginLeft": "2rem" }}>Opens a dialog with a table showing only primary scales with batches assigned if any are set as primaries. If no primaries are set, then all grouped scales with assigned batches are shown. Click Upgrade to immediately begin upgrading the selected scales.</p>
                <div>Upgrade Secondary Scales Now:</div>
                <p style={{ "marginLeft": "2rem" }}>Opens a dialog with a table showing only secondary scales with batches assigned if any primary scales are set. If no primaries are set, then all grouped scales with assigned batches are shown. Click Upgrade to immediately begin upgrading the selected scales.</p>
            </>
                : <>
                    <div>Upload Files:</div>
                    <p style={{ "marginLeft": "2rem" }}>Opens a dialog with a table showing the selected scales with files assigned. Click Upload to begin sending the assigned files to the scales.</p>
                    <div>Upgrade Files:</div>
                    <p style={{ "marginLeft": "2rem" }}>Opens a dialog with a table showing the selected scales with files assigned. Click Upgrade to begin applying the uploaded files to the scales.</p>
                </>}
            {/* <div>Manage Timeframes:</div>
            <p style={{"marginLeft":"2rem"}}>Opens a dialog with time selectors to set at what time of day uploads and upgrades can happen.</p> */}
        </>

        return (
            <Dialog open={infoDialog} onClose={handleCloseDialog} fullWidth={true} maxWidth="md">
                <DialogTitle>
                    <Typography variant="h6" component="div">Upgrade Manager Buttons Overview</Typography>
                </DialogTitle>

                <DialogContent>
                    <Typography variant="body1" component="div">{infoText}</Typography>
                </DialogContent>

                <Button style={{ "margin": "1rem auto" }} className="formButton1" onClick={handleCloseDialog}>
                    <span> Close </span>
                </Button>
            </Dialog>
        )
    }

    const showPageInfoDialog = () => {
        const handleCloseDialog = () => {

        }

        let infoText = <>

        </>

        return (
            <Dialog open={pageInfo} onClose={handleCloseDialog} fullWidth={true} maxWidth="md">
                <DialogTitle>
                    <Typography variant="h6" component="div"></Typography>
                </DialogTitle>
            </Dialog>
        )
    }

    const reduxHybridMultipath = useAppSelector(state => state.config.hybridMultipath.hybridMultipath)
    const [hybridMultipath, setHybridMultipath] = useState(reduxHybridMultipath === "true")
    const updateHybridMultipath = (event) => {
        if (event) {
            let newID = Date.now()
            setHybridMultipath(event.target.checked)
            dispatch(ConfigAPI.setConfiguration(newID, { "hybridMultipath": event.target.checked }))
        }
    }
    useEffect(() => {
        setHybridMultipath(reduxHybridMultipath === "true")
    }, [reduxHybridMultipath])
    useEffect(() => {
        if (hybridMultipath && scaleHeadCellsUpgrade[3].id !== 'primaryScale') {
            scaleHeadCellsUpgrade.splice(3, 0, { id: 'primaryScale', sorted: false, searchable: false, label: 'Is Primary', isShowing: true, mandatory: true })
        } else if (!hybridMultipath && scaleHeadCellsUpgrade[3].id === 'primaryScale') {
            scaleHeadCellsUpgrade.splice(3, 1)
        }

        if (hybridMultipath && manualUpgradeHeadCells[3].id !== 'primaryScale') {
            manualUpgradeHeadCells.splice(3, 0, { id: 'primaryScale', sorted: false, searchable: false, label: 'Is Primary', isShowing: true, mandatory: true })
        } else if (!hybridMultipath && manualUpgradeHeadCells[3].id === 'primaryScale') {
            manualUpgradeHeadCells.splice(3, 1)
        }
    }, [hybridMultipath])

    return (
        <AccessControl
            user={user}
            requiredRole={appRoles[AccessActions.UPGRADE_SCALES]}
            pageRequest={true}
        >
            <div className="UpgradeManager">
                <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/AssetList' }}>Asset List</Breadcrumb.Item>
                    <Breadcrumb.Item active>Upgrade Manager</Breadcrumb.Item>
                </Breadcrumb>
                <Row>
                    <Col>
                        <h2>Upgrade Manager</h2>
                        <h6>
                            Upgrade your scales manually <i>(upload file to server; upload file to scales; upgrade scales)</i>
                            {/* <Tooltip className="info" title={
                            <Typography variant="body1" component="div">Click to see information about this page</Typography>
                        }>
                            <IconButton size="small" disableRipple style={{padding: "0"}} onClick={() => {
                                setPageInfo(true)
                            }}>
                                <Help />
                            </IconButton>
                        </Tooltip> */}
                        </h6>
                        {batchUpgrade && <>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        indeterminate={false}
                                        checked={hybridMultipath}
                                        onChange={event => updateHybridMultipath(event)}
                                        style={{ padding: "0", color: "rgba(0,0,0,0.54)" }}
                                    />
                                }
                                label="Enable&nbsp;Hybrid&nbsp;Multipath&nbsp;Topology"
                                style={{ marginRight: "0" }}
                            />
                            <Tooltip className="info" style={{ "marginLeft": "0.5rem" }} title={
                                <Typography variant="body1" component="div">When enabled, scales will use primary and secondary communication</Typography>
                            }>
                                <IconButton disableRipple style={{ padding: "0", backgroundColor: "transparent", cursor: "default" }}>
                                    <Help />
                                </IconButton>
                            </Tooltip>
                        </>}
                    </Col>
                </Row>

                {/* Button Groups */}
                <br />
                <Row>
                    {batchUpgrade &&
                        <Col sm="3" style={{ marginRight: "35vh" }}> {/*marginTop to adjust for   */}
                            <ButtonGroup >
                                <Dropdown as={ButtonGroup}>

                                    <Tooltip title={<Typography variant="body1" component="div">Generate a batch (i.e., an upgrade file coupled to a timer)</Typography>}>
                                        <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm"
                                            title={<TitleFormat title="Create Batch" > <AddCircleOutlineIcon /> </TitleFormat>}
                                            onClick={() => {
                                                setModalShow(true)
                                                setPopUpValues({ title: "Create Batch", type: "createBatch" })
                                            }}
                                            show={false}
                                        >

                                            <></>
                                        </DropdownButton>
                                    </Tooltip>

                                    <Tooltip title={<Typography variant="body1" component="div">Edit or delete existing batches</Typography>}>
                                        <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm"
                                            title={<TitleFormat title="Manage Batches" > <div title="x"></div> </TitleFormat>}
                                            onClick={() => {
                                                setModalShow(true)
                                                setPopUpValues({ title: "Manage Batches", type: "editBatches" })
                                            }}

                                            show={false}
                                        >
                                            <></>
                                        </DropdownButton>
                                    </Tooltip>

                                    <Tooltip placement="top-end" title={<Typography variant="body1" component="div">Create or enable groups</Typography>}>
                                        <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm"
                                            title={<TitleFormat title="Manage Groups" />}
                                        >

                                            <Dropdown.Item id="Dropdown"
                                                onClick={() => {
                                                    setModalShow(true)
                                                    setPopUpValues({ title: "Group Scales", type: "createGroup" })
                                                }}>
                                                <AutoAwesomeMotionIcon className="IconDropdown" />
                                                Create Group
                                            </Dropdown.Item>

                                            <Dropdown.Divider />

                                            <Dropdown.Item id="itemDropdown3"
                                                onClick={() => {
                                                    setModalShow(true)
                                                    setPopUpValues({ title: "Enable Groups", type: "enableGroups" })
                                                }}>
                                                <CheckCircleIcon className="IconDropdown" />
                                                Enable Groups
                                            </Dropdown.Item>
                                        </DropdownButton>
                                    </Tooltip>
                                </Dropdown>
                            </ButtonGroup>
                        </Col>
                    }
                    <Col xs={batchUpgrade ? undefined : "4"}>
                        <ButtonGroup>
                            <Dropdown as={ButtonGroup}>

                                <Tooltip placement="top" title={<Typography variant="body1" component="div">Retrieve upgrade-related data from the selected scales</Typography>}>
                                    <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm"
                                        title={<TitleFormat title="Check Scale Info" > <></> </TitleFormat>}
                                        onClick={() => {
                                            if (true) {//change to actual conditional
                                                setModalShow(true)
                                                setPopUpValues({ title: "Check Scale Info", type: "checkScales" })
                                            }
                                        }}
                                        show={false}
                                    >
                                        <></>
                                    </DropdownButton>
                                </Tooltip>

                                {hybridMultipath ? <>
                                    <Tooltip placement="top" title={<Typography variant="body1" component="div">Send upload-file command to selected scales</Typography>}>
                                    <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm"
                                        title={<TitleFormat title="Upload Now" />}
                                    >
                                        <Dropdown.Item id="Dropdown"
                                            onClick={() => {
                                                setModalShow(true)
                                                setPopUpValues({ title: "Upload Files to Primary Scales Now", type: "uploadToPrimary" })
                                            }}>
                                            Upload Files to Primary Scales Now
                                        </Dropdown.Item>
                                        <Dropdown.Item id="Dropdown"
                                            onClick={() => {
                                                setModalShow(true)
                                                setPopUpValues({ title: "Upload Files to Secondary Scales Now", type: "uploadToSecondary" })
                                            }}>
                                            Upload Files to Secondary Scales Now
                                        </Dropdown.Item>
                                    </DropdownButton>
                                    </Tooltip>

                                    <Tooltip placement="top" title={<Typography variant="body1" component="div">Send upgrade command to selected scales</Typography>}>
                                    <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm"
                                        title={<TitleFormat title="Upgrade Now" />}
                                    >
                                        <Dropdown.Item id="Dropdown"
                                            onClick={() => {
                                                setModalShow(true)
                                                setPopUpValues({ title: "Upgrade Primary Scales Now", type: "upgradePrimary" })
                                            }}>
                                            Upgrade Primary Scales Now
                                        </Dropdown.Item>
                                        <Dropdown.Item id="Dropdown"
                                            onClick={() => {
                                                setModalShow(true)
                                                setPopUpValues({ title: "Upgrade Secondary Scales Now", type: "upgradeSecondary" })
                                            }}>
                                            Upgrade Secondary Scales Now
                                        </Dropdown.Item>
                                    </DropdownButton>
                                    </Tooltip>
                                </> : <>
                                <Tooltip placement="top" title={<Typography variant="body1" component="div">Send upload-file command to selected scales</Typography>}>
                                    <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm" title={<TitleFormat title="Upload Files"><></></TitleFormat>}
                                        onClick={() => {
                                            setModalShow(true)
                                            setPopUpValues({ title: "Upload Files to Scales Now", type: "uploadToPrimary" })
                                        }}
                                        show={false}
                                    >
                                        <></>
                                    </DropdownButton>
                                    </Tooltip>

                                    <Tooltip placement="top" title={<Typography variant="body1" component="div">Send upgrade command to selected scales</Typography>}>
                                    <DropdownButton as={ButtonGroup} id="actionDropdown2" size="sm" title={<TitleFormat title="Upgrade Scales"><></></TitleFormat>}
                                        onClick={() => {
                                            setModalShow(true)
                                            setPopUpValues({ title: "Upgrade Scales Now", type: "upgradePrimary" })
                                        }}
                                        show={false}
                                    >
                                        <></>
                                    </DropdownButton>
                                    </Tooltip>
                                </>}
                            </Dropdown>
                        </ButtonGroup>
                        <Tooltip className="info" title={
                            <Typography variant="body1" component="div">Click here to see additional information on the buttons</Typography>
                        }>
                            <IconButton size="small" style={{ marginBottom: "5px" }} aria-label="upgradeButtonsExplain" disableRipple onClick={() => {
                                setInfoDialog(true)
                            }}>
                                <Help />
                            </IconButton>
                        </Tooltip>
                        {showInfoDialog()}
                    </Col>
                    {!batchUpgrade && <>
                        <Col xs="6">
                            <Form className="form-control" onSubmit={handleUpload}>
                                <FormInput
                                    label="Upload file to server"
                                    id="fileUpload"
                                    name="fileUpload"
                                    type="file"
                                    accept={getValidFileExensions([FileType.FEATURE, FileType.DEBIAN, FileType.TARBALL, FileType.INSTALL_IMAGE])}
                                    register={register}
                                    validation={{}}
                                    error={errors.fileUpload}
                                />
                            </Form>
                        </Col>
                        <Col>
                            <Button className="formButton1" onClick={handleUpload} type="submit">
                                Upload to Server
                            </Button>
                        </Col>
                    </>}
                </Row>

                <br />
                <div className="Groups">
                    {StyledVirtualTable(groups, false, false, true, batchUpgrade)}
                </div>

                <UpgradeFormHandler
                    type={popUpValue.type}
                    show={modalShow}
                    onHide={setModalShow}
                    title={popUpValue.title}
                    groups={groups}
                    batchUpgrade={batchUpgrade}
                    hybridMultipath={hybridMultipath}
                />

            </div>
        </AccessControl>
    )
}