import { Dialog, DialogContent, DialogTitle, Typography } from "@material-ui/core"
import { Block, CheckCircle } from "@mui/icons-material";
import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap"
import { useForm } from "react-hook-form";
import VirtualTable, { HeadCellTypes } from "../common/virtualTable/VirtualTable";
import { useAppSelector } from "../../state";
import { FormInput } from "../forms/FormHelper";
import { useHistory } from "react-router-dom";
import { Group, UpgradeDevice } from "./UpgradeManager";
import { useDispatch } from "react-redux";
import { ResponseType } from "../../types/storeTypes";
import logger from "../utils/logger";
import { useRspHandler } from "../utils/ResponseProvider";
import { ScaleDevice } from "../../types/asset/ScaleTypes";

function SingleUpgradeSelection(props: { title: string, openPopup: boolean, setOpenPopup, tableHeaders: HeadCellTypes[], selectedDevices: ScaleDevice[] }) {
  const { title, openPopup, setOpenPopup, tableHeaders, selectedDevices } = props

  const selectedScales = useAppSelector((state) => state.asset.selected)
  const { register, handleSubmit, reset, formState: { errors } } = useForm<SelectDevicesForSingleUpgradeForm>();
  const history = useHistory();
  const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])
  const [selected, setSelected] = useState<string[]>([...selectedScales]) // used for updating selectedScales
  const [copiedSelectedDevices, setCopiedSelectedDevices] = useState<ScaleDevice[]>([...selectedDevices])
  const { callbacks, addCallback } = useRspHandler()
  const [selectedInDialog, setSelectedInDialog] = useState<ScaleDevice[]>([])

  const dispatch = useDispatch();

  useEffect(() => {
    setCopiedSelectedDevices([...selectedDevices])
  }, [selectedDevices])

  useEffect(() => {
    let copiedSubset: ScaleDevice[] = []
    for (let copied of copiedSelectedDevices) {
      if (selected.includes(copied.deviceId)) {
        copiedSubset.push(copied)
      }
    }
    setSelectedInDialog([...copiedSubset])
  }, [selected])

  interface SelectDevicesForSingleUpgradeForm {
    file : any
  }

  const handleCloseDialog = (event) => {
    setShowAlert([false, "Something went wrong!"])
    setOpenPopup(false);
    setSelected([])

    if (event != null) {
      event.stopPropagation()
    }
  }

  const handleSelection = handleSubmit(async (data: SelectDevicesForSingleUpgradeForm) => {

    function onSuccess(response: ResponseType) {
      logger.info("Group saved successfully")
      history.push({ pathname: "/UpgradeManager", state: selected })
    }
    function onFail(response: ResponseType) {
      logger.info("Could not save group")
    }

    const newID = Date.now()
    if (addCallback(callbacks, newID, "Save Group", onSuccess, onFail, { requestMsg: "Saving group...", successMsg: "Group successfully saved", failMsg: "default" })) {

    }
  })

  return (
    <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="lg" aria-labelledby='form-dialog-title'>
      <DialogTitle>
        <Typography variant="h5" component="div">
          {title}
        </Typography>
      </DialogTitle>

      <DialogContent>
        <Form className="form-control" onSubmit={handleSelection}>
          <FormInput
            label=""
            id="uploadFile"
            name="uploadFile"
            type="file"
            accept=".csv"
            enctype="multipart/form-data"
            method="post"
            action="#"
            register={register}
            validation={{ required: "Please select a file to upload" }}
            error={errors.file}
          /> <br />

          <VirtualTable
            dataSet={copiedSelectedDevices}
            headCells={tableHeaders}
            initialSortedBy={{ name: 'ipAddress' }}
            dispatchedUrls={[]}
            maxHeight="350px"
            dataIdentifier="deviceId"
            saveKey="selectedScalesForUpgradeTableHead"
            tableName={"selectedScalesForUpgradeTable"}
            selectedRows={selectedInDialog.length > 0 ? selected : copiedSelectedDevices.map((device) => { return device.deviceId })}
            setSelected={setSelected}
          /> <br />

          <Typography component="div" variant="h6" align="center" style={{ flexGrow: 2 }}>
            Are these the devices you want to upgrade?
          </Typography>

          <div className="formButtons">
            <Button className="formButton1" onClick={handleSelection}>
              <CheckCircle />
              <span> Yes </span>
            </Button>
            <Button className="formButton1" onClick={handleCloseDialog}>
              <Block />
              <span> Cancel </span>
            </Button>
          </div>
        </Form>
      </DialogContent>
    </Dialog>
  )
}

export default SingleUpgradeSelection
