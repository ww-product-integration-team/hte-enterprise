import { useState } from 'react';
import { Link} from 'react-router-dom'
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux"
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import {Upload, Block, Save, Email} from '@mui/icons-material';
import { Col, Row, Form, FormCheck} from '@themesberg/react-bootstrap';
import { FormInput} from '../forms/FormHelper';
import logger from '../utils/logger';
import {AccessAPI} from '../api/index'
import { useRspHandler } from '../utils/ResponseProvider';
import { ResponseType } from '../../types/storeTypes';


const ButtonMailto = ({ mailto, label }) => {
    return (
        <Link
            to='#'
            onClick={(e) => {
                window.location.href = mailto;
                e.preventDefault();
            }}
        >
            <span style={{color: 'white'}}>{label}</span>
        </Link>
    );
};

// Request License for HTe Enterprise (.req file or send as email)
const RequestEnterpriseLicense = () => {

    interface LicenseForm{
        name : string
        title : string
        companyName : string
        companyAddress : string
        email : string
        phone : string
        salesRep : string
        orderNumber : string
        requestType : string
        numberUsers : number
        numberScales : number
    }

    const { register, handleSubmit, formState: {errors} } = useForm<LicenseForm>();
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend
    const [formattedData, setFormattedData] = useState("") // Body of email (content of license request)


    const handleSendAsEmail = (data : LicenseForm) => {
        logger.info("License Request (Generate Email): ", data)
        setShowAlert([false, "Something went wrong!"])

        // %0D%0A = Encoded character for a 'New Line' 
        setFormattedData(`HOBART HTe LICENSE REQUEST FORM 
            %0D%0A%0D%0AResponsible Individual:${data.name}
            %0D%0ATitle:${data.title}
            %0D%0ACompany Name:${data.companyName}
            %0D%0ACompany Address:${data.companyAddress}
            %0D%0APhone Number:${data.phone.replace(/[^0-9]/g, "")}
            %0D%0AEmail Address:${data.email}
            %0D%0ASales Rep:${data.salesRep}
            %0D%0AOrder Number:${data.orderNumber}
            %0D%0ARequest Type:${data.requestType}
            %0D%0ALicense Type Requested:[HTE_SCALEMGMT_ENTERPRISE]
            %0D%0ALicense Feature(s) Requested:[HTE_SCALEMGMT_ENTERPRISE]
            %0D%0ANumber of Licenses:1
            %0D%0ACOMPUTING SYSTEM ID=IGNORE
            %0D%0ANumber of Users:${data.numberUsers}
            %0D%0ANumber of Scales:${data.numberScales}`
        )
    }

    // POST License upload to API
    const handleRequest = async (data : LicenseForm) => {      // When the Submit button is pressed
        logger.info("License Request (Save as File): ", data)
        setShowAlert([false, "Something went wrong!"])


        let temp = `HOBART HTe LICENSE REQUEST FORM 
            Responsible Individual:${data.name}
            Title:${data.title}
            Company Name:${data.companyName}
            Company Address:${data.companyAddress}
            Phone Number:${data.phone.replace(/[^0-9]/g, "")}
            Email Address:${data.email}
            Sales Rep:${data.salesRep}
            Order Number:${data.orderNumber}
            Request Type:${data.requestType}
            License Type Requested:[HTE_SCALEMGMT_ENTERPRISE]
            License Feature(s) Requested:[HTE_SCALEMGMT_ENTERPRISE]
            Number of Licenses:1
            COMPUTING SYSTEM ID=IGNORE
            Number of Users:${data.numberUsers}
            Number of Scales:${data.numberScales}`

        const element = document.createElement("a");
        const fileData = new Blob([temp], {type: 'text/plain'});
        element.href = URL.createObjectURL(fileData);
        element.download = `HTeLicenseRequest-${data.name}-${data.title}.req`;
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
    }

    return (
        <Form onSubmit={handleSubmit(handleRequest)}>
            <div className="licenseContainer1">
                <div className="form-control licenseGroup">
                    <FormCheck.Label>Request Type</FormCheck.Label>
                    <div className="htGroup1">
                        <div className="licenseTypes">
                            <input type="radio" id="type1" value="HTE_DEMO"
                                {...register("requestType", {required: 'Please select an option' })}
                            /> 
                            <span className="radioLabel"> Demo </span>
                        </div>

                        <div className="licenseTypes">
                            <input type="radio" id="type2" value="HTE_PURCHASE" 
                                {...register("requestType", {required: 'Please select an option' })}
                            />       
                            <span className="radioLabel"> Purchase </span>
                        </div>
                        <br />

                        {errors.requestType && <p className="formError">{errors.requestType.message}</p>}
                    </div>
                </div>

                <div className="form-control licenseGroup">
                    <FormCheck.Label>Included Features</FormCheck.Label>
                    <li>Upgrades</li>
                    <li>Asset Management</li>
                    <li>Transaction Monitoring</li>
                </div>
            </div>     

            <Row>
                <Col md={6} className="mb-3">
                    <FormInput
                        label="Name "
                        id="name"
                        name="name"
                        type="text"
                        placeholder="Enter a name"
                        register={register}
                        validation={{
                            required: "Please enter a name",
                            maxLength: { value: 60, message: "You exceeded the max name length"}
                        }}
                        error={errors.name}
                    />
                </Col>

                <Col md={6} className="mb-3">
                    <FormInput
                        label="Title "
                        id="title"
                        name="title"
                        type="text"
                        placeholder="Enter a title"
                        register={register}
                        validation={{
                            required: "Please enter a title",
                            maxLength: { value: 60, message: "You exceeded the max title length"}
                        }}
                        error={errors.title}
                    />
                </Col>
            </Row>

            <Row>
                <Col md={6} className="mb-3">
                    <FormInput
                        label="Email Address "
                        id="email"
                        name="email"
                        type="email"
                        placeholder="example@company.com"
                        register={register}
                        validation={{
                            required: "Please enter an email address",
                            maxLength: { value: 60, message: "You exceeded the max email length"}
                        }}
                        error={errors.email}
                    />
                </Col>

                <Col md={6} className="mb-3">
                    <FormInput
                        label="Phone Number (optional)"
                        id="phone"
                        name="phone"
                        type="tel"
                        placeholder="999-999-9999"
                        register={register}
                        validation={{
                            // required: "Please enter a phone number",
                            maxLength: { value: 16, message: "You exceeded the max phone number length"}
                        }}
                        error={errors.phone}
                    />
                </Col>
                <hr />

                <Col md={6} className="mb-3">
                    <FormInput
                        label="Company Name: "
                        id="companyName"
                        name="companyName"
                        type="text"
                        placeholder="Enter a company name"
                        register={register}
                        validation={{
                            required: "Please enter a company name",
                            maxLength: { value: 60, message: "You exceeded the max company name length"}
                        }}
                        error={errors.companyName}
                    />
                </Col>
                
                <Col md={6} className="mb-3">
                    <FormInput
                        label="Company Address: "
                        id="companyAddress"
                        name="companyAddress"
                        type="text"
                        placeholder="Enter a company address"
                        register={register}
                        validation={{
                            required: "Please enter a company address",
                            maxLength: { value: 80, message: "You exceeded the max company address length"}
                        }}
                        error={errors.companyName}
                    />
                </Col>
            </Row>

            <Row>
                <Col md={6} className="mb-3">
                    <FormInput
                        label="Number of Scales: "
                        id="numberScales"
                        name="numberScales"
                        type="number"
                        placeholder="Enter the max number of scales to be licensed"
                        register={register}
                        validation={{
                            required: "Please enter the max number of scales to be licensed",
                            valueAsNumber: true,
                            min: { value: 1, message: "You must specify a value of at least 1 scale"},
                            max: { value: 80000, message: "You exceeded the max number of scales"}
                        }}
                        error={errors.numberScales}
                    />
                </Col>
                
                <Col md={6} className="mb-3">
                    <FormInput
                        label="Number of Users: "
                        id="numberUsers"
                        name="numberUsers"
                        type="number"
                        placeholder="Enter the max number of users to be licensed"
                        register={register}
                        validation={{
                            required: "Please enter the max number of users to be licensed",
                            valueAsNumber: true,
                            min: { value: 1, message: "You must specify a value of at least 1 user"},
                            max: { value: 80000, message: "You exceeded the max number of users"}
                        }}
                        error={errors.numberUsers}
                    />
                </Col>

                <Col md={6} className="mb-3">
                    <FormInput
                        label="Sales Rep (optional): "
                        id="salesRep"
                        name="salesRep"
                        type="text"
                        register={register}
                        validation={{maxLength: { value: 60, message: "You exceeded the max name length"}}}
                        error={errors.salesRep}
                    />
                </Col>

                <Col md={6} className="mb-3">
                    <FormInput
                        label="Order Number (optional): "
                        id="orderNumber"
                        name="orderNumber"
                        type="number"
                        register={register}
                        validation={{maxLength: { value: 60, message: "You exceeded the max order number length"}}}
                        error={errors.orderNumber}
                    />
                </Col>
            </Row>
            <br />

            {/* Error uploading file */}
            {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

            <div className="formButtons">
                <button className="formButton1" disabled={true} onClick={handleSubmit(handleSendAsEmail)} type="submit">
                    <Email />
                    <span> Send as Email</span>
                </button>

                <button className="formButton1" onClick={handleSubmit(handleRequest)} type="submit">
                    <Save />
                    <span> Save as File </span>
                </button>
            </div>

            {formattedData === ""
                ? null
                : 
                <div className="licenseAlert">
                <Alert className="licenseAlert2 fw-bold" variant="filled" severity="success">
                    <ButtonMailto 
                        label="Click here to send us a license request via Email"
                        mailto={`mailto:HT.Support@itwfeg.com?subject=HTe License Request&body=${formattedData}`} 
                    />
                </Alert>
                </div>
            }
        </Form>
    );
}


// Upload HTe Enterprise License
const UploadEnterpriseLicense = () => {

    const { register, handleSubmit,  formState: {errors} } = useForm();
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend
    const [successAlert, setSuccessAlert] = useState([false, "Something went wrong!"]) // Checking if success + show temp password
    const [userEmail, setUserEmail] = useState<string>("")
    const {callbacks, addCallback} = useRspHandler();

    const dispatch = useDispatch();

    // POST File Upload to API
    const handleUpload = async (data) => {      // When the Submit button is pressed
        logger.info("(Upload HTe Enterprise License) POST Request Data: ", data)
        setShowAlert([false, "Something went wrong!"])
        setSuccessAlert([false, "Something went wrong!"])
        setUserEmail("")

        const fileData = new FormData();
        fileData.append('file', data.fileName[0]);
        setUserEmail(data.email)

        const newID = Date.now()
        function onSuccess(response : ResponseType){
            logger.info("Successful response (Upload HTe Enterprise License): ", response);
            localStorage.setItem("email", userEmail)
            dispatch(AccessAPI.getAccountInfo(Date.now(), userEmail))

            setSuccessAlert([true, response.response.errorDescription])
            setUserEmail("")
        }
        function onFail(response : ResponseType){
            logger.error("Unsuccessful response (Upload HTe Enterprise License): ", response.response);
    
            // TODO?: If we want to be more specific we can add something here.
            // Server Error
            setShowAlert([true, response.response.errorDescription ? response.response.errorDescription : "Error connecting to server. Please try again."])
        }
        if(addCallback(callbacks, newID, "Install License", onSuccess, onFail)){
            dispatch(AccessAPI.installLicense(newID, fileData))
        }
    }
    
    return (
        <Form onSubmit={handleSubmit(handleUpload)}>
            <Row>
                <Col md={12} className="mb-3 uploadFile">
                    <FormInput
                        label="Upload a license here: "
                        id="fileName"
                        name="fileName"
                        type="file"
                        accept=".lic" 
                        register={register}
                        validation={{required: "Please select a file to upload"}}
                        error={errors.fileName}
                    />
                </Col>
            </Row>

            <br />

            {/* Error uploading file */}
            {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

            {/* Successful account creation */}
            {successAlert[0] 
            ?
                <div className="licenseAlert">
                    <Alert className="licenseAlert2 fw-bold" variant="filled" severity="success">
                        {successAlert[1]}
                    </Alert>
                </div>
            : null}


            <div className="formButtons">
                <button className="formButton1" onClick={handleSubmit(handleUpload)} type="submit">
                    <Upload />
                    <span> Upload License </span>
                </button>
            </div>
        </Form>
    );
}


// Request License for HTe Pro/Business
const RequestLicense = (props) => {

    const { register, handleSubmit, reset,  formState: {errors} } = useForm();
    const { title, openPopup, setOpenPopup } = props;
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend
    
    const [formattedData, setFormattedData] = useState("") // Body of email (content of license request)

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        input: "",
    };

    const handleCloseDialog = (event) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);
        setFormattedData("");

        event.stopPropagation()
        reset({defaultValues});
    }

    const handleSendAsEmail = (data) => {
        logger.info("License Request Data: ", data)

        // TODO: Find out what to do about CSID, hardcoded for now
        // %0D%0A = Encoded character for a 'New Line' 
        setFormattedData(`HOBART HTe LICENSE REQUEST FORM 
            %0D%0A%0D%0AResponsible Individual:${data.responsibleIndividual}
            %0D%0ATitle:${data.title}
            %0D%0ACompany Name:${data.companyName}
            %0D%0ACompany Address:${data.companyAddress}
            %0D%0AEmail Address:${data.email}
            %0D%0APhone Number:${data.phoneNumber}
            %0D%0ASales Rep:${data.salesRep}
            %0D%0AOrder Number:${data.orderNumber}
            %0D%0ARequest Type:${data.requestType}
            %0D%0ALicense Type Requested: [${data.licenseType}]
            %0D%0ACOMPUTING SYSTEM ID=123456`
        )
    }

    // POST License upload to API
    const handleRequest = async (data) => {      // When the Submit button is pressed
        logger.info("POST request (Full Data - Save as File): ", data);
        setShowAlert([false, "Something went wrong!"])

        // TODO: Find out what to do about CSID, hardcoded for now
        let temp = `HOBART HTe LICENSE REQUEST FORM 
            Responsible Individual:${data.responsibleIndividual}
            Title:${data.title}
            Company Name:${data.companyName}
            Company Address:${data.companyAddress}
            Email Address:${data.email}
            Phone Number:${data.phoneNumber}
            Sales Rep:${data.salesRep}
            Order Number:${data.orderNumber}
            Request Type:${data.requestType}
            License Type Requested: [${data.licenseType}]
            COMPUTING SYSTEM ID=123456`

        const element = document.createElement("a");
        const fileData = new Blob([temp], {type: 'text/plain'});
        element.href = URL.createObjectURL(fileData);
        element.download = `HTeLicenseRequest-${data.responsibleIndividual}.req`;
        document.body.appendChild(element); // Required for this to work in FireFox
        element.click();
    }


    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>        
                <Form className="form-control" onSubmit={handleSubmit(handleRequest)}>
                    <div className="licenseContainer1">

                        <div className="form-control licenseGroup">
                            <FormCheck.Label>Request Type</FormCheck.Label>
                            <div className="htGroup1">
                                <div className="licenseTypes">
                                    <input type="radio" id="type1" value="HTE_DEMO"
                                        {...register("requestType", {required: 'Please select an option' 
                                    })}/> 
                                    <span className="radioLabel"> Demo </span>
                                </div>

                                <div className="licenseTypes">
                                    <input type="radio" id="type2" value="HTE_PURCHASE"
                                        {...register("requestType", {required: 'Please select an option' 
                                    })}/>       
                                    <span className="radioLabel"> Purchase </span>
                                </div>

                                <br />

                                {errors.requestType && <p className="formError">{errors.requestType.message}</p>}

                            </div>
                        </div>

                        <div className="form-control licenseGroup">
                            <FormCheck.Label>License Type</FormCheck.Label>
                            <div className="htGroup1">
                                <div className="licenseTypes">
                                    <input type="radio" id="type3" value="HTE_SCALEMGMT_SINGLESTORE"
                                        {...register("licenseType", {required: 'Please select an option' 
                                    })}/>       
                                    <span className="radioLabel"> HTe Pro (Single Store) </span>

                                </div>

                                <div className="licenseTypes">
                                    <input type="radio" id="type4" value="HTE_SCALEMGMT_MULTISTORE"
                                        {...register("licenseType", {required: 'Please select an option' 
                                    })}/>            
                                    <span className="radioLabel"> HTe Business (Multi Store) </span>
                                </div>

                                <br />
                                
                                {errors.licenseType && <p className="formError">{errors.licenseType.message}</p>}

                            </div>
                        </div>

                        <div className="form-control licenseGroup">
                            <FormCheck.Label>Included Features</FormCheck.Label>
                            <li>Media Management</li>
                            <li>Label Designer</li>
                            <li>Flashkey Editor</li>
                            <li>Remote Monitor</li>
                            <li>Legacy Support</li>
                            <li>Alerts</li>
                        </div>
                    </div>     

                    <div className="licenseContainer2">
                        {/* Column 1 */}
                        <div className="licenseInfo">
                            <FormInput
                                label="Responsible Individual: "
                                id="responsibleIndividual"
                                name="responsibleIndividual"
                                type="text"
                                placeholder="Enter a name"
                                register={register}
                                validation={{
                                    required: "Please enter a name",
                                    maxLength: { value: 60, message: "You exceeded the max name length"}
                                }}
                                error={errors.responsibleIndividual}
                            />

                            <FormInput
                                label="Title: "
                                id="title"
                                name="title"
                                type="text"
                                placeholder="Enter a job title"
                                register={register}
                                validation={{
                                    required: "Please enter a job title",
                                    maxLength: { value: 60, message: "You exceeded the max title length"}
                                }}
                                error={errors.title}
                            />

                            <FormInput
                                label="Company Name: "
                                id="companyName"
                                name="companyName"
                                type="text"
                                placeholder="Enter a company name"
                                register={register}
                                validation={{
                                    required: "Please enter a company name",
                                    maxLength: { value: 60, message: "You exceeded the max company name length"}
                                }}
                                error={errors.companyName}
                            />

                            <FormInput
                                label="Company Address: "
                                id="companyAddress"
                                name="companyAddress"
                                type="text"
                                placeholder="Enter a company address"
                                register={register}
                                validation={{
                                    required: "Please enter a company address",
                                    maxLength: { value: 60, message: "You exceeded the max company address length"}
                                }}
                                error={errors.companyAddress}
                            />

                            <FormInput
                                label="Email Address "
                                id="email"
                                name="email"
                                type="email"
                                placeholder="example@company.com"
                                register={register}
                                validation={{
                                    required: "Please enter your email address",
                                    maxLength: { value: 60, message: "You exceeded the max email length"}
                                }}
                                error={errors.email}
                            />
                        </div>

                        {/* Column 2 */}
                        <div className="licenseInfo">
                            <FormInput
                                label="Phone Number (optional): "
                                id="phoneNumber"
                                name="phoneNumber"
                                type="tel"
                                register={register}
                                validation={{maxLength: { value: 16, message: "You exceeded the max phone number length"}
                                }}
                                error={errors.phoneNumber}
                            />

                            <FormInput
                                label="Sales Rep (optional): "
                                id="salesRep"
                                name="salesRep"
                                type="text"
                                register={register}
                                validation={{maxLength: { value: 60, message: "You exceeded the max name length"}}}
                                error={errors.salesRep}
                            />

                            <FormInput
                                label="Order Number (optional): "
                                id="orderNumber"
                                name="orderNumber"
                                type="number"
                                register={register}
                                validation={{maxLength: { value: 60, message: "You exceeded the max order number length"}}}
                                error={errors.orderNumber}
                            />
                        </div>
                    </div>

                    <br />

                    {/* Error uploading file */}
                    {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleSubmit(handleSendAsEmail)} type="submit">
                            <Email />
                            <span> Generate Email</span>
                        </button>

                        <button className="formButton1" onClick={handleSubmit(handleRequest)} type="submit">
                            <Save />
                            <span> Save as File </span>
                        </button>

                        <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>

                    {formattedData === ""
                        ? null
                        : <ButtonMailto 
                            label="Click here to send us a license request via Email"
                            mailto={`mailto:HT.Support@itwfeg.com?subject=HTe License Request&body=${formattedData}`} 
                        />
                    }
                </Form>
            </DialogContent>
        </Dialog>
    );
}


export { RequestEnterpriseLicense, UploadEnterpriseLicense, RequestLicense };