import { useState } from 'react';
import { alpha, styled } from '@mui/material/styles';
import Switch from '@mui/material/Switch';
import { useDispatch } from "react-redux";
import FormControlLabel from '@mui/material/FormControlLabel';
import { store } from '../../../state';
import {  USER_ENABLE_DISABLE } from '../../../state/actions/accessActions';
import { ScaleAPI, StoreAPI } from '../../api';
import AccessControl from '../../common/AccessPermissions';
import { useRspHandler } from '../../utils/ResponseProvider';
import { IconButton, Tooltip, Typography } from '@material-ui/core';
import { Help } from '@material-ui/icons';
import { HTeUser } from '../../../types/access/AccessTypes';
import { Store } from '../../../types/asset/AssetTypes';
import { ScaleDevice } from '../../../types/asset/ScaleTypes';
import { ResponseType } from '../../../types/storeTypes';

const GreenSwitch = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: "#ba4735",
      '&:hover': {
        backgroundColor: alpha("#ba4735", theme.palette.action.hoverOpacity),
      },
    },
    '& .MuiSwitch-switchBase.Mui-checked.Mui-disabled': {
        color: "#f5f5f5",
    },
    '& .MuiSwitch-switchBase.Mui-disabled + .MuiSwitch-track':{
        backgroundColor: "#000 !important"
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: "#ba4735",
    },
}));

interface SetEnableProps{
    info: Store | ScaleDevice 
    user: HTeUser
    operation?: "STORE" | "SCALE"
    disabled?: boolean
}

export default function SetEnable(props : SetEnableProps){
    const {info, user, operation = "SCALE", disabled = false} = props;
    const dispatch = useDispatch();
    const [checked, setChecked] = useState(info == null ? false : info.enabled);
    const {callbacks, addCallback} = useRspHandler();

    //Callback related
    function onSuccess(response : ResponseType){
        if(operation === "STORE"){
            dispatch(StoreAPI.fetchStores(Date.now(), info.storeId))
        } 
    }

    const handleSwitchChange = (event : any) => {
        const newID = Date.now()
    
        function onFail(response : ResponseType){
            setChecked(!checked);
        }
        if(addCallback(callbacks, newID, operation === "STORE" ? "Enable Store" : "Enable Scale" , onSuccess, onFail)){
            setChecked(event.target.checked);

            if(operation === "STORE"){
                dispatch(StoreAPI.setStoreEnable(newID, info.storeId, event.target.checked))
            }
            else{
                dispatch(ScaleAPI.setScaleEnable(newID, info.ipAddress ?? "", event.target.checked, info.storeId, null, null, null))

            }
        }
    };

    return(
        <>
            {/* Enable/Disable (role: 1001) */}
            <AccessControl
                user={user}
                requiredRole={store.getState().access.roles[USER_ENABLE_DISABLE]}
            >
                <div className='noClass' style={{display: "flex"}}>
                    <FormControlLabel style={{marginLeft:'10px', marginRight:'4px', flex:2}}
                        control={
                            <GreenSwitch
                                disabled={disabled === true}
                                checked={checked}
                                onClick={handleSwitchChange}
                                inputProps={{ 'aria-label': 'controlled' }}    
                                name="scaleEnable"
                            />
                        }
                        label={operation === "STORE" ? "Store Enable" : "Scale Enable"}
                    />

                    <Tooltip className="info" 
                            style={{flex:2, justifyContent:'left'}} 
                            title={<Typography> Indicates whether a {operation === "STORE" ? "store" : "scale"} can receive new data from the server</Typography>}>
                        <IconButton aria-label="enableAccount" disableRipple style={{padding:"0px"}}>
                            <Help />
                        </IconButton>
                    </Tooltip>
                </div>
                

            </AccessControl>
        </>
    )

}