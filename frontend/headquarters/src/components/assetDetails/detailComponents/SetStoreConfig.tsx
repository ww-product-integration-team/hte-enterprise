import { useState, useEffect} from 'react';
import { alpha, styled } from '@mui/material/styles';
import Switch from '@mui/material/Switch';
import { useDispatch } from "react-redux";
import FormControlLabel from '@mui/material/FormControlLabel';
import { store } from '../../../state';
import {  SYSTEM_MANAGER} from '../../../state/actions/accessActions';
import { StoreAPI } from '../../api';
import AccessControl from '../../common/AccessPermissions';
import { useRspHandler } from '../../utils/ResponseProvider';
import { IconButton, Tooltip} from '@material-ui/core';
import { Help } from '@material-ui/icons';
import { HTeUser } from '../../../types/access/AccessTypes';
import { Store } from '../../../types/asset/AssetTypes';
import { Button, Form } from '@themesberg/react-bootstrap';
import { Typography } from '@mui/material';
import { FormCheck } from 'react-bootstrap';
import { FormInput } from '../../forms/FormHelper';
import { useForm } from 'react-hook-form';
import { AddCircleOutline, RemoveCircle } from '@mui/icons-material';
import { v4 as uuidv4 } from 'uuid';
import logger from '../../utils/logger';

const GreenSwitch = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: "#ba4735",
      '&:hover': {
        backgroundColor: alpha("#ba4735", theme.palette.action.hoverOpacity),
      },
    },
    '& .MuiSwitch-switchBase.Mui-checked.Mui-disabled': {
        color: "#f5f5f5",
    },
    '& .MuiSwitch-switchBase.Mui-disabled + .MuiSwitch-track':{
        backgroundColor: "#000 !important"
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: "#ba4735",
    },
}));

function NewlineText(props : {text : string}) {
    const text = props.text;
    return(text.split('\n').map(str => <p key={uuidv4()} style={{margin: "0px", textAlign:"center"}}>{str}</p>));
}

interface SetStoreConfigProps{
    info: Store 
    user: HTeUser
    disabled?: boolean
}

export default function SetStoreConfig(props : SetStoreConfigProps){
    interface StoreConfigForm{
        sendStoreInfo: boolean
        customStoreName: string | null
    }

    const { register, handleSubmit, reset, formState: {errors} } = useForm<StoreConfigForm>();
    const {info, user, disabled = false} = props;
    const dispatch = useDispatch();
    const [newStoreConfig, setNewStoreConfig] = useState<StoreConfigForm>({
        sendStoreInfo: false,
        customStoreName: null
    })
    const [checked, setChecked] = useState((!info.sendStoreInfo) ? false : true);
    const {callbacks, addCallback} = useRspHandler();


    useEffect(()=>{
        setNewStoreConfig({
            sendStoreInfo: (!info.sendStoreInfo || info.sendStoreInfo === "false") ? false : true,
            customStoreName : !info.sendStoreInfo || info.sendStoreInfo === "true" || info.sendStoreInfo === "false" ? null : info.sendStoreInfo
        })
    },[info])

    //Callback related
    function onSuccess(response : any){
        dispatch(StoreAPI.fetchStores(Date.now(), info.storeId))
    }

    function onFail(response : any){
        dispatch(StoreAPI.fetchStores(Date.now(), info.storeId))
    }

    const handleCheck = (event: any) => {
        setChecked(event.target.checked)

        if(!event.target.checked){
            let newId = Date.now()
            let storeToUpdate = {...info}
            storeToUpdate.sendStoreInfo = null
            if(addCallback(callbacks, newId, "Set Store Config to Default", onSuccess, onFail, {
                requestMsg: "Requesting Default Store Config...",
                successMsg: "Success! Store now will follow the Application Setting",
                failMsg: "default"
            })){
                dispatch(StoreAPI.postStore(newId, storeToUpdate, storeToUpdate.storeId))
            }
        }
    }

    const handleConfigChange = handleSubmit(async (data : StoreConfigForm) => {
        
        let storeToUpdate = {...info}

        if(!checked){
            storeToUpdate.sendStoreInfo = null
        }
        else if(!newStoreConfig.sendStoreInfo){
            storeToUpdate.sendStoreInfo = "false"
        }
        else if(newStoreConfig.customStoreName == null){
            storeToUpdate.sendStoreInfo = "true"
        }
        else{
            storeToUpdate.sendStoreInfo = newStoreConfig.customStoreName
        }

        let newId = Date.now()
        if(addCallback(callbacks, newId, "Set Store Config", onSuccess, onFail, {
            requestMsg: "Requesting Configuration Change...",
            successMsg: "default",
            failMsg: "default"
        })){
            dispatch(StoreAPI.postStore(newId, storeToUpdate, storeToUpdate.storeId))
        }

    });

    return(
        <>
            {/* Enable/Disable (role: 1001) */}
            <AccessControl
                user={user}
                requiredRole={store.getState().access.roles[SYSTEM_MANAGER]}
                requiredDomain={store.getState().access.domains[info.storeId]}
            >
            
                <div style={{maxWidth:"50rem", marginTop: ".5rem"}}>
                    <Form className="mt-4">
                        <div className="form-control">
                            <div className='noClass' style={{display: "flex"}}>
                                <FormControlLabel style={{marginLeft:'10px', marginRight:'4px'}}
                                    control={
                                        <GreenSwitch
                                            disabled={disabled === true}
                                            checked={checked}
                                            onClick={(e : any) => {handleCheck(e)}}
                                            onChange={(e: any) => logger.debug("Set Custom Store chang")}
                                            inputProps={{ 'aria-label': 'controlled' }}    
                                            name="customStoreEnable"
                                        />
                                    }
                                    label={"Set Custom Store Info"}
                                />

                                <Tooltip className="info" 
                                        title={<Typography> Sets a custom store name to be sent down to scales belonging to this store</Typography>}>
                                    <IconButton aria-label="enableAccount" disableRipple style={{padding:"0px"}}>
                                        <Help />
                                    </IconButton>
                                </Tooltip>
                            </div>

                            {checked ?
                            <>
                                <Form.Label >Store Configuration:</Form.Label>
            
                                <div style={{display:"flex", paddingBottom: "1rem", paddingLeft : "2rem", flexWrap:"wrap"}}>
                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Label className="customCheckbox">
                                            <FormCheck.Input checked={newStoreConfig.sendStoreInfo} 
                                                            id="sendStoreInfo"
                                                            className="me-2" 
                                                            onClick={(e : React.MouseEvent<HTMLInputElement, MouseEvent>) => setNewStoreConfig({...newStoreConfig, sendStoreInfo: e.currentTarget.checked})}
                                            />
                                            Send Store Info
                                        </FormCheck.Label>
                                    </FormCheck>
                                </div>
            
                                <span className="fw-bold">Custom Store Name instructions</span>
                                <ul>
                                    <li>NOTE: This is a custom configuation for only this store. If you want to set a configuration for the whole Application go to the settings page. </li>
                                    <li>Please enter a '\n' in the string to move to a new line</li>
                                </ul>
            
                                <div style={{display:"flex", alignItems:"flex-end"}}>
                                    <div style={{width: '100%'}}>
                                        <FormInput
                                            className="mb-2"
                                            label="Custom Store Name: "
                                            id="customStoreName"
                                            name="customStoreName"
                                            disabled={!newStoreConfig.sendStoreInfo}
                                            defaultValue={(info.sendStoreInfo && info.sendStoreInfo !== "false" && info.sendStoreInfo !== "true") ? info.sendStoreInfo : null}
                                            placeholder="Enter a Custom Store name here"
                                            onChange = {(event : React.ChangeEvent<HTMLInputElement>) => {
                                                let newStr = event.currentTarget.value.replaceAll("\\n", "\n")
                                                setNewStoreConfig({...newStoreConfig, customStoreName: newStr})}}
                                            register={register}
                                            validation={{
                                                maxLength: { value: 120, message: "You exceeded the max length"}
                                            }}
                                            error={errors.customStoreName}
                                        />
                                    </div>
            
                                    <Button id="actionBar1" size="sm" style={{padding:"0.5rem", backgroundColor:"#a4abb0", marginLeft:"1rem", marginBottom:"0.5rem", maxHeight: '3rem'}}
                                        onClick={() => {
                                            reset({sendStoreInfo: newStoreConfig.sendStoreInfo, customStoreName: ""});
                                            setNewStoreConfig({...newStoreConfig, customStoreName: null})
                                        }}> 
            
                                        <div style={{display:"flex"}}>
                                            <RemoveCircle style={{marginRight:"0.5rem"}}/>
                                            Reset
                                        </div>
                                    </Button>
                                </div>
                                
                                
                                <Form.Label >Expected Output:</Form.Label>
                                <div style={{display:"flex", alignItems: "center", justifyContent: "center", paddingBottom: "1rem", flexWrap:"wrap"}}>
                                    <div style={{backgroundColor:"rgba(186, 71,53, 0.25)", padding: "1rem", borderRadius: 10}}>
                                        {NewlineText({text: newStoreConfig.customStoreName ? newStoreConfig.customStoreName : `${info.storeName}\n${info.address}`})}
                                    </div>
                                </div>
            
                                <div style={{display:"flex", alignItems: "right", justifyContent:"right"}}>
            

                                    <Button id="actionBar1" variant="outline-primary" size="sm" type="submit" onClick={handleConfigChange}> 
                                        <AddCircleOutline style={{marginRight: '8px'}}/>
                                        Update Store
                                    </Button>
                                </div>
                            </>
                            : null}
                        </div>
                    </Form>
                </div>


            </AccessControl>
        </>
    )

}