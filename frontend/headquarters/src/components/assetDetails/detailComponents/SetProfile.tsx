import { useState, useEffect } from 'react';
import { alpha, styled } from '@mui/material/styles';
import Switch from '@mui/material/Switch';
import { useDispatch } from "react-redux";
import FormControlLabel from '@mui/material/FormControlLabel';
import 'status-indicator/styles.css'
import { Form } from '@themesberg/react-bootstrap';
import { AccessActions, store } from '../../../state';
import { ASSIGN_SCALE_PROFILES } from '../../../state/actions/accessActions';
import { ScaleAPI } from '../../api';
import AccessControl from '../../common/AccessPermissions';
import { useRspHandler } from '../../utils/ResponseProvider';
import logger from '../../utils/logger';
import { ScaleDevice } from '../../../types/asset/ScaleTypes';
import { HTeUser } from '../../../types/access/AccessTypes';
import { Profile } from '../../../types/asset/ProfileTypes';
import { ResponseType } from '../../../types/storeTypes';


const GreenSwitch = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: "#ba4735",
      '&:hover': {
        backgroundColor: alpha("#ba4735", theme.palette.action.hoverOpacity),
      },
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: "#ba4735",
    },
}));


//Manual Profile Enabled
const isUsingManualProfile = (scaleInfo : ScaleDevice) =>{
    //Find the assigned parent department
    let dept = store.getState().asset.depts[scaleInfo.deptId]

    if(dept == null){return false}
    //if found check if the profile IDs match, if they don't then scale is using manual profile
    return dept.defaultProfileId !== scaleInfo.profileId

}

interface SetProfileProps{
    classes:any
    scaleProperties: ScaleDevice
    user: HTeUser
    profiles: Profile[]
    scales: ScaleDevice[]
}

export default function SetProfile(props : SetProfileProps){
    const {classes, scaleProperties, user, profiles, scales} = props
    const dispatch = useDispatch()
    const {callbacks, addCallback} = useRspHandler()

    profiles.sort((a,b) => a.name.localeCompare(b.name, "en", {numeric: true}))

    let scaleInfo = scales.filter(scale => scale.deviceId === scaleProperties.deviceId)[0]
    if (!scaleInfo) {
        scaleInfo = scaleProperties
    }

    //Using a Manual Profile
    const [profileChecked, setProfileChecked] = useState(isUsingManualProfile(scaleInfo));
    //The current profile used by the element
    const [currentProfile, setCurrentProfile] = useState(scaleInfo.profileId);

    //Whether user has individual Assign Scale profiles role
    let assignScaleProfiles = user.roles.find(role => role.name === 'AssignScaleProfiles'); 

    //Callback related
    function onSuccess(response : ResponseType){
        // dispatch(ScaleAPI.fetchScales(Date.now()+1))
    }

    function onFail(response : ResponseType){
        logger.error("(SetAutoProfile) Error setting auto profile", response)
        setCurrentProfile(scaleInfo.profileId);
    }

    //When the Switch is changed
    const handleProfileSwitchChange = (event : any, sendRequest = true) => {
        setProfileChecked(event.target.checked);
        setCurrentProfile(scaleInfo.profileId);
        if(!event.target.checked && sendRequest){
            //If the manual switch is set to false make sure the scale is using the departments profile


            const newID = Date.now()
            addCallback(callbacks, newID, "Set Auto Profile", onSuccess, onFail, {requestMsg: "default", successMsg:scaleInfo.ipAddress + " is now using default department profile", failMsg: "default"})
            dispatch(ScaleAPI.setScaleProfile(newID,scaleInfo.deviceId, "AUTO", scaleInfo.storeId, null, null, null))
        }
    };

    useEffect(() => {
        let isChecked = isUsingManualProfile(scaleInfo)
        let newEvent = {target:{checked: isChecked}}
        handleProfileSwitchChange(newEvent, false)

    }, [scales])

    //When a new profile is selected 
    const handleView = (e : React.FormEvent<HTMLSelectElement>) => {
        const profileIndex = profiles[e.currentTarget.options.selectedIndex-1].profileId;
        logger.info("(ScaleViewDetails) Setting current profile to " + profileIndex)
        setCurrentProfile(profileIndex)

        const newID = Date.now()
        function onFailure(response : ResponseType){
            logger.info("(SetProfile) Error setting profile", response)
            setCurrentProfile(scaleInfo.profileId);
        }
        if(addCallback(callbacks, newID, "Set Scale Profile", onSuccess, onFailure, {requestMsg: 'Contacting ' + scaleInfo.deviceId + '...', successMsg:"default", failMsg:"default"})){
            dispatch(ScaleAPI.setScaleProfile(newID,scaleInfo.deviceId, profileIndex, scaleInfo.storeId, null, null, null))
        }

    };

    const disableKey = (ev : React.KeyboardEvent<HTMLSelectElement>) => {
        if (ev) {
            if   (ev.preventDefault)     ev.preventDefault();
        } 
     }


    return(
        <>
            {/* AssignScaleProfiles (role: 1004) */}
            <AccessControl
                user={user}
                requiredRole={store.getState().access.roles[ASSIGN_SCALE_PROFILES]}
            >
                <div className={classes.labelRoot}>
                    <FormControlLabel style={{marginLeft:'10px'}}
                        control={
                            <GreenSwitch
                            disabled={scaleInfo.deptId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID || scaleInfo.storeId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID}
                            checked={profileChecked}
                            onClick={handleProfileSwitchChange}
                            inputProps={{ 'aria-label': 'controlled' }}    
                            name="manualProfileEnable"
                            />
                        }
                        label="Set Manual Profile"
                    />

                    <Form.Select className="profileSelector" style={{margin: 5,backgroundColor: "transparent"}}
                        onChange={(e) => handleView(e)}
                        onKeyDown={(e) => disableKey(e)}
                        onClick={(e)=> e.stopPropagation()}
                        value={currentProfile ?? ""}
                        disabled={!profileChecked}
                    >
                        <option value="" disabled>--Select a profile--</option> : null
                        
                        {profiles.map(profile => (
                            <option key={profile.profileId} value={profile.profileId}>
                                {profile.name}
                            </option>
                        ))}
                    </Form.Select>

                </div>
            </AccessControl>      

        </>
    );

}