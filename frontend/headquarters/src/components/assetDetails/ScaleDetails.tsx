import { useState, useEffect} from 'react';
import { Link, useHistory } from 'react-router-dom'
import { Button, ButtonGroup} from '@themesberg/react-bootstrap';
import { AccountTree} from '@material-ui/icons';
import { useDispatch } from "react-redux"
import ScaleIcon from '../icons/ScaleIcon';
import { AssetAPI, ScaleAPI} from '../api';
import Box from '@material-ui/core/Box';
import './AssetDetailsStyle.css';
import { AccessActions, AssetActions, store, useAppSelector } from '../../state';
import {GetMessageView} from '../assetTreeView/MessageIndicator'
import ScaleActionsComponent from '../assetTreeView/ScaleActionsComponent';
import DeleteIcon from '@mui/icons-material/Delete';
import RemoveShoppingCartIcon from '@mui/icons-material/RemoveShoppingCart';
import MonitorHeartIcon from '@mui/icons-material/MonitorHeart';
import { ScaleSyncGraph } from '../dashboard/widgets/ScalesValuesChart';
import SetProfile from './detailComponents/SetProfile';
import SetEnable from './detailComponents/SetEnable';
import { DeleteScale, RemoveScale } from '../forms/ScaleForm';
import { notify } from 'reapop';
import { Alert } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import ConnectedTvIcon from '@mui/icons-material/ConnectedTv';
import AddchartIcon from '@mui/icons-material/Addchart';
import TuneIcon from '@mui/icons-material/Tune';
import { HTeRole, HTeUser } from '../../types/access/AccessTypes';
import { DEFAULT_UNASSIGNED_DOMAIN_ID, MONITOR } from '../../state/actions/accessActions';
import { AssetTypes } from '../../types/asset/AssetTypes';
import { Dispatch } from 'redux';
import { format } from 'date-fns';
import { Dialog, DialogContent, DialogTitle, Typography } from '@material-ui/core';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import AccessControl from '../common/AccessPermissions';
import { ScaleDevice } from '../../types/asset/ScaleTypes';

/**
 * This function allow you to modify a JS Promise by adding some status properties.
 * Based on: http://stackoverflow.com/questions/21485545/is-there-a-way-to-tell-if-an-es6-promise-is-fulfilled-rejected-resolved
 * But modified according to the specs of promises : https://promisesaplus.com/
 */
export class QuerablePromise<T> extends Promise<T>{
    public isPending = true;
    public isRejected = false;
    public isFullfilled = false;

    constructor(executor: (resolve: (value: T | PromiseLike<T>) => void, reject: (reason?: any) => void) => void) {
        super(executor);
        this.isPending = true;
        this.isRejected = false;
        this.isFullfilled = false;
    }

}

 function MakeQuerablePromise(promise : any){
    // Don't modify any promise that has been already modified.
    if (promise.isFullfilled) return promise;

    // Set initial state
    let isPending = true;
    let isRejected = false;
    let isFulfilled = false;

    // Observe the promise, saving the fulfillment in a closure scope.
    let result : QuerablePromise<any> = promise.then(
        function(v : any) {
            isFulfilled = true;
            isPending = false;
            return v; 
        }, 
        function(e : any) {
            isRejected = true;
            isPending = false;
            throw e; 
        }
    ) as QuerablePromise<any>

    result.isFullfilled = isFulfilled
    result.isPending = isPending
    result.isRejected = isRejected
    return result;
}

interface ScaleDetailProps{
    scale: ScaleDevice
    classes : any
    user : HTeUser
    roles : {[key: string] : HTeRole}
    getExpandedArray : (d : Dispatch<any>, storeId: string, deptId?: string) => void
}
export default function ScaleDetails(props : ScaleDetailProps){
    const {scale, classes, user, roles, getExpandedArray} = props;
    const dispatch = useDispatch();
    const state = useAppSelector((state) => state.asset.scaleDetails)
    const profiles = useAppSelector((state) => state.profile.profiles);
    const nodeStatus = useAppSelector((state) => state.asset.nodeData)
    
    const domains = useAppSelector((state) => state.access.domains)
    
    let scaleInfo = scale
    let nodeInfo = nodeStatus[scale.deviceId]

    const [refresher, setRefresher] = useState(false);
    const [openDelete, setOpenDelete] = useState<AssetTypes | null>(null);        // DELETE Request
    const [openRemove, setOpenRemove] = useState<AssetTypes | null>(null);        // REMOVE Request
    const [openHealthLog, setOpenHealthLog] = useState(false)
    const history = useHistory();

    useEffect(() => {

        if(props !== undefined && scaleInfo){
            dispatch(ScaleAPI.getDeviceSettings(Date.now(), scaleInfo.ipAddress))
            dispatch(AssetAPI.getNodeStatus(Date.now()+1, scaleInfo.deviceId, "DEVICE", scaleInfo.storeId))
        }
        setRefresher(true)
    }, []);

    useEffect(() => {
        if(refresher){
            setRefresher(false)
            const refreshPromise : Promise<any> = new Promise(function(resolve, reject) {setTimeout(() => resolve("done"), 10000);})
            .then(function(result){
                if(props !== undefined && scaleInfo){
                    const id = Date.now()
                    dispatch(ScaleAPI.getDeviceSettings(id, scaleInfo.ipAddress))
                    dispatch(AssetAPI.getNodeStatus(id+1, scaleInfo.deviceId, "DEVICE", scaleInfo.storeId))
                }
            })
            
            let refreshStatus = MakeQuerablePromise(refreshPromise);
            refreshStatus.then(function(data : any){
                setRefresher(true)
            })
        }
    }, [refresher])

    if(!scaleInfo){
        return(
            <Alert className="m-1" variant="filled" severity="error">Could not find information for Scale <b>{scale.deviceId}</b>!</Alert>
        )
    }

    let scaleSettings = state[scaleInfo.deviceId] ?? {}

    function deleteSuccessCallback(response){
        new Promise(function(resolve, reject) {setTimeout(() => resolve("done"), 1000);})
            .then(function(result){dispatch(notify('Scale ' + scaleInfo.ipAddress + ' has been deleted!', 'success', {dismissible: true, dismissAfter: 10000, position: 'bottom-right'}))})
        
        history.push('/')
    }

    function deleteFailCallback(response){

        dispatch(notify('Scale ' + scaleInfo.ipAddress + ' could not be deleted!', 'error', {dismissible: true, dismissAfter: 10000, position: 'bottom-right'}))


    }

    const deptName = scaleInfo.deptId === DEFAULT_UNASSIGNED_DOMAIN_ID ? "Unassigned" : 
                    store.getState().asset.depts[scaleInfo.deptId] ? 
                    store.getState().asset.depts[scaleInfo.deptId].deptName1 : "Unknown"

    const storeName = scaleInfo.storeId === DEFAULT_UNASSIGNED_DOMAIN_ID ? "Unassigned" : 
                    store.getState().asset.stores[scaleInfo.storeId] ? 
                    store.getState().asset.stores[scaleInfo.storeId].storeName : "Unknown"

    function createSyncMessage(){
        if(!nodeInfo){
            return(
                GetMessageView('error',"Could not fetch sync status for the scale")
            )
        }
        
        switch(nodeInfo.sync){
            case "NA":
                return (GetMessageView('disabled',nodeInfo.syncMessage))
            case "NOT_SYNCED":
                return(GetMessageView('error',nodeInfo.syncMessage))
            case "PENDING":
                return(GetMessageView('warning',nodeInfo.syncMessage))
            case "SYNCED":
                return(GetMessageView('synced',nodeInfo.syncMessage))
            default:
                return(GetMessageView('error',"No Sync info!"))
        }
    }

    function showCurrentConfiguration(){
        if(!scaleSettings || !scaleSettings.currentFiles){
            return null
        }

        let files = scaleSettings.currentFiles.split(",")
        let totalPre = ""
        let fileStr = ""
        let prefix = ""
        files.forEach((item) =>{
            let fileAndChecksum = item.split("=")
            
            if(fileAndChecksum.length === 2){
                totalPre = "Scale's downloaded configuration:"
                fileStr += prefix + fileAndChecksum[0]
                prefix = ", "
            }
        })
        
        return <span><b>{totalPre}</b> {fileStr}</span>
        
    }
    
    const scaleTitles = {
        "deviceId"      : "Device ID",
        "application"   : "Application Version",
        "buildNumber"   : "Build Number",
        "countryCode"   : "Country Code",
        "deptId"    : "Department", //Display Department name instead of ID
        "enabled"       : "Enabled",
        "hostname"      : "Hostname",
        "ipAddress"     : "IP Address",
        "macAddress"    : "MAC Address",
        "last24HourStatus" : "Last 24 Hour Status", //Omit
        "last24HourSync" : "Last 24 Hour Sync", //Omit
        "lastReportTimestampUtc" : "Last Report", //Format Timestamp
        "lastTriggerType" : "Last Trigger Type",
        "loader"        : "Loader",
        "operatingSystem" : "Operating System",
        "pluCount"      : "PLU Count",
        "primaryScale"  : "Primary Scale",
        "profileId"     : "Profile", //Display Profile name rather than ID
        "scaleModel"    : "Scale Model",
        "serialNumber"  : "Serial Number",
        "smBackend"     : "SM Backend",
        "storeId"   : "Store", //Display Store name rather than ID
        "systemController" : "System Contoller",
        "totalLabelsPrinted" : "Total Labels Printed",

        //Details

        "features"          : "Features", //Format to be multiline
        "hasCustomerDisplay" : "Customer Display", //Format to Yes/No
        "hasLoadCell"       : "Load Cell", //Format to Yes/No
        "labelStockSize"    : "Label Stock",  
        "lastCommunicationTimestampUtc" : "Last Communication", //Format time 
        "lastFetchPluOffsetTimestampUtc" : "Last Fetch of PLU", //Format timestamp
        "lastRebootTimestampUtc" : "Last Reboot",
        "licenses"          : "Licenses", //Format to be multiline
        "scaleItemEntryMode" : "Entry Mode",
        "scaleMode"         : "Scale Mode",
        "storeGraphicName"  : "Store Graphic",
        "storeInformation"  : "Store Information",
        "timeKeeper"        : "Time Keeper",
        "timezone"          : "Time Zone",
        "weigherCalibratedTimestampUtc" : "Last Weigher Calibration", //Format timestamp
        "weigherConfiguredTimestampUtc" : "Last Weigher Configuration", //Format timestamp 
        "weigherPrecision" : "Weigher Precision"
    }

    const handleCloseHealthLog = () => {
        setOpenHealthLog(false)
    }

    return(
        <>
         <AccessControl
            user={user}
            requiredDomain={domains[scaleInfo.storeId]}
            requiredRole={roles[MONITOR]}
            pageRequest = {true}
        >
            <div className="mb-4 mb-md-0">

                <div className="d-flex justify-content-between">
                    <ScaleIcon className="svg_icons"/>
                    <h5>IP: <b style={{color : "#ba4735"}}>{scaleInfo.ipAddress}</b></h5>
                    <h5>Hostname: <b style={{color : "#ba4735"}}>{scaleInfo.hostname}</b></h5>
                </div>

                <div className="d-flex justify-content-between">

                    {/* Scale Profile Actions */}
                    <ScaleActionsComponent
                        scale={scaleInfo}
                        user = {user}
                        roles = {roles}
                    />

                    {/* View the scale in the tree if it is assigned*/}
                    <ButtonGroup className="assetBar">
                        {(!scaleInfo.storeId && !scaleInfo.deptId) || 
                        (scaleInfo.storeId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID && 
                        scaleInfo.deptId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID) ?
                        
                    
                        <Button id="actionBar1" size="sm" disabled={true}> 
                            <AccountTree />
                                View In Tree
                        </Button> :

                        <Link to={{ pathname: '/AssetList'}}>
                            <Button id="actionBar1" size="sm" onClick={() => {
                                dispatch(AssetActions.setFocusedAsset(scaleInfo.deviceId))
                                getExpandedArray(dispatch, scaleInfo.storeId, scaleInfo.deptId)
                            }}> 
                                <AccountTree />
                                View In Tree
                            </Button>
                        </Link>
                    
                        }
                    </ButtonGroup>                
                </div>
            </div>

            {nodeInfo ?
                GetMessageView(nodeInfo.status, nodeInfo.message) :
                GetMessageView('warning', "Could not fetch scale status")
            }

            <div style={{marginTop:20}}/>
        
            <ScaleSyncGraph
                title={<h5 style={{marginBottom:0}}><ConnectedTvIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Scale Heartbeats</b></h5>}
                graphId={scaleInfo.deviceId}
                deviceList={[scaleInfo]}
            />
            
            <h5 style={{marginTop:20}}><MonitorHeartIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Sync Status</b></h5>
            {createSyncMessage()}
            {showCurrentConfiguration()}

            <h5 style={{marginTop:20}}><TuneIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Actions</b></h5>
            {scaleInfo.deptId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID || scaleInfo.storeId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID ?
                GetMessageView("warning", "Cannot assign a profile until the scale has been assigned to a store") : null
            }

            <div style={{display: "flex",flexWrap: "wrap",}}>
                <SetProfile
                    classes={classes}
                    user={user}
                    scaleProperties={scaleInfo}
                    profiles={profiles}
                    scales={[scaleInfo]}
                />

                <SetEnable
                    user={user}
                    info={scaleInfo}
                />
            </div>

            <div style={{display: "flex",flexWrap: "wrap", paddingTop:"1rem"}}>

                <Button id="actionBar1" size="sm" style={{padding:"0.5rem", backgroundColor:"#ff4d4d", marginRight:"1rem"}}
                    onClick={() => {
                        setOpenDelete(AssetTypes.Scale); 
                    }}> 
                    <DeleteIcon style={{marginRight:"0.5rem"}}/>
                    Delete Scale

                    <DeleteScale
                        title="Delete Scale"
                        openPopup={openDelete === AssetTypes.Scale}
                        setOpenPopup={setOpenDelete}
                        scaleProperties={scaleInfo}
                        successCallback={deleteSuccessCallback}
                        failCallback={deleteFailCallback}
                    />

                </Button>
                    
                {scaleInfo.storeId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID || scaleInfo.deptId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID ? null :
                    <Button id="actionBar1" size="sm" style={{padding:"0.5rem", backgroundColor:"#a4abb0", marginRight:"1rem"}}
                        onClick={() => {
                            setOpenRemove(AssetTypes.Scale)
                        }}> 
                        <RemoveShoppingCartIcon style={{marginRight:"0.5rem"}}/>
                        Unassign Scale

                        <RemoveScale
                            title="Remove Scale"
                            openPopup={openRemove === AssetTypes.Scale}
                            setOpenPopup={setOpenRemove}
                            scaleProperties={scaleInfo}
                        />

                    </Button>
                }
                
                <Button id="actionBar1" size="sm" style={{padding:"0.5rem", backgroundColor:"#4a5073", marginRight:"1rem"}}
                    onClick={()=>{
                        setOpenHealthLog(true)
                    }}>
                    <LocalHospitalIcon style={{marginRight:"0.5rem"}}/>View Health Agent Log
                </Button>

                <Dialog open={openHealthLog} onClose={handleCloseHealthLog} fullWidth={true} maxWidth="md">
                    <DialogTitle>
                        <Typography variant="h5" component="div">Scale Health Agent Log</Typography>
                    </DialogTitle>

                    <DialogContent>
                        <Typography variant="body1" component="div" style={{whiteSpace:"pre-wrap"}}>{scaleInfo.healthInfoLog ? scaleInfo.healthInfoLog : "No Health Agent Log provided to Enterprise"}</Typography>
                    </DialogContent>

                    <Button className="formButton1" style={{"margin":"1rem auto"}} onClick={handleCloseHealthLog}>
                        <span> Close </span>
                    </Button>
                </Dialog>
            </div>
            
            <h5 style={{marginTop:20}}><InfoIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Details</b></h5>

            <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", width: '100%',}}>
                <div className={classes.accordionRooCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Host Name:</Box>
                        <Box className="boxRegular">{scaleInfo.hostname == null ? "No Report" : scaleInfo.hostname}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Serial Number:</Box>
                        <Box className="boxRegular">{scaleInfo.serialNumber == null ? "No Report" : scaleInfo.serialNumber}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Scale Model:</Box>
                        <Box className="boxRegular">{scaleInfo.scaleModel == null ? "No Report" : scaleInfo.scaleModel}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Store:</Box>
                        <Box className="boxRegular">{storeName == null ? scaleInfo.storeId == null ? "No Report" : scaleInfo.storeId : storeName}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Department:</Box>
                        <Box className="boxRegular">{deptName == null ? scaleInfo.deptId == null ? "No Report" : scaleInfo.deptId : deptName}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>PLU Count: </Box>
                        <Box className="boxRegular">{scaleInfo.pluCount == null ? "No Report" : scaleInfo.pluCount}</Box>
                    </div>
            
                </div>
                <div className={classes.accordionRootCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Application:</Box>
                        <Box className="boxRegular">{scaleInfo.application == null ? "No Report" : scaleInfo.application}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">System Controller:</Box>
                        <Box className="boxRegular">{scaleInfo.systemController == null ? "No Report" : scaleInfo.systemController}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Loader:</Box>
                        <Box className="boxRegular">{scaleInfo.loader == null ? "No Report" : scaleInfo.loader}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">SM Backend:</Box>
                        <Box className="boxRegular">{scaleInfo.smBackend == null ? "No Report" : scaleInfo.smBackend}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Last Report:</Box>
                        <Box className="boxRegular">{scaleInfo.lastReportTimestampUtc == null ? "No Report" : format(Date.parse(scaleInfo.lastReportTimestampUtc), "PPpp")}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>Total Labels Printed: </Box>
                        <Box className="boxRegular">{scaleInfo.totalLabelsPrinted == null ? "No Report" : scaleInfo.totalLabelsPrinted}</Box>
                    </div>
                </div>
            </div>

            <h5 style={{marginTop:20}}><AddchartIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Additional Data</b></h5>
            {Object.keys(scaleSettings).length === 0 ? GetMessageView("Error", "Could not retrieve additional scale data!") : null}
            <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", width: '100%',}}>
                <div className={classes.accordionRooCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">MAC Address:</Box>
                        <Box className="boxRegular">{scaleSettings.macAddress == null ? "No Report" : scaleSettings.macAddress}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">IP Address:</Box>
                        <Box className="boxRegular">{scaleSettings.ipAddress == null ? "No Report" : scaleSettings.ipAddress}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Last Reboot:</Box>
                        <Box className="boxRegular">{scaleSettings.lastRebootTimestampUtc == null ? "No Report" : format(Date.parse(scaleSettings.lastRebootTimestampUtc), "PPpp")}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Last Communication:</Box>
                        <Box className="boxRegular">{scaleSettings.lastCommunicationTimestampUtc == null ? "No Report" : format(Date.parse(scaleSettings.lastCommunicationTimestampUtc), "PPpp")}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Last Fetch of PLU:</Box>
                        <Box className="boxRegular">{scaleSettings.lastFetchPluOffsetTimestampUtc == null ? "No Report" : format(Date.parse(scaleSettings.lastFetchPluOffsetTimestampUtc), "PPpp")}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Label Stock Size:</Box>
                        <Box className="boxRegular">{scaleSettings.labelStockSize == null ? "No Report" : scaleSettings.labelStockSize}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Weigher Precision</Box>
                        <Box className="boxRegular">{scaleSettings.weigherPrecision == null ? "No Report" : scaleSettings.weigherPrecision}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Customer Display:</Box>
                        <Box className="boxRegular">{scaleSettings.hasCustomerDisplay == null ? "No Report" : scaleSettings.hasCustomerDisplay ? "Yes" : "No"}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Timezone:</Box>
                        <Box className="boxRegular">{scaleSettings.timezone == null ? "No Report" : scaleSettings.timezone}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                            <Box className="boxBold" style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>Apphook Version:</Box>
                            <Box className="boxRegular">{scaleSettings.appHookVersion == null ? "No Report" : scaleSettings.appHookVersion}</Box>
                    </div>
            
                </div>
                    <div className={classes.accordionRootCol}>
                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Licenses:</Box>
                            <Box className="boxRegular" >{scaleSettings.licenses == null ? "No Report" : scaleSettings.licenses.replaceAll(',','\n')}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Features:</Box>
                            <Box className="boxRegular" >{scaleSettings.features == null ? "No Report" : scaleSettings.features.replaceAll(',',', ')}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Store Information:</Box>
                            <Box className="boxRegular">{scaleSettings.storeInformation == null ? "No Report" : scaleSettings.storeInformation}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Store Graphic Name:</Box>
                            <Box className="boxRegular">{scaleSettings.storeGraphicName == null ? "No Report" : scaleSettings.storeGraphicName}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Scale Mode:</Box>
                            <Box className="boxRegular">{scaleSettings.scaleMode == null ? "No Report" : scaleSettings.scaleMode}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Scale Entry Mode:</Box>
                            <Box className="boxRegular">{scaleSettings.scaleItemEntryMode == null ? "No Report" : scaleSettings.scaleItemEntryMode}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Load Cell:</Box>
                            <Box className="boxRegular">{scaleSettings.hasLoadCell == null ? "No Report" : scaleSettings.hasLoadCell ? "Yes" : "No"}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Last Weigher Calibration:</Box>
                            <Box className="boxRegular">{scaleSettings.weigherCalibratedTimestampUtc == null ? "No Report" : format(Date.parse(scaleSettings.weigherCalibratedTimestampUtc), "PPpp")}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Last Weigher Configuration:</Box>
                            <Box className="boxRegular">{scaleSettings.weigherConfiguredTimestampUtc == null ? "No Report" : format(Date.parse(scaleSettings.weigherConfiguredTimestampUtc), "PPpp")}</Box>
                        </div>

                        <div className={classes.accordionRoot}>
                            <Box className="boxBold">Time Keeper:</Box>
                            <Box className="boxRegular">{scaleSettings.timeKeeper == null ? "No Report" : scaleSettings.timeKeeper}</Box>
                        </div>

                    </div>
                </div>
            </AccessControl>
        </>
    );

}