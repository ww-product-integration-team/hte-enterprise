import { useState} from 'react';
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import {Block} from '@mui/icons-material';
import '../forms/formStyles.css';
import { GetMessageView } from '../assetTreeView/MessageIndicator';
import Box from '@material-ui/core/Box';
// BANNER, Region, Store, Department, Scale

// POST Request
export function NotificationDetails(props) {
    const { title, data, openPopup, setOpenPopup } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    
    const handleCloseDialog = (event) => {
        setShowAlert([false, "Something went wrong!"])
        event.stopPropagation()
        setOpenPopup(false);
    }

    let rspData = data.response.response
    if (rspData.error) {
        delete rspData.error
    }

    const handleObject = (obj) => {
        return (
            <>
                {Object.keys(obj).map(key => {
                    return <div key={key+"div"} style={{width: "100%",
                            display: "flex",
                            flexwrap: "wrap",
                            flexDirection: "row"}}>
                        <Box key={key+"box1"} className="boxBold">{key}:</Box>
                        <Box key={key+"box2"} className="boxRegular">{obj[key] == null ? "unknown" : typeof obj[key] === 'boolean' ? String(obj[key]) : obj[key]}</Box>
                    </div>
                })}
            </>
        )
    }
    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title !== "Error" ? title : "Notification"}
                </Typography>
            </DialogTitle>

            <div style={{margin: 15}}>
                {GetMessageView(data.notification.status, data.notification.message)}
            </div>

            <div style={{marginLeft:15}}>
                {rspData == null ? null : Object.keys(rspData).map((key) => {
                    switch(key){
                        case "operation":
                            return(
                                <div key={key + "div"} style={{width: "100%",
                                        display: "flex",
                                        flexWrap: "wrap",
                                        flexDirection: "row"}}>
                                    <Box key={key + "box1"} className="boxBold">Operation:</Box>
                                    <Box key={key + "box2"} className="boxRegular">{rspData[key] == null ? "unknown" : rspData[key]}</Box>
                                </div>
                            )
                        case "result":
                            return(
                                <div key={key + "div"} style={{width: "100%",
                                        display: "flex",
                                        flexWrap: "wrap",
                                        flexDirection: "row"}}>
                                    <Box key={key + "box1"} className="boxBold">Result:</Box>
                                    <Box key={key + "box2"} className="boxRegular">{rspData[key] == null ? "unknown" : rspData[key]}</Box>
                                </div>
                            )
                        case "errorDescription":
                            return(
                                <div key={key + "div"} style={{width: "100%",
                                        display: "flex",
                                        flexWrap: "wrap",
                                        flexDirection: "row"}}>
                                    <Box key={key + "box1"} className="boxBold">Details:</Box>
                                    <Box key={key + "box2"} className="boxRegular">{rspData[key] == null ? "unknown" : rspData[key]}</Box>
                                </div>
                            )
                        case "timestamp":
                            return(
                                <div key={key + "div"} style={{width: "100%",
                                        display: "flex",
                                        flexWrap: "wrap",
                                        flexDirection: "row"}}>
                                    <Box key={key + "box1"} className="boxBold">Timestamp:</Box>
                                    <Box key={key + "box2"} className="boxRegular">{rspData[key] == null ? "unknown" : rspData[key]}</Box>
                                </div>
                            )
                        default:
                            return(
                                <div key={key + "div"} style={{width: "100%",
                                        display: "flex",
                                        flexWrap: "wrap",
                                        flexDirection: "row"}}>
                                    <Box key={key + "box1"} className="boxBold">{key}:</Box>
                                    <Box key={key + "box2"} className="boxRegular">{rspData[key] == null ? "unknown" : typeof rspData[key] !== 'object' ? rspData[key] : handleObject(rspData[key])}</Box>
                                </div>
                            )
                    }
                })}
            </div>

            <DialogContent>
                <div className="formButtons">
                    <button className="formButton1" onClick={handleCloseDialog} type="button">
                        <Block />
                        <span> Close </span>
                    </button>
                </div>
            </DialogContent>
        </Dialog>
    )
}