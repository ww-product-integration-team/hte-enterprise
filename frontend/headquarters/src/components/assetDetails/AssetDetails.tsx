import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import {Breadcrumb } from '@themesberg/react-bootstrap';
import { useLocation } from 'react-router-dom'
import {assetListStyle} from '../styles/NavStyles';
import './AssetDetailsStyle.css';
import NotFound from '../common/NotFound'
import { store, useAppSelector } from '../../state';
import { setExpandedTreeItems } from '../../state/actions/assetActions';

import ScaleDetails from './ScaleDetails';
import StoreDetails from './StoreDetails';
import { Dispatch } from 'redux';
import { Store } from '../../types/asset/AssetTypes';

//Generates a list of items to expand to focus onto
function getExpandedArray(dispatch : Dispatch<any>, storeID : string, deptID ?: string){
    let expArray :string[] = []
    if(deptID){
        expArray.push(storeID + "|" + deptID)
    }
    let assetState = store.getState().asset
    let regionId = ""
    Object.values(assetState.stores).every((item : Store) => {
        if(item.storeId === storeID){
            regionId = item.regionId
            expArray.push(item.regionId + '|' + storeID)
            return false
        }
        return true
    })

    assetState.regions.every((item) => {
        if(item.regionId === regionId){
            expArray.push(item.bannerId + '|' + regionId)
            expArray.push(item.bannerId)
            return false
        }
        return true
    })

    //Now we have the expanded array that contains this item in the tree
    //We need to concatenate with the existing exp list
    expArray = expArray.concat(assetState.expanded)
    //And delete duplicates
    let tmp = expArray.filter((item, pos) => expArray.indexOf(item) === pos)
    dispatch(setExpandedTreeItems(tmp))
}

export default function AssetDetails() {
    // Redux
    const classes = assetListStyle();
    const props : any = useLocation().state

    const roles = useAppSelector((state) => state.access.roles);
    const user = useAppSelector((state) => state.access.account);
    
    if(props === undefined){
        return(<NotFound/>)
    }

    return (
        <>
        <div className="mb-4 mb-md-0">
            <Breadcrumb className="d-none d-md-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Asset Details</Breadcrumb.Item>
            </Breadcrumb>

            <h2 style={{marginBottom: 10}}>Asset Details</h2>

            <p className="mb-4">Here you can find more specific details relating to this asset as well the asset's current status</p>
        </div>

        
        {props.scale?
            <ScaleDetails
                scale = {props.scale}
                classes = {classes}
                user = {user}
                roles = {roles}
                getExpandedArray = {getExpandedArray}
            />
            :null
        }

        {props.store?
            <StoreDetails
                storeId = {props.store.storeId}
                classes = {classes}
                user = {user}
                roles = {roles}
                getExpandedArray = {getExpandedArray}
            />
            :null
        }

        

        </>   
    );
}