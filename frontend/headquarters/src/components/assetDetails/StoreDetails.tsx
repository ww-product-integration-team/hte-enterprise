import { useState, useEffect} from 'react';
import { Link} from 'react-router-dom'
import { Button, ButtonGroup} from '@themesberg/react-bootstrap';
import { AccountTree} from '@material-ui/icons';
import { useSelector, useDispatch } from "react-redux"
import ScaleIcon from '../icons/ScaleIcon';
import { AssetAPI, ScaleAPI, StoreAPI} from '../api';
import Box from '@material-ui/core/Box';
import './AssetDetailsStyle.css';
import { AssetActions, store, useAppSelector } from '../../state';
import {GetMessageView} from '../assetTreeView/MessageIndicator'
import MonitorHeartIcon from '@mui/icons-material/MonitorHeart';
import { ScaleSyncGraph } from '../dashboard/widgets/ScalesValuesChart';
import SetEnable from './detailComponents/SetEnable';
import { Alert } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import ConnectedTvIcon from '@mui/icons-material/ConnectedTv';
import TuneIcon from '@mui/icons-material/Tune';
import StoreIcon from '@material-ui/icons/Store';
import StoreActionsComponent from '../assetTreeView/StoreActionsComponent';
import VirtualTable from '../common/virtualTable/VirtualTable';
import EditIcon from '@mui/icons-material/Edit';
import { PutStore } from '../forms/StoreForm';
import { Help } from '@mui/icons-material';
import { IconButton, Tooltip, Typography } from '@material-ui/core';
import { useFileDownloader } from '../scaleProfiles/fileDownload/FileDownloader';
import { QuerablePromise } from './ScaleDetails';
import { HTeRole, HTeUser } from '../../types/access/AccessTypes';
import { AssetTypes } from '../../types/asset/AssetTypes';
import SetStoreConfig from './detailComponents/SetStoreConfig';
import { Dispatch } from 'redux';
import { format } from 'date-fns';
import AccessControl from '../common/AccessPermissions';
import {MANAGE_STORES, MONITOR} from '../../state/actions/accessActions';
import { ScaleDevice } from '../../types/asset/ScaleTypes';


const scaleHeadCells = [
    { id: 'ipAddress',        sorted: true, searchable: true,  label: 'IP Address',          isShowing: true, mandatory: true},
    { id: 'assignedDept',     sorted: true, searchable: true,   label: 'Assigned Department', isShowing: true, mandatory: false},
    { id: 'hostname',         sorted: true, searchable: true,   label: 'Host Name',           isShowing: true, mandatory: false },
    { id: 'application',      sorted: true, searchable: true,   label: 'Application Version', isShowing: true, mandatory: false },
    { id: 'totalLabelsPrinted',    sorted: true, searchable: false,  label: 'Labels Printed',      isShowing: true, mandatory: false },
    { id: 'pluCount',         sorted: true, searchable: false,  label: 'PLU Count',           isShowing: true, mandatory: false },
    { id: 'lastReportTimestampUtc', sorted: true, searchable: false, label: 'Last Report', isShowing: true, mandatory: false },
    { id: 'options',          sorted: false, searchable: false, label: 'Options',             isShowing: true, mandatory: true},
    // { id: "id", narrow: false, disablePadding: false, label: 'ID', mandatory: false },   // can use this to filter for Region / Banner this store belongs to
];

const serviceHeadCells = [
    { id: 'serviceName',        sorted: false, searchable: false,  label: 'Service Name',          isShowing: true, mandatory: true},
    { id: 'databaseId',         sorted: false, searchable: false,  label: 'Database ID',          isShowing: false, mandatory: false},
    { id: 'operationMode',      sorted: false, searchable: false,  label: 'Operation Mode', isShowing: true, mandatory: true},
    { id: 'url',                sorted: false, searchable: false,  label: 'URL',             isShowing: true, mandatory: true},
    { id: 'version',            sorted: false, searchable: false,   label: 'Version',           isShowing: true, mandatory: true },
    { id: 'buildDate',          sorted: false, searchable: false,   label: 'Build Date', isShowing: true, mandatory: true  },
    // { id: "id", narrow: false, disablePadding: false, label: 'ID', mandatory: false },   // can use this to filter for Region / Banner this store belongs to
];

/**
 * This function allow you to modify a JS Promise by adding some status properties.
 * Based on: http://stackoverflow.com/questions/21485545/is-there-a-way-to-tell-if-an-es6-promise-is-fulfilled-rejected-resolved
 * But modified according to the specs of promises : https://promisesaplus.com/
 */
 function MakeQuerablePromise(promise : any){
    // Don't modify any promise that has been already modified.
    if (promise.isFullfilled) return promise;

    // Set initial state
    let isPending = true;
    let isRejected = false;
    let isFulfilled = false;

    // Observe the promise, saving the fulfillment in a closure scope.
    let result : QuerablePromise<any> = promise.then(
        function(v : any) {
            isFulfilled = true;
            isPending = false;
            return v; 
        }, 
        function(e : any) {
            isRejected = true;
            isPending = false;
            throw e; 
        }
    ) as QuerablePromise<any>

    result.isFullfilled = isFulfilled
    result.isPending = isPending
    result.isRejected = isRejected
    return result;
}

interface StoreDetailProps{
    storeId: string
    classes : any
    user : HTeUser
    roles : {[key: string] : HTeRole}
    getExpandedArray : (d : Dispatch<any>, storeId: string, deptId?: string) => void
}
export default function StoreDetails(props : StoreDetailProps){
    const {storeId, classes, user, roles, getExpandedArray} = props;
    const dispatch = useDispatch();
    
    const stores = useAppSelector((state) => state.asset.stores);
    const depts = useAppSelector((state) => state.asset.depts);
    const nodeStatus = useAppSelector((state) => state.asset.nodeData)
    
    const domains = useAppSelector((state) => state.access.domains)

    const [downloadFile, downloaderComponentUI] = useFileDownloader();
    const download = (file : any) => {
        if(downloadFile instanceof Function){
            downloadFile(file);
        }
    };
    const [scales, setScales] = useState<ScaleDevice[]>([])
    useEffect(() => {
        dispatch(ScaleAPI.fetchScales(Date.now(), undefined, null, null, setScales))
    }, [])

    let storeInfo = {...stores[storeId]}

    let storeRegion = store.getState().asset.regions.find((region) => region.regionId === storeInfo.regionId)
    if(storeRegion){
        storeInfo.assignedRegion = storeRegion.regionName
    }

    let storeBanner = store.getState().asset.banners.find((banner) => banner.bannerId === storeRegion?.bannerId)
    if(storeBanner){
        storeInfo.assignedBanner = storeBanner.bannerName
    }

    let nodeInfo = nodeStatus[storeId]

    const [refresher, setRefresher] = useState(false);
    const [openDownload, setOpenDownload] = useState(false);        // DELETE Request
    const [openEdit, setOpenEdit] = useState<AssetTypes | null>(null);            // PUT    Request
    const [userPermitted, setUserPermitted] = useState(false);

    useEffect(() => {

        if(props != undefined && storeInfo){
            dispatch(AssetAPI.getNodeStatus(Date.now()+1, storeInfo.storeId, "STORE", storeInfo.regionId))
        }
        setRefresher(true)
    }, []);

    useEffect(() => {
        if(false){
            setRefresher(false)
            const refreshPromise = new Promise(function(resolve, reject) {setTimeout(() => resolve("done"), 10000);})
            .then(function(result){
                if(props != undefined && storeInfo){
                    const id = Date.now()
                    dispatch(AssetAPI.getNodeStatus(id+1, storeInfo.storeId, "STORE", storeInfo.regionId))
                    dispatch(StoreAPI.fetchStores(id+2, storeInfo.storeId))
                }
            })
            
            let refreshStatus = MakeQuerablePromise(refreshPromise);
            refreshStatus.then(function(data : any){
                if(refreshStatus.isFulfilled() || refreshStatus.isRejected()){
                    setRefresher(true)
                }
            })
        }
    }, [refresher])

    let storeScales = Object.values(scales).filter((scale) => scale.storeId === storeId)
    if(!storeInfo){
        return(
            <Alert className="m-1" variant="filled" severity="error">Could not find information for Store <b>{storeId}</b>!</Alert>
        )
    }
    function createSyncMessage(){
        return
        if(!nodeInfo){
            return(
                GetMessageView('error',"Could not fetch sync status for the scale")
            )
        }
        
        switch(nodeInfo.sync){
            case "NA":
                return (GetMessageView('disabled',nodeInfo.syncMessage))
            case "NOT_SYNCED":
                return(GetMessageView('error',nodeInfo.syncMessage))
            case "PENDING":
                return(GetMessageView('warning',nodeInfo.syncMessage))
            case "SYNCED":
                return(GetMessageView('synced',nodeInfo.syncMessage))
            default:
                return(GetMessageView('error',"No Sync info!"))
        }
    }

    function showCurrentConfiguration(){
        // if(!scaleSettings || !scaleSettings.currentFiles){
        //     return null
        // }

        // let files = scaleSettings.currentFiles.split(",")
        // let totalPre = ""
        // let fileStr = ""
        // let prefix = ""
        // files.forEach((item) =>{
        //     let fileAndChecksum = item.split("=")
            
        //     if(fileAndChecksum.length === 2){
        //         totalPre = "Scale's downloaded configuration:"
        //         fileStr += prefix + fileAndChecksum[0]
        //         prefix = ", "
        //     }
        // })
        
        // return <span><b>{totalPre}</b> {fileStr}</span>
        
    }

    return(
        <>
        <AccessControl
            user={user}
            requiredDomain={domains[storeId]}
            requiredRole={roles[MONITOR]}
            pageRequest = {true}
        >
            <div className="mb-4 mb-md-0">

                <div className="d-flex" style={{alignItems:"bottom"}}>
                    <StoreIcon fontSize='large'/>
                    <h5 style={{margin :"1rem"}}>Store: <b style={{color : "#ba4735"}}>{storeInfo.storeName && storeInfo.storeName !== "" ? storeInfo.storeName : "None"}</b></h5>
                    <h5 style={{margin :"1rem"}}>Store Number: <b style={{color : "#ba4735"}}>{storeInfo.customerStoreNumber && storeInfo.customerStoreNumber !== "" ? storeInfo.customerStoreNumber : "None"}</b></h5>
                </div>

                <div className="d-flex justify-content-between">

                    {/* Scale Profile Actions */}
                    <StoreActionsComponent
                        storeInfo={storeInfo}
                        storeScales={storeScales}
                        user = {user}
                        roles = {roles}
                        download = {download}
                    />

                    {/* View the asset in the tree if it is assigned*/}
                    <ButtonGroup className="assetBar">
                        <Link to={{ pathname: '/AssetList'}}>
                            <Button id="actionBar1" size="sm" onClick={() => {
                                dispatch(AssetActions.setFocusedAsset(storeInfo.regionId + "|" + storeInfo.storeId))
                                getExpandedArray(dispatch, storeInfo.storeId)
                            }}> 
                                <AccountTree />
                                View In Tree
                            </Button>
                        </Link>
                    </ButtonGroup>                
                </div>
            </div>

            {nodeInfo ?
                GetMessageView(nodeInfo.status, nodeInfo.message) :
                GetMessageView('warning', "Could not fetch store status")
            }

            <div style={{marginTop:20}}/>
        
            <ScaleSyncGraph
                title={<h5><ConnectedTvIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Store Scale Heartbeats</b></h5>}
                graphId={storeInfo.storeId}
                deviceList={storeScales}
            />

            <h5 style={{marginTop:20}}><MonitorHeartIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Server Status</b></h5>
            {storeInfo.ipAddress ? 
                <>
                    {createSyncMessage()}
                    {showCurrentConfiguration()}

                    <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", width: '100%',}}>
                        <div className={classes.accordionRooCol}>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">IP Address:</Box>
                                <Box className="boxRegular">{storeInfo.ipAddress == null ? "No Report" : storeInfo.ipAddress}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Hostname:</Box>
                                <Box className="boxRegular">{storeInfo.hostname == null ? "No Report" : storeInfo.hostname}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>URL: </Box>
                                <Box className="boxRegular">{storeInfo.url == null ? "No Report" : storeInfo.url}</Box>
                            </div>
                    
                        </div>
                        <div className={classes.accordionRootCol}>
                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Last Trigger Type:</Box>
                                <Box className="boxRegular">{storeInfo.triggerType == null ? "No Report" : storeInfo.triggerType}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Last Item Change:</Box>
                                <Box className="boxRegular">{storeInfo.lastItemChangeReceivedTimestampUtc == null ? "No Report" : storeInfo.lastItemChangeReceivedTimestampUtc === "" ? "None" : format(Date.parse(storeInfo.lastCommunicationTimestampUtc ?? ""), "PPpp")}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>Last Report: </Box>
                                <Box className="boxRegular">{storeInfo.lastCommunicationTimestampUtc == null ? "No Report" : format(Date.parse(storeInfo.lastCommunicationTimestampUtc), "PPpp")}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Last Reboot:</Box>
                                <Box className="boxRegular">{storeInfo.lastRebootTimestampUtc == null ? "No Report" : format(Date.parse(storeInfo.lastRebootTimestampUtc), "PPpp")}</Box>
                            </div>
                        </div>
                    </div>

                    <h5 style={{marginTop:20}}><TuneIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Actions</b></h5>
                    <div style={{display: "flex",flexWrap: "wrap",}}>
                        <SetEnable
                            user={user}
                            info={storeInfo}
                            operation="STORE"
                        />
                        
                    </div>
                </>
        
            : 
            <div style={{display:"flex"}}>
                <AccessControl
                        user={user}
                        requiredDomain={domains[storeId]}
                        requiredRole={roles[MANAGE_STORES]}
                        boolController= {setUserPermitted}
                >
                    <Button id="actionBar1" disabled = {!userPermitted} size="sm" style={{padding:"0.5rem", backgroundColor:"#203140"}}
                        onClick={() => {
                        setOpenEdit(AssetTypes.Store)
                        }}> 
                        <EditIcon style={{marginRight:"0.5rem"}}/>
                        Setup Store Server
                        
                            <PutStore
                                title="Setup Store"
                                openPopup={openEdit == AssetTypes.Store}
                                setOpenPopup={setOpenEdit}
                                treeElement={storeInfo}
                            />
                    </Button>

                    <Tooltip className="info" title={<Typography> Once a store server is setup, please enter in it's IP address here, this tells the program that the store server IP is valid</Typography>}>
                        <IconButton aria-label="enableAccount" disableRipple>
                            <Help />
                        </IconButton>
                    </Tooltip>
                </AccessControl>
            </div>

            }
            
            
            <h5 style={{marginTop:20}}><InfoIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Store Details</b></h5>
            <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", width: '100%',}}>
                <div className={classes.accordionRooCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Region:</Box>
                        <Box className="boxRegular">{storeInfo.assignedRegion == null ? "No Report" : storeInfo.assignedRegion}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Banner:</Box>
                        <Box className="boxRegular">{storeInfo.assignedBanner === null ? "No Report" : storeInfo.assignedBanner}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>Number of Scales: </Box>
                        <Box className="boxRegular">{storeScales.length === null ? "No Report" : storeScales.length}</Box>
                    </div>
            
                </div>
                <div className={classes.accordionRootCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Address:</Box>
                        <Box className="boxRegular">{storeInfo.address === null ? "No Report" : storeInfo.address === "" ? "None" : storeInfo.address}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Store Number:</Box>
                        <Box className="boxRegular">{storeInfo.customerStoreNumber === null ? "No Report" : storeInfo.customerStoreNumber === "" ? "None" : storeInfo.customerStoreNumber}</Box>
                    </div>

                </div>
            </div>


            <SetStoreConfig
                user={user}
                info={storeInfo}
            />
                

            {storeInfo.services && storeInfo.services.length > 0 ?
            <>
            <h5 style={{marginTop:20}}><b style={{color : "#ba4735"}}>  Service Info</b></h5>
            <VirtualTable
                tableName = "StoreServicesTable"
                useToolbar = {false}
                dataSet={storeInfo.services}
                headCells={serviceHeadCells}
                initialSortedBy={{name: '' }}
                dispatchedUrls = {[]}
                saveKey="StoreServicesTableHead"
            />
            </>
            : null}

            <h5 style={{marginTop:20}}><ScaleIcon style={{color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  Scales</b></h5>
            <VirtualTable
                tableName = "virtualStoreScaleTable"
                dataSet={storeScales}
                headCells={scaleHeadCells}
                initialSortedBy={{name: '' }}
                dispatchedUrls = {[ScaleAPI.fetchScales]}
                saveKey="storeScaleTableHead"
            />

            {downloaderComponentUI}
            </AccessControl>
        </>
    );

}