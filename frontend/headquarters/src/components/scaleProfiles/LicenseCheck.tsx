import { ReactNode, useEffect, useState } from "react"
import { Delete, Download, Edit, Refresh, Upload, FindInPage, Terminal } from "@mui/icons-material"
import { FileForEvent, FileType, getDefaultFile } from "../../types/asset/FileTypes"
import { Profile } from "../../types/asset/ProfileTypes"
import { DeleteFiles, EditFiles, UploadFiles, ViewFiles } from "./FileForm";
import { Box, ButtonGroup, Dialog, DialogContent, DialogTitle, IconButton, ThemeProvider, Tooltip } from "@material-ui/core"
import { Accordion, Card, useAccordionButton } from "react-bootstrap";

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import {Typography } from "@mui/material";
import { Button, Col, FormCheck, Row } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux";
import { FileAPI } from "../api";

import { CaretDownFilled } from '@ant-design/icons';

// Styling/icons
import { useFileDownloader } from "./fileDownload/FileDownloader";
import Checkbox from '@mui/material/Checkbox';
import { assetListStyle } from "../styles/NavStyles";
import InfoIcon from '@mui/icons-material/Info';
import { secondsInDay } from "date-fns";
import { transform } from "typescript";

interface LicenseCheckProps{
    title?: string,
    titleIcon?: JSX.Element,
    fileTypes: FileType[],
    files: FileForEvent[],
    currentProfile : Profile
    children?: ReactNode
}

interface LicenseContent {
    responsible: string,
    companyName: string,
    companyAddress: string,
    productNames: string, 
    keyType: string, 
    licenseActivationDate: string,
    licenseAllotment: string,
    licenseTimeout: string
    key: string
}

/*
Sorted into arrays so it cant be mapped through and organized 
*/
export const licenses = 
[["SCALE_AD_SERVICE","SCALE_CAT3_AUDITTRAIL","SCALE_CODE_CHECKER","SCALE_DIGIMARC","SCALE_FPP","SCALE_FULLSCREEN_FLASHKEYS",],
["SCALE_HANDSFREEDEVICE","SCALE_INTERNATIONAL_ROUNDING","SCALE_MEAL_BUILDER","SCALE_NOW_SERVING","SCALE_OFFICE_VIEWER","SCALE_PDF_VIEWER",],
["SCALE_PICK5","SCALE_PRINTER_EPP","SCALE_REMOTE_PRINTING","SCALE_REMOTE_MONITOR","SCALE_TRANS_GCD","SCALE_WEB_BROWSER",],
["SCALE_ENHANCED_CENTERSCREEN","SCALE_ONLINE_ORDERING","SCALE_SELFSERVE_AUTO_REZERO"]]

export const LicenseCheck : React.FC<LicenseCheckProps> = (props : LicenseCheckProps) => {
    const {title, titleIcon, fileTypes, files, currentProfile} = props;
    const dispatch = useDispatch();
    
    const [currentFile, setCurrentFile] = useState<FileForEvent | null>(null);
    const [openPopup, setOpenPopup] = useState<"UPLOAD" | "EDIT" | "DELETE" | "DOWNLOAD" | "VIEW" | null>(null);
    const [isLoading, setIsLoading] = useState(true);
    const [openContent, setOpenContent] = useState(false)
    const [contentData, setContentData] = useState("")
    const [licenseContent, setLicenseContent] = useState<LicenseContent | undefined>(undefined)
    useEffect(()=>{
        //parse files for license 
        if(files.length  < 1){ // may only use one? may use multiple
            setLicenseContent(undefined)
            return;
        }
        let buffer = files[0].content.split("\n");
        // TODO validate license 

        let content : LicenseContent  = {
            responsible: buffer[0].split(": ")[1],
            companyName: buffer[1].split(": ")[1],
            companyAddress: buffer[2].split(": ")[1],
            productNames:buffer[3].split(": ")[1],
            keyType: buffer[4].split(": ")[1],
            licenseActivationDate: buffer[5].split(": ")[1],
            licenseAllotment: buffer[6].split(": ")[1],
            licenseTimeout: buffer[7].split(": ")[1],
            key: buffer[8].split(": ")[1],
        }
        setCurrentFile(files[0])
        setLicenseContent(content)
        //setLicense Content
    },[files])
    const [downloadFile, downloaderComponentUI] = useFileDownloader();
    const download = (file : FileForEvent) => {
        if(downloadFile instanceof Function){
            downloadFile(file);
        }
    };

    return(
        <div className="form-control mb-2"> 
            <>
                <div className="d-flex justify-content-between">
                    <div className="d-flex justify-content-between">
                    <Typography style={{fontFamily:'"Quicksand", "Arial", "sans-serif"',
                                    fontWeight:600, fontSize:'1.5rem', paddingTop:"0.5rem"}} component="span" className="mb-0">
                        {titleIcon ? titleIcon : null}
                        {title ? title : "Files"}
                    </Typography>
                    {licenseContent && files[0]  ?
                        <>
                            <Tooltip title={<Typography>Download</Typography>}>
                                <span>
                                    <IconButton aria-label="download"
                                    disabled={false}
                                    onClick={() => {download(files[0])}}
                                    >
                                    <Download />
                                    </IconButton>
                                </span>
                            </Tooltip>
                            
                            <Tooltip title={<Typography>Delete</Typography>}>
                                <span>
                                    <IconButton aria-label="delete" 
                                    disabled={false}
                                    onClick={() => {
                                        setCurrentFile(files[1])
                                        setOpenPopup("DELETE")
                                    }}>
                                    <Delete />
                                    </IconButton>
                                </span>
                            </Tooltip>
                        </>
                        : null}
                    </div>
                    <div className="d-flex justify-content-between">
                        
                        <div className="assetBar">
                        <Button id="actionBar1" variant="outline-primary" size="sm" onClick={(e) => {
                            setOpenPopup("UPLOAD");               
                        }}> 
                            <Upload style={{marginRight: '8px'}}/>
                            Upload
                        </Button>

                        <Button id="actionBar2" variant="outline-primary" size="sm" style={{marginLeft: "0.5rem"}} onClick={() => {
                            setIsLoading(true);
                            dispatch(FileAPI.fetchFiles())
                            // fetchImages(files)
                            new Promise(function(resolve, reject) {
                                setTimeout(() => resolve("done"), 500);
                            })
                            .then(function(result){
                                setIsLoading(false);
                            })

                        }}>
                            <Refresh />
                            Refresh
                        </Button>
                    </div>
                </div>
            </div>
            <br/>
            {licenseContent && files[0]?
            <LicenseBody
                licenseContent = {licenseContent}
                licenseFile={files[0]}
            >
                <Tooltip style = {{paddingTop:".6rem"}} title={<Typography>Edit</Typography>}>
                    <span>
                        <IconButton aria-label="edit"
                            disabled={false}
                            onClick={() => {
                                setCurrentFile(files[1])
                                setOpenPopup("EDIT")
                            }
                            }
                        >
                            <Edit />
                        </IconButton>
                    </span>
                </Tooltip>
            </LicenseBody>
            : 
            <label className="noDataLabel">No Data</label>}
            {openPopup === "UPLOAD" &&
                <UploadFiles
                    title="Upload a File"
                    categories={fileTypes}
                    openPopup={openPopup === "UPLOAD"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "UPLOAD" : null)}
                    profiles={currentProfile ? [currentProfile] : []}
                    selectValue = {currentProfile ? currentProfile.name : ""}
                    onlyValue= {true}
                />
            }

            {downloaderComponentUI}

            {/*
            {openPopup === "VIEW" &&
                <ViewFiles
                    categories={fileTypes}
                    title="View File"
                    openPopup={openPopup === "VIEW"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "VIEW" : null)}
                    currentFile={currentFile ?? getDefaultFile()}
            />
            }
            */}

            {openPopup === "EDIT" && 
                <EditFiles
                    categories={fileTypes}
                    title="Edit File"
                    openPopup={openPopup === "EDIT"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "EDIT" : null)}
                    currentFile={files[0]}
                />
            }

            {openPopup === "DELETE" &&
                <DeleteFiles 
                    title="Delete File"
                    openPopup={openPopup === "DELETE"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "DELETE" : null)}
                    file={files[0]}
                />
            }
            </>

            <Dialog open={openContent} onClose={()=>setOpenContent(false)} maxWidth="md" fullWidth={true}>
                <DialogTitle>
                    <Typography variant="h5" component="div">File Content</Typography>
                </DialogTitle>

                <DialogContent>
                    <p>{contentData != null ?
                        contentData
                    :
                        "Content is empty or is not a parsable type"
                    }</p>

                    <ButtonGroup className="formButtons">
                        <Button className="formButton1" variant="primary" size="sm" onClick={()=>setOpenContent(false)}>Close</Button>
                    </ButtonGroup>
                </DialogContent>
            </Dialog>
        </div>
    )
}


interface CustomToggleProps{
    eventKey: string,
    currentKey: string
    setCurrentKey : React.Dispatch<React.SetStateAction<string>>
    children?: ReactNode
}
const CustomToggle : React.FC<CustomToggleProps> = (props : CustomToggleProps)=>{
    const {eventKey, children,currentKey, setCurrentKey} = props
    const decoratedOpen = useAccordionButton(eventKey, ()=>{setCurrentKey(eventKey)})
    const decoratedClose = useAccordionButton(eventKey, ()=>{setCurrentKey("")})
    if(currentKey == eventKey){
    return (
        <div className="d-flex ">
            <ArrowDropDownIcon 
                type="button"
                onClick={decoratedClose}
            />
            {children}
        </div>
    );
    } else {
        return (
            <div className="d-flex ">
                <ArrowDropDownIcon
                    style = {{transform: 'rotate(270deg)' }}
                    type="button"
                    onClick={decoratedOpen}
                />
                {children}
            </div>
        );
    }
  }
interface LicenseBodyProps {
    licenseContent : LicenseContent | undefined
    licenseFile : FileForEvent,
    children?: ReactNode
}
export const LicenseBody : React.FC<LicenseBodyProps> = (props : LicenseBodyProps) => {
    const {licenseContent, licenseFile, children} = props;
    const [checked, setChecked] = useState<String>("")
    const classes = assetListStyle();
    const [currentKey, setCurrentKey] = useState("");
    useEffect(()=>{
        if(licenseContent){
            setChecked(licenseContent.productNames)
        }
    },[licenseContent])
    return(
        <Card>
            {licenseContent && licenseFile?
            <div style = {{marginLeft:"2rem"}} className="justify-content-left">
                <Row>
                     <Col>
                        <div className="d-flex justfy-content-left">
                            <h5 style={{marginTop:20}}><InfoIcon style={{marginBottom : ".25rem", color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  File Info:</b></h5>
                            {children} {/*Edit buton JSX*/ }
                        </div>
                    </Col>
                    <Col>
                        <h5 style={{marginTop:20}}><InfoIcon style={{marginBottom : ".25rem", color:"#ba4735"}}/><b style={{color : "#ba4735"}}>  License Info:</b></h5>
                    </Col>
                </Row>
                <Row>
                    <Col xs = {12 / licenses.length}>
                        <div className={(classes as any).accordionRootCol}>
                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">File Name:</Box>
                                <Box className="boxRegular">{licenseFile.filename}</Box>
                            </div>

                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Description:</Box>
                                <Box className="boxRegular">{licenseFile.shortDesc}</Box>
                            </div>

                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Type:</Box>
                                <Box className="boxRegular">{licenseFile.fileType}</Box>
                            </div>
                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Size:</Box>
                                <Box className="boxRegular">{licenseFile.size}</Box>
                            </div>
                        </div>
                    </Col>
                    <Col xs = {12 / licenses.length}>
                        <div className={(classes as any).accordionRootCol}>
                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Upload Date:</Box>
                                <Box className="boxRegular">{licenseFile.uploadDate}</Box>
                            </div>

                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Start Date:</Box>
                                <Box className="boxRegular">{licenseFile.startDate}</Box>
                            </div>
                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">End Date:</Box>
                                <Box className="boxRegular">{licenseFile.endDate ? licenseFile.endDate : "No end date"}</Box>
                            </div>
                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Enabled:</Box>
                                <Box className="boxRegular">{licenseFile.enabled ? "Enabled": "Disabled"}</Box>
                            </div>
                        </div>
                    </Col>
                    <Col xs = {12 / licenses.length}> 
                        <div className={(classes as any).accordionRooCol}>
                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Responsible:</Box>
                                <Box className="boxRegular">{licenseContent.responsible}</Box>
                            </div>

                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Company Name:</Box>
                                <Box className="boxRegular">{licenseContent.companyName}</Box>
                            </div>

                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">Company Address:</Box>
                                <Box className="boxRegular">{licenseContent.companyAddress}</Box>
                            </div>
                        </div>
                    </Col>
                    <Col  xs = {12 / licenses.length} >
                        <div className={(classes as any).accordionRootCol}>
                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">License Activation Date:</Box>
                                <Box className="boxRegular">{licenseContent.licenseActivationDate}</Box>
                            </div>

                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">License Allotment:</Box>
                                <Box className="boxRegular">{licenseContent.licenseAllotment}</Box>
                            </div>

                            <div className={(classes as any).accordionRoot}>
                                <Box className="boxBold">License Timeout:</Box>
                                <Box className="boxRegular">{licenseContent.licenseTimeout}</Box>
                            </div>
                        </div>
                    </Col>
                    
                </Row>
                <br/>
                <Accordion>
                    <CustomToggle eventKey={"0"} currentKey={currentKey} setCurrentKey={setCurrentKey}>
                        <h5><InfoIcon style={{marginBottom : ".25rem",color:"#ba4735"}}/><b style={{ color : "#ba4735"}}>  Product Names:</b></h5>
                    </CustomToggle>
                    <Accordion.Collapse eventKey={"0"}>
                        <Card.Body>
                        <Row>
                            {licenses.map(licenseArr => {
                                let licenseCol = [<div/>]
                                for(let license of licenseArr){
                                    licenseCol.push(
                                    <div className=" d-flex flex-row ">
                                        
                                        <div className={(classes as any).accordionRoot}>
                                            <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>
                                                <Checkbox
                                                    checked = {checked.includes(license)}
                                                    disabled
                                                />
                                            </Box>
                                            <Box style = {{paddingTop:".6rem"}}className="boxRegular">{license}</Box>
                                        </div>
                                    </div>)
                                }
                                return (
                                <Col>
                                    {licenseCol}
                                </Col>
                                )
                            })}
                        </Row>
                        </Card.Body>
                    </Accordion.Collapse>
                </Accordion>
                <br/>
            </div>
            : <label className="noDataLabel">No Data</label>}
        </Card>
    )
}