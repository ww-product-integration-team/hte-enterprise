import React, { useState, useEffect } from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import { Upload, Edit, Delete, Block, RemoveCircle } from '@mui/icons-material';
import { Form, FormCheck, ProgressBar } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import { FormSelect, FormInput } from '../forms/FormHelper';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Help } from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { FileAPI, ProfileAPI } from '../api/index'
import axios from 'axios';
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { useAppSelector } from '../../state';
import { Profile } from '../../types/asset/ProfileTypes';
import { ResponseType } from '../../types/storeTypes';
import { formatBytes } from '../utils/utils';
import { AlertMessage, HideMessage, ShowErrorMessage, ShowWarningMessage } from '../../types/utils/utilTypes';
import { FileForEvent, FileType, getValidFileExensions, PrintFileType } from '../../types/asset/FileTypes';
import DoubleSliderTemplate from '../common/DoubleSlider';
import ReactSlider from 'react-slider'
import format from 'date-fns/format'

// =================== CATEGORY DATA ===================
function createData(name: string, fileId: any) {
    return { name, fileId };
}

// =================== UPLOAD FILES  ===================
interface UploadFilesProps {
    selectValue: string;
    title: string
    openPopup: boolean
    categories: FileType[]
    setOpenPopup: (value: boolean) => void
    profiles: Profile[],
    onlyValue?: boolean
}

const UploadFiles = (props: UploadFilesProps) => {
    const { title, openPopup, setOpenPopup, profiles, onlyValue } = props;
    interface UploadForm {
        profileID: string,
        fileName: FileList | null,
        fileDescription: string,
        fileType: FileType,
        imageType: FileType,
        overwriteExistingFile: boolean,
        enableSync: boolean,
        startRebootWindow : string,
        endRebootWindow : string
    }
    const { register, handleSubmit, reset, formState: { errors } } = useForm<UploadForm>();

    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();
    const profile = useAppSelector((state) => state.profile);

    //Create Categories from provided Categories
    const categories: { name: string, fileId: string }[] = []
    const imageCategories: { name: string, fileId: string }[] = []
    props.categories.forEach((category) => {
        if (category.startsWith("IMAGE_")) {
            if (!categories.find((cat) => cat.fileId === "IMAGE")) {
                categories.push(createData("Image", "IMAGE"))
            }
            else {
                imageCategories.push(createData(PrintFileType(category), category))
            }
        }
        else {
            categories.push(createData(PrintFileType(category), category))
        }
    })

    let UploadMode: "FEATURE" | "CONFIG" | "OTHER" = "OTHER"
    if (props.categories.length === 1 && props.categories[0] === FileType.FEATURE) {
        UploadMode = "FEATURE"
    }
    else if (props.categories.length === 1 && props.categories[0] === FileType.CONFIG) {
        UploadMode = "CONFIG"
    }

    // <Alert> Notifications for errors / warnings
    const [showAlert, setShowAlert] = useState<AlertMessage>(HideMessage())    // Upload Error
    const [replaceFileAlert, setReplaceFileAlert] = useState<AlertMessage>(HideMessage())    // If duplicate file in same scale profile
    const [syncAlert, setSyncAlert] = useState<AlertMessage>(HideMessage())    // Incorrect Sync date range
    const [invalidFileAlert, setInvalidFileAlert] = useState<AlertMessage>(HideMessage())    // Uploading a file without .tgz extension
    const [profilesAlert, setProfilesAlert] = useState<AlertMessage>(HideMessage())    // File with same name in different scale profile (FEATURE FILES ONLY)
    const [loadImageTypes, setLoadImageTypes] = useState<boolean>(false)    // Load image subtypes
    const [responseError, setResponseError] = useState<Boolean>(false)    // Response error from server

    const [awayThreshold, setAwayThreshold] = useState<number | null | undefined>(0);
    const [rebootWindow, setRebootWindow] = useState<number[] | null | undefined>([5,20]);
    const [offlineMin, setOfflineMin] = useState<number>(0);
    const [statusAlert, setStatusAlert] = useState<AlertMessage>(HideMessage())

    // Syncing variables
    const [enableSync, setEnableSync] = useState(true)          // Default: true
    const [startDate, setStartDate] = useState<Date | null>(new Date());     // Start date for syncing
    const [endDate, setEndDate] = useState<Date | null>(null);               // End date for syncing
    const currentDate = new Date();                             // Used for validating endDate

    // Axios Cancel / Upload progress variables
    const [controller] = useState(axios.CancelToken.source())    // Contains amount uploaded + total
    const [disable, setDisable] = useState(false)                               // Disable upload button when clicked
    const [uploadPercent, setUploadPercent] = useState(0)                       // Percentage on upload progress bar
    const [uploadProgress, setUploadProgress] = useState<{ loaded: number, total: number }>({ loaded: 0, total: 0 })                      // Contains amount uploaded + total

    // ==================   MISCELLANEOUS HELPER FUNCTIONS   ==================
    // Default values + Function to abort Axios calls + Function to close dialog box

    const handleColor = () => {
        return "text-success"
    };

    const handleAbort = () => {
        controller.cancel("Operation cancelled by the user.")
        logger.info("controller aborted: ", controller)
    };

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        profileID: profiles[0].profileId,
        fileName: null,
        fileDescription: "",
        fileType: FileType.UNKNOWN,
        overwriteExistingFile: false,
        enableSync: true,
    };

    const handleCloseDialog = (event: any) => {
        setShowAlert(HideMessage())
        setReplaceFileAlert(HideMessage())
        setSyncAlert(HideMessage())
        setInvalidFileAlert(HideMessage())
        setOpenPopup(false);

        setEnableSync(true);
        setStartDate(currentDate);
        setEndDate(null);

        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
        reset(defaultValues);
    }

    const handleOnCategoryChange = (event: React.FormEvent<HTMLSelectElement>) => {
        setLoadImageTypes(event.currentTarget.value === "IMAGE");
    }


    // ==================   HANDLEUPLOAD()   ==================
    // Function that is executed upon form submission

    const handleUpload = async (data: UploadForm) => {      // When the Submit button is pressed
        logger.info("(UploadFiles) POST request (Full Data): ", data);
        setResponseError(false)

        let duplicateFile = false;
        if (data.fileName === null) {
            setShowAlert(ShowErrorMessage("Could not find file. Please select a file to upload."));
            return;
        }
        logger.info("(UploadFiles) POST request (Just File): ", data.fileName[0]);

        let fileType = data.fileType;
        if ((data.fileType as string) === "IMAGE") {
            fileType = data.imageType;
        }

        //Reset all previous alerts
        setShowAlert(HideMessage())
        setReplaceFileAlert(HideMessage())
        setSyncAlert(HideMessage())
        setInvalidFileAlert(HideMessage())

        let formattedStartDate = "null"
        let formattedEndDate = "null"
        let encodedDescription = encodeURIComponent(data.fileDescription)   // Allows special characters

        let parameters = '?profile=' + data.profileID + "&shortDesc=" + encodedDescription
            + "&fileType=" + fileType + "&enabled=" + enableSync

        // ==================   SYNC SECTION   ==================
        // Everything to do with file time syncing

        if (enableSync) {
            // 2021-11-15T22:00:00Z (Data should be sent using this format)
            if (startDate !== null) {
                formattedStartDate = format(startDate, 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                parameters = parameters + "&startDate=" + formattedStartDate + "&endDate="
            }

            if (endDate !== null) {
                formattedEndDate = format(endDate, 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                parameters = parameters + formattedEndDate
            }

            // Error: If end date comes before start date
            if (endDate && startDate && endDate <= startDate) {
                logger.error("There is a sync date issue")
                setSyncAlert(ShowErrorMessage("Please select an end date that is later than the start date."));
                return
            }

            // Not an error, but extra validation: If end date comes before the current date and time
            else if (endDate !== null && endDate <= currentDate) {
                logger.error("End date occurs before the current date")
                setSyncAlert(ShowErrorMessage("Please select an end date that is later than today's date at the current time."));
                return
            }

            else if (startDate === null) {
                logger.error("User did not provide a start date")
                setSyncAlert(ShowErrorMessage("Please specify a date for this file to start syncing."));
            }

            else (logger.debug('No sync date issues'))
        }

        else {
            // logger('Sync is disabled')
            setStartDate(null)
            setEndDate(null)

            // Send empty parameters for start and end date
            parameters = parameters + "&startDate=&endDate="
        }

        // ==================   DUPLICATE FILE SECTION   ==================
        // Everything to do with duplicate files (check other scale profiles when uploading feature files as well, since we store all feature files in one directory)



        function onSuccess(response: ResponseType) {
            logger.info("(UploadFiles) Backend Response (POST File): ", response);
            setShowAlert(HideMessage())
            setReplaceFileAlert(HideMessage())
            setSyncAlert(HideMessage())
            setInvalidFileAlert(HideMessage())

            setOpenPopup(false)
            setDisable(false)

            setEnableSync(true);
            setStartDate(new Date());
            setEndDate(null);

            reset(defaultValues);

            dispatch(FileAPI.fetchFiles())
            dispatch(ProfileAPI.fetchProfiles())
        }

        function onFail(response: ResponseType) {
            setResponseError(true)
            // if user cancels upload
            setDisable(false);
            if (response.cancelled) {
                setShowAlert(ShowWarningMessage("Upload cancelled by user."));

            }

            // if any other error
            else {
                if (response.response && response.response.errorDescription) {
                    setShowAlert(ShowErrorMessage(response.response.errorDescription));
                }
                else {
                    setShowAlert(ShowErrorMessage("Failed to upload file. Please try again."));
                }
            }
        }

        // Check for duplicates (Feature files)
        if (UploadMode === "FEATURE") {
            let associatedProfileNames: string[] = []
            for (const file of profile.featureFiles) {
                if (data.fileName[0].name === file.filename) {

                    if (data.profileID === file.profileId) {
                        // If there is already a file with the same name in the same profile
                        duplicateFile = true;
                        setReplaceFileAlert(ShowErrorMessage("This Feature file already exists in this profile, would you like to overwrite it? If not, close this popup."));
                    }
                    else {
                        // If there is already a file with the same name in a different profile
                        let associatedProfile = profile.profiles.find((profile) => profile.profileId === file.profileId)
                        if (associatedProfile) {
                            associatedProfileNames.push(associatedProfile.name)
                        }
                        else {
                            logger.error("Could not find associated profile for file: ", file)
                        }
                    }
                }
            }

            if (associatedProfileNames.length > 0) {
                duplicateFile = true;
                setProfilesAlert(ShowWarningMessage(`Warning: This feature file exists in the following scale profiles: [${associatedProfileNames.join(", ")}]. 
                    Checking the box below will delete this feature file from those scale profiles as well. 
                    Leaving it unchecked and clicking Delete File again will only delete it from this scale profile.`))
            }
        }
        else { //Check for duplicates Config and Other files
            for (const file of profile.files) {
                if (data.fileName[0].name === file.filename && data.profileID === file.profileId) {
                    // If there is already a file with the same name in the same profile
                    duplicateFile = true;
                    setReplaceFileAlert(ShowErrorMessage("This file already exists in this profile, would you like to overwrite it? If not, close this popup."));
                }
            }
            for (const file of profile.mediaFiles) {
                if (data.fileName[0].name === file.filename && data.profileID === file.profileId) {
                    // If there is already a file with the same name in the same profile
                    duplicateFile = true;
                    setReplaceFileAlert(ShowErrorMessage("This file already exists in this profile, would you like to overwrite it? If not, close this popup."));
                }
            }
        }


        // ==================   CALLBACK RELATED CODE    ==================
        // onSuccess() and onFail() are executed depending on if the API call was successful or not.

        // Not a duplicate file
        if (!duplicateFile) {
            setDisable(true)
            const fileData = new FormData();
            fileData.append('file', data.fileName[0]);
            let reqID = Date.now()
            if (addCallback(callbacks, reqID, "Upload File", onSuccess, onFail, { successMsg: "File successfully uploaded!" })) {
                console.log("second", parameters)
                dispatch(FileAPI.fileUpload(reqID, fileData, parameters, uploadPercent, setUploadPercent, setUploadProgress, controller))
            }
        }

        // if duplicateFile === true and user opts to overwrite an existing file
        else {
            // TODO: Update the file version eventually
            // logger("(UploadFiles) Overwriting existing HT file in the scale profile")
            if (data.overwriteExistingFile) {
                setDisable(true)

                const fileData = new FormData();
                fileData.append('file', data.fileName[0]);
                let reqID = Date.now()

                if (addCallback(callbacks, reqID, "Upload File", onSuccess, onFail, { successMsg: "File successfully uploaded!" })) {
                    dispatch(FileAPI.fileUpload(reqID, fileData, parameters, uploadPercent, setUploadPercent, setUploadProgress, controller))
                }
            }
        }
    }

    useEffect(() => {
        // return () === cleanup function

        // return () => {
        //     controller.cancel("Operation cancelled by the user.")
        // }
    }, [])

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleSubmit(handleUpload)}>
                    <div className="uploadFile">
                        <FormSelect
                            label="Select a Scale Profile: "
                            id="profileID"
                            name="profileID"
                            placeholder={props.selectValue ? props.selectValue : "--Select a profile--"}
                            dataset={profiles}
                            register={register}
                            objId="profileId"
                            objName="name"
                            validation={{ required: "Please select a profile" }}
                            error={errors.profileID}
                            onlyValue={onlyValue ? onlyValue : false}
                            disabled={undefined}
                        />

                        <FormInput
                            label="Upload a file: "
                            id="fileName"
                            name="fileName"
                            type="file"
                            accept={getValidFileExensions(props.categories)}
                            register={register}
                            validation={{ required: "Please select a file to upload" }}
                            error={errors.fileName}
                        />

                        <FormInput
                            label="File Description: "
                            id="fileDescription"
                            name="fileDescription"
                            type="text"
                            placeholder="Enter a description"
                            register={register}
                            validation={{
                                required: "Please enter a brief description",
                                maxLength: { value: 60, message: "You exceeded the max description length" }
                            }}
                            error={errors.fileDescription}
                        />

                        <FormSelect
                            label="File Type: "
                            id="fileType"
                            name="fileType"
                            placeholder="--Select a Category--"
                            dataset={categories}
                            register={register}
                            objId="fileId"
                            objName="name"
                            validation={{ required: "Please select a category" }}
                            error={errors.fileType}
                            onlyValue={categories.length === 1}
                            onChange={handleOnCategoryChange}
                            disabled={undefined}
                        />

                        {loadImageTypes ?

                            <FormSelect
                                label="Image Type: "
                                id="imageType"
                                name="imageType"
                                placeholder="--Select an Image Type--"
                                dataset={imageCategories}
                                register={register}
                                objId="fileId"
                                objName="name"
                                validation={{ required: "Please select an image type" }}
                                error={errors.imageType}
                                onlyValue={undefined}
                                disabled={undefined}
                            />
                            : null}

                        <FormCheck type="checkbox" className="d-flex p-2" >
                            <FormCheck.Label className="customCheckbox">
                                <FormCheck.Input defaultChecked={enableSync} id="enableSync" className="me-2" onClick={(e: React.FormEvent<HTMLInputElement>) => setEnableSync(e.currentTarget.checked)} />
                                Enable syncing
                            </FormCheck.Label>
                        </FormCheck>

                        {enableSync ?
                            <>
                                <Form.Label>Sync start date:</Form.Label>
                                <DatePicker
                                    className="form-control"
                                    showTimeSelect
                                    dateFormat="MMMM d, yyyy h:mm aa"
                                    timeClassName={handleColor}
                                    selected={startDate}
                                    onChange={(date) => setStartDate(date)}
                                />

                                <Form.Label>Sync end date (optional):</Form.Label>
                                <Tooltip className="info" title={<Typography>Providing no end date means the file will be checked during every sync without a date to stop.</Typography>}>
                                    <IconButton aria-label="sync end date">
                                        <Help />
                                    </IconButton>
                                </Tooltip>
                                <DatePicker
                                    className="form-control"
                                    showTimeSelect
                                    dateFormat="MMMM d, yyyy h:mm aa"
                                    timeClassName={handleColor}
                                    selected={endDate}
                                    onChange={(date) => setEndDate(date)}
                                />

                                <br />
                            </> : null
                        }
                        <DoubleSliderTemplate 
                            title = "Reboot Window:" 
                            units = {"hours"} 
                            min = {1} 
                            max = {24} 
                            message = {"The reboot window should be hours when the scale is not in operation"}
                            value = {[5,20]}
                            errorStatus = {status => setStatusAlert(ShowErrorMessage("Please enter a valid number between " + 0 + " and " + 24 + " " + "hours"))} //
                            onValueChange = {(window) => setRebootWindow(window)}
                            isTime = {true}
                        />

                        {syncAlert.show ? <Alert className="m-1" variant="filled" severity={syncAlert.severity}>{syncAlert.message}</Alert> : null}

                        {invalidFileAlert.show ? <Alert className="m-1" variant="filled" severity={invalidFileAlert.severity}>{invalidFileAlert.message}</Alert> : null}

                        <br />

                        {/* Duplicate file name, prompt user if they want to replace it */}
                        {replaceFileAlert.show ?
                            <>
                                <Alert className="m-1" variant="filled" severity={replaceFileAlert.severity}>{replaceFileAlert.message}</Alert>

                                <FormCheck type="checkbox" className="d-flex p-2">
                                    <FormCheck.Label className="customCheckbox">
                                        {/* This is displaying an strange error but still runs, not sure why it is doing this */}
                                        <FormCheck.Input id="overwriteExistingFiles" className="me-2" {...register("overwriteExistingFile", { required: "Please opt to overwrite the file or close the popup" })} />
                                        Yes
                                    </FormCheck.Label>
                                </FormCheck>
                                {errors.overwriteExistingFile && <p className="formError">{errors.overwriteExistingFile.message}</p>}

                                <br />
                            </> : null}

                        {/* Duplicate feature file in other scale profiles, but not currently in user's selected profile */}
                        {(profilesAlert.show && !replaceFileAlert.show) ?
                            <>
                                <Alert className="m-1" variant="filled" severity={profilesAlert.severity}>{profilesAlert.message}</Alert>

                                <FormCheck type="checkbox" className="d-flex p-2">
                                    <FormCheck.Label className="customCheckbox">
                                        <FormCheck.Input id="overwrite" className="me-2" onClick={(e) => e.target.checked} {...register("overwriteExistingFile", { required: "Please opt to overwrite the file or close the popup" })} />
                                        Yes
                                    </FormCheck.Label>
                                </FormCheck>
                                {errors.overwriteExistingFile && <p className="formError">{errors.overwriteExistingFile.message}</p>}

                            </> : null}

                        {/* Error uploading file */}
                        {showAlert.show ? <Alert className="m-1" variant="filled" severity={showAlert.severity}>{showAlert.message}</Alert> : null}
                        {!responseError ?
                            <>
                                {uploadPercent > 0 && uploadPercent < 100 &&
                                    <>
                                        <ProgressBar now={uploadPercent} animated label={`${uploadPercent}%`} />
                                        <p>{`Upload Progress: ${formatBytes(uploadProgress.loaded)} / ${formatBytes(uploadProgress.total)}`}</p>
                                        <button className="formButton1" onClick={handleAbort} type="button">
                                            <Upload />
                                            <span> Cancel Upload </span>
                                        </button>
                                    </>
                                }

                                {uploadPercent === 100 &&
                                    <>
                                        <ProgressBar variant='success' now={uploadPercent} animated label={`${uploadPercent}%`} />
                                        <p>{`Upload Complete: ${formatBytes(uploadProgress.loaded)} / ${formatBytes(uploadProgress.total)}`}</p>
                                    </>
                                }
                            </>
                            : null}

                        <div className="formButtons">
                            <button className="formButton1" disabled={disable} onClick={handleSubmit(handleUpload)} type="submit">
                                <Upload />
                                <span> Upload File </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    );
}


// =================== EDIT FILES    ===================
interface EditFilesProps {
    title: string
    openPopup: boolean
    setOpenPopup: (openPopup: boolean) => void
    currentFile: FileForEvent
    categories: FileType[]
}

const EditFiles = (props: EditFilesProps) => {
    interface EditFilesForm {
        fileName: string
        fileDescription: string
        enableSync: boolean
        fileType: FileType
    }

    const { title, openPopup, setOpenPopup, currentFile } = props;
    const { register, handleSubmit, reset, formState: { errors } } = useForm<EditFilesForm>();

    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();

    // logger("currentFile (startDate): " , currentFile.startDate)
    // logger("currentFile (endDate): " , currentFile.endDate)

    // <Alert> Notifications for errors / warnings
    const [showAlert, setShowAlert] = useState<AlertMessage>(HideMessage())
    const [syncAlert, setSyncAlert] = useState<AlertMessage>(HideMessage())
    const [enableSync, setEnableSync] = useState(currentFile.enabled)

    // Syncing variables
    const [startDate, setStartDate] = useState<number | null>(Date.parse(currentFile.startDate));  // Start date for syncing
    const [endDate, setEndDate] = useState<number | null>(Date.parse(currentFile.endDate));        // End date for syncing
    const currentDate = Date.now();                                     // Used for validating endDate    
    const [clearStart, setClearStart] = useState(false)
    const [clearEnd, setClearEnd] = useState(false)

    const categories: { name: string, fileId: string }[] = []
    props.categories.forEach((category) => {
        categories.push(createData(PrintFileType(category), category))
    })

    // ==================   MISCELLANEOUS HELPER FUNCTIONS   ==================
    // Default values + two functions to reset pre-set dates + function to close dialog box

    const resetStartDate = () => {
        setStartDate(null)
        setClearStart(true)
    }

    const resetEndDate = () => {
        setEndDate(null)
        setClearEnd(true)
    }

    const handleColor = () => {
        return "text-success"
    };

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        fileName: currentFile.filename,
        fileDescription: currentFile.shortDesc,
        fileType: currentFile.fileType
    };

    const handleCloseDialog = (event: any) => {
        setShowAlert(HideMessage())
        setSyncAlert(HideMessage())
        setOpenPopup(false);

        setEnableSync(currentFile.enabled);
        setStartDate(null);
        setEndDate(null);
        setClearStart(false);
        setClearEnd(false);

        event.stopPropagation()
        reset(defaultValues);
    }


    // ==================   HANDLEADD()   ==================
    // Function that is executed upon form submission

    const handleAdd = handleSubmit(async (data: EditFilesForm) => {    // When the Submit button is pressed
        logger.info("(EditFiles) POST request data: ", data)

        setShowAlert(HideMessage())
        setSyncAlert(HideMessage())

        let formattedStartDate = ""
        let formattedEndDate = ""

        if (enableSync) {
            // 2021-11-15T22:00:00Z (Data should be sent using this format)
            if (startDate !== null) {
                formattedStartDate = format(startDate, 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                // logger("(EditFiles) formattedStartDate (1): ", formattedStartDate);
            }

            if (endDate !== null) {
                formattedEndDate = format(endDate, 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                // logger("(EditFiles) formattedEndDate (1): ", formattedEndDate);
            }

            // Not an error, but extra validation: If end date comes before the current date and time
            if (endDate !== null && endDate <= currentDate) {
                logger.error("(EditFiles) End date occurs before the current date")
                setSyncAlert(ShowErrorMessage("Please select an end date that is later than today's date at the current time."));
                return
            }

            // Error: If end date comes before start date
            else if (endDate && (!startDate || endDate <= startDate)) {
                if (formattedEndDate <= currentFile.startDate) {
                    logger.error("(EditFiles) No new start date was provided and the provided end date is earlier than the current start date.")
                    setSyncAlert(ShowErrorMessage("Please select an end date that is later than the start date."));
                    return
                }
            }

            // Error: If start date comes before end date
            else if (startDate && endDate && startDate >= endDate) {
                if (formattedStartDate >= currentFile.endDate) {
                    logger.error("(EditFiles) No new end date was provided and the provided start date is later than the current start date.")
                    setSyncAlert(ShowErrorMessage("Please select a start date that is earlier than the end date."));
                    return
                }
            }

            else (logger.debug('(EditFiles) No sync date issues'))

            // Error: If there was never a start date provided
            if (startDate === null && currentFile.startDate === null) {
                logger.error("(EditFiles) User did not provide a start date")
                setSyncAlert(ShowErrorMessage("Please specify a date for this file to start syncing."));
                return
            }

            // If there was a previous start date, but not a new one provided by user
            if (startDate === null && currentFile.startDate !== null) {
                if (clearStart) {
                    formattedStartDate = ""
                    // logger("(EditFiles) User must specify a start date if sync is enabled")
                    setSyncAlert(ShowErrorMessage("Please specify a date for this file to start syncing."))
                    return
                }

                formattedStartDate = format(Date.parse(currentFile.startDate), 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                // logger("(EditFiles) formattedStartDate (2): ", formattedStartDate);
            }

            // If there was a previous end date, but not a new one provided by user
            if (endDate === null && currentFile.endDate !== null) {
                try {
                    if (clearEnd) {
                        formattedEndDate = ""
                        // logger("(EditFiles) User did not provide an end date")                    
                    }

                    else {
                        formattedEndDate = format(Date.parse(currentFile.endDate), 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                        // logger("(EditFiles) formattedEndDate (2): ", formattedEndDate);    
                    }
                }
                catch {
                    formattedEndDate = ""
                }

            }
        }

        else {  // EnableSync === false
            logger.info('(EditFiles) Sync is disabled')

            // If file previously had a start date and is just disabled
            if (currentFile.startDate !== null) {
                formattedStartDate = format(Date.parse(currentFile.startDate), 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                // logger("(EditFiles) formattedStartDate (3): ", formattedStartDate);
            }

            // If file previously had an end date and is just disabled
            if (currentFile.endDate !== null) {
                formattedEndDate = format(Date.parse(currentFile.endDate), 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                // logger("(EditFiles) formattedEndDate (3): ", formattedEndDate);
            }

            if (!startDate && clearStart) formattedStartDate = ""     // No previous start date + cleared
            if (!endDate && clearEnd) formattedEndDate = ""       // No previous end date + cleared
        }

        // ==================   Updated Data   ==================
        // Formatting the data to be sent in the request
        const updatedData: FileForEvent = { ...currentFile }
        updatedData.fileId = currentFile.fileId
        updatedData.shortDesc = data.fileDescription
        updatedData.enabled = enableSync
        updatedData.startDate = formattedStartDate
        updatedData.endDate = formattedEndDate
        updatedData.fileType = data.fileType


        // logger("(EditFiles) updated POST request Data: ", updatedData)

        // ==================   CALLBACK RELATED CODE    ==================
        // onSuccess() and onFail() are executed depending on if the API call was successful or not.

        function onSuccess(response: ResponseType) {
            logger.info("(EditFiles) Backend Response: ", response);

            setShowAlert(HideMessage())
            setSyncAlert(HideMessage())
            setOpenPopup(false);

            setEnableSync(currentFile.enabled);
            setStartDate(null);
            setEndDate(null);
            setClearStart(false);
            setClearEnd(false);

            reset(defaultValues);

            dispatch(FileAPI.fetchFiles())
            dispatch(ProfileAPI.fetchProfiles())
        }

        function onFail(response: ResponseType) {
            logger.info("(EditFiles) Backend error: ", response);
            setShowAlert(ShowErrorMessage("Error trying to update file. Please try again later."))
        }

        let reqEditID = Date.now()
        if (addCallback(callbacks, reqEditID, "Edit File", onSuccess, onFail, { successMsg: "File successfully edited!" })) {
            dispatch(FileAPI.fileUpdate(reqEditID, updatedData))
        }
    })

    useEffect(() => {
        reset(defaultValues);
        setEnableSync(currentFile.enabled);
        setStartDate(null);
        setEndDate(null);
        setClearStart(false);
        setClearEnd(false);
        logger.info("(EditFiles) Current File: ", currentFile)

    }, [props]);

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title} - <span style={{ fontWeight: "bold" }}>{currentFile.filename}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleAdd}>
                    <FormInput
                        disabled    // Do not allow users to change file name, we don't support it as of now
                        defaultValue={currentFile.filename}
                        label="File Name: "
                        id="fileName"
                        name="fileName"
                        type="text"
                        placeholder="Enter a profile name"
                        register={register}
                        validation={{
                            maxLength: { value: 100, message: "You exceeded the max profile name length" }
                        }}
                        error={errors.fileName}
                    />

                    <FormInput
                        defaultValue={currentFile.shortDesc}
                        label="File Description: "
                        id="fileDescription"
                        name="fileDescription"
                        type="text"
                        placeholder="Enter a description"
                        register={register}
                        validation={{
                            required: "Please enter a brief description",
                            maxLength: { value: 60, message: "You exceeded the max description length" }
                        }}
                        error={errors.fileDescription}
                    />

                    <FormSelect
                        label="File Category: "
                        id="fileType"
                        name="fileType"
                        placeholder="--Select a Category--"
                        dataset={categories}
                        register={register}
                        objId="fileId"
                        objName="name"
                        validation={{ required: "Please select a category" }}
                        error={errors.fileType}
                        disabled={undefined}
                        onlyValue={false}
                        customdefault={currentFile.fileType ? currentFile.fileType : undefined}
                    />

                    <FormCheck type="checkbox" className="d-flex p-2" >
                        <FormCheck.Label className="customCheckbox">
                            <FormCheck.Input defaultChecked={currentFile.enabled} id="enableSync" className="me-2" onClick={(e: React.MouseEvent<HTMLInputElement, MouseEvent>) => setEnableSync(e.currentTarget.checked)} {...register("enableSync")} />
                            Enable syncing
                        </FormCheck.Label>
                    </FormCheck>

                    {enableSync ?

                        <>
                            <Form.Label>Sync start date:</Form.Label>
                            <DatePicker
                                className="form-control"
                                showTimeSelect
                                isClearable
                                dateFormat="MMMM d, yyyy h:mm aa"
                                timeClassName={handleColor}
                                selected={startDate}
                                placeholderText={currentFile.startDate && (!startDate && !clearStart) ? format(Date.parse(currentFile.startDate), "LLL") : ""}
                                onChange={(date) => setStartDate(date)}
                            />

                            <Form.Label>Sync end date (optional):</Form.Label>
                            <Tooltip className="info" title={<Typography>Providing no end date means the file will be checked during every sync without a date to stop.</Typography>}>
                                <IconButton aria-label="sync end date">
                                    <Help />
                                </IconButton>
                            </Tooltip>
                            <DatePicker
                                className="form-control"
                                showTimeSelect
                                isClearable
                                dateFormat="MMMM d, yyyy h:mm aa"
                                timeClassName={handleColor}
                                selected={endDate}
                                placeholderText={currentFile.endDate && (!endDate && !clearEnd) ? format(Date.parse(currentFile.endDate), "LLL") : ""}
                                onChange={(date) => setEndDate(date)}
                            />

                            <div className="formButtons2">

                                <button className="formButton1" onClick={resetStartDate} type="button">
                                    <RemoveCircle />
                                    <span> Clear Start Date </span>
                                </button>
                                <button className="formButton1" onClick={resetEndDate} type="button">
                                    <RemoveCircle />
                                    <span> Clear End Date </span>
                                </button>
                            </div>

                        </> : null
                    }

                    <br />

                    {syncAlert.show ? <Alert className="m-1" variant="filled" severity={syncAlert.severity}>{syncAlert.message}</Alert> : null}

                    <br />

                    {showAlert.show ? <Alert className="m-1" variant="filled" severity={showAlert.severity}>{showAlert.message}</Alert> : null}

                    <div className="formButtons">
                        <button className="formButton1" onClick={handleAdd} type="submit">
                            <Edit />
                            <span> Edit File </span>
                        </button>

                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    );
}


// =================== DELETE FILES  ===================
interface DeleteFilesProps {
    title: string
    openPopup: boolean
    setOpenPopup: (openPopup: boolean) => void
    file: FileForEvent | null
}
function DeleteFiles(props: DeleteFilesProps) {
    interface DeleteFilesForm {
        deleteAll: boolean
    }

    const { title, openPopup, setOpenPopup, file } = props;
    const { register, handleSubmit, formState: { errors } } = useForm<DeleteFilesForm>();


    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();
    const profileState = useAppSelector((state) => state.profile);

    // <Alert> Notifications for errors / warnings
    const [showAlert, setShowAlert] = useState<AlertMessage>(HideMessage())
    const [profilesAlert, setProfilesAlert] = useState<AlertMessage>(HideMessage())    // ?
    let currentProfileId = ""
    useEffect(()=>{
    if (!file) { setShowAlert(ShowErrorMessage("No file selected")) }
    else {
        if (!file.filename){setShowAlert(ShowWarningMessage("Could not Find File Name!")) }
        if (!file.fileId){setShowAlert(ShowErrorMessage("Could not Find File Identifier! Please Contact Support!")) }
        if (!file.profileId){setShowAlert(ShowErrorMessage("Could not Profile Identifier! Please Contact Support!")) }
        if (!file.shortDesc){setShowAlert(ShowWarningMessage("Could not Find File Description!")) }
    }
    },[file])


    const [featureFileList, setFeatureFileList] = useState<string[]>([])
    const [featureFileNames, setFeatureFileNames] = useState<string[]>([])

    let temp = []

    // Duplicate Feature File check / Checking other profiles for matching feature files
    let duplicateFeatureInOtherProfile = false; // This feature file exists in another scale profile already
    let associatedProfileIds: string[] = [];              // Contains scale profile IDs that contain the user's feature file already
    let associatedProfileNames: string[] = [];            // To display which scale profiles contain the user's feature file already

    // logger("(DeleteFiles) File Name: ", fileName)
    // logger("(DeleteFiles) TGZ List: ", featureFileList)

    // ==================   MISCELLANEOUS HELPER FUNCTIONS   ==================
    // Function to close dialog box and functions to grab scale profiles if a feature file exists multiple times.

    const handleCloseDialog = (event: any) => {
        setShowAlert(HideMessage())
        setProfilesAlert(HideMessage())
        event.stopPropagation()
        setOpenPopup(false);
    }


    /*  
        These functions are used to find the same package.tgz file in different scale profiles
        The purpose is to show the list to the user and allow them to choose whether they want to 
        delete for all profiles or just remove the one entry   
    */
    const handleFindTGZ = (fileName: string) => {

        function onSuccess(response: ResponseType) {
            // logger("response from fetch: ", response.response)
            temp = response.response.map((profile: Profile) => profile.profileId)
            setFeatureFileList(temp)
        }

        function onFail(response: ResponseType) {
            logger.error("(DeleteFiles)", response);
        }


        try {
            let reqID = Date.now()
            if (addCallback(callbacks, reqID, "Fetch Filenames", onSuccess, onFail, {})) {
                dispatch(FileAPI.fetchFileNames(reqID, fileName))
            }
        } catch {
            // do nothing
        }
    }

    const handleFindTGZNames = () => {
        // logger("featureFileList: ", featureFileList)

        profileState.profiles.map(profile => {
            if (featureFileList.includes(profile.profileId)) {

                // if already in array, don't add again.
                if (!featureFileNames.includes(profile.name)) {
                    setFeatureFileNames(oldList => [...oldList, profile.name])
                }
            }
            return null
        })
    }


    // ==================   HANDLEDELETE()   ==================
    // Function that is executed upon form submission

    const handleDelete = handleSubmit(async (data: DeleteFilesForm) => {
        logger.info("(DeleteFiles) Request (Full Data): ", data.deleteAll);

        setShowAlert(HideMessage())
        setProfilesAlert(HideMessage())

        duplicateFeatureInOtherProfile = false; // This feature file exists in another scale profile already
        associatedProfileIds = [];              // Contains scale profile IDs that contain the user's feature file already
        associatedProfileNames = [];            // To display which scale profiles contain the user's feature file already


        // ==================   CALLBACK RELATED CODE    ==================
        // onSuccess() and onFail() are executed depending on if the API call was successful or not.

        function onSuccess(response: ResponseType) {
            logger.info("(DeleteFiles) Backend Response (DELETE File): ", response);
            setOpenPopup(false);
            setProfilesAlert(HideMessage())

            dispatch(FileAPI.fetchFiles())
        }

        function onFail(response: ResponseType) {
            logger.error("(DeleteFiles)", response);
            setShowAlert(ShowErrorMessage("Error trying to delete file. Please try again later."))
        }

        // ====================================

        if(file){
            if (file.filename.slice(-11) === "package.tgz") {
                for (let i = 0; i < profileState.featureFiles.length; i++) {                        // Searches all feature files associated with all profiles

                    if (file.filename === profileState.featureFiles[i].filename) {    // Checks if user's current file matches any existing feature file in database in different profile
                        if (currentProfileId !== profileState.featureFiles[i].profileId) {  // data.scaleProfiles !== state.featureFiles[i].profileId      (Scale Profile does not match what user selected)
                            associatedProfileIds.push(profileState.featureFiles[i].profileId)
                        }
                    }
                }

                // To display which other scale profiles contain the user's feature file
                for (let i = 0; i < profileState.profiles.length; i++) {
                    if (associatedProfileIds.includes(profileState.profiles[i].profileId)) {
                        associatedProfileNames.push(profileState.profiles[i].name)

                        setProfilesAlert(ShowWarningMessage(`Warning: This feature file exists in the following scale profiles: [${associatedProfileNames.join(", ")}]. 
                        Checking the box below will delete this feature file from those scale profiles as well. 
                        Leaving it unchecked and clicking Delete File again will only delete it from this scale profile.`))
                        duplicateFeatureInOtherProfile = true;
                    }
                }
            }

            // If file is a .ht file, or is not a duplicate feature file in other scale profiles
            if (!duplicateFeatureInOtherProfile) {

                let parameters = '?id=' + file.fileId

                let delId = Date.now()
                if (addCallback(callbacks, delId, "Delete File", onSuccess, onFail, { successMsg: "File successfully deleted!" })) {
                    dispatch(FileAPI.fileDelete(delId, parameters))
                }
            }

            // if (duplicateFeatureInOtherProfile)
            else if (duplicateFeatureInOtherProfile && data.deleteAll !== undefined) {

                // (data.deleteAll === true)  | Delete every instance of this file from all scale profiles. File is deleted from upgrades folder
                if (data.deleteAll) {
                    let delId = Date.now()
                    let parameters = '?deleteInAllProfiles=true&id=' + file.fileId

                    if (addCallback(callbacks, delId, "Delete Duplicate Feature File", onSuccess, onFail, { successMsg: "File successfully deleted from all profiles!" })) {
                        dispatch(FileAPI.fileDelete(delId, parameters))
                    }
                }

                // (data.deleteAll === false) | Only delete the current entry on the database, file is not deleted from upgrades folder
                else if (!data.deleteAll) {
                    let delId = Date.now()
                    let parameters = '?deleteInAllProfiles=false&id=' + file.fileId

                    if (addCallback(callbacks, delId, "Delete Duplicate Feature File", onSuccess, onFail, { successMsg: "File successfully deleted from this profile!" })) {
                        dispatch(FileAPI.fileDelete(delId, parameters))
                    }
                }
            }
        }
    });

    useEffect(() => {
        if (featureFileList !== undefined && file && file?.filename.slice(-11) === "package.tgz") {
            if (featureFileList.length === 0) handleFindTGZ(file?.filename)
            if (featureFileList.length !== 0) handleFindTGZNames()
        }
    }, [featureFileList]);


    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                    {title} - <span style={{ fontWeight: "bold" }}>
                        {file && profileState.profiles[file.profileId]? 
                        profileState.profiles[file.profileId].profileName 
                        : 
                        "No Profile nme"}</span>
                </Typography>
            </DialogTitle>

            <DialogContent>
                <Form className="form-control" onSubmit={handleDelete}>
                    <div className="formSpacing">
                        <Form.Label id="confirmDelete" style={{ color: "black" }}>Are you sure you want to delete the following file:</Form.Label>
                        <p>File Name:   <span style={{ fontWeight: "bold" }}>{file?.filename}        </span></p>
                        <p>Description: <span style={{ fontWeight: "bold" }}>{file?.shortDesc} </span></p>


                        {file?.filename.slice(-11) === "package.tgz"
                            ? <>  <br />
                                <Alert className="m-1" variant="outlined" severity="warning">
                                    <p className="fw-bolder color-black"> The following scale profiles contain this feature file:
                                        {featureFileList !== undefined
                                            ? featureFileNames.map((file, index) => (
                                                <li key={index}>{file}</li>
                                            ))
                                            : null
                                        }

                                    </p>
                                </Alert>
                            </>
                            : null
                        }

                        {/* Duplicate feature file in other scale profiles, but not currently in user's selected profile */}
                        {(profilesAlert.show) ?
                            <>
                                <Alert className="m-1" variant="filled" severity={profilesAlert.severity}>{profilesAlert.message}</Alert>

                                <FormCheck type="checkbox" className="d-flex p-2">
                                    <FormCheck.Label className="customCheckbox">
                                        <FormCheck.Input id="deleteAll" className="me-2" onClick={(e) => e.target.checked} {...register("deleteAll")} />
                                        Delete from all above Scale Profiles
                                    </FormCheck.Label>
                                </FormCheck>
                                {errors.deleteAll && <p className="formError">{errors.deleteAll.message}</p>}

                            </> : null}

                        <br />

                        <Alert className="m-1" variant="outlined" severity="warning">
                            <p className="fw-bold"> Warning: Any file that is deleted cannot be recovered unless uploaded again</p>
                        </Alert>

                        {showAlert.show ? <Alert className="m-1" variant="filled" severity={showAlert.severity}>{showAlert.message}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleDelete} type="submit" value="Delete File" >
                                <Delete />
                                <span> Delete File </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>
                </Form>
            </DialogContent>
        </Dialog>
    )
}

// =================== VIEW FILES  ===================
interface ViewFilesProps {
    title: string
    openPopup: boolean
    setOpenPopup: (openPopup: boolean) => void
    currentFile: FileForEvent
    categories: FileType[]
}

const ViewFiles = (props: ViewFilesProps) => {
    interface EditFilesForm {
        fileName: string
        fileDescription: string
        enableSync: boolean
        fileType: FileType
    }

    const { title, openPopup, setOpenPopup, currentFile } = props;
    const { register, handleSubmit, reset, formState: { errors } } = useForm<EditFilesForm>();

    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();

    // logger("currentFile (startDate): " , currentFile.startDate)
    // logger("currentFile (endDate): " , currentFile.endDate)

    // <Alert> Notifications for errors / warnings
    const [showAlert, setShowAlert] = useState<AlertMessage>(HideMessage())
    const [syncAlert, setSyncAlert] = useState<AlertMessage>(HideMessage())
    const [enableSync, setEnableSync] = useState(currentFile.enabled)

    const handleCloseDialog = (event: any) => {
        setShowAlert(HideMessage())
        event.stopPropagation()
        setOpenPopup(false);
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="md" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title} - <span style={{ fontWeight: "bold" }}>{currentFile.filename}</span>
                </Typography>
            </DialogTitle>
            <DialogContent>
                <div className="viewSpacing">

                    <button className="formButton1" onClick={handleCloseDialog} type="button">
                        <Upload />
                        <span> Close </span>
                    </button>

                </div>
            </DialogContent>
        </Dialog>
    );
}

export { UploadFiles, EditFiles, DeleteFiles, ViewFiles };