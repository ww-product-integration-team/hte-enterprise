import { useState, useEffect, useRef, useMemo } from "react";
import { Delete, Download, Edit, Refresh, Upload, FindInPage } from "@mui/icons-material"
import { Button, ButtonGroup } from "@themesberg/react-bootstrap"
import type { Virtualizer } from '@tanstack/react-virtual';
import MaterialReactTable, { MRT_ColumnDef } from "material-react-table"
import { Profile } from "../../types/asset/ProfileTypes";
import { Dialog, DialogContent, DialogTitle, IconButton, Tooltip, Typography } from "@mui/material";
import { SortingState } from "@tanstack/table-core";
import { format } from "date-fns";
import { formatBytes } from "../utils/utils";
import { DeleteFiles, EditFiles, UploadFiles, ViewFiles } from "./FileForm";
import { useFileDownloader } from "./fileDownload/FileDownloader";
import { useDispatch } from "react-redux";
import { FileAPI } from "../api";
import { useAppSelector } from "../../state";
import { FileForEvent, FileType, getDefaultFile, IsImageType, PrintFileType } from "../../types/asset/FileTypes";

interface FileTableRowType{
    actions: JSX.Element | null,
    file: FileForEvent,
    // content: string | null,
    previewData: string | null,
}

interface FilesViewProps {
    title?: string,
    titleIcon?: JSX.Element,
    fileTypes: FileType[],
    files: FileForEvent[],
    currentProfile : Profile
}

export const FilesView = (props: FilesViewProps) => {
    const {title, titleIcon, fileTypes, files, currentProfile} = props;
    const rowVirtualizerInstanceRef = useRef<Virtualizer<HTMLDivElement, HTMLTableRowElement>>(null);
    const [isLoading, setIsLoading] = useState(true);
    const [sorting, setSorting] = useState<SortingState>([]);
    const [currentFile, setCurrentFile] = useState<FileForEvent | null>(null);
    const imageData = useAppSelector(state => state.profile.imageData);
    const dispatch = useDispatch();
    const [openPopup, setOpenPopup] = useState<"UPLOAD" | "EDIT" | "DELETE" | "DOWNLOAD" | "VIEW" | null>(null);
    const [openContent, setOpenContent] = useState(false)
    const [contentData, setContentData] = useState("")
    const [tableData, setTableData] = useState<FileTableRowType[]>([]);

    useEffect(() => {
        if(files){
            const tableData = files.map((file) => {
                return {
                    actions: null,
                    file: file,
                    // content: imageData[file.fileId] ? imageData[file.fileId] : null,
                    previewData: imageData[file.fileId] ? imageData[file.fileId] : null,
                }
            });
            setTableData(tableData);
        }
    }, [files, imageData]);

    const [downloadFile, downloaderComponentUI] = useFileDownloader();
    const download = (file : FileForEvent) => {
        if(downloadFile instanceof Function){
            downloadFile(file);
        }
    };

    const columns = useMemo<MRT_ColumnDef<FileTableRowType>[]>( //TS helps with the autocomplete while writing columns
    () => [
            {accessorKey: 'actions', header: 'Actions', enableColumnFilter: false, enableSorting: false,
                Cell: ({ cell, row }) => (
                    <div className="d-flex">
                        {/* <Tooltip title={<Typography>View</Typography>}>
                            <span>
                                <IconButton aria-label="view"
                                disabled={false}
                                onClick={() => {
                                    setCurrentFile(row.original.file)
                                    setOpenPopup("VIEW")
                                }}
                                >
                                <FindInPage />
                                </IconButton>
                            </span>
                        </Tooltip> */}

                        <Tooltip title={<Typography>Download</Typography>}>
                            <span>
                                <IconButton aria-label="download"
                                disabled={false}
                                onClick={() => {download(row.original.file)}}
                                >
                                <Download />
                                </IconButton>
                            </span>
                        </Tooltip>

                        <Tooltip title={<Typography>Edit</Typography>}>
                            <span>
                                <IconButton aria-label="edit"
                                    disabled={false}
                                    onClick={() => {
                                        setCurrentFile(row.original.file)
                                        setOpenPopup("EDIT")
                                    }
                                    }
                                >
                                    <Edit />
                                </IconButton>
                            </span>
                        </Tooltip>

                        <Tooltip title={<Typography>Delete</Typography>}>
                            <span>
                                <IconButton aria-label="delete" 
                                disabled={false}
                                onClick={() => {
                                    setCurrentFile(row.original.file)
                                    setOpenPopup("DELETE")
                                }}>
                                <Delete />
                                </IconButton>
                            </span>
                        </Tooltip>
                    </div>
                ),
            },
            {
                accessorFn: (row) => row.file.filename,
                id: "fileName",
                header: 'Name',
            },
            {
                accessorFn: (row) => row.file.shortDesc,
                id: "shortDesc",
                header: 'Description',
            },
            {
                accessorFn: (row) => row.file.fileType,
                id: "fileType",
                header: 'Type',
                Cell: ({ cell, row }) => (
                    <Typography>{PrintFileType(cell.getValue<FileType>())}</Typography>
                )
            },
            {
                accessorFn: (row) => row.file.size,
                id: "size",
                header: 'Size',
                Cell: ({ cell, row }) => (
                    <Typography>{formatBytes(cell.getValue<number>())}</Typography>
                )
            },
            {
                accessorFn: (row) => row.file.uploadDate,
                id: "uploadDate",
                header: 'Upload Date',
                Cell: ({ cell, row }) => (
                    <Typography>{cell.getValue<string>() ? format(Date.parse(cell.getValue<string>()), "PPpp") : null}</Typography>
                ),
            },
            {
                accessorFn: (row) => row.file.startDate,
                id: "startDate",
                header: 'Start Date',
                Cell: ({ cell, row }) => (
                    <Typography>{cell.getValue<string>() ? format(Date.parse(cell.getValue<string>()), "PPpp") : null}</Typography>
                ),
            },
            {
                accessorFn: (row) => row.file.endDate,
                id: "endDate",
                header: 'End Date',
                Cell: ({ cell, row }) => (
                    <Typography>{cell.getValue<string>() ? format(Date.parse(cell.getValue<string>()), "PPpp") : null}</Typography>
                ),
            },
        ],[],
    );

    function renderImage(image : string | null){
        if(image){
            return <img src={image} alt="Preview" className="img-fluid" style={{maxWidth:"100%", maxHeight:"100%"}}/>
        }
        else{
            return <img src="https://via.placeholder.com/150" alt="Preview" className="img-fluid" style={{maxWidth:"100%", maxHeight:"100%"}}/>
        }
    }

    function fetchImages(files : FileForEvent[]){
        for(const file of files){
            if(IsImageType(file.fileType)){
                dispatch(FileAPI.fetchImage(Date.now(), file.fileId))
            }
        }
    }

    //Show the content of the file with a new column
    // if(!columns.find(c => c.header === "Content")){
    //     for(const file of files){
    //         if(IsImageType(file.fileType)){
    //             columns.push({
    //                 accessorKey: 'content', header: 'Content', enableColumnFilter: false, enableSorting: false,
    //                 Cell: ({ cell, row }) => (
    //                     <>{renderImage(cell.getValue<string>())}</>
    //                 )
    //             })
    //             break
    //         } else if(FileType.FEATURE === file.fileType || FileType.CONFIG === file.fileType){
    //             columns.push({
    //                 accessorKey: 'content', header: 'Content', enableColumnFilter: false, enableSorting: false,
    //                 Cell: ({ cell, row }) => (
    //                     <Button id="actionBar2" variant="primary" size="sm" onClick={()=>{setOpenContent(true); setContentData(file.content)}}>See Content</Button>
    //                 )
    //             })
    //             break
    //         }
    //     }
    // }
    if(!columns.find(c => c.header === "Preview")){
        for(const file of files){
            if(IsImageType(file.fileType)){
                columns.push({
                    accessorKey: 'previewData', header: 'Preview', enableColumnFilter: false, enableSorting: false,
                    Cell: ({ cell, row }) => (
                        <>{renderImage(cell.getValue<string>())}</>
                    )
                })
                break;
            }
        }
    }

    useEffect(() => {
        if (typeof window !== 'undefined') {
            setIsLoading(false);
        }

        fetchImages(files)

    }, []);

    return(
        <div className="form-control mb-2"> 
            <>
            <div className="assetBar">

                <div className="d-flex justify-content-between">
                    {/* Left button section */}
                    <Typography style={{fontFamily:'"Quicksand", "Arial", "sans-serif"',
                                    fontWeight:600, fontSize:'1.5rem', paddingTop:"0.5rem"}} component="span" className="mb-0">
                        {titleIcon ? titleIcon : null}
                        {title ? title : "Files"}
                    </Typography>

                    <div className="d-flex">
                        <Button id="actionBar1" variant="outline-primary" size="sm" onClick={(e) => {
                            setOpenPopup("UPLOAD");               
                        }}> 
                            <Upload style={{marginRight: '8px'}}/>
                            Upload
                        </Button>

                        <Button id="actionBar2" variant="outline-primary" size="sm" style={{marginLeft: "0.5rem"}} onClick={() => {
                            setIsLoading(true);
                            dispatch(FileAPI.fetchFiles())
                            fetchImages(files)
                            new Promise(function(resolve, reject) {
                                setTimeout(() => resolve("done"), 500);
                            })
                            .then(function(result){
                                setIsLoading(false);
                            })

                        }}>
                            <Refresh />
                            Refresh
                        </Button>
                    </div>
                </div>
            </div>
        
            {files.length > 0 ?
                <MaterialReactTable
                    columns={columns}
                    data={tableData}
                    enableBottomToolbar={false}
                    enableGlobalFilterModes
                    enableColumnFilters={false}
                    enablePagination={false}
                    enableDensityToggle={false}
                    enableRowVirtualization
                    initialState={{columnVisibility: {storeId: false, pluCount: false,totalMemory: false,freeMemory: false }}}
                    muiTableContainerProps={{ sx: { maxHeight: '800px' } }}
                    onSortingChange={setSorting}
                    state={{ isLoading, sorting }}
                    rowVirtualizerInstanceRef={rowVirtualizerInstanceRef} //optional
                    rowVirtualizerProps={{ overscan: 8 }} //optionally customize the virtualizer
                /> :
                <label className="noDataLabel">No Data</label>
            }

            {openPopup === "UPLOAD" &&
                <UploadFiles
                    title="Upload a File"
                    categories={fileTypes}
                    openPopup={openPopup === "UPLOAD"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "UPLOAD" : null)}
                    profiles={currentProfile ? [currentProfile] : []}
                    selectValue = {currentProfile ? currentProfile.name : ""}
                    onlyValue= {true}
                />
            }

            {downloaderComponentUI}
            {/*
            {openPopup === "VIEW" &&
                <ViewFiles
                    categories={fileTypes}
                    title="View File"
                    openPopup={openPopup === "VIEW"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "VIEW" : null)}
                    currentFile={currentFile ?? getDefaultFile()}
            />
            }
            */}

            {openPopup === "EDIT" && 
                <EditFiles
                    categories={fileTypes}
                    title="Edit File"
                    openPopup={openPopup === "EDIT"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "EDIT" : null)}
                    currentFile={currentFile ?? getDefaultFile()}
                />
            }

            {openPopup === "DELETE" &&
                <DeleteFiles 
                    title="Delete File"
                    openPopup={openPopup === "DELETE"}
                    setOpenPopup={(input: boolean) => setOpenPopup(input ? "DELETE" : null)}
                    file={currentFile}
                />
            }
            </>

            <Dialog open={openContent} onClose={()=>setOpenContent(false)} maxWidth="md" fullWidth={true}>
                <DialogTitle>
                    <Typography variant="h5" component="div">File Content</Typography>
                </DialogTitle>

                <DialogContent>
                    <p>{contentData != null ?
                        contentData
                    :
                        "Content is empty or is not a parsable type"
                    }</p>

                    <ButtonGroup className="formButtons">
                        <Button className="formButton1" variant="primary" size="sm" onClick={()=>setOpenContent(false)}>Close</Button>
                    </ButtonGroup>
                </DialogContent>
            </Dialog>
        </div>
    )
}