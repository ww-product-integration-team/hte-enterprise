import { useState} from "react";
import { v4 as uuid } from "uuid";
import { FileForEvent } from "../../../types/asset/FileTypes";
import { TransactionRequest } from "../../../types/transaction/transactionTypes";

import Downloader from "./Downloader";

export interface DownloadFileObj{
    transactionFile ?: TransactionRequest
    profileFile ?: FileForEvent
    downloadId: string
}

function isTransactionReq(file : FileForEvent | TransactionRequest) : boolean{
    return (file as TransactionRequest).transData
}

export const useFileDownloader = () => {
    const [files, setFiles] = useState<DownloadFileObj[]>([]);

    const download = (file : FileForEvent | TransactionRequest) =>{
        if(isTransactionReq(file)){
            let fileToDownload : DownloadFileObj = {
                transactionFile: {...file} as TransactionRequest,
                downloadId: uuid()
            }

            setFiles((fileList) => [...fileList, fileToDownload]);
        }
        else{
            let fileToDownload : DownloadFileObj = {
                profileFile: {...file} as FileForEvent,
                downloadId: uuid()
            }

            setFiles((fileList) => [...fileList, fileToDownload]);
        }
        
    }
        
        
    const remove = (removeId : string) =>
        setFiles((files) => [
            ...files.filter((file) => file.downloadId !== removeId),
        ]);

    return [
        (e : FileForEvent | TransactionRequest) => download(e),
        files.length > 0 ? (
        <Downloader files={files} remove={(downloadId : string) => remove(downloadId)} />
        ) : null,
    ];
};
