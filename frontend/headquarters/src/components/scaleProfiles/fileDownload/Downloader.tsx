import { useEffect, useState } from "react";
import { useDispatch } from "react-redux"
import { ProgressBar } from "@themesberg/react-bootstrap";
import axios from 'axios';
import logger from "../../utils/logger";
import "../../styles/FileDownload.css";
import { FileAPI, TransAPI } from '../../api';
import { useRspHandler } from '../../utils/ResponseProvider';
import { TransactionRequest } from "../../../types/transaction/transactionTypes";
import { DownloadFileObj } from "./FileDownloader";
import { ResponseType } from "../../../types/storeTypes";
import { FileForEvent } from "../../../types/asset/FileTypes";

function isTransactionReq(file : FileForEvent | TransactionRequest) : boolean{
    return (file as TransactionRequest).transData
}

interface DownloadItemProps{
    file?: FileForEvent | TransactionRequest,
    removeFile : () => void
    key : number
}

export interface DownloadItemOptions{
    onDownloadProgress: (progressEvent: {loaded: number, total: number}) => void
}

interface DownloaderProps{
    files: (DownloadFileObj)[]
    remove : (downloadId : string) => void
}
const Downloader = (props : DownloaderProps) => {
    const { files = [], remove } = props
    return (
        <div className="downloader">
            <div className="card">
                <div className="card-header">File Downloader</div>
                <ul className="list-group list-group-flush">
                {files.map((file, idx) => (
                    <DownloadItem
                        key={idx}
                        removeFile={() => remove(file.downloadId)}
                        file={file.profileFile ?? file.transactionFile}
                    />
                ))}
                </ul>
            </div>
        </div>
    );
};

const DownloadItem = (props: DownloadItemProps) => {
    const{file, removeFile} = props
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();

    const [controller] = useState(axios.CancelToken.source())    // To prevent memory leak onPageChange
    const [downloadInfo, setDownloadInfo] = useState({
        progress: 0,
        completed: false,
        total: 0,
        loaded: 0,
    });

    const formatBytes = (bytes : number) => `${(bytes / (1024 * 1024)).toFixed(2)} MB`;

    useEffect(() => {
        if(!file){
            logger.error("DownloadItem - Cannot Download item, file is undefined!")
            return
        }

        const options : DownloadItemOptions = {
            onDownloadProgress: (progressEvent : {loaded: number, total: number}) => {
                const { loaded, total } = progressEvent;

                setDownloadInfo({
                    progress: Math.floor((loaded * 100) / total),
                    loaded,
                    total,
                    completed: false,
                });
            },
        };
    
        logger.info(`filename: ${file.filename}`)
        let reqDownloadID = Date.now()
        
        // ==================   CALLBACK RELATED CODE    ==================
        // onSuccess() and onFail() are executed depending on if the API call was successful or not.

        function onSuccess(response : ResponseType){
            logger.info('Backend Response (Downloader.jsx): ', response);

            const url = window.URL.createObjectURL(new Blob([response.response]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', file!.filename); //or any other extension
            document.body.appendChild(link);
            link.click()

            setDownloadInfo((info) => ({
                ...info,
                completed: true,
            }));

            setTimeout(() => {
                removeFile();
            }, 4000);
        }
        
        function onFail(response : ResponseType){
            logger.error("(Downloader) Error downloading file", response)
            setTimeout(() => {
                removeFile();
            }, 4000);
        }
        
        if(file && isTransactionReq(file)){
            let transReq : TransactionRequest = file as TransactionRequest
            if(addCallback(callbacks, reqDownloadID, "Transaction Downloader", onSuccess, onFail, {successMsg:"Transactions successfully downloaded!", failMsg:"File could not be downloaded!"})){
                dispatch(TransAPI.downloadTransactions(reqDownloadID, transReq, options, controller))
            }
        }
        else{
            let fileReq : FileForEvent = file as FileForEvent
            if(addCallback(callbacks, reqDownloadID, "Downloader", onSuccess, onFail, {successMsg:"File successfully downloaded!", failMsg:"File could not be downloaded!"})){
                dispatch(FileAPI.fileDownload(reqDownloadID, fileReq.fileId, options, controller))
            }
        }
        
    }, []);

    if(!file){
        logger.error("DownloadItem - Cannot Download item, file is undefined!")
        return null
    }


    return (
        <li className="list-group-item">

            <div className="row">
                <div className="downloadPopup">
                    <div className="downloadFileName">  {file.filename}  </div>

                    <div className="downloadFileSize">
                        {downloadInfo.loaded > 0 && (
                            <>
                            {/* Green text */}
                            <span className="text-success">         
                                {formatBytes(downloadInfo.loaded)}
                            </span>
                                / {formatBytes(downloadInfo.total)}
                            </>
                        )}

                        {/* Download hasn't started yet */}
                        {downloadInfo.loaded === 0 && <>Initializing...</>}
                    </div>

                    {/* Finished downloading message */}
                    <div className="downloadStatus">
                        {downloadInfo.completed && (<span className="text-success">Completed</span>)}
                    </div>
                </div>

                {/* React-Bootstrap progress bar */}
                <div className="downloadProgress">
                    <ProgressBar
                        variant="success"
                        now={downloadInfo.progress}
                        label={`${downloadInfo.progress}%`}
                    />
                    <hr className="fileSeparator" />
                </div>
            </div>
        </li>
    );
};

export default Downloader;