import { useState, useEffect, useMemo } from 'react';
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { Button, ButtonGroup, Breadcrumb} from '@themesberg/react-bootstrap';
import { AddCircle, FindInPage, Help } from '@material-ui/icons';
import { Form } from '@themesberg/react-bootstrap';
import { UploadFiles } from './FileForm';
import { PostProfile, EditProfile, DeleteProfile }  from '../forms/ProfileForm';
import PricingZoneActions, { DeletePricingZone, EditPricingZone, PostPricingZone } from '../forms/PricingZoneForm'
import { Dialog, DialogContent, DialogTitle, IconButton, Tooltip, Typography } from '@material-ui/core';
import { useAppSelector } from '../../state';
import logger from '../utils/logger';
import { DEFAULT_ADMIN_DOMAIN_ID, DEFAULT_PROFILE_DOMAIN_ID, DEFAULT_UNASSIGNED_DOMAIN_ID } from '../../state/actions/accessActions';
import { ChecksumLog, getDefaultProfile, Profile } from '../../types/asset/ProfileTypes';
import { Delete, Download, Edit, PhotoLibrary, SettingsApplicationsRounded, Terminal } from '@mui/icons-material';
import { FilesView } from './FilesView';
import { FileAPI, PricingZoneAPI, ProfileAPI } from '../api';
import { useDispatch } from 'react-redux';
import { Alert } from '@mui/material';
import { FileType } from '../../types/asset/FileTypes';
import VirtualTable, { HeadCellTypes } from '../common/virtualTable/VirtualTable';
import MaterialReactTable, { MRT_ColumnDef } from 'material-react-table';
import { format } from 'date-fns';
import AccessControl from '../common/AccessPermissions';
import * as AccessActions from '../../state/actions/accessActions'
import { LicenseCheck } from './LicenseCheck';
import { Col, Row } from 'react-bootstrap';
export default function ScaleProfiles() {
    
    const profileState = useAppSelector((state) => state.profile);
    const dispatch = useDispatch();    
    const user = useAppSelector((state) => state.access.account);
    const [currentProfile, setCurrentProfile] = useState<Profile | undefined>(undefined);   // Chosen Profile (Select tag)
    const pricingZones = useAppSelector(state=>state.asset.pricingZones)
    const reduxZonePricing = useAppSelector(state=>state.config.zonePricing.zonePricing)
    const [zonePricing, setZonePricing] = useState(reduxZonePricing === "true")
    const roles = useAppSelector((state)=> state.access.roles)
    const domains = useAppSelector(state=>state.access.domains)

    const [openUpload, setOpenUpload] = useState(false);                // Upload   Files/Licenses
    const [openAdd, setOpenAdd] = useState(false);                      // Create   Profile
    const [openEdit, setOpenEdit] = useState(false);                    // Edit     Profile
    const [openDelete, setOpenDelete] = useState(false);                // Delete   Profile
    const [openChecksumLogs, setOpenChecksumLogs] = useState(false);
    const [openZoneAction, setOpenZoneAction] = useState(false)
    const [zoneActionType, setZoneActionType] = useState<"create"|"edit"|"delete">("create")
    // const [openUpload, setOpenUpload] = useState(false);             // Upload a License

// ==================   PERMISSIONS CHECKING   ==================
// Everything to do with permissions / roles

    // const handlePermissionCheck = () => {
    //     try {
    //         // Role 2001 (ManageProfiles)
    //         if(userPermissions >= 3000 || currentAccount.domain.domainId === DEFAULT_ADMIN_DOMAIN_ID) {
    //             dispatch(FileAPI.fetchFiles())
    //             dispatch(ProfileAPI.fetchProfiles())
    //         }
    //         else{
    //             logger.error("(ScaleProfiles) not successful");
    //             history.push("/NotPermitted")
    //         }
            
    //     } catch (error) {
    //         // The catch block gets ran on page load, bootleg check to see if user is logged in
    //         if (loggedIn === false) {
    //           history.push("/login")
    //         }
    //     }
    // }

// ==================   SEARCHDOMAIN()   ==================
// Used to display the domain of the currently selected scale profile
function searchDomain(id: string) {
    if (id === AccessActions.DEFAULT_ADMIN_DOMAIN_ID) {
        return "Admin Only"
    }
    if (id === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID) {
        return "Unassigned (Admin Only)"
    }
    let domain = ""
    for (let key of Object.keys(domains)) {
        if (key === id) {
            domain = domains[key].name + " (" + domains[key].type + ")"
            break
        }
    }
    return domain
}

// =======================================================

    function renderProfileData(){
        if(!currentProfile || currentProfile.profileId === "") return null

        if(currentProfile.profileId === DEFAULT_PROFILE_DOMAIN_ID){
            return (
                <div className="text-center">
                    <Typography variant="h5" className="mb-4">Unassigned Scale Profile</Typography>
                    <Alert className="m-1" variant="outlined" severity="warning">
                        <p className="fw-bold"> Notice: This is a default profile used for unassigned scales. This profile cannot be edited or removed. </p>
                    </Alert>
                </div>
            )
        }

        return (
            <>
                <FilesView
                    title='Configuration Files'
                    titleIcon={<SettingsApplicationsRounded style={{marginRight:"0.5rem"}} fontSize='large'/>}
                    fileTypes={[FileType.CONFIG]}
                    files={profileState.files.filter(file => file.profileId === currentProfile.profileId)}
                    currentProfile={currentProfile}
                />    

                <FilesView
                    title='Media Files'
                    titleIcon={<PhotoLibrary style={{marginRight:"0.5rem"}} fontSize='large'/>}
                    fileTypes={[FileType.VIDEO, FileType.AUDIO, FileType.PLAYLIST, FileType.LAYOUT, FileType.FONT, FileType.DOCUMENT, 
                            FileType.IMAGE_CATEGORY,FileType.IMAGE_PRODUCT,FileType.IMAGE_PLANOGRAM,FileType.IMAGE_SELFSERVICE,
                            FileType.IMAGE_SOFTKEY,FileType.IMAGE_SPECIAL,FileType.IMAGE_LAYOUT,FileType.IMAGE_ONLINE_ORDERING,
                            FileType.IMAGE_NOW_SERVING]}
                    files={profileState.mediaFiles.filter(file => file.profileId === currentProfile.profileId)}
                    currentProfile={currentProfile}
                />

                <FilesView  
                    title='Feature Files'
                    titleIcon={<Terminal style={{marginRight:"0.5rem"}} fontSize='large'/>}
                    fileTypes={[FileType.FEATURE]}
                    files={profileState.featureFiles.filter(file => file.profileId === currentProfile.profileId)}
                    currentProfile={currentProfile}
                /> 

                <LicenseCheck
                    title='License File'
                    titleIcon={<Terminal style={{marginRight:"0.5rem"}} fontSize='large'/>}
                    fileTypes={[FileType.LICENSE]}
                    files={profileState.licenseFiles.filter(file => file.profileId === currentProfile.profileId)}
                    currentProfile={currentProfile}
                />
            </>
        )
    }

    const handleView = (e : React.FormEvent<HTMLSelectElement>) => {
        const profile = profileState.profiles[e.currentTarget.options.selectedIndex - 1];
        setCurrentProfile(profile);
    };

    const disableKey = (e) => {
       const ev = e ? e : window.event;
    
       if (ev) {
           if   (ev.preventDefault)     ev.preventDefault();
           else                         ev.returnValue = false;         
       }    
    }

    useEffect(() => {
        //TODO Add this back in when we are storing attributes locally
        // handlePermissionCheck()
        dispatch(ProfileAPI.fetchProfiles())
        dispatch(FileAPI.fetchFiles())
        dispatch(ProfileAPI.getChecksumLogs(Date.now()))
        dispatch(PricingZoneAPI.getPricingZones())
    }, []);

    useEffect(() => {
        // if (profiles.length !== 0)  logger("All Scale Profiles: ", profiles)

        if(currentProfile == null){return}
        setCurrentProfile(profileState.profiles.find(profile => {
            return profile.profileId === currentProfile.profileId
        }))

    }, [profileState.profiles]);

    useEffect(() => {
        if (currentProfile)     logger.info("(ScaleProfiles) Current Profile: ", currentProfile.name)
    }, [currentProfile]);

    useEffect(() => {
        setZonePricing(reduxZonePricing === "true")
    }, [reduxZonePricing])

    const checksumLogHeaders: HeadCellTypes[] = [
        {id: 'date', sorted: true, searchable: false, label: 'Date and Time', isShowing: true, mandatory: true},
        {id: 'scaleIp', sorted: true, searchable: false, label: "IP Address", isShowing: true, mandatory: true},
        {id: 'logEntry', sorted: false, searchable: false, label: "Log Entry", isShowing: true, mandatory: true}
    ]
    const checksumColumns = useMemo<MRT_ColumnDef<ChecksumLog>[]>(()=>[
        {
            accessorKey: 'date',
            header: 'Date and Time'
        },
        {
            accessorKey: 'scaleIp',
            header: 'IP Address'
        },
        {
            accessorKey: 'logEntry',
            header: 'Log Entry',
            enableColumnFilter: false,
            enableSorting: false,
            minSize: 600
        }
    ],[])
    const reduxChecksumLogs = useAppSelector(state=>state.profile.checksumLogs)
    const [checksumLogs, setChecksumLogs] = useState(reduxChecksumLogs)
    useEffect(()=>{
        setChecksumLogs(reduxChecksumLogs)
    },[reduxChecksumLogs])
    const handleChecksumView = () => {
        dispatch(ProfileAPI.getChecksumLogs(Date.now()))
        setOpenChecksumLogs(true)
    }
    const handleDownload = () => {
        let csvData = "data:text/csv;charset=utf-8,Date and Time,IP Address,Log Entry,\r"
        for (let log of checksumLogs) {
            csvData += log.date + ","
            csvData += log.scaleIp + ","
            csvData += log.logEntry + ",\r"
        }

        const encodedUri = encodeURI(csvData)
        const link = document.createElement("a")
        link.setAttribute("href", encodedUri)
        link.setAttribute("download", "checksumLogs_" + format(Date.now(),"MMddyyyy")+".csv")
        document.body.appendChild(link)
        link.click()
    }

    useEffect(() => {
        profileState.profiles.sort((a,b) => a.name.localeCompare(b.name, 'en', {numeric: true}))
    }, [profileState.profiles])

    return (
        <>
        <AccessControl
            user={user}
            requiredRole={roles[AccessActions.MANAGE_PROFILES]}
            pageRequest = {true}
        >
        <div className="mb-4 mb-md-0">
            <>
                <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                    <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                    <Breadcrumb.Item active>Scale Profiles</Breadcrumb.Item>
                </Breadcrumb>

                <h2>Scale Profiles</h2>
                <p className="mb-4">Here are the uploaded files in each defined profile.</p>

                {/* Scale Profile Actions */}
                <ButtonGroup className="assetBar" style={{marginRight:".5rem"}}>
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={() => {setOpenAdd(true)}}>
                        <AddCircle />
                        Create Profile
                    </Button>
                </ButtonGroup>

                {zonePricing &&
                    <>
                        <PricingZoneActions
                            setOpenPopup={setOpenZoneAction}
                            setPopupType={setZoneActionType}
                        />
                        {openZoneAction &&
                            <>
                                {zoneActionType === "create" &&
                                    <PostPricingZone
                                        title="Create a Zone"
                                        openPopup={openZoneAction}
                                        setOpenPopup={setOpenZoneAction}
                                    />
                                }
                                {zoneActionType === "edit" &&
                                    <EditPricingZone
                                        title="Edit a Zone"
                                        openPopup={openZoneAction}
                                        setOpenPopup={setOpenZoneAction}
                                    />
                                }
                                {zoneActionType === "delete" &&
                                    <DeletePricingZone
                                        title="Delete a Zone"
                                        openPopup={openZoneAction}
                                        setOpenPopup={setOpenZoneAction}
                                    />
                                }
                            </>
                        }
                    </>
                }

                <ButtonGroup className="assetBar">
                <Tooltip placement="top" title={<Typography variant="body1" component="div">View data on scales that reported as out-of-sync</Typography>}>
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={handleChecksumView}>
                        <FindInPage />
                        View Checksum Logs
                    </Button>
                    </Tooltip>

                    <Tooltip placement="top" title={<Typography variant="body1" component="div">Download data for reportedly out-of-sync scales</Typography>}>
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={handleDownload}>
                        <Download />
                        Download Checksum Logs
                    </Button>
                    </Tooltip>
                </ButtonGroup>
            </>
        </div>

        <Form >
            <div className="form-control mb-2"> 
                <Row>
                    <Col xs={2}>
                        <Form.Label>Scale Profile:</Form.Label>
                    </Col>
                    {zonePricing ?
                        <Col xs={2}>
                            <Form.Label>Pricing Zone:</Form.Label>
                        </Col>
                    :
                        null
                    }
                    <Col xs className="d-flex justify-content-sm-end">
                        {currentProfile && currentProfile.profileId !== "" ?
                            <ButtonGroup className="assetBar">
                                <Button id="actionBar2" variant="outline-primary" size="sm" disabled={currentProfile.profileId === DEFAULT_PROFILE_DOMAIN_ID} style = {{margin: 0}} onClick={() => {setOpenEdit(true)}}>
                                    <Edit />
                                    Edit Profile
                                </Button>

                                <Button id="actionBar2" variant="outline-primary" size="sm" disabled={currentProfile.profileId === DEFAULT_PROFILE_DOMAIN_ID} style = {{margin: 0}} onClick={() => {setOpenDelete(true)}}>
                                    <Delete />
                                    Delete Profile
                                </Button>
                            </ButtonGroup>
                        : null}
                    </Col>
                </Row>
                <Row>
                    <Col xs={2}>
                        <Form.Select className="scaleProfileSelector flex-1 flex-grow-0" required defaultValue=""
                            onChange={(e) => handleView(e)}
                            onKeyDown={(e) => disableKey(e)}
                            value={currentProfile == null ? "" : currentProfile.profileId}
                        >
                            <option value="" disabled>--Select a profile--</option>
                            
                            {profileState.profiles.map(profile => (
                                <option key={profile.profileId} value={profile.profileId}>
                                    {profile.name}
                                </option>
                            ))}
                        </Form.Select>
                    </Col>
                    <Col xs={2}>
                        {zonePricing ?
                            <Form.Control
                                as="input"
                                readOnly
                                value={currentProfile == null || currentProfile.profileId === "00000000-0000-0000-0000-000000000000" || !currentProfile.zoneId ? 
                                    "No Zone Selected" : pricingZones[currentProfile.zoneId].name}
                            />
                        :
                            null
                        }
                    </Col>
                    <Col className="d-flex justify-content-sm-end">
                        <div style={{display: 'flex', flexWrap: 'wrap', alignContent: 'flex-end'}}>
                            <Typography component="span">
                                <b>Domain:</b> {searchDomain(currentProfile == null ? "" : currentProfile.requiresDomainId) ?? "No Scale Profile Selected"}
                            </Typography>
                        </div>
                    </Col>
                </Row>
            </div>

            {renderProfileData()}

                {/* TODO: Implement this as a 2nd table underneath the current table in ViewHT */}
                {/* {currentProfile !== "" && <Licenses profileId={currentProfile}/>} */}
                
            </Form>

            {openAdd &&
                <PostProfile
                    title="Create Scale Profile"
                    openPopup={openAdd}
                    setOpenPopup={setOpenAdd} 
                    setProfile={setCurrentProfile}
                />
            }

            {openEdit &&
                <EditProfile
                    title="Edit Existing Scale Profile"
                    openPopup={openEdit}
                    setOpenPopup={setOpenEdit}
                    profile={currentProfile ?? getDefaultProfile()}
                    onlyValue={true} 
                    selectValue={''}
                />
            }

            {openDelete && 
                <DeleteProfile
                    title="Delete Scale Profile"
                    openPopup={openDelete}
                    setOpenPopup={setOpenDelete}
                    profiles={profileState.profiles}
                    profile={currentProfile ?? getDefaultProfile()}
                    onlyValue={true}
                    selectValue={''}
                />
            }

            {openUpload &&
                <UploadFiles
                    title="Upload a File"
                    categories={[FileType.CONFIG]}
                    openPopup={openUpload}
                    setOpenPopup={setOpenUpload}
                    profiles={currentProfile ? [currentProfile] : []}
                    selectValue={currentProfile ? currentProfile.name : ""}
                    onlyValue={true}
                />
            }

            <Dialog open={openChecksumLogs} onClose={()=>setOpenChecksumLogs(false)} fullWidth={true} maxWidth="lg">
                <DialogTitle>
                    <Typography variant="h5" component="div">
                        Checksum Logging
                        <Tooltip className="info" title={
                            <Typography>All logs are of times when scales and their checksums did not match with their Profiles</Typography>
                        }>
                            <IconButton size ="small" style = {{marginBottom: "5px"}} disableRipple>
                                <Help />
                            </IconButton>
                        </Tooltip>
                    </Typography>
                </DialogTitle>

                <DialogContent>
                    <VirtualTable
                        tableName={"profileChecksumLogsTable"}
                        saveKey="profileChecksumLogsTableHead"
                        dataSet={checksumLogs}
                        headCells={checksumLogHeaders} 
                        initialSortedBy={{name: ''}}
                        useToolbar={false}
                        maxHeight='600px'
                    />
                    {/* <MaterialReactTable thinking about using pagination but virtualization for now
                        columns={checksumColumns}
                        data={checksumLogs}
                        enableTopToolbar={false}
                    /> */}

                    <div className="formButtons">
                        <button className="formButton1" type="button" onClick={()=>setOpenChecksumLogs(false)}>
                            Close
                        </button>
                    </div>
                </DialogContent>
            </Dialog>
            </AccessControl>
        </>   
    );
}
