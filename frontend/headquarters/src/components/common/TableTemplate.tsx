import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'

import {Toolbar, IconButton, Menu} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@mui/icons-material/Edit';
import ViewListIcon from '@mui/icons-material/ViewList';
import DownloadIcon from '@mui/icons-material/Download';
import FilterListIcon from '@material-ui/icons/FilterList';
import RefreshIcon from '@mui/icons-material/Refresh';

import RegionIcon from '@material-ui/icons/RoomOutlined';
import StoreIcon from '@material-ui/icons/Store';
import DepartmentIcon from '@material-ui/icons/LocalGroceryStoreOutlined';
import ScaleIcon from '../icons/ScaleIcon';
import ErrorIcon from '@material-ui/icons/ErrorOutline';

import { Form, FormCheck, Button} from '@themesberg/react-bootstrap';
import moment from 'moment';
import {useDispatch } from "react-redux"
import { RootStore, store} from "../../state";

import "../styles/TableStyles.css"
import logger from '../utils/logger';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { AnyAction, Dispatch } from 'redux';
import { HTeUser } from '../../types/access/AccessTypes';
import { DEFAULT_ADMIN_DOMAIN_ID } from '../../state/actions/accessActions';
import { formatBytes } from '../utils/utils';
import { PrintFileType } from '../../types/asset/FileTypes';


export interface TableTemplateHeadCellType{ 
    id: string
    narrow: boolean
    disablePadding: boolean
    label: string
    mandatory: boolean
}

/*   TODO:  issue: Search bar only works with what is rendered, 
              ex. (viewing 5 rows, search only works for those 5 rows right now) 
*/

/*  ==============   PROPS / EXAMPLE USAGE   ==============
  <TableTemplate 
    recordId="id"                               // Unique Key for each record in table
    dispatchedUrls={dispatchedUrls}             // What is dispatched
    className={"max-height-750"}                // Extra custom styling on the table
    tableName={tableName}                       // Header / Name
    tableDescription={tableDescription}         // Subheader
    customRowsPerPage={25}                      // # of rows shown by default

    searchPlaceholder="Search for a user"       // Placeholder in Search Bar
    searchFilter="firstName"                    // The column being searched
    defaultOrderBy="firstName"                  // Default ordering by this column name
    currentUser={currentUser}                   // Account currently logged in

    headCells={headCells}                       // Column headers
    dataSet={filteredUserList}                  // Array of all data from backend
    treeState={treeState}                       // Full Asset Tree

    displayRoles={handleDisplayRoles}           // Display a user's roles button for UserManagement.jsx
    handleEditFile={handleEditFile}             // Edit File function used in ViewHT
    handleDeleteFile={handleDeleteFile}         // Delete File function used in ViewHT
    download={download}                         // Download function used in ViewHT
    actionDisable={actionDisable}               // For checking user's domain against scale profile ID
  />
*/


// ================== SORTING BLOCK ==================
// Controls how the sorting works when clicking on column headers

/*
  Here's some documentation on what this does and how this works.
  
    descendingComparator(a, b, orderBy, allAssets) 
      a =           value in cell a
      b =           value in cell b
      orderBy =     headCells[id]   |   Which is the id of the column header (ex. type, size, assignedBanner, etc.)
      allAssets =   data from redux store all put into an object to save space since it is passed several times as a prop


    The first if() statement is the general statement that should work assuming the headCell.id is the same as the name used in the database.
    The else if() statements are hard-coded because the property that needs to be sorted does not have the same name as the "headCell.id"
      ex. "properties.pluCount" is how you reach the actual data, but
          "pluCount" is the headCell.id

    The ("labelsPrinted") and ("pluCount") have their own sort / return within the else if statements so they can check using numerical values rather than Strings.
*/

function descendingComparator(a : any, b : any, orderBy : string) {

    let aSortName : string | null = "";     // Used to hold the Sorted Name

    let bSortName : string | null = "";     // Used to hold the Sorted Name

    // ==================   NUMERICAL SORTING IF() / ELSE IF()    ==================
    // The below statements have their own return statements so it sorts using numerical values rather than String values.

    if (orderBy === "size") {
        aSortName = a.size
        bSortName = b.size

        // numerical sort
        if (aSortName !== null && bSortName !== null) {
        if (bSortName < aSortName)        return -1;
        if (bSortName > aSortName)        return 1;
        }
    }

    else if (orderBy === "totalLabelsPrinted") {
        aSortName = a.properties.totalLabelsPrinted
        bSortName = b.properties.totalLabelsPrinted

        // numerical sort
        if (aSortName !== null && bSortName !== null) {
        if (bSortName < aSortName)        return -1;
        if (bSortName > aSortName)        return 1;
        }
    }

    else if (orderBy === "pluCount") {
        aSortName = a.properties.pluCount
        bSortName = b.properties.pluCount

        // numerical sort
        if (aSortName !== null && bSortName !== null) {
        if (bSortName < aSortName)        return -1;
        if (bSortName > aSortName)        return 1;
        }
    }

    // ==================================
    // The below cases just have data references that don't match their "headcell.id"
    // ex. "properties.pluCount" is how you reach the actual data, but
    // "pluCount" is the headCell.id

    else if (orderBy === "assignedBanner") {
        aSortName = a.assignedBanner ?? ""
        bSortName = b.assignedBanner ?? ""
    }

    else if (orderBy === "assignedRegion") {
        aSortName = a.assignedRegion ?? ""
        bSortName = b.assignedRegion ?? ""
    }

    else if (orderBy === "assignedStore") {
        aSortName = a.assignedStore ?? ""
        bSortName = b.assignedStore ?? ""
    }

    else if (orderBy === "assignedDept") {
        aSortName = a.assignedDept ?? ""
        bSortName = b.assignedDept ?? ""
    }

    else if (orderBy === "hostname") {
        aSortName = a.properties.hostname
        bSortName = b.properties.hostname
    }

    else if (orderBy === "application") {
        aSortName = a.properties.application
        bSortName = b.properties.application
    }

    // The String sort case
    else if (b[orderBy] !== undefined && a[orderBy] !== undefined) {
        aSortName = a[orderBy]
        bSortName = b[orderBy]
    }

    // Default error fallback
    else {
            logger.error(`Something went wrong with sorting. a = ${a}, b = ${b}, orderBy = ${orderBy}`)
            aSortName = null
            bSortName = null
    }

    // ==================================

    // This is the actual default String sort
    if (String(bSortName).toLowerCase() < String(aSortName).toLowerCase()) {
        return -1;
    }
    if (String(bSortName).toLowerCase() > String(aSortName).toLowerCase()) {
        return 1;
    }

    return 0;
}
  

function getComparator(order : 'asc' | 'desc' | undefined, orderBy : string) {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}
  

function stableSort(array : any[], comparator : (order: string, orderBy: string) => number) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

function getAssignedDomain(domainId : string) : string{
    if(domainId === DEFAULT_ADMIN_DOMAIN_ID){
        return "Admin Domain"
    }
    let bannerDomain = store.getState().asset.banners.find((banner) => banner.bannerId === domainId)
    if(bannerDomain){
        return "Banner: " + bannerDomain.bannerName 
    }

    let regionDomain = store.getState().asset.regions.find((region) => region.regionId === domainId)
    if(regionDomain){
        return "Region: " + regionDomain.regionName 
    }

    let storeDomain = store.getState().asset.stores[domainId]
    if(storeDomain){
        return "Store: " + storeDomain.storeName
    }

    return "Unknown"

}


// Checks the type param and returns an icon. Used on dashboard tables.
const typeColumn = (type : string) => {

  switch(type){
    case 'region':  return(<span> <RegionIcon />      {"Region"} </span>)
    case 'store':   return(<span> <StoreIcon />       {"Store"} </span>)
    case 'dept':    return(<span> <DepartmentIcon />  {"Department"} </span>)
    case 'scale':   return(<span> <ScaleIcon />       {"Scale"} </span>)
    default:        return(<span> <ErrorIcon />       {"Unknown"} </span>)
  }
}


// =======================================================
// Column names
interface EnhancedTableHeadProps{
    order : 'asc' | 'desc' | undefined
    orderBy : string
    onRequestSort : (e : React.MouseEvent<HTMLAnchorElement, MouseEvent>, b: TableTemplateHeadCellType) => void
    checkedState : boolean[]
    headCells : TableTemplateHeadCellType[]
    customClassName ?: string
}

function EnhancedTableHead(props : EnhancedTableHeadProps) {
  const {order, orderBy, onRequestSort, checkedState, headCells} = props;
  const createSortHandler = (property : TableTemplateHeadCellType) => (event : React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead className={`tableHeader`}>
      <TableRow>        
        {headCells.map((headCell, index) => (
          
          <TableCell
            className={checkedState[index] ? (headCell.narrow ? "smallColumn" : "normalColumn") : "hideColumn"}
            key={headCell.id}
            align="left"
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel 
              className={checkedState[index] && headCell.narrow ? "smallColumn" : "normalColumn"}
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell)}
            >            
              {headCell.label}
              {/* {headCell.label === "Access Level" 
              ? <Tooltip title={<Typography fontSize={12}>Additional Label if needed</Typography>}>
                  <div>{headCell.label}</div>
                </Tooltip>
              : headCell.label} */}

              {orderBy === headCell.id ? (
                <span className="visuallyHidden">
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}
  
interface EnhancedTableToolbarProps{
    tableName : string
    tableDescription : string
    dispatch : Dispatch
    checkedState : boolean[]
    setCheckedState : React.Dispatch<React.SetStateAction<boolean[]>>
    headCells : TableTemplateHeadCellType[]
    customClassName ?: string
    dispatchedUrls : any[]
}
  
// Table name + description (+ optional dynamic information)
const EnhancedTableToolbar = (props : EnhancedTableToolbarProps) => {
    const { tableName, tableDescription, dispatch, checkedState, setCheckedState, headCells } = props;

    // ==================== Filter Columns ====================

    const [anchorEl, setAnchorEl] = useState(null);
    const isMenuOpen = Boolean(anchorEl);                   // Show/hide profile menu (desktop)
    
    let tmpChecked = checkedState;
    // logger("tmpChecked: ", tmpChecked)
    // logger("headCells: ", headCells)

    const handleProfileMenuOpen = (event) => {setAnchorEl(event.currentTarget)};
    
    const handleMenuClose = () => {setAnchorEl(null)};

    const handleCheck = (e : React.ChangeEvent<HTMLInputElement>, index : number) => {
        tmpChecked[index] = e.target.checked

        setCheckedState(tmpChecked)

        if (props.dispatchedUrls) {
            props.dispatchedUrls.forEach((url) => {
                dispatch(url())
            })
        }
    };

    const renderMenu = (
        <Menu
            id="selectColumns"
            anchorEl={anchorEl}
            open={isMenuOpen}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            getContentAnchorEl={null}
            keepMounted
            onClose={handleMenuClose}
        >

        {/* For every column */}
        {checkedState.map((column, index) => {

            // If the column is not a mandatory column (filename, shortDesc, actions)
            if (headCells[index].mandatory === false){
            // Display the option to disable the column
                return (
                    <FormCheck key={headCells[index].id} type="checkbox" className="d-flex p-3" >
                    <FormCheck.Label className="px-0 customCheckbox">
                        <FormCheck.Input onChange ={(e) => handleCheck(e, index)} defaultChecked/>
                        {headCells[index].label}
                    </FormCheck.Label>
                    </FormCheck>
                )
            }
            else {return null}
            
        })}

        </Menu>
        // ==================== Filter Columns ====================

  );
  
    return (
        <>
            <Toolbar component="div" className={`tableToolbar`}>
                <Typography className="tableTitle" variant="h6" id="tableTitle" component="div">
                    {tableName}
                </Typography>

                <Typography className="tableDescription" variant="h6" id="tableTitle" component="div">
                    {tableDescription}
                </Typography>

                <Tooltip placement="top" title={<Typography>Filter List</Typography>}>
                    <IconButton 
                    edge="end"
                    aria-label="filter list"
                    aria-controls="selectColumns"
                    aria-haspopup="true"
                    onClick={handleProfileMenuOpen}
                    >
                    <FilterListIcon />
                    </IconButton>
                </Tooltip>

                <Tooltip placement="top" title={<Typography>Refresh Table</Typography>}>
                    <IconButton 
                    aria-label="refresh table"
                    onClick={() => {
                        props.dispatchedUrls.forEach((url) => {
                            dispatch(url())
                        })
                    }}
                    >
                    <RefreshIcon />
                    </IconButton>
                </Tooltip>
            </Toolbar>
            {renderMenu}
        </>
    );
};


interface TableTemplateProps{
    tableName: string
    tableDescription : string
    currentUser : HTeUser
    download ?: (e: any) => void
    handleEditFile : (e: any) => void
    handleDeleteFile : (e: any) => void
    displayRoles : (e : any) => void
    recordId : string
    className ?: string
    headCells : TableTemplateHeadCellType[]
    dataSet : any[]
    dispatchedUrls : ((id?: number) => (dispatch: Dispatch<AnyAction>, getState: () => RootStore) => void)[]
    actionDisable ?: boolean
    profileId ?: string
    defaultOrderBy : string
    customRowsPerPage : number
    searchPlaceholder : string
}

export const TableTemplate = (props : TableTemplateProps) => {
  
  const { headCells, dataSet, dispatchedUrls, actionDisable} = props;
  const dispatch = useDispatch();

  const [order, setOrder]               = useState<'asc' | 'desc' | undefined>('asc');                  // Default ascending order
  const [orderBy, setOrderBy]           = useState(props.defaultOrderBy);   // Selecting which column to sort by
  const [page, setPage]                 = useState(0);                      // Keeps track of which page you're on
  const [rowsPerPage, setRowsPerPage]   = useState(props.customRowsPerPage ? props.customRowsPerPage : 25);          // Default rows per page = 5  
  const [search, setSearch]             = useState("");                     // Search bar
  const [searchPlaceholder, setSearchPlaceholder] = useState(props.searchPlaceholder)
  const [checkedState, setCheckedState] = useState<boolean[]>(new Array(headCells.length).fill(true))  // Array of booleans to show/hide columns

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property.id && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property.id);
    setSearchPlaceholder("Search for " + property.label)

  };
  
  const handleChangePage = (event : React.MouseEvent<HTMLButtonElement, MouseEvent> | null , newPage : number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event : React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const searchByColumn = (orderBy : string, file : any, search: string) => {
    if(file.type === 'scale'){
        let item = String(file[orderBy] ?? file.properties[orderBy] ?? "")
        if(item.toLowerCase().includes(search))return true
    }
    else{
        let item = String(file[orderBy] ?? "")
        if(item.toLowerCase().includes(search))return true
    }
    return false
  }

    useEffect(() => {
        if (dispatchedUrls) {
        dispatchedUrls.forEach((url) => {
            dispatch(url())
        }) 
        }
        // May need to play with this value if things arent rendering properly
    }, []);

    return (
    <> 
      <div className={`tableTemplate`}>
        <Form.Control 
          type="text" 
          className="tableSearchBar fw-bold"
          placeholder={searchPlaceholder}
          onChange={(e) => {setSearch(e.target.value)}}
        />

        <EnhancedTableToolbar 
          tableName={props.tableName} 
          tableDescription={props.tableDescription ? props.tableDescription : ""}
          dispatch={dispatch}
          dispatchedUrls={dispatchedUrls}
          checkedState={checkedState}
          setCheckedState={setCheckedState}
          headCells={headCells}
          customClassName={props.className}
        />

        <TableContainer className={`${props.className ? props.className : ``} tableTemplate max-height-600`}>
          <Table 
            className="standardTable"
            aria-labelledby="tableTitle"
            aria-label="customized table"
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              checkedState={checkedState}
              headCells={headCells}
              customClassName={props.className ? props.className : undefined}
            />

            <TableBody>
            {dataSet.length !== 0 
            ? stableSort(dataSet, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .filter((file) => {
                  if (search === "") return file;
                  else if (searchByColumn(orderBy, file, search.toLowerCase())) return file
                  //else if (file[props.searchFilter].toLowerCase().includes(search.toLowerCase())) return file;
                  // else if (props.searchFilter.map(filter => (
                  //   logger("For testing multiple search filters", file[filter].toLowerCase().includes(search.toLowerCase()))
                  //   file[filter].toLowerCase().includes(search.toLowerCase()) === true
                  // ))) return file
                  else return null
              })
              .map((file) => {
                // logger("headCells id: ", headCells[0].id)
                // logger("all h  eadCells: ", headCells)
                let date : string = "";

                return (
                  <TableRow key={file[props.recordId]}> 
                    {/* Check to see if props.headCells contains "Actions" column, 
                          if yes => display column
                          if no  => hide column
                    */}
                    {headCells.some(action => action.id === "actions")
                    ? <TableCell>
                        {props.download
                          ? <Tooltip title={<Typography>Download</Typography>}>
                            <span>
                              <IconButton aria-label="download"
                                disabled={(props.currentUser === undefined || actionDisable)}
                                onClick={() => props.download!(file)}
                              >
                                <DownloadIcon />
                              </IconButton>
                            </span>
                          </Tooltip>
                          : null}

                        {props.handleEditFile
                          ? <Tooltip title={<Typography>Edit</Typography>}>
                            <span>
                              <IconButton aria-label="edit"
                                disabled={(props.currentUser === undefined || actionDisable)}
                                onClick={() => props.handleEditFile(file)}
                              >
                                <EditIcon />
                              </IconButton>
                            </span>
                            </Tooltip>
                          : null}

                        {props.handleDeleteFile
                          ? <Tooltip title={<Typography>Delete</Typography>}>
                            <span>
                              <IconButton aria-label="delete" 
                                disabled={(props.currentUser === undefined || actionDisable)}
                                onClick={() => props.handleDeleteFile(file)}>
                                <DeleteIcon />
                              </IconButton>
                            </span>
                          </Tooltip>
                          : null}
                      </TableCell>
                    : null}
                    
                    {checkedState.map((column, index) => {             
                      // logger("headCells: ", headCells[index].id)
                      // logger("checkedState: ", checkedState[index])
                      // logger("file: ", file) 
                      
                      // What to display for each headCell's id name
                      switch (headCells[index].id) {
                        case 'actions':
                          return (
                            null
                          )

                        // =================== DASHBOARD CASES ===================
                        // type = {scale, dept, store}, id, options for scales, assignedBanner, assignedRegion, assignedStore, assignedDept

                        case 'type':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {typeColumn(file[headCells[index].id])}  
                            </TableCell>
                          )

                        case 'id':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file[headCells[index].id] ? file[headCells[index].id] : "Not set"}   
                            </TableCell>
                          )

                        case 'hostname':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.properties.hostname ? file.properties.hostname : "Error fetching host name"}   
                            </TableCell>
                          )

                        case 'application':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.properties.application ? file.properties.application : "Error fetching version"}   
                            </TableCell>
                          )

                        case 'totalLabelsPrinted':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.properties.totalLabelsPrinted || file.properties.totalLabelsPrinted === 0 ? file.properties.totalLabelsPrinted : "N/A"}   
                            </TableCell>
                          )

                        case 'pluCount':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.properties.pluCount || file.properties.pluCount === 0 ? file.properties.pluCount : "N/A"}   
                            </TableCell>
                          )  

                        case 'options':
                          return (
                            <TableCell>
                              {file.type === "scale" 
                              ? <Link to={{ pathname: '/AssetDetails', state: { asset: file} }}>
                                  <Button className="minimizeButton p-0" variant="link">
                                      <Typography noWrap={true}>
                                            View More Details

                                            <FontAwesomeIcon icon={faChevronRight} className="animate-left-3 me-3 ms-2" style={{fontSize: "1em"}} />

                                      </Typography>
                                    

                                  </Button>
                                </Link> 
                              : null}
                            </TableCell>
                          )

                        case 'assignedBanner':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.assignedBanner == null ? "Error finding banner" : file.assignedBanner}
                            </TableCell>
                          )
  
                        case 'assignedRegion':
                        
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.assignedRegion == null ? "Error finding region" : file.assignedRegion}
                            </TableCell>
                          )

                        case 'assignedStore':
                            return (
                                <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                                {file.assignedStore == null ? <span className="color-orange fw-bold">Unassigned Scale</span> : file.assignedStore}
                                </TableCell>
                            )

                        case 'assignedDept':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.assignedDept == null ? <span className="color-orange fw-bold">Unassigned Scale</span> : file.assignedDept}
                            </TableCell>
                          )

                        // ====================================

                        case 'size':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"} align="right"> 
                              {formatBytes(file[headCells[index].id])}  
                            </TableCell>
                          )

                        case 'fileType':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file[headCells[index].id] ? PrintFileType(file[headCells[index].id]) : "Unknown"}  
                            </TableCell>
                          )

                        case 'syncStatus':
                        case 'enabled':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file.enabled ? "Enabled" : "Disabled"}  
                            </TableCell>
                          )

                        case 'date':
                        case 'dateCreated':
                        case 'uploadDate':
                        case 'startDate':
                        case 'endDate':
                          if      (file.date)         file.date ? date = moment(file[headCells[index].id]).format("lll") : date = "N/a"
                          else if (file.dateCreated)  file.dateCreated ? date = moment(file[headCells[index].id]).format("lll") : date = "N/a"
                          else if (file.uploadDate)   file.uploadDate ? date = moment(file[headCells[index].id]).format("lll") : date = "N/a"
                          else if (file.startDate)    file.startDate ? date = moment(file[headCells[index].id]).format("lll") : date = "N/a"
                          else if (file.endDate)      file.endDate ? date = moment(file[headCells[index].id]).format("lll") : date = "N/a"
                          else    date = "N/a"

                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file[headCells[index].id] ? date : "Not set"}  
                            </TableCell>
                          )

                        case 'firstName':
                        case 'lastName':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {(file.firstName && file.lastName) ? file[headCells[index].id] : "Not set"}  
                            </TableCell>
                          )  

                        case 'phoneNumber':
                            let formattedPhoneNumber = ""
                          if (file[headCells[index].id]) {
                            let tempPhoneNumber = file[headCells[index].id].replace(/[^0-9]/g, "")
                            formattedPhoneNumber = `(${tempPhoneNumber.substr(0,3)}) ${tempPhoneNumber.substr(3,3)}-${tempPhoneNumber.substr(6,4)}`
                          }

                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file[headCells[index].id] ? formattedPhoneNumber : ""}  
                            </TableCell>
                          )  

                        case 'accessLevel':
                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              {file[headCells[index].id] !== null
                              ? 
                                file[headCells[index].id] > 4000 ? 
                                  "Hobart" : 
                                file[headCells[index].id] === 4000 ? 
                                  "Admin" : 
                                file[headCells[index].id] === 3000 ? 
                                  "Banner Manager" : 
                                file[headCells[index].id] === 2000 ? 
                                  "Store Manager" : 
                                file[headCells[index].id] === 1000 ? 
                                  "Operator" : 
                                file[headCells[index].id] === 0 ? 
                                  "Custom Role" : 
                                  "Error" 
                              : "Not set"}  
                            </TableCell>
                          )  

                        case 'domain':
                          return (
                            
                            <TableCell 
                                key={headCells[index].id} 
                                className={checkedState[index] ? "showColumn" : "hideColumn"}> 
                              
                                {getAssignedDomain(file[headCells[index].id].domainId)}
                            </TableCell>
                          )                        

                        case 'roles':

                          return (
                            <TableCell key={headCells[index].id} className={checkedState[index] ? "showColumn" : "hideColumn"}> 

                            {props.displayRoles
                              ? <Tooltip title={<Typography>Display User's Roles</Typography>}>
                                <span style={{fontWeight: "500", color: "black"}}> View Roles
                                  <IconButton aria-label="download"
                                    onClick={() => props.displayRoles(file)}
                                  >
                                    <ViewListIcon />
                                  </IconButton>
                                </span>
                              </Tooltip>
                              : null}
                            </TableCell>
                          )    

                        default: 
                          return (
                            <TableCell key={headCells[index].id}>
                              {/* {file[headCells[index].id]} */}
                              {file[headCells[index].id] ? file[headCells[index].id] : "Not set (1)"}  

                            </TableCell>
                          )
                      }
                    })}
                  </TableRow>
                );
              })
            : null}
              
            </TableBody>
          </Table>
        </TableContainer>

        <TablePagination
          rowsPerPageOptions={[25, 50, 100, 200]}
          component="div"
          count={dataSet.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
        
      </div>
    </>
  );
}

export default TableTemplate;
