import { useState, useEffect, useRef } from "react";
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome} from '@fortawesome/free-solid-svg-icons';
import {Breadcrumb} from '@themesberg/react-bootstrap';
import DepartmentIcon from '@material-ui/icons/LocalGroceryStoreOutlined';
import RegionIcon from '@material-ui/icons/RoomOutlined';
import ScaleIcon from "../icons/ScaleIcon";
import BannerIcon from '@material-ui/icons/Flag';
import StoreIcon from '@material-ui/icons/Store';

import createProfileImg from "../../media/createProfileButton.png"
import createDeptImg from "../../media/createDeptButton.png"
import createDeptFormImg from "../../media/createDeptForm.png"
import updateDomainImg from "../../media/updateDomain.png"
import profileFilesImg from "../../media/profileFiles.png"
import addEntityImg from "../../media/addEntity.png"
import assetListImg from "../../media/assetList.png"
import assetListLegendImg from "../../media/assetListLegend.png"
import optionsMenuImg from "../../media/optionsMenu.png"
import scaleViewDetailsImg from "../../media/scaleViewDetails.png"
import domainChartImg from "../../media/DomainChart.png"
import "./DirectionStyle.css"
import { MoreHoriz } from "@mui/icons-material";

/**
 * This renders an item in the table of contents list.
 * scrollIntoView is used to ensure that when a user clicks on an item, it will smoothly scroll.
 */
interface ContentsType{
    id: string, 
    title: string, 
    items?: ContentsType[]
}


 const yOffset = -70; 
 const Headings = ({ headings, activeId }) => (
    <ul>
      {headings.map((heading) => (
        <li key={heading.id} className={heading.id === activeId ? "active" : ""}>
            <a
                href={`#${heading.id}`}
                onClick={(e) => {
                    e.preventDefault();

                    const element = document.getElementById(heading.id);
                    if(element){
                        const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
                        window.scrollTo({top: y, behavior: 'smooth'})
                    }
                }}
            >
            {heading.title}
          </a>
          {heading.items.length > 0 && (
            <ul>
              {heading.items.map((child) => (
                <li
                  key={child.id}
                  className={child.id === activeId ? "active" : ""}
                >
                  <a
                    href={`#${child.id}`}
                    onClick={(e) => {
                        e.preventDefault();
                        let selector = document.querySelector(`#${child.id}`)
                        if(selector){
                            selector.scrollIntoView({
                                behavior: "smooth"
                            });
                        }

                        const element = document.getElementById(child.id);
                        if(element){
                            const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
                            window.scrollTo({top: y, behavior: 'smooth'})
                        }
                        
                    }}
                  >
                    {child.title}
                  </a>
                </li>
              ))}
            </ul>
          )}
        </li>
      ))}
    </ul>
  );
  
  /**
   * Dynamically generates the table of contents list, using any H2s and h5s it can find in the main text
   */
  const useHeadingsData = () => {
    const [nestedHeadings, setNestedHeadings] = useState<ContentsType[]>([]);
  
    useEffect(() => {
      const headingElements : Element[] = Array.from(
        document.querySelectorAll("main h2, main h3")
      );
  
      // Created a list of headings, with h5s nested
      const newNestedHeadings = getNestedHeadings(headingElements);
      setNestedHeadings(newNestedHeadings);
    }, []);
  
    return { nestedHeadings };
  };
  
  const getNestedHeadings = (headingElements : Element[]) => {
    const nestedHeadings : ContentsType[] = [];
  
    headingElements.forEach((heading : Element, index : number) => {
      const { innerText: title, id } = heading as HTMLElement;
  
      if (heading.nodeName === "H2") {
        nestedHeadings.push({ id, title, items: [] });
      } else if (heading.nodeName === "H3" && nestedHeadings.length > 0) {
        if(!nestedHeadings[nestedHeadings.length - 1].items){
            nestedHeadings[nestedHeadings.length - 1].items = []
        }
        nestedHeadings[nestedHeadings.length - 1].items!.push({
          id,
          title
        });
      }
    });
  
    return nestedHeadings;
  };
  
  const useIntersectionObserver = (setActiveId : React.Dispatch<React.SetStateAction<string>>) => {
    const headingElementsRef = useRef({});
    useEffect(() => {
      const callback = (headings : IntersectionObserverEntry[]) => {
        headingElementsRef.current = headings.reduce((map, headingElement) => {
          map[headingElement.target.id] = headingElement;
          return map;
        }, headingElementsRef.current);
  
        // Get all headings that are currently visible on the page
        const visibleHeadings : IntersectionObserverEntry[] = [];
        Object.keys(headingElementsRef.current).forEach((key) => {
          const headingElement = headingElementsRef.current[key];
          if (headingElement.isIntersecting && headingElement.target.id !== "DirectionTitle") visibleHeadings.push(headingElement);
        });
  
        const getIndexFromId = (id : string) =>
          headingElements.findIndex((heading) => heading.id === id);
  
        // If there is only one visible heading, this is our "active" heading
        if (visibleHeadings.length === 1) {
          setActiveId(visibleHeadings[0].target.id);
          // If there is more than one visible heading,
          // choose the one that is closest to the top of the page
        } else if (visibleHeadings.length > 1) {
          const sortedVisibleHeadings = visibleHeadings.sort(
            (a, b) => getIndexFromId(b.target.id) > getIndexFromId(a.target.id) ? 1 :
                        getIndexFromId(a.target.id) > getIndexFromId(b.target.id) ? -1 : 0
          );
  
          setActiveId(sortedVisibleHeadings[0].target.id);
        }
      };
  
      const observer = new IntersectionObserver(callback, { root: document.querySelector("iframe"), rootMargin: "000px" });
  
      const headingElements = Array.from(document.querySelectorAll("h2, h3"));
  
      headingElements.forEach((element) => observer.observe(element));
  
      return () => observer.disconnect();
    }, [setActiveId]);
  };
  
  /**
   * Renders the table of contents.
   */
  const TableOfContents = () => {
    const [activeId, setActiveId] = useState<string>("");
    const { nestedHeadings } = useHeadingsData();
    useIntersectionObserver(setActiveId);
  
    return (
      <nav aria-label="Table of contents" className="tableContents">
        <Headings headings={nestedHeadings} activeId={activeId} />
      </nav>
    );
  };
  
  
const Content = () => {
    return (
      <div style={{display:"flex"}}>
        <div className="mb-4">
            <TableOfContents />
        </div>
        <div className="mb-4 mb-lg-0 bg-white shadow-soft border rounded border-light p-4 w-100 form-control">
            <main>
                <h2 id="gettingStartedVidh2">Getting Started</h2>
                <div style={{display:"flex", justifyContent:"center"}}>
                    <span className="fr-video fr-dvb fr-draggable">
                        <iframe
                            title="GettingStartedVideo"
                            src="https://wwhobart.box.com/s/1rlj8tpyfqwjqv3bpnih55t6fjzqhg1w" 
                            sandbox="allow-scripts allow-forms allow-same-origin allow-presentation" 
                            className="fr-draggable" 
                            width="800" 
                            height="650">
                            
                        </iframe>
                    </span>
                </div>
                

                <h2 id="scaleProfileh2">Scale Profiles</h2>
                <div style={{display:"flex", justifyContent:"center"}}>
                    <span className="fr-video fr-dvb fr-draggable">
                        <iframe 
                            title="ScaleProfileVideo"
                            src="https://wwhobart.box.com/s/qakt2aal2hxrlhvht3n7qm2lsq5f3q05" 
                            sandbox="allow-scripts allow-forms allow-same-origin allow-presentation" 
                            className="fr-draggable" 
                            width="800" 
                            height="650">
                            
                        </iframe>
                    </span>
                </div>
                <ol>
                    <h5 id="scaleProfileCreate"><li>Create a new Profile</li></h5>
                    <ul>
                        <li>Navigate to the Scale profiles page on the left hand side bar</li>
                        <li>Click on <span className="fw-bolder">Create Profile</span>
                            <br/>
                            <img alt="createProfile" src={createProfileImg} />
                        </li>
                        <li>A pop-up window will appear prompting for some information about the new profile</li>
                        <li>Use Account Domain Checkbox</li>
                        <ul>
                            <li>Leaving this checked will ensure only users with your same level of permissions or higher is able to access this scale profile</li>
                            <li>Unchecking this box will allow you to specify any domain to assign this scale profile to. (Note: Can only assign it to any domain your account has access to, otherwise it'll give you an error. We may filter this in the future)
                            <br/>
                            <div style={{width:"100%", display: "flex", alignItems: "center", justifyContent:"center"}}>
                                <img alt="updateDomain" src={updateDomainImg} style={{border:"0.0625rem solid #d1d7e0", borderRadius:"1rem"}}/>
                            </div>
                            </li>
                        </ul>

                    </ul>
                    <br />
                    <h5 id="uploadFiles"><li>Uploading Files</li></h5>
                    <ul>
                        <li>Click on the <span className="fw-bolder">Upload File button</span> and select a scale profile to upload the file to the server</li>
                        <li>A popup window will appear, prompting for some more information about the file you are about to upload</li>
                        <li> Form Components: </li>
                        <ul>
                            <li><span className="fw-bolder">Select a Scale Profile</span> : Drop down menu, for you to pick which profile this new file will belong to</li>
                            <li>Next you will be prompted with the <span className="fw-bolder">Choose File</span> button, select the configuration file you'd like to upload</li>
                            <li>The <span className="fw-bolder">File Description</span> field is a short reminder of what this file does</li>
                            <li><span className="fw-bolder">File Category</span> : indicates the type of information this file contains</li>
                            <li>Enable Syncing</li>
                            <ul>
                                <li>Leaving this checked will ensure this file will be included in every heartbeat. If a scale already contains the file, it will not resend the file unless the user forces it to download the file again.</li>
                                <ul>
                                    <li>If an end date is provided and the date passes, the file will no longer be included in heartbeats</li>
                                </ul>
                                <li>Unchecking this box will stop the file from being sent to every scale assigned to this scale profile with every heartbeat</li>
                            </ul>
                        </ul>
                        
                    </ul>
                    <h5 id="fileActions"><li>File Actions</li></h5>
                    <ul>
                        <li>If you open the scale profile the file was uploaded to, all of those files will appear in a table </li>
                        <li>These files can have their information edited, downloaded, and deleted
                            <br/>
                            <div style={{width:"100%", display: "flex", alignItems: "center", justifyContent:"center"}}>
                                <img alt="updateDomain" src={profileFilesImg} style={{border:"0.0625rem solid #d1d7e0", borderRadius:"1rem"}}/>
                            </div>
                        </li>
                        
                    </ul>
                </ol>
                <h2 id="departmentHeader">Departments</h2>
                <ol>
                    <h5 id="createDepartment"><li>Create a new Department</li></h5>
                    <ul>
                        <li>Navigate to the Departments on the left hand navigation bar</li>
                        <li>Click on <span className="fw-bold">Create Department</span>
                            <br/>
                            <img alt="createDeptBtn" src={createDeptImg} />
                        </li>
                        <li>A popup window will appear, prompting you for some information about the new department
                            <div style={{width:"100%", display: "flex", alignItems: "center", justifyContent:"center"}}>
                                <img alt="createDeptForm" src={createDeptFormImg} style={{border:"0.0625rem solid #d1d7e0", borderRadius:"1rem"}}/>
                            </div>
                        </li>
                        <li>Select a Default Scale Profile</li>
                        <ul>
                            <li>This is optional, but you can pre-select a scale profile to be tied to a new department.</li>
                            <li>When adding this department to a store in the Asset List, any scales assigned to this department will sync using the files in this scale profile</li>
                        </ul>
                        <li>Use Account Domain Checkbox</li>
                        <ul>
                            <li>Leaving this checked will ensure only users with your same level of permissions or higher is able to access this scale profile</li>
                            <li>Unchecking this box will allow you to specify any domain to assign this scale profile to. (Note: Can only assign it to any domain your account has access to, otherwise it'll give you an error. We may filter this in the future)</li>
                        </ul>
                    </ul>

                    <h5 id="deptActions"><li>Department Actions</li></h5>
                    <ul>
                        <li>Opening a department will display the information about it, and you are able to change the default scale profile and required domain associated with it here</li>
                        <li>Full departments are able to be deleted on this page as well. On the asset list, you are able to remove it from a store, but to delete the department, it must be done here</li>
                    </ul>
                </ol>
                <h2 id="assetList">Asset List</h2>
                <div style={{display:"flex", justifyContent:"center"}}>
                    <span className="fr-video fr-dvb fr-draggable">
                        <iframe
                            title="AssetListVideo"
                            src="https://wwhobart.box.com/s/1rlj8tpyfqwjqv3bpnih55t6fjzqhg1w" 
                            sandbox="allow-scripts allow-forms allow-same-origin allow-presentation" 
                            className="fr-draggable" 
                            width="800" 
                            height="650">
                            
                        </iframe>
                    </span>
                </div>
                <ol>
                    <h5 id="createNewAsset"><li>Overview</li></h5>
                    <ul>
                        <li>The asset list provides a tree-like view of the assets in HTe Enterprise, it helps you decipher the general layout of scales in your store and where they are located
                            <br/>
                            <div style={{width:"100%", display: "flex", alignItems: "center", justifyContent:"center"}}>
                                <img alt="assetListImg" src={assetListImg} style={{border:"0.0625rem solid #d1d7e0", borderRadius:"1rem"}}/>
                            </div>
                        </li>
                        <li><BannerIcon/> <span className="fw-bolder">Banners</span> are the top level component, they will encapsulate all your assets in your company. Most users will only have one banner</li>
                        <li><RegionIcon/> <span className="fw-bolder">Regions</span> are a location based separator to help split up your stores into different geographical locations such provinces, states, or counties</li>
                        <li><StoreIcon/> <span className="fw-bolder">Stores</span> contain all of the store based information, if you are using a multi-leveled version of HTe, this means information about the sync status of the store server, as well as it's current status</li>
                        <li><DepartmentIcon/> <span className="fw-bolder">Departments </span> indicate which departments you have in that store as well as which files(profile) that department is using</li>
                        <li><ScaleIcon/> <span className="fw-bolder">Scales/Wrappers</span> are the smallest component of the list, the asset list will show diagnostic info about the scale as well as it's current heartbeat and sync status</li>
                        <br/>
                        <li>All of these components (Except the scale) will have a pie chart next to it's name. This pie chart reflects the current status' of all the scales/wrappers contained within that component</li>
                        <li>The colors on these pie charts are shown by the legend in the top right of the asset list page
                            <br/>
                            <img alt="assetListLegend" src={assetListLegendImg} style={{border:"0.0625rem solid #d1d7e0", borderRadius:"1rem"}}/>
                        </li>
                    </ul>
                    <h5 id="createNewAsset"><li>Creating a new asset</li></h5>
                    <ul>
                        <li>If your account has proper access, click on <img alt="createProfile" src={addEntityImg} /> to add an entry to any level in tree</li>
                        <ul>
                            <li>Once clicked a drop down menu will appear, prompting for the type of entity you'd like to add</li>
                            <li>Clicking on one of these menu items will open a pop-up form prompting for information about the new entity</li>
                        </ul>
                        <li>You can also add items by clicking the <MoreHoriz/> button on the right side of any component. Or by <span className="fw-bolder">right clicking</span> the element you'd like to make changes to. Selecting either of these options will display a drop down menu.
                            <br/>
                            <img alt="createDeptForm" src={optionsMenuImg} style={{border:"0.0625rem solid #d1d7e0", borderRadius:"1rem"}}/>
                        </li>
                        <li>Departments</li>
                        <ul>
                            <li>Pre-existing Departments can be assigned to a store, if you want to create a new department please take a look at the <span className="fw-bolder">Departments</span> section</li>
                            <li>The<span className="fw-bolder"> View Details button </span>on a Department will show the current scale profile and some information about the scales assigned to it </li>
                        </ul>
                        <li>Scales</li>
                        <ul>
                            <li>Unassigned Scales can be assigned to a department, and new scales can be manually added here too</li>
                            <li>The<span className="fw-bolder"> View Details button </span>on a Scale will show the current scale profile and some information about the scale </li>
                            <li>Users with proper access can set a manual scale profile that is different than the department's assigned scale profile</li>
                            <li>Primary Scale is used for SFTP communication (Not currently working)</li>
                            <li><span className="fw-bolder">Pinging</span> the scale will check if the scale is reachable and responding</li>
                            <li><span className="fw-bolder">Manual Heartbeat</span> allows the user to manually push a sync command to the scale to check its files against what is in the assigned scale profile</li>
                            <li><span className="fw-bolder">Rebooting</span> the scale restarts the scale</li>
                            <li><span className="fw-bolder">Delete Data</span> will clear the scale of its current files, data, and configuration</li>
                            <li>Disabling a scale will stop the scale from syncing against the current scale profile every periodic heartbeat
                            <br/>
                            <div style={{width:"100%", display: "flex", alignItems: "center", justifyContent:"center"}}>
                                <img alt="scaleDetails" src={scaleViewDetailsImg}/>
                            </div>

                            </li>
                        </ul>
                    </ul>
                </ol>
                <h2 id="role-header">Roles</h2>
                <ol>
                    <ul>
                        <li>Roles are the permissons granted to a user and they help tell the server which actions that user can perform</li>
                        <li>While roles can be individually assigned to each user, to reduce complexity HTe has four premade groups of permissions:</li>
                            <span className="fw-bolder">Admin:</span>
                            <ul>
                                <li>Full access to the Asset Tree View</li>
                                <li>Includes all features listed below</li>
                            </ul>
                            <br />

                            <span className="fw-bolder">Banner Manager:</span>
                            <ul>
                                <li>Full access to their assigned banner in the Asset Tree</li>
                                <li>Full access to the Scale Profiles page</li>
                                <li>Ability to upgrade frontend and backend systems</li>
                                <li>Upload / Delete new and existing licenses for HTe</li>
                                <li>Includes all features listed below</li>
                            </ul>
                            <br />

                            <span className="fw-bolder">Store Manager:</span>
                            <ul>
                                <li>Ability to assign existing Scale Profiles to departments in the Asset Tree in their assigned store</li>
                                <li>Ability to add / modifify / delete existing HTe operators with a lower access level in their assigned store</li>
                                <li>Ability to assign and reassign scales to different departments in their assigned store</li>
                                <li>Ability to enable / disable scales in their assigned store</li>
                                <li>Includes all features listed below</li>
                            </ul>
                            <br />

                            <span className="fw-bolder">Operator:</span>
                            <ul>
                                <li>Ability to view current status of scales in their assigned store</li>
                                <li>Ability to send reboot command to a scale in their assigned store</li>
                                <li>Ability to send manual heartbeat to a scale in their assigned store</li>
                                <li>Ability to perform status check (ping) to a scale in their assigned store</li>
                            </ul>
                            <br/>
                            <span className="fw-bolder">Custom:</span>
                            <ul>
                                <li>Can contain any combination of the permissions listed above</li>
                            </ul>
                    </ul>
                </ol>
                <h2 id="domain-header">Domains</h2>
                <ol>
                    <h5 id="domainOverview"><li>Overview</li></h5>
                    <ul>
                        <li>Domains indicate the scope of what the user can edit within the program</li>
                        <li>Every asset on the asset tree creates a domain when the entity is created. This domain indicates that you can perform all of your permitted roles on <span className="fw-bolder">this Entity</span> and <span className="fw-bolder"> all of the Entities children</span>. 
                        </li>
                        <li> When a user account is created they are assigned one of these domains <span className="fw-bolder">Admin, Banner, Region, or Store</span> which gives them access to all the entities below that domain    
                            <br/>
                            <div style={{width:"100%", display: "flex", alignItems: "center", justifyContent:"center"}}>
                                <img alt="domainChart" src={domainChartImg}/>
                            </div>
                        </li>
                        <br/>
                        <li>For example, if you are assigned to have a domain of a certain <span className="fw-bolder">Region</span> then you can perform permitted actions on that region such as editing the regions, name and adding stores to the region. 
                        <br/> You will also be permitted to make changes to any items that belong to that Region, such as Stores, Departments, Profiles, and Scales. 
                        </li>
                        <li>The <span className="fw-bolder">Unassigned Domain</span> is the catch-all for entities that have been misassigned, or their parents have been deleted
                            <ul>
                                <li>Scales who are reporting for the first time to the server will report as unassigned, since they do not know which store they belong to</li>
                                <li>Items that have an 'Unassigned' Domain can only be edited by those with admin access in the application</li>
                            </ul>
                        </li>
                            
                    </ul>
                    <h5 id="domainSpecialCases"><li>Profiles and Departments</li></h5>
                    <ul>
                        <li>Profiles and Departments are handled a little differently because they can be used in more than one location. Multiple stores can use the same department/profile combinations. </li>
                        <li>Much like users, when a profile or department is created a prompt will be made asking for which domain it belongs to. </li>
                        <li>This domain will indicate to the backend which domain is required by the user to make changes to that entity. </li>
                        <li><span className="fw-bolder">NOTE:</span> When a profile's or department's assigned domain is deleted. They will autmatically be assigned to the "Unassigned" Domain. Meaning that only those with a domain of "Admin" will be able to make changes to them until they are reassigned</li>
                    </ul>
                </ol>
            </main>

        </div>
        
      </div>
    );
};
  


export const Directions = () => {

    return (
    <>
        <div className="mb-4 mb-md-0 ">
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Directions</Breadcrumb.Item>
            </Breadcrumb>
        </div>        

        <h2 id="DirectionTitle" className="mb-4">HTe Enterprise User Manual</h2>

        <Content/>
    </>
    );
};

export default Directions;