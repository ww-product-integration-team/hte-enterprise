import { useState } from 'react';
import { Link } from 'react-router-dom'
import { Drawer, CssBaseline, Toolbar, List, ListItem, ListItemIcon, ListItemText, Collapse } from '@material-ui/core';
import { Dashboard, AccountTree, PersonAdd, LocalGroceryStoreOutlined, PlaylistAdd, MoreVert, Settings, ListAlt, Info } from '@material-ui/icons';
import RuleIcon from '@mui/icons-material/Rule';
import { IconButton, Badge, Menu, MenuItem } from '@material-ui/core';
import AppsIcon from '@mui/icons-material/Apps';
import ScaleIcon from '../icons/ScaleIcon';
import { Handyman, Login } from '@mui/icons-material';

export default function SideBar() {

    // function getDomainName(){
    //     if(user.domain.domainId == null){return "Unknown"}

    //     switch(user.domain.type){
    //         case "BANNER":
    //             let banners = store.getState().asset.banners
    //             let domainBanner = banners.find(obj => {
    //                 return obj.bannerId === user.domain.domainId
    //             })

    //             if(domainBanner == null){return "BANNER - NOT FOUND"}
    //             return "BANNER - " + domainBanner.bannerName
    //         case "REGION":
    //             let regions = store.getState().asset.regions
    //             let domainRegion = regions.find(obj => {
    //                 return obj.regionId === user.domain.domainId
    //             })

    //             if(domainRegion == null){return "REGION - NOT FOUND"}
    //             return "REGION - " + domainRegion.regionName
    //         case "STORE":
    //             let stores = store.getState().asset.stores
    //             let domainStore = stores[user.domain.domainId]

    //             if(domainStore == null){return "STORE - NOT FOUND"}
    //             return "STORE - " + domainStore.storeName
    //         case "DEPARTMENT":
    //             return "DEPARTMENT"
    //         case "PROFILE":
    //             return "PROFILE"
    //         default:
    //             if(AccessActions.DEFAULT_ADMIN_DOMAIN_ID == user.domain.domainId){
    //                 return "ADMIN"
    //             }else if(AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID == user.domain.domainId){
    //                 return "UNASSIGNED"
    //             }
    //             else{
    //                 return "INVALID DOMAIN"
    //             }

    //     }

    //     return "NOT FOUND"
    // }





    // ============ START CHANGE ACCOUNT INFO SECTION FOR DEMO =====================

    // const [showAlert, setShowAlert] = useState([false, "Something went wrong!"]) // If there's an error communicating with backend or account already exists
    // const [showSuccess, setShowSuccess] = useState([false, "Something went wrong!"]) // Successful account creation
    // Current Access Level in Select tag 
    // const [currentDomain, setCurrentDomain] = useState<number>(-1);         // Current Access Level in Select tag 

    // const individualRoles = useAppSelector((state) => state.access.roles);
    // const [filteredRoles, setFilteredRoles] = useState([]);         // Current Access Level in Select tag 

    // let selectedRoles = []

    // let accessLevels = [
    //     createData('Admin', 4),
    //     createData('Banner Manager', 3),
    //     createData('Store Manager', 2),
    //     createData('Operator', 1),
    //     createData('Custom', 0),
    // ]

    // let accessDomains = [
    //     createData('Admin Level', 2),
    //     createData('Banner', 1),
    //     createData('Store', 0),
    // ]

    // let bannerList = useAppSelector((state) => state.asset.banners);
    // let storeDict = useAppSelector((state) => state.asset.stores);

    // // Remove the default banner from select tag
    // bannerList = bannerList.filter(banner => 
    //     banner.bannerId !== "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
    // )

    // // Remove the default store from select tag
    // let storeList = Object.values(storeDict)
    // storeList = storeList.filter(store => 
    //     store.storeId !== "cccccccc-cccc-cccc-cccc-cccccccccccc"
    // )

    // const handleView = (e : React.ChangeEvent<HTMLSelectElement>) => {
    //     const accessIndex = accessLevels[e.target.options.selectedIndex - 1].accessLevelId;
    //     // logger("accessIndex1: ", accessIndex)

    //     setCurrentLevel(accessIndex);
    //     if (accessIndex !== 0)  setCurrentDomain(-1)    // Reset the custom domain level when deselected
    // };

    // const handleViewDomain = (e: React.ChangeEvent<HTMLSelectElement>) => {
    //     const accessIndex2 = accessDomains[e.target.options.selectedIndex - 1].accessLevelId;
    //     // logger("accessIndex2: ", accessIndex2)

    //     setCurrentDomain(accessIndex2);
    // };

    // const handleCreate = async (data) => {
    //     // logger("Filtered Roles: ", filteredRoles)
    //     logger.info("Request Data: (SideBar) ", data)
    //     setShowAlert([false, "Something went wrong!"])
    //     setShowSuccess([false, "Something went wrong!"])

    //     // Copy and reverse the array for our usage
    //     let reversedLevels = accessLevels.slice(0)
    //     reversedLevels.reverse()

    //     // logger("ReversedLevels: ", reversedLevels[currentLevel].name)
    //     let updatedData;

    //     switch (reversedLevels[currentLevel].name) {
    //         case 'Admin':
    //             updatedData = {
    //                 id: currentUser.id,
    //                 accessLevel: 4000,
    //                 domain: {
    //                   domainId: DEFAULT_ADMIN_DOMAIN_ID
    //                 },
    //                 enabled: currentUser.enabled,
    //                 firstName: currentUser.firstName,
    //                 lastName: currentUser.lastName,
    //                 username: currentUser.username
    //             }
    //             break

    //         case 'Banner Manager':
    //             logger.debug("Actions for Banner Managers")
    //             updatedData = {
    //                 id: currentUser.id,
    //                 accessLevel: 3000,
    //                 domain: {
    //                   domainId: data.bannerSelected
    //                 },
    //                 enabled: currentUser.enabled,
    //                 firstName: currentUser.firstName,
    //                 lastName: currentUser.lastName,
    //                 username: currentUser.username
    //             }
    //             break

    //         case 'Store Manager':
    //             logger.debug("Actions for Store Managers")
    //             updatedData = {
    //                 id: currentUser.id,
    //                 accessLevel: 2000,
    //                 domain: {
    //                   domainId: data.storeSelectedManager
    //                 },
    //                 enabled: currentUser.enabled,
    //                 firstName: currentUser.firstName,
    //                 lastName: currentUser.lastName,
    //                 username: currentUser.username
    //             }
    //             break

    //         case 'Operator':
    //             logger.debug("Actions for Operators")
    //             updatedData = {
    //                 id: currentUser.id,
    //                 accessLevel: 1000,
    //                 domain: {
    //                   domainId: data.storeSelectedOperator
    //                 },
    //                 enabled: currentUser.enabled,
    //                 firstName: currentUser.firstName,
    //                 lastName: currentUser.lastName,
    //                 username: currentUser.username
    //             }
    //             break

    //         case 'Custom':
    //             logger.debug("Actions for Custom roles")
    //             filteredRoles.map((role, index) => {
    //                 if (data.customRoles[index])    selectedRoles.push(role)
    //             })

    //             if (selectedRoles.length === 0) {
    //                 setShowAlert([true, "Please select at least one custom permission for this account."])
    //             }

    //             else {
    //                 let userDomain = data.domainSelected ? data.domainSelected : DEFAULT_ADMIN_DOMAIN_ID

    //                 updatedData = {
    //                     id: currentUser.id,
    //                     accessLevel: 0,
    //                     domain: {domainId: userDomain},
    //                     roles: selectedRoles,
    //                     enabled: currentUser.enabled,
    //                     firstName: currentUser.firstName,
    //                     lastName: currentUser.lastName,
    //                     username: currentUser.username
    //                 }
    //             }
    //             break

    //         default: 
    //             return (null)
    //     }

    //     logger.debug("Updated Data (SideBar): ", updatedData)

    //     if (updatedData) {
    //         let reqRegisterID = Date.now()
    //         dispatch(LoginAPI.createAccount(reqRegisterID, updatedData))
    //         dispatch(AccessActions.setAccessTokens({}, "LOGOUT"))
    //         dispatch(AccessActions.checkTokenStatus())

    //     }
    // };
    // ============ END CHANGE ACCOUNT INFO SECTION FOR DEMO =====================

    const userAccountMenuId = 'userAccountMenuId';
    const [anchorEl, setUserAccAnchor] = useState(null);
    const handleMenuClose = () => {
        setUserAccAnchor(null);
        handleMobileMenuClose();
    };
    const renderUserAccountMenu = (
        <Menu
            id={userAccountMenuId}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            getContentAnchorEl={null}
            keepMounted
            onClose={handleMenuClose}
        >

            <Link to="/RequestLicense">
                <MenuItem onClick={handleMenuClose}>
                    Register
                </MenuItem>
            </Link>

            <Link to="/Login">
                <MenuItem onClick={handleMenuClose}>
                    Log In
                </MenuItem>
            </Link>

            <Link to="/ChangePassword">
                <MenuItem onClick={handleMenuClose}>
                    Change Password
                </MenuItem>
            </Link>

            <Link to="/ForgotPassword">
                <MenuItem onClick={handleMenuClose}>
                    Forgot Password
                </MenuItem>
            </Link>

            <Link to="/ResetPassword">
                <MenuItem onClick={handleMenuClose}>
                    Reset Password
                </MenuItem>
            </Link>
        </Menu>
    );

    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);   // Show/hide profile menu (mobile)
    const handleMobileMenuOpen = (event: any) => { setMobileMoreAnchorEl(event.currentTarget) }
    const handleMobileMenuClose = () => { setMobileMoreAnchorEl(null) };
    const mobileMenuId = 'navigationBarMobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            id={mobileMenuId}
            open={isMobileMenuOpen}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            getContentAnchorEl={null}
            keepMounted
            onClose={handleMobileMenuClose}
        >

            <Link to="/">
                <MenuItem onClick={handleMobileMenuClose}>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <Dashboard />
                        </Badge>
                    </IconButton>
                    <p>Dashboard</p>
                </MenuItem>
            </Link>

            <Link to="/AssetList" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <AccountTree />
                        </Badge>
                    </IconButton>
                    <p>Asset List</p>
                </MenuItem>
            </Link>

            <Link to="/ScaleProfiles" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <ScaleIcon />
                        </Badge>
                    </IconButton>
                    <p>Scale Profiles</p>
                </MenuItem>
            </Link>

            <Link to="/DeptManagement" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <LocalGroceryStoreOutlined />
                        </Badge>
                    </IconButton>
                    <p>Departments</p>
                </MenuItem>
            </Link>

            <Link to="/Transactions" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <ListAlt />
                        </Badge>
                    </IconButton>
                    <p>Transactions</p>
                </MenuItem>
            </Link>

            <Link to="/Settings" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <Settings />
                        </Badge>
                    </IconButton>
                    <p>Settings</p>
                </MenuItem>
            </Link>

            {/* <Link to = "/Licenses" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <BadgeIcon />
                        </Badge>
                    </IconButton>
                    <p>Licenses</p>
                </MenuItem>
            </Link> */}

            {/* <MenuItem onClick={handleUserAccountMenuOpen}>
                <IconButton color="inherit" >
                    <Badge overlap="rectangular" color="secondary">
                        <AccountBox />
                    </Badge>
                </IconButton>
                <p>User Account</p>
            </MenuItem> */}

            <Link to="/AboutHTe" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <Info />
                        </Badge>
                    </IconButton>
                    <p>About HTe</p>
                </MenuItem>
            </Link>

            <Link to="/Utilities" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <Info />
                        </Badge>
                    </IconButton>
                    <p>Utilities</p>
                </MenuItem>
            </Link>

            <Link to="/Directions" onClick={handleMobileMenuClose}>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge overlap="rectangular" color="secondary">
                            <RuleIcon />
                        </Badge>
                    </IconButton>
                    <p>User Instructions</p>
                </MenuItem>
            </Link>
        </Menu>

    );

    const [accountOpen] = useState(false);

    return (
        <>
            <div className="sidebarSectionMobile">
                <Toolbar />
                <CssBaseline />
                <IconButton className="customHoverFocus"
                    aria-label="show more"
                    aria-controls={"sideBarMobile"}
                    aria-haspopup="true"
                    onClick={handleMobileMenuOpen}>
                    <MoreVert />
                </IconButton>
            </div>

            <div className="sidebarSectionDesktop">
                <CssBaseline />
                <Drawer className="drawer" variant="permanent"
                    classes={{ paper: "drawerPaper" }}
                >
                    <Toolbar />
                    <div className="drawerContainer">
                        {/* Top Section of SideBar */}
                        <List>
                            {/* Link to Dashboard overview */}
                            <ListItem button component={Link} to='/' key='Dashboard'>
                                <ListItemIcon><Dashboard /></ListItemIcon>
                                <ListItemText primary='Dashboard' />
                            </ListItem>

                            {/* Link to Asset List */}
                            <ListItem button component={Link} to='/AssetList' key='AssetList'>
                                <ListItemIcon><AccountTree /></ListItemIcon>
                                <ListItemText primary='Asset List' />
                            </ListItem>

                            {/* Link to Scale Profiles */}
                            <ListItem button component={Link} to='/ScaleProfiles' key='ScaleProfiles'>
                                <ListItemIcon><ScaleIcon /></ListItemIcon>
                                <ListItemText primary='Scale Profiles' />
                            </ListItem>

                            {/* Link to Department Management */}
                            <ListItem button component={Link} to='/DeptManagement' key='DeptManagement'>
                                <ListItemIcon><LocalGroceryStoreOutlined /></ListItemIcon>
                                <ListItemText primary='Departments' />
                            </ListItem>

                            {/* Link to Transaction Management */}
                            <ListItem button component={Link} to='/Transactions' key='Transactions'>
                                <ListItemIcon><ListAlt /></ListItemIcon>
                                <ListItemText primary='Transactions' />
                            </ListItem>
                            
                            {/* Link to Apps Pages */}
                            <ListItem button component={Link} to='/Apps' key='Apps'>
                                <ListItemIcon><AppsIcon /></ListItemIcon>
                                <ListItemText primary='Apps' />
                            </ListItem>

                            {/* Link to Settings/Config Pages */}
                            <ListItem button component={Link} to='/Settings' key='Settings'>
                                <ListItemIcon><Settings /></ListItemIcon>
                                <ListItemText primary='Settings' />
                            </ListItem>

                            {/* Link to Utilities Page */}
                            <ListItem button component={Link} to='/Utilities' key='Utilities'>
                                <ListItemIcon><Handyman /></ListItemIcon>
                                <ListItemText primary='Utilities' />
                            </ListItem>

                            {/* Link to About Page */}
                            <ListItem button component={Link} to='/AboutHTe' key='AboutHTe'>
                                <ListItemIcon><Info /></ListItemIcon>
                                <ListItemText primary='About HTe' />
                            </ListItem>

                            {/* Link to Directions */}
                            <ListItem button component={Link} to='/Directions' key='Directions'>
                                <ListItemIcon><RuleIcon /></ListItemIcon>
                                <ListItemText primary='User Instructions' />
                            </ListItem>

                            <br />
                            <div className="p-2" style={{ textAlign: "center" }}>

                                {/* <Form onSubmit={handleSubmit(handleCreate)}>
                        <Form.Label>Access level for account:</Form.Label>     
                        <Form.Select required defaultValue="" id="accessLevel" 
                            onChange={(e) => handleView(e)}
                            onKeyDown={(e) => disableKey(e)}
                        >
                            <option value="" disabled>Select Access Level</option>
                            
                            {accessLevels.map(level => (
                                <option key={level.accessLevelId} value={level.accessLevelId}>
                                    {level.name}
                                </option>
                            ))}
                        </Form.Select>

                        {currentLevel === 4 &&      // Admin
                            <span className="fw-bold">NOTE: This gives the user visibility to everything on the site</span>
                        }

                        <br />
                        
                        {currentLevel === 3 &&      // Banner Manager
                        <>  
                            <FormSelect
                                label="Banner: "
                                id="bannerSelected"
                                name="bannerSelected"
                                placeholder="--Select a Banner--"
                                dataset={bannerList}
                                register={register}
                                objId="bannerId"
                                objName="bannerName"
                                validation={{required: "Please select a banner for this manager"}}
                                error={errors.bannerSelected}
                            />
                        </>}                                
                        
                        {currentLevel === 2 &&      // Store Manager
                        <>  
                            <FormSelect
                                label="Store: "
                                id="storeSelectedManager"
                                name="storeSelectedManager"
                                placeholder="--Select a Store--"
                                dataset={storeList}
                                register={register}
                                objId="storeId"
                                objName="storeName"
                                validation={{required: "Please select a store for this manager"}}
                                error={errors.storeSelectedManager}
                            />
                        </>}

                        {currentLevel === 1 &&      // Store Operator
                        <>  
                            <FormSelect
                                label="Store: "
                                id="storeSelectedOperator"
                                name="storeSelectedOperator"
                                placeholder="--Select a Store--"
                                dataset={storeList}
                                register={register}
                                objId="storeId"
                                objName="storeName"
                                validation={{required: "Please select a store for this operator"}}
                                error={errors.storeSelectedOperator}
                            />
                        </>}

                        {currentLevel === 0 &&      // Custom
                        <>  
                            <Form.Label>Domain Level:</Form.Label>     
                            <Form.Select required defaultValue="" id="domainLevel" 
                                onChange={(e) => handleViewDomain(e)}
                                onKeyDown={(e) => disableKey(e)}
                                // {...register("accessLevel", {required: "Please select an Access Level"})}
                            >
                                <option value="" disabled>--Select a Domain Level--</option>
                                
                                {accessDomains.map(domain => (
                                    <option key={domain.accessLevelId} value={domain.accessLevelId}>
                                        {domain.name}
                                    </option>
                                ))}
                            </Form.Select>


                            {currentDomain === 2 &&     // Admin Level
                                <div className="fw-bold">NOTE: This gives the user visibility to everything on the site</div>
                            }

                            <br />

                            {currentDomain === 1 &&     // Banner
                                <FormSelect
                                    label="Banner: "
                                    id="domainSelected"
                                    name="domainSelected"
                                    placeholder="--Select a Banner--"
                                    dataset={bannerList}
                                    register={register}
                                    objId="bannerId"
                                    objName="bannerName"
                                    validation={{required: "Please select a banner for this manager"}}
                                    error={errors.domainSelected}
                                />
                            }

                            {currentDomain === 0 &&     // Store
                                <FormSelect
                                    label="Store: "
                                    id="domainSelected"
                                    name="domainSelected"
                                    placeholder="--Select a Store--"
                                    dataset={storeList}
                                    register={register}
                                    objId="storeId"
                                    objName="storeName"
                                    validation={{required: "Please select a store for this operator"}}
                                    error={errors.domainSelected}
                                />
                            }

                        </>}

                        <div className="formButtons">
                            <button className="formButton1 createButton" type="submit" >
                                <Edit />
                                <span> Edit Account </span> 
                            </button>
                        </div>         
                        </Form> */}

                            </div>

                            {/* <ListItemText 
                            className="m-3"
                            primary={`Logged in: ${state.access.loggedIn ? `True` : `False`}`} 
                        />

                        {user.roles &&
                        <>
                            <ListItemText 
                                className="m-3"
                                primary={`Name: ${user.firstName} ${user.lastName}`} 
                            />

                            <ListItemText 
                                className="m-3"
                                primary={`Domain: ${getDomainName()}`} 
                            />
                            
                            <ListItemText 
                                className="m-3"
                                primary={`Access level: ${user.accessLevel}`} 
                            />
                            <ListItemText 
                                className="mx-3"
                                primary="Roles: "
                            />

                            <ListItemText 
                                className="mx-3"
                                primary={user.roles.map((role, index) => (
                                    <li key={index}> {role.name} </li>
                                ))}                         
                            />
                        </>
                        } */}
                        </List>

                    </div>
                </Drawer>
                {/* <Toolbar /> */}
            </div>

            {renderMobileMenu}
            {renderUserAccountMenu}
        </>
    );
}
