import React, { useState, useEffect } from 'react';
import DataTable from './DataTable';
import { Form, FormCheck} from '@themesberg/react-bootstrap';
import { Tooltip } from 'antd';
import { IconButton, Menu, Typography } from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import RefreshIcon from '@mui/icons-material/Refresh';
import { useDispatch } from 'react-redux';
import { AccessActions, AssetActions, useAppSelector } from '../../../state';
import { ScaleAPI } from '../../api';
import { notify } from 'reapop';

export interface HeadCellTypes{
    id: string
    sorted: boolean
    searchable: boolean
    label: string
    isShowing: boolean
    mandatory: boolean
}

export interface VirtualTableProps{
    tableName : string,
    dataSet : any[], 
    headCells : HeadCellTypes[],
    initialSortedBy : {[key: string] : string}, 
    renderRows ?: number, 
    dataIdentifier ?: string , 
    selectedRows ?: string[],  
    useToolbar ?: boolean, 
    saveKey : string,
    dispatchedUrls ?: any[],
    maxHeight ?: string,
    setSelected ?: React.Dispatch<React.SetStateAction<string[]>>
    changeReduxSelected ?: boolean
    changeReduxUpgradeSelected ?: boolean
    batchUpgrade?: boolean
    noSearchbar ?: boolean
}

export default function VirtualTable(props : VirtualTableProps){
    let {tableName, dataSet, headCells, initialSortedBy, renderRows, dataIdentifier, 
        selectedRows, useToolbar, saveKey, dispatchedUrls, maxHeight, setSelected, changeReduxSelected, changeReduxUpgradeSelected, batchUpgrade, noSearchbar} = props
    const render = renderRows ?? 25
    const dataId = dataIdentifier ?? "id"
    const selected = selectedRows ?? []
    const toolbar = useToolbar ?? true
    const [loading, setloading] = useState<number>(0) //If we're loading the next chunk of pages
    const [header, setHeader] = useState<HeadCellTypes[]>([])
    const [currentData, setCurrentData] = useState<any[]>(dataSet) //Total filtered data, i.e. if something has been searched
    const [dataTable, setDataTable] = useState<HTMLTableElement | null>(null)
    const [page, setPage] = useState<number>(1) //Current Page that we're displaying, 3 Pages are always displayed, 1 Before the current page and 1 after
    const [sortedBy, setSortedBy] = useState<{[key: string] : string}>(initialSortedBy)
    const [pageData, setPageData] = useState([...createPage(dataSet)]) //Currently displayed Rows
    const [tableState, setTableState] = useState("")
    const [query, setQuery] = useState<string>('')
    const dispatch = useDispatch()

    const reduxPrimaryScales : string[] = useAppSelector((state)=>state.asset.primaryScales)
    const [primaryScales, setPrimaryScales] = useState<string[]>(reduxPrimaryScales)
    //=============================================================================================
    //Checkbox Functionality
    const [rowsChecked, setRowsChecked] = useState<string[]>(selected)
    function onSelectAllClicked(e : React.ChangeEvent<HTMLInputElement>) {
        let checked : string [] = []
        
        if(e.target.checked){
            checked = (dataSet.map((item) => {
                return item[dataId]}))
        }
        else{
            checked = []
        }
        
        setRowsChecked([...checked])
    }
    function handleSetPrimary(e : React.ChangeEvent<HTMLInputElement>, row : any){
        let tempPrimaryScales :string[] = []
        const newID = Date.now()
        if (primaryScales && primaryScales.length) {
            if(e.target.checked){
                //Adding id to list
                tempPrimaryScales = ([...primaryScales, row[dataId]])
                dispatch(ScaleAPI.setPrimaryScale(newID, row[dataId], true, AccessActions.DEFAULT_ADMIN_DOMAIN_ID, null, null, null))
                dispatch(notify("Setting scale to primary in database", "info"))
            }
            else{
                tempPrimaryScales = ([...primaryScales].filter((item) => item !== row[dataId]))
                dispatch(ScaleAPI.setPrimaryScale(newID, row[dataId], false, AccessActions.DEFAULT_ADMIN_DOMAIN_ID, null, null, null))
                dispatch(notify("Setting scale to secondary in database", "info"))
            }
        } else {
            tempPrimaryScales = ([row[dataId]])
            dispatch(ScaleAPI.setPrimaryScale(newID, row[dataId], true, AccessActions.DEFAULT_ADMIN_DOMAIN_ID, null, null, null))
            dispatch(notify("Setting scale to primary in database", "info"))
        }
        setPrimaryScales([...tempPrimaryScales])
        dispatch(AssetActions.setReduxPrimaryScales([...tempPrimaryScales]))
    }
    function handleRowChecked(e : React.ChangeEvent<HTMLInputElement>, row : any){
        let checked :string[] = []
        if(e.target.checked){
            //Adding id to list
            checked = ([...rowsChecked, row[dataId]])
        }
        else{
            checked = ([...rowsChecked].filter((item) => item !== row[dataId]))   
        }
        setRowsChecked([...checked])
    }

    useEffect(() => {
        if(saveKey){
            //Check to see if there is already a saved item
            let itemStr = localStorage.getItem(saveKey)
            if(!itemStr){
                localStorage.setItem(saveKey, JSON.stringify(headCells))
                itemStr = localStorage.getItem(saveKey)
            }

            if(!itemStr){return}
            //Check to see if the saved item is valid
            //i.e. has the default been updated and if so update local storage
            let saveHeadCells = JSON.parse(itemStr)
            let localHeadIds : string[] = []
            let defaultHeadIds : string[] = []

            saveHeadCells.map((item : HeadCellTypes) =>{
                localHeadIds.push(item.id)
                return null
            })

            headCells.map((item : HeadCellTypes) =>{
                defaultHeadIds.push(item.id)
                return null
            })

            //If the Default Heads change i.e. developer changes the head cells 
            if(localHeadIds.toString() !== defaultHeadIds.toString()){
                localStorage.setItem(saveKey, JSON.stringify(headCells))
                itemStr = localStorage.getItem(saveKey)
            }

            if(!itemStr){return}

            saveHeadCells = JSON.parse(itemStr)
            setHeader(saveHeadCells)
        }
        else{
            setHeader(headCells)
        }
    }, [])
    useEffect(() => {
        setHeader(headCells)
    }, [headCells])

    useEffect(() => {
        if(changeReduxSelected){
            dispatch(AssetActions.setSelectedItems(rowsChecked))
        }
        if(changeReduxUpgradeSelected){
            dispatch(AssetActions.setUpgradeSelected(rowsChecked))
        }
        if(setSelected){
            setSelected(rowsChecked)
        }
    }, [rowsChecked])

    function createPage(dataSet : any[], per = render, page = 1) {
        // removed functionality, see <Datatable/>
        let acc = dataSet.slice(0, per * (page + 1))
        // console.log("create page data:", acc)
        // console.log( per * (page + 1))
        return acc
        
        //console.log(" Page Data" + "  per " + per + "  page:" + page + " slice " + per * (page - 1) + " -> " + per*(page + 1))
        if(typeof dataSet[Symbol.iterator] === 'function')
            return dataSet.slice(0, per * (page + 1)); // seems to break depending on dataSet
        else 
            return dataSet
    }

    useEffect(() => {
        setCurrentData(dataSet)

        setPageData([...createPage(dataSet)])
        setTableState("")

        setPage(0)
        setQuery('')

        const queryForm = document.getElementById("specifyPLU-"+tableName) as HTMLInputElement
        if(queryForm){
            queryForm.value = '';
        }
    }, [dataSet])

    function naturalCompare(a : any, b : any) {
        let ax : any[] = [], bx : any[] = [];
    
        a.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
        b.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
        
        while(ax.length && bx.length) {
            let an = ax.shift();
            let bn = bx.shift();
            let nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
            if(nn) return nn;
        }
    
        return ax.length - bx.length;
    }


    useEffect(() => {
        if (currentData.length === 0) {
            setCurrentData(dataSet)
        }
        const sortKey = Object.keys(sortedBy)[0];
        const direction = sortedBy[sortKey]; 
        let sorted = [...currentData.sort((a, b) => {
            if(direction === 'ascending'){
                return naturalCompare(String(a[sortKey]),String(b[sortKey]))
            }
            else{
                return naturalCompare(String(b[sortKey]),String(a[sortKey]))
            }
        })]
        setPageData(
            [...createPage(sorted, render, page)]
        )
    }, [sortedBy]);

    useEffect(() => {
        let newTempDataSet = search(dataSet)
        setCurrentData(newTempDataSet)
        setPage(1)
        setPageData([...createPage(newTempDataSet, render, 1)])
    }, [query]);

    //=============================================================================================
    //Search Function
    function search(data : any[]) : any[]{
        let searchableColumns = header.filter(item => item.searchable).map((item) => {return item.id})
        if(data.length === 0) return []
        return data.filter((row) =>{
        return searchableColumns.some(
            (column) => row[column] &&
            row[column]
                .toString()
                .toLowerCase()
                .indexOf(query.toLowerCase()) > -1,
        )},
        );
    }

    //=============================================================================================
    //Load New windowed dataset
    const loadMore = (addPage : number, tableElement : HTMLTableElement) => {
    
        if (loading !== 0) return;

        if(!dataTable && tableElement){
            setDataTable(tableElement)
        }
        setloading(addPage)
    }

    useEffect(() => {
        if(loading === 0) return
        let newPage = page + loading
        if(newPage < 1) newPage = 1

        //Only load a new page when 
        //1. The newly requested page does not overshoot the current data size
        //2. The dataset is larger than window size render*2
        if((render * (newPage+1)) < (currentData.length + render) && currentData.length > render*2){
            setPageData([
            ...createPage(currentData, render,  newPage),
            ])
            setPage(newPage)
        }

        //Give a half second delay before loading another page, 
        //This prevents table from flickering all over the place
        new Promise(function(resolve, reject) {

            setTimeout(() => resolve("done"), 500);
        })
        .then(function(result){
            setloading(0)
        })

    }, [loading])

    //=============================================================================================
    //Column Menu

    const [anchorEl, setAnchorEl] = useState(null);
    const isMenuOpen = Boolean(anchorEl); 
    const handleProfileMenuOpen = (event) => {setAnchorEl(event.currentTarget)};
    const handleMenuClose = () => {setAnchorEl(null)};

    const handleCheck = (e : React.ChangeEvent<HTMLInputElement>, index : number) => {
        let newHeader = [...header]
        newHeader[index].isShowing = e.target.checked
        setHeader(newHeader)
        if(saveKey){
            localStorage.setItem(saveKey, JSON.stringify(newHeader))
        }
    }

    const renderMenu = (
        <Menu
          id="selectColumns"
          anchorEl={anchorEl}
          open={isMenuOpen}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'center' }}
          getContentAnchorEl={null}
          keepMounted
          onClose={handleMenuClose}
        >
            {/* For every column */}
            {header.map((column, index) => {
                // If the column is not a mandatory column (filename, shortDesc, actions)
                if (header[index].mandatory === false){
                    // Display the option to disable the column
                    return (
                        <FormCheck key={column.id} type="checkbox" className="d-flex p-3" >
                            <FormCheck.Label className="px-0 customCheckbox">
                                <FormCheck.Input onChange ={(e) => handleCheck(e, index)} defaultChecked={column.isShowing}/>
                                {column.label}
                            </FormCheck.Label>
                        </FormCheck>
                    )
                }
                return null
            })}
        </Menu>    
    );


return (
    <>
    {toolbar ?
        <div className='toolbar' style={{float: noSearchbar?"right":"none"}}>
            {!noSearchbar && 
                <Form.Control
                    id={"specifyPLU-"+tableName}
                    type="text" 
                    className="tableSearchBar fw-bold"
                    placeholder='Type here to filter results'
                    onChange={(val) => {

                        setPage(1)
                        if(dataTable){
                            dataTable.scrollTop = 0
                        }
                        setQuery(val.target.value ?? "")
                    }}
                />
            }

            <Tooltip placement="top" title={<Typography>Filter List</Typography>}>
                <IconButton 
                edge="end"
                aria-label="filter list"
                aria-controls="selectColumns"
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                >
                <FilterListIcon />
                </IconButton>
            </Tooltip>
            {dispatchedUrls?
                <Tooltip placement="top" title={<Typography>Refresh Table</Typography>}>
                    <IconButton 
                    aria-label="refresh table"
                    onClick={() => {
                        setPage(0)
                        setQuery("")
                        let table = document.getElementById("specifyPLU-"+tableName) as HTMLInputElement
                        table.value = ""
                        
                        dispatchedUrls?.map((url) => {
                            dispatch(url())
                            return null
                        })
                        setTableState("Reloading...")
                        //This is a quick fix for an annoying reloading issue, 
                        //TODO tie this to when new info comes in
                        new Promise(function(resolve, reject) {
                            // the function is executed automatically when the promise is constructed
                        
                            // after 1 second signal that the job is done with the result "done"
                            setTimeout(() => resolve("done"), 2000);
                        })
                        .then(function(result){
                            setTableState("")
                        })

                    }}
                    >
                    <RefreshIcon />
                    </IconButton>
                </Tooltip>
            : 
            null}
            {renderMenu}
        </div>
    : null
    }
    {maxHeight ? 
        <div id={tableName} className='virtual-table-container' style={{maxHeight: maxHeight, clear: noSearchbar?"both":"none"}}>

        <DataTable
            dataId = {dataId}
            tableName={tableName}
            loadMore={loadMore}
            data={ pageData.length > 0 ? pageData : dataSet }
            header={header}
            sortedBy={sortedBy}
            setSortedBy={setSortedBy}
            tableState={tableState}
            numSelected={rowsChecked.length}
            rowCount={dataSet.length}
            onSelectAll={onSelectAllClicked}
            onCheck={handleRowChecked}
            rowsChecked={rowsChecked}
            setPrimary={handleSetPrimary}
            primary={primaryScales}
            batchUpgrade={batchUpgrade}
        />
        </div> :

        <div id={tableName} className='virtual-table-container' style={{clear: noSearchbar?"both":"none"}}>
            <DataTable
                dataId = {dataId}
                tableName={tableName}
                loadMore={loadMore}
                data={ pageData.length > 0 ? pageData : dataSet }
                header={header}
                sortedBy={sortedBy}
                setSortedBy={setSortedBy}
                tableState={tableState}
                numSelected={rowsChecked.length}
                rowCount={dataSet.length}
                onSelectAll={onSelectAllClicked}
                onCheck = {handleRowChecked}
                rowsChecked = {rowsChecked}                   
                setPrimary = {handleSetPrimary}
                primary = {primaryScales}     
            />
        </div>
    }
    </>
);
}
  