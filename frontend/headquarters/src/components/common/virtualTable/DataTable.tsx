import React, { useEffect, useMemo, useRef, useState } from 'react';
import logger from '../../utils/logger';
import {RenderHeader, RenderRow} from './RenderData';
import './styles.css';
import { HeadCellTypes } from './VirtualTable';
import { v4 as uuidv4 } from 'uuid';
import { useAppSelector } from '../../../state';
import { FileForEvent } from '../../../types/asset/FileTypes';
import { CircularProgress, Typography } from '@material-ui/core';

export enum DeviceStatuses {
    unassigned="unassigned",awaitingUpload="awaiting_upload",awaitingPrimary="awaiting_primary",readyToUpload="ready_to_upload",uploadInProgress="upload_in_progress",uploadCompletedNoErrors="upload_completed_no_errors",
    uploadCompletedErrors="upload_completed_errors",uploadFailed="upload_failed",uploadNow="upload_now",fileUploadedAwaitingUpgrade="file_uploaded_awaiting_upgrade",readyToUpgrade="ready_to_upgrade",upgradeInProgress="upgrade_in_progress",
    postInstallProcess="post_install_process",upgradeCompletedNoErrors="upgrade_completed_no_errors",upgradeCompletedErrors="upgrade_completed_errors",upgradeFailed="upgrade_failed",upgradeNow="upgrade_now"
}

export interface DataTableProps{
    dataId : string
    tableName : string
    data : any[]
    loadMore : (addPage : number, tableElement : HTMLTableElement) => void
    header : HeadCellTypes[]
    sortedBy : {[key: string] : string}
    setSortedBy : React.Dispatch<React.SetStateAction<{[key: string]: string}>>
    tableState : string 
    numSelected : number
    rowCount : number
    onSelectAll : (e: React.ChangeEvent<HTMLInputElement>) => void
    onCheck : (e: React.ChangeEvent<HTMLInputElement>, row: any) => void
    rowsChecked : any[]    
    setPrimary : (e: React.ChangeEvent<HTMLInputElement>, row: any) => void
    primary : string[],
    batchUpgrade?: boolean
}

export default function DataTable(props: DataTableProps) {
    const { dataId, tableName, data, loadMore, header, sortedBy, setSortedBy, tableState, numSelected, rowCount, onSelectAll, onCheck, rowsChecked, setPrimary, primary, batchUpgrade} = props
    const ref = useRef(null);
    const reduxBatches = useAppSelector((state)=> state.asset.batches)
    const reduxFeatureFiles = useAppSelector(state=>state.profile.featureFiles)
    const reduxDebianFiles = useAppSelector(state=>state.profile.debianFiles)
    const reduxTarballFiles = useAppSelector(state=>state.profile.tarballFiles)
    const reduxInstallImageFiles = useAppSelector(state=>state.profile.installImageFiles)
    const reduxUpgradeDevices = useAppSelector((state)=> state.asset.upgradeDevices)
    const reduxUpgradeSelected =  useAppSelector((state)=> state.asset.upgradeSelected)
    const [upgradeSelected, setUpgradeSelected] = useState<string[]>([])
    const reduxHybridMultipath = useAppSelector(state=>state.config.hybridMultipath).hybridMultipath
    const [hybridMultipath, setHybridMultipath] = useState(reduxHybridMultipath === "true")
    const [upgradeDevices, setUpgradeDevices] = useState((reduxUpgradeDevices)? {...reduxUpgradeDevices} : {})
    const [upgradeFiles, setUpgradeFiles] = useState<FileForEvent[]>([])
    const [items, setItems] = useState(data)
    useEffect(()=>{
        // console.log("DataTable data:", data)
        setItems(data)
    },[data])
    //used to manage Edit Dialogs
    const [currentIP, setCurrentIP] = useState("")
    const [currentPrID, setCurrentPrID] = useState("")

    let batchIdArray: string[] = useMemo(()=>{
        let temp: string[] = []
        if(reduxBatches){
            let sortedBatches = Object.fromEntries(Object.entries(reduxBatches).sort(([,a],[,b]) => a.name.localeCompare(b.name, "en", {numeric: true})))
            Object.keys(sortedBatches).map(key => temp.push(reduxBatches[key].batchId))
        }
        return temp
    },[reduxBatches])
    useEffect(()=>{
        setUpgradeDevices(Object.assign({}, reduxUpgradeDevices))
    },[reduxUpgradeDevices])
    useEffect(()=>{
        setUpgradeSelected([...reduxUpgradeSelected])
    }, [reduxUpgradeSelected])
    useEffect(() => {
        let filesToAdd: FileForEvent[] = []
        reduxFeatureFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        reduxDebianFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        reduxTarballFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        reduxInstallImageFiles.forEach(file => file.profileId == null ? filesToAdd.push(file) : null)
        filesToAdd.sort((a,b) => a.filename.localeCompare(b.filename, 'en', {numeric: true}))
        setUpgradeFiles(filesToAdd)
    }, [reduxFeatureFiles, reduxDebianFiles, reduxTarballFiles, reduxInstallImageFiles])

    useEffect(() => {
        let element : HTMLTableElement | null = null
        new Promise(function(resolve, reject) {
            // the function is executed automatically when the promise is constructed
          
            // after 1 second signal that the job is done with the result "done"
            setTimeout(() => resolve("done"), 1000);
        })
        .then(function(result){
            element = document.getElementById(tableName) as HTMLTableElement
            if(!element){
                logger.error("Error cannot find Table Element")
                return
            }
            element.addEventListener('scroll', handleScroll);
        })

        return () => {
            if(element){
                element.removeEventListener('scroll', handleScroll);
            }
        }
    }, []);

    useEffect(() => {
        setHybridMultipath(reduxHybridMultipath === "true")
    }, [reduxHybridMultipath])

    function handleScroll(e : Event) {
        const element = document.getElementById(tableName) as HTMLTableElement
        if(element == null){
            logger.error("HandleScroll - Error cannot find Table Element")
            return
        }

        const scrollTop = element.scrollTop; //Scroll Distance from the top of the table
        const scrollHeight = element.scrollHeight; //Total Scroll distance
        const tableHeight = element.offsetHeight; //Height of the Table
        const thresh = 10; //Threshold to load new page (thresh Pixels from bottom start loading more) 
        //console.log("scrollTop: " + scrollTop + " tableHeight: " + tableHeight + " thresh:" + thresh + "  scrollHeight: " + scrollHeight)
        if (scrollHeight - (scrollTop + tableHeight)  < thresh)loadMore(1,element)
        else if(scrollTop < 100){loadMore(-1, element)}
    }

    return (
        <>
            <React.Fragment key={uuidv4()}>
                <table className='virtual-table table-checkbox' cellSpacing={0} cellPadding={0} key={tableName + "tablebody"}>
                    <thead key={uuidv4()}>
                        <tr  key={uuidv4()}>
                            <RenderHeader
                                dataId={dataId}
                                items = {items}
                                headerConfig={header}
                                sortedBy={sortedBy}
                                setSortedBy={setSortedBy}
                                numSelected={numSelected}
                                rowCount={rowCount}
                                reduxUpgradeSelected={upgradeSelected}
                                onSelectAllClick={onSelectAll}
                            />
                        </tr>
                    </thead>
                    {tableState === "" && items.length > 0 ?
                        <tbody ref={ref} key={tableName + "tablebody"}>{items.map((row) => 
                            RenderRow(dataId, header, row, onCheck, setPrimary, primary, rowsChecked, upgradeSelected,
                                {...upgradeDevices}, batchIdArray, reduxBatches, upgradeFiles,
                                currentIP, setCurrentIP, currentPrID, setCurrentPrID, batchUpgrade, hybridMultipath)
                            )}
                        </tbody> : null
                    }
                </table>

                {
                    tableState !== "" ? 
                    <label className="noDataLabel">
                        <CircularProgress />
                        <Typography variant="body1" component="span" style={{marginLeft: "0.5rem", position: "relative", top: "-0.9rem"}}>{tableState}</Typography>
                    </label> :
                    items.length === 0 ? 
                    <label className="noDataLabel">No Data</label> : 
                    null
                }
            </React.Fragment>
        </>
    );
}
