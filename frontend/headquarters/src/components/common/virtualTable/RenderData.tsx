import TableHeader from "./TableHeader";
import React from 'react';
import RegionIcon from '@material-ui/icons/RoomOutlined';
import StoreIcon from '@material-ui/icons/Store';
import DepartmentIcon from '@material-ui/icons/LocalGroceryStoreOutlined';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import { Checkbox, IconButton, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import ScaleIcon from "../../icons/ScaleIcon";
import {MoreHoriz} from '@material-ui/icons';
import logger from "../../utils/logger";
import { HeadCellTypes } from "./VirtualTable";
import { DiscriminatorType, instanceOf } from "../../../types/storeTypes";
import { AssetActions, store } from "../../../state";
import { Banner } from "../../../types/asset/AssetTypes";
import { v4 as uuidv4 } from 'uuid';
import { formatBytes } from "../../utils/utils";
import Form from 'react-bootstrap/Form';
import { Batch, UpgradeDevice } from "../../upgradeManager/UpgradeManager";
import { format } from "date-fns";
import { DeviceStatuses } from "./DataTable";
import { CircularProgress } from '@mui/joy';
import { Button } from "react-bootstrap";
import { EditRule } from "../../appSettings/CreateRuleForAutoAssigner";
import { Tooltip } from '@mui/material';
import { prodType } from "../../prodsTable/prodsTable";
import { EditProd } from "../../prodsTable/editProd";
import { FileForEvent } from "../../../types/asset/FileTypes";
import { useDispatch } from "react-redux";
import { useRspHandler } from "../../utils/ResponseProvider";
import { UpgradeAPI } from "../../api";
import { ResponseType } from "../../../types/storeTypes"

export interface RenderHeaderProps {
    dataId : string
    items : any[]
    headerConfig : HeadCellTypes[]
    sortedBy: {[key: string] : string}
    setSortedBy : React.Dispatch<React.SetStateAction<{[key: string]: string}>>
    numSelected : number
    rowCount : number
    reduxUpgradeSelected : string[]
    onSelectAllClick : (e : React.ChangeEvent<HTMLInputElement>) => void
}

enum Numerator {
    "Awaiting Upload" = 1,
    "Upload In Progress" = 2,
    "Awaiting Upgrade" = 3,
    "Upgrade In Progress" = 4,
    "Complete" = 5,
}
function getProgressChart(deviceStatus : string, statusText :string){
    let numerator = (Numerator[deviceStatus] ? Numerator[deviceStatus] : 0)
    let denominator = 5 // num of steps
    let progressChart = <CircularProgress/>
    if(deviceStatus === "Failed"){
        progressChart =
            <CircularProgress color = "danger" sx={{ '--CircularProgress-size': '55px' }} determinate value = {100}>
                <Typography> <ErrorIcon style={{ fontSize: 30 }} /></Typography>
            </CircularProgress>
    }
    else if (deviceStatus === "Complete"){
        progressChart =
            <CircularProgress color = "success" determinate sx={{ '--CircularProgress-size': '55px' }} value = {100}>
                <Typography> {numerator} / {denominator}</Typography>
            </CircularProgress>
    }
    else{
        progressChart=
                <CircularProgress determinate sx={{ '--CircularProgress-size': '55px' }} value = {numerator/denominator * 100}>
                    <Typography> {numerator} / {denominator}</Typography>
                </CircularProgress>
    }
    if(statusText){
        return(
        <Tooltip title = {
            <Typography>{statusText}</Typography>
        }>
            {progressChart}
        </Tooltip>
        )
    }
    else{
        return (progressChart)
    }

        
}

//=============================================================================================
//Render the header cells
export function RenderHeader(props: RenderHeaderProps){
    const {headerConfig, items, sortedBy, setSortedBy, numSelected, rowCount, reduxUpgradeSelected, onSelectAllClick} = props
    return (
        <React.Fragment key={uuidv4()}>
            {headerConfig.map((headCell, index) => {
                if(headCell.isShowing){
                    if(headCell.id === 'checkmark' ){
                        return(
                            <th className='tableHead' key={uuidv4()}>
                                <TableHeader
                                    label={
                                            <Checkbox className="table-checkbox"
                                                
                                                indeterminate={numSelected > 0 && numSelected < rowCount}
                                                checked={rowCount > 0 && numSelected === rowCount}
                                                onChange={onSelectAllClick}
                                                inputProps={{ 'aria-label': 'select all products' }}
                                            />
                                    }
                                    sortedBy={headCell.sorted ? sortedBy : {}}
                                    sort={headCell.sorted ? { key: headCell.id, changer: setSortedBy }: undefined}
                                    headerId = {"headcell-"+headCell.id}
                                />
                            </th>
                        )
                    }
                    else if(headCell.id === 'checkmarkUpgrade' ){
                        // true if one upgrade id exists in upgrade Selected
                        let indeterminate = items.map((row)=>{if(row) return reduxUpgradeSelected.includes(row["upgradeId"])}).includes(true)
                        // true if all upgrade ids exist in upgrade selected
                        let checked = !items.map((row)=>{if(row) return reduxUpgradeSelected.includes(row["upgradeId"])}).includes(false)
                        return(
                            <th className='tableHead' key={uuidv4()}>
                                <TableHeader
                                    label={
                                            <Checkbox className="table-checkbox"
                                                indeterminate={!checked && indeterminate}
                                                checked={checked}
                                                onChange={onSelectAllClick}
                                                inputProps={{ 'aria-label': 'select all products' }}
                                            />
                                    }
                                    sortedBy={headCell.sorted ? sortedBy : {}}
                                    sort={headCell.sorted ? { key: headCell.id, changer: setSortedBy }: undefined}
                                    headerId = {"headcell-"+headCell.id}
                                />
                            </th>
                        )
                    }
                    else{
                        return(
                            <th className='tableHead'  key={uuidv4()}>
                                <TableHeader  
                                    label={
                                    <React.Fragment key={uuidv4()}>
                                            {headCell.label}
                                    </React.Fragment>}
                                    sortedBy={headCell.sorted ? sortedBy : {}}
                                    sort={headCell.sorted ? { key: headCell.id, changer: setSortedBy }: undefined}
                                    headerId = {"headcell-"+headCell.id}
                                />
                            </th>
                        )
                    }
                    
                }
                return <React.Fragment key={uuidv4()}/>
            })}
        </React.Fragment>
    )
}


//=============================================================================================
//Render the data cells

const typeColumn = (type : DiscriminatorType) => {

    switch(type){
      case DiscriminatorType.Region:        return(<span> <RegionIcon />      {"Region"} </span>)
      case DiscriminatorType.Store:         return(<span> <StoreIcon />       {"Store"} </span>)
      case DiscriminatorType.Department:    return(<span> <DepartmentIcon />  {"Department"} </span>)
      case DiscriminatorType.ScaleDevice:   return(<span> <ScaleIcon />       {"Scale"} </span>)
      default:        return(<span> <ErrorIcon />       {"Unknown"} </span>)
    }
}

export function getProperty(item, property){
    if(item.properties != null){
        return item.properties[property]
    }
    else{
        return item[property]
    }
}
export function RenderRow(
    dataId : string, 
    headerConfig : HeadCellTypes[],
    row : any,
    onCheck : (e: React.ChangeEvent<HTMLInputElement>, row: any) => void,
    setPrimary : (e: React.ChangeEvent<HTMLInputElement>, row: any) => void,
    primary : string [],
    rowsChecked : string[],
    reduxUpgradeSelected : string[],
    upgradeDevicesOriginal : {[key:string]:UpgradeDevice},
    batchIds : string[],
    batches : {[key:string]: Batch},
    upgradeFiles : FileForEvent[],
    currentIP : string,
    setCurrentIP : React.Dispatch<React.SetStateAction<string>>,
    currentPrID : string,
    setCurrentPrID : React.Dispatch<React.SetStateAction<string>>,
    batchUpgrade: boolean | undefined,
    hybridMultipath: boolean
    ){

        const dispatch = useDispatch()
        const {callbacks, addCallback} = useRspHandler()
    
        let upgradeDevices = Object.assign({},upgradeDevicesOriginal)
        // vars used for AutoAssign Table Elements
        const getOffset = (timeZone = 'UTC', date = new Date()) => {
            const utcDate = new Date(date.toLocaleString('en-US', { timeZone: 'UTC' }));
            const tzDate = new Date(date.toLocaleString('en-US', { timeZone }));
            return (tzDate.getTime() - utcDate.getTime()) / 6e4;
        }
        let USDollar = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });
        const fixShownDateTime = (value: string) => {
            if (!value) return value
            let localTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone
            let localOffset = getOffset(localTimezone) / 60
            if (localOffset < 0) {
                value = value.replace("+", "-")
            }
            if (localOffset >= 10 || localOffset <= -10) {
                value = value.slice(0, 24) + Math.abs(localOffset) + value.slice(26, value.length)
            } else {
                value = value.slice(0, 25) + Math.abs(localOffset) + value.slice(26, value.length)
            }
            return value
        }
        const handleView = (selectedRow, selectedFile, newBatchId: string) => {
            let upgradeDevicesOriginal = {...upgradeDevices}
            let upgradeDevicesTemp : {[key:string]:UpgradeDevice}= {}
            let upgradeDevice

            function onSuccess(response: ResponseType) {
                if (rowsChecked.includes(selectedRow.upgradeId)) {
                    dispatch(AssetActions.setUpgradeDevices({...upgradeDevicesOriginal, ...upgradeDevicesTemp}))
                } else {
                    dispatch(AssetActions.setUpgradeDevices({...upgradeDevicesOriginal, [selectedRow.upgradeId]: {...upgradeDevice}}))
                }
                if (batchUpgrade) {
                    logger.info("Batch Successfully Assigned")
                } else {
                    logger.info("Async Upgrade File successfully assigned")
                }
            }
            function onFail(response: ResponseType) {
                if (batchUpgrade) {
                    logger.info("Batch Assignment Failed")
                } else {
                    logger.info("Async Upgrade File successfully assigned")
                }
            }

            const newID = Date.now()
            if (rowsChecked.includes(selectedRow.upgradeId)) {
                rowsChecked.forEach(upgradeId => {
                    upgradeDevice = {...upgradeDevices[upgradeId]}
                    upgradeDevice["batchId"] = newBatchId
                    upgradeDevice["fileSelected"] = selectedFile
                    if (newBatchId) {
                        if (hybridMultipath) {
                            if (upgradeDevice["primaryScale"]) {
                                upgradeDevice["status"] = DeviceStatuses.awaitingUpload
                            } else {
                                upgradeDevice["status"] = DeviceStatuses.awaitingPrimary
                            }
                        } else {
                            upgradeDevice["status"] = DeviceStatuses.awaitingUpload
                        }
                    } else if (selectedFile) {
                        upgradeDevice["status"] = DeviceStatuses.readyToUpload
                        upgradeDevice["statusText"] = "Ready for user to initiate upload"
                    } else {
                        upgradeDevice["status"] = DeviceStatuses.unassigned
                        upgradeDevice["statusText"] = null
                    }
                    upgradeDevicesTemp[upgradeId] = {...upgradeDevice}
                })
                if (batchUpgrade) {
                    if (addCallback(callbacks, newID, "Update Batch Assignment", onSuccess, onFail)) {
                        dispatch(UpgradeAPI.updateBatchAssignments(newBatchId, Object.keys(upgradeDevicesTemp).map(key => upgradeDevicesTemp[key]), newID))
                    }
                } else {
                    if (addCallback(callbacks, newID, "Assign Manual Upgrade File", onSuccess, onFail)) {
                        dispatch(UpgradeAPI.assignManualFile(newID, Object.keys(upgradeDevicesTemp).map(key => upgradeDevicesTemp[key])))
                    }
                }
            } else {
                upgradeDevice = {...upgradeDevices[selectedRow.upgradeId]}
                upgradeDevice.batchId = newBatchId
                upgradeDevice.fileSelected = selectedFile
                if (newBatchId) {
                    if (hybridMultipath) {
                        if (upgradeDevice["primaryScale"]) {
                            upgradeDevice["status"] = DeviceStatuses.awaitingUpload
                        } else {
                            upgradeDevice["status"] = DeviceStatuses.awaitingPrimary
                        }
                    } else {
                        upgradeDevice["status"] = DeviceStatuses.awaitingUpload
                    }
                } else if (selectedFile) {
                    upgradeDevice["status"] = DeviceStatuses.readyToUpload
                    upgradeDevice["statusText"] = "Ready for user to initiate upload"
                } else {
                    upgradeDevice["status"] = DeviceStatuses.unassigned
                    upgradeDevice["statusText"] = null
                }

                if (batchUpgrade) {
                    if (addCallback(callbacks, newID, "Update Batch Assignment", onSuccess, onFail)) {
                        dispatch(UpgradeAPI.updateBatchAssignments(newBatchId, [upgradeDevice], newID))
                        // dispatch(AssetActions.setUpgradeDevices({})) // use to reset redux if out of Sync
                    }
                } else {
                    if (addCallback(callbacks, newID, "Assign Manual Upgrade File", onSuccess, onFail)) {
                        dispatch(UpgradeAPI.assignManualFile(newID, [upgradeDevice]))
                    }
                }
            }
        }

    return(
        <React.Fragment key={uuidv4()}>
            <tr key={uuidv4()}>
                {headerConfig.map((headCell, index) => {
                    if(headCell.isShowing && row){
                        switch(headCell.id){
                            case 'checkmark':
                                return(
                                    <td key={uuidv4()}>
                                        <Checkbox className="data-checkbox" 
                                            indeterminate={false}
                                            checked={rowsChecked.filter((id)=> id === row[dataId]).length > 0}
                                            onChange={(event) => onCheck(event, row)}
                                            inputProps={{ 'aria-label': 'select all products' }}
                                        />
                                    </td>
                                )

                            // =================== UpgradeManager CASES ===================
                            case 'checkmarkUpgrade':
                                return(
                                    <td key={uuidv4()}>
                                        <Checkbox className="data-checkbox" 
                                            indeterminate={false}
                                            checked={reduxUpgradeSelected.filter((id)=> id === row[dataId]).length > 0}
                                            onChange={(event) => onCheck(event, row)}
                                            inputProps={{ 'aria-label': 'select all products' }}
                                        />
                                    </td>
                                )
                            case 'primaryScale':
                                return(
                                    <td key={uuidv4()}>
                                        <Checkbox className="data-checkbox" 
                                            indeterminate={false}
                                            checked = {primary.includes(row[dataId])}
                                            onChange={(event)=>{setPrimary(event, row); }}
                                            inputProps={{ 'aria-label': 'select all products' }}
                                        />
                                    </td>
                                )
                            case 'batchSelected':
                                return(
                                    <td key={uuidv4()}>
                                        <Form.Select 
                                            style = {{minWidth : "13vh"}}
                                            onChange={(e) => handleView(row, "", e.currentTarget.value) }
                                            defaultValue = {(
                                                upgradeDevices && upgradeDevices[row[dataId]] && upgradeDevices[row[dataId]]["batchId"] && batches[upgradeDevices[row[dataId]]["batchId"]] && batches[upgradeDevices[row[dataId]]["batchId"]]["name"]
                                            ) ? 
                                            batches[upgradeDevices[row[dataId]]["batchId"]]["name"]
                                            :
                                            "--None--"}
                                        >
                                            <>
                                                <option value = "" >--None--</option>
                                                {batchIds?.map((batchId) => (
                                                    (upgradeDevices && upgradeDevices[row[dataId]] && upgradeDevices[row[dataId]]["batchId"] && batches[upgradeDevices[row[dataId]]["batchId"]] && batches[upgradeDevices[row[dataId]]["batchId"]]["name"] && batchId === upgradeDevices[row[dataId]]["batchId"]) ?
                                                    <option defaultValue = {batchId} > {batches[batchId]["name"]}</option>
                                                    :
                                                    <option value={batchId}> {batches[batchId]["name"]}</option>
                                                ))}
                                            </>
                                        </Form.Select>
                                    </td>
                                )
                            case 'batchId':
                                return (
                                    <td key={uuidv4()}> 
                                    {row[headCell.id] ? batches[row[headCell.id]]["name"]: "No Batch Assigned"}
                                    </td>
                                )
                            case 'fileSelected':
                                return (
                                    <td key={uuidv4()}>
                                        <Form.Select
                                            style={{minWidth: "13vh"}}
                                            onChange={e => handleView(row, e.currentTarget.value, "")}
                                            defaultValue={(
                                                upgradeDevices && upgradeDevices[row[dataId]] && upgradeDevices[row[dataId]]["fileSelected"] && upgradeFiles.find(file => file.fileId === upgradeDevices[row[dataId]]["fileSelected"]) ?
                                                    upgradeFiles.find(file => file.fileId === upgradeDevices[row[dataId]]["fileSelected"])?.filename
                                                :
                                                    "--None--"
                                            )}
                                        >
                                            <>
                                                <option value="">--None--</option>
                                                {upgradeFiles.map(file => (
                                                    
                                                    (upgradeDevices && upgradeDevices[row[dataId]] && upgradeDevices[row[dataId]]["fileSelected"] && file.fileId === upgradeDevices[row[dataId]]["fileSelected"]) ?
                                                        <option defaultValue={file.fileId}>{file.filename}</option>
                                                    :
                                                        <option value={file.fileId}>{file.filename}</option>
                                                ))}
                                            </>
                                        </Form.Select>
                                    </td>
                                )
                            case 'status':
                                let status = "unassigned"
                                if(row[dataId] && upgradeDevices[row[dataId]] && upgradeDevices[row[dataId]]["status"]){
                                    status =  (upgradeDevices[row[dataId]]["status"])? upgradeDevices[row[dataId]]["status"] : "unassigned"
                                }
                                let acc = <div/>
                                switch (status) {
                                    case DeviceStatuses.uploadNow:
                                    case DeviceStatuses.upgradeNow:
                                    case DeviceStatuses.uploadInProgress:
                                    case DeviceStatuses.upgradeInProgress:
                                        acc = 
                                        <td key={uuidv4()}>
                                            <div className="chartStyle">
                                                {status.includes("upload") ? getProgressChart("Upload In Progress",row['statusText']) : getProgressChart("Upgrade In Progress",row['statusText'])}
                                                <p className = "statusText">
                                                    <span className = "statusText">
                                                        {status.includes("upload") ? " Upload" : " Upgrade"} In Progress
                                                    </span>
                                                </p>
                                            </div>
                                        </td>
                                    break;

                                    case DeviceStatuses.uploadCompletedNoErrors:
                                    case DeviceStatuses.uploadCompletedErrors:
                                    case DeviceStatuses.upgradeCompletedNoErrors:
                                    case DeviceStatuses.upgradeCompletedErrors:
                                        acc = 
                                        <td key={uuidv4()}>
                                            <div className="chartStyle">
                                                {status.includes("no") ? getProgressChart("Complete",row['statusText']) : getProgressChart("Failed",row['statusText'])}  
                                                <p className = "statusText">
                                                    <span className = "statusText">
                                                        {status.includes("upload") ? " Upload" : " Upgrade"} Completed {status.includes("no") ? "Without Errors" : "With Errors"}
                                                    </span>
                                                </p>
                                            </div>
                                        </td>
                                    break; 
                                    
                                    case DeviceStatuses.postInstallProcess:
                                        acc = <td key={uuidv4()}>
                                            <div className="chartStyle">
                                                { getProgressChart("Upgrade In Progress", row['statusText'])}
                                                <span className = "statusText">
                                                    <span className = "statusText">
                                                        <span>Post Install Process</span>
                                                    </span>
                                                </span>
                                            </div>
                                        </td>
                                    break;

                                    case DeviceStatuses.awaitingUpload:
                                    case DeviceStatuses.readyToUpload:
                                    case DeviceStatuses.fileUploadedAwaitingUpgrade:
                                    case DeviceStatuses.readyToUpgrade:
                                    case DeviceStatuses.awaitingPrimary:
                                        acc = <td key={uuidv4()}>
                                            <div className="chartStyle">
                                                {status.includes("upgrade") ? getProgressChart("Awaiting Upgrade",row['statusText']) : getProgressChart("Awaiting Upload",row['statusText'])}
                                                <span className = "statusText">
                                                    <span className = "statusText">
                                                        {status.includes("post") ? 
                                                        <span>Post Install Process</span>
                                                        :
                                                        <span> Awaiting {!status.includes("primary") ? status.includes("upgrade") ? "Upgrade" : "Upload" : "Primary"}</span>
                                                        }
                                                    </span>
                                                </span>
                                            </div>
                                        </td>
                                    break;

                                    case DeviceStatuses.uploadFailed:
                                    case DeviceStatuses.upgradeFailed:
                                        acc = 
                                        <td key={uuidv4()}>
                                            <div className="chartStyle">
                                                {getProgressChart("Failed",row['statusText'])}  
                                                <p className = "statusText">
                                                    <span className = "statusText">
                                                        {status.includes("upload") ? " Upload" : " Upgrade"} Failed
                                                    </span>
                                                 </p>
                                            </div>
                                        </td>
                                    break;

                                    case DeviceStatuses.unassigned:
                                        acc = 
                                        <td key={uuidv4()}>
                                            <div className="chartStyle">
                                                {getProgressChart("unassigned",row["statusText"])}
                                                <p className = "statusText">
                                                    <span className = "statusText" >Unassigned</span>
                                                </p>
                                            </div>
                                        </td>
                                    break;

                                    default: //disabled 
                                        acc = 
                                        <td key={uuidv4()}>
                                            <div className="chartStyle">
                                                {getProgressChart("unassigned",row["statusText"])}
                                                <p className = "statusText">
                                                    <span className = "statusText">Unassigned</span> 
                                                </p>
                                            </div>
                                        </td>
                                    break;
                                }
                                return acc
                            case "statusText":
                                return (
                                    <td key={uuidv4()}> 
                                    {row[headCell.id] ? row[headCell.id] : "N/A"}  
                                    </td>
                                )
                            case 'scaleModel':
                            case 'fpc':
                            case 'statusText':
                            case 'bootloader':
                                return(
                                    <td key={uuidv4()}>
                                        {row[headCell.id] ? row[headCell.id] : "N/A"}
                                    </td>
                                    )
                            case 'actions':
                                return (
                                    null
                                )

                            // =================== DASHBOARD CASES ===================
                            // type = {scale, dept, store}, id, options for scales, assignedBanner, assignedRegion, assignedStore, assignedDept
                            case 'type':
                                return (
                                    <td key={uuidv4()}> 
                                        {typeColumn(instanceOf(row))}  
                                    </td>
                                )

                            case 'id':
                                return (
                                    <td key={uuidv4()}> 
                                    {row[headCell.id] ? row[headCell.id] : "Not set"}
                                    </td>
                                )

                            case 'hostname':
                                let hostname = getProperty(row, 'hostname')
                                return (
                                    <td key={uuidv4()}> 
                                    {hostname ? hostname : "Error fetching host name"}
                                    </td>
                                )

                            case 'application':
                                let application = getProperty(row, 'application')
                                return (
                                    <td key={uuidv4()}> 
                                    {application ? application : "Error fetching version"}
                                    </td>
                                )

                            case 'totalLabelsPrinted':
                                let totalLabelsPrinted = getProperty(row, 'totalLabelsPrinted')
                                return (
                                    <td key={uuidv4()}> 
                                    {totalLabelsPrinted || totalLabelsPrinted === 0 ? totalLabelsPrinted : "N/A"}
                                    </td>
                                )

                            case 'pluCount':
                                let pluCount = getProperty(row, 'pluCount')
                                return (
                                    <td key={uuidv4()}> 
                                    {pluCount || pluCount === 0 ? pluCount : "N/A"}
                                    </td>
                                )  

                            case 'options':
                                return (
                                    <td key={uuidv4()}>
                                    {row.deviceId 
                                    ? <Link to={{ pathname: '/AssetDetails', state: { scale: row} }}>
                                        <IconButton style={{padding:"0.2rem"}}>
                                            <Typography noWrap={true}>
                                                    {/* <FontAwesomeIcon icon={faChevronRight} className="animate-left-3 me-3 ms-2" style={{fontSize: "1em"}} /> */}
                                                    <MoreHoriz/>
                                            </Typography>
                                        </IconButton>
                                    </Link> 
                                    : null}

                                    {row.storeName
                                        ? <Link to={{ pathname: '/AssetDetails', state: { store: row} }}>
                                            <IconButton style={{padding:"0.2rem"}}>
                                                <Typography noWrap={true}>
                                                    {/* <FontAwesomeIcon icon={faChevronRight} className="animate-left-3 me-3 ms-2" style={{fontSize: "1em"}} /> */}
                                                    <MoreHoriz/>
                                                </Typography>
                                            </IconButton>
                                        </Link> 
                                    : null}
                                    </td>
                                )

                            case 'bannerId':
                            case 'assignedBanner':
                                let region = store.getState().asset.regions.find(region => region.regionId === row.regionId)


                                let banner : Banner | undefined
                                if(region){
                                    banner = store.getState().asset.banners.find((banner: Banner) => banner.bannerId === region!.bannerId)
                                }

                                return (
                                    <td key={uuidv4()}> 
                                    {!banner ? "Error finding banner" : banner.bannerName}
                                    </td>
                                )
    
                            case 'regionId':
                            case 'assignedRegion':
                                let assignedRegion = store.getState().asset.regions.find(region => region.regionId === row.regionId)

                                return (
                                    <td key={uuidv4()}> 
                                    {!assignedRegion ? "Error finding region" : assignedRegion.regionName}
                                    </td>
                                )

                            case 'storeId':
                            case 'assignedStore':
                                let assignedStore = row.assignedStore ? store.getState().asset.stores[row.assignedStore] : store.getState().asset.stores[row.storeId]

                                return (
                                    <td key={uuidv4()}> 
                                    {!assignedStore ? <span className="color-orange fw-bold">Unassigned Scale</span> : assignedStore.storeName}
                                    </td>
                                )

                            case 'deptId':
                            case 'assignedDept':
                                let assignedDept = row.deptId ? store.getState().asset.depts[row.deptId] : store.getState().asset.depts[row.assignedDept]

                                return (
                                    <td key={uuidv4()}> 
                                    {assignedDept == null ? <span className="color-orange fw-bold">Unassigned Scale</span> : assignedDept.deptName1}
                                    </td>
                                )
                            // Edit Auto Assign Rules Table
                            case 'editRule':
                            // ====================================
                                if(row["ipAddressStart"] + "-" + row["ipAddressEnd"] === currentIP){
                                    return (
                                        <EditRule
                                            openPopup = {row["ipAddressStart"] + "-" + row["ipAddressEnd"] === currentIP}
                                            setCurrentIP = {setCurrentIP}
                                            ruleIPs={currentIP}
                                            ruleData={row}
                                        />
                                    )
                                }
                                else{
                                    return(
                                        <td align="right">
                                            <Button id="actionBar2" variant="outline-primary" size="sm" 
                                            onClick={()=>{
                                                //Causing performance issues bc of two usestates
                                                setCurrentIP(row["ipAddressStart"] + "-" + row["ipAddressEnd"] )
                                            }} >
                                                Edit
                                            </Button>
                                        </td>
                                    )
                                }
                            case 'size':
                                return (
                                    <td align="right"> 
                                    {formatBytes(row[headCell.id])}  
                                    </td>
                                )

                            case 'rowCategory':
                                return (
                                    <td key={uuidv4()}> 
                                    {row[headCell.id] ? row[headCell.id] : "Not set"}  
                                    </td>
                                )

                            case 'syncStatus':
                            case 'enabled':
                                return (
                                    <td key={uuidv4()}> 
                                    {row.enabled ? "Enabled" : "Disabled"}  
                                    </td>
                                )

                            case "nextCheck":
                            case "uploadStart":
                            case "uploadEnd":
                            case "upgradeStart":
                            case "upgradeEnd":
                                let localDate = format(new Date(fixShownDateTime(row[headCell.id])), "PPpp")
                                return (
                                    <td key={uuidv4()}>
                                        {row[headCell.id] ? localDate : "Not Set"}
                                    </td>
                                )
                            case 'date':
                            case 'dateCreated':
                            case 'uploadDate':
                            case 'startDate':
                            case 'endDate':
                            case 'trDtTm':
                            case 'activationDate':
                            case 'timeoutDate':
                                let date = format(new Date(row[headCell.id]), "PPpp")
                                return (
                                    <td key={uuidv4()}> 
                                    {row[headCell.id] ? date : "Not set"}
                                    </td>
                                )
                            case "lastUpdate":
                                let reportString = ""
                                let time = Date.now() - new Date(fixShownDateTime(getProperty(row, headCell.id))).getTime()
                                if(time < 3.6e+6){
                                    reportString = Math.round(time/60000) + " Minute(s) Ago"
                                }
                                else if(time < 8.64e+7){
                                    reportString = Math.round(time/3.6e+6) + " Hour(s) Ago"
                                }
                                else{
                                    reportString = Math.round(time/8.64e+7) + " Day(s) Ago"
                                }

                                if(getProperty(row, headCell.id) == null){
                                    reportString = "Never"
                                }

                                return(
                                    <td key={uuidv4()}> 
                                        {reportString ? reportString : "Not Found"}  
                                    </td>
                                )
                            case "lastReport":
                            case "lastReportTimestampUtc":
                            case "lastCommunicationTimestampUtc":
                                let timeSince = Date.now() - new Date(getProperty(row, headCell.id)).getTime()
                                let reportStr = ""
                                if(timeSince < 3.6e+6){
                                    reportStr = Math.round(timeSince/60000) + " Minute(s) Ago"
                                }
                                else if(timeSince < 8.64e+7){
                                    reportStr = Math.round(timeSince/3.6e+6) + " Hour(s) Ago"
                                }
                                else{
                                    reportStr = Math.round(timeSince/8.64e+7) + " Day(s) Ago"
                                }

                                if(getProperty(row, headCell.id) == null){
                                    reportStr = "Never"
                                }

                                return(
                                    <td key={uuidv4()}> 
                                        {reportStr ? reportStr : "Not Found"}  
                                    </td>
                                )
                            case 'trTtVal':
                                let price : string | undefined
                                try{
                                    price = (row[headCell.id]/100).toFixed(2)
                                }
                                catch(e){
                                    logger.error("Error parsing price! " + row[headCell.id])
                                }
                                return (
                                    <td key={uuidv4()}> 
                                    {price ? "$" + price : "Invalid"}  
                                    </td>
                                )
                            case 'trUnPr':
                                let pricePerLb : string | undefined
                                try{
                                    pricePerLb = (row[headCell.id]/100).toFixed(2)
                                }
                                catch(e){
                                    logger.error("Error parsing price! " + row[headCell.id])
                                }
                                return (
                                    <td key={uuidv4()}> 
                                    {pricePerLb ? "$" + pricePerLb + "/lb" : "Invalid"}  
                                    </td>
                                )
                            
                            case 'firstName':
                            case 'lastName':
                                return (
                                    <td key={uuidv4()}> 
                                    {(row.firstName && row.lastName) ? row[headCell.id] : "Not set"}  
                                    </td>
                                )  
                            case 'name':
                                return (
                                    <td key={uuidv4()}> 
                                    {(row.name) ? row[headCell.id] : "Not set"}  
                                    </td>
                                )

                            case 'phoneNumber':
                                let formattedPhoneNumber : string | undefined = "Unknown"
                                if (row[headCell.id]) {
                                    let tempPhoneNumber = row[headCell.id].replace(/[^0-9]/g, "")
                                    formattedPhoneNumber = `(${tempPhoneNumber.substr(0,3)}) ${tempPhoneNumber.substr(3,3)}-${tempPhoneNumber.substr(6,4)}`
                                }

                                return (
                                    <td key={uuidv4()}> 
                                    {row[headCell.id] ? formattedPhoneNumber : ""}  
                                    </td>
                                )  

                            case 'accessLevel':
                                return (
                                    <td key={uuidv4()}> 
                                    {row[headCell.id] !== null
                                    ? 
                                        row[headCell.id] > 4000 ? 
                                        "Hobart" : 
                                        row[headCell.id] === 4000 ? 
                                        "Admin" : 
                                        row[headCell.id] === 3000 ? 
                                        "Banner Manager" : 
                                        row[headCell.id] === 2000 ? 
                                        "Store Manager" : 
                                        row[headCell.id] === 1000 ? 
                                        "Operator" : 
                                        row[headCell.id] === 0 ? 
                                        "Custom Role" : 
                                        "Error" 
                                    : "Not set"}  
                                    </td>
                                )  

                            case 'domain':
                                return (
                                    
                                    <td key={uuidv4()}>
                                        {"TODO Show Domain"}
                                    {/* {row[headCell.id].domainId === AccessActions.DEFAULT_ADMIN_DOMAIN_ID
                                    ? `Admin Domain`
                                    : Array.isArray(props.treeState) 
                                        ? props.treeState.map(banner => (

                                        banner.id === row[headCell.id].domainId 
                                            ? `${banner.name} (Banner)`
                                            : banner.children.map(region => (

                                            domainArray = region.id.split("|"),
                                            domainArray[1] === row[headCell.id].domainId
                                                ? `${region.name} (Region)`
                                                : region.children.map(store => (

                                                domainArray = store.id.split("|"),
                                                domainArray[1] === row[headCell.id].domainId
                                                    ? `${store.name} (Store)`
                                                    // : `Error showing domain (1):  ${row[headCell.id].domainId}`
                                                    : ""
                                                ))
                                            ))
                                        ))                   

                                        : logger("Error showing Domain: (2)", row[headCell.id].domainId)} */}
                                    </td>
                                )                              

                            case 'roles':

                                return (
                                    <td key={uuidv4()}> 
                                        {"TODO Show Roles"}
                                    {/* {props.displayRoles
                                    ? <Tooltip title={<Typography fontSize={12}>Display User's Roles</Typography>}>
                                        <span style={{fontWeight: "500", color: "black"}}> View Roles
                                        <IconButton aria-label="download"
                                            onClick={() => props.displayRoles(row)}
                                        >
                                            <ViewListIcon />
                                        </IconButton>
                                        </span>
                                    </Tooltip>
                                    : null} */}
                                    </td>
                                )    

                            case 'canConnect':
                                return (
                                    <td key={uuidv4()}>
                                        {row.canConnect !== undefined && row.canConnect !== null ? row.canConnect ? "Yes" : "No" : batchUpgrade ? "No Batch Assigned" : "No File Assigned"}
                                    </td>
                                )

                            case 'hasFile':
                                return (
                                    <td key={uuidv4()}>
                                        {row.hasFile !== undefined && row.hasFile !== null ? row.hasFile ? "Yes" : row.canConnect ? "No" : "N/A" : batchUpgrade ? "No Batch Assigned" : "No File Assigned"}
                                    </td>
                                )

                            case 'targetFirmware':
                                return (
                                    <td key={uuidv4()}>
                                        {row.targetFirmware ? row.targetFirmware : batchUpgrade ? "No Batch Assigned" : "No File Assigned"}
                                    </td>
                                )

                                
                            //============================================================
                            // Case structure for prods table
                            case 'editProd':
                            // ===================================
                                return(
                                    <td align="right">
                                        <Button id="actionBar2" variant="outline-primary" size="sm" 
                                            onClick={()=>{
                                                setCurrentPrID(row["prID"])
                                            }} >
                                                Edit
                                        </Button>
                                        <EditProd
                                        openPopup = {row["prID"] === currentPrID}
                                        setCurrentPrID = {setCurrentPrID}
                                        prod={{...row}}
                                    />   
                                    </td>
                                )
                            case 'prShlfLfD':
                            case 'prProdLfD':
                                return (
                                    <td key={uuidv4()}>
                                    
                                    {row[headCell.id] != null && row[headCell.id] !== undefined ? row[headCell.id] + " Days" : "N/A"}  
                                    </td>
                                )
                            case 'prPrice':
                            case 'prDscPr1':
                            case 'prDscPr2':
                            case 'prDscPr3':
                                return (
                                    <td key={uuidv4()}>
                                    {/*Dollar amount is in cents */}
                                    {row[headCell.id] != null && row[headCell.id] !== undefined ? USDollar.format(row[headCell.id]/100) : "N/A"}
                                    </td>
                                )
                            case 'prType':
                                return (
                                    <td key={uuidv4()}>
                                    {row[headCell.id]? prodType[row[headCell.id]] : "N/A"}  
                                    </td>
                                )
                            case 'lbNum1':
                            case 'lbNum2':
                            case 'lbNum3':
                            case 'deptNum':
                                return (
                                    <td>
                                        {row[headCell.id] != null && row[headCell.id] !== undefined ? row[headCell.id] : "N/A"}
                                    </td>
                                )

                            default:
                                if(dataId === "prID"){
                                    return (
                                        <td key={uuidv4()}>
                                        
                                        {row[headCell.id]? row[headCell.id] : "N/A"}  
                                        </td>
                                    )
                                }
                                return (
                                    <td key={uuidv4()}>
                                    {/* {row[headCell.id]} */}
                                    
                                    {row[headCell.id] ? row[headCell.id] : "Not Found"}  
                                    </td>
                                )
                        }
                    }
                    return null
                })}
            </tr>
        </React.Fragment>
    )
}