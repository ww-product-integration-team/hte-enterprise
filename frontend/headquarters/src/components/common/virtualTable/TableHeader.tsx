import React from 'react';
import SortIcon from './SortIcon';
import { v4 as uuidv4 } from 'uuid';

export interface TableHeaderProps{
    sortedBy ?: {[key: string] : string}
    sort ?: {key: string, changer: React.Dispatch<React.SetStateAction<{[key: string]: string}>>}
    label : JSX.Element,
    headerId : string
}

export default function TableHeader(props: TableHeaderProps) {
    const { sortedBy = {}, sort, label } = props

    function changeSort(direction : string) {
        if(!sort){
            return
        }
        sort.changer({[sort.key]: direction});
    }

    function sortClicked(){
        if(!sort) return
        if(sortedBy[sort.key] === 'ascending'){
            changeSort('descending')
        }
        else{
            changeSort('ascending')
        }
    }

    return (
        <div className='flex-container' onClick={sortClicked} key={uuidv4()}>
            <div className='flex-full'  style = {{margin: "auto"}} key={uuidv4()}>{label}</div>
            {sort ? (
            <div  key={uuidv4()}>
                <SortIcon
                    sortedBy={sortedBy && (sortedBy[sort.key] === 'ascending' || sortedBy[sort.key] === 'descending')}
                    isAscending={sortedBy[sort.key] === 'ascending'}
                />
            </div>
            ) : null}
        </div>
    );
}