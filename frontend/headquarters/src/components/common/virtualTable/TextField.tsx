import React from 'react';
import './styles.css';

export interface TextFieldProps{
    value : string | number
    onChange : (value: string, e: React.ChangeEvent<HTMLInputElement>) => void
    placeholder ?: string
}

export default function TextField(props: TextFieldProps) {
    const {value,onChange,placeholder} = props
    return (
        <input
            className='__dml_text-field'
            type='text'
            value={value}
            placeholder={placeholder}
            onChange={(e) => onChange(e.target.value, e)}
        />
    );
}
