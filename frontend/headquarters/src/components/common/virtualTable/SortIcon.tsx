import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';

export interface SortIconProps{
    sortedBy : boolean
    isAscending : boolean
}

export default function SortIcon(props : SortIconProps) {
    const {sortedBy, isAscending} = props

    if(isAscending){
        if(sortedBy){
            return(<ArrowUpwardIcon style={{fill:"#ba4735", maxHeight :"5vh"}}/>)
        }
        return(<ArrowUpwardIcon style={{ maxHeight :"5vh"}}/>)
    }
    else{
        if(sortedBy){
            return(<ArrowDownwardIcon style={{fill:"#ba4735", maxHeight :"5vh"}}/>)
        }
        return(<ArrowDownwardIcon style={{ maxHeight :"5vh"}}/>)
    }

}