import ReactSlider from "react-slider";
import { useState, useEffect} from "react";
import "../styles/index.css"
import { IconButton, Tooltip, Typography } from '@mui/material';
import { Help } from '@mui/icons-material';
import React from "react";

export interface DoubleSliderTemplateProps{
    title : string
    max : number
    min : number
    units : string
    message : string
    value : number[]
    onValueChange : (inputeValue : number[]) => void
    errorStatus : (status : string) => void
    isTime : boolean
}

const DoubleSliderTemplate = (props : DoubleSliderTemplateProps) => {

    const title = props.title;
    const sliderMax = props.max;
    const sliderMin = props.min;
    const units = props.units;
    const message = props.message;
    const isTime = props.isTime;
    const [currentMinValue, setCurrentMinValue] = useState<number | undefined>(props.value[0]);
    const [currentMaxValue, setCurrentMaxValue] = useState<number | undefined>(props.value[1]);

    useEffect(() =>{
        setCurrentMinValue(props.value[0])
        setCurrentMaxValue(props.value[1])
    }, [props.value])

    function changeInputValue(input : number ){
        let minInputValue = Math.round(input[0]);
        let maxInputValue = Math.round(input[1]);
        if (Number.isInteger(minInputValue) && Number.isInteger(maxInputValue)){
            setCurrentMinValue(minInputValue);
            setCurrentMaxValue(maxInputValue);
            // props.onValueChange(inputValue);

        }else{
            props.errorStatus("error");
        }
    }
    function convertToTime(value : number | undefined ){
        if(value){
            if(value > 12)
                return value -12
            else 
                return value
        }
        return -1
    }
    return ( 
    <><div style={{ display: 'flex', flexDirection: 'row', alignItems: "center", marginRight: ".5rem", marginLeft: ".5rem"}}>
        <label className="sliderLabel">{title}</label>
        <input 
            type="text" 
            inputMode="text"
            name="slider" 
            id="slider" 
            className="sliderOutput" 
            value = {convertToTime(currentMinValue)} 
            onChange={(e) =>{
                const newVal = Number.parseInt(e.target.value)
                if(e.target.value === "" || e.target.value === "-"){
                    setCurrentMinValue(undefined)
                    setCurrentMaxValue(undefined)
                }
                else if(newVal){
                    changeInputValue(newVal)
                }
            }}                
            max = {sliderMax}
            min = {sliderMin}
        />
        <label className="sliderLabel">{isTime ? (currentMinValue &&  currentMinValue > 12 && currentMinValue < 24 ? "pm" : "am ") : units}</label>
        <input 
            type="text" 
            inputMode="text"
            name="slider" 
            id="slider" 
            className="sliderOutput" 
            value = {convertToTime(currentMaxValue)} 
            onChange={(e) =>{
                const newVal = Number.parseInt(e.target.value)
                if(e.target.value === "" || e.target.value === "-"){
                    setCurrentMaxValue(undefined)
                }
                else if(newVal){
                    changeInputValue(newVal)
                }
            }}                
            max = {sliderMax}
            min = {sliderMin}
        />
        <label className="sliderLabel">{isTime ? (currentMaxValue &&  currentMaxValue > 12 && currentMaxValue < 24 ? "pm" : "am ") : units}</label>
        <Tooltip className="info" title={<Typography fontSize={12}>{message}</Typography>}>
            <IconButton aria-label="enableAccount" disableRipple>
                <Help />
            </IconButton>
        </Tooltip>
    </div>
    <div className="sliderDiv">
    <ReactSlider
        className="settingSlider"
        thumbClassName="settingSliderThumb"
        trackClassName="settingSliderTrack"
        defaultValue={[sliderMin, sliderMax]}
        min = {sliderMin}
        max = {sliderMax}
        ariaLabel={['Lower thumb', 'Upper thumb']}
        value= {[currentMinValue, currentMaxValue]}
        pearling
        onChange = { (value : number) => changeInputValue(value)}
    />
    </div></>

    );
}
 
export default React.memo(DoubleSliderTemplate);