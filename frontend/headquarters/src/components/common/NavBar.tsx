import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import { AppBar, Toolbar, IconButton, Menu, MenuItem, Badge } from '@material-ui/core';
import { AccountCircle, MoreVert, Mail, Notifications} from '@material-ui/icons';
import img from "../images/hobartLogo.png"
import '../styles/index.css';
import { useDispatch } from "react-redux"
import { AccessActions, useAppSelector} from '../../state';
import { AccessAPI, EventAPI } from '../api/index'

export default function NavBar() {

    // const users = useAppSelector((state) => state.access.users);
    const currentAccount = useAppSelector((state) => state.access.account);
    const isLoggedIn = useAppSelector((state) => state.access.loggedIn);
    const notifications = useAppSelector(state => state.event.events)

    const [anchorEl, setAnchorEl] = useState(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
    const [notificationsCount, setNotificationsCount] = useState(0)
    const socketMessage = useAppSelector(state => state.socket.socket.message)
    
    const isMenuOpen = Boolean(anchorEl);                   // Show/hide profile menu (desktop)
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);   // Show/hide profile menu (mobile)

    // Desktop: open/close menu on right of navbar
    const handleProfileMenuOpen = (event :  React.MouseEvent<any, MouseEvent>) => {setAnchorEl(event.currentTarget)};
    const handleMenuClose = () => {
        setAnchorEl(null);
        handleMobileMenuClose();
    };

    // Mobile: open/close menu on right of navbar
    const handleMobileMenuOpen = (event : React.MouseEvent<any, MouseEvent>) => {setMobileMoreAnchorEl(event.currentTarget)};
    const handleMobileMenuClose = () => {setMobileMoreAnchorEl(null)};
    const dispatch = useDispatch();

    const menuId = 'navigationBar';
    const renderMenu = (
        <Menu
            id={menuId}
            anchorEl={anchorEl}
            open={isMenuOpen}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            getContentAnchorEl={null}
            keepMounted
            onClose={handleMenuClose}
            >

            {isLoggedIn && currentAccount.id !== ""
            ?   <div>
                <Link to = "/AccountProfile">
                    <MenuItem onClick={handleMenuClose}>
                        {currentAccount.firstName} {currentAccount.lastName}
                    </MenuItem>
                </Link>

                <Link to = "/AccountSettings">
                    <MenuItem onClick={handleMenuClose}>
                        Account Settings
                    </MenuItem>
                </Link>
                </div>
            : null}

            <MenuItem onClick={() => {
                handleMenuClose()
                dispatch(AccessActions.setAccessTokens({}, "LOGOUT"))
                dispatch(AccessActions.checkTokenStatus())
            }}>
                    Log out
            </MenuItem>

        </Menu>
    );

    const mobileMenuId = 'navigationBarMobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            id={mobileMenuId}
            open={isMobileMenuOpen}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            getContentAnchorEl={null}
            keepMounted
            onClose={handleMobileMenuClose}
        >
            {/* <MenuItem>
                <IconButton aria-label="show 4 new mails" color="inherit">
                    <Badge badgeContent={4} color="secondary">
                        <Mail />
                    </Badge>
                </IconButton>
                <p>Messages</p>
            </MenuItem> */}

            {/* <MenuItem>
                <IconButton aria-label={"show " + notificationsCount + " new notifications"} color="inherit">
                    <Badge badgeContent={notificationsCount} color="secondary">
                        <Notifications />
                    </Badge>
                </IconButton>
                <p>Notifications</p>
            </MenuItem> */}

            {isLoggedIn && currentAccount.id !== ""
                ?   <MenuItem onClick={handleProfileMenuOpen}>
                        <IconButton aria-label="account of current user" aria-controls="navigationBar" aria-haspopup="true" color="inherit">
                            <AccountCircle />
                        </IconButton>
                        <p>{currentAccount.firstName} {currentAccount.lastName}</p>
                    </MenuItem>
                : null
            }
        </Menu>
    );

    // useEffect(() => {
    //     let currentEmail = localStorage.getItem("email")
    //     if(currentEmail){
    //         dispatch(AccessAPI.getAccountInfo(Date.now(), currentEmail))
    //     }
    //     dispatch(EventAPI.fetchEvents(Date.now()))
    // }, [users]);

    useEffect(() => {
        let unopened = 0
        for (let notif of notifications) {
            if (!notif.opened) {
                unopened++
            }
        }
        setNotificationsCount(unopened)
    }, [notifications])

    useEffect(() => {
        if (typeof socketMessage === 'number') {
            setNotificationsCount(socketMessage)
        }
    }, [socketMessage])
    
    // Desktop Version
    return (
        <div className="top">
        <AppBar position="fixed" className="appBar">
            <Toolbar>
                <Link to = "/">
                    <IconButton aria-label="Logo" color="inherit" id="logo" style={{color:'white'}}>
                        HTe Enterprise
                    </IconButton>
                </Link>

                <div className="grow"/>
                {isLoggedIn 
                ?   <div className="sectionDesktop">
                        <img src={img} className="hobartLogo" alt=""/>

                        {/* <Link to = "/products">
                            <IconButton aria-label="Products" color="inherit">
                                <StorefrontOutlined />
                                Our Products
                            </IconButton>
                        </Link>

                        <Link to = "/create">
                            <IconButton aria-label="Create" color="inherit">
                                <AddCircle />
                                Create
                            </IconButton>
                        </Link> */}

                        {/* Mail/Messages */}
                        {/* <IconButton aria-label="show 4 new mails" color="inherit">
                            <Badge badgeContent={4} color="secondary">
                                <Mail />
                            </Badge>
                        </IconButton> */}

                        {/* Notifications */}
                        {/* <Link to = "/notifications">
                            <IconButton 
                                edge="end"
                                aria-label={"show " + notificationsCount + " new notifications"}
                                color="inherit">
                                <Badge badgeContent={notificationsCount} color="secondary" style={{color:'white'}}>
                                    <Notifications />
                                </Badge>
                            </IconButton>
                        </Link> */}

                        {/* Profile/Account Settings */}
                        <IconButton
                            className="userProfile"
                            style={{color: "#FFF"}}
                            edge="end"
                            aria-label="account of current user"
                            aria-controls={menuId}
                            aria-haspopup={true}
                            onClick={handleProfileMenuOpen}
                            // color="inherit"
                        >
                            <AccountCircle />
                            {/* {user.firstName} */}
                        </IconButton>
                    </div>
                :   <img src={img} className="hobartLogo" alt=""/>
            }

                {/* When window size is too small or on mobile */}
                {isLoggedIn 
                ?   <div className="sectionMobile">
                        <img src={img} className="hobartLogo" alt=""/>

                        <IconButton
                            aria-label="show more"
                            aria-controls={mobileMenuId}
                            aria-haspopup="true"
                            onClick={handleMobileMenuOpen}
                            color="inherit">
                            <MoreVert />
                        </IconButton>
                    </div>
                :   null}
            </Toolbar>
        </AppBar>

        {renderMobileMenu}
        {renderMenu}
        </div>
    );
}
