import { useHistory } from "react-router-dom"
import { store } from "../../state"
import { HTeDomain, HTeRole, HTeUser } from "../../types/access/AccessTypes"
import logger from "../utils/logger"

interface AccessControlType{
    user ?: HTeUser
    requiredRole ?: HTeRole
    requiredDomain ?: HTeDomain
    pageRequest?: Boolean
    boolController?: React.Dispatch<React.SetStateAction<boolean>>  //Returns children but sets to fale. USE WISELY
    children : any
}


/**Check Domain
 *  Determines if the user has access to the required domain, by moving up the domain 
 *  tree to see if the required domain belongs to the users domain
 * @param requiredDomain - The domain needed to perform actions on the item in question
 * @param userDomain - Domain that the user belongs to
**/
function checkDomain(requiredDomain : HTeDomain | undefined, userDomain : HTeDomain) : boolean {
    const allDomains = store.getState().access.domains

    if(!requiredDomain){
        logger.error("AccessControl - checkDomain required domain not found! ")
        return false
    }

    if(requiredDomain.domainId === userDomain.domainId){
        return true
    }
    else if (requiredDomain.parentId == null){
        //If the currently checked domain is Admin and user Domain is not Admin, 
        //Then there are not any more domains to check in the tree
        return false
    }
    else{
        return checkDomain(allDomains[requiredDomain.parentId], userDomain)
    }
}

/**Check Permissions
 *  Determines if the user has access to the the requested item
 * @param requestor - Current User requesting access to resource
 * @param requiredRole - Role needed to access the resource
**/
const checkPermissions = (requestor: HTeUser, requiredRole ?: HTeRole, requiredDomain ?: HTeDomain) : boolean => {
    if(requiredDomain){
        const hasDomain = checkDomain(requiredDomain, requestor.domain)
        //If the user has the required domain, move onto access checks, otherwise return
        if(!hasDomain){
            return false
        }
        // If the access controller is only checking Domain
        if(!requiredRole){
            return true
        }

    }
    if(requiredRole){
    // Check if user's has a custom role set and if they do then check their custom roles
    //If the user has the role then give permission
    if (requestor.roles.find((role) => role.id === requiredRole.id)) return true
    //Next check if the user just has an access level higher than the required role
    //User's access level must have an access level greater than the required
    else if (requestor.accessLevel > requiredRole.id)  {return true}
    //If user does not have the specified role or a access level greater than 
    //the one required they do not have permission
    }
    return false
}

const AccessControl = (props: AccessControlType) => {
    const history = useHistory();
    const {user, requiredRole, requiredDomain, pageRequest, boolController, children} = props
    if(!user){
        logger.error("AccessControl - Requesting User was not valid : ", user)
        return null
    }
    if (checkPermissions(user, requiredRole, requiredDomain)) {
        if(boolController){
            boolController(true)
        }
        return <>{children}</>;
    }
    if(pageRequest){
        history.push("/NotPermitted")
    }
    
    // We can specify what is returned if permitted === false, but currently just returning null
    // logger("Access Denied")

    // Allows entity to be returned but bool should disable children 
    if(boolController){
        boolController(false)
        return <>{children}</>;
    }
    return null;    
};  
  

export default AccessControl;