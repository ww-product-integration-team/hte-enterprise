import { useDispatch } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import { ProductAPI } from '../api'
import { ProductsActions } from '../../state'


export default function PrivateRoute ({component: Component, authenticated, ...inputProps}) {
    // used to clean redux of products
    // products is too large so should only be pulled when needed
    let dispatch = useDispatch()
   if(inputProps.path !== "/Products"){
        dispatch(ProductsActions.setProducts([]))
   }
    return (
        <Route
            {...inputProps}
            render={(props) => authenticated === true
                // Ex. <Component /> === <Dashboard />
                ? <Component {...props} />      

                // If not logged in, redirect user to login screen
                : <Redirect to={{pathname: '/Login', state: {from: props.location}}} />}
        />
    )
  }