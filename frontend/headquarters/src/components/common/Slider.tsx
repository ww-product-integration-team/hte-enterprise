import ReactSlider from "react-slider";
import { useState, useEffect} from "react";
import "../styles/index.css"
import { IconButton, Tooltip, Typography } from '@mui/material';
import { Help } from '@mui/icons-material';
import React from "react";

export interface SliderTemplateProps{
    title : string
    max : number
    min : number
    units : string
    message : string
    value : number
    onValueChange : (inputeValue : number) => void
    errorStatus : (status : string) => void
}

const SliderTemplate = (props : SliderTemplateProps) => {

    const title = props.title;
    const sliderMax = props.max;
    const sliderMin = props.min;
    const units = props.units;
    const message = props.message;

    const [currentValue, setCurrentValue] = useState<number | undefined>(props.value);

    useEffect(() =>{
        setCurrentValue(props.value)
    }, [props.value])

    function changeInputValue(input : number){


        let inputValue = Math.round(input);
        if (Number.isInteger(inputValue)){
        
            setCurrentValue(inputValue);
            props.onValueChange(inputValue);

        }else{
            props.errorStatus("error");
        }
    }

    return ( 
    <><div style={{ display: 'flex', flexDirection: 'row', alignItems: "center", marginRight: ".5rem", marginLeft: ".5rem"}}>
        <label className="sliderLabel">{title}</label>
        <input 
            type="text" 
            inputMode="text"
            name="slider" 
            id="slider" 
            className="sliderOutput" 
            value = {currentValue} 
            onChange={(e) =>{
                const newVal = Number.parseInt(e.target.value)
                if(e.target.value === "" || e.target.value === "-"){
                    setCurrentValue(undefined)
                }
                else if(newVal){
                    changeInputValue(newVal)
                }
            }}                
            max = {sliderMax}
            min = {sliderMin}
        />
        <label className="sliderLabel">{units}</label>
        <Tooltip className="info" title={<Typography fontSize={12}>{message}</Typography>}>
            <IconButton aria-label="enableAccount" disableRipple>
                <Help />
            </IconButton>
        </Tooltip>
    </div>
    <div className="sliderDiv">
    <ReactSlider
        className="settingSlider"
        thumbClassName="settingSliderThumb"
        trackClassName="settingSliderTrack"
        min = {sliderMin}
        max = {sliderMax}
        defaultValue = {1}
        value= {currentValue}
        onChange = { (value : number) => changeInputValue(value)}
    />
    </div></>

    );
}
 
export default React.memo(SliderTemplate);