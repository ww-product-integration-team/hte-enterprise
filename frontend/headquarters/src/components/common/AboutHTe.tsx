import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome} from '@fortawesome/free-solid-svg-icons';
import { Col, Row, Breadcrumb, Container } from '@themesberg/react-bootstrap';
import { ConfigActions, useAppSelector} from "../../state";
import VirtualTable from "./virtualTable/VirtualTable";
import { Service, ServiceType } from '../../types/config/services';
import { useDispatch } from 'react-redux';

const serviceHeadCells = [
    { id: 'serviceName',        sorted: false, searchable: false,  label: 'Service Name',          isShowing: true, mandatory: true},
    { id: 'databaseId',         sorted: false, searchable: false,  label: 'Database ID',          isShowing: false, mandatory: false},
    { id: 'operationMode',      sorted: false, searchable: false,  label: 'Operation Mode', isShowing: false, mandatory: false},
    { id: 'url',                sorted: false, searchable: false,  label: 'URL',             isShowing: false, mandatory: false},
    { id: 'version',            sorted: false, searchable: false,   label: 'Version',           isShowing: true, mandatory: true },
    { id: 'buildDate',          sorted: false, searchable: false,   label: 'Build Date', isShowing: true, mandatory: true  },
    // { id: "id", narrow: false, disablePadding: false, label: 'ID', mandatory: false },   // can use this to filter for Region / Banner this store belongs to
];


export const AboutHTe = () => {

    // Redux
    const services : Service[] = useAppSelector((state) => state.config.services);
    const dispatch = useDispatch();
    //Retrieve IP Address of frontend service


    if(services && !services.find((item: Service) => item.serviceType === "FRONTEND")){

        let newServices = [...services]
        
        newServices.push({
            id:"1s3008a7-6a7e-4ca9-8032-6b74ba0d6f4e",
            databaseId:"12341234-1234-1234-1234-123412341234",
            serviceType: ServiceType.FRONTEND,
            serviceName:"User Interface",
            operationMode:"Release",
            lastRestart:"N/A",
            url:"http://" + window.location.hostname + ":" + window.location.port,
            version:"2.0.3",
            buildDate:"10/14/2024",
        })
        dispatch(ConfigActions.setServicesList(newServices))
    }

    return (
    <>
        <div className="mb-4 mb-md-0 ">
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>About HTe</Breadcrumb.Item>
            </Breadcrumb>
        </div>

        <Container >
            <Col xs={12} sm={12} xl={12} className="d-flex align-items-center justify-content-center ">
            <div className="mb-4 mb-lg-0 bg-white shadow-soft border rounded border-light p-4 w-100 form-control">
                <Row>
                    <Row>
                        <Col xl={12} className="text-align-center mb-xl-0">
                            <h4 className="mb-3 fw-bolder text-align-center">Current Build Information</h4>
                        </Col>

                        <Col xl={12} className="text-align-center mb-xl-0">
                            <p>
                                <span className="fw-bold">
                                    Found an Issue? Report it to 
                                    <a 
                                        className="fw-bold"
                                        href={"mailto:HT.Support@itwfeg.com?subject=Please Enter a Brief Title &body=Please provide a reason for contacting us."}
                                        target="_blank" 
                                        rel="noreferrer noopener"
                                        style={{paddingLeft:"0.5rem"}}
                                    >
                                        {`HT.Support.itwfeg.com`}
                                    </a>
                                </span>
                            </p>
                        </Col>

                        <VirtualTable
                            tableName="HQServicesTable"
                            useToolbar={false}
                            dataSet={services}
                            headCells={serviceHeadCells}
                            initialSortedBy={{name: '' }}
                            dispatchedUrls={[]}
                            saveKey="HQServicesTableHead"
                        />
                        <hr style={{marginTop:"1rem"}}/>
                    </Row>

                    <Row>
                        <Col xl={12} className="text-xl-center d-flex align-items-center justify-content-xl-center mb-xl-0">
                            <Row>
                                <Col xl={12} className="text-align-center mb-xl-0">
                                    <h4 className="mb-3 fw-bolder text-align-center">Release Notes</h4>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>10/14/2024: Version 1.3.0</span>

                                    <ul>
                                        <li>Removed filterings from Manual Upgrading dialog</li>
                                        <li>Fixed issue with assets duplicating</li>
                                        <li>Apphook performance improvements</li>
                                        <li>Added automatic update times to Asset List for gathering most recent data</li>
                                        <li>Minor improvements to Asset List tree building logic</li>
                                        <li>Heartbeating performance significantly improved</li>
                                        <li>Clarification of information throughout</li>
                                        <li>Updated Spring Boot configuration to version 2.7.5</li>
                                        <li>Added tooltips to multiple pages for what buttons do</li>
                                        <li>Addressed issue with creating Asset List filters</li>
                                        <li>Modified check when creating upgrade groups to restrict to homogeneous devices</li>
                                        <li>Bug fixes and general improvements</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>09/16/2024: Version 1.2.1</span>

                                    <ul>
                                        <li>Sorted all selection dropdowns alpha-numerically</li>
                                        <li>Added Departments filter to Manual Upgrading dialog</li>
                                        <li>Uploaded upgrade files now overwrite duplicates</li>
                                        <li>Resolved issue of scales reporting not in sync with HTe Enterprise time</li>
                                        <li>New startup script to rerun services until they are working</li>
                                        <li>Fixed issue with assets duplicating</li>
                                        <li>Apphook displays currently assigned Scale Profile</li>
                                        <li>Apphook can change frequency of Transactions upload</li>
                                        <li>Apphook performance improvements</li>
                                        <li>First steps of integrating NGW with Enterprise by having Apphook collect and send data to the HTe Enterprise server</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>07/31/2024: Version 1.2.0</span>

                                    <ul>
                                        <li>Pricing Zones added to Scale Profiles</li>
                                        <li>Can immediately upgrade single scale from Scale Details page</li>
                                        <li>PLU data can be imported by a new endpoint</li>
                                        <li>NTP and time zone information is now reporting from Apphook</li>
                                        <li>Upgrade Manager now has manual upgrading</li>
                                        <li>GT scales can upgrade with primary and secondary communications</li>
                                        <li>Apps Page introduced with Label Designer and HTe Scale Management downloads</li>
                                        <li>Apphook installation on scales now persists after upgrade</li>
                                        <li>Auto Assignment Rules overlapping bug fixed and stores more closely tied to Rules</li>
                                        <li>CSV data importing bug fixed</li>
                                        <li>Bug fixes and Improvements and code clean up</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>04/22/2024: Version 1.1.0</span>

                                    <ul>
                                        <li>Asset List filtering refactored to be more efficient</li>
                                        <li>Importing and deleteing Asset data now forces user to wait until process is complete before doing any other actions</li>
                                        <li>More scale information being reported in Dashboard and Asset List</li>
                                        <li>Bug fixes and Improvements and Code clean up</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>03/08/2024: Version 1.0.4</span>

                                    <ul>
                                        <li>Asset List now loads data lazily</li>
                                        <li>Bug fixes and Improvements and Code clean up</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>01/22/2024: Version 1.0.3</span>

                                    <ul>
                                        <li>Login screen license warning now properly shows number of days left on license</li>
                                        <li>License upload screen now works with Windows and Unix paths</li>
                                        <li>Added Upgrade Manager</li>
                                        <li>Added Products view at URL: {"<ipaddress or hostname>"}/Products</li>
                                        <li>Added filtering in Asset List screen</li>
                                        <li>Asset List data can now be imported from the Utils page with a CSV file or XML</li>
                                        <li>Added asset auto assigning in Utils page</li>
                                        <li>Bug fixes and Improvements</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>02/09/2023: Version 1.0.2 </span>
                                    
                                    <ul>
                                        <li>Login screen has been revamped with loading animation and dynamic loading times</li>
                                        <li>Apphook version of the scales in the fleet is now being reported</li>
                                        <li>HTe Enterprise will now send down store name and store id information depending on it's assigned store</li>
                                        <li>Revamp of Profiles to allow for multiple different file types, specifically media files to upload to the scale</li>
                                        <li>Fleet Status component added. View allows users to scan and detect the status of their fleet as well as allow them to import and export the results of the scan in the form of a .csv file</li>
                                        <li>Ability to select/highlight rows on the asset list</li>
                                        <li>Migration of Codebase from Javascript to Typescript</li>
                                        <li>Bugfixes and Improvements</li>
                                    </ul>

                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>11/09/2022: Version 1.0.1 </span>
                                    
                                    <ul>
                                        <li>Minor bugfixes/improvements. Removing unused imports, and files</li>
                                    </ul>

                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>10/04/2022: Version 1.0.0 </span>
                                    
                                    <ul>
                                        <li>Store View Details available in AssetList</li>
                                        <li>Uptime percentage displayed on the sync graph</li>
                                        <li>Redux Store information now clears on a logout</li>
                                        <li>Transactions Page to view and download transactions</li>
                                        <li>Virtual Table now remembers preferential columns</li>
                                    </ul>

                                </Col>


                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>09/06/2022: Version 0.1.2 </span>
                                    
                                    <ul>
                                        <li>Redux Store now keeps data through a refresh</li>
                                        <li>Profile Form has been updated to remove relesecting of profile to perform an action</li>
                                        <li>Offline/Away Configuration Sliders added (Not Functional in the Version)</li>
                                        <li>Store Details now displaying Store servers available services if any</li>
                                    </ul>

                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>07/19/2022: Version 0.1.1 </span>
                                    
                                    <ul>
                                        <li>Created a Form to set the Delete event config, on new “Settings Page”</li>
                                        <li>Delete Scale and Remove Scale are now split into different forms and also included on the scale details page</li>
                                        <li>Store and Scales now being saved in redux store as a dictionary</li>
                                        <li>Live updates for Scale details page, now polling live sync status of the scale</li>
                                        <li>New Store details page</li>
                                        <li>Total, Offline, Online, Disabled scale counters for the Front dashboard view</li>
                                    </ul>

                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>06/07/2022: Version 0.0.9 </span>
                                    
                                    <ul>
                                        <li>Update to react 18</li>
                                        <li>Refactoring of entity field Id names</li>
                                        <li>Migration to ChartJS, replacing chartist</li>
                                        <li>Freshening up of the Directions page</li>
                                        <li>Refactor of functionality to determine whether user is logged in</li>
                                    </ul>

                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>05/13/2022: Version 0.0.8 </span>
                                    
                                    <ul>
                                        <li>Updated hover dialog boxes on Dashboard piecharts</li>
                                        <li>Window now scrolls to the top of the page on a page change</li>
                                        <li>New virtual table for large number of assets</li>
                                        <li>“Add Banner” is now “Add Entity” which will allow you to add an entity any where in the asset list without having to navigate down to the entity</li>
                                    </ul>

                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>04/27/2022: Version 0.0.7 </span>
                                    
                                    <ul>
                                        <li>Unified response callback functionality</li>
                                        <li>package.tgz  support</li>
                                        <li>Dashboard updates: scale cards indicating offline and unassigned. As well as filterable tables for the user to sort through assets</li>
                                        <li>License Installation and Management</li>
                                        <li>Updated status monitor graphs to display 1day, 1 week or 1 month ranges</li>
                                        <li>Bugfix: Empty departments will now persist on a refresh</li>
                                        <li>Updated Dashboard tables allowing for adding and removing of columns</li>
                                        <li>Added Dashboard status cards allowing you to view unassigned and offline scales</li>
                                    </ul>


                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>03/10/2022: Version 0.0.6 </span>
                                    <ul>
                                        <li>Bugfix : Form not closing on creation of new department</li>
                                        <li>Dashboard now filtering based on the user’s domain</li>
                                        <li>Increased timeouts for some scale commands</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>03/08/2022: Version 0.0.5 </span>
                                    <ul>
                                        <li>Bugfixes</li>
                                        <li>New : Department Manager Page </li>
                                        <li>New : Legends for Graphs and Asset List</li>
                                        <li>New : Allowing internal users to change their access level and domain for testing purposes</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold" style={{color: "black"}}>03/04/2022: Version 0.0.4 </span>
                                    <ul>
                                        <li>Bug Fix: patched crashing issue with popups on AssetDetails Page</li>
                                        <li>New : Generic Logger, for eventual backend log uploads</li>
                                        <li>New : Now displaying total labels printed and PLU count</li>
                                    </ul>
                                </Col>

                                <Col xs={6} className="mb-3 releaseNotes">
                                    <span className="fw-bold color-black">03/02/2022: Version 0.0.3 </span>
                                    <ul>
                                        <li>Bug Fix: Styling fixes</li>
                                        <li>New: Log-in now required</li>
                                        <ul>
                                            <li>Authentication via JWT, with auto timeout after 30 minutes</li>
                                        </ul>
                                        <li>New: Asset list format change</li>
                                        <ul>
                                            <li>Virtual List for large asset list size</li>
                                            <li>Drag and Drop support for Scales</li>
                                        </ul>
                                        <li>New: User Roles and Access Levels</li>
                                        <ul>
                                            <li>Ability to edit existing users</li>
                                            <li>Create new accounts</li>
                                            <li>Assigning roles</li>
                                        </ul>
                                    </ul>
                                </Col>

                                <Col xs={8}className="mb-3 releaseNotes">
                                    <span className="fw-bold color-black">01/28/2022: Version 0.0.2 </span>
                                    <ul>
                                        <li>Bug Fix: Removal of react developer tools</li>
                                    </ul>
                                </Col>

                                <Col xs={8}className="mb-3 releaseNotes">
                                    <span className="fw-bold color-black">01/28/2022: Version 0.0.1 </span>
                                    <ul>
                                        <li>New: Initial Internal Server Release
                                            <ul>
                                                <li>Basic Asset List</li>
                                                <li>Dashboard</li>
                                                <li>Scale Profiles</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </Col>
                            </Row>
                        </Col>
                    </Row>

                </Row>
            </div>
            </Col>
        </Container>
    </>
    );
};

export default AboutHTe;