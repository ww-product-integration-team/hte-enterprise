import { instanceOfScaleDevice, ScaleDevice } from "../../types/asset/ScaleTypes";
import { lazyNode } from "../../types/asset/TreeTypes";
import logger from "./logger";

//Async
export const sleep = (ms : number) => new Promise(r => setTimeout(r, ms));

//Synchronous
export function wait(ms : number) {
    var start = Date.now(),
        now = start;
    while (now - start < ms) {
      now = Date.now();
    }
}

/*Function to validate the IP address
    @param ip - The IP address to validate
    @returns True if the IP address is valid, false otherwise
*/
export function validateIPv4(ip: string) {
    let ipArray = ip.split(".")
    // Check if the IP address has 4 parts
    if (ipArray.length !== 4) {
        return false
    }
    for (let i = 0; i < 4; i++) {
        // Check if the IP address is a number
        if (isNaN(parseInt(ipArray[i]))) {
            return false
        }
        // Check if the IP address is in the correct range
        if (parseInt(ipArray[i]) < 0 || parseInt(ipArray[i]) > 255) {
            return false
        }
    }
    return true
}
/*Function to sort IPv4 addresses
@param a - IPv4, b - IPv4
@implemenation -> is to be used within a .sort() method -> foo.sort(a,b => sortIPv4(a,b))
@returns -1 if a >b,  1 if a < b, 0 otherwise
*/
export function sortIPv4(a:string, b:string){
    let A = a.split(".")
    let B = b.split(".")
    if(A.length !==4 || B.length !==4){
        logger.error("Invalid IPs provided")
        return 0
    }
    for(let i = 0; i < 4; i++){
        if(A[i] === B[i]){
            continue
        } else if(A[i] > B[i]){
            return 1
        } else {
            return -1
        }
    }
    return 0
}
/*
Function tocapitalize the first letter of a string
*/
export function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
/*Version Parser Funtion
Parses a version string into an array of numbers to compare against valid versions. 
Helps determine if a version is greater than 4.1.300 or is exactly 3.3.628
@param version - version to compare (string)
*/
export function isValidApphookVersion(version : string) : boolean {
    let versionArray : number[] = []
    let versionStringArray = version.split(".")
    for(let i = 0; i < versionStringArray.length; i++){
        versionArray.push(parseInt(versionStringArray[i]))
    }
    
    if(versionArray.length > 3){
        //Invalid version format
        return false
    }

    if(versionArray[0] === 4){
        if(versionArray[1] > 1){
            return true
        }
        else if(versionArray[1] === 1 && versionArray[2] >= 300){
            return true
        }
    }
    else if(versionArray[0] === 3 && versionArray[1] === 3 && versionArray[2] === 628){
        return true
    }


    return false
}

/*Compare arrays
compare string arrays positionally and determines if any strings do not match
This is created because typescript compares by address and not by value
*/
export function compareArrays(firstArray : string[], secondArray :string[]){
    if(firstArray.length !== secondArray.length){
        return false
    }
    for(let pos = 0; pos < firstArray.length; pos++){
        if(firstArray[pos]!==secondArray[pos]){
            return false
        }
    }
    return true
}
export function formatBytes(bytes : number, decimals = 2) {
    if (!bytes) return null;
    if (bytes === 0) return '0 B';
  
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  
    const i = Math.floor(Math.log(bytes) / Math.log(k));
  
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

export function findScaleNodes(lazyTree : {[key: string]: lazyNode}) : ScaleDevice[]{
    let scaleNodes : ScaleDevice[] = []
    Object.keys(lazyTree).forEach(key => {
        if (lazyTree[key].type === "scale" && instanceOfScaleDevice(lazyTree[key].properties)) {
            scaleNodes.push(lazyTree[key].properties as ScaleDevice)
        } else if (lazyTree[key].children && Object.keys(lazyTree[key].children).length > 0) {
            let foundNodes = findScaleNodes(lazyTree[key].children)
            if (foundNodes.length > 0) {
                scaleNodes.push(...foundNodes)
            }
        }
    })
    return scaleNodes
}

export function verifyUuid(uuid: string) {
    if (uuid && uuid.includes("-") && uuid.length === 36) {
        let parts = uuid.split("-")
        if (parts.length === 5) {
            if (parts[0].length === 8 && parts[1].length === 4 && parts[2].length === 4
                && parts[3].length === 4 && parts[4].length === 12
            ) {
                let p1 = parts[0].match(/[a-z0-9]{8}/)
                let p2 = parts[1].match(/[a-z0-9]{4}/)
                let p3 = parts[2].match(/[a-z0-9]{4}/)
                let p4 = parts[3].match(/[a-z0-9]{4}/)
                let p5 = parts[4].match(/[a-z0-9]{12}/)
                if (p1 && p1[0] && p2 && p2[0] && p3 && p3[0] && p4 && p4[0] && p5 && p5[0]) {
                    return true
                }
                return false
            }
            return false
        }
        return false
    }
    return false
}
