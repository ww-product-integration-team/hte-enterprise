import { useDispatch} from "react-redux";
import { useState, useEffect, useContext} from 'react';
import { ResponseCallback, ResponseContext, ResponseContextType } from "./ResponseProvider";
import { handleResponse } from "../../state/actions/endpointActions";
import { NotificationDetails } from "../assetDetails/NotificationDetails";
import logger from "./logger";
import { notify, Notification, NotificationButton, STATUSES} from 'reapop';
import { useAppSelector } from "../../state";
import { ResponseType } from "../../types/storeTypes";

export function ResponseHandler(){
    const responses : {[key : number] : ResponseType} = useAppSelector((state) => state.endpoint.endpointResponses);
    const { callbacks, removeCallback } : ResponseContextType = useContext(ResponseContext);
    const [openViewNotDetails, setOpenViewNotDetails] = useState(false);
    const [notificationDetails, setNotificationDetails] = useState({})
    const dispatch = useDispatch();

    function handle(response : ResponseType){
        if(!response){return}
        try{
            const callback : ResponseCallback = callbacks[response.id]
            //check to see that response has a callback attached
            if(callback == null){
                //logger("Unhandled response : ", response)
                dispatch(handleResponse(response.id))
                return
            }

            const viewDetails = () => {
                setOpenViewNotDetails(true)
                setNotificationDetails({
                                        response: response,
                                        notification: callback.notification ? callback.notification.payload : {}})
            }
            
            let not : Partial<Notification> = {
                buttons : [],
                dismissable : true,
                dismissAfter : 10000,
                status : STATUSES.none,
                message : ""
            }

            if(callback.notification != null){
                not = callback.notification.payload
            }
            
            let detailsButton : NotificationButton= {name:"Details", primary:true, onClick: viewDetails}
            let dismissButton : NotificationButton = {name:"Dismiss", primary: true}
            not.buttons = [detailsButton, dismissButton]
            not.dismissible = true
            not.dismissAfter = 10000

            const reapop = callback.reapop
            let showResult = true
            if(response.success){
                if(reapop != null){
                    not.status = "success"
                    if(reapop.successMsg === "default"){
                      not.message = callback.name + " command sent!"
                    }
                    else if(reapop.successMsg == null){showResult = false}
                    else{
                      not.message = reapop.successMsg
                    }
                }

                callback.onSuccess(response)
            }
            else{
                if(reapop != null){
                    not.status = "error"
                    if(reapop.failMsg === "default"){
                      not.message = "Error sending " + callback.name + " command!"
                    }
                    else if(reapop.failMsg == null){showResult = false}
                    else{
                      not.message = reapop.failMsg
                    }
                }

                callback.onFail(response)
            }

            if(reapop != null && showResult){
                if(!callback.handleOne || not.status != "success")
                    dispatch(notify(not))
            }

            //Response has been handled, clear the entry
            dispatch(handleResponse(response.id))

            //Now clear the response's callback
            let tmp = {...callbacks}
            delete tmp[response.id]
            if(removeCallback){ //Verify removeCallback isn't null
                removeCallback(tmp)
            }
            else{
                logger.error("ResponseHandler: removeCallback was null!")
            }
            
        
        } catch (error) {
            //Do Nothing
            logger.error("(ResponseHandler) Caught error ", error)
        }
    }

    useEffect(()=>{
        for(const rspId in responses){
            let tmp = responses[rspId]
            handle(tmp)
            break;
        }
    }, [responses, callbacks])


    return(
        <>
        {openViewNotDetails ? 
            <NotificationDetails
                title="Notification Details"
                data={notificationDetails}
                openPopup={openViewNotDetails}
                setOpenPopup={setOpenViewNotDetails}/> : null}
        </>
    )
}