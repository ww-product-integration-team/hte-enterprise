import { format } from "date-fns"
import { Dispatch } from "redux"
import { FileAPI } from "../api"


enum LogTypes{
    ERROR = 0,
    INFO =  1,
    DEBUG = 2,
    TRACE = 3,
}

export interface LogType{
    dateAndType : string
    message: string
    object?: string
}

export default class logger {
    static saveLog = false
    static showLog = false
    static logLevel = LogTypes.DEBUG
    static logName = "logger"

    static error(message : string, object ?: any){
        if(logger.logLevel >= LogTypes.ERROR){
            this.log(message, object, LogTypes[LogTypes.ERROR])
        }
    }

    static info(message : string, object ?: any){
        if(logger.logLevel >= LogTypes.INFO){
            this.log(message, object, LogTypes[LogTypes.INFO])
        }
    }

    static debug(message : string, object ?: any){
        if(logger.logLevel >= LogTypes.DEBUG){
            this.log(message, object, LogTypes[LogTypes.DEBUG])
        }
    }

    static trace(message : string, object ?: any){
        if(logger.logLevel >= LogTypes.DEBUG){
            this.log(message, object, LogTypes[LogTypes.TRACE])
        }
    }

    private static log(message : string, object : any, type : string) : void{
        const log = "["+format(Date.now(), "Y-MM-dd HH:mm:ss")+"] ["+type+"] - "
    
        if(logger.showLog){
            if(object == null){
                console.log(log, message) 
            }
            else{
                console.log(log + message, object) 
            }
        }
        
        if(logger.saveLog){
    
            let messageString = ""
            switch(typeof message){
                case 'object':
                    messageString = JSON.stringify(message)
                    break;
                default:
                    messageString = message
            }
    
            let storageString = localStorage.getItem(logger.logName)
            let recentLogs = storageString ? JSON.parse(storageString) : null
            const newLog : LogType= {
                dateAndType : log,
                message: messageString,
                object: JSON.stringify(object)
            }
    
    
            if(recentLogs == null){
                localStorage.setItem(logger.logName, JSON.stringify([newLog]))
            }
            else{
                localStorage.setItem(logger.logName, JSON.stringify([...recentLogs, newLog]))
            }
    
            //TODO Create an endpoint for the backend to save logs
        }
    }


    
}


export function SendLogs(dispatch : Dispatch<any>){

    if(logger.saveLog){
        let storageString = localStorage.getItem(logger.logName)
        let recentLogs : LogType[] = storageString ? JSON.parse(storageString) : null
        

        if(recentLogs != null && recentLogs.length > 20){ //Send and clear local logs
            dispatch(FileAPI.postLogs(Date.now(), [...recentLogs]))
        }
    }
}