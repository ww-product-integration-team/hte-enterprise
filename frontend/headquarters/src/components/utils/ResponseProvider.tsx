import React, { useState, useCallback, useContext} from 'react';
import { notify } from 'reapop';
import { useDispatch} from "react-redux";
import { UpsertNotificationAction } from 'reapop/dist/reducers/notifications/actions';
import { EndpointActions, useAppSelector } from '../../state';
import { ResponseType } from "../../types/storeTypes";
import { setActiveRequests } from '../../state/actions/endpointActions';
import { ShowInfoMessage } from '../../types/utils/utilTypes';


interface ReapopMsg {
    requestMsg?: string
    successMsg?: string
    failMsg?: string
}

export interface ResponseCallback {
    id: number
    name : string
    notification : UpsertNotificationAction | null
    onSuccess : (response : ResponseType) => void
    onFail :  (response : ResponseType) => void
    reapop : ReapopMsg
    handleOne ? : boolean
}

export interface ResponseContextType {
    callbacks: {[id: number] : ResponseCallback}
    removeCallback: ((newState : {[id: number] : ResponseCallback}) => void)
    addCallback: ((state : {[id: number] : ResponseCallback}, 
                    requestId : number,
                    requestName : string,
                    onSuccess : (response : ResponseType) => void,
                    onFail : (response : ResponseType) => void,
                    reapop ?: ReapopMsg,
                    handleOne ? : boolean) => boolean)
}

export const ResponseContext = React.createContext<ResponseContextType>({
    callbacks: {}, 
    addCallback: (state : {[id: number] : ResponseCallback}, requestId : number, requestName : string = "", 
        onSuccess : (response : ResponseType) => void, onFail : (response : ResponseType) => void, reapop ?: 
        ReapopMsg, handleOne?: boolean) => {return false},
    removeCallback: (newState : {[id: number] : ResponseCallback}) => {}
});

export function useRspHandler() {
    const { callbacks, addCallback, removeCallback } = useContext(ResponseContext);
    return { callbacks, addCallback, removeCallback };
}

export function ResponseProvider({ children }) {
    const [callbacks, setCallbacks] = useState<{[id: number] : ResponseCallback}>({});
    const dispatch = useDispatch();
    const activeRequests = useAppSelector((state) => state.endpoint.activeRequests)
    const maxActiveRequests = useAppSelector((state) => state.endpoint.maxActiveRequests)


    const removeCallback = (newState : {[id: number] : ResponseCallback}) => {
        setCallbacks({...newState})

        let newActiveRequests = activeRequests - 1
        if(newActiveRequests < 0){newActiveRequests = 0}
        dispatch(EndpointActions.setActiveRequests(newActiveRequests))
    };

    /*
    Add Callback parameters, 
    state : passback in the current state to add to existing list
    requestId: id attached to the request, this function will use this ID to find the response
    requestName(Optional) : Name you want to assign the request
    onSuccess : function to be run when a response succeeds
    onFail : function to be run when a response errors out
    reapop : { (NOTE) if reapop is null no messages will be shown
        requestMsg : Message that will be displayed for the pending request
        successMsg : Message that wlil be displayed when request succeeds
        failMsg : Message that will be displayed when request fails
    }
    */
    const addCallback = (state : {[id: number] : ResponseCallback}, requestId : number, requestName : string = "", 
                        onSuccess : (response : ResponseType) => void, onFail : (response : ResponseType) => void, reapop ?: ReapopMsg, handleOne? : boolean) =>{

        if(!reapop){
            reapop = {requestMsg: "default", successMsg: "default", failMsg: "default"}
        }

        if(activeRequests > maxActiveRequests){
            dispatch(notify('Too many current requests, please wait!', 'warning'))
            return false
        }
        if(handleOne){
            if(activeRequests > 1){
                dispatch(notify('Too many current requests, please wait!', 'warning'))
                return false
            }
        }

        if(Object.values(state).find((callback) => (callback.name !== "Upload to Primaries" && callback.name !== "Upgrade Scales" && callback.name === requestName))){
            dispatch(notify('Already performing a task please wait!', 'warning'))
            return false
        }


        let payload : UpsertNotificationAction | null = null
        if(handleOne == null && reapop != null){
            if(reapop.requestMsg === "default"){
                payload = dispatch(notify("Sending " + requestName + " command...", 'loading', {dismissAfter: 0, dismissible: false,}))
            }
            else if(!reapop.requestMsg){
                payload = null
            }
            else{
                payload = dispatch(notify(reapop.requestMsg, 'loading', {dismissAfter: 0, dismissible: false,}))
            }
        }

        const newCallback : ResponseCallback = {
            id: requestId,
            name : requestName,
            notification : payload,
            onSuccess : onSuccess,
            onFail : onFail,
            reapop : reapop,
            handleOne : handleOne
        }

        setCallbacks({...state, 
            [requestId]: newCallback,
        })
        if(!activeRequests || activeRequests < 0){dispatch(setActiveRequests(1))}
        else{dispatch(setActiveRequests(activeRequests + 1))}
        return true
    };

    const contextValue = {
        callbacks: callbacks,
        addCallback: useCallback((state : {[id: number] : ResponseCallback}, requestId : number, requestName : string = "", 
        onSuccess : (response : ResponseType) => void, onFail : (response : ResponseType) => void, reapop ?: ReapopMsg, handleOne?:boolean) => 
            addCallback(state, requestId, requestName, onSuccess, onFail, reapop, handleOne), []),
        removeCallback: useCallback((newState : {[id: number] : ResponseCallback}) => 
            removeCallback(newState), [])
    };
    return (
        <ResponseContext.Provider value={contextValue}>
            {children}
        </ResponseContext.Provider>
    );
}
