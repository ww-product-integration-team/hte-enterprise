import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Typography } from "@material-ui/core";
import { Breadcrumb, Col, Row } from "react-bootstrap";
import { Link } from 'react-router-dom'
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { HeadCellTypes } from "../common/virtualTable/VirtualTable";
import VirtualTableProps from '../common/virtualTable/VirtualTable';

import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { ProductAPI } from "../api";
import { useAppSelector } from "../../state";

// This is being temporarily created to show live product info and allow for minimal 
// produc editing
export const prodTableHeader: HeadCellTypes[] = [
    { id: "editProd", sorted: false, searchable: false, label: "Options", isShowing: true, mandatory: true, },
    // { id: "prID", sorted: true, searchable: false, label: "False", isShowing: true, mandatory: true, },
    { id: "prNum", sorted: true, searchable: true, label:"PLU  Number", isShowing: true, mandatory: true, },
    { id: "deptNum", sorted: true, searchable: true, label: "Dept. Number", isShowing: true, mandatory: true, },
    { id: "zoneName", sorted: true, searchable: true, label: "Zone", isShowing: false, mandatory: false, },
    { id: "prType", sorted: true, searchable: false, label: "Type", isShowing: true, mandatory: true, },
    { id: "prDesc", sorted: false, searchable: true, label: "Description", isShowing: true, mandatory: true, },
    { id: "prPrice", sorted: true, searchable: false, label: "Price", isShowing: true, mandatory: true, },    
    { id: "prDscPr1", sorted: false, searchable: false, label: "Discount Price", isShowing: true, mandatory: true, },
    // { id: "prDscPr2", sorted: false, searchable: false, label: "Discount Price 2", isShowing: false, mandatory: false, },
    // { id: "prDscPr3", sorted: false, searchable: false, label: "Discount Price 3", isShowing: false, mandatory: false, },    
    { id: "lbNum1", sorted: false, searchable: false, label: "Label Type", isShowing: true, mandatory: true, },
    { id: "lbNum2", sorted: false, searchable: false, label: "Label Type (Second)", isShowing: false, mandatory: false, },
    { id: "lbNum3", sorted: false, searchable: false, label: "Label Type (Third)", isShowing: false, mandatory: false, },
    { id: "caNum", sorted: false, searchable: false, label: "Category Number", isShowing: true, mandatory: true, },
    { id: "prShlfLfD", sorted: false, searchable: false, label: "Product Shelf Life", isShowing: true, mandatory: true, },
    { id: "prProdLfD", sorted: false, searchable: false, label: "Product Life", isShowing: true, mandatory: true, },
];

export interface prod {
    prID: number,
    deptNum: number,
    clNum:number
    prNum:number
    txNum:number
    prodTxt5Num:number
    prodTxt6Num:number
    prodTxt7Num:number
    prodTxt8Num:number
    prodTxt9Num:number
    smnum:number
    nfnum:number
    ntNum:number
    lbNum1:number
    lbNum2:number
    lbNum3:number
    caNum:null
    spnum:null
    lcnum:null
    lpnum:null
    lxnum:null
    dlxnum:null
    llid:null
    maNum:null
    prPrice: number,
    prPrMod: number,
    prIsFrcPr: Boolean,
    prExcPr: number,
    prDscMe: number,
    prDscPr1: number,
    prDscPr2: number,
    prDscPr3: number,
    prTare: number,
    prTareB: number,
    prIsFrcTr: Boolean,
    prPrpTare: number,
    prNetWt: number,
    prByCnt: number,
    prIsFrcBC: Boolean
    prShlfLfD: number,
    prShlfLfH: number,
    prProdLfD: number,
    prProdLfH: number,
    prIsPrnSL: Boolean,
    prIsPrnPL: Boolean,
    prIsPrnPO: Boolean,
    prBnsPts: number,
    prPrtns: number,
    prIsUsSL: Boolean,
    prInMode: number,
    prLblRot1: number,
    prLblPlc1: number,
    prLblRot2: number,
    prLblPlc2: number,
    prLblRot3: number,
    prLblPlc3: number,
    prName1: string,
    prName2: string,
    prVCOLTxt: Boolean,
    prCOLReq: Boolean,
    prPrPlT: number,
    prType: number,
    prBarPref: number,
    prBarProN: number,
    prBarVenN: number,
    prEANCd: string
    prRawBarC: string
    prCdNm1: string,
    prCdNm2: string,
    prSpcSD: string,
    prDesc: string,
    prGrNm1: string,
    prGrNm2: string,
    prGrNm3: string,
    prGrNm4: string,
    prImage: string,
    prCOLTN: string,
    prMealSideItemsGroupNum: number,
    prMealSideItemsAreFree: Boolean,
    prMealNumSidesIncluded: number,
    prCutTestMeatSpecSheetGroupNum: number,
    prWeightMin: number,
    prWeightMax: number,
    prTotalPriceMin: number,
    prTotalPriceMax: number,
    prManualLabelTypeNum1: number,
    prManualLabelTypeNum2: number,
    prManualLabelTypeNum3: number,
    prPrepackLabelTypeNum1: number,
    prPrepackLabelTypeNum2: number,
    prPrepackLabelTypeNum3: number,
    prProdEntryLabelTypeNum1: number,
    prProdEntryLabelTypeNum2: number,
    prProdEntryLabelTypeNum3: number,
    prSelfServLabelTypeNum1: number,
    prSelfServLabelTypeNum2: number,
    prSelfServLabelTypeNum3: number,
    prPick5LabelTypeNum1: number,
    prPick5LabelTypeNum2: number,
    prPick5LabelTypeNum3: number,
    prOnlineOrderingLabelTypeNum1: number,
    prOnlineOrderingLabelTypeNum2: number,
    prOnlineOrderingLabelTypeNum3: number,
    prShlfLfM: number,
    prProdLfM: number,
    zoneId: string,
    zoneName: string,
}
export const prodType =[
    "Random Weight", "Fixed Weight", "By Count", "Fluid Ounces"
]
export function ProdsTable() {
    const prods = useAppSelector((state)=>state.products.products)
    const dispatch = useDispatch();
    useEffect(()=>{
        const newID = Date.now()
        dispatch(ProductAPI.getProds(newID))
    },[])
    const [selected, setSelected] = useState([""])
    return (
        <div className="UpgradeManager" style = {{maxHeight: 200}}>
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Products</Breadcrumb.Item>
            </Breadcrumb>
            <Row>
                <Col>
                    <h2>Product List</h2>
                    <h6>Use this page to look at your live product table and edit some variables.</h6>
                </Col>
            </Row>
            <VirtualTableProps
                tableName="productListTable"
                dataSet={[...prods]}
                maxHeight={"400"}
                renderRows={30}
                dispatchedUrls={[ProductAPI.getProds]}
                headCells={prodTableHeader}
                initialSortedBy={{ prdID: '' }}
                dataIdentifier="prID"
                saveKey="productListTable"
                selectedRows={selected}
            />
            <br/>
            <Typography>Total Number of Products Found: {prods.length}</Typography>
        </div>
    )
}