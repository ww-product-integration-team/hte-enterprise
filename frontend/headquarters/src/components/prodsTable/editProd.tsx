import { useState } from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import { Form } from '@themesberg/react-bootstrap';
import Alert from '@mui/material/Alert';
import { useDispatch } from "react-redux";
// BANNER, Region, Store, Department, Scale
import { Block, Save } from '@mui/icons-material';
import { useRspHandler } from '../utils/ResponseProvider';
import logger from '../utils/logger';
import { FormInput } from '../forms/FormHelper';
import { prod } from './prodsTable';
import { ProductAPI } from '../api';

interface EditProdProps {
    openPopup: boolean
    setCurrentPrID: React.Dispatch<React.SetStateAction<string>>,
    prod : prod
}

export interface prodEditableData {
    //pricing is string for masking purposes - Corey
    prPrice : string,
    prDscPr1 : string,
    prDscPr2 : string,
    prDscPr3 : string,
    lbNum1: number,
    prShlfLfD : number,
    prProdLfD : number,
}

function EditProd(props: EditProdProps) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<prodEditableData>();
    const { openPopup, setCurrentPrID, prod } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();

    let USDollar = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    });
    const defaultValues : prodEditableData = {
        prPrice : "0",
        prDscPr1 : "0",
        prDscPr2 : "0",
        prDscPr3 : "0",
        lbNum1: 0,
        prShlfLfD : 0,
        prProdLfD : 0,
    };
    const handleCloseDialog = (event?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if (event) { event.stopPropagation() }
        setCurrentPrID("");
        reset(defaultValues);
    }

    const handleEdit = handleSubmit(async (data) => {    // When the Submit button is pressed
        logger.info("(postAutoAssignerRule) POST request (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])
        let newProd = prod;
        let editVal = Object.keys(defaultValues)
        for(let prodFields of editVal){
            let value = data[prodFields]
            if(value){
                // if value can be a number make it into a number
                if(value.includes("$") || value.includes(",") || value.includes("."))
                    value = parseDollar(value)
                if(+value && !Number.isNaN(+value))
                    newProd[prodFields] = +value 
                else 
                    newProd[prodFields] = value
            }
        }
        const newID = Date.now()
        function onSuccess(response) {
            dispatch(ProductAPI.getProds(Date.now()))
            handleCloseDialog()
        }
        function onFail(response) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not edit product!"])
        }
        if (addCallback(callbacks, newID, "Edit Product", onSuccess, onFail)) {
            dispatch(ProductAPI.postProd(newID, prod))
        }
    })

    function parseDollar(value : any){
        
        if(value.includes("$")){
            value = value.split("$").join("")
        }
        if(value.includes(",")){
            value = value.split(",").join("")
        }
        if(value !== "null"){
            if(value.includes(".")){
                value = value.split(".").join("")
            }
            else {
                value = +value * 100
            }
        }
        return value
    }
    function checkValue(value : any){
        if (value) {
            if(value.includes("$")){
                value = value.split("$").join("")
            }
            if(value.includes(",")){
                value = value.split(",").join("")
            }
            if(value !== "null"){
                if(value.includes(".")){
                    value = value.split(".").join("")
                }
                else {
                    value = +Number.parseInt(value) * 100
                }
            }
            if (typeof value === "string") {
                return Number.parseInt(value) > 0
            } else {
                return value > 0
            }
        } else {
            return false
        }
    }

    return (
        <Dialog
            open={openPopup}
            onClose={setCurrentPrID}
            maxWidth="lg"
            fullWidth={true}
        >
            <DialogTitle style = {{ marginBottom:"0vh"}} >
                <h4 style = {{marginBlock:"1vh"}}>Edit Product: #{prod.prNum} {prod.prDesc}</h4>
                <h4 style = {{marginTop:"1vh", marginBottom:"0vh"}}> Department: #{prod.deptNum != null && prod.deptNum !== undefined ? prod.deptNum : "null"}</h4>
            </DialogTitle>
                <DialogContent style = {{margin:"0vh"}}>
                        <Form style={{overflowY: "auto", maxHeight : "60vh"}} className="form-control" onSubmit={handleEdit}>
                            <FormInput
                                label="Product Price"
                                name = "prPrice"
                                defaultValue = {prod.prPrice ? USDollar.format(prod.prPrice/100) :"null"}
                                type="text"
                                register={register}
                                validation = {{
                                    validate: value => checkValue(value) || 'Cannot be zero!',
                                    pattern : {value :/(?:(^\${1}\d{1,5}\.{1}\d{2}$)|(^\${1}\d{1,5}$))/,  message:"Invalid Dollar Amount (Include a $ before the amount)"},
                                }}
                                error={errors.prPrice}
                            />    
                            <br />
                            <FormInput
                                label="Product Discount Price"
                                id = "prDscPr1"
                                defaultValue = {prod.prDscPr1 ? USDollar.format(prod.prDscPr1/100) : "null"}
                                name = "prDscPr1"
                                type="text"
                                register={register}
                                validation = {{
                                    validate: value => checkValue(value) || 'Cannot be zero!',
                                    pattern : {value :/(?:(^\$?\d{1,5}\.{1}\d{2}$)|(^\$?\d{1,5}$)|(^[n,N][u,U][l,L]{2}$))/,  message:"Invalid Dollar Amount"}
                                }}
                                error={errors.prDscPr1}
                            />    
                            <br />
                            {/* <FormInput
                                label="Product Discount Price 2"
                                name = "prDscPr2"
                                defaultValue = {prod.prDscPr2 ? USDollar.format(prod.prDscPr2/100) : "null"}
                                type="text"
                                register={register}
                                validation = {{
                                    validate: value => checkValue(value) || 'Cannot be zero!',
                                    pattern : {value :/(?:(^\$?\d{1,5}\.{1}\d{2}$)|(^\$?\d{1,5}$)|(^[n,N][u,U][l,L]{2}$))/, message:"Invalid Dollar Amount"}
                                }}
                                error={errors.prDscPr2}
                            />    
                            <br />
                            <FormInput
                                label="Product Discount Price 3"
                                name = "prDscPr3"
                                defaultValue = {prod.prDscPr3 ? USDollar.format(prod.prDscPr3/100) : "null"}
                                type="text"
                                register={register}
                                validation = {{
                                    validate: value => checkValue(value) || 'Cannot be zero!',
                                    pattern : {value :/(?:(^\$?\d{1,5}\.{1}\d{2}$)|(^\$?\d{1,5}$)|(^[n,N][u,U][l,L]{2}$))/,  message:"Invalid Dollar Amount"}
                                }}
                                error={errors.prDscPr3}
                            />    
                            <br /> */}
                            <FormInput
                                label="Label Type [1-9999]"
                                id = "lbNum1"
                                name = "lbNum1"
                                defaultValue = {prod.lbNum1 ? prod.lbNum1 : "null"}
                                type="text"
                                register={register}
                                validation = {{
                                    min : {value : 1, message: "Value is too small."},
                                    max : {value : 9999, message: "Value is too large."},
                                    validate : value => +value && !Number.isNaN(+value) || value === "null"
                                }}
                                error={errors.lbNum1}
                            />    
                            <br />
                            <FormInput
                                label="Product Shelf Life (Days)"
                                id = "prShlfLfD"
                                name = "prShlfLfD"
                                defaultValue = {prod.prShlfLfD ? prod.prShlfLfD  : "null"}
                                type="text"
                                register={register}
                                validation = {{
                                    min : {value : 1, message: "Value is too small."},
                                    max : {value : 999, message: "Value is too large."},
                                    validate : value => +value && !Number.isNaN(+value) || value === "null"
                                }}
                                error={errors.prShlfLfD}
                            />    
                            <br />
                            <FormInput
                                label="Product Life (Days)"
                                id = "prProdLfD"
                                name = "prProdLfD"
                                defaultValue = {prod.prProdLfD ? prod.prProdLfD : "null"}
                                type="text"
                                register={register}
                                validation = {{
                                    min : {value : 1, message: "Value is too small."},
                                    max : {value : 999, message: "Value is too large."},
                                    validate : value => +value && !Number.isNaN(+value) || value === "null"
                                }}
                                error={errors.prProdLfD}
                            />    
                            <br />
                            {shouldShowAlert[0] ? <><br /><Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> </> : null}
                        </Form>
                    <div className="formButtons">
                        <button className="formButton1" onClick={handleEdit} type="submit">
                            <Save />
                            <span> Save Product </span>
                        </button>
                        <button className="formButton1" onClick={handleCloseDialog} type="button">
                            <Block />
                            <span> Cancel </span>
                        </button>
                    </div>
                </DialogContent>
        </Dialog>
    )
}
export { EditProd }