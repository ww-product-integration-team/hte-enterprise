import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import {Breadcrumb} from '@themesberg/react-bootstrap';
import { DEFAULT_ADMIN_DOMAIN_ID, SYSTEM_MANAGER } from "../../state/actions/accessActions"
import DeleteConfig from './DeleteConfig';
import ScaleStatusConfig from './ScaleStatusConfig';
import { useAppSelector } from '../../state';
import AccessControl from '../common/AccessPermissions';
import StoreConfigForm from './StoreConfigForm';
import PricingZoneConfig from './PricingZoneConfig';

export default function AppSettings() {

// ==================   PERMISSIONS CHECKING   ==================
// Everything to do with permissions / roles

    const user = useAppSelector((state) => state.access.account);
    const appRoles = useAppSelector((state) => state.access.roles);
    const domains = useAppSelector((state)=>state.access.domains)
    return (
        <>
        <div className="mb-4 mb-md-0">
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Settings</Breadcrumb.Item>
            </Breadcrumb>

            <h2>Settings</h2>
            <p className="mb-4">Various configuration settings to be set and changed by the user</p>
        </div>


        <div>

            
                <AccessControl
                    user = {user}
                    requiredDomain = {domains[DEFAULT_ADMIN_DOMAIN_ID]}
                    pageRequest={true}
                >
                    <DeleteConfig
                        operation="APP" 
                        configUpdated={undefined}                    
                    />

                    <ScaleStatusConfig/>

                    <StoreConfigForm/>

                    <PricingZoneConfig/>

                    {/* Hiding for now, not sure how we want to handle threshold alerts NOT FINISHED*/}
                    {/* <ErrorStatusConfig/>  */}
                </AccessControl>
        </div>

        </>   
    );
}
