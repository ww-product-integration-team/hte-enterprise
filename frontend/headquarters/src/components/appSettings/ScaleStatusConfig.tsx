import { useState, useEffect } from 'react';
import { Button } from '@themesberg/react-bootstrap';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import SliderTemplate from '../common/Slider';
import { Form } from 'react-bootstrap';
import { Alert } from '@mui/material';
import { ConfigAPI } from '../api';
import { useDispatch} from 'react-redux';
import { useRspHandler } from '../utils/ResponseProvider';
import logger from '../utils/logger';
import { useAppSelector } from '../../state';
import { ResponseType } from '../../types/storeTypes';
import { AlertMessage, HideMessage, ShowErrorMessage, ShowInfoMessage, ShowSuccessMessage } from '../../types/utils/utilTypes';



export default function ScaleStatusConfig(){

    const sliderMin = 2;
    const sliderMax = 96;
    const baseMessage = "Configure how long (in hours) to wait after the last heartbeat before considering a scale ";
    const awayAlert = "A scale will be considered away if it does not heartbeat in ";
    const offlineAlert = "\n and offline if it does not heartbeat in ";

    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const currentConfig = useAppSelector((state) => state.config.offlineAwayHours);

    const [statusAlert, setStatusAlert] = useState<AlertMessage>(HideMessage())

    const [awayThreshold, setAwayThreshold] = useState<number | null | undefined>(currentConfig.scaleAwayHours);
    const [offlineThreshold, setOfflineThreshold] = useState<number | null | undefined>(currentConfig.scaleOfflineHours);
    const [offlineMin, setOfflineMin] = useState<number>(sliderMin);

    function sendConfig(){

        function onSuccess(response : ResponseType){
            logger.info("Set Offline Info ", response);
            dispatch(ConfigAPI.getOfflineHours())
            setStatusAlert(ShowSuccessMessage("Configuration successfully set!"))
        }
        function onFail(response : ResponseType){
            logger.error("Update Away/Offline Request failed! ", response);
            setStatusAlert(ShowErrorMessage("Could not set configuration! " + response.response.errorDescription))
        }

        const reqId = Date.now()
        if(awayThreshold && offlineThreshold){
            if(addCallback(callbacks, reqId, "Set Offline Hours", onSuccess, onFail)){
                dispatch(ConfigAPI.setOfflineHours(reqId, awayThreshold, offlineThreshold))
            }
        }
        else{
            setStatusAlert(ShowErrorMessage("Invalid Away/Offline values! Cannot sent configuration"))
        }
        
        
    }


    function updateAwayThreshold(threshold : number){
        setAwayThreshold(Math.round(threshold));
        setOfflineMin(Math.round(threshold) + 1);
    }

    useEffect(() => {
        dispatch(ConfigAPI.getOfflineHours(Date.now()))
    },[])

    useEffect(() => {
        if(currentConfig){
            setAwayThreshold(currentConfig.scaleAwayHours)
            setOfflineThreshold(currentConfig.scaleOfflineHours)
        }
    },[currentConfig])


    useEffect(() => {
        if(!awayThreshold || !offlineThreshold){
            setStatusAlert(ShowErrorMessage("Could not fetch hour current hour configuration! If issue persists please contact HT support"))
        }
        else if(awayThreshold < sliderMin){
            setStatusAlert(ShowErrorMessage("Please enter an away threshold greater than " + sliderMin + " hours"))
        }else if(awayThreshold > sliderMax){
            setStatusAlert(ShowErrorMessage("Please enter an away threshold less than " + sliderMax + " hours"))
        }else if (offlineThreshold < sliderMin){
            setStatusAlert(ShowErrorMessage("Please enter an offline threshold less than " + sliderMin + " hours"))
        }else if (offlineThreshold > sliderMax){
            setStatusAlert(ShowErrorMessage("Please enter an offline threshold greater than " + sliderMax + " hours"))
        }else if(offlineThreshold <= awayThreshold) {
            setStatusAlert(ShowErrorMessage("Please enter an offline threshold that is greater than the away threshold of " + awayThreshold + " hours"))
        }
        else{
            setStatusAlert(ShowInfoMessage(awayAlert + awayThreshold + " hours" + offlineAlert + offlineThreshold + " hours"))
        }
    },[awayThreshold,offlineThreshold])


    return(
        <div className="form-control" style={{maxWidth:"50rem", marginTop: ".5rem"}}>

            <Form.Label>Away/Offline Policy:</Form.Label>

            <SliderTemplate 
                title = "Away Threshold:" 
                units = {"hours"} 
                min = {sliderMin} 
                max = {sliderMax} 
                message = {baseMessage + "away"}
                value = {awayThreshold ?? sliderMax}
                onValueChange = {threshold => updateAwayThreshold(threshold)}
                errorStatus = {status => setStatusAlert(ShowErrorMessage("Please enter a valid number between " + sliderMin + " and " + sliderMax + " " + "hours"))}

            />

            <SliderTemplate 
                title = "Offline Threshold:" 
                units = {"hours"} 
                min = {sliderMin} 
                max = {sliderMax} 
                message = {baseMessage + "offline"} 
                value = {offlineThreshold ?? offlineMin}
                errorStatus = {status => setStatusAlert(ShowErrorMessage("Please enter a valid number between " + sliderMin + " and " + sliderMax + " " + "hours"))}
                onValueChange = {(threshold) => setOfflineThreshold(threshold)}
            />

            {statusAlert.show ? <Alert id="statusAlert" className="m-1" variant="filled" severity={statusAlert.severity}>{statusAlert.message}</Alert> : null}


            <div style={{display:"flex", alignItems: "right", justifyContent:"right"}}>
                <Button id="actionBar1" variant="outline-primary" size="sm" disabled={statusAlert.severity === "error"} onClick={(e) => {
                    sendConfig();
                }}> 
                    <AddCircleOutlineIcon style={{marginRight: '8px'}}/>
                    Update Preferences
                </Button>
            </div>


        </div>
    )

    

}
