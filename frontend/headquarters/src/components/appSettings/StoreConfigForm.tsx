import { Button, Form, FormCheck } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import {Alert, Checkbox, FormControlLabel, IconButton, TextField, Tooltip, Typography } from '@mui/material';
import { AddCircleOutline, ArrowDropDown, Help } from '@mui/icons-material';
import { ConfigActions, useAppSelector } from '../../state';
import { Store } from '../../types/asset/AssetTypes';
import { ChangeEvent, useEffect, useState } from 'react';
import { ConfigAPI } from '../api';
import { useForm } from 'react-hook-form';
import { StoreConfiguration } from '../../types/config/config';
import { AlertMessage, HideMessage, ShowErrorMessage, ShowSuccessMessage, ShowWarningMessage } from '../../types/utils/utilTypes';


interface StoreConfigFormProps {

}

export default function StoreConfigForm(props : StoreConfigFormProps) {

    interface StoreConfigForm{
        sendStoreInfo: boolean
    }

    const { register, handleSubmit, reset, formState: {errors} } = useForm<StoreConfigForm>();

    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const stores = useAppSelector((state) => state.asset.stores);
    const storeConfig = useAppSelector((state) => state.config.storeConfig)

    const [newStoreConfig, setNewStoreConfig] = useState<StoreConfiguration>({})
    const [customStores, setCustomStores] = useState<Store[]>([])
    const [statusAlert, setStatusAlert] = useState<AlertMessage>(HideMessage())
    const reduxSendConfig = useAppSelector(state=>state.config.storeConfig.sendStoreInfo)
    const [sendStoreInformation, setSendStoreInformation] = useState(reduxSendConfig === "true")

    useEffect(()=>{
        dispatch(ConfigAPI.getConfiguration(Date.now(), ["sendStoreInfo", "customStoreName"], ConfigActions.setStoreConfig))
    },[])

    useEffect(() =>{
        if(Object.keys(storeConfig).length === 0 || !storeConfig.sendStoreInfo){
            setStatusAlert(ShowErrorMessage("Could not find Store configuration information"))
        }
        else{
            if(statusAlert.show && statusAlert.message === "Could not find Store configuration information"){
                setStatusAlert(HideMessage())
            }
            setNewStoreConfig(storeConfig)
        }
    },[storeConfig])

    useEffect(() => {
        let tmpStores = Object.values(stores).filter((store) => store.sendStoreInfo && store.sendStoreInfo !== "false")
        setCustomStores(tmpStores)
        if(tmpStores.length > 0){
            setStatusAlert(ShowWarningMessage("There are " + tmpStores.length + " store(s) with a custom configuration and will not listen to this setting"))
        }
    }, [stores])
    

    const handleConfigChange = (event) => {
    
        function onSuccess(rsp: any){
            dispatch(ConfigAPI.getConfiguration(Date.now(), ["sendStoreInfo", "customStoreName"], ConfigActions.setStoreConfig))
        }

        function onFail(rsp: any){
            dispatch(ConfigAPI.getConfiguration(Date.now(), ["sendStoreInfo", "customStoreName"], ConfigActions.setStoreConfig))
        }
        let newId = Date.now()
        
        setSendStoreInformation(event.target.checked)
        let newStoreState : StoreConfiguration = {
            sendStoreInfo: event.target.checked,
        }

        if(addCallback(callbacks, newId, "Set Store Config", onSuccess, onFail)){
            dispatch(ConfigAPI.setConfiguration(newId, newStoreState))
        }
    }

    return (
        <div style={{ maxWidth: "50rem", marginTop: ".5rem" }}>
            <div className="form-control">
                <Form.Label >Store Configuration:</Form.Label>
                <Tooltip className="info" title={<Typography fontSize={12}>Determines whether HTe sends store information to scales</Typography>}>
                    <IconButton aria-label="enableAccount" disableRipple>
                        <Help />
                    </IconButton>
                </Tooltip>

                <div style={{ display: "flex", paddingBottom: "1rem", flexWrap: "wrap" }}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={sendStoreInformation}
                                onClick={e => handleConfigChange(e)}
                                disableRipple
                                sx={{ marginLeft: "0.5rem", cursor: "default" }}
                            />
                        }
                        label="Send Store Information"
                        className="customCheckbox"
                        sx={{ cursor: "default" }}
                    />
                </div>
            </div>
        </div>
    )
}

