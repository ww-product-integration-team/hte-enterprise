import { Button, Form } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { Dialog, DialogContent, DialogTitle, IconButton, Tooltip, Typography } from '@mui/material';
import { AddCircleOutline, Help } from '@mui/icons-material';
import { useEffect, useRef, useState } from 'react';
import { ScaleAPI, RuleAPI, AssetAPI } from '../api';
import { useForm } from 'react-hook-form';
import { FormInput } from '../forms/FormHelper';
import { v4 as uuidv4 } from 'uuid';
import { ResponseType } from '../../types/storeTypes';
import VirtualTable, { HeadCellTypes } from '../common/virtualTable/VirtualTable';
import { useAppSelector, AssetActions } from '../../state';
import { PostRule } from './CreateRuleForAutoAssigner';
import { format } from "date-fns";

export interface AutoAssignRule {
    ipAddressStart: string
    ipAddressEnd: string
    banner: string
    region: string
    store: string
    dept: string
    enabled: boolean,
    selected: boolean,
    disableFlag?: boolean,
    deleteFlag?: boolean
}

export default function CsvUploadForAssigner() { // Setting up a special environment just for CSV-related work
    interface CsvFileUploadAssignment {
        file: any
    }

    const rulesHeaders: HeadCellTypes[] = [
        { id: "ipAddressStart", sorted: false, searchable: false, label: "IP Address Start", isShowing: true, mandatory: true },
        { id: "ipAddressEnd", sorted: false, searchable: false, label: "IP Address End", isShowing: true, mandatory: true },
        { id: "banner", sorted: false, searchable: false, label: "Banner", isShowing: true, mandatory: true },
        { id: "region", sorted: false, searchable: false, label: "Region", isShowing: true, mandatory: true },
        { id: "store", sorted: false, searchable: false, label: "Store", isShowing: true, mandatory: true },
        { id: "dept", sorted: false, searchable: false, label: "Department", isShowing: true, mandatory: true },
        { id: "enabled", sorted: false, searchable: false, label: "Status", isShowing: true, mandatory: true },
        { id: "editRule", sorted: false, searchable: false, label: "Edit Rule", isShowing: true, mandatory: true }
    ]

    const { register, handleSubmit, reset, formState: { errors } } = useForm<CsvFileUploadAssignment>() // Custom form
    const dispatch = useDispatch()
    const { callbacks, addCallback } = useRspHandler()

    useEffect(() => {
        dispatch(RuleAPI.getAutoAssignRules(Date.now()))
    }, [])


    const autoAssignRules = useAppSelector(state => state.asset.autoAssignRules)
    const [rules, setRules] = useState([...autoAssignRules])
    useEffect(() => {
        setRules([...autoAssignRules])
    }, [autoAssignRules])

    const [isDisabled, setDisabled] = useState(false) // Used to monitor state of the CSV submission directly below
    const [openDialog, setOpenDialog] = useState(false) // used to open PostRule Dialog

    const reduxCriticalDialog = useAppSelector((state) => state.asset.criticalDialog)
    const [criticalDialogAssigner, setCriticalDialogAssigner] = useState<string>(reduxCriticalDialog)

    useEffect(() => {
        setCriticalDialogAssigner(reduxCriticalDialog)
    }, [reduxCriticalDialog])

    useEffect(() => {
        if (criticalDialogAssigner) {
            window.onbeforeunload = () => true
            return () => {
                window.onbeforeunload = null;
            }
        }
    }, [criticalDialogAssigner]);

    const postCsv = handleSubmit(async (data: CsvFileUploadAssignment) => {

        function onSuccess(response: ResponseType) {
            logger.info("File saved successfully")
            dispatch(RuleAPI.getAutoAssignRules(Date.now()))
            dispatch(AssetActions.setCriticalDialog(""))
        }

        function onFail(response: ResponseType) {
            logger.info("Could not save file")
            dispatch(AssetActions.setCriticalDialog(""))
        }

        const newID = Date.now()
        const fileData = new FormData()

        fileData.append('file', data.file[0])

        if (addCallback(callbacks, newID, "Save File", onSuccess, onFail, { requestMsg: "Sending CSV file...", successMsg: "Successfully sent CSV file!", failMsg: "default" })) {
            dispatch(AssetAPI.importCSVToAutoAssigner(newID, fileData))
            dispatch(AssetActions.setCriticalDialog("Rules"))
        }
    })

    /*
     * Artifacts of an older implementation of assignment rules. 
     * Enabling all rules at once with the newer (post-July 2024) auto-assignment code will cause a myriad of issues.
     * Disabling all rules isn't as harmful, but it's not a bell a user can easily unring either.
     * These should probably remain unused, but I'll let them remain here in case someone wants them back.
     * 
    const disableRules = () => {
        function onSuccess(response: ResponseType) {
            logger.info("Assignment rules disabled.")
            dispatch(RuleAPI.getAutoAssignRules(Date.now()))
        }
        function onFail(response: ResponseType) {
            logger.info("Error: Assignment rules not disabled.")
        }
        const newID = Date.now()
        if (addCallback(callbacks, newID, "Disable Rules", onSuccess, onFail, { requestMsg: "Disabling rules... ", successMsg: "Rules disabled!", failMsg: "default" })) {
            dispatch(RuleAPI.disableRules(newID))
        }
    }

    const enableRules = () => {

        function onSuccess(response: ResponseType) {
            logger.info("Assignment rules enabled.")
            dispatch(RuleAPI.getAutoAssignRules(Date.now()))
        }
        function onFail(response: ResponseType) {
            logger.info("Error: Assignment rules not enabled.")
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Enable Rules", onSuccess, onFail, { requestMsg: "Enabling rules...", successMsg: "Rules enabled!", failMsg: "default" })) {
            dispatch(RuleAPI.enableRules(newID))
        }
    }
    */

    const downloadRules = () => {

        function onSuccess(response: ResponseType) {
            logger.info("Auto-assignment rules downloaded.")
            setCsvDownload(response.response)
            const table: string[][] = response.response
            let csvContent = "data:text/csv;charse=utf-8,"
            //headers
            csvContent += "IP Address Start,"
            csvContent += "IP Address End,"
            csvContent += "Banner,"
            csvContent += "Region,"
            csvContent += "Store,"
            csvContent += "Department"
            csvContent += "\r"

            for (const rule of table) {
                for (const item of rule)
                    csvContent += item + ","
                csvContent += "\r"
            }

            //Create a link to download the file
            const encodedUri = encodeURI(csvContent);
            const link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", "AutoAssignerRules_" + format(Date.now(), "MMddyyy") + ".csv");
            document.body.appendChild(link); // Required for FF
            link.click();
            setDisabled(false)
        }
        function onFail(response: ResponseType) {
            setDisabled(false)
            logger.info("Error: Auto-assignment rules failed to download.")
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Download Auto-Assignment Rules", onSuccess, onFail, { requestMsg: "Downloading rules...", successMsg: "Rules downloaded!", failMsg: "default" })) {
            setDisabled(true)
            dispatch(RuleAPI.downloadRules(newID))
        }
    }

    const [csvDownload, setCsvDownload] = useState('')
    const [openCurrentlyImporting, setOpenCurrentlyImporting] = useState(false)

    const handleCloseCritical = (event, reason) => {
        if (reason && reason === "backdropClick")
            return;
        setOpenCurrentlyImporting(false);
    }

    const criticalDialog = <>
        <Dialog open={criticalDialogAssigner ? true : false} onClose={handleCloseCritical} fullWidth={true} maxWidth="sm">
            <DialogTitle>
                <div style={{ display: "flex", justifyContent: "left" }}>
                    <div style={{ marginTop: ".4rem", marginRight: "1rem" }} className='spinnerAsset' />
                    <Typography variant="h6" component="div" >Importing rules...</Typography>
                </div>
            </DialogTitle>

            <DialogContent>
                <Typography variant="body1" component="div" >Importing data. Please wait; <b>do not</b> refresh your browser... </Typography>
            </DialogContent>
        </Dialog>
    </>

    return (
        <div style={{ maxWidth: "50rem", marginTop: ".5rem" }}>
            <Form className='mt-4'>
                <div className="form-control">
                    <Form.Label>Upload Scale Assignment Rules:</Form.Label>
                    <Tooltip className="info" title={<Typography fontSize={12}>Loads rules for automatic scale assignment into the database via CSV file.
                        Please read the instructions below before using this feature.</Typography>}>
                        <IconButton style={{ marginBottom: "5px" }} aria-label="enableAccount" disableRipple>
                            <Help />
                        </IconButton>
                    </Tooltip>

                    <FormInput
                        label=""
                        id="file"
                        name="file"
                        type="file"
                        accept=".csv"
                        enctype="multipart/form-data"
                        method="post"
                        action="#"
                        register={register}
                        validation={{ required: "Please select a file to upload" }}
                        error={errors.file}
                    />
                    <span className="fw-bold">Instructions for Use:</span>
                    <ul>
                        <span className="fw-bold">Rule Generation:</span>
                        <li key={uuidv4()}>Rules must have different IP ranges from all other rules</li>
                        <li key={uuidv4()}>Departments must match an identically named profile</li>
                        <br />
                        <span className="fw-bold">Upload by CSV:</span>
                        <div />
                        <b>NOTE: This will overwrite/remove any existing rules.</b>
                        <div />
                        <span>Ensure that your CSV file has headers, and for each row in the file:</span>
                        <div style={{ "marginLeft": "2rem" }}>
                            <li>Enter the first *full* IPv4 address of the range in the first column</li>
                            <li>Enter the last *full* IPv4 address of the range in the second column</li>
                            <li>Enter your banner in the third column</li>
                            <li>Enter your region in the fourth column</li>
                            <li>Enter your store in the fifth column</li>
                            <li>Enter your department in the sixth column</li>
                            <li><i>Optional: </i>Enter your store address in the seventh column</li>
                            <li><i>Optional: </i>Enter your store ID in the eighth column</li>
                        </div>
                    </ul>

                    <div style={{ display: "flex", alignItems: "right", justifyContent: "right" }}>
                        <Button style={{ marginRight: '.5rem' }} id="actionBar1" variant="outline-primary" size="sm" type="submit" onClick={postCsv}>
                            <AddCircleOutline style={{ marginRight: '8px' }} />
                            Upload File
                        </Button>
                        <Button id="actionBar1" variant="outline-primary" size="sm" onClick={() => { setOpenDialog(true) }}>
                            <AddCircleOutline style={{ marginRight: '8px' }} />
                            Create Rule
                        </Button>
                    </div>
                    {/* use either the br element or a line separator (hr) */}
                    <label>Assignment Rules</label>
                    <VirtualTable
                        tableName="autoAssignRulesTable"
                        useToolbar={false}
                        dataSet={autoAssignRules}
                        headCells={rulesHeaders}
                        initialSortedBy={{ name: '' }}
                        saveKey='autoAssignRulesTable'
                    />
                    <br />
                    <div style={{ display: "flex", alignItems: "right", justifyContent: "right" }}>
                        <Button id="actionBar1" variant="outline-primary" size="sm" onClick={downloadRules} style={{ marginRight: '.5rem' }}>
                            Download Rules
                        </Button>

                        {/* Disabled for reasons documented above.

                        <Button id="actionBar1" variant="outline-primary" size="sm" onClick={disableRules} style={{ marginRight: '.5rem' }}>
                            Disable All Rules
                        </Button>
                        <Button id="actionBar1" variant="outline-primary" size="sm" onClick={enableRules}>
                            Enable All Rules
                        </Button>
                        */} 

                    </div>
                </div>
            </Form>
            <PostRule
                openPopup={openDialog}
                setOpenPopup={setOpenDialog}
            />

            {criticalDialogAssigner && criticalDialogAssigner.includes("Rules") ? criticalDialog : null}
        </div>
    )
}