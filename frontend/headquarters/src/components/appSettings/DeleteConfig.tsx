import { useState, useEffect } from 'react';
import { Button} from '@themesberg/react-bootstrap';
import { Form, FormCheck } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import { ConfigAPI} from '../api';
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { Alert, IconButton, Tooltip, Typography } from '@mui/material';
import { Help } from '@mui/icons-material';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { useAppSelector } from '../../state';
import { DeleteEventConfig, DeleteIntervals, DeleteTypes } from '../../types/config/config';
import { ResponseType } from '../../types/storeTypes';
import { AlertMessage, HideMessage, ShowErrorMessage, ShowSuccessMessage } from '../../types/utils/utilTypes';

interface DeleteConfigProps{
    operation: "APP" | "PROFILE"
    configUpdated: ((ret: string) => void) | undefined
}

export default function DeleteConfig(props : DeleteConfigProps){
    const {operation, configUpdated} = props
    const dayOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    const types : DeleteTypes[] = [DeleteTypes.flashkey, DeleteTypes.plu, DeleteTypes.config, DeleteTypes.operator, DeleteTypes.label]
    const intervalTypes : DeleteIntervals[] = [DeleteIntervals.Never, DeleteIntervals.Daily, DeleteIntervals.Weekly, DeleteIntervals.Monthly]
    const [skipReload, setSkipReload] = useState(0);

    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const deleteEvent = useAppSelector((state) => state.config.deleteEvent);

    const [interval, setInterval] = useState(0)
    const [day, setDay] = useState(1)
    const [time, setTime] = useState(1)
    const [amPm, setAmPm] = useState("am")
    const [deleteTypes, setDeleteTypes] = useState({
        "flashkey" : false,
        "plu" : false,
        "config" : false,
        "operator" : false,
        "label" : false,
    })
    const [statusAlert, setStatusAlert] = useState<AlertMessage>(HideMessage())

    useEffect(() => {
        const newID = Date.now()
        dispatch(ConfigAPI.getDeleteConfig(newID))
    },[])

    useEffect(() =>{
        if(!deleteEvent){
            return
        }
        setInterval(intervalTypes.indexOf(deleteEvent.interval) ?? "")
        if(deleteEvent.time && deleteEvent.time > 12){
            setTime(deleteEvent.time-12 ?? 0)
            setAmPm("PM")
        }
        else{
            setTime(deleteEvent.time)
            setAmPm("AM")
        }

        setDay(deleteEvent.day ?? 1);

        let delTypes = {
            flashkey : false,
            plu : false,
            config : false,
            operator : false,
            label : false
        };
        
        (document.getElementById("flashkeychkbx") as HTMLInputElement).checked = false;
        (document.getElementById("pluchkbx") as HTMLInputElement).checked = false;
        (document.getElementById("configchkbx") as HTMLInputElement).checked = false;
        (document.getElementById("operatorchkbx") as HTMLInputElement).checked = false;
        (document.getElementById("labelchkbx") as HTMLInputElement).checked = false;

        if(Array.isArray(deleteEvent.deleteTypes)){
           for (let i = 0; i < deleteEvent.deleteTypes.length; i++) {
                switch(deleteEvent.deleteTypes[i]){
                    case "flashkey":
                        delTypes.flashkey = true;
                        (document.getElementById("flashkeychkbx") as HTMLInputElement).checked = true
                        break;
                    case "plu":
                        delTypes.plu = true;
                        (document.getElementById("pluchkbx") as HTMLInputElement).checked = true
                        break;
                    case "config" :
                        delTypes.config = true;
                        (document.getElementById("configchkbx") as HTMLInputElement).checked = true
                        break;
                    case "operator" : 
                        delTypes.operator = true;
                        (document.getElementById("operatorchkbx") as HTMLInputElement).checked = true
                        break;
                    case "label" : 
                        delTypes.label = true;
                        (document.getElementById("labelchkbx") as HTMLInputElement).checked = true
                        break;
                }
            } 
        }
        setDeleteTypes(delTypes)
    },[deleteEvent])


    const disableKey = (ev : React.KeyboardEvent<HTMLSelectElement>) => {
     
        if (ev) {
            if   (ev.preventDefault)     ev.preventDefault();
        } 
    }

    const handleIntervalSelect = (e : React.FormEvent<HTMLSelectElement>) => {
        setInterval(e.currentTarget.options.selectedIndex-1)
    }

    const handleTimeSelect = (e : React.FormEvent<HTMLSelectElement>) => {
        setTime(e.currentTarget.options.selectedIndex)
    }

    const handleAmPmSelect = (e : React.FormEvent<HTMLSelectElement>) => {
        setAmPm(["AM", "PM"][e.currentTarget.options.selectedIndex])
    }

    const handleDaySelect = (e : React.FormEvent<HTMLSelectElement>) => {
        setDay(e.currentTarget.options.selectedIndex-1)
    }

    const handleCheckbox = (e : boolean, type : string) => {
        setDeleteTypes({...deleteTypes, [type] : e})
    }

    useEffect(()=>{
        if(skipReload > 0){
            setSkipReload(skipReload-1)
            return
        }
        setStatusAlert(parseNewConfig())
        if(operation === "PROFILE"){
            sendConfig()
        }
    }, [interval, time, amPm, day, deleteTypes])

    function sendConfig(){
        let dayToSend = 0; 
        if(!day || day === 1){
            logger.info("day is null setting to default 1")
            dayToSend = 1
        }
        else{dayToSend = day}

        let totalTime = time
        if(!totalTime || totalTime === 1){
            totalTime = 1
        }
        if(totalTime == 12){
            totalTime = 0
        }
        if(amPm ==="PM"){
            totalTime += 12
        }
        const names : DeleteTypes[] = []
        Object.keys(deleteTypes).forEach((key) => {
                if(deleteTypes[key]){
                    names.push(key as DeleteTypes)
                }
        })

        function onSuccess(response : ResponseType){
            logger.info("Retrieved Config Info ", response);
            setSkipReload(2)
            dispatch(ConfigAPI.getDeleteConfig(Date.now()))
            setStatusAlert(ShowSuccessMessage("Configuration successfully set!"))
            
        }
        function onFail(response : ResponseType){
            logger.info("Could not retrieve Config info", response);
            setStatusAlert(ShowErrorMessage("Could not set configuration!"))
        }

        if(operation == "APP"){
            const newID = Date.now()

            let newConfig : DeleteEventConfig = {
                interval : intervalTypes[interval],
                day : dayToSend,
                time : totalTime,
                deleteTypes : names
            }

            if(addCallback(callbacks, newID, "Get Delete Config", onSuccess, onFail)){
                dispatch(ConfigAPI.setDeleteConfig(newID, newConfig))
            }
        }
        else if(operation === "PROFILE"){
            let ret = intervalTypes[interval] + " " + dayToSend.toString() + " " + totalTime.toString() + " " + names.toString()
            if(configUpdated){
                configUpdated(ret) 
            }
        }
        else{
            logger.error("(DeleteConfig) Invalid operation type: " + operation)
        }

    }

    function parseNewConfig() : AlertMessage{
        let newStatus : AlertMessage = {
            show: false,
            severity: "error",
            message:"",
        }
        
        if(interval === 0){
            newStatus.show = true
            newStatus.severity = "info"
            newStatus.message = "Server will never send a delete event"
            return newStatus
        }

        if(!interval){
            newStatus.show = true
            newStatus.severity = "error"
            newStatus.message = "Please select a valid interval for the configuration"
            return newStatus
        }

        if(interval > 0 && (!time || !amPm)){
            newStatus.show = true
            newStatus.severity = "error"
            newStatus.message = "Please select a valid time for the configuration"
            return newStatus
        }


        if(interval > 1 && (!day)){
            newStatus.show = true
            newStatus.severity = "error"
            newStatus.message = "Please select a valid day for the configuration"
            return newStatus
        }

        newStatus.show = true
        newStatus.severity = "info"

        newStatus.message = `Server will send delete events `
        
        switch(interval){
            case 1:
                newStatus.message += `every day `
                break;
            case 2:
                newStatus.message += "every week on " + dayOfWeek[day] + " "
                break;
            case 3:
                newStatus.message += "every first " + dayOfWeek[day] + " of the month "
                break;
        }
    
        newStatus.message += `at ${time} ${amPm}`
        // newStatus.message += '\n deleting types : '
        // if(deleteTypes.flashkey) newStatus.message += 'Flashkeys, '
        // if(deleteTypes.config) newStatus.message += 'Configuration, '
        // if(deleteTypes.plu) newStatus.message += 'PLUs, '
        // if(deleteTypes.operator) newStatus.message += 'Operator, '
        // newStatus.message = newStatus.message.slice(0,-2)
        return newStatus
    }


    return(
        <div className="form-control" style={{maxWidth:"50rem"}}>
            <Form.Label>Delete Event Config:</Form.Label>
            <Tooltip className="info" title={<Typography fontSize={12}>Clears any changes made on the scale so that the only active configuration is from a profile</Typography>}>
                <IconButton aria-label="enableAccount" disableRipple>
                    <Help />
                </IconButton>
            </Tooltip>

            <div style={{display:"flex", paddingBottom: "1rem"}}>
            <Form.Label style={{marginTop: "0.5rem", paddingRight:"1rem"}}>Interval: </Form.Label>
            <Form.Select id="intervalSelector" style={{backgroundColor: "transparent", maxWidth:"300px"} }
                onChange={(e) => handleIntervalSelect(e)}
                onKeyDown={(e) => disableKey(e)}
                onClick={(e)=> e.stopPropagation()}
                value={interval}
            >
                <option value="" disabled>--Select an Interval Type--</option> : null

                {intervalTypes.map((type,index) => (
                    <option key={index} value={index}>
                        {type}
                    </option>
                ))}
            </Form.Select>

            </div>

            <div style={{display:"flex", paddingBottom: "1rem"}}>
                <Form.Label style={{marginTop: "0.5rem", paddingRight:"1rem"}}>Time: </Form.Label>
                <Form.Select id="timeSelector" style={{backgroundColor: "transparent", maxWidth:"250px"} }
                    disabled={interval==0}
                    onChange={(e) => handleTimeSelect(e)}
                    onKeyDown={(e) => disableKey(e)}
                    onClick={(e)=> e.stopPropagation()}
                    value={time}
                >
                    <option value="" disabled>--Select a Time--</option> : null

                    {Array.from(Array(12).keys()).map((type,index) => (
                        <option key={index+1} value={index+1}>
                            {index+1}
                        </option>
                    ))}
                </Form.Select>


                <Form.Select id="amSelector" style={{backgroundColor: "transparent", maxWidth:"100px", marginLeft:"1rem"} }
                    disabled={interval==0}
                    onChange={(e) => handleAmPmSelect(e)}
                    onKeyDown={(e) => disableKey(e)}
                    onClick={(e)=> e.stopPropagation()}
                    value={amPm ?? "AM"}
                >

                    {["AM", 'PM'].map((type,index) => (
                        <option key={index} value={type}>
                            {type}
                        </option>
                    ))}
                </Form.Select>

            </div>

            <div style={{display:"flex", paddingBottom: "1rem"}}>
            <Form.Label style={{marginTop: "0.5rem", paddingRight:"1rem"}}>Day of Week: </Form.Label>
            <Form.Select id="daySelector" style={{backgroundColor: "transparent", maxWidth:"300px"} }
                disabled={interval<2}
                onChange={(e) => handleDaySelect(e)}
                onKeyDown={(e) => disableKey(e)}
                onClick={(e)=> e.stopPropagation()}
                value={day}
            >
                <option value="" disabled>--Select a Day--</option> : null

                {dayOfWeek.map((type,index) => (
                    <option key={index} value={index}>
                        {type}
                    </option>
                ))}
            </Form.Select>

            </div>


            <Form.Label>Types to Delete:</Form.Label>
            <Tooltip className="info" title={<Typography fontSize={12}>Pick the types of data delete from a scale on a deletion event</Typography>}>
                <IconButton aria-label="enableAccount" disableRipple>
                    <Help />
                </IconButton>
            </Tooltip>

            <div style={{display:"flex", paddingBottom: "1rem", flexWrap:"wrap"}}>
                <FormCheck type="checkbox" className="d-flex p-2" >
                    <FormCheck.Label className="customCheckbox">
                        <FormCheck.Input defaultChecked={deleteTypes.flashkey} id="flashkeychkbx" className="me-2" onClick={(e : React.MouseEvent<HTMLInputElement, MouseEvent>) => handleCheckbox(e.currentTarget.checked, "flashkey")}/>
                        Flashkeys
                    </FormCheck.Label>
                </FormCheck>

                <FormCheck type="checkbox" className="d-flex p-2" >
                    <FormCheck.Label className="customCheckbox">
                        <FormCheck.Input defaultChecked={deleteTypes.config} id="configchkbx" className="me-2" onClick={(e : React.MouseEvent<HTMLInputElement, MouseEvent>) => handleCheckbox(e.currentTarget.checked, "config")}/>
                        Configuration
                    </FormCheck.Label>
                </FormCheck>

                <FormCheck type="checkbox" className="d-flex p-2" >
                    <FormCheck.Label className="customCheckbox">
                        <FormCheck.Input defaultChecked={deleteTypes.plu} id="pluchkbx" className="me-2" onClick={(e : React.MouseEvent<HTMLInputElement, MouseEvent>) => handleCheckbox(e.currentTarget.checked, "plu")}/>
                        PLU
                    </FormCheck.Label>
                </FormCheck>

                <FormCheck type="checkbox" className="d-flex p-2" >
                    <FormCheck.Label className="customCheckbox">
                        <FormCheck.Input defaultChecked={deleteTypes.operator} id="operatorchkbx" className="me-2" onClick={(e : React.MouseEvent<HTMLInputElement, MouseEvent>) => handleCheckbox(e.currentTarget.checked, "operator")}/>
                        Operator
                    </FormCheck.Label>
                </FormCheck>

                <FormCheck type="checkbox" className="d-flex p-2" >
                    <FormCheck.Label className="customCheckbox">
                        <FormCheck.Input defaultChecked={deleteTypes.label} id="labelchkbx" className="me-2" onClick={(e : React.MouseEvent<HTMLInputElement, MouseEvent>) => handleCheckbox(e.currentTarget.checked, "label")}/>
                        Label
                    </FormCheck.Label>
                </FormCheck>

            </div>

            {statusAlert.show ? <Alert id="statusAlert" className="m-1" variant="filled" severity={statusAlert.severity}>{statusAlert.message}</Alert> : null}
            <div style={{display:"flex", alignItems: "right", justifyContent:"right"}}>
                <Button id="actionBar1" variant="outline-primary" size="sm" disabled={statusAlert.severity === "error"}onClick={(e) => {
                    sendConfig()
                }}> 
                    <AddCircleOutlineIcon style={{marginRight: '8px'}}/>
                    Update Configuration
                </Button>
            </div>


        </div>
    )
}