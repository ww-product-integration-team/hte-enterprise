import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import {Breadcrumb} from '@themesberg/react-bootstrap';
import { IMPORT_TREE, SYSTEM_MANAGER } from "../../state/actions/accessActions"
import { useAppSelector } from '../../state';
import AccessControl from '../common/AccessPermissions';
import CsvUpload from './ImportCSVForAsset';
import CsvUploadForAssigner from './ImportCSVForAssigner';
import { ImportXML } from './importXML';

export default function Utilities() {

// ==================   PERMISSIONS CHECKING   ==================
// Everything to do with permissions / roles

    const user = useAppSelector((state) => state.access.account);
    const appRoles = useAppSelector((state) => state.access.roles);

    return (
        <>
        <div className="mb-4 mb-md-0">
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Utilities</Breadcrumb.Item>
            </Breadcrumb>

            <h2>Utilities</h2>
            <h5 className="mb-4">Advanced tools for HTe Enterprise data configuration</h5>
        </div>


        <div>

            
                <AccessControl
                    user = {user}
                    requiredRole = {appRoles[IMPORT_TREE]}
                    pageRequest={true}
                >
                    <CsvUploadForAssigner/>

                    <ImportXML/>
                    
                    <CsvUpload/>
                    
                    {/* Hiding for now, not sure how we want to handle threshold alerts NOT FINISHED*/}
                    {/* <ErrorStatusConfig/>  */}
                </AccessControl>
        </div>

        </>   
    );
}
