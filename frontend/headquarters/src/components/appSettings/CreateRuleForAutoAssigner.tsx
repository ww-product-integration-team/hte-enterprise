import { useState } from 'react';
import { useForm } from "react-hook-form";
import { FormCheck } from '@themesberg/react-bootstrap';
import { Checkbox, Dialog, DialogContent, DialogTitle, IconButton, Typography } from '@material-ui/core';
import { Form } from '@themesberg/react-bootstrap';
import Alert from '@mui/material/Alert';
import { useDispatch } from "react-redux";
// BANNER, Region, Store, Department, Scale
import { AddCircle, Block, Delete } from '@mui/icons-material';
import { Help, Save } from '@material-ui/icons';
import { AccessActions, useAppSelector } from '../../state';
import { useRspHandler } from '../utils/ResponseProvider';
import { AutoAssignRule } from './ImportCSVForAssigner';
import logger from '../utils/logger';
import { FormInput } from '../forms/FormHelper';
import { RuleAPI } from '../api';
import { Department } from '../../types/asset/AssetTypes';
import { validateIPv4 } from '../utils/utils';
import { FormControlLabel, Tooltip } from '@mui/material';
interface PostRuleProps {
    openPopup: boolean
    setOpenPopup: React.Dispatch<React.SetStateAction<boolean>>
}
interface EditRuleProps {
    openPopup: boolean
    setCurrentIP: React.Dispatch<React.SetStateAction<string>>
    ruleIPs: string
    ruleData: AutoAssignRule
}

function validateRule(newRule: AutoAssignRule) {
    //Validate IP address
    if (!validateIPv4(newRule.ipAddressStart)) {
        return ([true, "IP Address Start is invalid!"])
    }
    if (!validateIPv4(newRule.ipAddressEnd)) {
        return ([true, "IP Address End is invalid!"])
    }
    if (+newRule.ipAddressStart.split(".")[0] !== +newRule.ipAddressEnd.split(".")[0]) {
        return ([true, "First octet of IP addresses do not match!"])
    }
    if (+newRule.ipAddressStart.split(".")[1] !== +newRule.ipAddressEnd.split(".")[1]) {
        return ([true, "Second octet of IP addresses do not match!"])
    }
    if (+newRule.ipAddressStart.split(".")[2] !== +newRule.ipAddressEnd.split(".")[2]) {
        return ([true, "Third octet of IP addresses do not match!"])
    }
    if (+newRule.ipAddressStart.split(".")[3] > +newRule.ipAddressEnd.split(".")[3]) {
        return ([true, "Start IP is greater than End IP!"])
    }
    return ([false, ""])
}

function PostRule(props: PostRuleProps) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<AutoAssignRule>();
    const { openPopup, setOpenPopup } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])

    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();
    const [currentDepartment, setCurrentDepartment] = useState<string>("");
    const departments = useAppSelector(state=>state.asset.depts)
    const sortedDepts = Object.fromEntries(Object.entries(departments).sort(([,a],[,b]) => a.deptName1.localeCompare(b.deptName1, 'en', {numeric: true})))

    const [disableConflictingRuleEditAdd, setDisableConflictingRuleEditAdd] = useState(false);
    const [deleteConflictingRuleEditAdd, setDeleteConflictingRuleEditAdd] = useState(false);

    const defaultValues = {
        ipAddressStart: "",
        ipAddressEnd: "",
        banner: "",
        region: "",
        store: "",
        dept: "",
        enabled: false,
        selected: false,
    }

    const handleCloseDialog = (event?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if (event) { event.stopPropagation() }
        setOpenPopup(false)
        reset(defaultValues)
        setCurrentDepartment("")
    }

    const handleDept = (e: React.FormEvent<HTMLSelectElement>) => {
        let deptKeys = Object.keys(sortedDepts)
        let currentDeptKey = deptKeys[e.currentTarget.options.selectedIndex - 1]
        setCurrentDepartment(currentDeptKey)
    }

    const handleDisableConflict = (event) => {
        setDisableConflictingRuleEditAdd(event.target.checked)
        if (event.target.checked) {
            setDeleteConflictingRuleEditAdd(false)
        }
    }

    const handleDeleteConflict = (event) => {
        setDeleteConflictingRuleEditAdd(event.target.checked)
        if (event.target.checked) {
            setDisableConflictingRuleEditAdd(false)
        }
    }

    const handleAdd = handleSubmit(async (data: AutoAssignRule) => {    // When the Submit button is pressed
        logger.info("(postAutoAssignerRule) POST request (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])
        //create new rule object
        const newID = Date.now()
        let newRule: AutoAssignRule = {
            ipAddressStart: data.ipAddressStart,
            ipAddressEnd: data.ipAddressEnd,
            banner: data.banner,
            region: data.region,
            store: data.store,
            dept: departments[currentDepartment].deptName1,
            enabled: true,
            selected: false,
            disableFlag: disableConflictingRuleEditAdd,
            deleteFlag: deleteConflictingRuleEditAdd
        }

        let ruleValidated = validateRule(newRule)

        if (ruleValidated[0]) {
            setShowAlert([true, ruleValidated[1]])
            return
        }

        function onSuccess(response) {
            dispatch(RuleAPI.getAutoAssignRules(Date.now()))
            handleCloseDialog()
        }

        function onFail(response) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not add rule!"])
                return
        }

        if (addCallback(callbacks, newID, "Post AutoAssigner Rule", onSuccess, onFail)) {
            dispatch(RuleAPI.postAutoAssignerRule(newID, newRule))
        }
    })

    return (
        <Dialog
            open={openPopup}
            onClose={setOpenPopup}
            maxWidth="lg"
            fullWidth={true}
        >
            <DialogTitle>
                Create New Rule
            </DialogTitle>
            <Form>
                <div className="" style={{ marginLeft: "0.6rem" }}>
                    <FormCheck.Label>
                        <Typography>Handle Conflicting Rules</Typography>
                    </FormCheck.Label>
                    <Tooltip className="info" title={<Typography>Check one of the boxes below to determine how to handle existing rules that
                        conflict with this rule.
                    </Typography>}>
                        <IconButton size="small" style={{ marginBottom: "5px" }} disableRipple>
                            <Help />
                        </IconButton>
                    </Tooltip>
                </div>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={disableConflictingRuleEditAdd}
                            onClick={e => handleDisableConflict(e)}
                            disableRipple
                        />
                    }
                    label="Disable Conflicting Rules"
                    className="customCheckbox"
                    sx={{ cursor: "default" }}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={deleteConflictingRuleEditAdd}
                            onClick={e => handleDeleteConflict(e)}
                            disableRipple
                        />
                    }
                    label="Delete Conflicting Rules"
                    className="customCheckbox"
                    sx={{ cursor: "default" }}
                />
            </Form>
            <DialogContent>
                <>
                    <Form className="form-control" onSubmit={handleAdd}>
                        <FormInput
                            label="IP Address Start: "
                            id="ipAddressStart"
                            name="ipAddressStart"
                            type="text"
                            placeholder="Enter IP Address Start"
                            register={register}
                            validation={{
                                required: "Please enter an IP Address",
                                maxLength: { value: 16, message: "You exceeded the max length for an IP Address" }

                            }}
                            error={errors.ipAddressStart}
                        />
                        <br />
                        <FormInput
                            label="IP Address End: "
                            id="ipAddressEnd"
                            name="ipAddressEnd"
                            type="text"
                            placeholder="Enter IP Address End"
                            register={register}
                            validation={{
                                required: "Please enter an IP Address",
                                maxLength: { value: 16, message: "You exceeded the length for an IP Address" },
                            }}
                            error={errors.ipAddressEnd}
                        />
                        <br />
                        <FormInput
                            label="Banner: "
                            id="banner"
                            name="banner"
                            type="text"
                            placeholder="Enter Banner"
                            register={register}
                            validation={{
                                required: "Please enter a banner",
                                maxLength: { value: 45, message: "You exceeded the max banner length" }
                            }}
                            error={errors.banner}
                        />
                        <br />
                        <FormInput
                            label="Region: "
                            id="region"
                            name="region"
                            type="text"
                            placeholder="Enter Region"
                            register={register}
                            validation={{
                                required: "Please enter a region",
                                maxLength: { value: 45, message: "You exceeded the max region length" }
                            }}
                            error={errors.region}
                        />
                        <br />
                        <FormInput
                            label="Store: "
                            id="store"
                            name="store"
                            type="text"
                            placeholder="Enter Store"
                            register={register}
                            validation={{
                                required: "Please enter a region",
                                maxLength: { value: 45, message: "You exceeded the max store length" }
                            }}
                            error={errors.store}
                        />
                        <br />
                        <Form.Label>Department:</Form.Label>
                        <Form.Select required
                            id="dept"
                            name="dept"
                            onChange={(e) => handleDept(e)}
                            value={currentDepartment}
                        >
                            <option value="">--Select a department--</option>

                            {Object.keys(sortedDepts).map(deptKey => {
                                if (sortedDepts[deptKey].deptId !== AccessActions.DEFAULT_DEPT_DOMAIN_ID) {
                                    return (
                                        <option key={sortedDepts[deptKey].deptId} value={sortedDepts[deptKey].deptId}>
                                            {sortedDepts[deptKey].deptName1}
                                        </option>
                                    )
                                }
                                else { return null }
                            })}
                        </Form.Select>
                        {shouldShowAlert[0] ? <><br /><Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> </> : null}
                    </Form>
                </>
                <div className="formButtons">
                    <button className="formButton1" onClick={handleAdd} type="submit">
                        <AddCircle />
                        <span> Add Rule </span>
                    </button>
                    <button className="formButton1" onClick={handleCloseDialog} type="button">
                        <Block />
                        <span> Cancel </span>
                    </button>
                </div>
            </DialogContent>
        </Dialog>
    )
}

function EditRule(props: EditRuleProps) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<AutoAssignRule>();
    const { openPopup, setCurrentIP, ruleIPs, ruleData } = props;
    const [shouldShowAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const dispatch = useDispatch();
    const { callbacks, addCallback } = useRspHandler();
    const [openDelete, setOpenDelete] = useState(false)
    const departments = useAppSelector((state) => state.asset.depts);
    const sortedDepts = Object.fromEntries(Object.entries(departments).sort(([,a],[,b]) => a.deptName1.localeCompare(b.deptName1, 'en', {numeric: true})))
    const [currentDepartment, setCurrentDepartment] = useState<Department>(
        departments[Object.keys(departments).filter(dept => {
            if (departments[dept].deptName1 === ruleData.dept) {
                return departments[dept]
            }
        })[0]]
    );
    const [disableConflictingRuleEdit, setDisableConflictingRuleEdit] = useState(false);
    const [deleteConflictingRuleEdit, setDeleteConflictingRuleEdit] = useState(false);
    const [enableRule, setEnableRule] = useState(ruleData.enabled)

    const defaultValues = {
        ipAddressStart: "",
        ipAddressEnd: "",
        banner: "",
        region: "",
        store: "",
        dept: "",
        enabled: false,
        selected: false,
    }
    const handleCloseDialog = (event?: any) => {
        setShowAlert([false, "Something went wrong!"])
        if (event) { event.stopPropagation() }
        setCurrentIP("");
        reset(defaultValues);
    }
    const handleDepartment = (e: React.FormEvent<HTMLSelectElement>) => {
        let deptKeys = Object.keys(sortedDepts)
        let currentDeptKey = deptKeys[e.currentTarget.options.selectedIndex - 1]
        const dept = sortedDepts[currentDeptKey];
        setCurrentDepartment(dept);
    };

    const handleEdit = handleSubmit(async (data: AutoAssignRule) => {    // When the Submit button is pressed
        logger.info("(postAutoAssignerRule) POST request (Full Data): ", data)
        setShowAlert([false, "Something went wrong!"])
        
        const newID = Date.now()
        let rule: AutoAssignRule = {
            ipAddressStart: data.ipAddressStart ? data.ipAddressStart : ruleData.ipAddressStart,
            ipAddressEnd: data.ipAddressEnd ? data.ipAddressEnd : ruleData.ipAddressEnd,
            banner: data.banner ? data.banner : ruleData.banner,
            region: data.region ? data.region : ruleData.region,
            store: data.store ? data.store : ruleData.store,
            dept: currentDepartment ? currentDepartment.deptName1 : ruleData.dept,
            enabled: enableRule,
            selected: false,
            disableFlag: disableConflictingRuleEdit,
            deleteFlag: deleteConflictingRuleEdit
        }

        let ruleValidated = validateRule(rule)
        if (ruleValidated[0]) {
            setShowAlert([true, ruleValidated[1]])
            return
        }
        function onSuccess(response) {
            dispatch(RuleAPI.getAutoAssignRules(Date.now()))
            handleCloseDialog()
        }
        function onFail(response) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not edit rule!"])

            // Hack to bypass weird rendering issues generated by this component being within a VirtualTable
            // I don't like this, and neither should you, but without this the onFail result is utterly nauseating
            handleCloseDialog()
        }
        if (addCallback(callbacks, newID, "Edit AutoAssigner Rule", onSuccess, onFail)) {
            dispatch(RuleAPI.postAutoAssignerRule(newID, rule, ruleIPs))
        }
    })

    const handleDelete = handleSubmit(async (data: AutoAssignRule) => {    // When the Submit button is pressed
        setShowAlert([false, "Something went wrong!"])
        setOpenDelete(false)

        let rule: AutoAssignRule = {
            ipAddressStart: data.ipAddressStart ? data.ipAddressStart : ruleData.ipAddressStart,
            ipAddressEnd: data.ipAddressEnd ? data.ipAddressEnd : ruleData.ipAddressEnd,
            banner: data.banner ? data.banner : ruleData.banner,
            region: data.region ? data.region : ruleData.region,
            store: data.store ? data.store : ruleData.store,
            dept: currentDepartment ? currentDepartment.deptName1 : ruleData.dept,
            enabled: enableRule,
            selected: false,
            disableFlag: disableConflictingRuleEdit,
            deleteFlag: deleteConflictingRuleEdit
        }

        const newID = Date.now()
        function onSuccess(response) {
            dispatch(RuleAPI.getAutoAssignRules(Date.now()))
            handleCloseDialog()
        }
        function onFail(response) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not delete rule!"])

        }
        if (addCallback(callbacks, newID, "Delete AutoAssigner Rule", onSuccess, onFail)) {
            dispatch(RuleAPI.deleteAutoAssignerRule(newID, rule))
        }
    })

    const handleDisableConflict = (event) => {
        setDisableConflictingRuleEdit(event.target.checked)
        if (event.target.checked) {
            setDeleteConflictingRuleEdit(false)
        }
    }

    const handleDeleteConflict = (event) => {
        setDeleteConflictingRuleEdit(event.target.checked)
        if (event.target.checked) {
            setDisableConflictingRuleEdit(false)
        }
    }

    return (
        <Dialog
            open={openPopup}
            onClose={setCurrentIP}
            maxWidth="lg"
            fullWidth={true}
        >
            <DialogTitle>
                Edit Rule:
            </DialogTitle>
            <div className="" style={{ marginLeft: "0.6rem" }}>
                <FormCheck.Label>
                    <Typography>Handle Conflicting Rules</Typography>
                </FormCheck.Label>
                <Tooltip className="info" title={<Typography>Check one of the boxes below to determine how to handle existing rules that
                    conflict with this rule.
                </Typography>}>
                    <IconButton size="small" style={{ marginBottom: "5px" }} disableRipple>
                        <Help />
                    </IconButton>
                </Tooltip>
            </div>

            <FormControlLabel
                control={
                    <Checkbox
                        checked={disableConflictingRuleEdit}
                        onClick={e => handleDisableConflict(e)}
                        disableRipple
                    />
                }
                label="Disable Conflicting Rules"
                className="customCheckbox"
                sx={{ cursor: "default" }}
            />
            <FormControlLabel
                control={
                    <Checkbox
                        checked={deleteConflictingRuleEdit}
                        onClick={e => handleDeleteConflict(e)}
                        disableRipple
                    />
                }
                label="Delete Conflicting Rules"
                className="customCheckbox"
                sx={{ cursor: "default" }}
            />
            <DialogContent>
                <>
                    <Form className="form-control" onSubmit={handleEdit}>
                        <FormInput style={{ marginBottom: "10px" }}
                            label="IP Address Start: "
                            id="ipAddressStart"
                            name="ipAddressStart"
                            type="text"
                            placeholder={ruleData.ipAddressStart}
                            register={register}
                            validation={{
                                maxLength: { value: 13, message: "You exceeded the max length for an IP Address" }
                            }}
                            error={errors.ipAddressStart}
                        />
                        <FormInput style={{ marginBottom: "10px" }}
                            label="IP Address End: "
                            id="ipAddressEnd"
                            name="ipAddressEnd"
                            type="text"
                            placeholder={ruleData.ipAddressEnd}
                            register={register}
                            validation={{
                                maxLength: { value: 13, message: "You exceeded the length for an IP Address" }
                            }}
                            error={errors.ipAddressEnd}
                        />
                        <FormInput style={{ marginBottom: "10px" }}
                            label="Banner: "
                            id="banner"
                            name="banner"
                            type="text"
                            placeholder={ruleData.banner}
                            register={register}
                            validation={{
                                maxLength: { value: 45, message: "You exceeded the max banner length" }
                            }}
                            error={errors.banner}
                        />
                        <FormInput style={{ marginBottom: "10px" }}
                            label="Region: "
                            id="region"
                            name="region"
                            type="text"
                            placeholder={ruleData.region}
                            register={register}
                            validation={{
                                maxLength: { value: 45, message: "You exceeded the max region length" }
                            }}
                            error={errors.region}
                        />
                        <FormInput style={{ marginBottom: "10px" }}
                            label="Store: "
                            id="store"
                            name="store"
                            type="text"
                            placeholder={ruleData.store}
                            register={register}
                            validation={{
                                maxLength: { value: 45, message: "You exceeded the max store length" }
                            }}
                            error={errors.store}
                        />
                        <Form.Label>Department:</Form.Label>
                        <Form.Select style={{ marginBottom: "10px" }}
                            id="dept"
                            name="dept"
                            onChange={(e) => handleDepartment(e)}
                            value={currentDepartment ? currentDepartment.deptId : ""}
                        >
                            <option value="" disabled>--Select a department--</option>
                            {Object.keys(sortedDepts).map(deptKey => {

                                if (sortedDepts[deptKey].deptId !== AccessActions.DEFAULT_DEPT_DOMAIN_ID) {
                                    return (
                                        <option key={sortedDepts[deptKey].deptId} value={sortedDepts[deptKey].deptId}>
                                            {sortedDepts[deptKey].deptName1}
                                        </option>
                                    )
                                }
                                else { return null }
                            })}
                        </Form.Select>
                        <FormCheck type="checkbox" className="d-flex mb-2 p-2 tableTemplate" >
                            <FormCheck.Input className="me-2" style={{ marginBottom: "5px" }}
                                onClick={(e) => setEnableRule(e.target.checked)}
                                checked={enableRule}
                            />
                            <FormCheck.Label>
                                Rule Enabled
                            </FormCheck.Label>
                        </FormCheck>

                        {shouldShowAlert[0] ? <><br /><Alert className="m-1" variant="filled" severity="error">{shouldShowAlert[1]}</Alert> </> : null}
                    </Form>
                </>
                <div className="formButtons">
                    <button className="formButton1" onClick={handleEdit} type="submit">
                        <Save />
                        <span> Save Edit </span>
                    </button>
                    <button className="formButton1" onClick={() => { setOpenDelete(true) }} type="submit">
                        <Delete />
                        <span> Delete Rule </span>
                    </button>
                    <button className="formButton1" onClick={handleCloseDialog} type="button">
                        <Block />
                        <span> Cancel </span>
                    </button>
                </div>
                <Dialog open={openDelete}>
                    <DialogTitle> Delete Rule</DialogTitle>
                    <DialogContent>
                        <Form className="form-control" onSubmit={handleDelete}>
                            <Form.Label id="confirmDelete">Are you sure you want to delete this rule?</Form.Label>
                            <div className="formButtons">
                                <button className="formButton1" onClick={handleDelete} type="submit">
                                    <Delete style={{ marginRight: 10 }} />
                                    <span>Delete Rule</span>
                                </button>
                                <button className="formButton1" onClick={() => { setOpenDelete(false) }} type="button">
                                    <Block style={{ marginRight: 10 }} />
                                    <span>Cancel</span>
                                </button>
                            </div>
                        </Form>
                    </DialogContent>
                </Dialog>
            </DialogContent>
        </Dialog>

    )
}
export { PostRule, EditRule }