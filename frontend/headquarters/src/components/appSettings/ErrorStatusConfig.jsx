import { useState, useEffect } from 'react';
import { Button } from '@themesberg/react-bootstrap';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import SliderTemplate from '../common/Slider';
import { Form } from 'react-bootstrap';
import { Alert } from '@mui/material';



export default function ErrorStatusConfig(props){

    const sliderMin = 1;
    const sliderMax = 100;
    const baseMessage = "Configure what percentage of your fleet should reach the offline status before reaching the ";
    const warningAlert = "The offline fleet will be in a warning state if "
    const errorAlert = "\n of the overall fleet is offline and in an error state if that percentage exceeds ";

    const [units, setUnits] = useState("% ");
    const [statusAlert, setStatusAlert] = useState("info");
    const [errorThreshold, setErrorThreshold] = useState(Math.floor((Math.random() * 96) + 1));
    const [warningThreshold, setWarningThreshold] = useState(3);
    const [warningStatus, setWarningStatus] = useState ("info");
    const [errorStatus, setErrorStatus] = useState("info");
    const [alert1, setAlert1] = useState("")
    const [alert2, setAlert2] = useState("");
    const [display1, setDisplay1] = useState();
    const [display2, setDisplay2] = useState();
    const [returnValue, setReturnValue] = useState("");
    const [errMin, setErrMin] = useState("");

    function sendThreshold(){
        
    }

    function updateErrorThreshold(threshold){
        setReturnValue(threshold);
        setErrorThreshold(threshold);
    } 

    function changeThreshold(threshold){
        setWarningThreshold(threshold);
        setErrMin(threshold + 1);
    }

    useEffect(() => {
        setErrMin(warningThreshold + 1);
    },[])


    useEffect(() => {
        if(warningStatus === "info" && errorStatus === "info"){
            setStatusAlert("info");
            setAlert1(warningAlert);
            setAlert2(errorAlert);
            setDisplay1(warningThreshold);
            setDisplay2(errorThreshold);
            setUnits("% ");

        }else{
            setStatusAlert("error");
            setAlert1("Please enter a valid number between " + sliderMin + " " + units + " and " + sliderMax + " ");
            setAlert2("");
            setDisplay1("");
            setDisplay2("");
            setUnits("");
        }
    })

    return(
        <div className="form-control" style={{maxWidth:"50rem", marginTop: ".5rem"}}>

            <Form.Label>Offline Status Configuration:</Form.Label>

            <div className="sliderContainer" style = {{backgroundColor: "rgba(255, 213, 136, 0.644)", borderRadius: "5px", marginBottom: ".5rem"}}>
                <SliderTemplate 
                    title = "Warning Threshold:" 
                    units = { units } 
                    min = {sliderMin} 
                    max = {sliderMax} 
                    message = {baseMessage + "warning state"}
                    value = {warningThreshold}
                    changeSliderValue = {threshold => changeThreshold(threshold)}
                    errorStatus = {status => setWarningStatus(status)}
                    valueReturn = {() => {}}
                    minValue = {sliderMin}
                />
            </div>
            
            {/* <Alert severity = "error" className='m-1'> */}
            <div className="sliderContainer" style={{backgroundColor: "rgba(255, 142, 142, 0.644)", borderRadius: "5px", marginBottom: ".25rem"}}>
                <SliderTemplate 
                    title = "Error Threshold:" 
                    units = { units }
                    min = {sliderMin} 
                    max = {sliderMax} 
                    message = {baseMessage + "error state"} 
                    value = {errorThreshold}
                    changeSliderValue = {() => {}}
                    errorStatus = {status => setErrorStatus(status)}
                    valueReturn = {(threshold) => updateErrorThreshold(threshold)}
                    minValue = {errMin}
                ></SliderTemplate>
            </div>
            {/* </Alert> */}
           
            <Alert id="statusAlert" className="m-1" variant="filled" severity={ statusAlert }>{ alert1 + " " + display1 + " " + units + alert2 + " " + display2 + " " + units }</Alert>

            <div style={{display:"flex", alignItems: "right", justifyContent:"right"}}>
                <Button id="actionBar1" variant="outline-primary" size="sm" disabled={statusAlert=== "error"} onClick={(e) => {
                    sendThreshold()
                }}> 
                    <AddCircleOutlineIcon style={{marginRight: '8px'}}/>
                    Update Preferences
                </Button>
            </div>


        </div>
    )
}