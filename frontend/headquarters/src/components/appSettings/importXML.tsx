import { Button, Form } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { Dialog, DialogContent, DialogTitle, IconButton, Tooltip, Typography } from '@mui/material';
import { AddCircleOutline, Help } from '@mui/icons-material';
import { v4 as uuidv4 } from 'uuid';
import { useState, useEffect } from 'react';
import { useForm } from "react-hook-form"
import { FormInput } from '../forms/FormHelper';
import "react-datepicker/dist/react-datepicker.css";
import { useAppSelector, AssetActions } from '../../state';
import { BannerAPI, RegionAPI, StoreAPI, DeptAPI, AssetAPI } from "../api/index"
import { Loop, Block } from '@mui/icons-material';
import Alert from '@mui/material/Alert';


// =================== CATEGORY DATA ===================
interface JsonFormat {
    text: string,
    name: string,
    type: string,
    id: string,
    bannerId?: string,
    regionId?: string,
    storeId?: string,
    deptId?: string,
    status?: {
        state: string,
        message: string
    },
    properties?: {},
    children?: { [id: string]: JsonFormat }
}

const initialJson: JsonFormat = {
    text: "",
    name: "",
    type: "",
    id: ""
}

let hadParsingError = false
let postArray = [initialJson] //Constructed array for sending the imported array to the
function parseXML(xmlDoc, parentObj, currentID, banners, shouldMerge = true) {
    let obj = initialJson
    obj.name = xmlDoc.attributes.getNamedItem("name") == null ? null : xmlDoc.attributes.getNamedItem("name").nodeValue
    switch (xmlDoc.nodeName) {
        case 'root':        // BANNER ================================================================
            obj.type = "banner";
            obj.id = String(currentID)
            obj.name = xmlDoc.nodeName == null ? "root" : xmlDoc.nodeName;

            if (shouldMerge) {
                //Set Banner id to the last in the banner list +1
                // ! after a variable or value asserts it is not-null
                currentID = banners.length > 0 ? parseInt(banners!.at(-1)!.bannerId) + 1 : 1
                obj.id = currentID
            }

            break;
        case 'region':      // REGION ================================================================
            obj.type = xmlDoc.nodeName;
            obj.id = parentObj.id + "|" + currentID;
            obj.regionId = currentID
            obj.bannerId = parentObj.id

            break;
        case 'store':       // STORE ================================================================
            obj.type = xmlDoc.nodeName;
            obj.id = parentObj.regionId + "|" + currentID;
            obj.storeId = currentID
            obj.regionId = parentObj.regionId

            break;
        case 'department':  // DEPARTMENT ===========================================================
            obj.type = "dept";
            obj.id = parentObj.storeId + "|" + currentID
            obj.deptId = currentID

            break;
        case 'asset':       // SCALE ================================================================

            obj.type = "scale";
            obj.id = Date.now() + "|" + currentID //Temporary ID for local tree, this should not be used anywhere
            obj.name = xmlDoc.attributes.getNamedItem("ip") == null ? null : xmlDoc.attributes.getNamedItem("ip").nodeValue
            obj.properties = {
                ipAddress: obj.name,
                hostname: xmlDoc.attributes.getNamedItem("hostname") == null ? null : xmlDoc.attributes.getNamedItem("hostname").nodeValue,
                //serialNumber: null,
                scaleModel: xmlDoc.attributes.getNamedItem("device_type") == null ? null : xmlDoc.attributes.getNamedItem("device_type").nodeValue,
                storeId: parseInt(parentObj.id.split("|")[0]),
                deptId: parseInt(parentObj.id.split("|")[1]),
                //countryCode: null,
                //buildNumber: null,
                application: xmlDoc.attributes.getNamedItem("application_version") == null ? null : xmlDoc.attributes.getNamedItem("application_version").nodeValue,
                //operatingSystem: null,
                //systemController: null,
                loader: xmlDoc.attributes.getNamedItem("bootloader_version") == null ? null : xmlDoc.attributes.getNamedItem("bootloader_version").nodeValue,
                smBackend: xmlDoc.attributes.getNamedItem("backend_version") == null ? null : xmlDoc.attributes.getNamedItem("backend_version").nodeValue,
                //lastReportTimestampUtc: null,
                //lastTriggerType: null,
                //profileId: null,
                enabled: true,
                last24HourStatus: "XXXXXXXXXXXXXXXXXXXXXXXX",
                last24HourSync: "------------------------",
                primaryScale: false
            }
            obj.status = {
                state: "disabled",
                message: "Scale is disabled and is not communicating with server"
            }
            break;
        default:
            hadParsingError = true
            postArray = []
            break;
    }
    postArray.push({ ...obj })

    obj.children = {}

    // let elements = xmlDoc.getElementsByTagName()
    for (let item of xmlDoc.children) {
        obj.children[item.id] = parseXML(item, obj, currentID + 1, banners)
        // obj.children.push(parseXML(item, obj, currentID+1, banners))
        //logger(item)
        //parseXML(item)
    }

    return obj
}

// Upload Files
function ImportXML() {

    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const [openDialog, setOpenDialog] = useState(false)
    const [openCurrentlyImporting, setOpenCurrentlyImporting] = useState(false)
    const [formType, setFormType] = useState("password")
    const { callbacks, addCallback } = useRspHandler();

    const dispatch = useDispatch();
    const banners = useAppSelector(state => state.asset.banners)
    const user = useAppSelector(state => state.access.account)

    const reduxCriticalDialog = useAppSelector((state) => state.asset.criticalDialog)
    const [criticalDialogXML, setCriticalDialogXML] = useState<string>(reduxCriticalDialog)

    useEffect(() => {
        setCriticalDialogXML(reduxCriticalDialog)
    }, [reduxCriticalDialog])

    useEffect(() => {
        if (criticalDialogXML) {
            window.onbeforeunload = () => true
            return () => {
                window.onbeforeunload = null;
            }
        }
    }, [criticalDialogXML]);

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        input: "",
    };

    const handleCloseDialog = (event) => {
        setShowAlert([false, "Something went wrong!"])
        if (event != null) {
            event.stopPropagation()
        }

        reset({ defaultValues });
    }

    const handleCloseCritical = (event, reason) => {
        if (reason && reason === "backdropClick")
            return;
        setOpenCurrentlyImporting(false);
    }

    // POST File Upload to API
    const handleImport = handleSubmit(async (data, event) => { // When the Submit button is pressed
        logger.debug("(ImportForm) POST request (Just File): ", data.fileName[0]);
        setShowAlert([false, "Something went wrong!"])
        let replaceTreeBool = false;
        let file = data.fileName[0];
        let reader = new FileReader();
        let parser = new DOMParser();
        let tree: JsonFormat[] = []
        if (!data.fileName[0]) {
            setShowAlert([true, "No file imported"]);
            setOpenDialog(false);
            return
        }
        reader.onload = function (progressEvent) {
            let xmlDoc: Document = parser.parseFromString(this.result as string, "text/xml");
            hadParsingError = false
            postArray = []
            let elements = xmlDoc.getElementsByTagName("root")
            if (!elements || elements.length === 0) {
                setShowAlert([true, "Invalid file format!"]);
                return
            }
            for (let i = 0; i < elements.length; i++) {
                tree.push(parseXML(elements[i], null, 1, banners))
            }
            // for(let item of xmlDoc.children){
            //     tree.push(parseXML(item, null, 1))
            // }

            if (hadParsingError) {
                setShowAlert([true, "Invalid file format!"])
                return
            } else {
                logger.debug("(ImportForm) ", postArray)
                //dispatch(AssetActions.setTreeView(tree))
            }

            //Parsing was a success Now post to array
            //PostTree()
            logger.debug("(ImportForm) ", postArray)
            const newID = Date.now()
            function onSuccess(response) {
                logger.debug("(ImportForm) Import Form Response:", response)
                handleCloseDialog(event)
                dispatch(AssetActions.setCriticalDialog(""))
                dispatch(BannerAPI.fetchBanners())
                dispatch(RegionAPI.fetchRegions())
                dispatch(StoreAPI.fetchStores())
                dispatch(DeptAPI.fetchDepartment())
            }
            function onFail(response) {
                logger.error("(ImportTree) Error importing tree", response)
                response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                    setShowAlert([true, "Error trying to import existing tree!"])
                dispatch(AssetActions.setCriticalDialog(""))
            }
            if (addCallback(callbacks, newID, "Import Tree", onSuccess, onFail)) {
                dispatch(AssetAPI.importTree(newID, postArray))
                dispatch(AssetActions.setCriticalDialog("XML"))
                setOpenDialog(false)
            }

        };
        reader.readAsText(file);
    })
    const replaceDialog = <>
        <Dialog
            open={openDialog}
            onClose={setOpenDialog}
            maxWidth="lg"
            fullWidth={true}
        >
            <DialogTitle>
                Replace Asset List?
            </DialogTitle>
            <DialogContent>

                <h2>Warning! You are about to rewrite your asset list.</h2>
                {/* <Form id="password" className="form-control mt-4" onSubmit={handleImport}>
                    <FormInput
                        label="Input Password for Replacing Asset List"
                        id="password"
                        name="password"
                        type={formType}
                        placeholder="Your account password"
                        register={register}
                        validation={{}}
                        // validation={{required: "Incorrect password, try again", validate: value => value === user.password || "Incorrect password, try again"}}
                        error={errors.password}
                        setFormType={setFormType}
                    />
                </Form> */}
                {showAlert[0] ? <Alert className="m-1" variant="filled" severity="info">{showAlert[1]}</Alert> : null}
                <div className="formButtons">
                    <button className="formButton1" type="submit" onClick={() => { handleImport() }} >
                        <Loop />
                        <span> Replace Asset List</span>
                    </button>
                    <button className="formButton1" onClick={() => { setOpenDialog(false) }} type="button" value="Cancel">
                        <Block />
                        <span> Cancel</span>
                    </button>
                </div>
            </DialogContent>
        </Dialog>

    </>

    const criticalDialog = <>
        <Dialog open={criticalDialogXML ? true : false} onClose={handleCloseCritical} fullWidth={true} maxWidth="sm">
            <DialogTitle>
                <div style={{ display: "flex", justifyContent: "left" }}>
                    <div style={{ marginTop: ".4rem", marginRight: "1rem" }} className='spinnerAsset' />
                    <Typography variant="h6" component="div" >Importing XML data...</Typography>
                </div>
            </DialogTitle>

            <DialogContent>
                <Typography variant="body1" component="div" >Importing data. Please wait; <b>do not</b> refresh your browser... </Typography>
            </DialogContent>
        </Dialog>
    </>
    return (
        <div style={{ maxWidth: "50rem", marginTop: ".5rem" }}>
            <>
                <Form className='mt-4' onSubmit={handleImport}>
                    <div className="form-control">
                        <Form.Label>Upload XML from HTe Scale Management to Asset List:</Form.Label>
                        <Tooltip className="info" title={<Typography fontSize={12}>Adds or replaces tree structure in Asset List from HTe Enterprsie using the file: enterprise.xml
                            Please read the instructions below before using this feature.</Typography>}>
                            <IconButton aria-label="enableAccount" disableRipple>
                                <Help />
                            </IconButton>
                        </Tooltip>
                        <FormInput
                            label=""
                            id="fileName"
                            name="fileName"
                            type="file"
                            accept=".xml"
                            method="post"
                            action="#"
                            register={register}
                            validation={{ required: "Please select a file to upload" }}
                            error={errors.fileName}
                        />
                        <span className="fw-bold">Instructions for Use:</span>
                        <ul>
                            <li key={uuidv4()}>Click "Choose File" and go to the following file path: C:\HTe Scale Management</li>
                            <li key={uuidv4()}>Select the enterprise.xml file</li>
                            <li key={uuidv4()}>Click "Upload XML"</li>
                            <li key={uuidv4()}><b>Note:</b> The current Asset List will be replaced. Ensure there are no scales assigned to a batch or group beforehand!</li>
                        </ul>

                        <div style={{ display: "flex", alignItems: "right", justifyContent: "right" }}>
                            <Button id="actionBar1" variant="outline-primary" size="sm" type="button" onClick={() => { setShowAlert([false, ""]); setOpenDialog(true) }} disabled={errors.fileName}>
                                <AddCircleOutline style={{ marginRight: '8px' }} />
                                Upload XML
                            </Button>
                        </div>
                        {openDialog ? errors.fileName ? <>{setOpenDialog(false)} </> : replaceDialog : null}
                        {criticalDialogXML && criticalDialogXML.includes('XML') ? criticalDialog : null}
                    </div>
                </Form>

            </>
        </div>
    );
}

export { ImportXML }