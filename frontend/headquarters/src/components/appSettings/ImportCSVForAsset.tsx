import { Button, Form } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { Dialog, DialogContent, DialogTitle, IconButton, Tooltip, Typography } from '@mui/material';
import { AddCircleOutline, Help, MenuBook } from '@mui/icons-material';
import { useState, useEffect } from 'react';
import { AssetAPI, ScaleAPI } from '../api';
import { useForm } from 'react-hook-form';
import { FormInput } from '../forms/FormHelper';
import { useAppSelector, AssetActions } from '../../state';
import { ResponseType } from '../../types/storeTypes';

export default function CsvUpload() { // Setting up a special environment just for CSV-related work
  interface CsvFileUpload {
    uploadFile: any
  }
  const { register, handleSubmit, reset, formState: { errors } } = useForm<CsvFileUpload>() // Custom form
  const dispatch = useDispatch()
  const [openCurrentlyImporting, setOpenCurrentlyImporting] = useState(false)
  const { callbacks, addCallback } = useRspHandler()

  const reduxCriticalDialog = useAppSelector((state) => state.asset.criticalDialog)
  const [criticalDialogCSVUpload, setCriticalDialogCSVUpload] = useState<string>(reduxCriticalDialog)

  useEffect(() => {
    setCriticalDialogCSVUpload(reduxCriticalDialog)
  }, [reduxCriticalDialog])

  useEffect(() => {
    if (criticalDialogCSVUpload) {
      window.onbeforeunload = () => true
      return () => {
        window.onbeforeunload = null;
      }
    }
  }, [criticalDialogCSVUpload]);

  const handleCloseCritical = (event, reason) => {
    if (reason && reason === "backdropClick")
      return;
    setOpenCurrentlyImporting(false);
  }

  const postCsv = handleSubmit(async (data: CsvFileUpload) => {

    function onSuccess(response: ResponseType) {
      logger.info("File saved successfully")
      dispatch(AssetActions.setCriticalDialog(""))
    }

    function onFail(response: ResponseType) {
      logger.info("Could not save file")
      dispatch(AssetActions.setCriticalDialog(""))
    }

    const newID = Date.now()
    const fileData = new FormData()

    fileData.append('file', data.uploadFile[0])

    if (addCallback(callbacks, newID, "Save File", onSuccess, onFail, { requestMsg: "Sending CSV file...", successMsg: "Successfully sent CSV file!", failMsg: "default" })) {
      dispatch(AssetAPI.importCSVToTree(newID, fileData))
      dispatch(AssetActions.setCriticalDialog("CSV Assets"))
    }
  })

  const criticalDialog = <>
    <Dialog open={criticalDialogCSVUpload ? true : false} onClose={handleCloseCritical} fullWidth={true} maxWidth="sm">
      <DialogTitle>
        <div style={{ display: "flex", justifyContent: "left" }}>
          <div style={{ marginTop: ".4rem", marginRight: "1rem" }} className='spinnerAsset' />
          <Typography variant="h6" component="div" >Importing assets...</Typography>
        </div>
      </DialogTitle>

      <DialogContent>
        <Typography variant="body1" component="div" >Importing data. Please wait; <b>do not</b> refresh your browser... </Typography>
      </DialogContent>
    </Dialog>
  </>

  return (
    <div style={{ maxWidth: "50rem", marginTop: ".5rem" }}>
      <Form className='mt-4' onSubmit={postCsv}>
        <div className="form-control">
          <Form.Label>Upload CSV to Asset List:</Form.Label>
          <Tooltip className="info" title={<Typography fontSize={12}>Loads fleet data from a CSV file into your Asset List.
            Please read the instructions below before using this feature.</Typography>}>
            <IconButton style={{ marginBottom: "5px" }} aria-label="enableAccount" disableRipple>
              <Help />
            </IconButton>
          </Tooltip>
          <FormInput
            label=""
            id="uploadFile"
            name="uploadFile"
            type="file"
            accept=".csv"
            enctype="multipart/form-data"
            method="post"
            action="#"
            register={register}
            validation={{ required: "Please select a file to upload" }}
            error={errors.uploadFile}
          />
          <span className="fw-bold">Instructions for Use:</span>
          <ul>
            <span>Ensure that your CSV file has headers, and for each row in the file:</span>
            <div style={{ "marginLeft": "2rem" }}>
              <li>Enter your banner in the first column</li>
              <li>Enter your region in the second column</li>
              <li>Enter your store in the third column</li>
              <li>Enter your department in the fourth column</li>
              <li>Enter your scale hostname in the fifth column</li>
              <li>Enter your scale IP address in the sixth column</li>
            </div>
          </ul>
          <div style={{ display: "flex", alignItems: "right", justifyContent: "right" }}>
            <Button id="actionBar1" variant="outline-primary" size="sm" type="submit" onClick={() => { return postCsv }}>
              <AddCircleOutline style={{ marginRight: '8px' }} />
              Upload File
            </Button>
          </div>
        </div>
      </Form>
      {criticalDialogCSVUpload && criticalDialogCSVUpload.includes("CSV Assets") ? criticalDialog : null}
    </div>
  )
}