import { Help } from "@material-ui/icons";
import { Checkbox, FormControlLabel, IconButton, Tooltip, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import { ResponseType } from "../../types/storeTypes";
import { useRspHandler } from "../utils/ResponseProvider";
import { useDispatch } from "react-redux";
import logger from '../utils/logger';
import { ConfigAPI } from "../api";
import { ConfigActions, useAppSelector } from "../../state";

export default function PricingZoneConfig() {
    const {callbacks, addCallback} = useRspHandler()
    const dispatch = useDispatch()
    const reduxZonePricing = useAppSelector(state=>state.config.zonePricing.zonePricing)
    const [usingZonePricing, setUsingZonePricing] = useState(reduxZonePricing === "true")

    const updateZonePricingConfig = (event) => {
        function onSuccess(response: ResponseType) {
            logger.info("Successfully toggled Zone Pricing")
            dispatch(ConfigAPI.getConfiguration(Date.now(), ["zonePricing"], ConfigActions.setZonePricing))
        }
        function onFail(response: ResponseType) {
            logger.error("Error toggling Zone Pricing")
            dispatch(ConfigAPI.getConfiguration(Date.now(), ["zonePricing"], ConfigActions.setZonePricing))
        }

        const newID = Date.now()
        setUsingZonePricing(event.target.checked)
        if (addCallback(callbacks, newID, "Toggle Zone Pricing", onSuccess, onFail)) {
            dispatch(ConfigAPI.setConfiguration(newID, {"zonePricing": event.target.checked}))
        }
    }

    useEffect(() => {
        dispatch(ConfigAPI.getConfiguration(Date.now(), ["zonePricing"], ConfigActions.setZonePricing))
    }, [])
    useEffect(() => {
        setUsingZonePricing(reduxZonePricing === "true")
    }, [reduxZonePricing])

    return (
        <div className="form-control" style={{maxWidth: "50rem", marginTop: ".5rem"}}>
            <Form.Label>Zone Pricing:</Form.Label>
            <Tooltip className="info" title={
                <Typography fontSize={12}>Enables or disables using Zone Pricing with Profiles</Typography>
            }>
                <IconButton aria-label="toggleZonePricingInfo" disableRipple>
                    <Help/>
                </IconButton>
            </Tooltip>

            <br/>

            <FormControlLabel 
                control={
                    <Checkbox
                        checked={usingZonePricing}
                        onClick={e => updateZonePricingConfig(e)}
                        disableRipple
                        sx={{marginLeft: "0.5rem", cursor: "default"}}
                    />
                }
                label="Use Zone Pricing"
                className="customCheckbox"
                sx={{cursor: "default"}}
            />
        </div>
    )
}