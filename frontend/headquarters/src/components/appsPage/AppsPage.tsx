import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import {Breadcrumb, Button} from '@themesberg/react-bootstrap';
import { IMPORT_TREE, SYSTEM_MANAGER } from "../../state/actions/accessActions"
import { useAppSelector } from '../../state';
import AccessControl from '../common/AccessPermissions';
import Grid from '@mui/material/Grid';
import { Card, Typography } from '@mui/material';
import { CardActions, CardContent, CardHeader, IconButton, Tooltip } from '@material-ui/core';
import { Help } from '@material-ui/icons';

function BoxLink(props){
    const {title, description, link} = props
    return (
        <Grid item xs={2} sm={4} md={4} key={1}>
            <Card>
                <CardHeader
                    title = {title}
                />
                <CardContent>
                    <Typography variant="body1" color="text.secondary">
                    {description}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Tooltip 
                        enterDelay={800} 
                        key={title + description}
                        disableTouchListener 
                        title={
                            <Typography fontSize={12}>
                                Redirects to {link}
                            </Typography>
                        }>
                            <div className='assetBar'>
                                <Button id="actionBar2" variant="outline-primary"
                                    onClick={(event) => {
                                        event.preventDefault();
                                        window.open(link, "_blank");
                                        }}>
                                        Link to {title}
                                </Button>
                            </div>
                    </Tooltip>
                </CardActions>
            </Card>
        </Grid>
    )
}

export default function AppsPage() {

// ==================   PERMISSIONS CHECKING   ==================
// Everything to do with permissions / roles

    const user = useAppSelector((state) => state.access.account);
    const appRoles = useAppSelector((state) => state.access.roles);
    
    return (
        <>
        <div className="mb-4 mb-md-0">
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Apps</Breadcrumb.Item>
            </Breadcrumb>

            <h2>Apps</h2>
            <h5 className="mb-4">
                Link to Hobart Applications Downloads
                <Tooltip 
                    className="info" 
                    title={
                        <Typography fontSize={12}>
                            Each button will redirect you to a new website which contains the download for the corressponding application
                        </Typography>
                    }>
                    <IconButton style={{ marginBottom: "5px" }} aria-label="enableAccount" disableRipple>
                        <Help />
                    </IconButton>
                </Tooltip>
            </h5>
        </div>
        <div className="mainContent">
                <AccessControl
                    user = {user}
                    requiredRole = {appRoles[IMPORT_TREE]}
                    pageRequest={true}
                >
                    <div>
                    <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 1, sm: 8, md: 12 }}>
                        <BoxLink 
                            title = {"Hobart Communication Tool"}
                            description = {"This is the free desktop application that can be used for sending and receiving data to and from scales."}
                            link = {"https://wwhobart.box.com/s/ec656p2l9ge94yi838qsdvvthv1mji6j"}
                        />
                        <BoxLink 
                            title = {"HTe Scale Management"}
                            description = {"This is the licensed software suite that contains, PLU Manager, Label Designer, Firmware Upgrade Manager, and Scale Monitoring."}
                            link = {"https://wwhobart.box.com/s/mj403kiokrlrktjps7og6ff89k5a0pli"}
                        />
                    </Grid>
                        
                    </div>
                </AccessControl>
        </div>

        </>   
    );
}
