import ErrorIcon from '@mui/icons-material/Error';
import SuccessIcon from '@mui/icons-material/CheckCircle';
import WarningIcon from '@mui/icons-material/Warning';
import CloudOffIcon from '@mui/icons-material/CloudOff';
import MonitorHeartIcon from '@mui/icons-material/MonitorHeart';
import Box from '@material-ui/core/Box';
import { alpha, styled } from '@mui/material/styles';
import 'status-indicator/styles.css'

/*This function displays a message highlighed with a color that indicates the status of the message. 
Usage : 
status : (String), used to show an icon and background color that is indicative of the string. 
            options: online, warning, disabled, NOTE: if string is not found in case statement it will default to an ERROR statement

messsage : (String or Object), an object that will have all of the available message outcomes based on the status. If there is only one message i.e. a string then
           the object will be made autmatically
            structure :  message = {online: "STRING",
                                    away: "STRING",
                                    disabled: "STRING",
                                    error: "STRING"}
*/
export function GetMessageView(status : string, messageObj : string | {online: string,
                                                                    away: string,
                                                                    disabled: string,
                                                                    error: string}){

    let message : {online: string,
        away: string,
        disabled: string,
        error: string}
    if (typeof messageObj === 'string' || messageObj instanceof String){
        message = {online: messageObj as string,
                   away: messageObj as string,
                   disabled: messageObj as string,
                   error: messageObj as string}
    }
    else{
        message = messageObj
    }


    switch(status){
        case 'online':
        case 'success':
        case 'ONLINE':
            return(
                <div style={{width: '100%', display: 'flex', backgroundColor: alpha("#4bd28f", 0.25), borderRadius: 5}}>
                    <SuccessIcon style={{color: "#4bd28f", margin:4}}/>
                    <Box style={{fontWeight: '400', margin: '4px'}}>{message == null ? "Online and reporting" : message.online}</Box>
                </div>
            )
        case 'away':
        case 'warning':
        case 'WARNING':
            return(
                <div style={{width: '100%', display: 'flex', backgroundColor: alpha("#ffaa00", 0.25), borderRadius: 5}}>
                    <WarningIcon style={{color: "#ffaa00", margin:4}}/>
                    <Box style={{fontWeight: '400', margin: '4px'}}>{message == null ? "Warning" : message.away}</Box>
                </div>
            )
        case 'disabled':
        case 'DISABLED':
            return(
                <div style={{width: '100%', display: 'flex', backgroundColor: alpha("#a4abb0", 0.25), borderRadius: 5}}>
                    <CloudOffIcon style={{color: "#a4abb0", margin:4}}/>
                    <Box style={{fontWeight: '400', margin: '4px'}}>{message == null ? "Scale is disabled and is not communicating with server" : message.disabled}</Box>
                </div>
            )
        case 'synced':
            return(
                <div style={{width: '100%', display: 'flex', backgroundColor: alpha("#73cfec", 0.25), borderRadius: 5}}>
                    <MonitorHeartIcon style={{color: "#73cfec", margin:4}}/>
                    <Box style={{fontWeight: '400', margin: '4px'}}>{message == null ? "Scale is synced " : message.disabled}</Box>
                </div>
            )
        default:
            return(
                <div style={{width: '100%', display: 'flex', backgroundColor: alpha("#ff4d4d", 0.25), borderRadius: 5}}>
                    <ErrorIcon style={{color: "#ff4d4d", margin:4}}/>
                    <Box style={{fontWeight: '400', margin: '4px'}}>{message == null ? "Scale is in an error state, please contact Hobart support" : message.error}</Box>
                </div>
            )
    }
}