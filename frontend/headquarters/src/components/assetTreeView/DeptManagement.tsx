import { useState, useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { Button, ButtonGroup, Breadcrumb} from '@themesberg/react-bootstrap';
import { AddCircle, Delete } from '@material-ui/icons';
import { Form} from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import { CreateDepartment, PutDepartment, DeleteDepartment } from '../forms/DepartmentForm';
import { ProfileAPI } from '../api';
import { AccessActions, useAppSelector } from '../../state';
import { DeptAPI } from "../api/index" 
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { DEFAULT_ADMIN_DOMAIN_ID, MANAGE_PROFILES } from "../../state/actions/accessActions"
import { Department, getDefaultDepartment } from '../../types/asset/AssetTypes';
import { Profile } from '../../types/asset/ProfileTypes';
import { ResponseType } from '../../types/storeTypes';


export default function DeptManagement() {

    const [currentDepartment, setCurrentDepartment] = useState<Department | null>(null);   // Chosen Department (Select tag)
    const [currentProfile, setCurrentProfile] = useState<Profile | null | undefined>(null);   // Chosen Profile (Select tag)
    const [openAdd, setOpenAdd] = useState(false);                      // Create   Department
    const [openDelete, setOpenDelete] = useState(false);                // Delete   Department

    const {callbacks, addCallback} = useRspHandler();

    const history = useHistory();
    const dispatch = useDispatch();

// ==================   PERMISSIONS CHECKING   ==================
// Everything to do with permissions / roles

    const currentAccount = useAppSelector((state) => state.access.account);
    const user = useAppSelector((state) => state.access.account);
    const loggedIn = useAppSelector((state) => state.access.loggedIn);

    const handlePermissionCheck = () => {
        try {
            // Role 2001 (ManageProfiles)
            if(user.accessLevel >= 3000 || user.roles.find((domain) => domain.name === MANAGE_PROFILES) || currentAccount.domain.domainId === DEFAULT_ADMIN_DOMAIN_ID) {
                dispatch(ProfileAPI.fetchProfiles())
            }
            else{
                logger.error("not successful");
                history.push("/NotPermitted")
            }
            
        } catch (error) {
            // The catch block gets ran on page load, bootleg check to see if user is logged in
            if (loggedIn === false) {
              history.push("Login")
            }
        }
    }

// ==================   FILTER DEPARTMENTS   ==================
// Only show departments within current user's domain

    const profiles = useAppSelector((state) => state.profile.profiles);
    const departments = useAppSelector((state) => state.asset.depts);
    const domains = useAppSelector(state=>state.access.domains)
    const [filteredDepts, setFilteredDepts] = useState<Department[]>([])

    const handleFilterDepts = () => {
        setFilteredDepts([])

        let sortedAndFiltered = Object.values(Object.fromEntries(Object.entries(departments).sort(([,a],[,b]) => 
            a.deptName1.localeCompare(b.deptName1, 'en', {numeric: true}))))
        setFilteredDepts(Object.values(sortedAndFiltered))
    }

// ==================   SEARCHDOMAIN()   ==================
// Used to display the domain of the currently selected scale profile
    function searchDomain(id: string) {
        if (id === AccessActions.DEFAULT_ADMIN_DOMAIN_ID) {
            return "Admin Only"
        }
        if (id === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID) {
            return "Unassigned (Admin Only)"
        }
        let domain = ""
        for (let key of Object.keys(domains)) {
            if (key === id) {
                domain = domains[key].name + " (" + domains[key].type + ")"
                break
            }
        }
        return domain
    }

// =======================================================

    const handleView = (e : React.FormEvent<HTMLSelectElement>) => {
        const dept = filteredDepts[e.currentTarget.options.selectedIndex - 1];
        setCurrentDepartment(dept);
    };

    const handleChangeProfile = (e : React.FormEvent<HTMLSelectElement>) => {
        const profileIndex = profiles[e.currentTarget.options.selectedIndex-1].profileId;
        logger.info("Setting current profile to " + profileIndex)
        let tmp = {...currentDepartment!}
        tmp.defaultProfileId = profileIndex 
        setCurrentDepartment(tmp)



        const newID = Date.now()
        function onSuccess(response : ResponseType){
            dispatch(DeptAPI.fetchDepartment())
        }
        function onFail(response : ResponseType){
            logger.info("(SetDeptProfile) Something went wrong", response)
        }
        if(addCallback(callbacks, newID, "Set Department Profile", onSuccess, onFail)){
            dispatch(DeptAPI.assignDeptProfile(newID, tmp.deptId, tmp.requiresDomainId, profileIndex, []))
        }
    };

    const disableKey = (e : any) => {
       const ev = e ? e : window.event;
    
       if (ev) {
           if   (ev.preventDefault)     ev.preventDefault();
           else                         ev.returnValue = false;         
       }    
    }

    useEffect(() => {
        if(currentDepartment === null)  {return}

        if (currentDepartment) 
            setCurrentProfile(profiles.find(profile => profile.profileId === currentDepartment.defaultProfileId))

        logger.info("Current department: ", currentDepartment)    
    }, [currentDepartment]);

    useEffect(() => {        
        if (currentDepartment) {
            // Check after a department gets deleted
            if (Object.values(departments).length < filteredDepts.length) {
                // temp = if the current selected department exists in the redux store anymore
                let temp = departments[currentDepartment.deptId]

                // Reset the <select> if temp doesn't exist anymore
                if (!temp)      setCurrentDepartment(null)
            } 
        }

        handleFilterDepts()
        logger.info("filtered Departments: ", filteredDepts)
    }, [departments]);

    useEffect(() => {
        handlePermissionCheck()
        dispatch(DeptAPI.fetchDepartment())
      }, []);

    return (
        <>
        <div className="mb-4 mb-md-0">
            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item active>Dept. Management</Breadcrumb.Item>
            </Breadcrumb>

            <h2>Department Management</h2>
            <p className="mb-4">Here are your departments.</p>

            <div className="d-flex justify-content-between">

            {/* Department Actions */}
            <ButtonGroup className="assetBar">
                <Button id="actionBar2" variant="outline-primary" size="sm" onClick={() => {setOpenAdd(true)}}>
                    <AddCircle />
                    Create Department
                </Button>
            </ButtonGroup>         
            </div>
        </div>

        <div className="form-control">
            <Form.Label>Departments:</Form.Label>
            <div style={{display: 'flex', alignItems:'center'}}>
                <Form.Select className="profileSelector" required 
                    id="deptSelector"
                    name="deptSelector"
                    onChange={(e) => handleView(e)}
                    onKeyDown={(e) => disableKey(e)}
                    value={currentDepartment ? currentDepartment.deptId : ""}
                >
                    <option value="">--Select a department--</option>
                    
                    {filteredDepts.map(department => {

                        if(department.deptId !== AccessActions.DEFAULT_DEPT_DOMAIN_ID){
                            return(
                                <option key={department.deptId} value={department.deptId}>
                                    {department.deptName1}
                                </option>
                            )
                        }
                        else{return null}
                    })}
                </Form.Select>
            </div>

            {/* Only shown when a department is selected */}
            {currentDepartment &&
            <>  <hr />  

                <div className="d-flex justify-content-between">
                {/* Department Actions */}
                <ButtonGroup className="assetBar">
                    {/* TODO: test the interaction of (deleting departments / scale profiles) + departments / scales being assigned to them. */}
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={() => {setOpenDelete(true)}}>
                        <Delete />
                        Delete Department
                    </Button>
                </ButtonGroup>         
                </div>

                <br />
                
                <div>
                <h4 className="" style={{color: "black"}}>Current Department Information:</h4>

                <div className="roleContainer">
                    <p>Department Name: <span style={{fontWeight: "bold", color : "#ba4735"}}>{currentDepartment.deptName1}        </span></p>
                    <p>Current Profile: <span style={{fontWeight: "bold", color : "#ba4735"}}>{currentProfile ? currentProfile.name : "Not set"} </span></p>
                    <p>Description:     <span style={{fontWeight: "bold", color : "#ba4735"}}>{currentDepartment.deptName2 ? currentDepartment.deptName2 : "Not set"} </span></p>
                    <p>Current Domain:  <span style={{fontWeight: "bold", color : "#ba4735"}}>{
                        currentDepartment === null ? "No department currently selected" : searchDomain(currentDepartment.requiresDomainId)
                    } </span></p>
                </div>
                <hr />

                <PutDepartment
                    title = ""
                    treeElement={currentDepartment}
                />

                    {/* w/ Notification working */}
                    {/* <Form.Label>Department Profile:</Form.Label> 
                    <Form.Select id="profileSelector" className="profileSelector" style={{margin: 5,backgroundColor: "transparent"}}
                        onChange={(e) => handleChangeProfile(e)}
                        onKeyDown={(e) => disableKey(e)}
                        onClick={(e)=> e.stopPropagation()}
                        value={currentDepartment === null ? "" : currentDepartment.defaultProfileId ?? ""}
                    >
                        <option value="" disabled>--Select a profile--</option> : null
                        
                        {profiles.map(profile => (
                            <option key={profile.profileId} value={profile.profileId}>
                                {profile.name}
                            </option>
                        ))}
                    </Form.Select> */}
                </div>
            </>}
        </div>

        {openAdd &&
            <CreateDepartment
                title="Create a new Department"
                openPopup={openAdd}
                setOpenPopup={setOpenAdd}
            />
        }

        {openDelete &&
            <DeleteDepartment
                title={"Delete Department"}
                openPopup={openDelete}
                setOpenPopup={setOpenDelete}
                department={currentDepartment ?? getDefaultDepartment()}
                setCurrentDepartment={setCurrentDepartment}
            />
        }

        </>   
    );
}
