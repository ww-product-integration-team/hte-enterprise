import React, { useState} from 'react';
import { MenuItem, ListItemIcon, ListItemText} from '@material-ui/core';
import { PutBanner, DeleteBanner } from '../forms/BannerForm';
import { PostRegion, PutRegion, DeleteRegion } from '../forms/RegionForm';
import { PostStore, PutStore, DeleteStore } from '../forms/StoreForm';
import { PostDepartment, PutDepartment, RemoveDepartment } from '../forms/DepartmentForm';
import { PostScale, DeleteScale, RemoveScale } from '../forms/ScaleForm';
import {Info, Edit, RemoveCircle, AddCircle} from '@mui/icons-material'
import { ASSIGN_SCALES, FULL_TREEVIEW, MONITOR } from '../../state/actions/accessActions';
import 'status-indicator/styles.css'
import { useAppSelector } from '../../state';
import RemoveShoppingCartIcon from '@mui/icons-material/RemoveShoppingCart';
import { Node, lazyNode } from '../../types/asset/TreeTypes';
import AccessControl from '../common/AccessPermissions';
import { getDefaultDevice, ScaleDevice } from '../../types/asset/ScaleTypes';
import { AssetTypes } from '../../types/asset/AssetTypes';

export default function GetTreeItemOptions(treeElement : lazyNode | Node, 
                                            handleShowDetails : (event : React.MouseEvent<HTMLElement, MouseEvent>, nodeId : string) => void,
                                            handleAssetMenuOpen : (event : React.MouseEvent<HTMLButtonElement, MouseEvent>, nodeId : string) => void,
                                            handleAssetMenuClose : (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => void,
                                            setScales : React.Dispatch<React.SetStateAction<ScaleDevice[]>>,
                                            anchorEl ?: Element | null | undefined,
                                            setAnchorEl ?: React.Dispatch<React.SetStateAction<Element | null | undefined>>) {

    const [openAdd, setOpenAdd] = useState<AssetTypes | null>(null);              // POST   Request
    const [openEdit, setOpenEdit] = useState<AssetTypes | null>(null);            // PUT    Request
    const [openDelete, setOpenDelete] = useState<AssetTypes | null>(null);        // DELETE Request
    const [openRemove, setOpenRemove] = useState<AssetTypes | null>(null);        // REMOVE Request
    const user = useAppSelector((state) => state.access.account);
    const roles = useAppSelector((state) => state.access.roles);

    switch(treeElement.type){
    case 'banner':
        return (
        <div className="assetOptions">
            <AccessControl user = {user} requiredRole = {roles[FULL_TREEVIEW]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenAdd(AssetTypes.Region);
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <AddCircle />
                    </ListItemIcon>
                    <ListItemText primary="Add Region"/>

                    <PostRegion
                        title="Add Region"
                        openPopup={openAdd === AssetTypes.Region}
                        setOpenPopup={setOpenAdd}
                        treeElement={treeElement}
                    />
                </MenuItem>

                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenEdit(AssetTypes.Banner); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <Edit />
                    </ListItemIcon>
                    <ListItemText primary="Edit Banner"/>
                
                    <PutBanner
                        title="Edit a Banner"
                        openPopup={openEdit === AssetTypes.Banner}
                        setOpenPopup={setOpenEdit}
                        treeElement={treeElement}
                    />
                </MenuItem>

                <MenuItem disabled={treeElement["hasChildren"]}  className='deleteBanner' onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenDelete(AssetTypes.Banner);
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <RemoveCircle />
                    </ListItemIcon>
                    <ListItemText primary="Delete Banner"/>
                
                    <DeleteBanner
                        title="Delete a Banner"
                        openPopup={openDelete === AssetTypes.Banner}
                        setOpenPopup={setOpenDelete}
                        treeElement={treeElement}
                    />
                </MenuItem>
            </AccessControl>

            {/* <ExpandNext/> */}

            {/* <ExpandAll/> */}

        </div>
        )
    case 'region':
        return (
        <div>

            <AccessControl user = {user} requiredRole = {roles[FULL_TREEVIEW]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenAdd(AssetTypes.Store); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <AddCircle />
                    </ListItemIcon>
                    <ListItemText primary="Add Store"/>
                
                    <PostStore
                        title="Add a Store"
                        openPopup={openAdd === AssetTypes.Store}
                        setOpenPopup={setOpenAdd}
                        treeElement={treeElement}
                    />
                </MenuItem>

                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenEdit(AssetTypes.Region); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <Edit />
                    </ListItemIcon>
                    <ListItemText primary="Edit Region" />
                
                    <PutRegion
                        title="Edit Region"
                        openPopup={openEdit === AssetTypes.Region}
                        setOpenPopup={setOpenEdit}
                        treeElement={treeElement}
                    />
                </MenuItem>

                <MenuItem disabled={treeElement["hasChildren"]} onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenDelete(AssetTypes.Region); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <RemoveCircle />
                    </ListItemIcon>
                    <ListItemText primary="Delete Region"/>
                
                    <DeleteRegion
                        title="Delete Region"
                        openPopup={openDelete === AssetTypes.Region}
                        setOpenPopup={setOpenDelete}
                        treeElement={treeElement}
                    />
                </MenuItem>
            </AccessControl>

            {/* <ExpandNext/> */}

            {/* <ExpandAll/> */}
        </div>
        )
    case 'store':
        return(
        <div>
            <AccessControl user = {user} requiredRole = {roles[MONITOR]}>
                <MenuItem onClick={(e) => {
                    handleShowDetails(e, treeElement.id)
                    handleAssetMenuClose(e)
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <Info />
                    </ListItemIcon>
                    <ListItemText primary="View Details"/>
                </MenuItem>
            </AccessControl>

            <AccessControl user = {user} requiredRole = {roles[FULL_TREEVIEW]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenAdd(AssetTypes.Department); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <AddCircle />
                    </ListItemIcon>
                    <ListItemText primary="Add Department"/>
                
                    <PostDepartment
                        title="Add a Department"
                        openPopup={openAdd === AssetTypes.Department}
                        setOpenPopup={setOpenAdd}
                        storeElement = {treeElement}
                    />
                </MenuItem>

                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenEdit(AssetTypes.Store);
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <Edit />
                    </ListItemIcon>
                    <ListItemText primary="Edit Store"/>
                
                    <PutStore
                        title="Edit Store"
                        openPopup={openEdit === AssetTypes.Store}
                        setOpenPopup={setOpenEdit}
                        treeElement={treeElement}
                    />
                </MenuItem>

                <MenuItem disabled={treeElement["hasChildren"]} onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenDelete(AssetTypes.Store); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <RemoveCircle />
                    </ListItemIcon>
                    <ListItemText primary="Delete Store"/>

                    <DeleteStore
                        title="Delete Store"
                        openPopup={openDelete === AssetTypes.Store}
                        setOpenPopup={setOpenDelete}
                        treeElement={treeElement}
                    />
                </MenuItem>
            </AccessControl>

            {/* <ExpandNext/> */}

            {/* <ExpandAll/> */}
        </div>
        )
    case 'dept':
        return(
        <div>
            <AccessControl user = {user} requiredRole = {roles[MONITOR]}>
                <MenuItem onClick={(e) => {
                    handleShowDetails(e, treeElement.id)
                    handleAssetMenuClose(e)
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <Info />
                    </ListItemIcon>
                    <ListItemText primary="View Details"/>
                </MenuItem>
            </AccessControl>
            
            <AccessControl user = {user} requiredRole = {roles[ASSIGN_SCALES]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenAdd(AssetTypes.Scale);
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <AddCircle />
                    </ListItemIcon>
                    <ListItemText primary="Add Scale"/>
                    {openAdd === AssetTypes.Scale && 
                        <PostScale
                            title="Add a Scale"
                            openPopup={openAdd === AssetTypes.Scale}
                            setOpenPopup={setOpenAdd}
                            dept = {treeElement}
                            setAssetScales={setScales}
                        />
                    }
                    
                </MenuItem>
            </AccessControl>

            <AccessControl user = {user} requiredRole = {roles[FULL_TREEVIEW]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenEdit(AssetTypes.Department);
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <Edit />
                    </ListItemIcon>
                    <ListItemText primary="Edit Department"/>

                    {openEdit === AssetTypes.Department ? 
                    <PutDepartment
                        title="Edit Department"
                        openPopup={openEdit === AssetTypes.Department}
                        setOpenPopup={setOpenEdit}
                        treeElement={treeElement}
                    /> : null}
                </MenuItem>
            </AccessControl>

            <AccessControl user = {user} requiredRole = {roles[FULL_TREEVIEW]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenDelete(AssetTypes.Department);
                }}> 
                
                    <ListItemIcon className="optionIcon">
                        <RemoveCircle />
                    </ListItemIcon>
                    <ListItemText primary="Remove Department"/>

                    <RemoveDepartment
                        title={"Remove Department"}
                        openPopup={openDelete === AssetTypes.Department}
                        setOpenPopup={setOpenDelete}
                        treeElement={treeElement}
                    />
                </MenuItem>
            </AccessControl>

            {/* <ExpandNext/> */}

        </div>
        )
    case 'scale':
        return(
        <div>
        
            <AccessControl user = {user} requiredRole = {roles[MONITOR]}>
                <MenuItem onClick={(e) => {
                    handleShowDetails(e, treeElement.id)
                    handleAssetMenuClose(e)
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}>
                    <ListItemIcon className="optionIcon">
                        <Info />
                    </ListItemIcon>
                    <ListItemText primary="View Details"/>
                </MenuItem>
            </AccessControl>

            <AccessControl user = {user} requiredRole = {roles[ASSIGN_SCALES]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenDelete(AssetTypes.Scale); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <RemoveCircle />
                    </ListItemIcon>
                    <ListItemText primary="Delete Scale"/>

                    <DeleteScale
                        title="Delete Scale"
                        openPopup={openDelete === AssetTypes.Scale}
                        setOpenPopup={setOpenDelete}
                        scaleProperties={treeElement.properties ? treeElement.properties : getDefaultDevice()}
                    />
                </MenuItem>
            </AccessControl>

            <AccessControl user = {user} requiredRole = {roles[ASSIGN_SCALES]}>
                <MenuItem onClick={(e) => {
                    handleAssetMenuClose(e)
                    setOpenRemove(AssetTypes.Scale); 
                    if (anchorEl && setAnchorEl) setAnchorEl(null);
                }}> 
                    <ListItemIcon className="optionIcon">
                        <RemoveShoppingCartIcon />
                    </ListItemIcon>
                    <ListItemText primary="Remove Scale"/>

                    <RemoveScale
                        title="Remove Scale"
                        openPopup={openRemove === AssetTypes.Scale}
                        setOpenPopup={setOpenRemove}
                        scaleProperties={treeElement.properties ? treeElement.properties : getDefaultDevice()}
                        setScales={setScales}
                    />
                </MenuItem>
            </AccessControl>

        </div>
        )
    default:
        return(null);
    }
}