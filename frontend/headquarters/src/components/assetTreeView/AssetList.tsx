import { useState, useEffect, useRef } from 'react';

import { Link, useHistory } from 'react-router-dom';

import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { Block, ContactlessOutlined, DeleteForever, Help, Warning } from '@material-ui/icons';
import VisibilityIcon from '@mui/icons-material/Visibility';
import RefreshIcon from '@mui/icons-material/Refresh';
import logger from '../utils/logger'

import DepartmentIcon from '@material-ui/icons/LocalGroceryStoreOutlined';
import RegionIcon from '@material-ui/icons/RoomOutlined';
import BannerIcon from '@material-ui/icons/Flag';
import StoreIcon from '@material-ui/icons/Store';
import ScaleIcon from '../icons/ScaleIcon';

//Tool Bar Forms and Icons
import OpenInNewOutlinedIcon from '@mui/icons-material/OpenInNewOutlined';
import ParkOutlinedIcon from '@mui/icons-material/ParkOutlined';
import TableViewIcon from '@mui/icons-material/TableView';
import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import Offcanvas from 'react-bootstrap/Offcanvas';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

import { PostBanner } from '../forms/BannerForm';
import { Button, ButtonGroup, Breadcrumb, Col, Row, Card, } from '@themesberg/react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { NotificationDetails } from '../assetDetails/NotificationDetails';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';


//Redux and State
import { useDispatch } from "react-redux"
import { DEFAULT_ADMIN_DOMAIN_ID, FULL_TREEVIEW } from "../../state/actions/accessActions"
import AccessControl from "../common/AccessPermissions"
import { BannerAPI, DeptAPI, RegionAPI, ScaleAPI, StoreAPI, ProfileAPI, AssetAPI, NodeStatusTimerAPI } from '../api/index';

//CSS
import 'antd/dist/antd.min.css';
import 'status-indicator/styles.css'
import '../assetTreeView/AssetListStyle.css'
import Alert from '@mui/material/Alert';

//Table
import { HeadCellTypes } from '../common/virtualTable/VirtualTable';
import VirtualTableProps from '../common/virtualTable/VirtualTable';

//Tree
import { Tree } from 'antd';
import { CaretDownFilled } from '@ant-design/icons';
import StyledTreeNode from './StyledTreeItem';
import { AccessActions, AssetActions, useAppSelector } from '../../state';
import { useRspHandler } from '../utils/ResponseProvider';
import { CircularProgress, Dialog, DialogContent, DialogTitle, IconButton, Menu, MenuItem, Tooltip, Typography } from '@material-ui/core';
import { PostRegion } from '../forms/RegionForm';
import { PostStore } from '../forms/StoreForm';
import { PostDepartment } from '../forms/DepartmentForm';
import { PostScale } from '../forms/ScaleForm';
import { UnassignedScale } from '../forms/UnassignedScaleForm'
import { lazyNode } from '../../types/asset/TreeTypes';
import StatusIndicator from '../dashboard/widgets/StatusIndicator';
import { EventDataNode } from 'antd/lib/tree';
import { Key } from 'antd/lib/table/interface';

import { AssetTypes } from '../../types/asset/AssetTypes';
import { ResponseType } from '../../types/storeTypes';
import { RemoveCircle } from '@mui/icons-material';
import { Dropdown, DropdownButton, Form, OffcanvasBody } from 'react-bootstrap';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import UpgradeSelection from '../upgradeManager/UpgradeForm';

import { Sidebar, filter } from './Sidebar';
import * as FilterAPI from '../api/ui-controller/filterAPI';
// import {format} from 'date-fns'
import { useForm } from 'react-hook-form';

import { MakeQuerablePromise } from '../upgradeManager/UpgradeManager';
import { FilterCard } from './supportingFiles/filterCard';
// import { compareArrays } from '../utils/utils';

import dayjs from "dayjs"


/*Compare arrays
compare string arrays positionally and determines if any strings do not match
This is created because typescript compares by address and not by value
*/
export function compareArrays(firstArray: string[], secondArray: string[]) {
    if (firstArray.length !== secondArray.length) {
        return false
    }
    for (let pos = 0; pos < firstArray.length; pos++) {
        if (firstArray[pos] !== secondArray[pos]) {
            return false
        }
    }
    return true
}

const { TreeNode } = Tree;

function AssetList() {
    const dispatch = useDispatch();

    const treeRef = useRef<any>();
    const [openAddEntity, setOpenAddEntity] = useState<AssetTypes | null>(null);  // POST Request for Banner
    const [openBatchUpgrade, setOpenBatchUpgrade] = useState(false)
    const [openManualUpgrade, setOpenManualUpgrade] = useState(false)
    const [openUnassignedScales, setOpenUnassignedScales] = useState(false)

    //Alerts
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const [openViewNotDetails, setOpenViewNotDetails] = useState(false);
    const [notificationDetails, setNotificationDetails] = useState({})
    const { callbacks, addCallback } = useRspHandler();

    //Links
    const history = useHistory();

    //Redux
    const depts = useAppSelector((state) => state.asset.depts);
    const stores = useAppSelector((state) => state.asset.stores);
    const regions = useAppSelector((state) => state.asset.regions);
    const banners = useAppSelector((state) => state.asset.banners);
    const reduxLazyTree = useAppSelector(state => state.asset.lazyTree)
    const reduxCriticalDialog = useAppSelector((state) => state.asset.criticalDialog)
    const reduxNodeStatusTimers = useAppSelector((state) => state.asset.nodeStatusUpdaterTimers)
    const [criticalDialog, setCriticalDialog] = useState(reduxCriticalDialog)
    const [lazyTreeData, setLazyTreeData] = useState<{ [id: string]: lazyNode }>(reduxLazyTree ? JSON.parse(JSON.stringify(reduxLazyTree)) : {})

    const reduxLazyTreeKeys = useAppSelector((state) => state.asset.lazyExpanded)
    const [lazyExpandedKeys, setLazyExpandedKeys] = useState<string[]>([...reduxLazyTreeKeys])

    const reduxLazyFilterTree = useAppSelector((state) => state.asset.lazyFilterTree)
    const [lazyFilterTree, setLazyFilterTree] = useState<{ [id: string]: lazyNode }>(JSON.parse(JSON.stringify(reduxLazyFilterTree)))

    const reduxLazyFilterTreeKeys = useAppSelector((state) => state.asset.lazyFilterExpanded)
    const [lazyFilterExpandedKeys, setLazyFilterExpandedKeys] = useState<string[]>()

    const reduxFilteredAssets = useAppSelector(state => state.asset.filteredAssets)
    const [filteredAssets, setFilteredAssets] = useState({ ...reduxFilteredAssets })

    const reduxFilters = useAppSelector((state) => state.asset.filterArray)
    const [filters, setFilters] = useState<{ [key: string]: filter }>(JSON.parse(JSON.stringify(reduxFilters)))
    
    let lastUpdateTime = reduxNodeStatusTimers !== undefined && reduxNodeStatusTimers != null ?
                        dayjs((reduxNodeStatusTimers["lastUpdate"])).format("MM/DD/YYYY hh:mm A") : "N/A"


    let nextUpdateTime = reduxNodeStatusTimers !== undefined && reduxNodeStatusTimers != null ?
                        dayjs((reduxNodeStatusTimers["nextUpdate"])).format("MM/DD/YYYY hh:mm A") : "N/A"


    const [loadingPage, setLoadingPage] = useState(true)
    const reduxPageView = useAppSelector((state) => state.asset.pageView);
    const [pageView, setPageView] = useState((reduxPageView ? reduxPageView : false))
    useEffect(() => {
        setPageView(reduxPageView)
    }, [reduxPageView])

    const user = useAppSelector((state) => state.access.account);
    const roles = useAppSelector((state) => state.access.roles);
    const domains = useAppSelector((state) => state.access.domains);
    const focused = useAppSelector((state) => state.asset.focused);
    const selected = useAppSelector((state) => state.asset.selected);

    const [scales, setScales] = useState<ScaleDevice[]>([])
    const [selectedRows, setSelectedRows] = useState<string[]>([])

    useEffect(() => {
        // refreshLazytree if any nodes have been updated
        dispatch(AssetActions.setLazyExpandedTreeItems(lazyExpandedKeys))
    }, [lazyExpandedKeys])
    useEffect(() => {
        // refreshLazyFiltertree if any nodes have been updated
        dispatch(AssetActions.setLazyFilterExpandedTreeItems(lazyFilterExpandedKeys))
    }, [lazyFilterExpandedKeys])

    useEffect(() => {

        if (reduxLazyFilterTreeKeys && lazyFilterExpandedKeys && !compareArrays(reduxLazyFilterTreeKeys, lazyFilterExpandedKeys as string[])) {
            if (reduxLazyFilterTreeKeys && reduxLazyFilterTreeKeys.length > 0) {
                setLazyFilterExpandedKeys([...reduxLazyFilterTreeKeys])
            } else {
                setLazyFilterExpandedKeys([])
            }
        }
    }, [reduxLazyFilterTreeKeys])
    useEffect(() => {
        setLazyTreeData(reduxLazyTree)
    }, [reduxLazyTree])
    useEffect(() => {
        setLazyFilterTree(reduxLazyFilterTree)
    }, [reduxLazyFilterTree])
    useEffect(() => {
        setCriticalDialog(reduxCriticalDialog)
    }, [reduxCriticalDialog])

    const [timestamp, setTimeStamp] = useState(0)
    useEffect(() => {
        setTimeStamp(Date.now())
        dispatch(BannerAPI.fetchBanners())
        dispatch(RegionAPI.fetchRegions())
        dispatch(StoreAPI.fetchStores())
        dispatch(DeptAPI.fetchDepartment())
        dispatch(ProfileAPI.fetchProfiles())
        dispatch(ScaleAPI.fetchApphook())
        dispatch(FilterAPI.fetchFilters())
        dispatch(AssetAPI.fetchFilteredAssets(Date.now()+1))

        function onSuccess() {
            setLoadingPage(false)
        }
        function onFail() {
            setLoadingPage(false)
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Page Load", onSuccess, onFail)) {
            dispatch(ScaleAPI.fetchScales(newID, undefined, null, null, setScales))
        }
        
        dispatch(NodeStatusTimerAPI.getNodeStatusTimers())
        // dispatch(ScaleAPI.getScaleDevice())
    }, []);

    useEffect(() => {
        // uses a buffer to ensuer no extra dispatches are called
        if (timestamp + 1000 < Date.now()) {
            dispatch(AssetAPI.refreshLazyTree(Date.now(), reduxLazyTreeKeys))
            dispatch(AssetAPI.refreshLazyFilterTree(Date.now(), reduxLazyFilterTreeKeys))
            dispatch(FilterAPI.fetchFilters(Date.now()))
            setTimeStamp(Date.now())
        }
        if (timestamp === 0) {
            setTimeStamp(Date.now())
        }
    }, [scales, depts, stores, regions, banners, domains])

    const [refresher, setRefresher] = useState(true)
    useEffect(() => {
        if (refresher) {
            setRefresher(false)
            const refreshPromise: Promise<any> = new Promise((resolve, reject) => {
                setTimeout(() => resolve("done"), 60000)
            }).then(() => {
                // Also causes  dispatch(AssetAPI.refreshLazyTree(Date.now(), reduxLazyTreeKeys)) to occur by useEffect
                dispatch(BannerAPI.fetchBanners())
                dispatch(RegionAPI.fetchRegions())
                dispatch(ScaleAPI.fetchScales(timestamp, undefined, null, null, setScales))
                // refresh symbol uses scale isFetching
                dispatch(StoreAPI.fetchStores())
                dispatch(DeptAPI.fetchDepartment())
                dispatch(ProfileAPI.fetchProfiles())
                dispatch(ScaleAPI.fetchApphook())
                dispatch(FilterAPI.fetchFilters())
                dispatch(NodeStatusTimerAPI.getNodeStatusTimers())
            })

            let refreshStatus = MakeQuerablePromise(refreshPromise)
            refreshStatus.then(() => {
                setRefresher(true)
            })
        }
    }, [refresher])

    //The data shown in the tree, filtered from the treeState

    useEffect(() => {
        setLazyTreeData({ ...reduxLazyTree }); // Can See the full tree
    }, [reduxLazyTree])

    useEffect(() => {
        setLazyFilterTree({ ...reduxLazyFilterTree }); // Can See the full tree
    }, [reduxLazyFilterTree])

    useEffect(() => {
        setFilteredAssets({ ...reduxFilteredAssets })
    }, [reduxFilteredAssets])

    useEffect(() => {
        // for(let filterKey of Object.keys(reduxFilters)){
        //     let filter = reduxFilters[filterKey]
        //     if(filter && filter.filterId)
        //         dispatch(AssetAPI.fetchFilterTreePart(Date.now(), filter.filterId)) // swap out for refreshLazyFilterTree
        // }
        setFilters({ ...reduxFilters })
    }, [reduxFilters])

    const [anchorEl, setAnchorEl] = useState(null)
    const isMenuOpen = Boolean(anchorEl);                   // Show/hide profile menu (desktop)
    const handleMenuClose = () => { setAnchorEl(null) };
    const handleMenuOpen = (event) => { setAnchorEl(event.currentTarget) };
    const renderMenu = (
        <Menu
            id={"addEntityMenu"}
            anchorEl={anchorEl}
            open={isMenuOpen}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'center' }}
            getContentAnchorEl={null}
            keepMounted
            onClose={handleMenuClose}
        >

            <MenuItem onClick={() => {
                setOpenAddEntity(AssetTypes.Banner)
                handleMenuClose()
            }}>
                <BannerIcon style={{ marginRight: "8px" }} />
                Add Banner

                <PostBanner
                    title="Add a Banner"
                    openPopup={openAddEntity === AssetTypes.Banner}
                    setOpenPopup={setOpenAddEntity}
                />

            </MenuItem>

            <MenuItem onClick={() => {
                setOpenAddEntity(AssetTypes.Region)
                handleMenuClose()
            }}>
                <RegionIcon style={{ marginRight: "8px" }} />
                Add Region

                <PostRegion
                    title="Add Region"
                    openPopup={openAddEntity === AssetTypes.Region}
                    setOpenPopup={setOpenAddEntity}
                />

            </MenuItem>

            <MenuItem onClick={() => {
                setOpenAddEntity(AssetTypes.Store)
                handleMenuClose()
            }}>
                <StoreIcon style={{ marginRight: "8px" }} />
                Add Store

                <PostStore
                    title="Add a Store"
                    openPopup={openAddEntity === AssetTypes.Store}
                    setOpenPopup={setOpenAddEntity}
                />

            </MenuItem>

            <MenuItem onClick={() => {
                setOpenAddEntity(AssetTypes.Department)
                handleMenuClose()
            }}>
                <DepartmentIcon style={{ marginRight: "8px" }} />
                Add Department

                <PostDepartment
                    title="Add a Department"
                    openPopup={openAddEntity === AssetTypes.Department}
                    setOpenPopup={setOpenAddEntity}
                />

            </MenuItem>

            <MenuItem onClick={() => {
                setOpenAddEntity(AssetTypes.Scale)
                handleMenuClose()
            }}>
                <ScaleIcon style={{ marginRight: "8px" }} />
                Add Scale
                {openAddEntity === AssetTypes.Scale &&
                    <PostScale
                        title="Add a Scale"
                        openPopup={openAddEntity === AssetTypes.Scale}
                        setOpenPopup={setOpenAddEntity}
                        setAssetScales={setScales}
                    />
                }
            </MenuItem>

        </Menu>
    );

    useEffect(() => {

        let promise = new Promise((resolve, reject) => {
            setTimeout(resolve, 1000)
        })
            .then(() => {

                try {
                    if (focused != null && focused !== "" && treeRef) {
                        treeRef!.current.scrollTo({ key: focused, align: "bottom", offset: 200 })
                    }
                }
                catch (e) {
                    logger.error("ERROR Scrolling", e)
                }

            })


    }, [focused])
    function uniq(a) {
        var seen = {};
        return a.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

    // function createNotDetails(status : string, message : string, desc : string, operation : string){
    //     return(
    //     {
    //         'notification': {
    //             status: status,
    //             message: message
    //         },
    //         'response': {response: 
    //                     {
    //                         operation: operation, 
    //                         result: status,
    //                         errorDescription: desc,
    //                         timestamp: format(new Date(), "PPpp")
    //                     },
    //         }
    //     }
    //     )
    // }

    // function postDragAction(dragType : number, newParent?: lazyNode,dragItem ?: lazyNode){
    //     if(!dragItem || !newParent){
    //         createNotDetails('error', 'Drag Error', 
    //                         "System could not complete the requested action, something went wrong", "Undefined Move")
    //         return false
    //     }

    //     if(dragType === 3 && dragItem.type !== newParent.type){
    //     setNotificationDetails(
    //         createNotDetails('error', 'Unsupported drag action', 
    //                         "This action is currently not supported, parent mismatch",
    //                         'Move ' + dragItem.type)
    //     )
    //     setOpenViewNotDetails(true)
    //     return false;
    //     }

    //     function onFail(response : ResponseType){
    //         logger.error("(DragScale) Error assigning scale", response)
    //     }

    //     switch(dragItem.type){
    //         case 'banner':
    //         case 'region':
    //         case 'store':
    //         case 'dept':
    //         setNotificationDetails(
    //             createNotDetails('error', 'Unsupported drag action', 
    //                             "This action is currently not supported, you can only drag scales at this time",
    //                             'Move ' + dragItem.type)
    //         )
    //         setOpenViewNotDetails(true)
    //         return false;
    //         case 'scale':
    //         if(newParent.type === 'dept'){

    //             const location = newParent.id.split("|")
    //             if(location.length !== 2){
    //             setNotificationDetails(
    //                 createNotDetails('error', 'Unsupported drag action', 
    //                                 "You may only move scales into other departments",
    //                                 'Move ' + dragItem.type)
    //             )
    //             setOpenViewNotDetails(true)
    //             return false
    //             }
    //             const newID = Date.now()
    //             if(dragItem.properties){
    //                 dispatch(ScaleAPI.setScaleAssignment(newID, [dragItem.id], dragItem.properties.storeId, 
    //                                                 location[0], 
    //                                                 location[1]))
    //                 return true
    //             }
    //             else{
    //                 createNotDetails('error', 'Invalid Drag Item', 
    //                                 "Dragged Asset is missing necessary properties to ccomplete this action please contact Hobart Support!",
    //                                 'Move ' + dragItem.type)
    //                 return false
    //             }                                  
    //         }
    //         else if(newParent.type === 'scale'){

    //             const newID = Date.now()
    //             if(dragItem.properties && newParent.properties){
    //                 if(addCallback(callbacks, newID, "Assign Scale", onSuccess, onFail, {failMsg:"default"})){
    //                     dispatch(ScaleAPI.setScaleAssignment(newID, [dragItem.id], dragItem.properties.storeId, 
    //                         newParent.properties.storeId, 
    //                         newParent.properties.deptId))
    //                 }
    //             }
    //             else{
    //                 createNotDetails('error', 'Invalid Drag Item', 
    //                                 "Asset is missing necessary properties to ccomplete this action please contact Hobart Support!",
    //                                 'Move ' + dragItem.type)
    //                 return false
    //             }

    //         }
    //         else{
    //             setNotificationDetails(
    //             createNotDetails('error', 'Unsupported drag action', 
    //                                 "You may only move scales into other departments",
    //                                 'Move ' + dragItem.type)
    //             )
    //             setOpenViewNotDetails(true)
    //             return false
    //         }
    //         return false



    //         default: 
    //         return false
    //     }
    // }
    // used for filters
    // const onExpand = (expandedKeys: Key[], info: {
    //         node: EventDataNode;
    //         expanded: boolean;
    //         nativeEvent: MouseEvent;
    //     }) => {
    //     setExpandedKeys(expandedKeys as string[])
    //     dispatch(AssetActions.setExpandedTreeItems(expandedKeys as string[]))       
    // };

    function getNode(newPath, lazyTree: { [id: string]: lazyNode }) {

        let path = [...newPath.split(",")]
        let node = lazyTree[path[0]]  //get banner 
        path = path.splice(1, path.length)
        for (let pathPart of path) {
            node = node.children[pathPart]
        }
        return node
    }
    //used for main asset tree
    const onLazyExpand = (expandedKeys: Key[], info: {
        node: EventDataNode;
        expanded: boolean;
        nativeEvent: MouseEvent;

    }) => {
        expandedKeys = uniq(expandedKeys)
        function onSuccess(response: ResponseType) {
            setShowAlert([false, "Got tree!"])
        }
        function onFail(response: ResponseType) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not fetch tree!"])

            // remove parent node and any child nodes from expanded keys
            let newExpandedKeys: string[] = []
            expandedKeys.forEach((key) => { if (key && !key.toString().includes(lazynode.id)) { newExpandedKeys.push(key.toString()) } })
            setLazyExpandedKeys(newExpandedKeys as string[])
            dispatch(AssetActions.setLazyExpandedTreeItems(newExpandedKeys as string[]))
        }

        let stringPath = info.node.key
        // find node in treeData
        let lazyTree = JSON.parse(JSON.stringify(lazyTreeData))
        let lazynode = getNode(stringPath, lazyTree)
        if (lazynode && lazynode.children && Object.keys(lazynode.children).length > 0) {
            //closing node
            lazynode.children = {}
            dispatch(AssetActions.setLazyTree({ ...lazyTree }))

            // remove parent node and any child nodes from expanded keys
            let newExpandedKeys: string[] = []
            expandedKeys.forEach((key) => {
                if (key && !key.toString().includes(lazynode.id)) {
                    newExpandedKeys.push(key.toString())
                }
            })
            setLazyExpandedKeys(newExpandedKeys as string[])
            dispatch(AssetActions.setLazyExpandedTreeItems(newExpandedKeys as string[]))
        }
        else {
            //opening node
            let id = Date.now()
            if (addCallback(callbacks, id, "Fetch Tree", onSuccess, onFail, undefined, true)) {
                setLazyExpandedKeys(expandedKeys as string[])
                dispatch(AssetActions.setLazyExpandedTreeItems(expandedKeys as string[]))
                dispatch(AssetAPI.fetchLazyTree(id, stringPath))
            }
        }
    };
    const onLazyFilterExpand = (expandedKeys: Key[], info: {
        node: EventDataNode;
        expanded: boolean;
        nativeEvent: MouseEvent;

    }) => {
        expandedKeys = uniq(expandedKeys)
        function onSuccess(response: ResponseType) {
            setShowAlert([false, "Got tree!"])
        }
        function onFail(response: ResponseType) {
            response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Could not fetch tree!"])

            // remove parent node and any child nodes from expanded keys
            let newExpandedKeys: string[] = []
            expandedKeys.forEach((key) => { if (key && !key.toString().includes(lazynode.id)) { newExpandedKeys.push(key.toString()) } })
            setLazyExpandedKeys(newExpandedKeys as string[])
            dispatch(AssetActions.setLazyFilterExpandedTreeItems(newExpandedKeys as string[]))
        }

        let filterId = (info.node.key as string).split("~")[0]
        let path = info.node.key
        // find lazy node in fitler tree data
        let lazyTree = JSON.parse(JSON.stringify(lazyFilterTree))
        let lazynode = getNode(path, lazyTree)
        if (lazynode && lazynode.children && Object.keys(lazynode.children).length > 0) {
            //close node
            lazynode.children = {}
            dispatch(AssetActions.setLazyFilterTree({ ...lazyTree }))

            // remove parent node and any child nodes from expanded keys
            let newExpandedKeys: string[] = []
            expandedKeys.forEach((key) => {
                if (key && !key.toString().includes(lazynode.id)) {
                    newExpandedKeys.push(key.toString())
                }
            })
            setLazyFilterExpandedKeys(newExpandedKeys as string[])
            dispatch(AssetActions.setLazyFilterExpandedTreeItems(newExpandedKeys as string[]))
        }
        else {
            //open node
            let id = Date.now()
            if (addCallback(callbacks, id, "Fetch Tree", onSuccess, onFail, undefined, true)) {
                setLazyFilterExpandedKeys(expandedKeys as string[])
                dispatch(AssetActions.setLazyFilterExpandedTreeItems(expandedKeys as string[]))
                dispatch(AssetAPI.fetchFilterTreePart(id, filterId, path as string))
            }
        }
    };

    const lazyLoop = (data: { [id: string]: lazyNode }) => Object.keys(data).map((itemKey) => {
        let item = data[itemKey]
        if (!item.path) {
            console.log("NODE DOESNT HAVE A PATH")
            return
        }
        let key = item.path.join()
        let node: lazyNode = JSON.parse(JSON.stringify(item))
        if (node.id.includes("~")) {
            node.id = node.id.split("~")[1]
        }
        if (item && item.hasChildren && item["children"] && Object.keys(item.children).length > 0) {
            return <TreeNode expanded={true} className="treeViewNode" key={key} title={
                <StyledTreeNode
                    treeElement={JSON.parse(JSON.stringify(node))}
                    scales={scales}
                    setScales={setScales}
                />
            }>
                {lazyLoop(item.children)}
            </TreeNode>;
        }
        if (item && !item.hasChildren) {
            return <TreeNode className="treeViewNode" key={key} title={
                <StyledTreeNode
                    scales={scales}
                    treeElement={node}
                    setScales={setScales}
                />
            } />
        }
        return (
            <TreeNode className="treeViewNode" key={key} title={
                <StyledTreeNode
                    scales={scales}
                    treeElement={node}
                    setScales={setScales}
                />
            }>
                <TreeNode className="treeViewNode" key={item.id + "fake"}
                    title={
                        <StyledTreeNode
                            scales={scales}
                            treeElement={node}
                            loadingTreeNode
                            setScales={setScales}
                        />
                    }>
                </TreeNode>
            </TreeNode>)
    });

    /*Instantion for Filter Componenents*/
    const [control, setControl] = useState(false)
    const update = () => {
        setControl(!control)
        return (<div />)
    }


    // const reduxFilterArray = useAppSelector((state) => state.asset.filterArray);


    // useEffect(()=>{
    //     function onScaleSuccess(){
    //         console.log("fetched scales")
    //     }
    //     function onFail(){
    //         console.log("fetch scales failed")
    //     }
    //     if(!reduxPageView){
    //         let newID = Date.now()
    //         if (addCallback(callbacks, newID, "Fetch Scales", onScaleSuccess, onFail)) {
    //             dispatch(ScaleAPI.fetchScales(newID))
    //         }
    //     }
    //     else{
    //         dispatch(AssetActions.setScaleList({}))
    //     }
    // },[reduxPageView])
    // const [openImport, setOpenImport] = useState(false);

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);

    const handleShow = () => setShow(true);

    const scaleHeadCells: HeadCellTypes[] = [
        { id: 'checkmark', sorted: false, searchable: false, label: 'checkmark', isShowing: true, mandatory: true, },
        { id: "type", sorted: false, searchable: false, label: 'Type', isShowing: true, mandatory: true, },
        { id: 'ipAddress', sorted: true, searchable: true, label: 'IP Address', isShowing: true, mandatory: true, },
        { id: 'application', sorted: true, searchable: true, label: 'Application Version', isShowing: true, mandatory: false, },
        { id: 'enabled', sorted: false, searchable: false, label: 'Enabled / Disabled', isShowing: true, mandatory: false, },
        { id: 'storeId', sorted: true, searchable: true, label: 'Assigned Store', isShowing: true, mandatory: false, },
        { id: 'deptId', sorted: true, searchable: true, label: 'Assigned Department', isShowing: true, mandatory: false, },
        { id: 'hostname', sorted: false, searchable: false, label: 'Host Name', isShowing: true, mandatory: false, },
        { id: 'totalLabelsPrinted', sorted: true, searchable: false, label: 'Labels Printed', isShowing: true, mandatory: false, },
        { id: 'pluCount', sorted: true, searchable: false, label: 'PLU Count', isShowing: true, mandatory: false, },
        { id: 'lastReportTimestampUtc', sorted: false, searchable: false, label: 'Last Report', isShowing: true, mandatory: false, },
        { id: 'options', sorted: false, searchable: false, label: 'Details', isShowing: true, mandatory: true, },
    ];

    function handleDeleteAllFilters() {
        let id = Date.now()
        function onFilterSuccess() {
            dispatch(FilterAPI.fetchFilters())
            dispatch(AssetActions.setLazyFilterTree({}))
            dispatch(AssetActions.setLazyFilterExpandedTreeItems([]))
            dispatch(NodeStatusTimerAPI.getNodeStatusTimers())
        }
        function onFilterFail() {
            console.log("delete all failed")
        }

        if (addCallback(callbacks, id, "Delete All filter", onFilterSuccess, onFilterFail)) {
            dispatch(FilterAPI.deleteFilter(id))
        }
    }

    useEffect(() => {
        update()
    }, [pageView])

    const getLazyScales = (data: { [id: string]: lazyNode }, tableData: ScaleDevice[], onlySelected = false) => {
        Object.keys(data).map((itemKey) => {
            let item = data[itemKey]
            if (item.children && item.children.length)
                tableData = getLazyScales(item.children, tableData, onlySelected)
            if (item.type === "scale" && item["properties"]) {
                if (onlySelected && selected.length > 0) {
                    if (selected.includes(item["id"])) {
                        tableData.push(item["properties"])
                    }
                } else {
                    tableData.push(item["properties"])
                }
            }
            return tableData
        })
        return tableData
    }

    const findUnassignedScales = () => {
        let unassignedScales = scales.filter(scale =>
            scale.deviceId != null &&
            (scale.storeId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID ||
                scale.deptId === AccessActions.DEFAULT_UNASSIGNED_DOMAIN_ID ||
                scale.storeId === null ||
                scale.deptId === null)
        )
        if (unassignedScales.length > 0) {
            return true
        }
        return false
    }

    // /*used to fix no Data bug on Virtaultable*/
    // useEffect(()=>{
    //     if(Object.keys(filterArray).length === 0)
    //         update()
    // },[filterArray])


    function CustomToggle({ eventKey }) {
        const decoratedOnClick = useAccordionButton(eventKey);

        return (
            <ArrowDropDownIcon
                type="button"
                onClick={decoratedOnClick}
            />
        );
    }

    const mainTree = () => {
        return <Tree
            expandedKeys={lazyExpandedKeys}
            showLine
            height={(Object.keys(filters).length > 0) ? 650 : 5000}
            switcherIcon={<CaretDownFilled />}
            onExpand={onLazyExpand}
            ref={treeRef}
            style={{ paddingBottom: "20px", paddingTop: "10px" }}
        >
            {lazyLoop({ ...lazyTreeData })}
        </Tree>
    }

    const mainTable = (data: ScaleDevice[]) => {
        return (
            <>
                <VirtualTableProps
                    tableName="virtualScaleTable"
                    dataSet={scales}
                    headCells={scaleHeadCells}
                    initialSortedBy={{ name: 'ipAddress' }}
                    dataIdentifier="deviceId"
                    dispatchedUrls={[ScaleAPI.fetchScales]} // does this even work still??
                    saveKey="virtualScaleTableHead"
                    selectedRows={selected}
                    setSelected={setSelectedRows}
                    changeReduxSelected={true}
                    maxHeight='600px'
                />
                <br />
                <h5>Scales Found : {data.length}</h5>
            </>
        )
    }

    const filterTrees = () => {
        let filterJSX = [<div />]
        for (let filterId of Object.keys(filters)) {
            let filterData = {}
            for (let filterPartKey of Object.keys(lazyFilterTree)) {
                if (filterPartKey.includes(filterId)) {
                    filterData[filterPartKey] = lazyFilterTree[filterPartKey]
                }
            }
            let filter = filters[filterId]
            if (filter && filterData && Object.keys(filterData).length > 0) {
                filterJSX.push(
                    <FilterCard filter={filter}>
                        <Tree
                            expandedKeys={lazyFilterExpandedKeys}
                            showLine
                            height={(Object.keys(filters).length > 0) ? 650 : 5000}
                            switcherIcon={<CaretDownFilled />}
                            onExpand={onLazyFilterExpand}
                            ref={treeRef}
                            style={{ paddingBottom: "20px", paddingTop: "10px" }}
                        >
                            {lazyLoop({ ...filterData })}
                        </Tree>
                    </FilterCard>
                )
            }
        }
        return (
            <>
                {(Object.keys(filters).length > 0) ?
                    filterJSX
                    :
                    null
                }
            </>
        )
    }
    const filterTables = () => {
        let filterJSX = [<div />]
        for (let filterId of Object.keys(filteredAssets)) {
            let filter = filters[filterId]
            let assets = filteredAssets[filterId]
            if (filter && assets && assets.length > 0) {
                filterJSX.push(
                    <FilterCard filter={filter}>
                        <VirtualTableProps
                            tableName="virtualScaleFilterTable"
                            dataSet={assets}
                            headCells={scaleHeadCells}
                            initialSortedBy={{ name: 'ipAddress' }}
                            dataIdentifier='deviceId'
                            saveKey="virtualScaleFilterTableHead"
                            selectedRows={selected}
                            setSelected={setSelectedRows}
                            changeReduxSelected={true}
                            maxHeight="400px"
                        />
                    </FilterCard>
                )
            }
        }
        return (
            <>
                {/*Table view */}
                {(Object.keys(filters).length > 0) ?
                    filterJSX
                    :
                    null
                }
            </>
        )
    }

    interface UserPassword {
        password: string
    }
    const { register, handleSubmit, reset, formState: { errors } } = useForm<UserPassword>()
    const [openDeleteAll, setOpenDeleteAll] = useState(false)
    // const [openCurrentlyDeletingAll, setOpenCurrentlyDeletingAll] = useState(false)
    const [formType, setFormType] = useState("password")

    useEffect(() => {
        if (criticalDialog) {
            window.onbeforeunload = () => true
            return () => {
                window.onbeforeunload = null;
            }
        }
    }, [criticalDialog]);

    const handleDelete = handleSubmit(async (data: UserPassword) => {
        function onSuccess(response: ResponseType) {
            logger.info("Success deleting all assets")
            //setOpenCurrentlyDeletingAll(false)
            dispatch(AssetActions.setCriticalDialog(""))
            reset({ password: "" })
            dispatch(AssetAPI.refreshLazyTree(Date.now(), lazyExpandedKeys))
            dispatch(ScaleAPI.fetchScales(timestamp, undefined, null, null, setScales))
        }
        function onFail(response: ResponseType) {
            logger.error("Error deleting all assets")
            dispatch(AssetActions.setCriticalDialog(""))
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "DeleteAllAssets", onSuccess, onFail)) {
            setOpenDeleteAll(false)
            dispatch(AssetActions.setCriticalDialog("Delete All"))
            dispatch(AssetAPI.deleteAllAssets(newID))
        }
    })

    const handleCloseCritical = (event, reason) => {
        if (reason && reason === "backdropClick")
            return;
        dispatch(AssetActions.setCriticalDialog(""))
    }

    return (
        <>
            <div className="mainContent">
                <Offcanvas show={show} placement="end" style={{ outerHeight: "auto" }} onHide={handleClose}>
                    <Offcanvas.Header closeButton>
                        <Offcanvas.Title>Not Rendered</Offcanvas.Title>
                    </Offcanvas.Header>
                    <OffcanvasBody>
                        <h2 style={{ marginLeft: "8px", alignItems: "center", justifyContent: 'center' }}>
                            Filters
                            <Tooltip className="info" placement="top" style={{ marginLeft: ".85rem" }}
                                title={
                                    <Typography>
                                        Filter options are created by the list of assigned scales.
                                        If you think you are missing options or scales, please assign all scales before filtering
                                    </Typography>}>
                                <Help />
                            </Tooltip>
                        </h2>
                        <Sidebar
                            handleClose={handleClose}
                            scales={scales}
                            setScales={setScales}
                        />
                    </OffcanvasBody>
                </Offcanvas>
                <Row>
                    <Col>
                        <div className="assetBar ">
                            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                                <Breadcrumb.Item active>Asset List </Breadcrumb.Item>
                            </Breadcrumb>
                            <Row>
                                <Col>
                                    <div className='d-flex'>
                                        <h2>Asset List</h2>
                                        {loadingPage &&
                                            <span style={{marginLeft: "2rem"}}>
                                                <CircularProgress />
                                                <Typography variant="body1" component="span" style={{marginLeft: "0.5rem", position: "relative", top: "-0.9rem"}}>Loading data. Please wait...</Typography>
                                            </span>
                                        }
                                    </div>
                                    {(pageView) ?
                                        <p className="mb-4">All assets in a tree format.</p>
                                        :
                                        <p className="mb-4">All assets in a table format.</p>
                                    }
                                </Col>
                                <Col>
                                    <Col>
                                        <Tooltip className="info" placement="bottom" title={<Typography>This data indicates when the asset tree will automatically receive the latest scale data. Scale heartbeats will still occur per their schedules.</Typography>}>
                                            <IconButton aria-label="enableAccount" disableRipple>
                                                <Help />
                                            </IconButton>
                                        </Tooltip>
                                    </Col>
                                    <Row>
                                        <span>{"Last scale status update: " + lastUpdateTime}</span>
                                    </Row>
                                    <Row>
                                        <span>{"Next scale status update: " + nextUpdateTime}</span>
                                    </Row>
                                </Col>
                                <Col style={{ maxWidth: '335px' }}>
                                    <Card border="light" className="shadow-sm" style={{ padding: '0.1rem 0.1rem' }}>
                                        <Card.Body>
                                            <Row>
                                                <Col style={{ maxWidth: '100px' }}>
                                                    <h5>Legend</h5>
                                                </Col>

                                                <Col>
                                                    <p style={{ textAlign: 'left', marginBottom: '0px' }}><StatusIndicator positive /> Online/Good</p>
                                                    <p style={{ textAlign: 'left', marginBottom: '0px' }}><StatusIndicator intermediary /> Away/Warning</p>
                                                    <p style={{ textAlign: 'left', marginBottom: '0px' }}><StatusIndicator negative /> Not responding/Error</p>
                                                    <p style={{ textAlign: 'left', marginBottom: '0px' }}><StatusIndicator style={{ backgroundColor: '#a4abb0' }} /> Disabled</p>
                                                </Col>
                                            </Row>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg="2">
                                    {/* Left button section */}
                                    <AccessControl
                                        user={user}
                                        requiredDomain={domains[DEFAULT_ADMIN_DOMAIN_ID]}
                                        requiredRole={roles[FULL_TREEVIEW]}
                                    >
                                        <ButtonGroup>
                                            {/* Role 2003 (FullTreeView) */}

                                            <Button id="actionBar1" variant="outline-primary" size="sm"
                                                onClick={(e) => { handleMenuOpen(e) }}>
                                                <AddCircleOutlineIcon style={{ marginRight: '8px' }} />
                                                Add Entity
                                            </Button>
                                            <Button id="actionBar1" variant="outline-primary" size="sm"
                                                onClick={() => setOpenDeleteAll(true)}>
                                                <DeleteForever />
                                                Delete All
                                            </Button>
                                        </ButtonGroup>
                                    </AccessControl>
                                </Col>
                                <Col className="text-right" lg="4">
                                    {/*middle button group*/}
                                    <ButtonGroup >
                                        <Tooltip placement="top" title={<Typography variant="body1" component="div">View entities in a tree structure</Typography>}>
                                            <Button id="actionBar2" variant="outline-primary" size="sm"
                                                onClick={() => { dispatch(AssetActions.setPageView(true)) }}>
                                                <ParkOutlinedIcon style={{ marginRight: "5px" }} />
                                                Tree
                                            </Button>
                                        </Tooltip>

                                        <Tooltip placement="top" title={<Typography variant="body1" component="div">View scale data in tabular form</Typography>}>
                                            <Button id="actionBar2" variant="outline-primary" size="sm"
                                                onClick={() => { dispatch(AssetActions.setPageView(false)) }}>
                                                <TableViewIcon style={{ marginRight: "5px" }} />
                                                Table
                                            </Button>
                                        </Tooltip>

                                        {scales.length > 0 ?
                                        <Tooltip placement="top" title={<Typography variant="body1" component="div">Specify a filter to view select scales</Typography>}>
                                            <Button id="actionBar2" variant="outline-primary" size="sm"
                                                onClick={() => { handleShow() }}>
                                                <FilterAltOutlinedIcon style={{ marginRight: "5px" }} />
                                                Add Filter...
                                            </Button>
                                            </Tooltip>
                                            : null}
                                        {Object.keys(filters).length > 0 ?
                                            <Button id="actionBar2" variant="outline-red" size="sm"
                                                onClick={(e) => { handleDeleteAllFilters() }}>
                                                <RemoveCircle style={{ marginRight: '8px' }} />
                                                Delete All Filters
                                            </Button>
                                            : null}

                                    </ButtonGroup>
                                </Col>
                                {/* Right button section */}
                                {/* Role 3001 (ImportTree)*/}
                                <Col className="text-right" lg="6">
                                    <ButtonGroup>
                                        {/* Not sure whether we need the pageView evaluation anymore; it prevents certain buttons
                                            from appearing on the Table View */}
                                        {selected.length > 0 /*&& pageView*/ ?
                                            <>
                                                <Button id="actionBar2" variant="outline-red" size="sm"
                                                    onClick={(e) => { dispatch(AssetActions.setSelectedItems([])) }}>
                                                    <RemoveCircle style={{ marginRight: '8px' }} />
                                                    Unselect All<br />({selected.length} selected)
                                                </Button>
                                            </>
                                            : null}
                                        <AccessControl
                                            user={user}
                                            requiredRole={roles[AccessActions.UPGRADE_SCALES]}
                                        >
                                            {/* <Button id="batchUpgradeLink" className="singleUpgradeRedirect" variant="outline-success"
                                            onClick={() => { setOpenUpgrade(true) }} size="sm"> 
                                            <OpenInNewOutlinedIcon style={{ marginRight: '8px' }}/>
                                                <UpgradeSelection
                                                    title="Select Devices to Upgrade"
                                                    openPopup={openUpgrade}
                                                    setOpenPopup={setOpenUpgrade}
                                                    tableHeaders={scaleHeadCells}
                                                    selectedDevices={Object.keys(scales).map((scalekey) => { return scales[scalekey] })}
                                                />
                                                Upgrade Scales
                                            </Button> */}
                                            <DropdownButton as={ButtonGroup} className="actionBar2" variant="outline-success" size="sm"
                                                title={<><OpenInNewOutlinedIcon /> Upgrade Scales</>}>
                                                    <Tooltip placement="right" title={<Typography variant="body1" component="div">Upgrade scales via a timer with batching (requires Upgrade Engine)</Typography>}>
                                                <Dropdown.Item variant="outline-primary" id="Dropdown"
                                                    onClick={() => setOpenBatchUpgrade(true)}
                                                >
                                                    <UpgradeSelection
                                                        openPopup={openBatchUpgrade}
                                                        setOpenPopup={setOpenBatchUpgrade}
                                                        batchUpgrade={true}
                                                        tableHeaders={scaleHeadCells}
                                                        selectedDevices={selected.length > 0 ? scales.filter(scale => selected.includes(scale.deviceId)) : scales}
                                                    />
                                                    Batch Upgrading
                                                </Dropdown.Item>
                                                </Tooltip>

                                                <Tooltip placement="right" title={<Typography variant="body1" component="div">Upgrade scales via a three-step process</Typography>}>
                                                <Dropdown.Item variant="outline-primary" id="Dropdown"
                                                    onClick={() => setOpenManualUpgrade(true)}
                                                >
                                                    <UpgradeSelection
                                                        openPopup={openManualUpgrade}
                                                        setOpenPopup={setOpenManualUpgrade}
                                                        batchUpgrade={false}
                                                        tableHeaders={scaleHeadCells}
                                                        selectedDevices={selected.length > 0 ? scales.filter(scale => selected.includes(scale.deviceId)) : scales}
                                                    />
                                                    Manual Upgrading
                                                </Dropdown.Item>
                                                </Tooltip>
                                            </DropdownButton>
                                        </AccessControl>

                                        <Tooltip placement="bottom" title={<Typography variant="body1" component="div">View scales that are not currently under a department</Typography>}>
                                        <Button id="actionBar2" variant="outline-primary" disabled={!findUnassignedScales()} size="sm" onClick={() => {
                                            setOpenUnassignedScales(true)
                                        }}>
                                            <VisibilityIcon style={{ marginRight: '8px' }} />

                                            View Unassigned Scales
                                        </Button>
                                        </Tooltip>

                                        <Tooltip placement="bottom" title={<Typography variant="body1" component="div">Verify connectivity to your scales</Typography>}>
                                        <Button id="actionBar2" variant="outline-primary" size="sm" onClick={() => {
                                            history.push("/FleetStatus")
                                        }}>
                                            <ContactlessOutlined />
                                            Ping Scales
                                        </Button>
                                        </Tooltip>

                                        <DropdownButton as={ButtonGroup} className="actionBar2" variant="outline-primary" size="sm"
                                            title={<><RefreshIcon /> Refresh Tree  </>}
                                        >
                                            <Tooltip className="info" placement='left' title={<Typography>Refresh tree by retrieving asset data from the database </Typography>}>
                                                <Dropdown.Item variant="outline-primary" id="Dropdown"
                                                    onClick={() => {
                                                        setLoadingPage(true)
                                                        dispatch(AssetActions.setScaleList({}))
                                                        dispatch(BannerAPI.fetchBanners())
                                                        dispatch(RegionAPI.fetchRegions())
                                                        dispatch(ScaleAPI.fetchScales(Date.now(), undefined, null, null, setScales))
                                                        dispatch(StoreAPI.fetchStores())
                                                        dispatch(DeptAPI.fetchDepartment())
                                                        dispatch(ProfileAPI.fetchProfiles())
                                                        dispatch(FilterAPI.fetchFilters())
                                                        dispatch(NodeStatusTimerAPI.getNodeStatusTimers())

                                                        const newID = Date.now()
                                                        function onSuccess() {
                                                            setLoadingPage(false)
                                                            console.log("Not implemented")
                                                        }
                                                        function onFail(response) {
                                                            setLoadingPage(false)
                                                            logger.error("(RefreshTree) Error getting data", response)
                                                        }
                                                        if (addCallback(callbacks, newID, "Refresh tree", onSuccess, onFail, { requestMsg: "Refreshing asset list ...", successMsg: "Asset list successfully updated!", failMsg: "default" })) {
                                                            dispatch(AssetAPI.fetchLazyTree(newID))
                                                            setLazyExpandedKeys([] as string[])
                                                            dispatch(AssetActions.setLazyExpandedTreeItems([] as string[]))
                                                        }
                                                    }}>

                                                    Refresh Tree
                                                </Dropdown.Item>
                                            </Tooltip>
                                            <Tooltip className="info" placement='left' title={
                                                <Typography>Refresh tree by cleaning and rebuilding the asset database. This should be done when data is out of sync</Typography>}>
                                                <Dropdown.Item variant="outline-primary" id="Dropdown"
                                                    onClick={() => {
                                                        setLoadingPage(true)
                                                        dispatch(AssetActions.setScaleList({}))
                                                        setLazyExpandedKeys([] as string[])
                                                        const newID = Date.now()
                                                        function onSuccess() {
                                                            dispatch(BannerAPI.fetchBanners())
                                                            dispatch(RegionAPI.fetchRegions())
                                                            dispatch(ScaleAPI.fetchScales(newID, undefined, null, null, setScales))
                                                            dispatch(StoreAPI.fetchStores())
                                                            dispatch(DeptAPI.fetchDepartment())
                                                            dispatch(ProfileAPI.fetchProfiles())
                                                            dispatch(NodeStatusTimerAPI.getNodeStatusTimers())
                                                            // dispatch(FilterAPI.fetchFilters())
                                                            setLoadingPage(false)
                                                        }
                                                        function onFail(response) {
                                                            setLoadingPage(false)
                                                            logger.error("(RefreshTree) Error getting data", response)
                                                        }
                                                        if (addCallback(callbacks, newID, "Refresh tree", onSuccess, onFail, { requestMsg: "Recompiling asset list ...", successMsg: "Asset list successfully updated!", failMsg: "default" })) {
                                                            dispatch(AssetAPI.instantiateNodeStatus(newID))
                                                        }
                                                    }}>
                                                    Rebuild Tree
                                                </Dropdown.Item>
                                            </Tooltip>
                                        </DropdownButton>
                                    </ButtonGroup>
                                </Col>
                            </Row>
                            {/* :
                            
                            <div className="d-flex justify-content-left align-items-center ">
                                <div className='spinnerDashboard'/>
                                <h2 style={{marginLeft: "1rem", marginTop: "1rem"}}> Loading Asset Data... </h2>
                            </div>
                            } */}
                        </div>

                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="info">{showAlert[1]}</Alert> : null}
                        {/*Render main Asset Tree or main Table */}
                        {pageView ? mainTree() : null}
                        {pageView ? filterTrees() : null}

                        {pageView ? null : mainTable(scales)}
                        {/*TODO: new scale fetch deprecated filtertables*/}
                        {[pageView ? null : filterTables()]}
                        {openViewNotDetails ?
                            <NotificationDetails
                                title="Error"
                                data={notificationDetails}
                                openPopup={openViewNotDetails}
                                setOpenPopup={setOpenViewNotDetails} /> : null}

                        {renderMenu}
                        <UnassignedScale
                            title="View Unassigned Scales"
                            openPopup={openUnassignedScales}
                            setOpenPopup={setOpenUnassignedScales}
                            scales={scales}
                        />
                    </Col>
                </Row>

                <Dialog open={openDeleteAll} onClose={() => { reset({ password: "" }) }} fullWidth={true} maxWidth="sm">
                    <DialogTitle>
                        <Typography variant="h6" component="div">Delete All Assets?</Typography>
                    </DialogTitle>

                    <DialogContent>
                        <Typography variant="body1" component="div">This will delete all assets shown below and upgrade groups, upgrade batches, and upgrade batch files. Are you sure you want to continue?</Typography>
                        {/* <Form id="password" className="form-control" onSubmit={handleDelete}>
                            <FormInput
                                label="Confirm Password to Continue:"
                                id="password"
                                name="password"
                                type={formType}
                                placeholder="Your account password"
                                register={register}
                                validation={{required: "Incorrect password, try again", validate: value => value === user.password || "Incorrect password, try again"}}
                                error={errors.password}
                                setFormType={setFormType}
                            />
                        </Form> */}
                        <div className="formButtons">
                            <button className="formButton1" onClick={handleDelete} type="submit">
                                <Warning />
                                <span> Delete All </span>
                                <Warning />
                            </button>
                            <button className="formButton1" onClick={() => { setOpenDeleteAll(false); reset({ password: "" }) }} type="button">
                                <Block />
                                <span> Cancel</span>
                            </button>
                        </div>
                    </DialogContent>
                </Dialog>

                <Dialog open={criticalDialog && criticalDialog.includes("Delete All") ? true : false} onClose={handleCloseCritical} fullWidth={true} maxWidth="sm">
                    <DialogTitle>
                        <div style={{ display: "flex", justifyContent: "left" }}>
                            <div style={{ marginTop: ".4rem", marginRight: "1rem" }} className='spinnerAsset' />
                            <Typography variant="h6" component="div" >Deleting Assets...</Typography>
                        </div>
                    </DialogTitle>

                    <DialogContent>
                        <Typography variant="body1" component="div" >Deleting assets. Please wait; <b>do not</b> refresh your browser... </Typography>
                    </DialogContent>
                </Dialog>
            </div>
        </>
    )
}

export default AssetList;
