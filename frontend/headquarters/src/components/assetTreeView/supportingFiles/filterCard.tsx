import { Accordion, Button, Card } from "react-bootstrap";
import { lazyNode } from "../../../types/asset/TreeTypes";
import { ArrowDropDownIcon } from "@mui/x-date-pickers";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { ReactElement, ReactNode, useState } from "react";
import { filter } from "../Sidebar";
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import { useRspHandler } from "../../utils/ResponseProvider";
import { useDispatch } from "react-redux";
import { FilterAPI } from "../../api";
import { AssetActions, useAppSelector } from "../../../state";



export interface filterCardProps{
    filter: filter
    children: ReactNode
}

function CustomToggle({ eventKey }) {
    const decoratedOnClick = useAccordionButton(eventKey);
  
    return (
        <ArrowDropDownIcon 
            type="button"
            onClick={decoratedOnClick}
        />
    );
  }
  
export const FilterCard : React.FC<filterCardProps> = (props : filterCardProps) => {
    const {filter, children} = props
    const baseLength = 35
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();
    const [lengthObject, setLengthObject] = useState({})
    const reduxLazyFilterTree = useAppSelector((state)=>state.asset.lazyFilterTree)

    function handleDeleteFilter(filterId : string) {
        let id = Date.now()
        function onFilterSuccess(){
            dispatch(FilterAPI.fetchFilters())
            let filterTree = JSON.parse(JSON.stringify(reduxLazyFilterTree))
            for(let filterPartKey of Object.keys(reduxLazyFilterTree)){
                if(filterPartKey.includes(filterId)){
                    delete filterTree[filterPartKey]
                }
            }
            dispatch(AssetActions.setLazyFilterTree(filterTree))
         }
         function onFilterFail(){
            console.log("delete failed")
         }
         
         if (addCallback(callbacks, id, "Delete filter", onFilterSuccess, onFilterFail)){
             dispatch(FilterAPI.deleteFilter(id, filterId))
         }
    }
return (<>
<Accordion >
    <Card >
        <Card.Header>
            <CustomToggle eventKey={filter.filterId.toString()}/>
            <Button style={{ margin: "5px" }} variant="danger" size="sm" onClick={() => { handleDeleteFilter(filter.filterId ) }}>
                Delete Filter
            </Button>
            <label style={{ fontSize: "15px", paddingInline: "8px", marginBlockStart: "5px" }} >
                Scales Found : {filter.numScales.toString()}
            </label>
            <label style={{ fontSize: "15px", padding: "0px", marginBlockStart: "5px" }} >
                | Filters: {(lengthObject[Object.keys(filter).join()] === baseLength) ? filter.header.toString().substring(0, lengthObject[Object.keys(filter).join()]) : filter.header.toString()}
                {(filter.header.length > baseLength) ?
                    (lengthObject[Object.keys(filter).join()] === baseLength) ?
                        <Button style={{ borderColor: "transparent", backgroundColor: "transparent", color: "black" }}
                            onClick={() => { lengthObject[Object.keys(filter).join()] =  filter.header.toString().length; }}>
                            {"..."}
                        </Button>
                        :
                        <Button style={{ borderColor: "transparent", backgroundColor: "transparent", color: "black" }}
                            onClick={() => { lengthObject[Object.keys(filter).join()] = baseLength; }}>
                            <ArrowBackIcon style={{ fontSize: "small" }} />
                        </Button>
                    :
                    null
                }
            </label>
        </Card.Header>
        <Accordion.Collapse eventKey={filter.filterId.toString()}>
            <Card.Body>
                {children}
            </Card.Body>
        </Accordion.Collapse>
    </Card>
</Accordion>
</>)
}

function dispatch(arg0: any) {
    throw new Error("Function not implemented.");
}
