import MinimizeIcon from '@mui/icons-material/Remove';
import {assetListStyle} from '../styles/NavStyles';
import Box from '@material-ui/core/Box';
import { Button} from '@themesberg/react-bootstrap';
import 'status-indicator/styles.css'
import { GetMessageView } from './MessageIndicator';
import logger from '../utils/logger';
import ScaleActionsComponent from './ScaleActionsComponent';
import SetProfile from '../assetDetails/detailComponents/SetProfile';
import SetEnable from '../assetDetails/detailComponents/SetEnable';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { Link } from 'react-router-dom';
import { useAppSelector } from '../../state';
import { Node, lazyNode } from '../../types/asset/TreeTypes';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { format } from 'date-fns';

export default function ScaleViewDetails(treeElement : lazyNode | Node, minimizeFunction : (e : React.MouseEvent<HTMLElement, MouseEvent>, id: string) => void, infoShowing : string[], scales: ScaleDevice[]){
    const classes : any = assetListStyle();

    // Not currently allowing user to cancel requests
    //     Currently, if user does an action (ping, executeManualHeartbeat, etc.), then the request will still finish even if you leave the page.
    // const [controller, setController] = useState(axios.CancelToken.source())      // Contains amount uploaded + total

    const profiles = useAppSelector((state) => state.profile.profiles);
    const roles = useAppSelector((state) => state.access.roles);
    const user = useAppSelector((state) => state.access.account);
    let scale = scales.filter(scale => scale.deviceId === treeElement.id)[0]
    if (!scale && treeElement.properties) {
        scale = treeElement.properties
    }

    if(infoShowing.length === 0){
        return null
    }

    let treeProperties = treeElement.properties
    return(
        <div className="assetBar" style={{width: '100%'}}>

            {/* {GetScaleMessage(treeElement)} */}
            {GetMessageView(treeElement.nodeStatus.status, treeElement.nodeStatus.message)}
            {treeProperties != null ?
            <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", width: '100%',}}>
                <div className={classes.accordionRooCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Host Name:</Box>
                        <Box className="boxRegular">{treeProperties.hostname == null ? "No Report" : treeProperties.hostname}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Serial Number:</Box>
                        <Box className="boxRegular">{treeProperties.serialNumber == null ? "No Report" : treeProperties.serialNumber}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Scale Model:</Box>
                        <Box className="boxRegular">{treeProperties.scaleModel == null ? "No Report" : treeProperties.scaleModel}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold" style={{color : "#ba4735"}}>PLU Count: </Box>
                        <Box className="boxRegular">{treeProperties.pluCount == null ? "No Report" : treeProperties.pluCount}</Box>
                    </div>
            
                </div>
                <div className={classes.accordionRootCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Application:</Box>
                        <Box className="boxRegular">{treeProperties.application == null ? "No Report" : treeProperties.application}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">System Controller:</Box>
                        <Box className="boxRegular">{treeProperties.systemController == null ? "No Report" : treeProperties.systemController}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Loader:</Box>
                        <Box className="boxRegular">{treeProperties.loader == null ? "No Report" : treeProperties.loader}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">SM Backend:</Box>
                        <Box className="boxRegular">{treeProperties.smBackend == null ? "No Report" : treeProperties.smBackend}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Last Report:</Box>
                        <Box className="boxRegular">{treeProperties.lastReportTimestampUtc == null ? "No Report" : format(Date.parse(treeProperties.lastReportTimestampUtc), "PPpp")}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold" style={{color : "#ba4735"}}>Total Labels Printed: </Box>
                        <Box className="boxRegular">{treeProperties.totalLabelsPrinted == null ? "No Report" : treeProperties.totalLabelsPrinted}</Box>
                    </div>

                </div>
            </div> : null}


            <div style={{display: "flex",flexWrap: "wrap",}}>
                <SetProfile 
                    classes={classes}
                    scaleProperties={scale}
                    user={user}
                    profiles={profiles}
                    scales={scales}
                />
                <SetEnable
                    info={scale}
                    user={user}
                />
            </div>
            

            <div style={{display: "flex",flexWrap: "wrap",}}>

                <div className={classes.labelRoot}>
                    <ScaleActionsComponent
                        scale={scale}
                        user={user}
                        roles={roles}
                    />
                </div>

                <Link to={{ pathname: '/AssetDetails', state: { scale: scale}}}>
                    <Button id="detailsLink" className="minimizeButton" variant="link" onClick={() => { console.log(scale) }}> 
                        View More Details
                        <ArrowForwardIcon/>
                    </Button>
                </Link>

                <Button className="minimizeButton" variant="link"
                onClick={(e) => {
                        logger.info("(ScaleViewDetails) ",treeElement)
                        minimizeFunction(e, treeElement.id)
                    }}>
                    <MinimizeIcon/>
                    Minimize
                </Button>
            </div>
        </div>
    )
}