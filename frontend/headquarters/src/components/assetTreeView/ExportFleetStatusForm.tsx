import { Block, Download, ExpandMore, Upload } from "@mui/icons-material"
import { Alert, Dialog, DialogContent, DialogTitle, Typography } from "@mui/material"
import { FormCheck } from "@themesberg/react-bootstrap"
import { useState, useMemo} from "react"
import { Form } from "react-bootstrap"
import { useForm } from "react-hook-form"
import logger from "../utils/logger"
import { ScaleStatusTableType } from "./FleetStatus"
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import { styled } from '@mui/material/styles';
import MuiAccordionSummary, {
    AccordionSummaryProps,
  } from '@mui/material/AccordionSummary';
import MaterialReactTable, { MRT_ColumnDef } from "material-react-table"
import { ScaleStatusType } from "../../types/asset/ScaleTypes"
import { format } from "date-fns"
import { AlertMessage, HideMessage } from "../../types/utils/utilTypes"

const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
  ))(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
  }));
  
  const AccordionSummary = styled((props: AccordionSummaryProps) => (
    <MuiAccordionSummary
      expandIcon={<ExpandMore sx={{ fontSize: '0.9rem' }} />}
      {...props}
    />
  ))(({ theme }) => ({
    backgroundColor: 'rgba(32, 49, 64, .1)',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
      transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
      marginLeft: theme.spacing(1),
    },
  }));
  
  const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
  }));


interface ExportFleetStatusFormProps{
    openPopup : boolean
    setOpenPopup : React.Dispatch<React.SetStateAction<"RANGE" | "IMPORT" | "EXPORT" | null>>
    scaleStatusList : {[key : string] : ScaleStatusTableType}
}


/* Form to export fleet status 
* Form will take the current ScaleStatusLists and export them to a CSV file
* it will prompt the user to browse to a location to save the file and will prompt if
* the user wants to include headers in the file. As well as if the user wants to customize
* the columns of the file.
*/
export const ExportFleetStatusForm = (props: ExportFleetStatusFormProps) => {
    const { openPopup, setOpenPopup, scaleStatusList } = props;

    interface HeaderName{
        headerName : string
    }

    const columns = useMemo<MRT_ColumnDef<HeaderName>[]>(() => [
        {accessorKey: 'headerName', header: 'Name'}
    ], [])

    interface ExportForm{
        includeHeaders : boolean
        includeIP: boolean
        includeHostname: boolean
        includeDeviceType: boolean
        includeHeartbeat: boolean
        includeAppVersion: boolean
        includeAppHookVersion: boolean
        includeStoreId: boolean
        includePLUCount: boolean
        includeTotalMem: boolean
        includeFreeMem: boolean
        includeStatusMsg: boolean
    }

    const { register, handleSubmit, formState: { errors } } = useForm<ExportForm>();
    const [alertMessage, setAlertMessage] = useState<AlertMessage>(HideMessage())

    const handleCloseDialog = (event: any) => {
        event.stopPropagation()
        setOpenPopup(null);
    }

    const handleExport = (data : ExportForm) => {
        setAlertMessage(HideMessage())
        logger.info("(ExportFleetStatusForm) Exporting scale list to file: ")

        let csvContent = "data:text/csv;charset=utf-8,";
        if(data.includeHeaders){
            if(data.includeIP) csvContent += "Scale IP,"
            if(data.includeHostname) csvContent += "Hostname,"
            if(data.includeDeviceType) csvContent += "Device Type,"
            if(data.includeHeartbeat) csvContent += "Heartbeat,"
            if(data.includeAppVersion) csvContent += "App Version,"
            if(data.includeAppHookVersion) csvContent += "App Hook Version,"
            if(data.includeStoreId) csvContent += "Store ID,"
            if(data.includePLUCount) csvContent += "PLU Count,"
            if(data.includeTotalMem) csvContent += "Total Memory,"
            if(data.includeFreeMem) csvContent += "Free Memory,"
            if(data.includeStatusMsg) csvContent += "Status Message,"
            csvContent += "\r"
        }
        

        //Add each scale to the file
        for (const scale in scaleStatusList) {
            if(data.includeIP) csvContent += scaleStatusList[scale].ipAddress + ","
            if(data.includeHostname) csvContent += scaleStatusList[scale].hostname + ","
            if(data.includeDeviceType) csvContent += scaleStatusList[scale].scaleModel + ","
            if(data.includeHeartbeat) csvContent += scaleStatusList[scale].lastReportTimestampUtc + ","
            if(data.includeAppVersion) csvContent += scaleStatusList[scale].applicationVersion + ","
            if(data.includeAppHookVersion) csvContent += scaleStatusList[scale].apphookVersion + ","
            if(data.includeStoreId) csvContent += scaleStatusList[scale].storeId + ","
            if(data.includePLUCount) csvContent += scaleStatusList[scale].pluCount + ","
            if(data.includeTotalMem) csvContent += scaleStatusList[scale].totalMemory + ","
            if(data.includeFreeMem) csvContent += scaleStatusList[scale].freeMemory + ","
            if(data.includeStatusMsg) csvContent += scaleStatusList[scale].statusMessage + ","

            csvContent += "\r"
        }
        
        //Create a link to download the file
        const encodedUri = encodeURI(csvContent);
        const link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "scaleStatusList_" + format(Date.now(),"MMddyyy") + ".csv");
        document.body.appendChild(link); // Required for FF
        link.click();

        setOpenPopup(null);
    }


    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    Export Scale List
                </Typography>
            </DialogTitle>

            <DialogContent>        
                <Form className="form-control" onSubmit={handleSubmit(handleExport)}>
                    <div className="uploadFile">

                        <FormCheck type="checkbox" className="d-flex p-2">
                            <FormCheck.Input id="includeHeaders" className="me-2" {...register("includeHeaders")}/>
                            <FormCheck.Label>Include Headers</FormCheck.Label>
                        </FormCheck>

                        <Accordion>
                            <AccordionSummary
                                expandIcon={<ExpandMore />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography style={{fontFamily:'"Quicksand", "Arial", "sans-serif"',
                                fontWeight:600, fontSize:'1rem'}}>Columns to Include</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                
                                <div>
                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeIP" className="me-2" defaultChecked={true} {...register("includeIP")}/>
                                        <FormCheck.Label>Ip Address</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeHostname" className="me-2" defaultChecked={true} {...register("includeHostname")}/>
                                        <FormCheck.Label>Hostname</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeDeviceType" className="me-2" defaultChecked={true} {...register("includeDeviceType")}/>
                                        <FormCheck.Label>Device Type</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeHeartbeat" className="me-2" defaultChecked={true} {...register("includeHeartbeat")}/>
                                        <FormCheck.Label>Last Heartbeat</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeAppVersion" className="me-2" defaultChecked={true} {...register("includeAppVersion")}/>
                                        <FormCheck.Label>Application Version</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeAppHookVersion" className="me-2" defaultChecked={true} {...register("includeAppHookVersion")}/>
                                        <FormCheck.Label>Apphook Version</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeStoreId" className="me-2" defaultChecked={true} {...register("includeStoreId")}/>
                                        <FormCheck.Label>Store ID</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includePLUCount" className="me-2" defaultChecked={true} {...register("includePLUCount")}/>
                                        <FormCheck.Label>PLU Count</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeTotalMem" className="me-2" defaultChecked={true} {...register("includeTotalMem")}/>
                                        <FormCheck.Label>Total Memory</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeFreeMem" className="me-2" defaultChecked={true} {...register("includeFreeMem")}/>
                                        <FormCheck.Label>Free Memory</FormCheck.Label>
                                    </FormCheck>

                                    <FormCheck type="checkbox" className="d-flex p-2" >
                                        <FormCheck.Input id="includeStatusMsg" className="me-2" defaultChecked={false} {...register("includeStatusMsg")}/>
                                        <FormCheck.Label>Status Message</FormCheck.Label>
                                    </FormCheck>
                                </div>
                            </AccordionDetails>
                        </Accordion>


                        {/* Error importing file */}
                        {alertMessage.show ? <Alert className="m-1" variant="filled" severity={alertMessage.severity}>{alertMessage.message}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleSubmit(handleExport)} type="submit">
                                <Download />
                                <span> Download File </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>     
                </Form>
            </DialogContent>
        </Dialog>
    )


}