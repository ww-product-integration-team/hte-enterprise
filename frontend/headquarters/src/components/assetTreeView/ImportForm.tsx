import { useState } from 'react';
import { useForm } from "react-hook-form";
import { Dialog, DialogTitle, DialogContent, Typography } from '@material-ui/core';
import Alert from '@mui/material/Alert';
import {Upload, Block} from '@mui/icons-material';
import { Form, FormCheck } from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux"
import { FormInput } from '../forms/FormHelper';
import "react-datepicker/dist/react-datepicker.css";
import {useAppSelector} from '../../state/store'
import { BannerAPI , RegionAPI, StoreAPI, DeptAPI, AssetAPI} from "../api/index"
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';

// =================== CATEGORY DATA ===================
interface JsonFormat {
    text: string,
    name: string,
    type: string,
    id: string,
    bannerId?: string,
    regionId?: string,
    storeId?: string,
    deptId?: string,
    status?: {
        state: string,
        message: string
    },
    properties?: {},
    children?: JsonFormat[]
}

const initialJson: JsonFormat = {
    text: "",
    name: "",
    type: "",
    id: ""
}

let hadParsingError = false
let postArray = [initialJson] //Constructed array for sending the imported array to the
function parseXML(xmlDoc, parentObj, currentID, banners, shouldMerge = true){
    let obj = initialJson
    obj.name = xmlDoc.attributes.getNamedItem("name") == null ? null : xmlDoc.attributes.getNamedItem("name").nodeValue

    switch(xmlDoc.nodeName){
        case 'root':        // BANNER ================================================================
            obj.type = "banner";
            obj.id = String(currentID)
            obj.name = xmlDoc.nodeName == null ? "root" : xmlDoc.nodeName;

            if(shouldMerge){
                //Set Banner id to the last in the banner list +1
                // ! after a variable or value asserts it is not-null
                currentID = banners.length > 0 ? parseInt(banners!.at(-1)!.bannerId) + 1 : 1
                obj.id =currentID
            }

            break;
        case 'region':      // REGION ================================================================
            obj.type = xmlDoc.nodeName;
            obj.id = parentObj.id +"|" + currentID;
            obj.regionId = currentID
            obj.bannerId = parentObj.id
            

            break;
        case 'store':       // STORE ================================================================
            obj.type = xmlDoc.nodeName;
            obj.id = parentObj.regionId +"|" + currentID;
            obj.storeId = currentID
            obj.regionId = parentObj.regionId

            break;
        case 'department':  // DEPARTMENT ===========================================================
            obj.type = "dept";
            obj.id = parentObj.storeId +"|" + currentID
            obj.deptId = currentID

            break;
        case 'asset':       // SCALE ================================================================
            obj.type = "scale";
            obj.id = Date.now() + "|" + currentID //Temporary ID for local tree, this should not be used anywhere
            obj.name = xmlDoc.attributes.getNamedItem("ip") == null ? null : xmlDoc.attributes.getNamedItem("ip").nodeValue
            obj.properties ={
                ipAddress: obj.name,
                hostname: xmlDoc.attributes.getNamedItem("hostname") == null ? null : xmlDoc.attributes.getNamedItem("hostname").nodeValue,
                //serialNumber: null,
                scaleModel: xmlDoc.attributes.getNamedItem("device_type") == null ? null : xmlDoc.attributes.getNamedItem("device_type").nodeValue,
                storeId: parseInt(parentObj.id.split("|")[0]),
                deptId: parseInt(parentObj.id.split("|")[1]),
                //countryCode: null,
                //buildNumber: null,
                application: xmlDoc.attributes.getNamedItem("application_version") == null ? null : xmlDoc.attributes.getNamedItem("application_version").nodeValue,
                //operatingSystem: null,
                //systemController: null,
                loader: xmlDoc.attributes.getNamedItem("bootloader_version") == null ? null : xmlDoc.attributes.getNamedItem("bootloader_version").nodeValue,
                smBackend: xmlDoc.attributes.getNamedItem("backend_version") == null ? null : xmlDoc.attributes.getNamedItem("backend_version").nodeValue,
                //lastReportTimestampUtc: null,
                //lastTriggerType: null,
                //profileId: null,
                enabled: true,
                last24HourStatus: "XXXXXXXXXXXXXXXXXXXXXXXX",
                last24HourSync: "------------------------",
                primaryScale: false
            }
            obj.status = {
                state: "disabled",
                message: "Scale is disabled and is not communicating with server"
            }
            break;
        default: 
            hadParsingError = true
            postArray = []
            break;
    }
    postArray.push({...obj})

    obj.children = []

    // let elements = xmlDoc.getElementsByTagName()
    for(let item of xmlDoc.children){
        obj.children.push(parseXML(item, obj, currentID+1, banners))
        //logger(item)
        //parseXML(item)
    }

    return obj
}

// Upload Files
function ImportTree(props){

    const { register, handleSubmit, reset,  formState: {errors} } = useForm();
    const { title, openPopup, setOpenPopup} = props;
    const [showAlert, setShowAlert]                 = useState([false, "Something went wrong!"])
    const {callbacks, addCallback} = useRspHandler();

    const dispatch = useDispatch();
    const banners = useAppSelector(state => state.asset.banners)

    // This clears the input form when used with "reset" from React-Hook-Form
    const defaultValues = {
        input: "",
    };

    const handleCloseDialog = (event) => {
        setShowAlert([false, "Something went wrong!"])
        setOpenPopup(false);

        if(event != null){
            event.stopPropagation()
        }
        
        reset({defaultValues});
    }

    // POST File Upload to API
    const handleImport = async (data, event) => { // When the Submit button is pressed
        logger.debug("(ImportForm) POST request (Just File): ", data.fileName[0]);
        
        setShowAlert([false, "Something went wrong!"])
        
        let file = data.fileName[0];
        let reader = new FileReader();
        let parser = new DOMParser();
        let tree: JsonFormat[] = []
        reader.onload = function(progressEvent){    
            let xmlDoc = parser.parseFromString(this.result as string, "text/xml");
            hadParsingError = false
            postArray = []
            let elements = xmlDoc.getElementsByTagName("root")
            for (let i = 0; i < elements.length; i++) {
                tree.push(parseXML(elements[i], null, 1, banners))
            }
            // for(let item of xmlDoc.children){
            //     tree.push(parseXML(item, null, 1))
            // }
            
            
            if(hadParsingError){
                setShowAlert([true, "Invalid file format!"])
                return
            }
            else{
                logger.debug("(ImportForm) ", postArray)
                //dispatch(AssetActions.setTreeView(tree))
            }

            //Parsing was a success Now post to array
            //PostTree()
            logger.debug("(ImportForm) ", postArray)
            const newID = Date.now()
            function onSuccess(response){
                logger.debug("(ImportForm) Import Form Response:", response)
                handleCloseDialog(event)

                dispatch(BannerAPI.fetchBanners())
                dispatch(RegionAPI.fetchRegions())
                dispatch(StoreAPI.fetchStores())
                dispatch(DeptAPI.fetchDepartment())
            }
            function onFail(response){
                logger.error("(ImportTree) Error importing tree", response)
                response.response.errorDescription != null ? setShowAlert([true, response.response.errorDescription]) :
                setShowAlert([true, "Error trying to import existing tree!"])
            }
            if(addCallback(callbacks, newID, "Import Tree", onSuccess, onFail)){
                dispatch(AssetAPI.importTree(newID, postArray))
            }

        };
        reader.readAsText(file);
    }

    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    {title}
                </Typography>
            </DialogTitle>

            <DialogContent>        
                <Form className="form-control" onSubmit={handleSubmit(handleImport)}>
                    <div className="uploadFile">

                        <FormInput
                            label="Upload a .xml file here: "
                            id="fileName"
                            name="fileName"
                            type="file"
                            accept=".xml" 
                            register={register}
                            validation={{required: "Please select a file to upload"}}
                            error={errors.fileName}
                        />

                        <FormCheck type="checkbox" className="d-flex p-2" >
                            <FormCheck.Input id="overwriteTree" className="me-2"/>
                            <FormCheck.Label>Overwrite Current Tree</FormCheck.Label>
                        </FormCheck>

                        {/* Error importing file */}
                        {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleSubmit(handleImport)} type="submit">
                                <Upload />
                                <span> Import </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>     
                </Form>
            </DialogContent>
        </Dialog>
    );
}

export { ImportTree };