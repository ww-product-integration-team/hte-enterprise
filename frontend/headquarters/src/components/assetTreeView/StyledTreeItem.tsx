import React, {useState, useEffect, useMemo} from 'react';
import LocalParkingIcon from '@mui/icons-material/LocalParking';
import { Typography, IconButton, Menu, Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import {MoreHoriz} from '@material-ui/icons';
import { accordionStyle} from '../styles/NavStyles';
import GetTreeItemOptions from './GetTreeItemOptions';
import ScaleIcon from '../icons/ScaleIcon';
import { useDispatch} from "react-redux";
import 'status-indicator/styles.css'
import { PieChart } from 'react-minimal-pie-chart';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ScaleViewDetails from './ScaleViewDetails';
import DeptViewDetails from './DeptViewDetails';
import { AssetActions, useAppSelector } from '../../state';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

import '../assetTreeView/AssetListStyle.css'
import DepartmentIcon from '@material-ui/icons/LocalGroceryStoreOutlined';
import RegionIcon from '@material-ui/icons/RoomOutlined';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import BannerIcon from '@material-ui/icons/Flag';
import StoreIcon from '@material-ui/icons/Store';
import logger from '../utils/logger';
import StoreViewDetails from './StoreViewDetails';
import StatusIndicator from '../dashboard/widgets/StatusIndicator';
import { Node, lazyNode } from '../../types/asset/TreeTypes';
import { Block, TableChart } from '@mui/icons-material';
import VirtualTable, { HeadCellTypes } from '../common/virtualTable/VirtualTable';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { Batch } from '../upgradeManager/UpgradeManager';

function GetStatusIcon(treeElement : lazyNode | Node){
    //logger("GetStatusIcon : " + treeElement.name)
    if(treeElement.name == null){return}
    if(treeElement.type === "scale"){
        switch(treeElement.nodeStatus.status){
            case "ONLINE":
                return(<StatusIndicator positive pulse style={{marginLeft:8}}></StatusIndicator>)
            case "WARNING":
                return(<StatusIndicator intermediary pulse style={{marginLeft:8}}></StatusIndicator>)
            case "DISABLED":
                return(<StatusIndicator pulse style={{marginLeft:8, backgroundColor:'#a4abb0'}}></StatusIndicator>)
            default:
                return(<StatusIndicator negative pulse style={{marginLeft:8}}></StatusIndicator>)
        }
    }

    let totalScales = treeElement.nodeStatus.numOnline + treeElement.nodeStatus.numWarning + treeElement.nodeStatus.numError + treeElement.nodeStatus.numDisabled
    return (
    <div>
        <div className="chart-container">
            {totalScales > 0 ?
            <PieChart
                // animate
                // animationDuration={500}
                // animationEasing="ease-out"
                data={[
                {
                color: "#4bd28f",//"#2e7d32",
                title: "online",
                value: treeElement.nodeStatus.numOnline,
                },
                {
                color: "#ffaa00",//"#f9a825",
                title: "away",
                value: treeElement.nodeStatus.numWarning,
                },
                {
                color: "#ff4d4d",//"#c62828",
                title: "offline",
                value: treeElement.nodeStatus.numError,
                },
                {
                color: "#a4abb0",
                title: "disabled",
                value: treeElement.nodeStatus.numDisabled,
                }
                ]}
            /> : <InfoOutlinedIcon style={{color: '#a4abb0', marginTop: '4px', marginLeft: '4px'}}/> } 
        </div>

        <style>{`
        .chart-container {
            height: 30px;
            margin-left: 8px;
            margin-right: auto;
            width: 30px;
        }

        .inline-container {
            align-items: center;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            width: 100%;
        }
        `}</style>
    </div>
    );
}

function getStoreInfo(treeElement : lazyNode | Node){

    if(treeElement.type === "store"){
        switch(treeElement.nodeStatus.status){
            case "ONLINE":
                return(<StatusIndicator positive pulse style={{marginLeft:8}}></StatusIndicator>)
            case "WARNING":
                return(<StatusIndicator intermediary pulse style={{marginLeft:8}}></StatusIndicator>)
            case "DISABLED":
                return(<StatusIndicator pulse style={{marginLeft:8, backgroundColor:'#a4abb0'}}></StatusIndicator>)
        }
    }
}

function GetViewDetailsFormat(treeElement : lazyNode | Node, handleShowDetails, infoShowing : string[], scales: ScaleDevice[]){

    switch(treeElement.type){
        case 'scale':
            return ScaleViewDetails(treeElement, handleShowDetails, infoShowing, scales)
        case 'dept':
            return DeptViewDetails(treeElement, handleShowDetails, infoShowing)
        case 'store':
            return StoreViewDetails(treeElement, handleShowDetails, infoShowing)
        default:
            return(<div className="assetBar" style={{width: '100%'}}></div>)
    }
}

function itemIcon(type){
    let icon = ErrorIcon
    switch(type){
        case 'banner': icon = BannerIcon; break
        case 'region': icon = RegionIcon; break
        case 'store': icon = StoreIcon; break
        case 'dept': icon = DepartmentIcon; break
        case 'scale': icon = ScaleIcon; break
        default: break
    }
    return icon
}

interface StyledTreeNodeProps {
    treeElement: lazyNode | Node
    scales: ScaleDevice[]
    setScales: React.Dispatch<React.SetStateAction<ScaleDevice[]>>
    filterArrayLength?: number
    loadingTreeNode?: boolean
}

// Tree Node Styling
//=====================================================================================================
export default function StyledTreeNode(props : StyledTreeNodeProps){
    
    const {treeElement, scales, setScales, filterArrayLength, loadingTreeNode} = props;
    const dispatch = useDispatch();

    //Highlight of focused asset
    //=============================================================================================
    const [highlight, setHighlight] = useState({backgroundColor: '', transition: '', border: ''})
    const focusedAsset = useAppSelector(state => state.asset.focused)
    const selected : string[] = useAppSelector(state => state.asset.selected)
    const isSelected : string = selected.filter((item)=>{if(item === treeElement.id)return item})[0]
    function fadeIn(){setHighlight({backgroundColor: '', transition:'background-color 1s linear', border: ''})}
    function killFade(){setHighlight({backgroundColor: '', transition:'', border: ''})}
    function clearFocus(){dispatch(AssetActions.setFocusedAsset(""))}

    useEffect(()=>{
        let id = treeElement.id;
        if(id && treeElement.id === focusedAsset){
            setTimeout(() => {setHighlight({backgroundColor: 'rgba(186,71,53,0.50)', transition:'background-color 1s linear', border: '3px solid rgba(186,71,53, 1)'})}, 100)
            setTimeout(fadeIn, 4000)
            setTimeout(killFade, 5000)
            setTimeout(clearFocus, 5000)
        }
    }, [focusedAsset])
    useEffect(() => {
        // dispatch(UpgradeAPI.getBatches(Date.now()))
    }, [])

    useMemo(()=> {
        settingHiglightVar(isSelected)
    },[isSelected])
    
    function settingHiglightVar(isSelected){
        if(isSelected){

            setHighlight({backgroundColor: 'rgba(45,85,119,0.25)', transition:'', border: "3px solid rgba(45,85,119,1)"})
        }
        else{
            setHighlight({backgroundColor: '', transition:'', border: ''})        
        }
    }    
    //Show Details
    //=============================================================================================
    const [infoShowing, setShowInfo] = useState<string[]>([]);
    const handleShowDetails = (event : React.MouseEvent<HTMLElement, MouseEvent>, nodeId : string) => {
        logger.debug("(StyledTreeItem) info showing " + infoShowing)
        if(!infoShowing.includes(nodeId)){
            infoShowing.push(nodeId)
        }
        else{
            setShowInfo([])
        }
    };

    //Node Action Menu
    //=============================================================================================
    const [renderTreeOptions, setRenderTreeOptions] = useState(false)
    const [assetMenu, setAssetMenu] = useState<Element | null>(null);
    const isAssetMenuOpen = Boolean(assetMenu);   // Show/hide asset actions menu
    const handleAssetMenuOpen = (event : React.MouseEvent<HTMLButtonElement, MouseEvent>, nodeId : string) => {
        event.stopPropagation()
        setAssetMenu(event.currentTarget)
    };
    const handleAssetMenuClose = (event) => {
        event.stopPropagation()
        setAssetMenu(null)
    };

    const assetId = 'assetActions';
    const renderAssetActions = (
        <Menu
            id={assetId}
            anchorEl={assetMenu}
            open={isAssetMenuOpen}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            keepMounted
            onClose={handleAssetMenuClose}
        >
            {GetTreeItemOptions(treeElement, handleShowDetails, handleAssetMenuOpen, handleAssetMenuClose, setScales)}
        </Menu>
    );

    //Styles
    //=============================================================================================
    const accordionClass = accordionStyle();
    const ContentIcon = itemIcon(treeElement.type)

    const [anchorEl, setAnchorEl] = useState<Element | null | undefined>(null);
    const [xPos, setXPos] = useState("0px");
    const [yPos, setYPos] = useState("0px");
    const isOpen = Boolean(anchorEl);   // Show/hide asset actions menu

    const handleRightClick = (event : any) => {
        if (!anchorEl) {
            event.preventDefault();
            event.stopPropagation();        
        }

        setXPos(event.clientX);
        setYPos(event.clientY);
        setAnchorEl(event.currentTarget);
    };

    const handleLeftClick = (event : any) =>{
        let elementId = treeElement.id
        if(treeElement.id.includes("!~!"))
            elementId = treeElement.id.substring(0,treeElement.id.indexOf("!~!"))

        let elementIds:string[] = [elementId]
        
        if(filterArrayLength){
                for(let pos = 0; pos < filterArrayLength; pos++){
                    elementIds.push(elementId + "!~!filter"+ pos)
                }
            }
        
        if(event.ctrlKey){
            logger.debug("CTRL key selected")
            let newSelected = [...selected]
            if(newSelected.includes(treeElement.id)){
                // newSelected.splice(newSelected.indexOf(treeElement.id), 1)
                newSelected  = newSelected.filter((nodeId)=>{if(!nodeId.includes(treeElement.id)) return nodeId})
            }
            else{
                newSelected.push(...elementIds)
            }
            
            dispatch(AssetActions.setSelectedItems(newSelected))
        }
        else{
            if(selected.includes(treeElement.id)){
                dispatch(AssetActions.setSelectedItems([]))
            }
            else{
                dispatch(AssetActions.setSelectedItems(elementIds))
            }
        }
    }
    
    const handleClose = () => {
        setAnchorEl(null);
    };

    // To view batch info on specific entities
    const [showBatch, setShowBatch] = useState(false);
    const batchInfoHeaders: HeadCellTypes[] = [
        {id: 'name', sorted: false, searchable: false, label: 'Batch Name', isShowing: true, mandatory: true},
        {id: 'description', sorted: false, searchable: false, label: 'Description', isShowing: false, mandatory: false},
        {id: 'fileName', sorted: false, searchable: false, label: 'File Name', isShowing: true, mandatory: true},
        {id: 'uploadStart', sorted: false, searchable: false, label: 'Upload Start', isShowing: true, mandatory: true},
        {id: 'uploadEnd', sorted: false, searchable: false, label: 'Upload End', isShowing: true, mandatory: true},
        {id: 'upgradeStart', sorted: false, searchable: false, label: 'Upgrade Start', isShowing: true, mandatory: true},
        {id: 'upgradeEnd', sorted: false, searchable: false, label: 'Upgrade End', isShowing: true, mandatory: true}
    ]
    const batches = useAppSelector(state=>state.asset.batches)
    let entityBatches: Batch[] = []
    for(let key of Object.keys(batches)) {
        if (treeElement.batchIds && treeElement.batchIds.includes(key)) {
            entityBatches.push(batches[key])
        }
    }

    //Content
    //=============================================================================================
    return(
        <div>
        {(!loadingTreeNode)?
        <div id={treeElement.id + "content"} className='treeNodeContents' style={{backgroundColor:highlight.backgroundColor, transition:highlight.transition, borderBottom: highlight.border}}>
            
            <Accordion
                classes={accordionClass}    
                expanded={infoShowing.includes(treeElement.id)}
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="elevation"
                color="primary"
                onContextMenu={handleRightClick}
                onClick={handleLeftClick}
            >
                
                <AccordionSummary>
                    { treeElement.type ==="store" || treeElement.type ==="dept" ||treeElement.type === "scale"  ?
                        <IconButton color="inherit" disableRipple onClick={(e)=>{handleShowDetails(e, treeElement.id)}} title="Click to view details" style={{marginLeft:-10, marginRight: -5,padding:0}}>
                            <ContentIcon style={{marginBlock:"0","padding":"0"}} className='treeNodeContentIcon'/>
                        </IconButton>
                    :
                        <ContentIcon className='treeNodeContentIcon'/>
                    }

                    {getStoreInfo(treeElement)}

                    {GetStatusIcon(treeElement)}
                    {treeElement ? 
                    <Typography variant='body2' className='treeNodeLabel'>
                        {treeElement.name}
                        {treeElement.type === "scale" && treeElement.properties ?
                            " | Hostname: " + (treeElement.properties.hostname == null ? "No Report" : treeElement.properties.hostname)
                        :
                            null
                        }
                        {treeElement.type === "scale" && treeElement.properties ?
                            " | Application Version: " + (treeElement.properties.application == null ? "No Report" : treeElement.properties.application)
                        :
                            null
                        }
                        {treeElement.type === "scale" && treeElement.properties ?
                            " | Model: " + (treeElement.properties.scaleModel == null ? "No Report" : treeElement.properties.scaleModel)
                        :
                            null
                        }
                    </Typography>
                    : null
                }

                    {
                        // ((treeElement.type === "store" || treeElement.type === "dept" || treeElement.type === "scale") && treeElement.batchIds.length > 0) ?
                        // <TableChart onClick={()=>setShowBatch(true)} className="treeNodeContentIcon" style={{color: '#0288d1'}} titleAccess={"This entity is included in a batch - Click to see more"}/> : null
                        ((treeElement.type === "store" || treeElement.type === "dept" || treeElement.type === "scale") && treeElement.batchIds.length > 0) ?
                        <TableChart className="treeNodeContentIcon" style={{color: '#0288d1'}} titleAccess={"This entity is included in a upgrade"}/> : null
                    }
                    {
                        (treeElement.type === "scale" && treeElement.properties && treeElement.properties.isPrimaryScale) ?
                        <LocalParkingIcon className='treeNodeContentIcon' style={{color: '#4bd28f'}} titleAccess={"This is a primary scale"}/> : null
                    }

                    {renderTreeOptions ? renderAssetActions : null} 
                    {treeElement.id && !treeElement.id.includes("~") ? 
                    <IconButton
                        aria-label="show actions" aria-controls={assetId} aria-haspopup={true} 
                        onClick={e => {
                            setRenderTreeOptions(true)
                            handleAssetMenuOpen(e, treeElement.id)
                        }} color="inherit">
                        <MoreHoriz />
                    </IconButton>
                    : null}
                </AccordionSummary>
                {treeElement.id && !treeElement.id.includes("~") ? 
                <AccordionDetails>
                    {GetViewDetailsFormat(treeElement, handleShowDetails, infoShowing, scales)}
                </AccordionDetails>
                : 
                null}
            </Accordion>

            <Menu
                id="customized-menu"
                style={{contextMenu: { pointerEvents: 'none' }} as any} 
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                elevation={0}
                anchorEl={anchorEl}
                anchorReference="anchorPosition"
                anchorPosition={
                    yPos !== null && xPos !== null
                    ? { top: Number(yPos), left: Number(xPos) }
                    : undefined
                }
            >   
                {treeElement.id && !treeElement.id.includes("~") ? 
                GetTreeItemOptions(treeElement, handleShowDetails, handleAssetMenuOpen, handleAssetMenuClose, setScales, anchorEl, setAnchorEl) :
                 null }
            </Menu>

            <Dialog open={showBatch} onClose={()=>setShowBatch(false)} fullWidth={true} maxWidth="lg" aria-label='form-dialog-title'>
                <DialogTitle>
                    <Typography variant="h5" component="div">
                        Batch Information for Entity: {treeElement.name}
                    </Typography>
                </DialogTitle>

                <DialogContent>
                    <VirtualTable
                        dataSet={entityBatches}
                        headCells={batchInfoHeaders}
                        initialSortedBy={{name: ''}}
                        maxHeight="500px"
                        dataIdentifier="batchId"
                        saveKey="upgradeBatchesForEntitiesTableHead"
                        tableName={'upgradeBatchesForEntitiesTable'}
                    />
                    <div className="formButtons">
                        <button className="formButton1" onClick={()=>setShowBatch(false)} type="button">
                            <Block />
                            <span> Close</span>
                        </button>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
        :
            <div id={treeElement.id + "loading"} className='treeNodeContents' style={{backgroundColor:highlight.backgroundColor, transition:highlight.transition, borderBottom: highlight.border}}>
                <Accordion
                    classes={accordionClass}    
                    expanded={infoShowing.includes(treeElement.id)}
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    variant="elevation"
                    color="primary"
                    onContextMenu={handleRightClick}
                    onClick={handleLeftClick}
                >                    
                    <AccordionSummary>
                        <div className='spinnerAsset'/>
                        <Typography style={{marginLeft: "1rem", }}> Loading... </Typography>
                    </AccordionSummary>
                </Accordion>
            </div>
        }
        </div>
    )
}
