import {useState, useEffect } from 'react';
import MinimizeIcon from '@mui/icons-material/Remove';
import ReadMoreIcon from '@mui/icons-material/ReadMore';
import { assetListStyle} from '../styles/NavStyles';
import { Button, ButtonGroup,} from '@themesberg/react-bootstrap';
import { useDispatch } from "react-redux";
import { DeptAPI } from "../api/index" 
import { Form } from '@themesberg/react-bootstrap';
import {ScaleSyncGraph} from '../dashboard/widgets/ScalesValuesChart'
import { faHeartbeat } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {NotificationDetails} from '../assetDetails/NotificationDetails'
import AccessControl from "../common/AccessPermissions"
import logger from '../utils/logger';
import { useRspHandler } from '../utils/ResponseProvider';
import { useAppSelector } from '../../state';
import { Department } from '../../types/asset/AssetTypes';
import { Node, lazyNode } from '../../types/asset/TreeTypes';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { ASSIGN_DEPT_PROFILES } from '../../state/actions/accessActions';
import { ResponseType } from '../../types/storeTypes';

export default function DeptViewDetails(treeElement : lazyNode | Node, minimizeFunction : (e : React.MouseEvent<HTMLElement, MouseEvent>, id: string) => void, infoShowing : string[]){
    const classes : any = assetListStyle();
    const [currentDept, setCurrentDept] = useState<Department | null>(null);
    const [deptScales, setDeptScales] = useState<ScaleDevice[]>([]);
    const [callUseEffect, setCallUseEffect] = useState<number | null>(null);
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const reduxProfiles = useAppSelector((state) => state.profile.profiles);
    const [profiles, setProfiles] = useState([...reduxProfiles])
    const departments = useAppSelector((state) => state.asset.depts);
    const pricingZones = useAppSelector(state => state.asset.pricingZones)
    const reduxZonePricing = useAppSelector(state => state.config.zonePricing.zonePricing)
    const [zonePricing, setZonePricing] = useState(reduxZonePricing === "true")

    const roles = useAppSelector((state) => state.access.roles);
    const user = useAppSelector((state) => state.access.account);
    const domains = useAppSelector((state) => state.access.domains)

    const [openViewNotDetails, setOpenViewNotDetails] = useState(false);
    const [notificationDetails, setNotificationDetails] = useState({});

    useEffect(()=>{
        if(!treeElement.id){
            return
        }

        if (treeElement.id.includes("|")) {
            let deptId = treeElement.id.split("|")[1]
            setCurrentDept(departments[deptId])
        } else {
            setCurrentDept(departments[treeElement.id])
        }

        let newScaleList : ScaleDevice[] = []
        Object.keys(treeElement.children).forEach((itemKey) =>{
            let item = treeElement.children[itemKey]
            if(item.properties){
                newScaleList.push(item.properties)
            }
        })
        setDeptScales(newScaleList)

    }, [callUseEffect, departments])
    useEffect(() => {
        setProfiles(reduxProfiles.sort((a,b) => a.name.localeCompare(b.name, 'en', {numeric: true})))
    }, [reduxProfiles])

    const handleView = (e : React.FormEvent<HTMLSelectElement>) => {
        const profileIndex = reduxProfiles[e.currentTarget.options.selectedIndex-1].profileId;
        logger.info("Setting current profile to " + profileIndex)
        let tmp = {...currentDept} as Department
        tmp.defaultProfileId = profileIndex 
        setCurrentDept(tmp)

        // if(treeElement.id){
        //     //setShowAlert([true, "Error Parsing out Banner/Region Info"])
        //     logger.error("(DeptViewDetails) Invalid Location ID")
        //     return
        // }
        
        const newID = Date.now()
        function onSuccess(response : ResponseType){
            dispatch(DeptAPI.fetchDepartment())
        }
        function onFail(response : ResponseType){
            logger.error("(GetStatus) Error getting scale status", response)
            setCallUseEffect(Date.now())
        }
        if(addCallback(callbacks, newID, "Assign Department Profile", onSuccess, onFail, {successMsg:"Successfully set department profile!", failMsg:"default"})){
            dispatch(DeptAPI.assignDeptProfile(newID, tmp.deptId, tmp.requiresDomainId, profileIndex, treeElement["path"]))
        }

    };

    const disableKey = (e : any) => {
       const ev = e ? e : window.event;
    
       if (ev) {
           if   (ev.preventDefault)     ev.preventDefault();
           else                         ev.returnValue = false;         
       } 
    }

    if(infoShowing.length === 0){
        return null
    }

    return (
        <div className="assetBar" style={{width: '100%'}}>
            {/* {GetMessageView('Error', "TODO: Need to calculate Department Status")} */}

            <AccessControl
                user={user}
                requiredDomain={currentDept ? domains[currentDept.requiresDomainId] : undefined}
                requiredRole={roles[ASSIGN_DEPT_PROFILES]}
            >
                <div className={classes.labelRoot} style={{marginBottom: 5}}>
                    <Form.Label>Department Profile:</Form.Label> 
                    <Form.Select id="profileSelector" className="profileSelector" style={{margin: "0px 5px",backgroundColor: "transparent"}}
                        onChange={(e) => handleView(e)}
                        onKeyDown={(e) => disableKey(e)}
                        onClick={(e)=> e.stopPropagation()}
                        value={currentDept == null ? "" : currentDept.defaultProfileId ?? ""}
                    >
                        <option value="" disabled>--Select a Profile--</option> : null
                        
                        {reduxProfiles.map(profile => (
                            <option key={profile.profileId} value={profile.profileId}>
                                {profile.name}
                            </option>
                        ))}
                    </Form.Select>

                    {zonePricing &&
                        <>
                            <Form.Label>Profile Pricing Zone:</Form.Label>
                            <Form.Control id="profileSelector" className="profileSelector" style={{marginLeft: 5,backgroundColor: "transparent"}} disabled
                                placeholder={pricingZones && Object.keys(pricingZones).length > 0 ? reduxProfiles.filter(profile => profile.profileId === currentDept?.defaultProfileId)[0].zoneId ?
                                pricingZones[reduxProfiles.filter(profile => profile.profileId === currentDept?.defaultProfileId)[0].zoneId].name : "None" : "None"}/>
                        </>
                    }
                </div>
            </AccessControl>
            
            <div style={{borderRadius:"0.5rem", border:"0.0625rem solid #d1d7e0", padding:5}}>
                <ScaleSyncGraph
                    title={<Form.Label>Department Scale Status:</Form.Label>}
                    graphId={treeElement.id}
                    deviceList={deptScales}
                />
            </div>
            

            <div className={classes.labelRoot}>
                <ButtonGroup>
                {/* Buttons are not implemented */}
                {/* <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(e)=> {e.stopPropagation()}}>
                    <FontAwesomeIcon icon={faHeartbeat} className="me-2 fa-lg" />
                    Manual Heartbeat
                </Button>

                {/* TODO?: Logs are permitted to any level here, but maybe we would want to change that? */}
                {/* <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(e)=> e.stopPropagation()}>
                    <ReadMoreIcon />
                    View Logs
                </Button>  */}
                </ButtonGroup>

                <Button className="minimizeButton" variant="link"
                onClick={(e) => {
                    minimizeFunction(e, treeElement.id)
                    }}>
                    <MinimizeIcon/>
                    Minimize
                </Button>
            </div>

            {openViewNotDetails ? 
            <NotificationDetails
                title="Notification Details"
                data={notificationDetails}
                openPopup={openViewNotDetails}
                setOpenPopup={setOpenViewNotDetails}/> : null}
        </div>
    )
}