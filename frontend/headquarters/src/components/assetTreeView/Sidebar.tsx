import React, { useState } from "react"
import { Accordion, Col, Form, Row, Button } from "react-bootstrap"
import { useForm } from "react-hook-form"
import AccordionHeader from "react-bootstrap/esm/AccordionHeader"
import AccordionBody from "react-bootstrap/esm/AccordionBody"
import { AssetActions, useAppSelector } from "../../state"
import { ScaleDevice } from "../../types/asset/ScaleTypes"
import { useDispatch } from "react-redux"
import { FormInput } from "../forms/FormHelper"
import { validateIPv4, verifyUuid } from "../utils/utils"
import Alert from '@mui/material/Alert';
import * as FilterAPI from "../api/ui-controller/filterAPI"
import { useRspHandler } from "../utils/ResponseProvider"
import { AssetAPI, ScaleAPI } from "../api"
import { notify } from "reapop"

interface sideBarProps {
    handleClose: () => void
    sidebarData: {}
}
interface sideBarFormProps {
    filteritem1: string
    startIP: string
    endIP: string
}
export interface filter{
    filterId: string
    header: string []
    numScales: Number 
}
export const filterHeaders = {
    ipAddress: "IP Range",
    bannerId: "Banners",
    regionId: "Regions",
    storeId: "Stores",
    deptId: "Departments",
    profileId: "Profiles",
    scaleModel: "Scale Model",
    application: "Firmware Version",
    appHookVersion: "Apphook Version",
    isPrimaryScale: "Primary/Secondary",
    enabled: "Enabled",
    operatingSystem: "Operating System",
    loader: "Loader",
}


function SideBarForm(props: sideBarProps) {
    const { register, handleSubmit, formState: { errors } } = useForm<sideBarFormProps>();
    const dispatch = useDispatch();
    const {callbacks, addCallback} = useRspHandler();
    const reduxProfiles = useAppSelector((state) => state.profile.profiles)
    const reduxDepts = useAppSelector((State) => State.asset.depts)
    const reduxStores = useAppSelector((State => State.asset.stores))
    const reduxBanners = useAppSelector(state=>state.asset.banners)
    const reduxRegions = useAppSelector(state=>state.asset.regions)

    const [IPSelectObjects, setIPSelectObject] = useState({})
    const [IPErrorObjects, setIPErrorObjects] = useState({})
    const { handleClose, sidebarData } = props
    const [filterData, setFilterData] = useState<{[key: string]: (string | undefined)[]}>({})

    function handleScaleIps(startIP: string, endIP: string) {
        let startIPArray = startIP.split(".")
        let endIPArray = endIP.split(".")
        //add a system check for startIpArray[0] and [1]
        for (let i = parseInt(startIPArray[2]); i <= parseInt(endIPArray[2]); i++) {
            for (let j = parseInt(startIPArray[3]); j <= parseInt(endIPArray[3]); j++) {
                if (!filterData["ipAddress"]) {
                    filterData["ipAddress"] = []
                }
                filterData["ipAddress"].push(startIPArray[0] + "." + startIPArray[1] + "." + i + "." + j);
            }
        }
    }

    const handleAdd = handleSubmit(async (data: sideBarFormProps) => {
        if (Object.keys(filterData).length === 0) {
            dispatch(notify("Filter contents are empty. Please double check you have the filters you want selected.", "warning"))
            handleClose()
            return
        }

        let IP_IDs = Object.keys(IPSelectObjects)
        for (let IP_ID of IP_IDs) {
            let start = "startIP" + IP_ID
            let end = "endIP" + IP_ID
            if (data[start] && data[end]) {
                if (data[start].length > 0 && !validateIPv4(data[start])) {
                    IPErrorObjects[IP_ID] = <Alert id={"statusAlert" + IP_ID} className="m-1" variant="filled" severity={"error"}>{"The start IP address is invalid"}</Alert>
                    setIPErrorObjects(IPErrorObjects)
                    return
                }
                if (data[end].length > 0 && !validateIPv4(data[end])) {
                    IPErrorObjects[IP_ID] = <Alert id={"statusAlert" + IP_ID} className="m-1" variant="filled" severity={"error"}>{'The end IP address is invalid'}</Alert>
                    setIPErrorObjects(IPErrorObjects)
                    return
                }
                /*add IP range to current Filter*/
                if (data[start].length > 0)
                    handleScaleIps(data[start], data[end])
            }
        }
        // parse FilteredData into an array of filterObjects
        let acc = Object.keys(filterData).map(key => {
            return {filterId : 'tmpKey', prop : key, values : filterData[key].join(",")}
        })
        function onSuccess(){
            dispatch(FilterAPI.fetchFilters())
            dispatch(AssetAPI.fetchFilteredAssets(Date.now()))
            dispatch(AssetAPI.refreshLazyFilterTree(Date.now()+1, []))
            dispatch(AssetActions.setLazyFilterExpandedTreeItems([]))
            handleClose()
        }
        function onFail(){
            handleClose()
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "Save Filter", onSuccess, onFail)){
            dispatch(FilterAPI.postFilter(newID, acc))
        }
    })

    function handleInput(checked: boolean, header: string, value: string) {
        if (checked) {
            if (!filterData[header]) {
                filterData[header] = []
            }
            filterData[header].push(value);
        }
        else {
            if (filterData[header]) {
                let valueArray = filterData[header].map((item) => { if (item !== value) { return item } });
                filterData[header] = valueArray ? valueArray : [];
            }
        }
    }
    function addIPRangeElement() {
        let IP_ID = Object.keys(IPSelectObjects).length + 1
        while (Object.keys(IPSelectObjects).includes(IP_ID.toString())) {
            IP_ID++
        }
        let startID = "startIP" + IP_ID
        let endID = "endIP" + IP_ID
        IPSelectObjects[IP_ID] =
            <>
                {<>
                    <FormInput
                        label="Start Address"
                        name={startID}
                        // name = "startIP"
                        type="text"
                        placeholder="Enter start of IP range"
                        register={register}
                        validation={{
                            maxLength: { value: 15, message: " You have exceeded the max characters" },
                        }}
                        reset
                        error={errors.startIP}
                    />
                    <FormInput
                        label="End Address"
                        name={endID}
                        // name = "endIP"
                        type="text"
                        placeholder="Enter End of IP range"
                        register={register}
                        validation={{
                            maxLength: { value: 15, message: " You have exceeded the max characters" },
                        }}
                        error={errors.endIP}
                    />
                </>}
            </>
        setIPSelectObject({ ...IPSelectObjects })
    }

    return (
        <>
            <Form className="form-control" onSubmit={handleAdd}>
                <>
                    <Button style={{ marginTop: 0, position: "sticky" }} className="formButton1" onClick={handleAdd} type="submit">
                        <span> Create Filter </span>
                    </Button>
                    {(Object.keys(IPSelectObjects).length < 1) ? addIPRangeElement() : null}
                    <Accordion  >
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>IP Range</Accordion.Header>
                            {
                                <>
                                    {
                                        (Object.keys(IPSelectObjects).length > 0) ?
                                            Object.keys(IPSelectObjects).map((key) => {
                                                return (
                                                    <Accordion.Body>
                                                        {IPSelectObjects[key]}
                                                        {IPErrorObjects[key]}
                                                        <Row style={{ marginTop: "5px" }} >
                                                            <Col>
                                                                <Button size="sm" onClick={() => { addIPRangeElement(); }}>
                                                                    Add Range
                                                                </Button>
                                                            </Col>
                                                            <>
                                                                {(Object.keys(IPSelectObjects).length > 1) ?
                                                                    <>
                                                                        <Col>
                                                                            <Button variant="danger" size="sm" onClick={() => { delete IPSelectObjects[key]; setIPSelectObject({ ...IPSelectObjects }) }}>
                                                                                Delete Range
                                                                            </Button>
                                                                        </Col>
                                                                    </>
                                                                    :
                                                                    null
                                                                }
                                                            </>
                                                        </Row >
                                                    </Accordion.Body>
                                                )
                                            })
                                            :
                                            null
                                    }
                                </>
                            }
                        </Accordion.Item>
                    </Accordion>
                    {Object.keys(filterHeaders).map((filterHeaderKey) => {
                        if (filterHeaderKey !== "ipAddress" )
                            return (
                                <>
                                    <Accordion>
                                        <AccordionHeader>
                                            {filterHeaders[filterHeaderKey]}
                                        </AccordionHeader>
                                        <AccordionBody>
                                            {sidebarData && sidebarData[filterHeaderKey] ?
                                            (Object.keys(sidebarData[filterHeaderKey])).map(value => {
                                                if (value) {
                                                    if (filterHeaderKey === "profileId") {
                                                        for (let profile of reduxProfiles) {
                                                            if (profile.profileId === value) {
                                                                return (
                                                                    <Form.Check onChange={(e) => { handleInput(e.target.checked, filterHeaderKey, value) }}
                                                                        type={'checkbox'}
                                                                        id={value + filterHeaderKey}
                                                                        label={profile.name}
                                                                    />)
                                                            }
                                                        }
                                                    }
                                                    else if (filterHeaderKey === "bannerId") {
                                                        for (let banner of reduxBanners) {
                                                            if (banner.bannerId === value) {
                                                                return (
                                                                    <Form.Check type={'checkbox'}
                                                                        id={value + filterHeaderKey}
                                                                        label={banner.bannerName}
                                                                        onChange={e => {handleInput(e.target.checked, filterHeaderKey, value)}}
                                                                    />
                                                                )
                                                            }
                                                        }
                                                    }
                                                    else if (filterHeaderKey === "regionId") {
                                                        for (let region of reduxRegions) {
                                                            if (region.regionId === value) {
                                                                return (
                                                                    <Form.Check type={'checkbox'}
                                                                        id={value + filterHeaderKey}
                                                                        label={region.regionName}
                                                                        onChange={e => {handleInput(e.target.checked, filterHeaderKey, value)}}
                                                                    />
                                                                )
                                                            }
                                                        }
                                                    }
                                                    else if (filterHeaderKey === "deptId") {
                                                        return (
                                                            <Form.Check onChange={(e) => { handleInput(e.target.checked, filterHeaderKey, value) }}
                                                                type={'checkbox'}
                                                                id={value + filterHeaderKey}
                                                                label={reduxDepts[value] ? reduxDepts[value].deptName1 : value}
                                                            />)
                                                    }
                                                    else if (filterHeaderKey === "storeId") {
                                                        return (
                                                            <Form.Check onChange={(e) => { handleInput(e.target.checked, filterHeaderKey, value) }}
                                                                type={'checkbox'}
                                                                id={value + filterHeaderKey}
                                                                label={reduxStores[value] ? reduxStores[value].storeName : value}
                                                            />)
                                                    }
                                                    else {
                                                        return (
                                                            <Form.Check onChange={(e) => { handleInput(e.target.checked, filterHeaderKey, value) }}
                                                                type={'checkbox'}
                                                                id={value + filterHeaderKey}
                                                                label={value}
                                                            />)
                                                    }
                                                }
                                            }) : null}
                                        </AccordionBody>
                                    </Accordion>
                                </>
                            )
                    })}
                </>
            </Form>
        </>
    )
}
//create props
interface SidebarProps {
    handleClose :  () => void
    scales : ScaleDevice[]
    setScales : React.Dispatch<React.SetStateAction<ScaleDevice[]>>
}
function Sidebar(props : SidebarProps) { // will return the contents of the side bar

    const reduxBanners = useAppSelector(state=>state.asset.banners)
    const reduxRegions = useAppSelector(state=>state.asset.regions)
    const reduxStores = useAppSelector(state=>state.asset.stores)
    const reduxDepts = useAppSelector(state=>state.asset.depts)
    const unnassignedDeptId = "11111111-1111-1111-1111-111111111111";
    const unassignedStoreId = "11111111-1111-1111-1111-111111111111";
    const dispatch = useDispatch();
    const { handleClose, scales, setScales} = props

    function formatScales() {
        let filterHeads = Object.keys(filterHeaders);
        let sidebarData = {}
        if (!scales || scales.length === 0) {
            dispatch(ScaleAPI.fetchScales(Date.now(), undefined, null, null, setScales))
            return {};
        }

        for (let scale of scales) {
            if (scale["storeId"] !== unassignedStoreId && scale["deptId"] !== unnassignedDeptId) {
                for (let prop of filterHeads) {
                    if (!sidebarData[prop])
                        sidebarData[prop] = {};
                    if (scale.hasOwnProperty(prop))
                        sidebarData[prop][scale[prop]] = scale[prop]
                }
            }
        }
        for (let banner of reduxBanners) {
            sidebarData["bannerId"][banner.bannerId] = banner.bannerId
        }
        for (let region of reduxRegions) {
            sidebarData["regionId"][region.regionId] = region.regionId
        }

        Object.keys(sidebarData).forEach(prop => {
            let first = Object.keys(sidebarData[prop])[0]
            if (verifyUuid(first)) {
                sidebarData[prop] = Object.fromEntries(Object.entries(sidebarData[prop])
                    .sort(([a,],[b,]) => 
                        prop.includes("banner") ? reduxBanners[reduxBanners.findIndex(ban => ban.bannerId === a)].bannerName.toLocaleLowerCase()
                            .localeCompare(reduxBanners[reduxBanners.findIndex(ban => ban.bannerId === b)].bannerName.toLocaleLowerCase(), 'en', {numeric: true}) :
                        prop.includes("region") ? reduxRegions[reduxRegions.findIndex(reg => reg.regionId === a)].regionName.toLocaleLowerCase()
                            .localeCompare(reduxRegions[reduxRegions.findIndex(reg => reg.regionId === b)].regionName.toLocaleLowerCase(), 'en', {numeric: true}) :
                        prop.includes("store") ? reduxStores[a].storeName.localeCompare(reduxStores[b].storeName, 'en', {numeric: true}) : 
                        prop.includes("dept") ? reduxDepts[a].deptName1.localeCompare(reduxDepts[b].deptName1, 'en', {numeric: true}) : 0
                    ))
            } else {
                sidebarData[prop] = Object.fromEntries(Object.entries(sidebarData[prop]).sort())
            }
        })

        return sidebarData
    }
    
    return (
        <SideBarForm
            handleClose={handleClose}
            sidebarData={formatScales()}
        />
    )
}

export { Sidebar }