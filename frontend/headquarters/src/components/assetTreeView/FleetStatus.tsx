import { faHome } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Cancel, CheckCircleOutline, CloudOff, FileDownload, HelpOutline, LowPriority, Radar, ReadMore, RemoveCircle, Sync, WarningAmber } from "@mui/icons-material"
import { Breadcrumb, Button, ButtonGroup, Col, Row } from "@themesberg/react-bootstrap"
import { Link } from "react-router-dom"
import logger from "../utils/logger"
import { useState, useEffect, useRef, useMemo } from "react";
import type { Virtualizer } from '@tanstack/react-virtual';
import type { SortingState } from '@tanstack/react-table';
import { ScanRangeForm } from "./ScanRangeForm"
import MaterialReactTable, { MRT_ColumnDef } from "material-react-table"
import SockJsClient from 'react-stomp';
import {Tooltip, Typography} from "@material-ui/core"
import { HighlightOff } from "@material-ui/icons"
import { useDispatch } from "react-redux"
import { SocketAPI } from "../api/index"
import { store } from "../../state"
import { ScaleStatusDTO } from "../../types/socket/socket"
import { Endpoints, getURL } from "../api/responseTags"
import { useRspHandler } from "../utils/ResponseProvider"
import { ResponseType } from "../../types/storeTypes"
import { format } from "date-fns"
import { findScaleNodes, formatBytes, isValidApphookVersion } from "../utils/utils"
import { notify } from "reapop"
import { lazyNode } from "../../types/asset/TreeTypes"
import { ImportScaleListForm } from "./ImportScaleListForm"
import { ExportFleetStatusForm } from "./ExportFleetStatusForm"
import * as types from "../api/responseTags"

interface FleetStatusProps {

}

enum PingStatus {
    PINGING = "PINGING",
    ONLINE = "ONLINE",
    OFFLINE = "OFFLINE",
    ERROR = "ERROR", 
    WARNING = "WARNING",
    UNKNOWN = "UNKNOWN"
}

enum ScanButtonStatus {
    DISABLED = "DISABLED",
    SCANNABLE = "SCANNABLE",
    SCANNING = "SCANNING",
    CANCELLING = "CANCELLING"
}

export interface ScaleStatusTableType {
    ipAddress: string;
    hostname: string | null;
    scaleModel: string | null;
    lastReportTimestampUtc: string | null;
    applicationVersion: string | null;
    apphookVersion: string | null;
    viewDetails: string | null;
    pingStatus: PingStatus;
    storeId: string | null;
    pluCount: number | null;
    totalMemory: number | null;
    freeMemory: number | null;
    statusMessage: string;
    error: string;
}

/*
Function to display ping status of a scale
When hovering over the icon, it will display the status message
*/
const PingStatusIcon = (props : {pingStatus : PingStatus, ipAddress : string, statusMessage : string}) => {
    const {pingStatus, ipAddress, statusMessage} = props;
    let icon : JSX.Element = <HelpOutline style={{color:"grey"}}/>;
    switch(pingStatus){
        case PingStatus.PINGING:
            icon = <Sync style={{color:"grey", animation: "spin 3s linear infinite"}}/>;break;
        case PingStatus.ONLINE:
            icon = <CheckCircleOutline style={{color:"#4bd28f"}}/>;break;
        case PingStatus.OFFLINE:
            icon = <HighlightOff style={{color:"#ff4d4d"}}/>;break;
        case PingStatus.ERROR:
            icon = <CloudOff style={{color:"#ff4d4d"}}/>;break;
        case PingStatus.WARNING:
            icon = <WarningAmber style={{color:"#ffaa00"}}/>;break;
        case PingStatus.UNKNOWN:
            icon = <HelpOutline style={{color:"grey"}}/>;break;
    }
    return (
        <Tooltip className="info" title={<Typography>{statusMessage && statusMessage !== "" ? statusMessage : "No Status Message"}</Typography>}>
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
            {icon}
            <Typography style={{marginLeft: "1rem"}}>{ipAddress}</Typography> 
            </div>
        </Tooltip>
    )
}

const PrintApplicationVersion = (props : {applicationVersion : string | null}) : JSX.Element => {
    const {applicationVersion} = props;
    //Display nothing if applicationVersion is null
    if(applicationVersion === null){
        return <Typography style={{color: "grey"}}></Typography>
    }
    //Display warning icon if applicationVersion is 4.1.0, as there is an issue with the ping response on 4.1.X versions
    if(applicationVersion === "4.1.0"){
        return(
            <Tooltip className="info" title={<Typography>{"Minor version may not be accurate, there is an issue with 4.1.X ping responses"}</Typography>}>
                <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                    <WarningAmber style={{color:"#ffaa00"}}/>
                    <Typography style={{marginLeft: "1rem"}}>{applicationVersion}</Typography> 
                </div>
            </Tooltip>
        )
    }
    //Else just display the applicationVersion
    return <Typography>{applicationVersion}</Typography>
}

export const FleetStatus = ( props: FleetStatusProps) => {

    const [openPopup, setOpenPopup] = useState<"RANGE" | "IMPORT" | "EXPORT" | null>(null);

    const [scaleIPList, setScaleIPList] = useState<string[]>([]);
    const [socketStatus, setSocketStatus] = useState<"UNKOWN" | "CONNECTED" | "DISCONNECTED">("UNKOWN");
    const [updateStatusList, setUpdateStatusList] = useState<boolean>(false);
    const [scaleStatusList, setScaleStatusList] = useState<{[key : string] : ScaleStatusTableType}>({});
    const {callbacks, addCallback} = useRspHandler();

    const dispatch = useDispatch();

    const rowVirtualizerInstanceRef =
    useRef<Virtualizer<HTMLDivElement, HTMLTableRowElement>>(null);
    const [isLoading, setIsLoading] = useState(true);
    const [scanButtonStatus, setScanButtonStatus] = useState<ScanButtonStatus>(ScanButtonStatus.DISABLED);
    const [sorting, setSorting] = useState<SortingState>([]);
    const columns = useMemo<MRT_ColumnDef<ScaleStatusTableType>[]>( //TS helps with the autocomplete while writing columns
        () => [
            {accessorKey: 'ipAddress', header: 'IP Address',
            Cell: ({ cell, row }) => (
                <PingStatusIcon
                    pingStatus={row.original.pingStatus}
                    ipAddress={row.original.ipAddress}
                    statusMessage={row.original.statusMessage}
                />
              ),
            },
            {accessorKey: 'hostname', header: 'Hostname'},
            {accessorKey: 'scaleModel', header: 'Device Type'},
            {accessorKey: 'lastReportTimestampUtc', header: 'Last Heartbeat',
                Cell: ({ cell, row }) => (
                    <Typography>{cell.getValue<string>() ? format(Date.parse(cell.getValue<string>()), "PPp") : null}</Typography>
                ),
            },
            {accessorKey: 'applicationVersion', header: 'Application Version',
                Cell: ({ cell, row }) => (
                    <PrintApplicationVersion applicationVersion={cell.getValue<string>()}/>
                ),
            },
            {accessorKey: 'apphookVersion', header: 'Apphook Version'},
            {accessorKey: 'storeId', header: 'Store ID'},
            {accessorKey: 'pluCount', header: 'PLU Count'},
            {accessorKey: 'totalMemory', header: 'Total Memory',
                Cell: ({ cell, row }) => (
                    <Typography>{formatBytes(cell.getValue<number>())}</Typography>
            )},
            {accessorKey: 'freeMemory', header: 'Free Memory',
                Cell: ({ cell, row }) => (
                    <Typography>{formatBytes(cell.getValue<number>())}</Typography>
            )},
        ],[],
    );

    useEffect(() => {
        if(store.getState().asset.selected.length > 0){
            setIsLoading(true);
            new Promise(function(resolve, reject) {
                let newFleetList = getScaleStatus(store.getState().asset.selected, store.getState().asset.lazyTree)
                if(Object.keys(newFleetList).length > 0){
                    setScaleStatusList(newFleetList);
                    setScanButtonStatus(ScanButtonStatus.SCANNABLE);
                }
                resolve("done");
            })
            .then(function(result){
                setIsLoading(false);
            })

        }
        else if (typeof window !== 'undefined') {
            setIsLoading(false);
        }
    }, []);

    useEffect(() => {
        //scroll to the top of the table when the sorting changes
        rowVirtualizerInstanceRef.current?.scrollToIndex(0);
      }, [sorting]);

    useEffect(() => {
        if(!updateStatusList){return;}

        //Convert the scaleIPList to a list of scaleStatus objects
        let scaleStatusList: {[key : string] : ScaleStatusTableType} = {};
        scaleIPList.forEach((ipAddress) => {
            scaleStatusList[ipAddress] = {
                ipAddress: ipAddress,
                hostname: null,
                scaleModel: null,
                lastReportTimestampUtc: null,
                applicationVersion: null,
                apphookVersion: null,
                viewDetails: null,
                error: "",
                pingStatus: PingStatus.UNKNOWN,
                statusMessage: "",
                storeId: null,
                pluCount: null,
                totalMemory: null,
                freeMemory: null
            }
        })

        //Update the scaleStatusList based on current scale information from the redux store
        const scales = store.getState().asset.scales;
        Object.values(scales).forEach((scale) => {
            if(scaleStatusList[scale.ipAddress]){
                scaleStatusList[scale.ipAddress].hostname = scale.hostname;
                scaleStatusList[scale.ipAddress].scaleModel = scale.scaleModel;
                scaleStatusList[scale.ipAddress].lastReportTimestampUtc = scale.lastReportTimestampUtc;
                scaleStatusList[scale.ipAddress].applicationVersion = scale.application;
            }
        })

        setScaleStatusList(scaleStatusList);

        if(Object.keys(scaleStatusList).length > 0){
            setScanButtonStatus(ScanButtonStatus.SCANNABLE);
        }
        else{
            setScanButtonStatus(ScanButtonStatus.DISABLED);
        }

        new Promise(function(resolve, reject) {setTimeout(() => resolve("done"), 500);})
        .then(function(result){setIsLoading(false);})

        setUpdateStatusList(false);

    }, [scaleIPList, updateStatusList])

    function getScaleStatus(nodeIds : string[], lazyTree : {[key: string]: lazyNode}) : {[key:string] : ScaleStatusTableType}{
        let status : {[key:string] : ScaleStatusTableType} = {};
        for(let i = 0; i < nodeIds.length; i++){
            //Locate the node from the nodeId
            let node = lazyTree[nodeIds[i]]
            
            //If you found the selected node, get the child scale ips
            if(node){
                let childScales = findScaleNodes(node.children)
                for(let j = 0; j < childScales.length; j++){
                    status[childScales[j].ipAddress] = {
                        ipAddress: childScales[j].ipAddress,
                        hostname: childScales[j].hostname,
                        scaleModel: childScales[j].scaleModel,
                        lastReportTimestampUtc: childScales[j].lastReportTimestampUtc,
                        applicationVersion: childScales[j].application,
                        apphookVersion: null,
                        viewDetails: null,
                        error: "",
                        pingStatus: PingStatus.UNKNOWN,
                        statusMessage: "",
                        storeId: null,
                        pluCount: null,
                        totalMemory: null,
                        freeMemory: null
                    }
                }
            }
        }
        return status;
    }

    //Calculates hours between the current time and the last report time
    function HoursSinceLastReport(timestamp : string) : number{
        if(!timestamp) return -1;
        const lastReportTime = new Date(timestamp);
        const currentTime = new Date();
        return Math.abs(currentTime.getTime() - lastReportTime.getTime()) / 36e5;
    }

    const onConnect = () => {
        logger.debug("Connected to websocket");
        setSocketStatus("CONNECTED");
    }

    const onDisconnect = () => {
        logger.debug("Disconnected from websocket");
        dispatch(notify('Disconnected from scan service!', 'error'))
        setSocketStatus("DISCONNECTED")
    }

    const onMessageRecieved = (msg : ScaleStatusDTO) => {
        if(!msg) return;
        logger.debug("Message Recieved: ", msg);
        let scaleStatusListCopy = {...scaleStatusList};

        if(msg.asyncState === "IDLE" && msg.asyncProgress === 100){
            setScanButtonStatus(ScanButtonStatus.SCANNABLE);
            dispatch(notify('Scan Completed!', 'success'))
        }

        if(msg.asyncState === "CANCELLED"){
            Object.values(scaleStatusListCopy).forEach((scaleStatus) => {
                if(scaleStatus.pingStatus === PingStatus.PINGING){
                    scaleStatus.pingStatus = PingStatus.UNKNOWN;
                    scaleStatus.statusMessage = "Scan was cancelled before completion";
                }
            })
            setScaleStatusList(scaleStatusListCopy);
            setScanButtonStatus(ScanButtonStatus.SCANNABLE);
            return
        }

        if(scaleStatusListCopy[msg.ipAddress]){
            if(msg.statusModel){
                scaleStatusListCopy[msg.ipAddress].scaleModel = msg.statusModel.scaleType;
                //Currently there is a bug in the port 6000 command that will return a
                //4.1.0 version despite the scale being 4.1.x, so for now we prioritize the database version
                scaleStatusListCopy[msg.ipAddress].applicationVersion = scaleStatusListCopy[msg.ipAddress].applicationVersion ?? msg.statusModel.firmwareVersion;
                scaleStatusListCopy[msg.ipAddress].apphookVersion = msg.apphookVersion;
                scaleStatusListCopy[msg.ipAddress].storeId = msg.statusModel.storeId;
                scaleStatusListCopy[msg.ipAddress].pluCount = msg.statusModel.pluCount;
                scaleStatusListCopy[msg.ipAddress].totalMemory = msg.statusModel.totalMemory;
                scaleStatusListCopy[msg.ipAddress].freeMemory = msg.statusModel.freeMemory;
            }

            //Check to see if ping has failed
            if(!msg.pingSuccess){
                scaleStatusListCopy[msg.ipAddress].pingStatus = PingStatus.OFFLINE;
                scaleStatusListCopy[msg.ipAddress].statusMessage = "Could not contact the device!";
                scaleStatusListCopy[msg.ipAddress].error = msg.message;
                return;
            }

            //Check to see if scale can support apphook
            if(isValidApphookVersion(scaleStatusListCopy[msg.ipAddress].applicationVersion ?? "")){
                //Show a warning if apphook is not installed
                if(!msg.apphookVersion){
                    scaleStatusListCopy[msg.ipAddress].pingStatus = PingStatus.WARNING;
                    scaleStatusListCopy[msg.ipAddress].statusMessage = "Apphook is not installed!";
                    scaleStatusListCopy[msg.ipAddress].apphookVersion = "Not Installed";
                    return
                }
                //Scale has an apphook installed check if it has completed a heartbeat
                let hoursSinceLastReport = HoursSinceLastReport(msg.lastReportTimestamp)
                let offlineHours = store.getState().config.offlineAwayHours.scaleOfflineHours ?? 24;

                if(hoursSinceLastReport === -1){
                    scaleStatusListCopy[msg.ipAddress].pingStatus = PingStatus.ERROR;
                    scaleStatusListCopy[msg.ipAddress].statusMessage = "Scale has not reported a heartbeat!";
                    return
                }
                else if(hoursSinceLastReport > offlineHours){
                    scaleStatusListCopy[msg.ipAddress].pingStatus = PingStatus.ERROR;
                    
                    if(hoursSinceLastReport > 24){
                        scaleStatusListCopy[msg.ipAddress].statusMessage = "Scale has not reported a heartbeat in the last " + Math.floor(hoursSinceLastReport / 24) + " days!";
                    }
                    else{
                        scaleStatusListCopy[msg.ipAddress].statusMessage = "Scale has not reported a heartbeat in the last " + hoursSinceLastReport + " hours!";
                    }
                    return
                }
            }
            else if(msg.statusModel && msg.statusModel.firmwareVersion){
                scaleStatusListCopy[msg.ipAddress].apphookVersion = "Not Supported";
            }
            
            scaleStatusListCopy[msg.ipAddress].pingStatus = PingStatus.ONLINE;
            scaleStatusListCopy[msg.ipAddress].statusMessage = "Scale is online!";
        }
        else{
            logger.error("Scale IP not found in ipAddressList: ", msg.ipAddress);
        }

        setScaleStatusList(scaleStatusListCopy);
    }

    function getScanButton() {
        switch(scanButtonStatus){
            case ScanButtonStatus.SCANNING:
                return(<>
                    <Cancel style={{marginRight: '8px'}}/>
                    Cancel Scan
                </>)
            case ScanButtonStatus.CANCELLING:
                return(<>
                    <Cancel style={{marginRight: '8px'}}/>
                    Cancelling...
                </>)
            
            case ScanButtonStatus.DISABLED:
            case ScanButtonStatus.SCANNABLE:
            default:
                return(<>
                        <Radar className="spin" style={{marginRight: '8px'}}/>
                        Scan Scales
                    </>)
        }
    }

    const headers = {
        "Authorization": types.getAccessToken()
    }

    return (
        <>
        <SockJsClient 
            url={getURL(Endpoints.messager, "")}
            topics={['/topic/fleetStatus']}
            headers={headers}
            onConnect={() => {onConnect()}}
            onDisconnect={() => {onDisconnect()}}
            onMessage={(msg : any) => {onMessageRecieved(msg)}}
            debug={true}
        />

        <div className="assetBar">

            <Breadcrumb className="d-none d-sm-inline-block" listProps={{ className: "breadcrumb-dark breadcrumb-transparent" }}>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/'}}><FontAwesomeIcon icon={faHome} /></Breadcrumb.Item>
                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/AssetList'}}>Asset List</Breadcrumb.Item>
                <Breadcrumb.Item active>Fleet Status</Breadcrumb.Item>
            </Breadcrumb>

            <Row>
                <Col>
                    <h2>Fleet Status</h2>
                    <p className="mb-4">Scans a range of scales in order to get their online status</p>
                </Col>
            </Row>

            <div className="d-flex justify-content-between">
                {/* Left button section */}
                <ButtonGroup>

                    <Button id="actionBar1" variant="outline-primary" size="sm" disabled={scanButtonStatus === ScanButtonStatus.DISABLED} onClick={(e) => {
                        if(socketStatus !== "CONNECTED"){
                            logger.debug("Socket not connected, cannot scan scales")
                            dispatch(notify("Not connected to scanning service!", "error"));
                            return
                        }
                        if(scanButtonStatus === ScanButtonStatus.SCANNABLE){
                            logger.debug("Scan Scales")
                            let scaleStatusListCopy = {...scaleStatusList};
                            Object.values(scaleStatusListCopy).forEach((scaleStatus) => {
                                scaleStatus.pingStatus = PingStatus.PINGING;
                                scaleStatus.statusMessage = "Scanning...";
                            })
                            setScanButtonStatus(ScanButtonStatus.SCANNING)
                            setScaleStatusList(scaleStatusListCopy);
                            dispatch(SocketAPI.postFleetStatus(Date.now(), Object.keys(scaleStatusListCopy)));
                        }
                        else if(scanButtonStatus === ScanButtonStatus.SCANNING){
                            logger.debug("Cancel Scan")
                            let newId = Date.now();
                            if(addCallback(callbacks, newId, "Cancel Scan", (rsp : ResponseType) => {}, (err : ResponseType) => {}, {failMsg: "default"})){
                                dispatch(SocketAPI.cancelFleetStatus(newId));
                                setScanButtonStatus(ScanButtonStatus.CANCELLING);
                            }
                        }                       
                    }}> 
                        {getScanButton()}
                    </Button>
                </ButtonGroup>
 
                <ButtonGroup>

                    {Object.keys(scaleStatusList).length > 0 ?
                    
                        <Button id="actionBar2" variant="outline-red" size="sm" onClick={(e) => {
                            logger.debug("Clear Selection")
                            setUpdateStatusList(true)
                            setScaleIPList([]);
                        }}> 
                            <RemoveCircle style={{marginRight: '8px'}}/>
                            Clear Selection
                        </Button>
                    : null}

                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={() => {
                        logger.debug("Select Scan Range")
                        setOpenPopup("RANGE");
                    }}>
                        <LowPriority />
                        Select Scan Range
                        <ScanRangeForm 
                            openPopup={openPopup === "RANGE"} 
                            setOpenPopup={setOpenPopup}
                            handleRangeUpdate={(e : string[]) => {
                                setIsLoading(true)
                                setUpdateStatusList(true)
                                setScaleIPList(e)} }
                            />
                    </Button>     

                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={() => {
                        logger.debug("Import Scale List")
                        setOpenPopup("IMPORT");
                    }}>
                        <ReadMore />
                        Import Scale List
                        <ImportScaleListForm
                            openPopup={openPopup === "IMPORT"}
                            setOpenPopup={setOpenPopup}
                            handleListUpdate={(e : string[]) => {
                                setIsLoading(true)
                                setUpdateStatusList(true)
                                setScaleIPList(e)
                            }}
                        />
                    </Button>

                    <Button id="actionBar2" variant="outline-primary" size="sm" disabled={Object.keys(scaleStatusList).length === 0 || scanButtonStatus === ScanButtonStatus.SCANNING || scanButtonStatus === ScanButtonStatus.CANCELLING} onClick={() => {
                        logger.debug("Export List")
                        setOpenPopup("EXPORT");
                    }}>
                        <FileDownload />
                        Export Results
                        <ExportFleetStatusForm
                            openPopup={openPopup === "EXPORT"}
                            setOpenPopup={setOpenPopup}
                            scaleStatusList={scaleStatusList}
                        />
                    </Button>
                </ButtonGroup>
            </div>
        </div>
        
        <MaterialReactTable
            columns={columns}
            data={Object.values(scaleStatusList)} //10,000 rows
            enableBottomToolbar={false}
            enableGlobalFilterModes
            enablePagination={false}
            enableDensityToggle={false}
            enableColumnFilters={false}
            enableRowVirtualization
            initialState={{columnVisibility: {storeId: false, pluCount: false,totalMemory: false,freeMemory: false }}}
            muiTableContainerProps={{ sx: { maxHeight: '800px' } }}
            onSortingChange={setSorting}
            state={{ isLoading, sorting }}
            rowVirtualizerInstanceRef={rowVirtualizerInstanceRef} //optional
            rowVirtualizerProps={{ overscan: 8 }} //optionally customize the virtualizer
        />
        </>
    )
}