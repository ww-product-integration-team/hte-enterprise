import React, {useState, useEffect, useMemo} from 'react';
import {Block, Check, Edit, MoreHoriz, RemoveCircle} from '@material-ui/icons';
import { accordionStyle} from '../styles/NavStyles';
import { useDispatch} from "react-redux";
import 'status-indicator/styles.css'

import Accordion from '@material-ui/core/Accordion';

// import Accordion from 'react-bootstrap/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import { AssetActions, useAppSelector } from '../../state';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

import '../assetTreeView/AssetListStyle.css'
import TableViewIcon from '@mui/icons-material/TableView';
import logger from '../utils/logger';
import StatusIndicator from '../dashboard/widgets/StatusIndicator';
import { Group, UpgradeDevice, manualUpgradeHeadCells, scaleHeadCellsUpgrade } from '../upgradeManager/UpgradeManager';
import { UpgradeAPI } from '../api/index';

//Table
import VirtualTableProps from '../common/virtualTable/VirtualTable';
import { Card } from 'react-bootstrap';
import { Checkbox, IconButton, Typography, Menu, ListItemIcon, ListItemText, Dialog, DialogTitle, DialogContent} from '@material-ui/core';
import { ButtonGroup } from '@themesberg/react-bootstrap';
import { Button } from 'react-bootstrap';

import { MenuItem } from '@mui/material';
import { ManualEditGroup } from '../upgradeManager/upgradeForms/manualGroupForm';
import { ResponseType } from '../../types/storeTypes';
import { useRspHandler } from '../utils/ResponseProvider';
interface StyledTableProps {
    filterScales:boolean
    group: Group 
    groupScales: UpgradeDevice[]
    batchUpgrade: boolean | undefined
}

// Table Styling
//=====================================================================================================
export default function StyledTableNode(props :StyledTableProps ){
    const dispatch = useDispatch();
    const {filterScales, group, groupScales, batchUpgrade} = props
    const groupId = group.groupId
    const groups = useAppSelector((state)=> state.asset.groups)
    const [openDialog, setOpenDialog] = useState(false)
    const [currentGroups, setCurrentGroups] = useState({...groups})
    const [scales, setScales] = useState<UpgradeDevice[]>(groupScales)

    //Highlight of focused asset
    //=============================================================================================
    const [highlight, setHighlight] = useState({backgroundColor: '', transition: '', border: ''})
    const focusedAsset = useAppSelector(state => state.asset.focused)
    const selected : string[] = useAppSelector(state => state.asset.upgradeSelected)
    const isSelected : boolean = group.isSelected

    function fadeIn(){setHighlight({backgroundColor: '', transition:'background-color 1s linear', border: ''})}
    function killFade(){setHighlight({backgroundColor: '', transition:'', border: ''})}
    function clearFocus(){dispatch(AssetActions.setFocusedAsset(""))}

    useEffect(()=>{
        let id = groupId;
        if((id.length === 2 && id[1] === focusedAsset) || groupId === focusedAsset){
            setTimeout(() => {setHighlight({backgroundColor: 'rgba(186,71,53,0.50)', transition:'background-color 1s linear', border: '3px solid rgba(186,71,53, 1)'})}, 100)
            setTimeout(fadeIn, 4000)
            setTimeout(killFade, 5000)
            setTimeout(clearFocus, 5000)
        }
    }, [focusedAsset])
    useEffect(()=>{
        setCurrentGroups({...groups})
    }, [groups])
    useEffect(()=>{
        setScales([...groupScales])
    },[groupScales])

    useMemo(()=> {
        settingHiglightVar(isSelected)
    },[isSelected])
    
    function settingHiglightVar(isSelected){
        if(isSelected){
            // setHighlight({backgroundColor: '', transition:'', border: ''})   
            setHighlight({backgroundColor: 'rgba(45,85,119,0.25)', transition:'', border: "3px solid rgba(45,85,119,1)"})
        }
        else{
            setHighlight({backgroundColor: '', transition:'', border: ''})        
        }
    }

    //Node Action Menu
    //=============================================================================================
    const [renderTreeOptions, setRenderTreeOptions] = useState(false)
    const [assetMenu, setAssetMenu] = useState<Element | null>(null);
    const isAssetMenuOpen = Boolean(assetMenu);   // Show/hide asset actions menu
    const handleAssetMenuOpen = (event : React.MouseEvent<HTMLButtonElement, MouseEvent>, nodeId : string) => {
        event.stopPropagation()
        setAssetMenu(event.currentTarget)
    };
    const handleAssetMenuClose = (event) => {
        event.stopPropagation()
        setAssetMenu(null)
    };

    const assetId = 'assetActions';
    const renderAssetActions = (
        <Menu
        id={assetId}
        anchorEl={assetMenu}
        open={isAssetMenuOpen}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        keepMounted
        onClose={handleAssetMenuClose}
        >
            <MenuItem onClick={() => {setOpen(true); handleClose()}}>
                <ListItemIcon className="optionIcon">
                    <Edit />
                </ListItemIcon>
                <ListItemText primary="Edit Group" />
            </MenuItem>
            <MenuItem onClick={()=>{setOpenDialog(true)}}>
                <ListItemIcon className="optionIcon">
                    <RemoveCircle />
                </ListItemIcon>
                <ListItemText primary="Delete Group" />
            </MenuItem>
        </Menu>
    );

    //Styles
    //=============================================================================================
    const accordionClass = accordionStyle();

    const [anchorEl, setAnchorEl] = useState<Element | null | undefined>(null);
    const [xPos, setXPos] = useState("0px");
    const [yPos, setYPos] = useState("0px");
    const isOpen = Boolean(anchorEl);   // Show/hide asset actions menu

    const handleRightClick = (event : any) => {
        if (!anchorEl) {
            event.preventDefault();
            event.stopPropagation();        
        }

        setXPos(event.clientX);
        setYPos(event.clientY);
        setAnchorEl(event.currentTarget);
    };

    const handleLeftClick = (event : any) =>{
        let elementId = groupId     
        
        if(event.ctrlKey){
            logger.debug("CTRL key selected")
            let newSelected = [...selected]
            if(newSelected.includes(groupId)){
                // newSelected.splice(newSelected.indexOf(treeElement.id), 1)
                newSelected  = newSelected.filter((nodeId)=>{if(!nodeId.includes(groupId)) return nodeId})
            }
            else{
                newSelected.push(elementId)
            }
            
            // dispatch(AssetActions.setSelectedItems(newSelected))
        }
        // else{
        //     if(selected.includes(treeElement.id)){
        //         dispatch(AssetActions.setSelectedItems([]))
        //     }
        //     else{
        //         dispatch(AssetActions.setSelectedItems(elementIds))
        //     }
        // }
    }
    
    const handleClose = () => {
        setAnchorEl(null);
    }

    const {callbacks, addCallback} = useRspHandler()

    
    function handleDelete(){
        function onSuccess(response: ResponseType) {
            logger.info("Group Deleted Successfully")
            let tempGroups = {...currentGroups}
            delete tempGroups[groupId]

            setCurrentGroups(Object.assign({}, tempGroups))
            dispatch(AssetActions.setGroups({...tempGroups}))
        }
        function onFail(response: ResponseType) {
            logger.error("Error Deleting Group")
        }

        const newID = Date.now()
        if (addCallback(callbacks, newID, "DeleteGroup", onSuccess, onFail, {requestMsg:"Deleting Group...", successMsg:"Group Deleted Successfully", failMsg:"Error Deleting Group"})) {
            dispatch(UpgradeAPI.deleteGroup(newID, groupId))
        }

        handleClose()
    }

    const [open, setOpen] = useState(false)

    function CustomToggle({ toggleAccordion }) {
        // const decoratedOnClick = useAccordionButton(eventKey);
        // TODO: add Rotation of Arrow click
        return (
                <>
                    <ArrowDropDownIcon 
                    type="button"
                    onClick={toggleAccordion}
                    />
                </>  
            );

                
      }
    //Content
    //=============================================================================================
    const [expand, setExpand] = React.useState(false);
    const toggleAcordion = () => {
        setExpand((prev) => !prev);
    };
    return(
        <div id={groupId + "content"} >
                <Accordion   
                    classes={accordionClass}
                    aria-controls="customized-menu"
                    aria-haspopup="true"
                    color="primary"
                    expanded={expand}
                    onContextMenu={handleRightClick}
                    onClick={handleLeftClick}
                >
                <div className='StyledTable' style={{backgroundColor:highlight.backgroundColor, transition:highlight.transition, borderBottom: highlight.border}}>
                <AccordionSummary 
                style = {{marginTop: "2px"}}>
                    <CustomToggle  toggleAccordion={toggleAcordion}></CustomToggle>
                        <Checkbox 
                            checked = {group.isSelected}
                            indeterminate={false}
                            onChange={() =>{currentGroups[groupId] = {...group, isSelected: !group.isSelected}; dispatch(AssetActions.setGroups({...currentGroups}))}}
                            inputProps={{ 'aria-label': 'select all products' }}
                        />
                        <TableViewIcon/>
                        <Typography  variant='body2' className='treeNodeLabel'>
                            Group: {group.groupName}
                        </Typography>
                    {group.batchGroup ? (group.status === "ENABLED")?
                        <>
                            <StatusIndicator positive />
                            <Typography variant='body2' className='treeNodeLabel'>
                                Enabled
                            </Typography>
                        </>
                        :
                        <>
                            <StatusIndicator style={{ backgroundColor: '#a4abb0' }} />
                            <Typography variant='body2' className='treeNodeLabel'>
                                Disabled
                            </Typography>
                        </>
                        :
                        null
                    }
                    {renderTreeOptions ? renderAssetActions : null}
                    <IconButton
                        aria-label="show actions" aria-controls={assetId} aria-haspopup={true} 
                        onClick={e => {
                            setRenderTreeOptions(true)
                            handleAssetMenuOpen(e, groupId)
                        }} color="inherit">
                        <MoreHoriz />
                    </IconButton>

                </AccordionSummary>
                </div>
                <Card>
                    <AccordionDetails>
                        <Card.Body>
                        {(filterScales)?
                            <VirtualTableProps
                                tableName="virtualScaleTable"
                                dataSet={scales.filter((scale)=>{if(selected.includes(scale.upgradeId)) return(scale)})}
                                headCells={batchUpgrade ? scaleHeadCellsUpgrade : manualUpgradeHeadCells}
                                initialSortedBy={{ name: '' }}
                                dataIdentifier= "upgradeId"
                                dispatchedUrls={[UpgradeAPI.getDevices]}
                                saveKey="virtualScaleTableHead"
                                selectedRows={selected}
                                changeReduxUpgradeSelected={true}
                                batchUpgrade={batchUpgrade}
                            />
                            :
                            <VirtualTableProps
                                tableName="virtualScaleTable"
                                dataSet={scales}
                                headCells={batchUpgrade ? scaleHeadCellsUpgrade : manualUpgradeHeadCells}
                                initialSortedBy={{ name: '' }}
                                dataIdentifier= "upgradeId"
                                dispatchedUrls={[UpgradeAPI.getDevices]}
                                saveKey="virtualScaleTableHead"
                                selectedRows={selected} 
                                changeReduxUpgradeSelected={true}
                                batchUpgrade={batchUpgrade}
                            />
                        }
                        </Card.Body>
                    </AccordionDetails>
                </Card>
                
            </Accordion>
            <Menu
                id="customized-menu"
                style={{contextMenu: { pointerEvents: 'none' }} as any} 
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
                elevation={0}
                anchorEl={anchorEl}
                anchorReference="anchorPosition"
                anchorPosition={
                    yPos !== null && xPos !== null
                    ? { top: Number(yPos), left: Number(xPos) }
                    : undefined
                }
            >
                <MenuItem onClick = {()=>{setOpen(true); handleClose()}}>
                    <ListItemIcon className="optionIcon">
                        <Edit />
                    </ListItemIcon>
                    <ListItemText primary="Edit Group" />
                </MenuItem>
                <MenuItem onClick={()=>{setOpenDialog(true)}}>
                    <ListItemIcon className="optionIcon" >
                        <RemoveCircle />
                    </ListItemIcon>
                    <ListItemText primary="Delete Group" />
                </MenuItem>
                {/* {GetTreeItemOptions(treeElement, handleShowDetails, handleAssetMenuOpen, handleAssetMenuClose, anchorEl, setAnchorEl)} */}
            </Menu>
           { (open)?
            <ManualEditGroup
                openPopup={open}
                setOpenPopup={setOpen}
                group={group}
                />   
            : null}       
            {/*Delete group Dialog*/}
            <Dialog open={openDialog} onClose={()=>{setOpenDialog(false);}} fullWidth={true} maxWidth="sm">
                <DialogTitle>
                    <Typography variant="h5" component="div">Are You Sure?</Typography>
                </DialogTitle>

                <DialogContent>
                    <Typography variant="body1" component="div" style={{"marginBottom": "1em"}}>
                        You are about to delete the group: <b>{group.groupName}</b> .
                    </Typography>

                    <ButtonGroup className="formButtons">
                        <Button className="formButton1" onClick={()=>{setOpenDialog(false); handleDelete(); }}>
                            <Check/>
                            <span> I'm Sure</span>
                        </Button>
                        <Button className="formButton1" onClick={()=>setOpenDialog(false)}>
                            <Block/>
                            <span> Cancel</span>
                        </Button>
                    </ButtonGroup>
                </DialogContent>
            </Dialog>  
        </div>
    )
    
}
