import {useEffect, useState} from 'react';
import MinimizeIcon from '@mui/icons-material/Remove';
import {assetListStyle} from '../styles/NavStyles';
import Box from '@material-ui/core/Box';
import { Button} from '@themesberg/react-bootstrap';
import 'status-indicator/styles.css'
import format from 'date-fns/format'
import { GetMessageView } from './MessageIndicator';
import logger from '../utils/logger';
import SetEnable from '../assetDetails/detailComponents/SetEnable';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { Link } from 'react-router-dom';
import StoreActionsComponent from './StoreActionsComponent';
import { Alert, IconButton, Tooltip } from '@mui/material';
import { useFileDownloader } from '../scaleProfiles/fileDownload/FileDownloader';
import { PutStore } from '../forms/StoreForm';
import { Typography } from 'antd';
import { Edit, Help } from '@mui/icons-material';
import { AccessActions, useAppSelector } from '../../state';
import { AssetTypes, Store } from '../../types/asset/AssetTypes';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import AccessControl from '../common/AccessPermissions';
import { Node, lazyNode } from '../../types/asset/TreeTypes';
import { useDispatch } from 'react-redux';
import { ScaleAPI } from '../api';

export default function StoreViewDetails(treeElement : lazyNode | Node, minimizeFunction : (e : React.MouseEvent<HTMLElement, MouseEvent>, id: string) => void, infoShowing : string[]){
    const classes : any = assetListStyle();

    // Not currently allowing user to cancel requests
    //     Currently, if user does an action (ping, executeManualHeartbeat, etc.), then the request will still finish even if you leave the page.
    // const [controller, setController] = useState(axios.CancelToken.source())      // Contains amount uploaded + total

    const stores = useAppSelector((state) => state.asset.stores);
    const roles = useAppSelector((state) => state.access.roles);
    const user = useAppSelector((state) => state.access.account);
    const domains = useAppSelector((state) => state.access.domains)
    const [showAlert, setShowAlert] = useState([false, "Something went wrong!"])
    const [openEdit, setOpenEdit] = useState<AssetTypes | null>(null);            // PUT    Request
    const dispatch = useDispatch()
    const [scales, setScales] = useState<ScaleDevice[]>([])
    useEffect(() => {
        dispatch(ScaleAPI.fetchScales(Date.now(), undefined, null, null, setScales))
    }, [])

    const [downloadFile, downloaderComponentUI] = useFileDownloader();
    const download = (file : any) => {
        if(downloadFile instanceof Function){
            downloadFile(file);
        }
    };

    if(infoShowing.length === 0){
        return null
    }

    let storeInfo : Store | undefined
    let storeScales : ScaleDevice[] = []
    if(treeElement.id){
        storeInfo = stores[treeElement.id]
        storeScales = Object.values(scales).filter((scale) => scale.storeId === treeElement.id)
    }
    else{
        setShowAlert([true, "Could not fetch store information!"])
    }


    return(
        <div className="assetBar" style={{width: '100%'}}>
            <>

            {/* {GetScaleMessage(treeElement)} */}
            {GetMessageView(treeElement.nodeStatus.status, treeElement.nodeStatus.message)}
            
            {storeInfo != null ?
            <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", width: '100%',}}>
                <div className={classes.accordionRooCol}>

                    <div className={classes.accordionRoot}>
                        <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>Number of Scales: </Box>
                        <Box className="boxRegular">{storeScales.length == null ? "No Report" : storeScales.length}</Box>
                    </div>
            
                </div>
                <div className={classes.accordionRootCol}>
                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Address:</Box>
                        <Box className="boxRegular">{storeInfo.address == null ? "No Report" : storeInfo.address}</Box>
                    </div>

                    <div className={classes.accordionRoot}>
                        <Box className="boxBold">Store Number:</Box>
                        <Box className="boxRegular">{storeInfo.customerStoreNumber == null ? "No Report" : storeInfo.customerStoreNumber === "" ? "None" : storeInfo.customerStoreNumber}</Box>
                    </div>

                </div>
            </div> : null}

            {storeInfo && storeInfo.services.length > 0 ? 
                <>

                    <div style={{ display: "grid", gridTemplateColumns: "repeat(2, 1fr)", width: '100%',}}>
                        <div className={classes.accordionRooCol}>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">IP Address:</Box>
                                <Box className="boxRegular">{storeInfo.ipAddress == null ? "No Report" : storeInfo.ipAddress}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Hostname:</Box>
                                <Box className="boxRegular">{storeInfo.hostname == null ? "No Report" : storeInfo.hostname}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>URL: </Box>
                                <Box className="boxRegular">{storeInfo.url == null ? "No Report" : storeInfo.url}</Box>
                            </div>
                    
                        </div>
                        <div className={classes.accordionRootCol}>
                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Last Trigger Type:</Box>
                                <Box className="boxRegular">{storeInfo.triggerType == null ? "No Report" : storeInfo.triggerType}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Last Item Change:</Box>
                                <Box className="boxRegular">{storeInfo.lastItemChangeReceivedTimestampUtc == null ? "No Report" : storeInfo.lastItemChangeReceivedTimestampUtc === "" ? "None" : format(Date.parse(storeInfo.lastCommunicationTimestampUtc ?? ""), "PPpp")}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box style={{fontWeight: '700', margin: '4px', color : "#ba4735"}}>Last Report: </Box>
                                <Box className="boxRegular">{storeInfo.lastCommunicationTimestampUtc == null ? "No Report" : format(Date.parse(storeInfo.lastCommunicationTimestampUtc), "PPpp")}</Box>
                            </div>

                            <div className={classes.accordionRoot}>
                                <Box className="boxBold">Last Reboot:</Box>
                                <Box className="boxRegular">{storeInfo.lastRebootTimestampUtc == null ? "No Report" : format(Date.parse(storeInfo.lastRebootTimestampUtc), "PPpp")}</Box>
                            </div>
                        </div>
                    </div>

                    <div style={{display: "flex",flexWrap: "wrap",}}>
                        <SetEnable
                            user={user}
                            info={storeInfo}
                            operation="STORE"
                        />
                    </div>
                </>
        
            : 
            <AccessControl
                user={user}
                requiredDomain={domains[storeInfo? storeInfo.storeId : ""]}
                requiredRole={roles[AccessActions.MANAGE_STORES]}
            >
                <div style={{display:"flex"}}>
                    <Button id="actionBar1" size="sm" style={{padding:"0.5rem", backgroundColor:"#203140"}}
                        onClick={() => {
                        setOpenEdit(AssetTypes.Store)
                        }}> 
                        <Edit style={{marginRight:"0.5rem"}}/>
                        Setup Store Server

                        <PutStore
                            title="Setup Store"
                            openPopup={openEdit === AssetTypes.Store}
                            setOpenPopup={setOpenEdit}
                            treeElement={treeElement}
                        />

                    </Button>

                    <Tooltip className="info" title={<Typography> Once a store server is setup, please enter in it's IP address here, this tells the program that the store server IP is valid</Typography>}>
                        <IconButton aria-label="enableAccount" disableRipple>
                            <Help />
                        </IconButton>
                    </Tooltip>
                </div>
            </AccessControl>
            }
            


   
            <div style={{display: "flex",flexWrap: "wrap",}}>

                <div className={classes.labelRoot}>
                    {storeInfo ?
                    <StoreActionsComponent
                        storeInfo={storeInfo}
                        storeScales={storeScales}
                        user = {user}
                        roles = {roles}
                        download = {download}
                    /> : null}

                </div>

                <Link to={{ pathname: '/AssetDetails', state: { store: storeInfo} }}>
                    <Button id="detailsLink" className="minimizeButton" variant="link" disabled={!storeInfo} onClick={() => {
                        
                    }}> 
                        View More Details
                        <ArrowForwardIcon/>
                    </Button>
                </Link>

                <Button className="minimizeButton" variant="link" style={{justifyContent:"right"}}
                    onClick={(e) => {
                    logger.info("(ScaleViewDetails) ",treeElement)
                    minimizeFunction(e, treeElement.id)
                    }}>
                    <MinimizeIcon/>
                    Minimize
                </Button>
            </div>
            
            {showAlert[0] ? <Alert className="m-1" variant="filled" severity="error">{showAlert[1]}</Alert> : null}
            {downloaderComponentUI}
            </>
        </div>
    )
}