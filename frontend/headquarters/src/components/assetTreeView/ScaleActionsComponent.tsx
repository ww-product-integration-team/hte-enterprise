import { useState } from 'react';
import { useDispatch } from "react-redux"
import AccessControl from "../common/AccessPermissions"
import { useRspHandler } from '../utils/ResponseProvider';
import logger from '../utils/logger';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeartbeat, faFileUpload } from "@fortawesome/free-solid-svg-icons";
import { Refresh, ContactlessOutlined } from '@material-ui/icons';
import { DELETE_SCALE_DATA, HEARTBEAT, PING, REBOOT } from "../../state/actions/accessActions"
import { Dialog, DialogContent, DialogTitle, Typography } from '@material-ui/core';
import { AssetAPI, ScaleAPI, ScaleCtrlAPI } from '../api';
import { Button, ButtonGroup, Form } from '@themesberg/react-bootstrap';
import DeleteIcon from '@mui/icons-material/Delete';
import { HTeRole, HTeUser } from '../../types/access/AccessTypes';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { store, useAppSelector } from '../../state';
import { ResponseType } from '../../types/storeTypes';
import { FormInput } from '../forms/FormHelper';
import { useForm } from 'react-hook-form';
import { useLocation } from 'react-router-dom'
import { sleep } from '../utils/utils';

interface ScaleActionsComponentProps {
    scale: ScaleDevice
    user: HTeUser
    roles: { [key: string]: HTeRole }

}

export default function ScaleActionsComponent(props: ScaleActionsComponentProps) {

    interface FeatureFileUpload {
        file: any
    }

    const { scale, user, roles } = props;
    const { callbacks, addCallback } = useRspHandler();
    const dispatch = useDispatch();

    const [featureFileButtonClicks, setFeatureFileButtonClicks] = useState<number>(0)
    const [warningState, setWarningState] = useState(false)
    const [confirmData, setConfirmData] = useState({ open: false, title: "", message: "", action: () => { } });
    const [confirmFeatureFile, setConfirmFeatureFile] = useState({ open: false, title: "", message: "", action: () => { } })
    const { register, handleSubmit, formState: { errors } } = useForm<FeatureFileUpload>()

    /* Used to preclude the "Apply Feature File" button from rendering on the Asset List -- *ever* -- because the sodding
       mainTree from AssetList.tsx just cannibalizes file-based FormInputs for some reason that no one can make sense of 
    */
    const location = useLocation()
    const isAssetList = location.pathname === '/AssetList'

    const profileState = useAppSelector((state) => state.profile);
    const files = profileState.featureFiles

    const handleCloseDialog = (event: any) => {
        event.stopPropagation()
        setConfirmData({ open: false, title: "", message: "", action: () => { } });
    }

    // Evaluates whether the file is stored in an HTe Enterprise before sending it right away
    const searchForMatchingFile = handleSubmit(async (fileData: FeatureFileUpload) => {


        const fileToEvaluate = fileData.file[0]
        if (fileToEvaluate["name"].endsWith(".package.tgz") && !fileToEvaluate["name"].startsWith("hti-linux-feature")) {
            let warningMessage = "The file you are attempting to submit does not appear to be a feature file. " +
                                 "Please utilize the Upgrade Manager to apply software upgrades to your scale."
            setConfirmFeatureFile({ open: true, title: "", message: warningMessage, action: () => {} })
            return;
        }
        // Increment the click accumulator so we know that the user has tried to submit the file
        setFeatureFileButtonClicks(featureFileButtonClicks + 1)

        // The user has clicked the "Send" button again, despite the warning.
        // Reset the accumulator and let them apply the file to that scale.
        if (featureFileButtonClicks > 0) {
            setFeatureFileButtonClicks(0);
            postFeatureFile()
        }

        let matchFound = false;
        for (let i = 0; i < files.length; i++) {
            let fileNameMatches = files[i].filename === fileToEvaluate["name"]
            let profileMatches = files[i].profileId === scale.profileId

            if (fileNameMatches && profileMatches) {
                matchFound = true;
            }    
        } 
        
        if (!matchFound) {
            let warningMessage = "The file you are trying to send does not exist under this scale's profile; " +
                                 "applying this file may cause this scale to fail a sync check. " +
                                 "Are you sure you want to apply this file?"
            setWarningState(true)
            setConfirmFeatureFile({ open: true, title: "", message: warningMessage, action: () => {} });
        } else {
            postFeatureFile()
        }
    })

    const postFeatureFile = handleSubmit(async (data: FeatureFileUpload) => {
        
        // Reset the "click accumulator" in case the user tries to send another file from this page
        setFeatureFileButtonClicks(0)

        function onSuccess(response: ResponseType) {
            logger.info("File pushed successfully")
        }

        function onFail(response: ResponseType) {
            logger.info("Could not push file")
        }

        const newID = Date.now()
        const fileData = new FormData()
        const fileToSend = data.file[0]

        fileData.append('file', fileToSend)

        if (addCallback(callbacks, newID, "Send Feature File", onSuccess, onFail, 
        { requestMsg: "Sending feature file...", successMsg: "Successfully sent feature file!", failMsg: "default" })) {
            dispatch(AssetAPI.pushOneOffFeature(newID, fileData, scale.ipAddress))
            setConfirmFeatureFile({ open: false, title: "", message: "", action: () => { } })
            setWarningState(false)
        }})


    function showConfirmDialog() {

        return (
            <Dialog open={confirmData.open} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle>
                    <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                        {confirmData.title}
                    </Typography>
                </DialogTitle>

                <DialogContent>
                    <Form className="form-control">
                        <Form.Label id="confirm">{confirmData.message} <b style={{ color: "#ba4735" }}>{scale.ipAddress}</b>?</Form.Label>

                        <div className="formButtons">
                            <button className="formButton1" onClick={(e) => {
                                confirmData.action()
                                handleCloseDialog(e)
                            }} type="button">
                                <span> Yes </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button">
                                <span> Cancel </span>
                            </button>
                        </div>
                    </Form>
                </DialogContent>
            </Dialog>
        )
    }

    function ShowFeatureFileDialog() {

        const handleCloseFeatureDialog = (event: any) => {
            event.stopPropagation()
            setConfirmFeatureFile({ open: false, title: "", message: "", action: () => { } })
            setFeatureFileButtonClicks(0)
            setWarningState(false)
        }

        return (
            <Dialog open={confirmFeatureFile.open} onClose={handleCloseFeatureDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle>
                    <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                        {confirmFeatureFile.title}
                    </Typography>
                </DialogTitle>

                <DialogContent>
                    <Form className="form-control">
                        <FormInput
                            label={confirmFeatureFile.message}
                            id="file"
                            name="file"
                            type="file"
                            accept=".package.tgz, .deb"
                            enctype="multipart/form-data"
                            method="post"
                            action="#"
                            register={register}
                            validation={{ required: "Please select a file to upload" }}
                            error={errors.file}
                        />
                        <div className="formButtons">
                            <button className="formButton1" onClick={searchForMatchingFile} type="button">
                                <span> {warningState === true ? "Yes, apply this file" : "Send File"} </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseFeatureDialog} type="button">
                                <span> Cancel </span>
                            </button>
                        </div>

                    </Form>
                </DialogContent>
            </Dialog>
        )
    }


    //Callback related
    function onSuccess(response : ResponseType){
        logger.info("Successfully performed action")
    }

    return (

        <div className="assetBar">
            <ButtonGroup>
                {/* Role 4 (Ping)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[PING]}
                    requiredDomain={store.getState().access.domains[scale.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                        event.stopPropagation();
                        const newID = Date.now()
                        function onFail(response: ResponseType) {
                            logger.error("(GetStatus) Error getting scale status", response)
                        }
                        if (addCallback(callbacks, newID, "Get Status", onSuccess, onFail, { requestMsg: 'Contacting ' + scale.ipAddress + '...', successMsg: scale.ipAddress + " successfully contacted!", failMsg: "default" })) {
                            dispatch(ScaleCtrlAPI.getScaleStatus(newID, scale.ipAddress, scale.storeId))
                        }
                    }}>
                        <ContactlessOutlined />
                        Ping
                    </Button>
                </AccessControl>

                {/* Role 3 (ManualHeartbeat)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[HEARTBEAT]}
                    requiredDomain={store.getState().access.domains[scale.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                        event.stopPropagation();
                        const newID = Date.now()
                        function onFail(response: ResponseType) {
                            logger.error("(SetProfile) Error executing manual heartbeat")
                        }
                        if (addCallback(callbacks, newID, "Manual Heartbeat", onSuccess, onFail, { requestMsg: 'Contacting ' + scale.ipAddress + '...', successMsg: "default", failMsg: "default" })) {
                            dispatch(ScaleCtrlAPI.executeManualHeartBeat(newID, scale.ipAddress, scale.storeId))
                        }
                    }}>
                        <FontAwesomeIcon icon={faHeartbeat} className="me-2 fa-lg" />
                        Manual Heartbeat
                    </Button>
                </AccessControl>

                {/* Role X (Apply Feature File)  */}
                {!isAssetList && <AccessControl 
                    user={user}
                    requiredRole={roles[DELETE_SCALE_DATA]}
                    requiredDomain={store.getState().access.domains[scale.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                        event.stopPropagation();
                        function onConfirm() {
                        }

                        setConfirmFeatureFile({
                            open: true,
                            title: 'Select Feature File',
                            message: "Please select a feature file to push to scale " + scale.ipAddress,
                            action: onConfirm
                        })

                    }}>
                        <FontAwesomeIcon icon={faFileUpload} className="me-2 fa-lg" />
                        Apply Feature File
                    </Button>
                </AccessControl>}

                {/* Role 2 (Reboot)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[REBOOT]}
                    requiredDomain={store.getState().access.domains[scale.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                        event.stopPropagation();
                        function onConfirm() {
                            const newID = Date.now()
                            function onFail(response) {
                                logger.error("(SetProfile) Error sending reboot command", response)
                            }
                            if (addCallback(callbacks, newID, "Reboot", onSuccess, onFail, { requestMsg: 'Contacting ' + scale.ipAddress + '...', successMsg: "default", failMsg: "default" })) {
                                dispatch(ScaleCtrlAPI.rebootDevice(newID, scale.ipAddress, scale.storeId))
                            }
                        }

                        setConfirmData({
                            open: true,
                            title: 'Confirm Restart Scale',
                            message: "WARNING: You will not be able to contact the scale during this process. Are you sure you want to restart scale: ",
                            action: onConfirm
                        })

                    }}>
                        <Refresh />
                        Reboot Scale
                    </Button>
                </AccessControl>

                {/* Role 1005 (DeleteScaleData)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[DELETE_SCALE_DATA]}
                    requiredDomain={store.getState().access.domains[scale.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" onClick={(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                        event.stopPropagation();
                        function onConfirm() {
                            const newID = Date.now()
                            function onFail(response: ResponseType) {
                                logger.error("(DeleteData) Error sending delete data command", response)
                            }
                            if (addCallback(callbacks, newID, "Delete Data", onSuccess, onFail, { requestMsg: 'Contacting ' + scale.ipAddress + '...', successMsg: "default", failMsg: "default" })) {
                                dispatch(ScaleCtrlAPI.deleteDeviceData(newID, scale.ipAddress, scale.storeId))
                            }
                        }

                        setConfirmData({
                            open: true,
                            title: 'Confirm Delete Scale Data',
                            message: "WARNING: You will not be able to recover any configurations after this step. Are you sure you want to delete all of the data in scale: ",
                            action: onConfirm
                        })

                    }}>
                        <DeleteIcon />
                        Delete Data
                    </Button>
                </AccessControl>
            </ButtonGroup>
            {confirmData.open ? showConfirmDialog() : null}
            {confirmFeatureFile.open ? ShowFeatureFileDialog() : null}
        </div>
    )
}