import {useState, useEffect} from 'react';
import {useDispatch} from "react-redux"
import AccessControl from "../common/AccessPermissions"
import { useRspHandler } from '../utils/ResponseProvider';
import logger from '../utils/logger';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeartbeat } from "@fortawesome/free-solid-svg-icons";
import {Refresh, ContactlessOutlined} from '@material-ui/icons';
import { DOWNLOAD_TRANSACTIONS, HEARTBEAT, PING, REBOOT } from "../../state/actions/accessActions"
import { Dialog, DialogContent, DialogTitle, Typography } from '@material-ui/core';
import { ScaleAPI, StoreAPI} from '../api';
import { Button, ButtonGroup, Form } from '@themesberg/react-bootstrap';
import DownloadIcon from '@mui/icons-material/Download';

import DownloadTransactionForm from '../transactions/TransactionsForm';
import { Store } from '../../types/asset/AssetTypes';
import { ScaleDevice } from '../../types/asset/ScaleTypes';
import { HTeRole, HTeUser } from '../../types/access/AccessTypes';
import { store } from '../../state';
import { ResponseType } from '../../types/storeTypes';


interface StoreActionsComponentProps{
    storeInfo : Store
    storeScales : ScaleDevice[]
    user : HTeUser
    roles : {[key: string] : HTeRole}
    download : (e : any) => void
}
export default function StoreActionsComponent(props : StoreActionsComponentProps){
    const {storeInfo, storeScales, user, roles, download} = props;
    const {callbacks, addCallback} = useRspHandler();
    const dispatch = useDispatch();
    const [confirmData, setConfirmData] = useState({open: false, title: "", message:"", action: () => {}});
    const [downloadDialog, setDownloadDialog] = useState(false)
    const [storeClientUrl, setStoreClientUrl] = useState<string | null>(null)
    const [transactionUrl, setTransactionUrl] = useState<string | null>(null)
    
    function parseServices(){
        if(!storeInfo){return}
        storeInfo.services.forEach((service) => {
            if(service.serviceType === "STORE_CLIENT"){setStoreClientUrl(service.url)}
            if(service.serviceType === "UI_MANAGER"){setTransactionUrl(service.url)}
        })
    }

    useEffect(()=>{
        parseServices()
    },[storeInfo])

    
    function showConfirmDialog(){
        
        const handleCloseDialog = (event : any) => {
            event.stopPropagation()
            setConfirmData({open: false, title: "", message:"", action: () => {}});
        }
        return(
            <Dialog open={confirmData.open} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
                <DialogTitle>
                    <Typography variant="h6" component="div" style={{ flexGrow: 2 }}>
                        {confirmData.title}
                    </Typography>
                </DialogTitle>
    
                <DialogContent>
                    <Form className="form-control">
                        <Form.Label id="confirm">{confirmData.message} <b style={{color : "#ba4735"}}>{storeInfo.storeName}</b>?</Form.Label>
    
                        <div className="formButtons">
                            <button className="formButton1" onClick={(e) =>{
                                confirmData.action()
                                handleCloseDialog(e)
                            }} type="button">
                                <span> Yes </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button">
                                <span> Cancel </span>
                            </button>
                        </div>
                    </Form>
                </DialogContent>
            </Dialog>
        )
    }


    //Callback related
    function onSuccess(response : ResponseType){
        logger.info("Successfully performed action")
    }

    return(

        <div className="assetBar">
            <ButtonGroup>
                {/* Role 4 (Ping)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[PING]}
                    requiredDomain={store.getState().access.domains[storeInfo.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" disabled={true} onClick={() =>{

                        const newID = Date.now()
                        function onFail(response : ResponseType){
                            logger.error("(GetStatus) Error getting store status", response)
                        }
                        
                        console.log("NOT IMPLEMENTED!")
                        
                    }}>
                        <ContactlessOutlined />
                        Ping Server
                    </Button>
                </AccessControl>


                {/* Role 3 (ManualHeartbeat)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[HEARTBEAT]}
                    requiredDomain={store.getState().access.domains[storeInfo.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" disabled={!storeClientUrl}onClick={() =>{
                        
                        const newID = Date.now()
                        function onFail(response : ResponseType){
                            logger.error("(SetProfile) Error executing manual heartbeat", response)
                        }


                        if(storeClientUrl && addCallback(callbacks, newID, "Manual Store Heartbeat", onSuccess, onFail, {requestMsg: 'Contacting ' + storeInfo.storeName + '...', successMsg:"default", failMsg:"default"})){
                            dispatch(StoreAPI.executeManualStoreHeartBeat(newID, storeClientUrl, false, storeInfo.storeId))
                        }
                        }}>
                        <FontAwesomeIcon icon={faHeartbeat} className="me-2 fa-lg" />
                        Manual Heartbeat
                    </Button>
                </AccessControl>

                {/* Role 2 (Reboot)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[REBOOT]}
                    requiredDomain={store.getState().access.domains[storeInfo.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" disabled={true} onClick={() =>{
                        function onConfirm(){
                            const newID = Date.now()
                            function onFail(response){
                                logger.error("(SetProfile) Error sending reboot command", response)
                            }
                            // if(addCallback(callbacks, newID, "Reboot", onSuccess, onFail, {requestMsg: 'Contacting ' + treeElement.ipAddress + '...', successMsg:"default", failMsg:"default"})){
                            //     dispatch(ScaleCtrlAPI.rebootDevice(newID, treeElement.ipAddress))
                            // } 
                            console.log("TODO NOT Implemented!")
                        }

                        setConfirmData({
                            open:true,
                            title:'Confirm Restart Scale',
                            message:"WARNING: You will not be able to contact the scale during this process. Are you sure you want to restart scale: ", 
                            action: onConfirm
                        })                    

                        }}>
                        <Refresh />
                        Reboot Server
                    </Button>
                </AccessControl>

                {/* Role 1008 (Download Store Transactions)  */}
                <AccessControl
                    user={user}
                    requiredRole={roles[DOWNLOAD_TRANSACTIONS]}
                    requiredDomain={store.getState().access.domains[storeInfo.storeId]}
                >
                    <Button id="actionBar2" variant="outline-primary" size="sm" disabled={!transactionUrl} onClick={() =>{
                        

                        setDownloadDialog(true)                   
                        
                        }}>
                        <DownloadIcon />
                        Download Transactions
                    </Button>
                </AccessControl>
            </ButtonGroup>
    
        <DownloadTransactionForm
            transactionUrl = {transactionUrl ?? ""}
            storeScales = {storeScales}
            openPopup = {downloadDialog}
            setOpenPopup = {setDownloadDialog}
            download = {download}
        />
        {confirmData.open ? showConfirmDialog() : null}
        </div>
    )
}