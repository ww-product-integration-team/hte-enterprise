import { Dialog, DialogTitle, DialogContent, Typography} from '@material-ui/core';
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Form} from '@themesberg/react-bootstrap';
import logger from "../utils/logger";
import { FormInput } from '../forms/FormHelper';
import { Alert } from '@mui/material';
import { Block, Upload } from '@mui/icons-material';
import { validateIPv4 } from '../utils/utils';
import { AlertMessage, HideMessage, ShowErrorMessage, ShowWarningMessage } from '../../types/utils/utilTypes';


/*Form for the user to select a csv file to import a list of scales to scan.
    @param openPopup: boolean - true if the popup is open
    @param setOpenPopup: React.Dispatch<React.SetStateAction<boolean>> - function to set the openPopup state
    @param handleRangeUpdate: (scaleList: string[]) => void - function to update the scale list
*/
interface ImportScaleListFormProps {
    openPopup: boolean,
    setOpenPopup: React.Dispatch<React.SetStateAction<"RANGE" | "IMPORT" | "EXPORT" | null>>,
    handleListUpdate: (scaleList: string[]) => void
}
export const ImportScaleListForm = ({openPopup, setOpenPopup, handleListUpdate}: ImportScaleListFormProps) => {
    const [alertMessage, setAlertMessage] = useState<AlertMessage>(HideMessage())
    interface FileForm{
        importFile : Blob | null
    }

    const { register, handleSubmit, formState: { errors } } = useForm<FileForm>();

    const handleCloseDialog = (event : any) => {
        event.stopPropagation()

        setOpenPopup(null);
    }

    const handleImport = (data : FileForm) => {
        setAlertMessage(HideMessage())
        logger.info("(ImportScaleListForm) Importing scale list from file: ", data.importFile)
        //Read the .csv file of line separated scale IP addresses and add them to the scale list
        if(data.importFile === null){
            setAlertMessage(ShowErrorMessage("Could not read file, please try again."));
            return;
        }
        
        const reader = new FileReader();
        reader.onload = (e) => {
            const lines = (e.target?.result as string).split("\n");
            //Parse each line for a valid IP address
            const pattern = /\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/;
            let scaleIPs : string[] = [];
            for(const line of lines){
                let columns = line.split(",");
                if(columns.length <= 1){
                    columns = line.split(";");
                }

                for(const column of columns){
                    const match = column.match(pattern);
                    if(match){
                        scaleIPs.push(match[0]);
                        if(!validateIPv4(match[0])){
                            setAlertMessage(ShowWarningMessage("Invalid IP address: " + match[0]));
                            return;
                        }
                    }
                }
            }

            handleListUpdate(scaleIPs);
            setOpenPopup(null);
        }
        reader.readAsText(data.importFile[0]);
    
    }

    
    return (
        <Dialog open={openPopup} onClose={handleCloseDialog} fullWidth={true} maxWidth="sm" aria-labelledby="form-dialog-title">
            <DialogTitle>
                <Typography variant="h5" component="div">
                    Import Scale List
                </Typography>
            </DialogTitle>

            <DialogContent>        
                <Form className="form-control" onSubmit={handleSubmit(handleImport)}>
                    <div className="uploadFile">

                        <FormInput
                            label="Upload a .csv file here: "
                            id="importFile"
                            name="importFile"
                            type="file"
                            accept=".csv" 
                            register={register}
                            validation={{required: "Please select a file to upload"}}
                            error={errors.importFile}
                        />

                        {/* Error importing file */}
                        {alertMessage.show ? <Alert className="m-1" variant="filled" severity={alertMessage.severity}>{alertMessage.message}</Alert> : null}

                        <div className="formButtons">
                            <button className="formButton1" onClick={handleSubmit(handleImport)} type="submit">
                                <Upload />
                                <span> Import </span>
                            </button>
                            <button className="formButton1" onClick={handleCloseDialog} type="button" value="Cancel">
                                <Block />
                                <span> Cancel </span>
                            </button>
                        </div>
                    </div>     
                </Form>
            </DialogContent>
        </Dialog>
    );
}