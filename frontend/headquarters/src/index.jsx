import React from 'react';
import { createRoot } from 'react-dom/client';
import './components/styles/index.css';
import App from './App';
import { Provider } from 'react-redux'
import { store, persistor} from "./state/index"
import { BrowserRouter as Router } from 'react-router-dom'
import { PersistGate } from 'redux-persist/integration/react'


// createRoot(
//     document.getElementById("root"),
//   )
//   .render(
//     <React.StrictMode>
//      <Provider store={store}>
//        <Router basename={'/frontend'}>
//          <App />
//        </Router>
//      </Provider>
//    </React.StrictMode>
// );

const root = createRoot(document.getElementById('root'))
root.render(
    <React.StrictMode>
     <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <Router basename={'/'}>
            <App />
            </Router>
        </PersistGate>
     </Provider>
   </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(logger))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

