// BrowserRouter = change pages
// Switch = ensures only 1 route from a div shows at a given time

import { useEffect } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom'
import { useLocation } from 'react-router-dom';

import './components/styles/App.css';
import PrivateRoute from './components/common/PrivateRoute';
import Dashboard from './components/dashboard/Dashboard';
import NavBar from './components/common/NavBar';
import Notifications from './components/account/Notifications'
import SideBar from './components/common/SideBar';
import ScaleProfiles from './components/scaleProfiles/ScaleProfiles';
import AssetList from './components/assetTreeView/AssetList'
import NotFound from './components/common/NotFound';
import Register from './components/account/Register';
import UploadHTeLicense from './components/account/UploadHTeLicense';
import Login from './components/account/Login';
import ChangePassword from './components/account/ChangePassword';
import CreateAccount from './components/account/users/CreateAccount';
import ForgotPassword from './components/account/ForgotPassword';
import ResetPassword from './components/account/ResetPassword';
import AccountProfile from './components/account/AccountProfile';
import AccountSettings from './components/account/AccountSettings';
import AssetDetails from './components/assetDetails/AssetDetails'
import NotificationsSystem, {atalhoTheme, dismissNotification, setUpNotifications, notify, dismissNotifications} from 'reapop'
import { useDispatch} from "react-redux";
import AboutHTe from "./components/common/AboutHTe";
import Directions from "./components/common/Directions"
import NotPermitted from "./components/common/NotPermitted";
import { AccessActions, store, useAppSelector} from './state';
import jwt_decode from "jwt-decode";
import axios from 'axios';
import { LoginAPI } from './components/api';
import {getAccessToken} from './components/api/responseTags'
import UserManagement from './components/account/users/UserManagement';
import LicenseManagement from './components/account/users/LicenseManagement';
import logger, { SendLogs } from './components/utils/logger';
import DeptManagement from './components/assetTreeView/DeptManagement';
import Transactions from "./components/transactions/Transactions";
import {ResponseProvider} from './components/utils/ResponseProvider';
import { ResponseHandler } from './components/utils/ResponseHandler';
import AppSettings from './components/appSettings/AppSettings';
import { FleetStatus } from './components/assetTreeView/FleetStatus';
import { UpgradeManager } from './components/upgradeManager/UpgradeManager';
import Utilities from './components/appSettings/Utilities';
import { ProdsTable } from './components/prodsTable/prodsTable';
import AppsPage from './components/appsPage/AppsPage';



function App() {
  const location = useLocation()
  const dispatch = useDispatch();
  // 1. Retrieve the notifications to display.
  const notifications = useAppSelector((state) => state.notifications)
  const userPermissions = useAppSelector((state) => state.access);
  const checkLoginStatus = useAppSelector((state) => state.access.shouldCheckToken);
  const isLoggedIn = useAppSelector((state) => state.access.loggedIn)
  const history = useHistory();

  SendLogs(dispatch)

  useEffect(()=>{
    axios.interceptors.response.use(function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function (error) {

      // console.log("error in app.jsx: ", error)
      const originalRequest = error.config
      try{
        // //If Axios call was cancelled by CancelToken
        // if(error.message != null){
        //   return Promise.reject(error);
        // }

        //If the reason for failure is that the token has expired
        if(error && error.response && error.response.data.errorDescription.startsWith('The Token has expired on')){
          let refreshingState = store.getState().access.isRefreshingToken
          //If the refresh token request has not already been sent, and the request isn't a refresh token request
          if(refreshingState === "false" && !originalRequest.url.endsWith("refreshToken")){
            dispatch(LoginAPI.requestRefresh(Date.now()))
          }

          //If the refresh request has timed out, fail it immediately and don't resend
          if(originalRequest.url.endsWith("refreshToken")){
            return Promise.reject(error);
          }


          //If the token has timed out, try to resend the request in 1 second i.e. hopefully after the new tokens have arrived
          return new Promise((resolve, reject) => {
            logger.info('Resending Request : ' + originalRequest.url);
            setTimeout(resolve, 1000)
          })
          .then(() => {

              //If the refresh token has also timed out then reset the isSendingRefresh to false after 10 seconds and return an error
              //The isRefreshingToken variable should be set to false on every successful login
              if(store.getState().access.isRefreshingToken === "failed"){ 
                return Promise.reject(error); 
              }
              else{
                let newRequest = originalRequest
                newRequest.headers = {...newRequest.headers, 'Authorization' : getAccessToken()}
                return(axios(newRequest))
              }
              
          })
        }
        else{ //All other error responses not handled
          return Promise.reject(error);
        }


      } catch (_error) {
        logger.error('Something went wrong handling timeout response:', _error)
        return Promise.reject(_error);
      }
    });
  }, [])

    const handleLicenseCheck = () => {
        
        try {
            if(userPermissions.account.id !== "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"){
                if (userPermissions.licenses.length === 0) {
                    logger.info("(accountProfile) No license detected, logging out");
                    dispatch(AccessActions.setAccessTokens({}, "LOGOUT"))
                    dispatch(AccessActions.checkTokenStatus())

                    new Promise(function(resolve, reject) {setTimeout(() => resolve("done"), 500);})
                    .then(function(result){ dispatch(notify('No license detected, logging out', 'info', {dismissible: true, dismissAfter: 10000, position: 'bottom-right'}))})
                    return
                }

                let license = userPermissions.licenses[0]
                let expiration = new Date(license.activationDate).getTime() + 
                            (license.timeout * 8.64e+7)

                if (Date.now() > expiration) {
                    // TODO: eventually show a notification that the license is expired and that's why user got logged out.
                    logger.info("(accountProfile) License is expired, logging out");
                    dispatch(AccessActions.setAccessTokens({}, "LOGOUT"))
                    dispatch(AccessActions.checkTokenStatus())
                    new Promise(function(resolve, reject) {setTimeout(() => resolve("done"), 500);})
                    .then(function(result){dispatch(notify('License is expired, logging out', 'info', {dismissible: true, dismissAfter: 10000, position: 'bottom-right'}))})
                    return
                }
            }
        } catch (error) {
            // The catch block gets ran on page load, bootleg check to see if user is logged in
            if (userPermissions.loggedIn === false) {
                history.push("/Login")
            }
        }
    }

    function parseLoginStatus(){
        let JsonStr = localStorage.getItem("tokens")
        if(!JsonStr){
            return false
        }

        let tokens = JSON.parse(JsonStr)
        if(!tokens){
            return false
        }
        if(!tokens.access_token){
            return false
        }
        let accessToken : any = jwt_decode(tokens.access_token)
        let refreshToken : any = jwt_decode(tokens.refresh_token)
        if(!accessToken || !refreshToken){       
            return false
        }

        if(refreshToken.exp > Date.now()/1000){
            return true
        }
        else{
            new Promise(function(resolve, reject) {setTimeout(() => resolve("done"), 500);})
            .then(function(result){dispatch(notify('You have been logged out due to inactivity', 'info', {dismissible: true, dismissAfter: 10000, position: 'bottom-right'}))})
            localStorage.removeItem("tokens")
            dispatch(AccessActions.setAccessTokens({}, "LOGOUT"))
            return false
        }
    }

  useEffect(()=>{
      //Update the Logged in Boolean based on the Tokens EXP rate, 
      //TODO we will need to find a better way to update this, tokens don't change often
      dispatch(AccessActions.setLoggedIn(parseLoginStatus()))
     
  },[checkLoginStatus])

  useEffect(()=>{
    setUpNotifications({
      defaultProps: {
          position: 'bottom-right',
          dismissible: true,
          dismissAfter: 10000
      } 
  })  }, [])


  useEffect(()=>{
    dispatch(dismissNotifications())
    window.scrollTo(0, 0);
    
}, [location])

  return (
    <>
      <div>
        <NotificationsSystem
            // 2. Pass the notifications you want Reapop to display.
            notifications={notifications}
            // 3. Pass the function used to dismiss a notification.
            dismissNotification={(id) => dispatch(dismissNotification(id))}
            // 4. Pass a builtIn theme or a custom theme.
            theme={atalhoTheme}
        />
      </div>

      <div className ="app">
        <NavBar />

        <div className ="bars">
          {isLoggedIn ? <SideBar /> : null}
        </div>

        <ResponseProvider>
            <div className={isLoggedIn ? "content" : "content loggedOut"}>
                {isLoggedIn
                  ? // (Logged in)
                  <Switch>
                    <PrivateRoute authenticated={isLoggedIn} path='/Products' component={ProdsTable} />
                    <PrivateRoute authenticated={isLoggedIn} exact path='/' component={Dashboard} />
                    <PrivateRoute authenticated={isLoggedIn} path='/AccountProfile' component={AccountProfile} />
                    <PrivateRoute authenticated={isLoggedIn} path='/AccountSettings' component={AccountSettings} />
                    <PrivateRoute authenticated={isLoggedIn} path='/Notifications' component={Notifications} />
                    <PrivateRoute authenticated={isLoggedIn} path='/AssetList' component={AssetList} />
                    <PrivateRoute authenticated={isLoggedIn} path='/FleetStatus' component={FleetStatus} />
                    <PrivateRoute authenticated={isLoggedIn} path='/UpgradeManager' component={UpgradeManager} />
                    <PrivateRoute authenticated={isLoggedIn} path='/ChangePassword' component={ChangePassword} />
                    <PrivateRoute authenticated={isLoggedIn} path='/CreateAccount' component={CreateAccount} />
                    <PrivateRoute authenticated={isLoggedIn} path='/AssetDetails' component={AssetDetails} />
                    <PrivateRoute authenticated={isLoggedIn} path='/AboutHTe' component={AboutHTe} />
                    <PrivateRoute authenticated={isLoggedIn} path='/Directions' component={Directions} />
                    <PrivateRoute authenticated={isLoggedIn} path='/NotPermitted' component={NotPermitted} />
                    <PrivateRoute authenticated={isLoggedIn} path='/UserManagement' component={UserManagement} />
                    <PrivateRoute authenticated={isLoggedIn} path="/LicenseManagement" component={LicenseManagement} />
                    <PrivateRoute authenticated={isLoggedIn} path='/DeptManagement' component={DeptManagement} />
                    <PrivateRoute authenticated={isLoggedIn} path='/Transactions' component={Transactions} />
                    <PrivateRoute authenticated={isLoggedIn} path='/Apps' component={AppsPage} />
                    <PrivateRoute authenticated={isLoggedIn} path='/Settings' component={AppSettings} />
                    <PrivateRoute authenticated={isLoggedIn} path='/Utilities' component={Utilities} />

                    {/* Redirect user to dashboard if they are logged in already */}
                    <Route exact path = "/RequestLicense">      <Register /></Route>     
                    <Route exact path = "/UploadHTeLicense">    <UploadHTeLicense /></Route>     
                    <Route exact path = "/Login">               <Login /></Route> 
                    {/* <PrivateRoute authenticated={isLoggedIn} path='/RequestLicense' component={isLoggedIn ? Dashboard : Register} /> */}
                    {/* <PrivateRoute authenticated={isLoggedIn} path='/Login' component={isLoggedIn ? Dashboard : Login} /> */}
                    <PrivateRoute authenticated={isLoggedIn} path='/ForgotPassword' component={isLoggedIn ? Dashboard : ForgotPassword} />
                    <PrivateRoute authenticated={isLoggedIn} path='/ResetPassword' component={isLoggedIn ? Dashboard : ResetPassword} />

                    {/* This is the current implementation as opposed to the commented section below */}
                    <PrivateRoute authenticated={isLoggedIn} exact path='/ScaleProfiles' component={ScaleProfiles} />
                    <PrivateRoute authenticated={isLoggedIn} path='*' component={NotFound} />
                  </Switch>

                  : // (Not logged in)      
                    <Switch>
                      <Route exact path = "/RequestLicense">    <Register /></Route> 
                      <Route exact path = "/UploadHTeLicense">  <UploadHTeLicense /></Route>         
                      <Route exact path = "/Login">             <Login /></Route> 
                      <Route exact path = "/ForgotPassword">    <ForgotPassword /></Route>
                      <Route exact path = "/ResetPassword">     <ResetPassword /></Route>
                      <PrivateRoute authenticated={isLoggedIn} path='*' component={NotFound} />
                    </Switch>
                }
              
              <ResponseHandler/>
            </div>
        </ResponseProvider>
      </div>
    </>
  )
}

export default App;
