import { ResponseType, StoreAction } from "../../types/storeTypes";
import * as ACTIONS from "../actions/endpointActions"

export interface ResponseState {
    endpointResponses : {[key: number] : ResponseType}
    activeRequests : number
    maxActiveRequests : number
}


const initialState : ResponseState= {
    endpointResponses: {},
    activeRequests: 0,
    maxActiveRequests: 5
};

const endpointReducer = (
    state = initialState,
    action : StoreAction
) : ResponseState => {
    //logger("endpointReducer " + action.type)
    switch(action.type){
        case ACTIONS.SET_RESPONSE:
            let newResp = {...state.endpointResponses}
            newResp[action.payload.id] = action.payload
            return {
                ...state,
                endpointResponses: newResp
            }
        case ACTIONS.HANDLE_RESPONSE:
            let existingRsp = {...state.endpointResponses}
            delete existingRsp[action.payload]
            return{
                ...state,
                endpointResponses: existingRsp
            }
        case ACTIONS.SET_ACTIVE_REQ:
            return{
                ...state,
                activeRequests: action.payload
            }
        default:
            return state
        
    }

}


export default endpointReducer;