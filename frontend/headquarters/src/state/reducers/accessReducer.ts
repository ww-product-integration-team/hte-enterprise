import { HTeUser, HTeDomain, HTeRole, JWT_Tokens} from "../../types/access/AccessTypes";
import { License } from "../../types/access/HTeLicense";
import { StoreAction } from "../../types/storeTypes";
import * as ACTIONS from "../actions/accessActions"


export interface AccessState {
    account : HTeUser
    licenses: License[],
    cloudLicenses: License[],
    isRefreshingToken: string, //State to check if the program is already fetching the next refresh token
    shouldCheckToken: number, //Modifier to tell App.jsx subscriber to update the logged in status
    loggedIn: boolean,
    tokens: JWT_Tokens | null, //JWT tokens for accessing the backend
    domains: {[key: string] : HTeDomain}, //Table of all domains
    roles: {[key: string] : HTeRole}, //Table of all available roles
    users: HTeUser[], //Table of all users saved on the database
};

const initialState : AccessState = {
    account : {
        id : "",
        firstName: "",
        lastName: "",
        username: "",
        password: "",       // This will be encrypted
        phoneNumber: "",
        accessLevel: 0,    // 1000, 2000, 3000, 4000?
        domain: {
            domainId: "",
            type: "",
            name: "",
            parentId: ""
        },
        roles: [],
        enabled: false,
        dateCreated: ""
    },
    licenses: [],
    cloudLicenses: [],
    isRefreshingToken: 'false', //State to check if the program is already fetching the next refresh token
    shouldCheckToken: 0, //Modifier to tell App.jsx subscriber to update the logged in status
    loggedIn: false,
    tokens: null, //JWT tokens for accessing the backend
    domains: {}, //Table of all domains
    roles: {}, //Table of all available roles
    users: [], //Table of all users saved on the database
};


const accessReducer = (
    state = initialState,
    action : StoreAction
) : AccessState => {
    switch(action.type){
        case ACTIONS.SET_ACCOUNT_INFO:
            return{
                ...state,
                account : action.payload
            }

        case ACTIONS.SET_USER_INFO:
            return{
                ...state,
                users: action.payload
            }
        case ACTIONS.SET_TOKENS:
            localStorage.setItem("tokens", JSON.stringify(action.payload))
            return{
                ...state,
                tokens: action.payload
            }
        case ACTIONS.SET_HTE_LICENSE:
            return{
                ...state,
                licenses: action.payload
            }
        case ACTIONS.SET_ROLES:
            return{
                ...state,
                roles: action.payload
            }
        case ACTIONS.SET_DOMAINS:
            return{
                ...state,
                domains: action.payload
            }
        case ACTIONS.SET_LOGGED_IN:
            return{
                ...state,
                loggedIn: action.payload
            }
        case ACTIONS.SET_SHOULD_CHECK_TOKENS:
            return{
                ...state,
                shouldCheckToken: action.payload
            }
        case ACTIONS.SET_IS_REFRESHING_TOKENS:
            return{
                ...state,
                isRefreshingToken: action.payload
            }
        case ACTIONS.SET_CLOUD_LICENSES:
            return {
                ...state,
                cloudLicenses: action.payload
            }
        default:
            return state
    }
}

export default accessReducer;