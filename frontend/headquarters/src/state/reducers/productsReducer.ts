import { HTeUser, HTeDomain, HTeRole, JWT_Tokens} from "../../types/access/AccessTypes";
import { License } from "../../types/access/HTeLicense";
import { StoreAction } from "../../types/storeTypes";
import * as PRODCUTS from "../actions/productsActions"
import { prod } from "../../components/prodsTable/prodsTable";


export interface AccessState {
    products: prod[]
};

const initialState : AccessState = {
    products : []
};


const productsReducer = (
    state = initialState,
    action : StoreAction
) : AccessState => {
    switch(action.type){
        case PRODCUTS.SET_PRODUCTS:
            return{
                ...state,
                products : action.payload
            }
        default:
            return state
    }
}

export default productsReducer;