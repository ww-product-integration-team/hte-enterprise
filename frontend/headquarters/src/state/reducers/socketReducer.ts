import { Dispatch, SetStateAction } from 'react'
import { Socket } from "../../types/socket/socketTypes"
import { StoreAction } from "../../types/storeTypes"
import * as ACTIONS from "../actions/socketActions"

export interface SocketState {
    socket: Socket
}

const initialState: SocketState = {
    socket: {
        message: null,
        status: "UNKNOWN",
        setStatus: (status) => {initialState.socket.status = status}
    }
}

const socketReducer = (state = initialState, action: StoreAction): SocketState => {
    let existingData = {...state.socket}
    switch (action.type) {
        case ACTIONS.SET_SOCKET_STATUS:
            existingData.setStatus(action.payload)

            return {
                ...state,
                socket: existingData
            }
        case ACTIONS.SET_MESSAGE:
            existingData.message = action.payload

            return {
                ...state,
                socket: existingData
            }
        default:
            return state
    }
}

export default socketReducer