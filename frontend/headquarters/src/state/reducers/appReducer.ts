
import { combineReducers } from '@reduxjs/toolkit'
import assetReducer from "./assetReducer"
import endpointReducer from "./endpointReducer"
import profileReducer from "./profileReducer"
import accessReducer from "./accessReducer";
import configReducer from "./configReducer";
import eventReducer from './eventReducer';
import {reducer as notificationsReducer} from 'reapop'
import { AccessActions } from "../index";
import storage from "redux-persist/lib/storage";
import { StoreAction } from "../../types/storeTypes";
import socketReducer from './socketReducer';
import productsReducer from './productsReducer';

//Peristor (Saves the react state on a refresh)
//==================================================================


const reducers = combineReducers({
    asset: assetReducer,
    products: productsReducer,
    profile: profileReducer,
    endpoint: endpointReducer,
    access: accessReducer,
    config: configReducer,
    event: eventReducer,
    socket: socketReducer,
    notifications: notificationsReducer()
})

export type AppState = ReturnType<typeof reducers> | undefined;

const rootReducer = (state : AppState, action : StoreAction) =>{
    if(action && AccessActions && action.type === AccessActions.SET_TOKENS && action.subAction === "LOGOUT"){
        storage.removeItem('persist:root')
        return reducers(undefined, action)
    }
    else{
        return reducers(state, action)
    }
    
}

export default rootReducer
