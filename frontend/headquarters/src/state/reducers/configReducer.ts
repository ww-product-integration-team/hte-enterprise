import * as ACTIONS from "../actions/configActions"
import { DeleteEventConfig, OfflineAway, StoreConfiguration } from "../../types/config/config";
import { Service } from "../../types/config/services";
import { StoreAction } from "../../types/storeTypes";

export interface ConfigState {
    deleteEvent : DeleteEventConfig | null,
    offlineAwayHours: OfflineAway,
    services : Service[],
    storeConfig : StoreConfiguration,
    inTestingMode: Boolean,
    hybridMultipath: {hybridMultipath: string}
    zonePricing: {zonePricing: string}
}

const initialState : ConfigState = {
    deleteEvent : null,
    offlineAwayHours: {
        scaleAwayHours: null,
        scaleOfflineHours: null
    },
    services : [],
    storeConfig : {},
    inTestingMode: false,
    hybridMultipath: {hybridMultipath: ""},
    zonePricing: {zonePricing: ""}
};

const configReducer = (
    state = initialState,
    action: StoreAction
) : ConfigState => {
    switch (action.type) {
        case ACTIONS.SET_DELETE_EVENT:
            return{
                ...state,
                deleteEvent: action.payload
            }
        case ACTIONS.SET_OFFLINE_CONFIG:
            return{
                ...state,
                offlineAwayHours: action.payload
            }
        case ACTIONS.SET_SERVICE_LIST:
            return{
                ...state,
                services: action.payload
            }
        case ACTIONS.SET_STORE_CONFIG:
            return{
                ...state,
                storeConfig: action.payload
            }
        case ACTIONS.SET_IN_TEST:
            return{
                ...state,
                inTestingMode: action.payload
            }
        case ACTIONS.SET_HYBRID_MULTIPATH:
            return{
                ...state,
                hybridMultipath: action.payload
            }
        case ACTIONS.SET_ZONE_PRICING:
            return{
                ...state,
                zonePricing: action.payload
            }
        default:
            return state
    }
}

export default configReducer;