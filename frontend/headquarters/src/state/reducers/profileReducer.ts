import logger from "../../components/utils/logger";
import { FileForEvent } from "../../types/asset/FileTypes";
import { ChecksumLog, Profile } from "../../types/asset/ProfileTypes";
import { StoreAction } from "../../types/storeTypes";
import * as ACTIONS from "../actions/profileActions"

export interface ProfileState{
    profiles : Profile[]
    files : FileForEvent[]
    featureFiles : FileForEvent[]
    debianFiles : FileForEvent[]
    tarballFiles : FileForEvent[]
    installImageFiles : FileForEvent[]
    licenseFiles : FileForEvent[]
    mediaFiles : FileForEvent[]
    imageData : {[key : string] : any}
    checksumLogs : ChecksumLog[]
}

const initialState : ProfileState = {
    profiles: [],
    files: [],
    featureFiles: [],
    debianFiles: [],
    tarballFiles: [],
    installImageFiles: [],
    licenseFiles:[],
    mediaFiles: [],
    imageData: {},
    checksumLogs: []
};

const profileReducer = (
    state = initialState,
    action : StoreAction
) : ProfileState => {
    // logger("Action Type (scaleProfiles): " + action.type)
    switch (action.type) {
        case ACTIONS.SET_SCALE_PROFILE:
            return{
                ...state,
                // Sorts by name alphabetically
                profiles: action.payload.sort((a : Profile,b : Profile) => (b.name.toLowerCase() < a.name.toLowerCase()) ? 1 : ((a.name.toLowerCase() < b.name.toLowerCase()) ? -1 : 0))    
            }
        case ACTIONS.SET_CONFIG_FILES:
            return{
                ...state,
                files: action.payload
            }
        case ACTIONS.SET_FEATURE_FILES:
            return{
                ...state,
                featureFiles: action.payload
            }
        case ACTIONS.SET_DEBIAN_FILES:
            return{
                ...state,
                debianFiles: action.payload
            }
        case ACTIONS.SET_TARBALL_FILES:
            return{
                ...state,
                tarballFiles: action.payload
            }
        case ACTIONS.SET_INSTALL_IMAGE_FILES:
            return{
                ...state,
                installImageFiles: action.payload
            }
        case ACTIONS.SET_LICENSE_FILES:
            return{
                ...state,
                licenseFiles: action.payload
            }
        case ACTIONS.SET_MEDIA_FILES:
            return{
                ...state,
                mediaFiles: action.payload
            }
        case ACTIONS.SET_IMAGE_DATA:
            if(!action?.id){
                logger.error("No ID provided for image data")
                return state
            }

            let existingData = {...state.imageData}
            existingData[action?.id] = action.payload

            return{
                ...state,
                imageData: existingData
            }
        case ACTIONS.SET_CHECKSUM_LOGS:
            return{
                ...state,
                checksumLogs: action.payload
            }

        default:
            // logger("Unknown Scale Profiles action : " + action.type)
            return state
    }
}

//Supplemental functions


export default profileReducer;