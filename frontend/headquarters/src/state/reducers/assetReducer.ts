import * as ACTIONS from "../actions/assetActions"
import logger from "../../components/utils/logger";
import { NodeStatus, lazyNode } from "../../types/asset/TreeTypes";
import { Banner, Department, PricingZone, Region, Store } from "../../types/asset/AssetTypes";
import { ScaleDevice, ScaleSettings, SyncData } from "../../types/asset/ScaleTypes";
import { TransactionData } from "../../types/transaction/transactionTypes";
import { StoreAction } from "../../types/storeTypes";
import { Batch, Group, UpgradeDevice, Timeframe } from "../../components/upgradeManager/UpgradeManager";
import { upgradeFile } from "../../components/upgradeManager/upgradeForms/uploadUpgradeFileForm";
import { AutoAssignRule } from "../../components/appSettings/ImportCSVForAssigner";
import { filter } from "../../components/assetTreeView/Sidebar";

export interface groupsObject{
    group: Group
}
export interface AssetState {
    lazyTree : {[id : string] : lazyNode}
    lazyFilterTree : {[id : string] : lazyNode}
    focused : string
    expanded : string[]
    lazyExpanded : string []
    lazyFilterExpanded: string [],
    filteredAssets: {[key: string]: ScaleDevice[]} ,
    selected : string[]
    banners : Banner[]
    regions : Region[]
    stores : {[key: string] : Store}
    depts : {[key: string] : Department}
    scales : {[key: string] : ScaleDevice}
    scaleCount : {assigned: number,
                unassigned: number}
    scaleDetails : {[key: string] : ScaleSettings}
    pricingZones : {[key: string] : PricingZone}
    syncData : {[key: string] : SyncData}
    nodeData : {[key: string] : NodeStatus}
    transactions : {[key: string] : TransactionData}
    filterArray: {[key:string]: filter}
    pageView : boolean
    appHook: object
    batches: {[key:string]: Batch}
    groups: {[key:string]: Group}
    upgradeDevices : {[key:string]: UpgradeDevice}
    upgradeSelected : string[]
    upgradeFiles :upgradeFile[]
    primaryScales : string[]
    timeframes : Timeframe
    autoAssignRules : AutoAssignRule[]
    criticalDialog: string
    nodeStatusUpdaterTimers: {[key:string]: Date}
}

const initialState : AssetState = {
    lazyTree : {},
    lazyFilterTree : {},
    focused: "",
    expanded: [],
    lazyExpanded: [],
    lazyFilterExpanded: [],
    filteredAssets: {},
    selected: [],
    banners: [],
    regions: [],
    stores: {},
    depts : {},
    scales : {},
    scaleCount : {assigned : 0,
                unassigned: 0},
    scaleDetails : {},
    pricingZones : {},
    syncData : {},
    nodeData : {},
    transactions : {},
    filterArray : {}, 
    pageView : true,
    appHook : {},
    batches : {},
    groups : {}, 
    upgradeDevices : {},
    upgradeSelected : [],
    upgradeFiles : [],
    primaryScales : [],
    timeframes : {
        uploadPrimaryStart: "",
        uploadPrimaryEnd: "",
        uploadSecondaryStart: "",
        uploadSecondaryEnd: "",
        upgradeTimeStart: "",
        upgradeTimeEnd: ""
    },
    autoAssignRules : [],
    criticalDialog: "",
    nodeStatusUpdaterTimers: {}
};

const assetReducer = (
    state = initialState,
    action : StoreAction
) : AssetState => {
    switch (action.type) {
        case ACTIONS.SET_LAZYTREE:
            return{
                ...state,
                lazyTree: action.payload
            }
        case ACTIONS.INS_LAZYTREE:
            if(!action.id){
                logger.error("INS_LAZYFILTERTREE: Action does not have an assigned ID!")
                return state
            }
            let tree = JSON.parse(JSON.stringify(state.lazyTree))
            let paths = action.id.split(",")
            let controlledPaths = [...paths] // edited to find node 
            // get banner node
            let node : lazyNode = tree[paths[0]]
            controlledPaths = controlledPaths.splice(1,controlledPaths.length)  // remove banner id (already used)
            // iterate to correct node
            if(controlledPaths && controlledPaths.length > 0){
                for (let path of controlledPaths) {
                    if (node["children"] && Object.keys(node.children).length > 0 ) {
                        node = node.children[path];
                    }
                }
            }
            // place response data at correct node point
            node.children =  action.payload[paths[paths.length-1]].children // place at node.child bc typescript uses pointers for subclass
            return{
                ...state,
                lazyTree: tree
            }
        case ACTIONS.SET_LAZY_FILTER_TREE:
            return{
                ...state,
                lazyFilterTree: action.payload
            }
        case ACTIONS.INS_NEW_LAZY_FILTER_TREE:
            if(!action.id){
                logger.error("INS_NEW_LAZY_FILTER_TREE: Action does not have an assigned ID!")
                return state
            }
            let existingLazyFilterTree = JSON.parse(JSON.stringify(state.lazyFilterTree))
            existingLazyFilterTree[action.id] = action.payload[action.id]
            return{
                ...state,
                lazyFilterTree: existingLazyFilterTree
            }
        case ACTIONS.INS_LAZY_FILTER_TREE:
            if(!action.id){
                logger.error("INS_LAZYFILTERTREE: Action does not have an assigned ID!")
                return state
            }
            let fTree = JSON.parse(JSON.stringify(state.lazyFilterTree))
            let fPaths = action.id.split(",")
            let fControlledPaths = [...fPaths] //edited and converted to find node 
            // get banner node
            let fNode : lazyNode = fTree[fPaths[0]]
            fControlledPaths = fControlledPaths.splice(1,fControlledPaths.length)  // remove banner id (already used)
            // iterate to correct node
            if(fControlledPaths && fControlledPaths.length > 0){
                for (let path of fControlledPaths) {
                    if (fNode["children"] && Object.keys(fNode.children).length > 0 ) {
                        fNode = fNode.children[path];
                    }
                }
            }
            // place response data at correct node point
            fNode.children =  action.payload[fPaths[fPaths.length-1]].children // place at node.child bc typescript uses pointers for subclass
            return{
                ...state,
                lazyFilterTree: fTree
            }
        case ACTIONS.SET_EXPANDED:
            return{
                ...state,
                expanded: action.payload
            }
        case ACTIONS.SET_LAZY_EXPANDED:
            return{
                ...state,
                lazyExpanded: action.payload
            }
        case ACTIONS.SET_LAZY_FILTER_EXPANDED:
            return{
                ...state,
                lazyFilterExpanded: action.payload
            }
        case ACTIONS.SET_FILTERED_ASSETS:
            return{
                ...state,
                filteredAssets: action.payload
            }
        case ACTIONS.SET_SELECTED:
            return{
                ...state,
                selected: action.payload
            }
        case ACTIONS.SET_FOCUSED:
            return{
                ...state,
                focused: action.payload
            }
        case ACTIONS.SET_SCALES:
            return{
                ...state,
                scales: action.payload
            }
        case ACTIONS.SET_SCALE_COUNT:
            return{
                ...state,
                scaleCount: action.payload
            }
        case ACTIONS.SET_SCALE_DETAILS:
            let currentDetails = Object.assign({}, state.scaleDetails)
            currentDetails[action.payload.deviceId] = action.payload
            return{
                ...state,
                scaleDetails: currentDetails
            }
        case ACTIONS.SET_BANNERS:
            return{
                ...state,
                banners: action.payload.sort((a : Banner,b : Banner) => (b.bannerName.toLowerCase() < a.bannerName.toLowerCase()) ? 1 : ((a.bannerName.toLowerCase() < b.bannerName.toLowerCase()) ? -1 : 0))
            }
        case ACTIONS.SET_REGIONS:
            return{
                ...state,
                regions: action.payload.sort((a : Region,b : Region) => (b.regionName.toLowerCase() < a.regionName.toLowerCase()) ? 1 : ((a.regionName.toLowerCase() < b.regionName.toLowerCase()) ? -1 : 0))
            }
        case ACTIONS.SET_STORES:
            return{
                ...state,
                stores: action.payload
            }
        case ACTIONS.INS_STORES:
            if(!action.id){
                logger.error("INS_STORES : Action does not have an assigned ID!")
                return state
            }
            let existingStores = Object.assign({}, state.stores)
            existingStores[action.id] = action.payload

            return{
                ...state,
                stores: existingStores
            }
        case ACTIONS.SET_DEPARTMENTS:
            return{
                ...state,
                depts: action.payload//action.payload.sort((a : Department,b : Department) => (b.deptName1.toLowerCase() < a.deptName1.toLowerCase()) ? 1 : ((a.deptName1.toLowerCase() < b.deptName1.toLowerCase()) ? -1 : 0))
            }
        case ACTIONS.INS_DEPARTMENTS:
            if(!action.id){
                logger.error("INS_DEPARTMENTS : Action does not have an assigned ID!")
                return state
            }
            let existingDepts = Object.assign({}, state.depts)
            existingDepts[action.id] = action.payload

            return{
                ...state,
                depts: existingDepts
            }
        case ACTIONS.SET_SYNC_STATUS:
            let currentSyncInfo = Object.assign({}, state.syncData)
            currentSyncInfo[action.payload.id] = action.payload
            return{
                ...state,
                syncData: currentSyncInfo
            }
        case ACTIONS.SET_NODE_STATUS:
            if(!action.id){
                logger.error("SET_NODE_STATUS : Action does not have an assigned ID!")
                return state
            }
            let existingNodeData = Object.assign({}, state.nodeData)
            existingNodeData[action.id] = action.payload

            return{
                ...state,
                nodeData: existingNodeData
            }
        case ACTIONS.SET_TRANSACTIONS:

            if(!action.id){
                logger.error("SET_TRANSACTIONS : Action does not have an assigned ID!")
                return state
            }
            let existingTransactions = Object.assign({}, state.transactions)
            existingTransactions[action.id] = action.payload
            // let existingTransactions = {...state.transactions} // If above doesn't work, use this for all
            // existingTransactions[action?.id] = action.payload

            return{
                ...state,
                transactions: existingTransactions
            }
        case ACTIONS.SET_FILTERARRAY:
            return{
                ...state,
                filterArray: action.payload
            }
        case ACTIONS.SET_PAGEVIEW:
            return{
                ...state,
                pageView: action.payload
            }
        case ACTIONS.SET_APPHOOKOBJECT:
            return {
                ...state,
                appHook: action.payload
            }
        case ACTIONS.SET_BATCHES:
            return {
                ...state,
                batches: action.payload
            }
        case ACTIONS.SET_GROUPS:
            return {
                ...state,
                groups: action.payload
            }
        case ACTIONS.SET_UPGRADEDEVICES:
            return {
                ...state,
                upgradeDevices: action.payload
            }
        case ACTIONS.SET_UPGRADESELECTED:
            return {
                ...state,
                upgradeSelected: action.payload
            }
        case ACTIONS.SET_UPGRADEFILES:
                return {
                    ...state,
                    upgradeFiles: action.payload
                }
        case ACTIONS.SET_PRIMARYSCALES:
            return {
                ...state,
                primaryScales: action.payload
            }
        case ACTIONS.SET_TIMEFRAME:
            return {
                ...state,
                timeframes: action.payload
            }
        case ACTIONS.SET_AUTOASSIGNRULES:
            return {
                ...state,
                autoAssignRules: action.payload
            }
        case ACTIONS.SET_CRITICAL_DIALOG:
            return {
                ...state,
                criticalDialog: action.payload
            }
        case ACTIONS.SET_PRICING_ZONES:
            return {
                ...state,
                pricingZones: action.payload
            }
        case ACTIONS.SET_NODE_STATUS_UPDATE_TIMERS:
            return {
                ...state,
                nodeStatusUpdaterTimers: action.payload 
            }
        default:
            return state
    }
}

export default assetReducer;