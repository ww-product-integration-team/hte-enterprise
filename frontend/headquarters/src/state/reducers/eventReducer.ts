import { StoreAction } from "../../types/storeTypes"
import { Event } from "../../types/event/eventTypes"
import * as ACTIONS from "../actions/eventActions"

export interface EventState {
    events: Event[]
}

const initialState: EventState = {
    events: []
}

const eventReducer = (state = initialState, action: StoreAction): EventState => {
    switch (action.type) {
        case ACTIONS.SET_EVENTS:
            return {
                ...state,
                events: action.payload
            }
        default:
            return state
    }
}

export default eventReducer;
