import { ActionType, StoreAction } from '../../types/storeTypes'

export const SET_SOCKET_STATUS = "SET_SOCKET_STATUS"
export const SET_MESSAGE = "SET_MESSAGE"

const ACTION_TYPE = ActionType.Socket

export function setSocketStatus(data: "UNKNOWN" | "CONNECTED" | "DISCONNECTED"): StoreAction {
    return {
        type: SET_SOCKET_STATUS,
        class: ACTION_TYPE,
        payload: data
    }
}

export function setMessage(data: any): StoreAction {
    return {
        type: SET_MESSAGE,
        class: ACTION_TYPE,
        payload: data
    }
}