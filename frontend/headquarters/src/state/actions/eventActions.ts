import { ActionType, StoreAction } from '../../types/storeTypes'
import { Event } from '../../types/event/eventTypes'

export const SET_EVENTS = "SET_EVENTS"

const ACTION_TYPE = ActionType.Event

export function setEvents(data: Event[]): StoreAction {
    return {
        type: SET_EVENTS,
        class: ACTION_TYPE,
        payload: data
    }
}