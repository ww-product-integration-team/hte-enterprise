import { ActionType, ResponseType, StoreAction } from "../../types/storeTypes";

export const SET_BASE_URL = "SET_BASE_URL"
export const SET_PORT = "SET_PORT";
export const SET_RESPONSE = "SET_RESPONSE"
export const HANDLE_RESPONSE = "HANDLE_RESPONSE"
export const SET_ACTIVE_REQ = "SET_ACTIVE_REQ"

const ACTION_TYPE = ActionType.Endpoint

export function setResponse(response : ResponseType):StoreAction{
    return {
        type: SET_RESPONSE,
        class: ACTION_TYPE,
        payload: response
    }
}

export function handleResponse(responseId : number):StoreAction{
    return {
        type: HANDLE_RESPONSE,
        class: ACTION_TYPE,
        payload: responseId
    }
}

export function setActiveRequests(activeRequests : number):StoreAction{
    return {
        type: SET_ACTIVE_REQ,
        class: ACTION_TYPE,
        payload: activeRequests
    }
}
