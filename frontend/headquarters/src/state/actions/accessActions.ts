import { HTeUser } from "../../types/access/AccessTypes";
import { License } from "../../types/access/HTeLicense";
import { ActionType, StoreAction } from "../../types/storeTypes";

export const SET_ACCOUNT_INFO = "SET_ACCOUNT_INFO" //Info about the currently logged in user
export const SET_USER_INFO = "SET_USER_INFO" //User info requested by the account user
export const SET_TOKENS = "SET_TOKENS";
export const SET_USERNAME = "SET_USERNAME";
export const SET_HTE_LICENSE = "SET_HTE_LICENSE";
export const SET_CLOUD_LICENSES = "SET_CLOUD_LICENSES";

export const SET_ROLES = "SET_ROLES";
export const SET_DOMAINS = "SET_DOMAINS";

export const SET_LOGGED_IN = "SET_LOGGED_IN"

export const SET_SHOULD_CHECK_TOKENS = "SET_SHOULD_CHECK_TOKENS"
export const SET_IS_REFRESHING_TOKENS = "SET_IS_REFRESHING_TOKENS"

//Default Domains
export const DEFAULT_ADMIN_DOMAIN_ID =      "87588758-8758-8758-8758-875887588758"
export const DEFAULT_PROFILE_DOMAIN_ID =    "00000000-0000-0000-0000-000000000000"
export const DEFAULT_BANNER_DOMAIN_ID =     "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
export const DEFAULT_DEPT_DOMAIN_ID =       "dddddddd-dddd-dddd-dddd-dddddddddddd"
export const DEFAULT_ALL_ACCESS_DOMAIN_ID = "22222222-2222-2222-2222-222222222222"
export const DEFAULT_UNASSIGNED_DOMAIN_ID = "11111111-1111-1111-1111-111111111111"


//Default Roles
export const MONITOR                = "Monitor";
export const REBOOT                 = "Reboot";
export const HEARTBEAT              = "ManualHeartbeat";
export const PING                   = "Ping";
export const USER_ENABLE_DISABLE    = "UserEnableDisable";
export const DOWNLOAD_TRANSACTIONS  = "DownloadTransactions";
export const ASSIGN_SCALES          = "AssignScales";
export const MANAGE_OPERATORS       = "ManageOperators";
export const MANAGE_STORES          = "ManageStores"
export const ASSIGN_SCALE_PROFILES  = "AssignScaleProfiles";
export const DELETE_SCALE_DATA      = "DeleteScaleData";
export const MANAGE_PROFILES        = "ManageProfiles";
export const ASSIGN_DEPT_PROFILES   = "AssignDeptProfiles";
export const FULL_TREEVIEW          = "FullTreeView";
export const MANAGE_SUPERVISORS     = "ManageSupervisors";
export const MANAGE_LICENSES        = "ManageLicenses";
export const SYSTEM_MANAGER         = "SystemManager";
export const IMPORT_TREE            = "ImportTree";
export const UPGRADE_SCALES         = "UpgradeScales";
export const MANUAL_OVERRIDE        = "ManualOverride";
export const OTP_PASS_MANAGER       = "OtpPasswordManager";

const ACTION_TYPE = ActionType.Access
//====================================================================================
// HTe User object containing all user info including assigned roles

// Used to get info of the user that is currently logged in
export function setAccountInfo(info : HTeUser) : StoreAction {
    return {
        type: SET_ACCOUNT_INFO,
        class: ACTION_TYPE,
        payload: info
    }
}

// Used to get info of users that are registered on the site
export function setUserInfo(info) : StoreAction {
    return {
        type: SET_USER_INFO,
        class: ACTION_TYPE,
        payload: info
    }
}

// ?
export function setAccessTokens(tokens : {access_token ?: string, refresh_token ?: string}, action : "LOGOUT" | "LOGIN" | "REFRESH" | "NONE" = "NONE") : StoreAction {
    return {
        type: SET_TOKENS,
        subAction: action,
        class: ACTION_TYPE,
        payload: tokens
    }
}

// ?
export function setUsername(username) : StoreAction {
    return {
        type: SET_USERNAME,
        class: ACTION_TYPE,
        payload: username
    }
}

// Information from HTe's license
export function setHTeLicense(licenses) : StoreAction {
    return {
        type: SET_HTE_LICENSE,
        class: ACTION_TYPE,
        payload: licenses
    }
}

// ?
export function setRoles(roles) : StoreAction {
    return {
        type: SET_ROLES,
        class: ACTION_TYPE,
        payload: roles
    }
}

export function setDomains(domains) : StoreAction {
    return {
        type: SET_DOMAINS,
        class: ACTION_TYPE,
        payload: domains
    }
}

export function setLoggedIn(isLoggedIn : boolean) : StoreAction {
    return {
        type: SET_LOGGED_IN,
        class: ACTION_TYPE,
        payload: isLoggedIn
    }
}

export function checkTokenStatus() : StoreAction {
    return{
        type: SET_SHOULD_CHECK_TOKENS,
        class: ACTION_TYPE,
        payload: Date.now()
    }
}

export function setIsRefreshingToken(isRefreshing : "true" | "false" | "failed") : StoreAction {
    return{
        type: SET_IS_REFRESHING_TOKENS,
        class: ACTION_TYPE,
        payload: isRefreshing
    }
}

export function setCloudLicenses(cloudLicenses: License[]) : StoreAction {
    return {
        type: SET_CLOUD_LICENSES,
        class: ACTION_TYPE,
        payload: cloudLicenses
    }
}