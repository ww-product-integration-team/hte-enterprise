import { FileForEvent } from "../../types/asset/FileTypes";
import { ChecksumLog, Profile } from "../../types/asset/ProfileTypes";
import { ActionType, StoreAction } from "../../types/storeTypes";

export const SET_SCALE_PROFILE = "SET_SCALE_PROFILE";
export const SET_CONFIG_FILES = "SET_CONFIG_FILES";
export const SET_MEDIA_FILES = "SET_MEDIA_FILES";
export const SET_FEATURE_FILES = "SET_FEATURE_FILES";
export const SET_DEBIAN_FILES = "SET_DEBIAN_FILES";
export const SET_TARBALL_FILES = "SET_TARBALL_FILES";
export const SET_INSTALL_IMAGE_FILES = "SET_INSTALL_IMAGE_FILES";
export const SET_LICENSE_FILES = "SET_LICENSE_FILES";
export const SET_IMAGE_DATA = "SET_IMAGE_DATA";
export const SET_CHECKSUM_LOGS = "SET_CHECKSUM_LOGS";

const ACTION_TYPE = ActionType.Profile

//====================================================================================
// Get Scale Profiles
export function setScaleProfile(data : Profile[]) : StoreAction{
    return {
        type: SET_SCALE_PROFILE,
        class: ACTION_TYPE,
        payload: data
    }
}

//====================================================================================
// Get Config Files
export function setConfigFiles(data : FileForEvent[]){
    return {
        type: SET_CONFIG_FILES,
        class: ACTION_TYPE,
        payload: data
    }
}

//====================================================================================
// Get Feature Files
export function setFeatureFiles(data : FileForEvent[]){
    return {
        type: SET_FEATURE_FILES,
        class: ACTION_TYPE,
        payload: data
    }
}

//====================================================================================
// Get Debian Files
export function setDebianFiles(data : FileForEvent[]){
    return {
        type: SET_DEBIAN_FILES,
        CLASS: ACTION_TYPE,
        payload: data
    }
}

//====================================================================================
// Get Debian Files
export function setTarballFiles(data : FileForEvent[]){
    return {
        type: SET_TARBALL_FILES,
        CLASS: ACTION_TYPE,
        payload: data
    }
}

//====================================================================================
// Get Debian Files
export function setInstallImageFiles(data : FileForEvent[]){
    return {
        type: SET_INSTALL_IMAGE_FILES,
        CLASS: ACTION_TYPE,
        payload: data
    }
}

//====================================================================================
// Get License Files
export function setLicenseFiles(data : FileForEvent[]){
    return {
        type: SET_LICENSE_FILES,
        CLASS: ACTION_TYPE,
        payload: data
    }
}


//====================================================================================
// Get Media Files
export function setMediaFiles(data : FileForEvent[]){
    return {
        type: SET_MEDIA_FILES,
        class: ACTION_TYPE,
        payload: data
    }
}

//====================================================================================
// Set Image Data
export function setImageData(fileId: string, data : string){
    return {
        type: SET_IMAGE_DATA,
        class: ACTION_TYPE,
        payload: data,
        id: fileId
    }
}

//====================================================================================
// Set Checksum Logs
export function setChecksumLogs(data: ChecksumLog[]){
    return {
        type: SET_CHECKSUM_LOGS,
        class: ACTION_TYPE,
        payload: data
    }
}