import { Banner, Department, PricingZone, Region } from "../../types/asset/AssetTypes";
import { ScaleDevice } from "../../types/asset/ScaleTypes";
import { ActionType, StoreAction} from "../../types/storeTypes";
import { Store } from "../../types/asset/AssetTypes";
import { upgradeFile } from "../../components/upgradeManager/upgradeForms/uploadUpgradeFileForm";
import { Batch, Timeframe } from "../../components/upgradeManager/UpgradeManager";
import { AutoAssignRule } from "../../components/appSettings/ImportCSVForAssigner";
import { lazyNode } from "../../types/asset/TreeTypes";
import { filter } from "../../components/assetTreeView/Sidebar";

export const SET_LAZYTREE = "SET_LAZYTREE"
export const INS_LAZYTREE = "INS_LAZYTREE"
export const INS_LAZY_FILTER_TREE = "INS_LAZY_FILTER_TREE";
export const SET_LAZY_FILTER_TREE = "SET_LAZY_FILTER_TREE";
export const INS_NEW_LAZY_FILTER_TREE = "INS_NEW_LAZY_FILTER_TREE";
export const SET_EXPANDED = "SET_EXPANDED";
export const SET_LAZY_EXPANDED = "SET_LAZY_EXPANDED";
export const SET_LAZY_FILTER_EXPANDED = "SET_LAZY_FILTER_EXPANDED";
export const SET_FILTERED_ASSETS = "SET_FILTERED_ASSETS"
export const SET_SELECTED = "SET_SELECTED";
export const SET_FOCUSED = "SET_FOCUSED"
export const SET_BANNERS = "SET_BANNERS";
export const SET_REGIONS = "SET_REGIONS";
export const SET_STORES = "SET_STORES";
export const INS_STORES = "INS_STORES";
export const SET_DEPARTMENTS = "SET_DEPARTMENTS";
export const INS_DEPARTMENTS = "INS_DEPARTMENTS";
export const SET_SCALES = "SET_SCALES";
export const SET_SCALE_COUNT = "SET_SCALE_COUNT";
export const SET_SCALE_DETAILS = "SET_SCALE_DETAILS"
export const SET_SYNC_STATUS = "SET_SYNC_STATUS";
export const SET_NODE_STATUS = "SET_NODE_STATUS";
export const SET_TRANSACTIONS = "SET_TRANSACTIONS";
export const SET_FILTERARRAY = "SET_FILTERARRAY";
export const SET_PAGEVIEW = "SET_PAGEVIEW"
export const SET_APPHOOKOBJECT = "SET_APPHOOKOBJECT"
export const SET_BATCHES = "SET_BATCHES"
export const SET_GROUPS = "SET_GROUPS"
export const SET_UPGRADEDEVICES = "SET_UPGRADEDEVICES"
export const SET_UPGRADESELECTED = "SET_UPGRADESELECTED"
export const SET_UPGRADEFILES = "SET_UPGRADEFILES"
export const SET_PRIMARYSCALES = "SET_PRIMARYSCALES"
export const SET_TIMEFRAME = "SET_TIMEFRAME"
export const SET_AUTOASSIGNRULES = "SET_AUTOASSIGNRULES"
export const SET_CRITICAL_DIALOG = "SET_CRITICAL_DIALOG"
export const SET_PRICING_ZONES = "SET_PRICING_ZONES"
export const SET_NODE_STATUS_UPDATE_TIMERS = "SET_NODE_STATUS_UPDATE_TIMERS"

const ACTION_TYPE = ActionType.Asset

//====================================================================================
//Tree Related
export function setLazyTree(lazyTreePart: {[id : string] : lazyNode}) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_LAZYTREE,
        payload: lazyTreePart
        
    }
}
export function insertLazyTree(lazyTree: {[id : string] : lazyNode}, stringPaths: string) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: INS_LAZYTREE,
        payload: lazyTree,
        id:stringPaths
    }
}
export function setLazyFilterTree(lazyFilterTreePart: {[id : string] : lazyNode}) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_LAZY_FILTER_TREE,
        payload: lazyFilterTreePart
        
    }
}
export function insertFilterLazyTree(lazyFilterTree: {[id : string] : lazyNode}, stringPaths: string) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: INS_LAZY_FILTER_TREE,
        payload: lazyFilterTree,
        id: stringPaths
    }
}
export function insertNewLazyFilterTree(lazyFilterTree: {[id : string] : lazyNode}) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: INS_NEW_LAZY_FILTER_TREE,
        payload: lazyFilterTree,
        id: Object.keys(lazyFilterTree)[0]
    }
}

export function setExpandedTreeItems(expandedItems) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_EXPANDED,
        payload: expandedItems
    }
}
export function setLazyExpandedTreeItems(lazyExpanded) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_LAZY_EXPANDED,
        payload: lazyExpanded
    }
}
export function setLazyFilterExpandedTreeItems(lazyFilterExpanded) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_LAZY_FILTER_EXPANDED,
        payload: lazyFilterExpanded
    }
}
export function setFilteredAssets(filteredAssets: {[key: string]: ScaleDevice[]}) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_FILTERED_ASSETS,
        payload: filteredAssets
    }
}


export function setSelectedItems(selectedItems : string[]) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_SELECTED,
        payload: selectedItems
    }
}


export function setFocusedAsset(treeElement) : StoreAction {
    return{
        class: ACTION_TYPE,
        type: SET_FOCUSED,
        payload: treeElement
    }
}

//====================================================================================
// List of Scales
export function setScaleList(scaleList : {[id: string] : ScaleDevice}) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_SCALES,
        payload: scaleList
    };
}

export function setScaleCount(scaleCounts: {assigned: number, unassigned: number}) : StoreAction{
    return {
        class: ACTION_TYPE,
        type: SET_SCALE_COUNT,
        payload: scaleCounts
    };
}

export function setScaleDetails(scaleObj) : StoreAction {
    return {
        class: ACTION_TYPE,
        type : SET_SCALE_DETAILS,
        payload: scaleObj
    }
}

//====================================================================================
//List of Banners
export function setBannerList(bannerList : Banner[]) : StoreAction {
    return{
        class: ACTION_TYPE,
        type: SET_BANNERS,
        payload: bannerList
    }
}

//====================================================================================
//List of Regions
export function setRegionList(regionList : Region[]) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_REGIONS,
        payload: regionList
    }
}

//====================================================================================
//List of Stores
export function setStoreList(storeList : {[key : string] : Store}) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_STORES,
        payload: storeList
    }
}

export function insertStoreList(storeObj : Store) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: INS_STORES,
        payload: storeObj,
        id: storeObj.storeId
    }
}

//====================================================================================
//List of Departments
export function setDepartmentList(deptList : {[key : string] : Department}) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_DEPARTMENTS,
        payload: deptList
    }
}

export function insertDepartment(dept : Department) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: INS_STORES,
        payload: dept,
        id: dept.deptId
    }
}

//====================================================================================
//Latest sync object
export function setSyncObject(syncObject) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_SYNC_STATUS,
        payload: syncObject
    }
}

export function setNodeObject(nodeObject, id) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_NODE_STATUS,
        payload: nodeObject,
        id: id
    }
}

//====================================================================================
//Latest sync object
export function setTransactions(transObject, id) : StoreAction {
    return {
        class: ACTION_TYPE,
        type: SET_TRANSACTIONS,
        id: id,
        payload: transObject
    }
}

//====================================================================================
//AssetList View Sync
export function setFilters(filterArray: {[key:string]: filter}) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_FILTERARRAY,
        payload: filterArray
    }
}
export function setPageView(pageView: boolean) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_PAGEVIEW,
        payload: pageView
    }
}
export function setAppHookObject(appHook : object) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_APPHOOKOBJECT,
        payload : appHook
    }
}
//====================================================================================
// UpgradeManager data
export function setBatches(batches : {[key:string]:Batch}) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_BATCHES,
        payload : batches
    }
}
export function setGroups(groups : object) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_GROUPS,
        payload : groups
    }
}
export function setUpgradeDevices(upgradeDevices : object) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_UPGRADEDEVICES,
        payload : upgradeDevices
    }
}
export function setUpgradeSelected(upgradeSelected : string[]) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_UPGRADESELECTED,
        payload : upgradeSelected
    }
}
export function setUpgradeFiles(upgradeFiles : upgradeFile[]) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_UPGRADEFILES,
        payload : upgradeFiles
    }
}
export function setReduxPrimaryScales(primaryScales : string[]) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_PRIMARYSCALES,
        payload : primaryScales
    }
}
export function setTimeframes(timeframes: Timeframe): StoreAction {
    return {
        class : ACTION_TYPE,
        type : SET_TIMEFRAME,
        payload : timeframes
    }
}
export function setAutoAssignRules(rules: AutoAssignRule[]): StoreAction {
    return {
        class : ACTION_TYPE,
        type : SET_AUTOASSIGNRULES,
        payload : rules
    }
}
export function setCriticalDialog(criticalDialog: string): StoreAction {
    return {
        class : ACTION_TYPE,
        type : SET_CRITICAL_DIALOG,
        payload : criticalDialog
    }
}
export function setPricingZones(pricingZones: PricingZone[]): StoreAction {
    return {
        class : ACTION_TYPE,
        type : SET_PRICING_ZONES,
        payload : pricingZones
    }
}
export function setNodeStatusTimers(updateTimers: {[key:string]: Date}): StoreAction {
    return {
        class : ACTION_TYPE,
        type : SET_NODE_STATUS_UPDATE_TIMERS,
        payload : updateTimers
    }
}