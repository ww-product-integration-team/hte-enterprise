import { DeleteEventConfig, OfflineAway } from "../../types/config/config"
import { Service } from "../../types/config/services"
import { ActionType, StoreAction } from "../../types/storeTypes"

export const SET_DELETE_EVENT = "SET_DELETE_EVENT"
export const SET_OFFLINE_CONFIG = "SET_OFFLINE_CONFIG"
export const SET_HEARTBEAT_OPERATION = "SET_HEARTBEAT_OPERATION"
export const SET_SERVICE_LIST = "SET_SERVICE_LIST"
export const SET_STORE_CONFIG = "SET_STORE_CONFIG"
export const SET_IN_TEST = "SET_IN_TEST"
export const SET_HYBRID_MULTIPATH = "SET_HYBRID_MULTIPATH"
export const SET_ZONE_PRICING = "SET_ZONE_PRICING"

const ACTION_TYPE = ActionType.Config
//====================================================================================
//
export function setDeleteEvent(data : DeleteEventConfig) : StoreAction {
    return {
        type: SET_DELETE_EVENT,
        class: ACTION_TYPE,
        payload: data
    }
}

export function setOfflineConfig(data : OfflineAway) : StoreAction {
    return {
        type: SET_OFFLINE_CONFIG,
        class: ACTION_TYPE,
        payload: data
    }
}

export function setServicesList(data : Service[]) : StoreAction{
    return {
        type: SET_SERVICE_LIST,
        class: ACTION_TYPE,
        payload: data
    }
}

export function setStoreConfig(data: {sendStoreInfo: "true" | 'false', 
                                        sendStoreNameInfo: string}) : StoreAction{
    return {
        type: SET_STORE_CONFIG,
        class: ACTION_TYPE,
        payload: data
    }
}

export function setInTestingMode(data: boolean) : StoreAction{
    return {
        type: SET_IN_TEST,
        class: ACTION_TYPE,
        payload: data
    }
}

export function setHybridMultipath(data: boolean): StoreAction {
    return {
        type: SET_HYBRID_MULTIPATH,
        class: ACTION_TYPE,
        payload: data
    }
}

export function setZonePricing(data: boolean): StoreAction {
    return {
        type: SET_ZONE_PRICING,
        class: ACTION_TYPE,
        payload: data
    }
}