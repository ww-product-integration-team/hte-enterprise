
import { ActionType, StoreAction} from "../../types/storeTypes";
import { prod } from "../../components/prodsTable/prodsTable";

export const SET_PRODUCTS = "SET_PRODUCTS";

const ACTION_TYPE = ActionType.Products
//====================================================================================
export function setProducts(products : prod[]) : StoreAction{
    return {
        class : ACTION_TYPE,
        type : SET_PRODUCTS,
        payload : products
    }
}