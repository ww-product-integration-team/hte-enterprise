import { TypedUseSelectorHook, useSelector} from "react-redux"
import { persistStore, persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import thunk from "redux-thunk"
import appReducer from "./reducers/appReducer"
import { configureStore } from '@reduxjs/toolkit'

const persistConfig = {
    key: 'root',
    version: 1,
    storage,
}

const persistedReducer = persistReducer(persistConfig, appReducer)

//For Develop Release
//==================================================================
export const composeEnhancers =
  (window && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__);


  const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        serializableCheck: false
      }).prepend(thunk),
  })
const persistor = persistStore(store)

//For Master Release
//==================================================================
//const store = createStore(persistedReducer, applyMiddleware(thunk))
//const persistor = persistStore(store)

export type RootStore = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

const useAppSelector: TypedUseSelectorHook<RootStore> = useSelector

export {store, persistor, useAppSelector}