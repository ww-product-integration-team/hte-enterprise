
export enum ActionType {
    Access, Asset, Config, Endpoint, Profile, Event, Socket, Products
}

export interface StoreAction {
    type : string, //The Action being called
    class : ActionType //Which State does this action belong to 
    id ?: string, //Specific ID to modify
    subAction ?: string, //Specific behavior under one action tye
    payload: any,

}

export interface ResponseType {
    id: number
    type: string
    action: string
    success: boolean
    cmd ?: string
    cancelled ?: boolean
    response : any
}


export enum DiscriminatorType{
    Banner, Region, Store, Department, ScaleDevice, ScaleSettings, NoDiscriminator
}

export function instanceOf(obj : any) : DiscriminatorType{
    
    if(obj.bannerId){
        return DiscriminatorType.Banner
    }
    else if(obj.regionId && obj.bannerId){
        return DiscriminatorType.Region
    }
    else if(obj.storeId && obj.regionId){
        return DiscriminatorType.Store
    }
    else if(obj.deptId && obj.deptName1){
        return DiscriminatorType.Department
    }
    else if(obj.deviceId && obj.deptId && obj.storeId){
        return DiscriminatorType.ScaleDevice
    }


    return DiscriminatorType.NoDiscriminator
}
