import { instanceOfScaleDevice, ScaleDevice, ScaleStatusType, SyncStatus } from "./ScaleTypes"

export interface NodeStatus {
    status : ScaleStatusType
    sync : SyncStatus
    syncMessage : string
    message : string
    numOnline : number
    numError : number 
    numWarning : number
    numDisabled : number
    numUnknown : number
}

export function instanceOfNodeStatus(object : NodeStatus) : boolean{
    if(!object){return false}

    return ("status" in object && Object.values(ScaleStatusType).includes(object.status) &&
            "sync" in object && Object.values(SyncStatus).includes(object.sync) && 
            "syncMessage" in object && typeof object.syncMessage === "string" &&
            "message" in object && typeof object.message === "string" &&
            "numOnline" in object && typeof object.numOnline === "number" &&
            "numError" in object && typeof object.numError === "number" &&
            "numWarning" in object && typeof object.numWarning === "number" &&
            "numDisabled" in object && typeof object.numDisabled === "number" &&
            "numUnknown" in object && typeof object.numUnknown === "number")
}


export interface Node {
    id : string
    name : string
    text : string | null
    type : "banner" | "region" | "store" | "dept" | "scale"
    children : Node[]
    nodeStatus : NodeStatus
    batchIds : string[]
    properties : ScaleDevice | null
}

export interface lazyNode {
    id : string
    name : string
    path : string []
    text : string | null
    type : "banner" | "region" | "store" | "dept" | "scale"
    children : {[id : string] : lazyNode}
    hasChildren : boolean
    nodeStatus : NodeStatus
    batchIds : string[]
    properties : ScaleDevice | null
    filterId : string
}

export function instanceOfNode(object : any) : boolean{
    if(!object){return false}

    const possibleTypes = ["banner", "region", "store", "department", "scale"]
    return ("id" in object && typeof object.id === "string" &&
            "name" in object && typeof object.name === "string" &&
            "text" in object && (typeof object.text === "string" || object.text === null) &&
            "type" in object && possibleTypes.includes(object.type) && 
            "children" in object && Array.isArray(object.children) && 
            "nodeStatus" in object && instanceOfNodeStatus(object.nodeStatus) &&
            "properties" in object && (instanceOfScaleDevice(object.properties) || object.properties === null))

}