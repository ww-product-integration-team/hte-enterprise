import { Service } from "../config/services"

export enum AssetTypes{
    Banner, Region, Department, Store, Scale
}

export interface Banner {
    bannerId : string
    bannerName : string
}

export interface Region {
    regionId : string
    regionName : string
    bannerId : string
}

export interface Store { 
    storeId : string
	storeName : string
	address : string
	regionId : string
	customerStoreNumber : string
	enabled : boolean
	ipAddress : string | null
	hostname : string | null
	triggerType : string | null
	url : string | null
	services : Service[]
	lastRebootTimestampUtc : string | null
	lastCommunicationTimestampUtc : string | null
	lastItemChangeReceivedTimestampUtc : string | null
	lastFetchPluOffsetTimestampUtc : string | null
    sendStoreInfo : null | "true" | "false" | string
    assignedRegion ?: string
    assignedBanner ?: string
}

export function getDefaultDepartment() : Department{
    return{
        deptId : "Unknown",
        deptName1 : "Department not Found",
        deptName2 : "Unknown",
        defaultProfileId : "Unknown",
        requiresDomainId : "Unknown"
    }
}

export interface Department {
	deptId : string
	deptName1 : string
	deptName2 : string
	defaultProfileId : string
	requiresDomainId : string

    assignedStore ?: string //This should always be null/undefined in the store
                            //Only for lists extracted from Tree Obj
}

export interface PricingZone {
    id: string
    name: string
}