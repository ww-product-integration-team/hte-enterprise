export interface Profile{
    profileId : string
    name : string
    description : string
    lastUpdate : string
    requiresDomainId : string
    deleteSyncConfig : string | null
    zoneId: string
}

export function getDefaultProfile() : Profile{
    return{
        profileId : "",
        name : "Unknown",
        description : "Profile Not Found",
        lastUpdate : "None",
        requiresDomainId : "",
        deleteSyncConfig : "" ,
        zoneId: ""
    }
}

export interface ChecksumLog{
    date: string
    scaleIp: string
    logEntry: string
}