export enum FileType{
    FEATURE = "FEATURE",
    CONFIG = "CONFIG",
    DEBIAN = "DEBIAN",
    TARBALL = "TARBALL",
    INSTALL_IMAGE = "INSTALL_IMAGE",
    LICENSE = "LICENSE",

    IMAGE_CATEGORY = "IMAGE_CATEGORY",
    IMAGE_PRODUCT = "IMAGE_PRODUCT",
    IMAGE_PLANOGRAM = "IMAGE_PLANOGRAM",
    IMAGE_SELFSERVICE = "IMAGE_SELFSERVICE",
    IMAGE_SOFTKEY = "IMAGE_SOFTKEY",
    IMAGE_SPECIAL = "IMAGE_SPECIAL",
    IMAGE_LAYOUT = "IMAGE_LAYOUT",
    IMAGE_ONLINE_ORDERING = "IMAGE_ONLINE_ORDERING",
    IMAGE_NOW_SERVING = "IMAGE_NOW_SERVING",

    VIDEO = "VIDEO",
    AUDIO = "AUDIO",
    PLAYLIST = "PLAYLIST",
    LAYOUT = "LAYOUT",
    FONT = "FONT",
    DOCUMENT = "DOCUMENT",
    UNKNOWN = "UNKNOWN"
}

export function getDefaultFile() : FileForEvent{
    return({
        fileId : "Unknown",
        filename : "Unknown",
        shortDesc : "Unknown",
        fileType : FileType.UNKNOWN,
        fileVersion : "Unknown",
        size : 0,
        checksum : "Unknown",
        uploadDate : "Unknown",
        profileId : "Unknown",
        startDate : "Unknown",
        endDate : "Unknown",
        enabled : false,
        content : "Unknown"
    })
}

export function getValidFileExensions(fileTypes : FileType[]) : string[]{
    let extensions : string[] = []

    fileTypes.forEach((fileType : FileType) => {
        switch(fileType){
            case FileType.FEATURE:
                extensions = extensions.concat([".package.tgz"])
                break
            case FileType.DEBIAN:
            case FileType.TARBALL:
            case FileType.INSTALL_IMAGE:
                extensions = extensions.concat([".deb", ".tgz", ".img.bz2"])
                break
            case FileType.CONFIG:
                extensions = extensions.concat([".ht", ".label_tagged", ".hlx", ".bin"])
                break
            case FileType.LICENSE:
                extensions = extensions.concat([".lic"])
                break
            case FileType.IMAGE_CATEGORY:
            case FileType.IMAGE_PRODUCT:
            case FileType.IMAGE_PLANOGRAM:
            case FileType.IMAGE_SELFSERVICE:
            case FileType.IMAGE_SOFTKEY:
            case FileType.IMAGE_SPECIAL:
            case FileType.IMAGE_LAYOUT:
            case FileType.IMAGE_ONLINE_ORDERING:
            case FileType.IMAGE_NOW_SERVING:
                extensions = extensions.concat([".jpg", ".gif", ".png", ".bmp"])
                break
            case FileType.VIDEO:
                extensions = extensions.concat([".mp4", ".avi"])
                break
            case FileType.AUDIO:
                extensions = extensions.concat([".m4a", ".aac"])
                break
            case FileType.PLAYLIST:
                extensions = extensions.concat([".txt"])
                break
            case FileType.LAYOUT:
                extensions = extensions.concat([".png", ".xml"])
                break
            case FileType.FONT:
                extensions = extensions.concat([".ttf"])
                break
            case FileType.DOCUMENT:
                extensions = extensions.concat([".pdf", ".doc", ".docx", ".xls", ".xlsx"])
                break
            default:
                extensions = extensions.concat([])
        }
    })
    //Remove duplicates
    extensions = extensions.filter((value, index, self) => {
        return self.indexOf(value) === index
    })
    return extensions
}


export function getImageTypes() : FileType[]{
    return [
        FileType.IMAGE_CATEGORY,
        FileType.IMAGE_PRODUCT,
        FileType.IMAGE_PLANOGRAM,
        FileType.IMAGE_SELFSERVICE,
        FileType.IMAGE_SOFTKEY,
        FileType.IMAGE_SPECIAL,
        FileType.IMAGE_LAYOUT,
        FileType.IMAGE_ONLINE_ORDERING,
        FileType.IMAGE_NOW_SERVING
    ]
}


export function IsImageType(type : FileType) : boolean{
    switch(type){
        case FileType.IMAGE_CATEGORY:
        case FileType.IMAGE_PRODUCT:
        case FileType.IMAGE_PLANOGRAM:
        case FileType.IMAGE_SELFSERVICE:
        case FileType.IMAGE_SOFTKEY:
        case FileType.IMAGE_SPECIAL:
        case FileType.IMAGE_LAYOUT:
        case FileType.IMAGE_ONLINE_ORDERING:
        case FileType.IMAGE_NOW_SERVING:
            return true
        default:
            return false
    }
}

export function PrintFileType(type : FileType) : string{
    switch(type){
        case FileType.FEATURE:
            return "Feature"
        case FileType.CONFIG:
            return "Config"
        case FileType.DEBIAN:
            return "Debian"
        case FileType.TARBALL:
            return "Tarball"
        case FileType.INSTALL_IMAGE:
            return "Install Image"
        case FileType.LICENSE:
            return "License"
        case FileType.IMAGE_CATEGORY:
            return "Category Image"
        case FileType.IMAGE_PRODUCT:
            return "Product Image"
        case FileType.IMAGE_PLANOGRAM:
            return "Planogram Image"
        case FileType.IMAGE_SELFSERVICE:
            return "Self Service Image"
        case FileType.IMAGE_SOFTKEY:
            return "Soft Key Image"
        case FileType.IMAGE_SPECIAL:
            return "Specials Image"
        case FileType.IMAGE_LAYOUT:
            return "Layout Image"
        case FileType.IMAGE_ONLINE_ORDERING:
            return "Online Ordering Image"
        case FileType.IMAGE_NOW_SERVING:
            return "Now Serving Image"
        case FileType.VIDEO:
            return "Video"
        case FileType.AUDIO:
            return "Audio"
        case FileType.PLAYLIST:
            return "Playlist"
        case FileType.LAYOUT:
            return "Layout"
        case FileType.FONT:
            return "Font"
        case FileType.DOCUMENT:
            return "Document"
        case FileType.UNKNOWN:
            return "Unknown"
    }
}

export interface FileForEvent{
    fileId : string
    filename : string
    shortDesc : string
    fileType : FileType
    fileVersion : string
    size : number
    checksum : string
    uploadDate : string
    profileId : string
    startDate : string
    endDate : string
    enabled : boolean
    content : string
}