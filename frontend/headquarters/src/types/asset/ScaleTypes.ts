export enum ScaleStatusType {
    UNKNOWN     = "UNKNOWN",
    ONLINE      = "ONLINE", 
    WARNING     = "WARNING",
    ERROR       = "ERROR",
    DISABLED    = "DISABLED",
}

export enum SyncStatus {
    NA          = "NA",
    NOT_SYNCED  = "NOT_SYNCED",
    PENDING     = "PENDING",
    SYNCED      = "SYNCED",
    UNKNOWN     = "UNKNOWN"
}

export interface SyncData {
    start : number
    end : number
    del : number 
    range : string
    numScales : number
    numHeartbeats : number[]
    numInSync : number[]
    uptime: string
}

export interface ScaleDevice {
    deviceId : string
    ipAddress : string
    hostname : string
    serialNumber : number
    scaleModel : string
    storeId : string
    deptId : string
    countryCode : string
    buildNumber : string
    application : string
    operatingSystem : string
    systemController : string
    loader : string
    smBackend : string
    lastReportTimestampUtc : string
    lastTriggerType : "INITIAL" | "INTERVAL" | "MANUAL" | "SCALE_POWER_UP" | "SCALE_POWER_DOWN" | "WAS_OFFLINE"
    profileId : string
    isPrimaryScale : boolean
    enabled : boolean
    pluCount ?: number
    totalLabelsPrinted ?: number
    labelStockSize?: string
    upgradeId? : string
    weigherPrecision?: string
    assignedDept?: string
    assignedStore?:string
    profileRecentlyUpdated?:boolean
    healthInfoLog : string
}

export function instanceOfScaleDevice(object : any) : boolean{
    if(!object){return false}

    let triggerTypes = ["INITIAL", "INTERVAL", "MANUAL", "SCALE_POWER_UP", "SCALE_POWER_DOWN", "WAS_OFFLINE"]

    return ("deviceId" in object && typeof object.deviceId === "string" &&
            "ipAddress" in object && typeof object.ipAddress === "string" &&
            "hostname" in object && typeof object.hostname === "string" &&
            "serialNumber" in object && typeof object.serialNumber === "number" &&
            "scaleModel" in object && typeof object.scaleModel === "string" &&
            "storeId" in object && typeof object.storeId === "string" &&
            "deptId" in object && typeof object.deptId === "string" &&
            "countryCode" in object && typeof object.countryCode === "string" &&
            "buildNumber" in object && typeof object.buildNumber === "string" &&
            "application" in object && typeof object.application === "string" &&
            "operatingSystem" in object && typeof object.operatingSystem === "string" &&
            "systemController" in object && typeof object.systemController === "string" &&
            "loader" in object && typeof object.loader === "string" &&
            "smBackend" in object && typeof object.smBackend === "string" &&
            "lastReportTimestampUtc" in object && typeof object.lastReportTimestampUtc === "string" &&
            "lastTriggerType" in object && triggerTypes.includes(object.lastTriggerType) &&
            "profileId" in object && typeof object.profileId === "string" &&
            "primaryScale" in object && typeof object.primaryScale === "boolean" &&
            "enabled" in object && typeof object.enabled === "boolean" &&
            "pluCount" in object && (object.pluCount === null || typeof object.pluCount === "number") &&
            "totalLabelsPrinted" in object && (object.totalLabelsPrinted === null || typeof object.totalLabelsPrinted === "number"))
}

export function getDefaultDevice() : ScaleDevice{
    return {
        deviceId : "Unknown",
        ipAddress : "127.0.0.1",
        hostname : "Unknown",
        serialNumber : 0,
        scaleModel : "Unknown",
        storeId : "Unknown",
        deptId : "Unknown",
        countryCode : "Unknown",
        buildNumber : "Unknown",
        application : "Unknown",
        operatingSystem : "Unknown",
        systemController : "Unknown",
        loader : "Unknown",
        smBackend : "Unknown",
        lastReportTimestampUtc : "Unknown",
        lastTriggerType : "INITIAL",
        profileId : "Unknown",
        isPrimaryScale : false,
        enabled : false,
        pluCount : 0,
        totalLabelsPrinted : 0,
        profileRecentlyUpdated : false,
        healthInfoLog: "No Health Log"
    }
}

export interface ScaleSettings {
    deviceId : string
    macAddress: string
    ipAddress : string
    appHookVersion : string
    lastRebootTimestampUtc : string
    lastCommunicationTimestampUtc : string
    lastFetchPluOffsetTimestampUtc : string
    licenses : string
    features : string
    storeInformation : string
    storeGraphicName : string
    scaleMode : string
    scaleItemEntryMode : string
    labelStockSize : string
    hasLoadCell : boolean
    weigherPrecision : string
    weigherCalibratedTimestampUtc : string
    weigherConfiguredTimestampUtc : string
    hasCustomerDisplay : boolean
    timezone : string
    timeKeeper : string
    currentFiles : string
}