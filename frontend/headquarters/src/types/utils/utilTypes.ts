import { AlertColor } from "@mui/material"

export interface AlertMessage{
    show: boolean
    severity : AlertColor
    message: string
}

export function ShowErrorMessage(message: string): AlertMessage {
    return {
        show: true,
        severity: "error",
        message: message
    }
}

export function ShowSuccessMessage(message: string): AlertMessage {
    return {
        show: true,
        severity: "success",
        message: message
    }
}

export function ShowInfoMessage(message: string): AlertMessage {
    return {
        show: true,
        severity: "info",
        message: message
    }
}

export function ShowWarningMessage(message: string): AlertMessage {
    return {
        show: true,
        severity: "warning",
        message: message
    }
}


export function HideMessage(): AlertMessage {
    return {
        show: false,
        severity: "info",
        message: ""
    }
}