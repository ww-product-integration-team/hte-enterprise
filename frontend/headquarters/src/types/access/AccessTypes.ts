export interface HTeDomain {
    domainId: string,
    type: string,
    name: string,
    parentId: string | null
}

export interface HTeUser {
    id : string,
    firstName: string,
    lastName: string,
    username: string,
    password: string,       // This will be encrypted
    phoneNumber: string | null,
    accessLevel: number,    // 1000, 2000, 3000, 4000?
    domain: HTeDomain,
    roles: HTeRole[]
    enabled: boolean,
    dateCreated: string
}

export interface HTeRole {
    id:number,
    name:string,
    description:string
}

export interface JWT_Tokens{
    access_token : string,
    refresh_token : string
}