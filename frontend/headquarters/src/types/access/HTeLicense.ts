export interface License {
    licenseId:string,
    license:string,
    fileName: string,
    ownerEmail: string,
    fullName: string,
    companyName:string,
    companyAddress:string,
    activationDate:string,
    timeout: number,
    productNames: string,
    numUsers: number,
    numScales: number
}