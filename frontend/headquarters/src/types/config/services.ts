export enum ServiceType{
    HEARTBEAT = "HEARTBEAT",
    ITEM_LOOKUP = "ITEM_LOOKUP",
    STORE_CLIENT = "STORE_CLIENT",
    STORE_SERVER = "STORE_SERVER",
    UI_MANAGER = "UI_MANAGER",
    FRONTEND = "FRONTEND",
}

export interface Service {
    id:string
    databaseId:string
    serviceType:ServiceType
    serviceName:string
    operationMode :string
    lastRestart :string
    url :string
    version :string
    buildDate   :string
}