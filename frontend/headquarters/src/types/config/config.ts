
export enum DeleteIntervals {
    Never = "Never",
    Daily = "Daily",
    Weekly = "Weekly",
    Monthly = "Monthly"
}

export enum DeleteTypes{
    flashkey = "flashkey",
    plu = "plu",
    config = "config",
    operator = "operator",
    label = "label"
}

export interface DeleteEventConfig {
    interval : DeleteIntervals
    time : number
    day: number
    deleteTypes : DeleteTypes[]
}

export interface OfflineAway{
    scaleAwayHours: number|null,
    scaleOfflineHours: number|null
}

export interface StoreConfiguration{
    sendStoreInfo ?: "true" | "false",
    customStoreName ?: string | null,
}

export function RemoveDuplicates<T>(array: T[]) {
    const result: T[] = [];
    array.forEach((item) => {
        if (!result.includes(item)) {
            result.push(item);
        }
    })
    return result;
}