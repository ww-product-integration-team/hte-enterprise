export interface TransEntry { 
    TransID : number
    scaleId : string
    TrScID : number
    TrID : number
    TTNum : number
    TrGrp : number
    trDtTm : string
    TrVoid : number
    TrAPrdt : number
    TrRdClr : number
    TrNmPck : number
    TrSecLb : boolean
    TrCOOLF : boolean
    trTtVal : number
    trOTtVl : number
    deptNum : number
    trPrNum : number
    TrClNum : number
    trPrTyp : number
    TrPrPlT : number
    trPrDes : string
    TrPrMod : number
    TrBarPref : number
    TrBarNum : string
    TrUnPr : number
    TrExPr : number
    TrFnlPr : number
    trTare : number
    trPrTare : number
    TrFnlTare : number
    trByCount : number
    trFnlNtWt : number
    trFxWt : number
    TrSfLfD : number
    TrSfLfH : number
    TrSfLfM : number
    TrPrLfD : number
    TrPrLfH : number
    TrPrLfM : number
    trLab1 : number
    trLab2 : number
    trLab3 : number
    trLab4 : number
    trOpNum : string
    trOpRFID : string
    TrOpName : string
    TrOpChg : number
    TrOBarDig : number
    TrOUnPr : number
    TrOFnPr : number
    TrOTare : number
    TrOByCount : number
    TrOFxWt : number
    TrOSfLfD : number
    TrOSfLfH : number
    TrOSfLfM : number
    TrOPrLfD : number
    TrOPrLfH : number
    TrOPrLfM : number
    trOLab1 : number
    trOLab2 : number
    trOLab3 : number
    trOLab4 : number
    trGrNm1 : string
    trGrNm2 : string
    trGrNm3 : string
    trGrNm4 : string
    TrCOTxt : string
    TrCOTrTxt : string
    trOGrNm1 : string
    trOGrNm2 : string
    trOGrNm3 : string
    trOGrNm4 : string
    TrUTNum : number
    TrUTPercentOfPrimalWeight : number
    TrUTPercentOfPrimalCost : number
    TrModeChange : number
    TrSweetheartWeight : number
}

export interface TransactionData {
    list : TransEntry[]
    numberOfTransactions : number
    totalPrice : number
    totalWeight : number
    start : string
    end : string
    span : string
    plus : string[]
    scales : string[]
    depts : string[]
    stores : string[]

}

export interface TransactionRequest{
    transData: boolean 
    url: string
    plus: number[]
    scaleIps: string[]
    startDate: string //'MM-DD-YYYY',
    endDate: string //'MM-DD-YYYY',
    storeId?: string //UUID of store (Nullable)
    deptId?: string //UUID of dept (Nullable)
    filename: string // Name that the file will download as (if you're downloading)
}