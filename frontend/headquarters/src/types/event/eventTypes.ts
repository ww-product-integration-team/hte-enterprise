export interface Event {
    timestamp: Date,
    eventId: number,
    entityId: string,
    entityType: string,
    type: string,
    opened: boolean,
    flagged: boolean,
    muted: boolean,
    statusStr: string,
    occurrences: string,
    event: string,
    message: string
}