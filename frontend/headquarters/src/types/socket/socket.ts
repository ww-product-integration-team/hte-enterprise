
export enum AsyncState {
    IDLE = 'IDLE',
    RUNNING = 'RUNNING',
    CANCELLED = 'CANCELLED',
}

export interface ScaleStatusModel{
    scaleIP: string;
    firmwareVersion: string;
    scaleType: string;
    storeId: string;
    pluCount: number;
    freeMemory: number;
    totalMemory: number;
}

export interface ScaleStatusDTO{

    deviceId: string;
    ipAddress: string;

    asyncState: AsyncState;
    asyncProgress: number;
    pingSuccess: boolean;

    statusModel: ScaleStatusModel | null;

    lastReportTimestamp: string;
    apphookVersion: string;
    message: string;
}