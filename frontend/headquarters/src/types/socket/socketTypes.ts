import { Dispatch, SetStateAction } from "react"

export interface Socket {
    message: any,
    status: "UNKNOWN" | "CONNECTED" | "DISCONNECTED",
    // setStatus: Dispatch<SetStateAction<"UNKNOWN" | "CONNECTED" | "DISCONNECTED">>,
    setStatus: (status: "UNKNOWN" | "CONNECTED" | "DISCONNECTED") => void
}