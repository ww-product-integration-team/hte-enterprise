#! /bin/bash

#path to where the existing frontend is being hosted
tomcatPath=/var/lib/tomcat9/webapps/ROOT
#path of the folder holding the the new build files
newBuildPath=/home/hobart/Documents/frontend_builds
#path of the folder holding the WEB config folders for tomcat
configPath="$newBuildPath/TomcatConfig"


if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

#Check to see it the file exists in the current directory
if [ ! -d "$newBuildPath/$1" ]
  then
    echo "Build directory does not exist!"
    exit 1
fi

echo " "
read -p "Are you sure, have you created a backup of the existing configuration? " -n 1 -r
echo " "   # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

echo "Updating frontend with version: $1 ...";
echo " ";
echo "Deleting existing files from $tomcatPath..."
rm -r $tomcatPath
mkdir $tomcatPath
echo "Copying build files from $newBuildPath..."
cp -a "$newBuildPath/$1/." $tomcatPath 
echo "Copying config files from $configPath..."
cp -a "$configPath/." $tomcatPath

echo "Done!"


