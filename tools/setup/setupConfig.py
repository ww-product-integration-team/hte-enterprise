import os
import shutil
import subprocess
from time import sleep
try:
    import pexpect
except e:
    print("Warning could not import pexpect")
import socket
import sys
import re
from datetime import date
try:
    import wexpect
except:
    print("Warning could not import wexpect")
today = date.today()


certPath = "/home/hobart/Documents/certificates/"
buildPath = "/home/hobart/Documents/frontend_builds/" #"C:\\Users\\caleb.vanhaaren\\Documents\\Hobart\\HTe-Enterprise\\frontend\\build\\static\\js"
tomcatPath = "/var/lib/tomcat9/"
caCertsPath = "/etc/ssl/certs/java/cacerts"
rootCertPath = "rootCA.crt"

if os.name == "nt":
    certPath = "C:\\Users\\Public\\Documents\\hte\\certificates\\"
    buildPath = "C:\\Users\\Public\\Documents\\hte\\setup\\frontend_builds\\"
    tomcatPath = "C:\\Program Files (x86)\\Apache Software Foundation\\Tomcat 9.0"
    caCertsPath = "C:\\Program Files\\OpenJDK\\jdk-8.0.342.07-hotspot\\jre\\lib\\security\\cacerts"


frontendRelease = None
UPDATEFRONTEND_SH = "updateFrontend.sh"

#IP Address of the tomcat server
newIP = "255.255.255.255"
hostnameFQDN, hostname = None, None
#Key denoting the URL the frontend code uses to point to the server
SERVER_URL = "\"SERVER_URL\":"
SERVER_PORT = "\"SERVER_PORT\":"
SERVER_PRODUCT = "\"SERVER_PRODUCT\":"
#Cert config file used to create the certs
CERT_CONFIG = "customerCertConfig"
DOM_CONFIG = "domain.ext"

def printUsage():
    print("")
    print("================ Setup Config Usage : ================")
    #print("Caleb van Haaren: Oct 13, 2022")
    print("     -v : (--version) Indicate the version of the frontend release you would like to install ex: {-v 1.0.0}")
    print("          Must have the frontend folder with a matching name or this will not work. ")
    print("          NOTE: If not used the program will select the highest version found")
    print("     -b : (--buildPath) Indicate a custom build path, this is where the program will look for the frontend builds")
    print("          NOTE: Default path is: {}".format(buildPath))
    print("     -c : (--certPath) Indicates a custom certificate path, this is where the tomcat server.xml file is pointing")
    print("          NOTE: Default path is: {}".format(certPath))
    print("     -i : (--serverIP) IP Address of the server, program will try to find IP on it's own if not specified")
    print("     -f : (--serverHostname) Enter the Fully Qualifed Domain name of the server")
    print("     -g : (--genCert) Generate a new certificate for the server and install it in the certificates folder")
    print("     -u : (--installFrontend) Update where the frontend files will be pointing and install them on Tomcat")
    print("     -p : (--useHTTP) Setup the frontend to use HTTP to communicate instead of HTTPS")
    print("     -d : (--useHostname) Point the frontend to the hostname of the server rather than the ipAddress")
    print("     -k : (--trustRootCA) install the root certificate used to sign the HTTPS key into the java trust store to")
    print("          allow for https communication, default caCerts path is: {}".format(caCertsPath))
    print("")


def updateFrontendEndpoints(buildPath, newIP, http="https", newPort = "8443", newEndpoint = "hte-enterprise/"):
    srcPath = os.path.join(buildPath, "static", "js")
    fileNames = os.listdir(srcPath)
    print('\033[92m' + "New server endpoint is now {}".format(http + "://" + newIP + ":" + newPort) + '\033[0m')
    found = 0
    for file in fileNames:
        if file.endswith(".js"):
            f = open(os.path.join(srcPath, file),'r')
            filedata = f.read()
            f.close()


            #Update SERVER URL
            idx = filedata.find(SERVER_URL)
            if(idx != -1):
                end = filedata[idx:].find(",")
                subStr = filedata[idx:idx+end]
                filedata = filedata.replace(subStr, "{}\"{}\"".format(SERVER_URL, http + "://" + newIP))
                found+=1
            else:
                #These configurations will all be in the same build file, if one can't be found none of them can
                continue


            #Update SERVER PORT
            idx = filedata.find(SERVER_PORT)
            if(idx != -1):
                end = filedata[idx:].find(",")
                subStr = filedata[idx:idx+end]
                filedata = filedata.replace(subStr, "{}\"{}\"".format(SERVER_PORT, newPort))
                found+=1

            #Update SERVER PRODUCT
            idx = filedata.find(SERVER_PRODUCT)
            if(idx != -1):
                end = filedata[idx:].find(",")
                subStr = filedata[idx:idx+end]
                filedata = filedata.replace(subStr, "{}\"{}\"".format(SERVER_PRODUCT, newEndpoint))
                found+=1

            f = open(os.path.join(srcPath, file),'w')
            f.write(filedata)
            f.close()
    
    if(found != 3):
        print("FATAL ERROR: Could not set all frontend endpoint configurations {}".format(found))

def updateCertConfig():
    global hostnameFQDN, hostname
    if(hostnameFQDN == None or hostname == None):
        print("ERROR : Could not find FQDN!")
        return -1

    country = input("Please enter the customers country: ")
    state = input("Please enter the customers state: ")
    city = input("Please enter the customers city: ")
    company = input("Please enter the customers company name: ")
    department = input("Please enter the customers unit name: ")

    def findAndReplace(key, name, value):
        f = open(CERT_CONFIG,'r')
        filedata = f.read()
        f.close()
        idx = filedata.find(key)
        if idx != -1:
            end = filedata[idx:].find("\n")
            subStr = filedata[idx:idx+end]
            filedata = filedata.replace(subStr, key + value)

            f = open(CERT_CONFIG,'w')
            f.write(filedata)
            f.close()
            return 0
        else:
            print("ERROR: Could not find {} name ({}) entry".format(name, key))
            return -1

    status = 0
    status += findAndReplace("C = ", "Country", country)
    status += findAndReplace("ST = ", "State", state)
    status += findAndReplace("L = ", "City", city)
    status += findAndReplace("O = ", "Company", company)
    status += findAndReplace("OU = ", "Business Unit", department)
    status += findAndReplace("CN = ", "Server", hostnameFQDN)

    if status != 0:
        print("Could not update customer certificate config!")
        return -1 

    
    
    return 0

def updateDomainConfig():
    global hostnameFQDN, hostname
    if(hostnameFQDN == None or hostname == None):
        print("ERROR : Could not find FQDN!")
        return -1



    def findAndReplace(key, name, value):
        f = open(DOM_CONFIG,'r')
        filedata = f.read()
        f.close()
        idx = filedata.find(key)
        if idx != -1:
            end = filedata[idx:].find("\n")
            subStr = filedata[idx:idx+end]
            filedata = filedata.replace(subStr, key + value)

            f = open(DOM_CONFIG,'w')
            f.write(filedata)
            f.close()

            return 0
        else:
            print("ERROR: Could not find {} ({}) entry".format(name, key))
            return -1

    status = 0
    status += findAndReplace("DNS.1 = ", "hostname", hostname)
    status += findAndReplace("DNS.2 = ", "FQDN", hostnameFQDN)
    status += findAndReplace("IP.1 = ", "Ip Address", newIP)

    if status != 0:
        print("Could not update domain certificate config!")
        return -1 

    return 0

def createCert():
    print("=====  Creating certs...  =====")
    print("Creating key and signing request...")
    keyPassword = input("Please create password to be used for key (Please save this!): ")

    CSRprocess = None
    if os.name == "nt":
        CSRprocess = wexpect.spawn("openssl req -newkey rsa:2048 -keyout domain.key -out domain.csr -config customerCertConfig")
    else:
        CSRprocess = pexpect.spawn("openssl req -newkey rsa:2048 -keyout domain.key -out domain.csr -config customerCertConfig")
    CSRprocess.expect("Enter PEM pass phrase:")
    CSRprocess.sendline(keyPassword)
    CSRprocess.expect("Verifying - Enter PEM pass phrase:")
    CSRprocess.sendline(keyPassword)
    sleep(5)
    print("Updating Domain Config...")
    if(updateDomainConfig() != 0): return
    print("Signing key with root cert...")
    rootPassword = input("Please input the password for the root key: ")


    SignProcess = None
    if os.name == "nt":
        SignProcess = wexpect.spawn("openssl x509 -req -CA rootCA.crt -CAkey rootCA.key -in domain.csr -out customerCert.crt -days 730 -CAcreateserial -extfile domain.ext")
    else:
        SignProcess = pexpect.spawn("openssl x509 -req -CA rootCA.crt -CAkey rootCA.key -in domain.csr -out customerCert.crt -days 730 -CAcreateserial -extfile domain.ext")
    SignProcess.expect("Enter pass phrase for rootCA.key:")
    SignProcess.sendline(rootPassword)
    

    print("Packaging....")
    sleep(5)
    process = None
    if os.name == "nt":
        process = wexpect.spawn("openssl pkcs12 -export -out domain.p12 -inkey domain.key -in customerCert.crt")
    else:
        process = pexpect.spawn("openssl pkcs12 -export -out domain.p12 -inkey domain.key -in customerCert.crt")
    process.expect("Enter pass phrase for domain.key:")
    process.sendline(keyPassword)
    process.expect("Enter Export Password:")
    process.sendline('admin8758')
    process.expect("Verifying - Enter Export Password:")
    process.sendline('admin8758')
    process.read()
    print(".p12 package complete")

def updateFrontend():
    
    print("move Windows")
    madeBackup = input("Are you sure, have you created a backup of the existing configuration? (y/n)")
    if(madeBackup != "y" and madeBackup != "Y"):
        print("Aborting frontend move...")
        return
    print("Updating frontend with version: {} ...".format(frontendRelease))
    print("")
    dirPath = os.path.join(tomcatPath, "webapps", "ROOT")
    print("Deleting existing files from {}...".format(dirPath))
    if os.path.exists(dirPath):
        shutil.rmtree(dirPath)

    try:
        os.mkdir(dirPath)
    except FileExistsError:
        print("Directory Exists, continueing")
    bPath = os.path.join(buildPath, frontendRelease)
    print("Copying build files from {}...".format(bPath))
    shutil.copytree(bPath, dirPath, dirs_exist_ok=True)
    cPath = os.path.join(buildPath, "TomcatConfig")
    print("Copying config files from {}...".format(cPath))
    shutil.copytree(cPath, dirPath, dirs_exist_ok=True)
    print("done!")

def trustRootCert():
    process = None
    if os.name == "nt":
        process = wexpect.spawn('keytool -import -trustcacerts -storepass changeit -alias hte-enterprise -keystore {} -file {} -noprompt'.format(caCertsPath, rootCertPath))
        print(process.after)
    else:
        process = pexpect.spawn('keytool -import -trustcacerts -storepass changeit -alias hte-enterprise -keystore {} -file {} -noprompt'.format(caCertsPath, rootCertPath))
    #keytool -import -trustcacerts -alias hte-enterprise -keystore /etc/ssl/certs/java/cacerts -file rootCA.crt

    process.read()


def copyCerts():
    #make a copy of the old domain file
    try:
        shutil.copy("{}domain.p12".format(certPath),"{}domain_{}.p12".format(certPath, today.strftime("%b-%d-%Y")))
    except:
        print("No pre-existing file found in cert folder")
    #copy in the new domain file
    shutil.copy("domain.p12","{}domain.p12".format(certPath))
    #Let Tomcat read and execute this certificate
    subprocess.run(["chmod 755 {}domain.p12".format(certPath)], shell=True)
    return

def restartTomcat():
    subprocess.run(["systemctl restart tomcat9.service"], shell=True)


def parseFQDN():
    global hostnameFQDN, hostname
    if(hostnameFQDN != None):
        domainindex = hostnameFQDN.find('.')
        if(domainindex == -1):
            print("WARNING: Domain not included in hostname!")
            hostname = hostnameFQDN
        else:
            hostname = hostnameFQDN[:domainindex]
    else:
        hostnameFQDN = socket.getfqdn()
        hostname = socket.gethostname()
        



def versionToInt(versionStr : str):
    total = 0
    if not re.search("([\d.]+){2}", versionStr):
        return total
    try:
        array = versionStr.split(".")
        for count, ele in enumerate(array):
            total += pow(10, 6-(count*3)) * int(ele)
    except:
        total = 0
    return total


#==================================================================================================
#Beginning of Script
certInstall = False
frontendInstall = False
rootCaInstall = False
useHTTP = False
useHostName = False

#Handle Args
if len(sys.argv) > 0: #No Arguments provided
    sys.argv.pop(0)
    while len(sys.argv) > 0:
        currentArg = sys.argv.pop(0)
        if currentArg == "--help" or currentArg == "-h":
            printUsage()
            exit()
        elif currentArg == "-v" or currentArg == "--version":
            frontendRelease = sys.argv.pop(0)
        elif currentArg == "-b" or currentArg == "--buildPath":
            buildPath = sys.argv.pop(0)
        elif currentArg == "c" or currentArg == "--certPath":
            certPath = sys.argv.pop(0)
        elif currentArg == "-i" or currentArg == "--serverIP":
            newIP = sys.argv.pop(0)
        elif currentArg == "-f" or currentArg == "--serverHostname":
            hostnameFQDN = sys.argv.pop(0)
        elif currentArg == "-g" or currentArg == "--genCert":
            certInstall = True
        elif currentArg == "-u" or currentArg == "--installFrontend":
            frontendInstall = True
        elif currentArg == "-p" or currentArg == "--useHTTP":
            useHTTP = True
        elif currentArg == "-d" or currentArg == "--useHostname":
            useHostName = True
        elif currentArg == "-k" or currentArg == "--trustRootCA":
            rootCaInstall = True
        else:
            print('\033[91m' + "Unknown Arg Type : {}".format(currentArg) + '\033[0m')
            printUsage()
            exit()



if frontendRelease == None:
    frontendRelease = ""
    #Auto grab the latest version from the builds folder
    for item in os.listdir(buildPath):
        if versionToInt(item) > versionToInt(frontendRelease):
            frontendRelease = item

if newIP == "255.255.255.255":
    #Calculate IP     
    newIP = socket.gethostbyname(socket.gethostname() + ".local")
    if newIP == None:
        print("ERROR: Could not resolve IP address")
        exit()
parseFQDN()


print("Current Inputs: ")
print("     Using frontend path :   {} ".format(buildPath+frontendRelease))
print("     Using IP Address :      {}".format(newIP))
print("     Hostname :              {}".format(hostname))
print("     FQDN :                  {}".format(hostnameFQDN))
print("")
print("Tasks to be done: ")
if(certInstall):
    print("     - Create and install new certificate")
    print("             CertPath: {}".format(certPath))
if(frontendInstall):
    print("     - Update and install frontend")
if(rootCaInstall):
    print("     - Install Root Cert in Java Keytool")
    print("             rootCert path is: {}".format(rootCertPath))
    print("             caCert path is: {}".format(caCertsPath))

cont = input("Is the above information correct? (y/n):")

if cont != "y":
    print("Information not correct, Aborting ...")
    exit()

if certInstall:
    print("Updating Cert config...")
    status = updateCertConfig()
    if(status != 0): exit()
    print("Creating cert...")
    createCert()
    print("Importing Cert...")
    copyCerts()

if frontendInstall:
    print("Updating frontend Endpoint...")
    frontEndpoint = newIP
    if useHostName:
        frontEndpoint = hostnameFQDN

    if useHTTP:
        updateFrontendEndpoints(buildPath+frontendRelease, frontEndpoint, http="http", newPort = "8080")
    else:
        updateFrontendEndpoints(buildPath+frontendRelease, frontEndpoint)
    print("Moving Frontend...")
    updateFrontend()


if certInstall:
    print("Restarting Tomcat...")
    restartTomcat()
    sleep(5)
    print("Finished!")

if rootCaInstall:
    trustRootCert()




