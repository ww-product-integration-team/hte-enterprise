1. Create /home/pi directory if it does not already exist

2. Install These Python packages on the device before running
pip install getmac termcolor netifaces

3. Drop fileUpdate folder and empty piConfig.json file containing empty brackets: {} in the /home/pi directory

4. Update the Crontab to run these programs
* * * * * /usr/bin/python /home/pi/fileUpdater/fileUpdate.py >> /tmp/out.txt 2>&1
0 * * * * /usr/bin/python /home/pi/heartbeat.py

5. On the fileUpdate server (the server the pi is pointing to). Set up the config.json file

    the config.json file in the fileupdate folder should reflect the files you'd like the pi to download like so

    Note: saveLocation is where the pi will save the file on the device

    {
    "downloadURL":"http://10.3.195.90:8002/",
    "backupDownloadURL":"http://hte-enterprise:8002/",
    "heartbeatURL":"http://hte-enterprise:8080/scale-service/heartbeat/",
    "itemLookupURL" : "",
    "files":[
        {"name": "item_lookup.py",
         "saveLocation": "/home/pi/"
        },
        {"name": "heartbeat.py",
         "saveLocation": "/home/pi/"
        },
        {"name": "generateJson.py",
         "saveLocation": "/home/pi/"
        }
    ]
}

6. Run the following command in the folder containing both the fileUpdater folder and all necessary .py files
    python3 -m http.server 8002

    This will open a simple server for the files to be downloaded






