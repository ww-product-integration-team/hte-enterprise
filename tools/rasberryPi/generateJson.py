import json
from random import randint

scaleModel = ["HTi-7LS", "HTi-LS", "HTiP-LS", "HTi-7ELS", "HTi-7LS2", "HTi-LS2", "HTiP-LS2", "HTi-7ELS2", "HTi-10ELS",
              "HTi-10ELS2", "HTi-7LH", "HTi-LH", "HTiP-LH", "HTi-7ELH", "HTi-10ELH", "HTs-LS", "HTsP-LS", "HTs-7LS",
              "HTs-7ELS", "HTs-LST", "HTsP-LST", "HTs-7LST", "HTs-7ELST", "HTi-SSLS", "HTi-SSLSB"]

appVersion = ["4.1.300", "4.1.600", "3.3.628", "3.3.655", "4.2.100"]


def generateJson():

    with open('fileUpdater/config.json', 'r') as openfile:
 
        # Reading from json file
        json_object = json.load(openfile)

    heartbeatURL = ""
    try:
        heartbeatURL = json_object["heartbeatURL"]
    except:
        print("Could not find heartbeat URL!")
    
    newConfig = {
        "heartbeatURL" : heartbeatURL,
        "deviceUuid" : None,
        "pluRecordCount" : randint(0,3000),
        "totalLabelsPrinted" : 0,
        "scaleModel": scaleModel[randint(0,len(scaleModel)-1)],
        "appVersion": appVersion[randint(0,len(appVersion)-1)],
        "serialNumber" : randint(1000000, 9999999),
        "lastCommunicationTimestamp" : None,
        "files" : []
    }

    # Serializing json
    newJson = json.dumps(newConfig, indent=4)
    # Writing to sample.json
    with open("piConfig.json", "w") as outfile:
        outfile.write(newJson)