import os
from urllib import response
import requests
import json
import socket
import datetime
from random import *
import netifaces
from getmac import get_mac_address as gma
from termcolor import colored
import csv
from generateJson import generateJson
import uuid

CONFIG_FILE = "piConfig.json"

hostname = socket.gethostname()
IPAddr = socket.gethostbyname(hostname)


def extract_ip():
    st = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        st.connect(('10.3.128.222', 1))
        IP = st.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        st.close()
    return str(IP)

IPAddr = extract_ip()

def extract_gateway():
    gws = netifaces.gateways()
    gateway = gws['default'][netifaces.AF_INET][0]

def writeResponse( ipAddress, responseTime):
    with open('responseTimes.csv', 'a+', newline = '') as responseLine:
        writer = csv.writer(responseLine)
        writtenResponse = [datetime.datetime.now(), ipAddress, responseTime]
        writer.writerow(writtenResponse)



mac = gma()
devicegateway = extract_gateway()
url = None
deviceUUID = None
scaleModel = None
pluRecordCount = 0
totalLabelsPrinted = 0
appVersion = None
serialNumber = None
lastComm = None
currentFiles = []
filePath = "piConfig.json"

ct = datetime.datetime.now()
timestamp = ct.strftime('%Y-%m-%dT%H:%M:%SZ')

try:
    uuidF = None
    with open(filePath, 'r') as openfile:
        uuidF = json.load(openfile)
        device = uuidF["deviceUuid"]
except:
    print("Could not access piConfig.json!")
    generateJson()

print("Grabbing piConfig...")
try:
    uuidF = None
    with open(filePath, 'r') as openfile:
        uuidF = json.load(openfile)
        deviceUUID = uuidF["deviceUuid"]
        url = uuidF["heartbeatURL"]
        pluRecordCount = uuidF["pluRecordCount"]
        totalLabelsPrinted = uuidF["totalLabelsPrinted"]
        scaleModel = uuidF["scaleModel"]
        appVersion = uuidF["appVersion"]
        serialNumber = uuidF["serialNumber"]
        lastComm = uuidF["lastCommunicationTimestamp"]
        currentFiles = uuidF["files"]
        #Update Labels printed
        uuidF["totalLabelsPrinted"] +=1
    
    with open(filePath, 'w') as openfile:
        newJson = json.dumps(uuidF, indent=4)
        openfile.write(newJson)


except:
    print("Could not get piConfig info!")

heartbeatData = {
    "deviceUuid": deviceUUID,
    "healthCheck": {
        "lastCommunicationTimestampUtc": lastComm,
        "lastItemChangeReceivedTimestampUtc": "2022-10-07T21:16:50Z",
        "lastRebootTimestampUtc": "2022-10-19T18:35:52Z",
        "pluRecordCount": pluRecordCount,
        "totalLabelsPrinted": totalLabelsPrinted
    },
    "hostname": hostname,
    "ipAddress": IPAddr,
    "localBackupFileInfo": currentFiles,
    "messageUuid": str(uuid.uuid4()),
    "scaleInfo": {
        "appHookVersion": "2.1.9-pi",
        "application": appVersion,
        "buildNumber": "hte-4.1.300-04212022_151433",
        "countryCode": "US",
        "deptNumber": 0,
        "features": [
            "bluez",
            "connectstatusupdate",
            "costco_meathws_hti_v8_20220721",
            "fbvnc",
            "hte_apphook_v2",
            "hteapphookconfig",
            "office_viewer",
            "pdf_viewer",
            "wm_mx_nutri_cloud_client",
            "wmca_ssqr",
            "wpasupplicant"
        ],
        "licenses": [
            "HONEY BAKED HAM\nHONEY BAKED HAM\nALPHARETTA, GA\nProducts:\nSCALE_FULLSCREEN_FLASHKEYS\nType: USB_SERIAL_NUMBER\nAllotment: 1\nKey: 8A46A9216B2205776C3BD06D523D2E064A16DE3E\n",
            "HT SCALE\nHT SCALE\nHT SCALE\nProducts:\nSCALE_REMOTE_MONITOR\nType: SERVER_IP_ADDRESS\nAllotment: 1\nKey: 987BBC32BDD52B3C1AF8FE1568FCE0BBE6192680\n",
            "HOBART\nHOBART\nTROY, OH\nProducts:\nSCALE_CAT3_AUDITTRAIL\nType: SERVER_IP_ADDRESS\nAllotment: 9999\nKey: A7E2CCF01F1A4131BB64F1D6AC571F1CC8B60CD5\n",
            "DELHAIZE RETAIL SYSTEMS\nDELHAIZE USA\n145 PLEASANT HILL ROAD, SCARBOROUGH ME, 04074\nProducts:\nSCALE_OFFICE_VIEWER\nSCALE_PDF_VIEWER\nSCALE_REMOTE_MONITOR\nSCALE_CAT3_AUDITTRAIL\nType: USB_SERIAL_NUMBER\nAllotment: 1\nKey: 3531D539372F658842E219527D9AF4FB7FCDF492\n"
        ],
        "loader": "3.7.10",
        "operatingSystem": "4.9.88-1.0.0-hobart-4.0.0-gc946a96",
        "profileId": None,
        "scaleModel": scaleModel,
        "serialNumber": serialNumber,
        "smBackend": "3.1.17",
        "storeNumber": 0,
        "systemController": "1.0"
    },
    "scaleSettings": {
        "countryCode": "US",
        "deptNumber": 0,
        "hasCustomerDisplay": True,
        "hasLoadCell": True,
        "labelStockSize": "2.25in x 4.00in",
        "scaleItemEntryMode": "NORMAL",
        "scaleMode": "MANUAL",
        "storeGraphicName": "FoodCityLogoHT.png",
        "storeName": "FOOD CITY, LIVERMORE FALLS MAINE",
        "storeNumber": 0,
        "timeKeeper": "MANUAL",
        "timezone": "Eastern Standard Time",
        "weigherCalibratedTimestampUtc": None,
        "weigherConfiguredTimestampUtc": None,
        "weigherPrecision": "30 x 0.01 lb"
    },
    "timestampUTC": timestamp,
    "triggerType": "INTERVAL"
}

headers = {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Pragma': 'no-cache'
}

print("Sending Heartbeat out")
print("HB url: " + url)
response = requests.post(url, headers=headers, data=json.dumps(heartbeatData))
# prepared = req.prepare()

#Now Process the response and download any files


#update Data
if response.status_code == 200:
    print("HB Success!")
    response_dict = json.loads(response.text)
    try:
        uuidF = None
        with open(filePath, 'r') as openfile:
            uuidF = json.load(openfile)
            uuidF["deviceUuid"] = response_dict["deviceUuid"]
            print("     uuid is: {}".format(response_dict["deviceUuid"])) 
            uuidF["lastCommunicationTimestamp"] = timestamp
        
        with open(filePath, 'w') as openfile:
            newJson = json.dumps(uuidF, indent=4)
            openfile.write(newJson)
    except:
        print("Could not get piConfig info!")

else:
    print("Could not send heartbeat! ")
    print(response.status_code)
    print(response.text)
    exit()


#Read Events
successFiles = []
response_dict = json.loads(response.text)
try:
    for event in response_dict["events"]:
        print("EVENT: =====>")
        print(event)
        if event["eventType"] == "SYNC_CONFIG":
            for file in event["files"]:
                downloadurl = event["downloadUrl"]
                fileId = file["fileId"]
                print(file["fileName"])

                fileResponse = requests.get(downloadurl+"?id=" + fileId)

                print(fileResponse.status_code)

                if(fileResponse.status_code == 200):
                    successFiles.append(file)
except:
    print("Could not download!")



#update Data
try:
    uuidF = None
    with open(filePath, 'r') as openfile:
        uuidF = json.load(openfile)
        uuidF["files"] = successFiles
    
    with open(filePath, 'w') as openfile:
        newJson = json.dumps(uuidF, indent=4)
        openfile.write(newJson)
except:
    print("Could not get piConfig info!")
