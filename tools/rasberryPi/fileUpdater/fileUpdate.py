

from distutils.command.config import config
import json
import os
import requests
import datetime

"""
File Updater:
=============================================================
This script allows the pi or beagle board to update it's scripts by running this command periodically via Cron

It reads the config.json file to know where to find the updated files. When it recieves a new config file it will download the new config

This will tell it all of the updated files to download as well as where to save them

"""

DOWNLOAD_URL = "downloadURL"
BACKUP_URL = "backupDownloadURL"
FILES = "files"
FILENAME = "name"
SAVE_LOCATION = "saveLocation"
CONFIG = "config.json"
UPDATER_LOCATION = "/home/pi/fileUpdater/"

log = None
logSize = 0
try:
    logSize = os.path.getsize(os.path.join(UPDATER_LOCATION,"log.txt"))
except:
    logSize = 0

#Write new file if over 10MB otherwise append
if logSize > 10000000:
    log = open(UPDATER_LOCATION + "log.txt", "w")
else:
    log = open(UPDATER_LOCATION + "log.txt", "a")

#Open Current Local Config
f = open(UPDATER_LOCATION + CONFIG)
currentConfig = json.load(f)

def is_downloadable(url):
    """
    Does the url contain a downloadable resource
    """
    h = requests.head(url, allow_redirects=True)
    header = h.headers
    content_type = header.get('content-type')
    if 'text' in content_type.lower():
        return False
    if 'html' in content_type.lower():
        return False
    return True

#Starting the Config Download Script
ct = datetime.datetime.now()
toDownload = currentConfig[DOWNLOAD_URL] + 'fileUpdater/' + CONFIG
log.write("\n")
log.write("<-------> Current Time: {} <------->".format(ct))
log.write("\n\n Downloading new config from : " + toDownload)

workingURL = False
try:
    if not is_downloadable(toDownload):
        log.write("\nConfig is not downloadable, aborting!")
        exit()
    else:
        workingURL = True
except:
    log.write("\nCould not reach download endpoint!")

if not workingURL and currentConfig[BACKUP_URL] != "":
    toDownload = currentConfig[BACKUP_URL] + 'fileUpdater/' + CONFIG
    log.write("\nTrying backup download URL " +  toDownload)
    try:
        if not is_downloadable(toDownload):
            log.write("\nBackup Download Config is not downloadable, aborting!")
            exit()
    except:
        log.write("\nCould not reach backup download endpoint!")
        log.write("\nExiting....")
        exit()



r = requests.get(toDownload, allow_redirects=True)
open(UPDATER_LOCATION + CONFIG,"wb").write(r.content)
log.write("\nSuccessfully downloaded config!")


f = open(UPDATER_LOCATION + CONFIG)
currentConfig = json.load(f)
filesToDownload = currentConfig[FILES]
toDownload = currentConfig[DOWNLOAD_URL]
try:
    filePath = "/home/pi/piConfig.json"
    uuidF = None
    with open(filePath, 'r') as openfile:
        uuidF = json.load(openfile)
        uuidF["heartbeatURL"] = currentConfig["heartbeatURL"]
    
    with open(filePath, 'w') as openfile:
        newJson = json.dumps(uuidF, indent=4)
        openfile.write(newJson)
except:
    print("Could not set HB URL!")

for f in filesToDownload:
    try:
        name = f[FILENAME]
        saveLocation = f[SAVE_LOCATION]
    except:
        log.write("\nERROR could not grab filename, invalid format!")
        continue
    
    fileURL = toDownload + name
    
    try:
        log.write("\nDownloading " + fileURL)
        r = requests.get(fileURL, allow_redirects=True)
        open(saveLocation + name,"wb").write(r.content)
    except:
        log.write("\n \033[91m Error downloading " + fileURL + '\033[0m')
        continue
    log.write("\n \033[92m + Downloaded " + fileURL + '\033[0m')

log.write("\nfinished")
log.close()


