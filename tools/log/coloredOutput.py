import sys
import os
import time
from datetime import datetime

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Tail(object):
    ''' Represents a tail command. '''
    def __init__(self, tailed_file):
        ''' Initiate a Tail instance.
            Check for file validity, assigns callback function to standard out.
            
            Arguments:
                tailed_file - File to be followed. '''

        self.check_file_validity(tailed_file)
        self.tailed_file = tailed_file
        self.callback = sys.stdout.write

    def follow(self, s=1):
        ''' Do a tail follow. If a callback function is registered it is called with every new line. 
        Else printed to standard out.
    
        Arguments:
            s - Number of seconds to wait between each iteration; Defaults to 1. '''

        with open(self.tailed_file) as file_:
            # Go to the end of file
            file_.seek(0,2)
            while True:
                curr_position = file_.tell()
                line = file_.readline()
                if not line:
                    file_.seek(curr_position)
                    time.sleep(s)
                else:
                    self.callback(line)

    def register_callback(self, func):
        ''' Overrides default callback function to provided function. '''
        self.callback = func

    def check_file_validity(self, file_):
        ''' Check whether the a given file exists, readable and is a file '''
        if not os.access(file_, os.F_OK):
            raise TailError("File '%s' does not exist" % (file_))
        if not os.access(file_, os.R_OK):
            raise TailError("File '%s' not readable" % (file_))
        if os.path.isdir(file_):
            raise TailError("File '%s' is a directory" % (file_))

class TailError(Exception):
    def __init__(self, msg):
        self.message = msg
    def __str__(self):
        return self.message


def print_line(txt):
    ''' Prints received text '''
    formatText = txt

    requestStr = "- request:"
    idx = txt.find(requestStr)
    if idx != -1:
        subStr = txt[idx:idx+len(requestStr)]
        formatText = txt.replace(subStr, bcolors.WARNING + subStr)


    responseStr = "- response:"
    idx = formatText.find(responseStr)
    if idx != -1:
        subStr = formatText[idx:idx+len(responseStr)]
        formatText = formatText.replace(subStr, bcolors.OKGREEN + subStr)

    fileStr = "- file:"
    idx = formatText.find(fileStr)
    if idx != -1:
        subStr = formatText[idx:idx+len(fileStr)]
        formatText = formatText.replace(subStr, bcolors.OKCYAN + subStr)


    try:
        datetime.strptime(formatText[:19], "%m/%d/%Y %H:%M:%S")
        print(bcolors.OKBLUE + formatText[:23] + bcolors.ENDC + formatText[23:-1])
    except:
        print(formatText[:-1])

t = Tail(sys.argv[1])
t.register_callback(print_line)
t.follow(s=5)